<?php 
    require_once '../module/Pos/view/pos/nav/devices.php';
?>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#">Demo</a></li>
    </ul>
</div>

<div class="content">
    <div class="row">
        <div class="col-md-2">
            <div class="sidebar">
                <div class="sidebar-container" style="height: 100px;">
                    <div class="grid-stack-item container-0 container" data-type="container">
                        <div class="grid-stack-item-content">
                            <span class="title">Botões</span>
                        </div>
                    </div>
                </div>
                <div class="sidebar-logo" style="height: 100px;">
                    <div class="grid-stack-item logo" data-type="logo" data-gs-width="1" data-gs-height="1">
                        <div class="grid-stack-item-content">
                            <span class="title">Logo</span>
                        </div>
                    </div>
                </div>
                <div class="sidebar-customer" style="height: 100px;">
                    <div class="grid-stack-item customer" data-type="customer" data-gs-width="1" data-gs-height="1">
                        <div class="grid-stack-item-content">
                            <span class="title">Customers</span>
                        </div>
                    </div>
                </div>
                <div class="sidebar-employee" style="height: 100px;">
                    <div class="grid-stack-item employee" data-type="employee" data-gs-width="1" data-gs-height="1">
                        <div class="grid-stack-item-content">
                            <span class="title">Employee</span>
                        </div>
                    </div>
                </div>
                <div class="sidebar-lastsale" style="height: 100px;">
                    <div class="grid-stack-item lastsale" data-type="lastsale" data-gs-width="1" data-gs-height="1">
                        <div class="grid-stack-item-content">
                            <span class="title">Last Sale</span>
                        </div>
                    </div>
                </div>
                <div class="sidebar-order" style="height: 100px;">
                    <div class="grid-stack-item order" data-type="order" data-gs-width="1" data-gs-height="1">
                        <div class="grid-stack-item-content">
                            <span class="title">Order</span>
                        </div>
                    </div>
                </div>
                <div class="sidebar-table" style="height: 100px;">
                    <div class="grid-stack-item table" data-type="table" data-gs-width="1" data-gs-height="1">
                        <div class="grid-stack-item-content">
                            <span class="title">Tabela</span>
                        </div>
                    </div>
                </div>
                <div class="sidebar-abas" style="height: 100px;">
                    <div class="grid-stack-item abas" data-type="abas" data-gs-width="1" data-gs-height="1">
                        <div class="grid-stack-item-content">
                            <span class="title">Abas</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-10" style="padding-left: 4px;">
            <div class="grid-stack grid-stack-6" id="grid-main" style="width: 1024px; min-height: 140px; margin-top: 1px;"></div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="yes">Save</button>
      </div>
    </div>
  </div>
</div>

<?php

$url = $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
if(stripos($_SERVER['SERVER_SIGNATURE'], "443")) {
    $protocol = "https://";
} else {
    $protocol = "http://";
    
}
$url_base = $protocol.substr($url, 0, -16);
?>

<link rel="stylesheet" href="<?php echo $url_base.'assets/plugins/GridStack/gridstack.css'?>"/>
<link rel="stylesheet" href="<?php echo $url_base.'assets/plugins/GridStack/gridstack-extra.css'?>"/>
<link rel="stylesheet" href="<?php echo $url_base.'assets/plugins/GridStack/pos.css'?>"/>
<link rel="stylesheet" href="<?php echo $url_base.'assets/plugins/ColorPicker/css/bootstrap-colorpicker.css'?>"/>

<script src="<?php echo $url_base.'assets/plugins/GridStack/lodash.min.js'?>"></script>
<script src="<?php echo $url_base.'assets/plugins/GridStack/knockout-min.js'?>"></script>
<script src="<?php echo $url_base.'assets/plugins/GridStack/gridstack2.js'?>"></script>
<script src="<?php echo $url_base.'assets/plugins/GridStack/gridstack.JQueryUI.js'?>"></script>
<script src="<?php echo $url_base.'assets/plugins/ColorPicker/js/bootstrap-colorpicker.min.js'?>"></script>
<script src="<?php echo $url_base.'assets/plugins/GridStack/pos.js'?>"></script>
