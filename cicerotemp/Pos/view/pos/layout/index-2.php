<?php
    $url = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    if(stripos($_SERVER['SERVER_SIGNATURE'], "443")) {
        $protocol = "https://";
    } else {
        $protocol = "http://";

    }
    $url_base = $protocol.substr($url, 0, -16);
    
    $select_pos = "";
    $idDeviceCurrent = "";
    $current_mac = AuthenticationController::getMac();
    foreach ($array_answer['listPos'] as $value) {
        if ($value->getMacaddress() == $current_mac) {
            $idDeviceCurrent = $value->getIdpos();
            $current_mac = '<option value="' . $value->getIdpos() . '">#' . $value->getIdpos() . ' - ' . $value->getNickname() . '</option>';
        } else {
            $select_pos = '<option value="' . $value->getIdpos() . '">#' . $value->getIdpos() . ' - ' . $value->getNickname() . '</option>';
        }
    }
    $select_function = "<option value='none'>None</option>";
    foreach ($array_answer['functions'] as $value){
        $select_function .= "<option value='".$value->getAction()."'>".$value->getName()."</option>";
    }
    $select_pos = $current_mac . $select_pos;
?>

<link rel="stylesheet" href="<?php echo $url_base ?>assets/plugins/jQueryUI/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo $url_base ?>assets/plugins/ColorPicker/css/bootstrap-colorpicker.css">
<script type="text/javascript" src="<?php echo $url_base ?>assets/plugins/ColorPicker/js/bootstrap-colorpicker.js"></script>
<script type="text/javascript" src="<?php echo $url_base ?>assets//js/build-layout-pos.js"></script>
<script type="text/javascript">$('#posSelect').val('<?php echo $idDeviceCurrent ?>');</script>

<style>
    .content {
        margin-top: 20px;
    }
    .bar-layout {
        margin-top: 20px;
    }
    .button p {
        padding: 0px;
    }
    #dropHere {
        width:600px;
        height: 400px;
        margin-top: 30px;
        margin-left: 30px;
        margin-right: 30px;
        margin-bottom: 30px;
        height: 400px;
        background-color: #fff;
        border-radius: 5px;
        outline: red dashed 2px;  
    }
    .bordered-ui {
        outline: green dashed 2px;
    }
    .colorpicker-2x .colorpicker-saturation {
        width: 200px;
        height: 200px;
    }
    
    .colorpicker-2x .colorpicker-hue,
    .colorpicker-2x .colorpicker-alpha {
        width: 30px;
        height: 200px;
    }
    
    .colorpicker-2x .colorpicker-color,
    .colorpicker-2x .colorpicker-color div {
        height: 30px;
    }
</style>

<div class="row">
    <div class="bar-layout">
        <div class="col-md-12">
            <div class="btn-group pull-right">
                <button class="btn btn-primary" onclick="window.history.back();">Back</button>
                <button class="btn btn-primary" onclick="$('#dropHere').html('')">Reset</button>
                <button class="btn btn-success">Save</button>
            </div>
        </div>
    </div>
</div>

<div class="content" style="padding-bottom:30px;">
    <div class="row" id="init_layout">
        <div class="bar-widgets">
            <ul class="widgets list-inline">
                <li><div id="button-widget" class="button-widget"><span class="lnr lnr-bold"></span><p>Button</p></div></li>
                <li><div id="image-widget" class="image-widget"><span class="lnr lnr-picture"></span><p>Image</p></div></li>
                <li><div id="customer-widget" class="customer-widget"><span class="lnr lnr-user"></span><p>Customer</p></div></li>
                <li><div id="employee-widget" class="employee-widget"><span class="lnr lnr-users"></span><p>Employee</p></div></li>
                <li><div id="order-info-widget" class="order-info-widget"><span class="lnr lnr-picture"></span><p>Order Info</p></div></li>
                <li><div id="last-sale-widget" class="last-sale-widget"><span class="lnr lnr-layers"></span><p>Last Sale</p></div></li>
                <li><div id="table-widget" class="table-widget"><span class="lnr lnr-list"></span><p>Table</p></div></li>
                <li><div id="tabs-widget" class="tabs-widget"><span class="lnr lnr-layers"></span><p>Tab</p></div></li>
<!--                <li><div id="tabs-widget" class="tabs-widget"><span class="lnr lnr-layers"></span><p>Keyboard</p></div></li>
                <li><div id="tabs-widget" class="tabs-widget"><span class="lnr lnr-layers"></span><p>Calculator</p></div></li>-->
            </ul>
        </div>
        <div id="dropHere" class="screen-device"></div>
    </div>
</div>

<!-- Open Button -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <form id="form_info_btn">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-5">
                            <input type="text" id="idElemBtn" name="idElemBtn" style="display: none">
                            <label for="nameBtn" class="label-style">Name button</label>
                            <input type="text" class="form-control" id="nameBtn" name="nameBtn" >
                        </div>
                        <div class="col-md-2">
                            <label for="fontSize" class="label-style">Font Size</label>
                            <input type="number" class="form-control" id="fontSize" name="fontSize" >
                        </div>
                        <div class="col-md-2">
                            <label for="fontColor" class="label-style">Font Color</label>
                            <input id="cp8" name="fontColor" type="text" class="form-control" value="" />
                        </div>
                        <div class="col-md-3">                            
                            <label for="backgroundColor" class="label-style">Background Color</label>
                            <input id="cp9" name="backgroundColor" type="text" class="form-control" value="" />
                        </div>
                        <div class="col-md-12 text-center" style="margin-top: 15px;">
                            <div class="alert alert-warning" role="alert"><p>What will be the functionality of this button?</p></div>
                        </div>
                        <div class="col-md-12 text-center">
                            <ul class="list-inline">
                                <li style="width: 45%">
                                    <label for="command" class="label-style">Command <a onclick="openListCommands()">Open list</a></label>
                                    <input type="text" class="form-control" id="command" name="command" >
                                </li>
                                <li>Or</li>
                                <li style="width: 45%">
                                    <label for="functionBTN" class="label-style">Function</label>
                                    <select name="functionBTN">
                                        <?php echo $select_function ?>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="pull-left">
                        <input type='number' style="display: none" value="0" id="product_select_btn" name='product_select_btn'>
                        <button type="button" class="btn btn-primary">Select Product</button>
                    </div>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel Button</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn-remove">Delete Button</button>
                            <button type="submit" class="btn btn-success" id="btn-save">Save Changes</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function openListCommands(){
        $('#allCommandsModal').modal({show : true});
        var tableCommands = $('#allCommands').DataTable( {
            "ajax": {"url": my_url+"Pos/layout/getAllCommands/"},
            "destroy": true,
            "retrieve": true,
            "columns": [
                { "data": "id" },
                { "data": "command" },
                { "data": "description" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
    }
    
    function saveLayout(){
        var htmlS = $('#dropHere').html();
        $.ajax({
            url : my_url+"Pos/layout/save/",
            type: "POST",
            dataType: 'JSON',
            data : 'structure=' + htmlS,
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    }
</script>
<style>
    #allCommands_length { padding: 0px;}
    #allCommands_filter { padding: 0px;}
    #allCommands_paginate { padding: 0px;}
</style>

<!-- All commands -->
<div class="modal fade" id="allCommandsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Commands</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="allCommands" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th>Command</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">    
    function openTabs(idTab){
        $('#openTabsModal').modal({show : true});
    }
    
    function addNewTab(elem){
        
    }
</script>

<!-- All Open Tabs -->
<div class="modal fade" id="openTabsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tabs</h4>
            </div>
            <form id="form_tabs">
                <div class="modal-body">
                    <div id="content" class="row">
                        <input type="text" id="idElem" name="idElem[]" style="display: none">
                        <div class="col-md-12">
                            <label for="nameBtn" class="label-style">Name Tab</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="tab[]">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" onclick="addNewTab(this)">+</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn-remove">Delete Tab</button>
                            <button type="submit" class="btn btn-success" id="btn-save">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>