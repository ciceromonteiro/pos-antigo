<?php 
    $select_device = "<option=''>Select Option</option>";
    foreach ($array_answer['devices'] as $value) {
        $select_device .= "<option value=".$value->getIdpos().">".$value->getNickname()."</option>";
    }
    
    $select_presets = "<option =''>Select Preset</option>";
    foreach ($array_answer['presets'] as $value) {
        $select_presets .= "<option value=".$value->getIdpospreset().">".$value->getDescription()."</option>";


    }
?>

<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#layout').DataTable( {
            "ajax": {"url": my_url+"Pos/Layout/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "device" },
                { "data": "resolution" },
                { "data": "description" },
                { "data": "layoutEdit" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_layout');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_layout_update');
            $.ajax({
                url : my_url+"Pos/Layout/getTemplate/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#layoutEditModal').modal({show : true});
                        $('#form_layout_update #idPosTmpt').val(data.data[0].id);
                        $('#form_layout_update #device').val(data.data[0].device);
                        $('#form_layout_update #resolution').val(data.data[0].resolution);
                        $('#form_layout_update #description').val(data.data[0].description);
                        if(data.data[0].preset == ""){
                            $('#form_layout_update #preset').val(0);
                        } else {
                            $('#form_layout_update #preset').val(data.data[0].preset);
                        }
                        
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_layout', function(e){
            var device = $('#device').val();
            var resolution = $('#resolution').val();
            var description = $('#description').val();
            var preset = $('#preset').val();
            var userlog = <?php echo $_SESSION['user']?>;
            

            e.preventDefault();
            $.ajax({
                url : my_url+"Pos/Layout/insertTemplate/" + device + '/' + resolution + '/' + description + '/' + preset + '/' + userlog,
                type: "POST",
                dataType: 'JSON',
                data : $('#form_layout').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                        $('#myModal').modal('hide');
                        reload_table(table);
                    }else{
                    if(data.result == 'duplicate'){
                        alert('An active template already exists for your device');
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_layout_update', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Pos/Layout/updateTemplate",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_layout_update').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                        $('#layoutEditModal').modal('hide');
                        reload_table(table);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idLayouts : ids};
            deleteItens('Pos/Layout/deleteTemplate', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#layout tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#layout');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
        
    function openLayoutTerminal(id){
        window.location = my_url+"Pos/layout/index/" + id;
    }
</script>

<!-- Menu -->
<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List of templates</a></li>
    </ul>
</div>

<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="layout" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('color')"></th>
                        <th style="width: 10px">ID</th>
                        <th class="translate">Device</th>
                        <th class="translate">Resolution</th>
                        <th class="translate">Description</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Layout</h4>
            </div>
            <form id="form_layout">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="device" class="label-style translate">Select Device</label>
                            <select name="device" id="device" required="true">
                                <?php echo $select_device ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="resolution" class="label-style translate">Select Resolution</label>
                            <select name="resolution" id="resolution" required="true">
                                <option value="">Select Option</option>
                                <option value="320x480">320px x 480px</option>
                                <option value="360x640">360px x 640px</option>
                                <option value="768x1024">768px x 1024px</option>
                                <option value="800x1280">800px x 1280px</option>
                                <option value="1024x768">1024px x 768px</option>
                                <option value="1024x821">1024px x 821px</option>
                                <option value="1280x600">1280px x 600px</option>
                                <option value="1920x900">1920px x 900px</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Description</label>
                            <input type="text" class="form-control" id="description" name="description" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="preset" class="label-style translate">Use Preset</label>
                            <select name="preset" id="preset" required="true">
                                <?php echo $select_presets ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="layoutEditModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Layout</h4>
            </div>
            <form id="form_layout_update">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="idPosTmpt" id="idPosTmpt" style="display: none" required="true">
                            <label for="device" class="label-style translate">Select Device</label>
                            <select name="device" id="device" required="true">
                                <?php echo $select_device ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="resolution" class="label-style translate">Select Resolution</label>
                            <select name="resolution" id="resolution" required="true">
                                <option value="">Select Option</option>
                                <option value="320x480">320px x 480px</option>
                                <option value="360x640">360px x 640px</option>
                                <option value="768x1024">768px x 1024px</option>
                                <option value="800x1280">800px x 1280px</option>
                                <option value="1024x768">1024px x 768px</option>
                                <option value="1024x821">1024px x 821px</option>
                                <option value="1280x600">1280px x 600px</option>
                                <option value="1920x900">1920px x 900px</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Description</label>
                            <input type="text" class="form-control" id="description" name="description" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="preset" class="label-style translate">Use Preset</label>
                            <select name="preset" id="preset" required="true">
                                <?php echo $select_presets ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>