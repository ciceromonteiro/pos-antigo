<?php 
    require_once '../module/Pos/view/pos/nav/devices.php';
?>

<ul class="navbar-three">
    <li <?php echo ($nav == "Features") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Pos/settings/index" ?>" class="translate">Features</a>
    </li>
    <li <?php echo ($nav == "Delivery List") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Pos/settings/delivery_list" ?>" class="translate">Delivery List</a>
    </li>
    <li <?php echo ($nav == "Closing parameters") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Pos/settings/closing_parameters" ?>" class="translate">Closing parameters</a>
    </li>
    <li <?php echo ($nav == "Credit Notes") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Pos/settings/credit_notes" ?>" class="translate">Credit Notes</a>
    </li>
    <li <?php echo ($nav == "Receipts") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Pos/settings/receipts" ?>" class="translate">Receipts</a>
    </li>
</ul>