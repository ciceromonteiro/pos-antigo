<?php
$nav = "Delivery List";
include "nav.php";

$select = '<option value="0">Select option</option>';
$select_template = "";
$select_printer = "";

foreach ($array_answer['templates'] as $value) {
    $select_template .= "<option value='" . $value->getIdordertmpt() . "'>" . $value->getName() . "</option>";
}

foreach ($array_answer['prints'] as $value) {
    $select_printer .= "<option value='" . $value->getIdprinter() . "'>" . $value->getDescription() . "</option>";
}
?>

<script type="text/javascript">
    $(document).ready(function () {
        //var idPos = $('#posSelect').val();
        function getData() {
            var idPos = $('#posSelect').val();
            $.ajax({
                url: my_url + "Pos/Settings/getDeliveryList/"+idPos,
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        var values = data.data[0];
                        for (var prop in values) {
                            if (prop == "group_identical_products" || prop == "confirm_print_before_print" ||
                                    prop == "show_price_in_print" || prop == "print_same_items_on_same_line" ||
                                    prop == "place_the_last_two_digits_of_the_purchase" || prop == "when_you_modify_a_purchase_order" ||
                                    prop == "print_two_rows_for_each_product" || prop == "omit_price_on_document" ||
                                    prop == "print_group_to_which_the_product_belongs" || prop == "print_the_id_to_which_the_product_belongs") {
                                //checkboxes
                                if (values[prop] == "on" || values[prop] == "1") {
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            } else if (prop == "idTemplate" || prop == "group_by" || prop == "print_param_idTemplate") {
                                //selects
                                document.getElementById(prop).value = values[prop];
                            } else {
                                //inputs
                                document.getElementById(prop).value = values[prop];
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }

        $(document).on('submit', '#pos_settings_delivery_list_form', function (e) {
            var pos = $('#posSelect').val();
            e.preventDefault();
            $.ajax({
                url: my_url + "Pos/Settings/updateDeliveryList/"+pos,
                type: "POST",
                dataType: 'JSON',
                data: $('#pos_settings_delivery_list_form').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        document.getElementById("pos_settings_delivery_list_form").reset();
        getData();
    });

    function deliveryList() {
            var pos = $('#posSelect').val();
            $.ajax({
                url: my_url + "Pos/Settings/getDeliveryList/"+pos,
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        var values = data.data[0];
                        for (var prop in values) {
                            if (prop == "group_identical_products" || prop == "confirm_print_before_print" ||
                                    prop == "show_price_in_print" || prop == "print_same_items_on_same_line" ||
                                    prop == "place_the_last_two_digits_of_the_purchase" || prop == "when_you_modify_a_purchase_order" ||
                                    prop == "print_two_rows_for_each_product" || prop == "omit_price_on_document" ||
                                    prop == "print_group_to_which_the_product_belongs" || prop == "print_the_id_to_which_the_product_belongs") {
                                //checkboxes
                                if (values[prop] == "on" || values[prop] == "1") {
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            } else if (prop == "idTemplate" || prop == "group_by" || prop == "print_param_idTemplate") {
                                //selects
                                document.getElementById(prop).value = values[prop];
                            } else {
                                //inputs
                                document.getElementById(prop).value = values[prop];
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }
</script>

<div class="content">
    <form id="pos_settings_delivery_list_form">
        <div class="row row-fluid" style="margin-bottom: 60px;">
            <div class="col-md-12">
                <div class="col-md-8">
                    <label for="title" class="translate">Title</label>
                    <input name="title" id="title" type="text" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="size_font" class="translate">Size font</label>
                    <input id="size_font" name="size_font" type="text" class="form-control" placeholder="Title">
                </div>
                <div class="col-md-6">
                    <label for="idTemplate" class="translate">Template</label>
                    <select id="idTemplate" name="idTemplate">
                        <?php echo $select . $select_template ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="group_by" class="translate">Group by</label>
                    <select name="group_by" id="group_by">
                        <option value="0">None</option>
                        <option value="1" class="translate">By product group</option>
                        <option value="2" class="translate">By product rules</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <h4 class="translate">Text additional</h4>
                    <textarea cols="5" rows="6" name="text_additional" id="text_additional" class="form-control"></textarea><br>
                    <p><input type="checkbox" id="group_identical_products"  name="group_identical_products"> <t class="translate">Group identical products</t></p>
                    <p><input type="checkbox" id="confirm_print_before_print" name="confirm_print_before_print"> <t class="translate">Confirm Print Before Printing</t></p>
                    <p><input type="checkbox" id="show_price_in_print"  name="show_price_in_print"> <t class="translate">Show price in print</t></p>
                    <p><input type="checkbox" id="print_same_items_on_same_line"  name="print_same_items_on_same_line"> <t class="translate">Print same items on same line</t></p>
                    <p><input type="checkbox" id="place_the_last_two_digits_of_the_purchase"  name="place_the_last_two_digits_of_the_purchase"> <t class="translate">Place the last two digits of the purchase order in bold in the delivery list</t></p>
                    <p><input type="checkbox" id="when_you_modify_a_purchase_order"  name="when_you_modify_a_purchase_order"> <t class="translate">When you modify a purchase order, the new delivery list prints only the changes</t></p>
                </div>

                <div class="col-md-6">
                    <h4 class="translate">Print parameters per carrier</h4>
                    <div class="col-md-12">
                        <label for="print_param_per_carrier_title" class="translate">Header</label>
                        <input id="print_param_per_carrier_title" name="print_param_per_carrier_title" type="text" class="form-control">
                    </div>
                    <div class="col-md-12">
                        <label for="print_param_idTemplate" class="translate">Printer</label>
                        <select name="print_param_idTemplate" id="print_param_idTemplate">
                            <?php echo $select . $select_printer ?>
                        </select>
                    </div>
                    <div class="col-md-12">
                        <p><br><input type="checkbox" id="print_two_rows_for_each_product" name="print_two_rows_for_each_product"> <t class="translate">Print two rows for each product</t></p>
                        <p><input type="checkbox" id="omit_price_on_document" name="omit_price_on_document"> <t class="translate">Omit price on document</t></p>
                        <p><input type="checkbox" id="print_group_to_which_the_product_belongs" name="print_group_to_which_the_product_belongs"> <t class="translate">Print group to which the product belongs</t></p>
                        <p><input type="checkbox" id="print_the_id_to_which_the_product_belongs" name="print_the_id_to_which_the_product_belongs"> <t class="translate">Print the id to which the product belongs</t></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="btns-footer">
            <div class="pull-right">
                <button type="reset" class="btn btn-primary translate">Cancel</button>
                <button type="submit" class="btn btn-success translate">Save</button>
            </div>
        </div>
    </form>
</div>
