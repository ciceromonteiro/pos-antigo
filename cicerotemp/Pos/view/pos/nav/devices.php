<?php 
    $current_mac = "";
    $permissions = getEm()->getRepository('UsersPermission')->findOneBy(array('usersusers' => $_SESSION['user'], 'permissionpermission' => 355));    
    
    foreach ($array_answer['listPos'] as $value) {
        if($permissions == NULL){
            if($value->getIdpos() == $_SESSION['pos']){
                $current_mac = '<option value="'.$value->getIdpos().'">#'.$value->getIdpos().' - '.$value->getNickname().'</option>';
            }
        } else if($permissions != NULL) {
            $current_mac .= '<option value="'.$value->getIdpos().'">#'.$value->getIdpos().' - '.$value->getNickname().'</option>';
        }
    }
?>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left" style="padding-top: 30px">
            <label class="translate">Select Device:</label>
            <select onchange="general();" id="posSelect">
                <?php echo $current_mac ?>
            </select>
        </div>
        
<!--        <div class="pull-right" style="padding-top: 60px">
            <div class="btn btn-group">
                <?php //if($permissions != NULL): ?>
                    <button class="btn btn-primary">Save</button>
                 <?php //endif; ?>
            </div>
        </div>-->
    </div>
</div>

<script type="text/javascript">
    function general(){
        var url_atual =window.location.href;
        if((url_atual.indexOf("receipts") != -1) == true){
            changeReceiptts();
        }else if((url_atual.indexOf("closing_parameters") != -1) == true){
            teste(); 
        }else if((url_atual.indexOf("index") != -1) == true){
            getIndex();

        }else if((url_atual.indexOf("delivery_list") != -1) == true){
            deliveryList();
        }else if((url_atual.indexOf("credit_notes") != -1) == true){
            creditNotes();
        }

    }


    $('#posSelect').val('<?php echo $_SESSION['pos'] ?>');
</script>