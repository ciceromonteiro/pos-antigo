<?php
    $url = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    if(stripos($_SERVER['SERVER_SIGNATURE'], "443")) {
        $protocol = "https://";
    } else {
        $protocol = "http://";

    }
    $url_base = $protocol.substr($url, 0, -16);
?>

<link rel="stylesheet" href="<?php echo $url_base ?>assets/plugins/jQueryUI/jquery-ui.min.css">
<script type="text/javascript" src="<?php echo $url_base ?>assets//js/build-layout-tables.js"></script>

<style>
    .content {
        margin-top: 20px;
    }
    .bar-layout {
        margin-top: 20px;
    }
    .button p {
        padding: 0px;
    }
    #dropHere {
        display: block;
        height: 400px;
        margin-top: 30px;
        margin-left: 30px;
        margin-right: 30px;
        margin-bottom: 30px;
        height: 400px;
        background-color: #fff;
        border-radius: 5px;
        outline: red dashed 2px;  
    }
    .bordered-ui {
        outline: green dashed 2px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <ul class="navbar-three">
                <li class="active">
                    <a href="#" class="translate">Tables</a>
                </li>
            </ul>
        </div>
        <div class="pull-right">
            <button class="btn btn-success translate" style="margin-top: 28px" onclick="saveTables()">Save</button>
        </div>
    </div>
</div>

<div class="content" style="padding-bottom:30px;">
    <div class="row" id="init_layout">
        <div class="bar-widgets">
            <ul class="widgets list-inline">
                <li><div id="tables-widget" class="tables-widget"><span class="lnr lnr-bold"></span><p class="translate">Table</p></div></li>
                <li><div id="section-widget" class="section-widget"><span class="lnr lnr-picture"></span><p class="translate">Section</p></div></li>
            </ul>
        </div>
        <div id="dropHere" class="screen-device"><?php echo $array_answer["structure"] ?></div>
    </div>
</div>

<script type="text/javascript">
    function saveTables(){
        var htmlS = $('#dropHere').html();
        $.ajax({
            url : my_url+"Pos/tables/save/",
            type: "POST",
            dataType: 'JSON',
            data : 'structure=' + htmlS,
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    }
    
    function saveSeatTable(){
        var id = $('#tablesModal #idElemTable').val();
        var qtySeats = $('#tablesModal #qtySeats').val();
        $('#dropHere .item-'+id).find('span').html(qtySeats);
        $('#tablesModal').modal('hide');
    }
    
    function deleteSeatTable(){
        var id = $('#tablesModal #idElemTable').val();
        $('.item-'+id).remove();
    }
    
    function saveSection(){
        var id = $('#sectionModal #idElemSection').val();
        var name = $('#sectionModal #name').val();
        $('#dropHere .item-'+id).find('p').html(name);
        $('#sectionModal').modal('hide');
    }
    
    function deleteSection(){
        var id = $('#sectionModal #idElemSection').val();
        $('.item-'+id).remove();
    }
</script>


<!-- Tables -->
<div class="modal fade" id="tablesModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate">Tables</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <input type="number" id="idElemTable" name="idElemTable" style="display: none">
                        <label for="qtySeats" class="label-style translate">Qty Seats</label>
                        <input type="text" class="form-control" id="qtySeats" name="qtySeats">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary translate" data-dismiss="modal" onclick="deleteSeatTable()">Delete</button>
                        <button type="submit" class="btn btn-primary translate" id="btn-save" onclick="saveSeatTable()">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Sections -->
<div class="modal fade" id="sectionModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate">Section</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <input type="number" id="idElemSection" name="idElemSection" style="display: none">
                        <label for="name" class="label-style translate">Name</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary translate" data-dismiss="modal" onclick="deleteSection()">Delete</button>
                        <button type="submit" class="btn btn-primary translate" id="btn-save" onclick="saveSection()">Save Changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>