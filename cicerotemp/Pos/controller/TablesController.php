<?php

class TablesController {

    public function index() {
        $navbar = "Pos|Tables";
        $html = getEm()->getRepository('Tables')->findOneBy(array('active' => 1));
        if(!empty($html)){
            $structure = $html->getStructure();
        } else {
            $structure = "";
        }
        $array_answer = array(
            "structure" => $structure
        );
        GenericController::template("Pos", "tables", "index", $navbar, $array_answer, 61);
    }
    
    public static function save(){
        if(isset($_POST['structure'])){
            $table = getEm()->getRepository('Tables')->findAll(array('active' => 1));
            if($table){
                try {
                    $table[0]->setStructure($_POST['structure']);
                    $table[0]->setDateUpdate(new DateTime());
                    $table[0]->setActive(1);
                    getEm()->persist($table[0]);
                    getEm()->flush();
                    $result  = 'success';
                    $message = 'query success';
                    $data = "";
                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
            } else {
                try {
                    $table = new Tables();
                    $table->setStructure($_POST['structure']);
                    $table->setDateCreate(new DateTime());
                    $table->setActive(1);
                    getEm()->persist($table);
                    getEm()->flush();

                    $result  = 'success';
                    $message = 'query success';
                    $data = "";

                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
            }
            
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

}

?>