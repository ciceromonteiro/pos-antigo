<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PosController {

    public function create() {
        try {
            $entityManager = getEm();

            $license = getLicenseServerCentral();
            $pos = new Pos();
            $pos->setPosCompanyposCompany();
            $pos->setPosNumber();
            $pos->getValidUntil($license["valid_until"]);
            $pos->setQtyUsers($license["qty_users"]);
            $pos->setQtyWaiters($license["qty_waiters"]);
            $pos->setActive(true);
            $pos->setLicense();

            $entityManager->persist($pos);
            $entityManager->flush();
            $message = "success";
        } catch (Exception $e) {
            $message = $e->getMessage();
        }
    }

    public function getLicenseServerCentral() {
        return $array_result = array(
            "valid_until" => "2017-12-12",
            "qty_users" => "1",
            "qty_waiters" => "2"
        );
    }

    public function generateSRV() {
        
    }

    public function getAddressMAC() {
        $agent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($agent, "Windows") !== false) {
            echo "Windows";
            ob_start();
            system('ipconfig-a');
            $mycom = ob_get_contents();
            ob_clean();
            $findme = "Physical";
            $pmac = strpos($mycom, $findme);
            $mac = substr($mycom, ($pmac + 36), 17);
            return $mac;
        } else {
            echo "Mac ou Linux";
            ob_start();
            system('ifconfig -a');
            $mycom = ob_get_contents();
            ob_clean();
            var_dump($mycom);
            $findme = "ether";
            $pmac = strpos($mycom, $findme);
            $mac = substr($mycom, ($pmac + 37), 18);
            return $mac;
        }
    }

    public function insertButton($nome, $select, $idbuttao) {
        $user = "root";
        $pass = "102030";

        try {
            $conn = new PDO('mysql:host=127.0.0.1;dbname=standard', $user, $pass);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO `button_info` (`namebutton`, `functionsbutton`, `idbuttonhtml`)"
                    . " VALUES ('$nome', '$select', '$idbuttao');";
            // use exec() because no results are returned
            $conn->exec($sql);
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }

        $conn = null;
    }

    public function selectButton() {
        $user = "root";
        $pass = "102030";

        $conn = new PDO('mysql:host=127.0.0.1;dbname=standard', $user, $pass);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = 'SELECT name, color, calories FROM fruit ORDER BY name';
        foreach ($conn->query($sql) as $row) {
            print $row['name'] . "\t";
            print $row['color'] . "\t";
            print $row['calories'] . "\n";
        }

        $conn = null;
    }
    
    public function deleteButton(){
        echo "";
    }

}
