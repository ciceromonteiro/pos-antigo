<?php
$select_orders = "";
foreach ($array_answer['orders'] as $value) {
    $select_orders .= "<option value='".$value->getIdorder()."'>".$value->getIdorder()."</option>";
}

$select_nvoice_tmpt = "";
foreach ($array_answer['invoiceTemplate'] as $value) {
    $select_nvoice_tmpt .= "<option value='".$value->getIdinvoiceTmpt()."'>".$value->getName()."</option>";
}
 

$nav = "Invoice";
include 'nav.php';
?>
<script type="text/javascript">
   var filter = 0;
   var value;
   var date;
         $(document).ready(function () {
            document.getElementById("id_value").value = "";
            document.getElementById("date").value = "";
            value = 0;
            date = 0;
        table = $('#invoiceTable').DataTable({
            "ajax": {"url": my_url + "Entry/invoice/listInvoice/"+ value+'/'+date},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "dataInvoice"},
                {"data": "idCustomer"},
                {"data": "nameCustomer"},
                {"data": "nameEmployee"},
                {"data": "liquidValue"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
        filter = 1;



        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#formCreateInvoice');
            $('#modalInvoiceCreate').modal({
                show : true
            });
        });

        $(document).on('submit', '#formCreateInvoice', function(e){
            var order = $('#formCreateInvoice #orderValue').val();
            order = b64EncodeUnicode(order);

            var invoiceTemplate = $('#formCreateInvoice #invoiceTemplateValue').val();
            invoiceTemplate = b64EncodeUnicode(invoiceTemplate);

            var id = $('#formCreateInvoice #idValue').val();
            id = b64EncodeUnicode(id);

            var dateDue = $('#formCreateInvoice #dateDueValue').val();
            if(dateDue == ""){
                dateDue = "0";
            }
            dateDue = b64EncodeUnicode(dateDue);

            var limitDateDiscount = $('#formCreateInvoice #limitDateDiscountValue').val();
            if(limitDateDiscount == ""){
                limitDateDiscount = "0";
            }
            limitDateDiscount = b64EncodeUnicode(limitDateDiscount);

            var printed = $('#formCreateInvoice #printedValue').val();
            if(printed == ""){
                printed = "0";
            }
            printed = b64EncodeUnicode(printed);

            var extraInfo = $('#formCreateInvoice #extraInfoValue').val();
            if(extraInfo == ""){
                extraInfo = "0";
            }
            extraInfo = b64EncodeUnicode(extraInfo);

            var divideParcelsValue = $('#formCreateInvoice #divideParcelsValue').val();
            if(divideParcelsValue == ""){
                divideParcelsValue = "0";
            }
            divideParcelsValue = b64EncodeUnicode(divideParcelsValue);

            var totalValueOrder = $('#formCreateInvoice #totalValueOrder').val();
            if(totalValueOrder == ""){
                totalValueOrder = "0";
            }
            totalValueOrder = b64EncodeUnicode(totalValueOrder);

            var qtyParcels = $('#formCreateInvoice #qtyParcels').val();
            if(qtyParcels == ""){
                qtyParcels = "0";
            }
            qtyParcels = b64EncodeUnicode(qtyParcels);

            var valueParcels = $('#formCreateInvoice #valueParcels').val();
            if(valueParcels == ""){
                valueParcels = "0";
            }
            valueParcels = b64EncodeUnicode(valueParcels);

            
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/invoice/insertInvoice/"+ order +"/"+ invoiceTemplate +"/"+ id +"/"+ dateDue +"/"+ limitDateDiscount +"/"+ printed +"/"+ extraInfo +"/"+ divideParcelsValue +"/"+ totalValueOrder +"/"+ qtyParcels +"/"+ valueParcels,
                type: "POST",
                dataType: 'JSON',
                data : "",
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modalInvoiceCreate').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $('#delete').attr('disabled', true);
        $("#invoiceTable tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#invoiceTable');
            if(itemSelected == 0){
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#delete').attr('disabled', false);
            }
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idInvoice : ids};
            deleteItens('Entry/invoice/deleteInvoice', array_deletes, table);
        });

        
  });
         function reloadTableCompanyBank(){
            value = 0;
             date = 0;
             document.getElementById("id_value").value = "";
             document.getElementById("date").value = "";
            table.destroy();
            table = $('#invoiceTable').DataTable({
            "ajax": {"url": my_url + "Entry/invoice/listInvoiceCompanyBankruptcy/"+ value+'/'+date},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "dataInvoice"},
                {"data": "idCustomer"},
                {"data": "nameCustomer"},
                {"data": "nameEmployee"},
                {"data": "liquidValue"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
            filter = 2;
         }

         function reloadTableInvoice(){
            value = 0;
             date = 0;
             document.getElementById("id_value").value = "";
             document.getElementById("date").value = "";
            table.destroy();
            table = $('#invoiceTable').DataTable({
            "ajax": {"url": my_url + "Entry/invoice/listInvoice/"+value+'/'+date},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "dataInvoice"},
                {"data": "idCustomer"},
                {"data": "nameCustomer"},
                {"data": "nameEmployee"},
                {"data": "liquidValue"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
            filter = 1;
         }

         function reloadTableOpenTickets(){
            value = 0;
             date = 0;
             document.getElementById("id_value").value = "";
             document.getElementById("date").value = "";
            table.destroy();
            table = $('#invoiceTable').DataTable({
            "ajax": {"url": my_url + "Entry/invoice/listForOpenTickets/"+ value+'/'+date},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "dataInvoice"},
                {"data": "idCustomer"},
                {"data": "nameCustomer"},
                {"data": "nameEmployee"},
                {"data": "liquidValue"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
            filter = 3;
         }

         function searchValueEdate(){
           value = $('#id_value').val();
           date = $('#date').val();
           if(value == "") {
                value = 0;
            }

            if (date == "") {
                date = 0;
            }else{    
                date = b64EncodeUnicode(date);
            }

           
           if(filter == 1){
            table.destroy();
                table = $('#invoiceTable').DataTable({
                "ajax": {"url": my_url + "Entry/invoice/listInvoice/"+ value+'/'+date},
                "columns": [
                    {"data": "checkbox"},
                    {"data": "id"},
                    {"data": "dataInvoice"},
                    {"data": "idCustomer"},
                    {"data": "nameCustomer"},
                    {"data": "nameEmployee"},
                    {"data": "liquidValue"}
                ],
                "language": {
                    "url": my_url + my_language + ".json"
                }
            });
            }else if(filter == 2){
                table.destroy();
                table = $('#invoiceTable').DataTable({
                "ajax": {"url": my_url + "Entry/invoice/listInvoiceCompanyBankruptcy/"+ value+'/'+date},
                "columns": [
                    {"data": "checkbox"},
                    {"data": "id"},
                    {"data": "dataInvoice"},
                    {"data": "idCustomer"},
                    {"data": "nameCustomer"},
                    {"data": "nameEmployee"},
                    {"data": "liquidValue"}
                ],
                "language": {
                    "url": my_url + my_language + ".json"
                }
            });
            }else if(filter == 3){
                table.destroy();
                table = $('#invoiceTable').DataTable({
                "ajax": {"url": my_url + "Entry/invoice/listForOpenTickets/"+ value+'/'+date},
                "columns": [
                    {"data": "checkbox"},
                    {"data": "id"},
                    {"data": "dataInvoice"},
                    {"data": "idCustomer"},
                    {"data": "nameCustomer"},
                    {"data": "nameEmployee"},
                    {"data": "liquidValue"}
                ],
                "language": {
                    "url": my_url + my_language + ".json"
                }
            });
            }
         }
         var mask = {
         money: function() {
            var el = this
            ,exec = function(v) {
            v = v.replace(/\D/g,"");
            v = new String(Number(v));
            var len = v.length;
            if (1== len)
            v = v.replace(/(\d)/,"0.0$1");
            else if (2 == len)
            v = v.replace(/(\d)/,"0.$1");
            else if (len > 2) {
            v = v.replace(/(\d{2})$/,'.$1');
            }
            return v;
            };

            setTimeout(function(){
            el.value = exec(el.value);
            },1);
         }

        }

        $(function(){
             $('#id_value').bind('keypress',mask.money)
            });

          function teste(){
              $.ajax({
                url: my_url + "Entry/invoice/teste",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
          }  


          function b64EncodeUnicode(str) {
            // first we use encodeURIComponent to get percent-encoded UTF-8,
            // then we convert the percent encodings into raw bytes which
            // can be fed into btoa.
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                function toSolidBytes(match, p1) {
                    return String.fromCharCode('0x' + p1);
            }));
        }


        function getValueOrder(){
            var order = $('#formCreateInvoice #orderValue').val();
            $.ajax({
                url: my_url + "Entry/invoice/getValueOrder/"+order,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#formCreateInvoice #totalValueOrder').val(data.data);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }

        function divideParcels(){
            var divide = $('#formCreateInvoice #divideParcelsValue').val();
            if(divide == 2){
                $('#formCreateInvoice #qtyParcels').attr('disabled', false);
                $('#formCreateInvoice #qtyParcels').attr('required', true);
            }else{
                $('#formCreateInvoice #qtyParcels').attr('disabled', true);
                $('#formCreateInvoice #qtyParcels').attr('required', false);
                $('#formCreateInvoice #qtyParcels').val("");

            }
        }

        function valuePerParcels(){
            var qtyParcels = $('#formCreateInvoice #qtyParcels').val();
            if(qtyParcels == ""){
                qtyParcels = "1";
            }
            qtyParcels = b64EncodeUnicode(qtyParcels);

            var ValueOrderTotal = $('#formCreateInvoice #totalValueOrder').val();
            if(ValueOrderTotal == ""){
                ValueOrderTotal = "0";
            }
            ValueOrderTotal = b64EncodeUnicode(ValueOrderTotal);
            $.ajax({
                url: my_url + "Entry/invoice/getValuePerParcels/"+qtyParcels+"/"+ValueOrderTotal,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#formCreateInvoice #valueParcels').val(data.data);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    //notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }


</script>
<style type="text/css">
    
.pointer{
    cursor: pointer;
}

</style>
<div class="content">
    <div class="row">
        <div class="row row-fluid">
            <div class="col-md-12">
                <div class="col-md-4">
                    <label class="label-style translate">Deadline date:</label>
                    <div class="input-group date">
                        <input id="date" type="text" class="form-control" id="dataStar" name="dataStar" required="true" onchange="searchValueEdate()">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-th"></i>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="id_report" class="translate">Maximum amount for credit in the ticket:</label>
                    <input id="id_value" type="number" name="id_value" class="form-control" onkeyup="searchValueEdate()">
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <button id="create" class="btn btn-primary" onclick="getValueOrder()"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                    <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
                </div>
            </div>
            <table id="invoiceTable" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Date of invoice</th>
                        <th class="translate">ID of customer</th>
                        <th class="translate">Name of customer</th>
                        <th class="translate">Name of employee</th>
                        <th class="translate">liquid value</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a class="pointer translate" onclick="reloadTableInvoice()">Refresh</a></li> 
                    <li><a class="pointer translate" onclick="reloadTableCompanyBank()">Companies in bankruptcy</a></li> 
                    <li><a href="#" class="translate">Print</a></li> 
                    <li><a class="pointer translate" onclick="reloadTableOpenTickets()">Information for open tickets</a></li> 
                </ul> 
            </div>
        </div>

    </div>
</div>

<!-- Create -->
<div class="modal fade" id="modalInvoiceCreate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="closeModalCreateProductInSection" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">create Invoice</h4>
            </div>
            <form id="formCreateInvoice" name="formCreateProductInSection" class="form_unit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8">
                            <label for="orderValue" class="label-style translate" required >Order:</label>
                            <select name="orderValue" id="orderValue" onchange="getValueOrder()">
                                <?php echo $select_orders ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="totalValueOrder" class="label-style translate">Value Total Order:</label>
                            <input type="text" class="form-control" id="totalValueOrder" name="totalValueOrder" min="1" disabled>
                        </div>

                        <div class="col-md-4">
                            <label for="divideParcelsValue" class="label-style translate" required>Divide in Parcels:</label>
                            <select name="divideParcelsValue" id="divideParcelsValue" onchange="divideParcels()">
                                <option value="1">No</option>
                                <option value="2">Yes</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="qtyParcels" class="label-style translate">Qty Parcels:</label>
                            <input type="text" class="form-control number" id="qtyParcels" name="qtyParcels" min="2" disabled onkeyup="valuePerParcels()">
                        </div>
                        <div class="col-md-4">
                            <label for="valueParcels" class="label-style translate">Value per parcel:</label>
                            <input type="text" class="form-control number" id="valueParcels" name="valueParcels" min="1" disabled>
                        </div>
                        <div class="col-md-8">
                            <label for="invoiceTemplateValue" class="label-style translate" required>Invoice Template</label>
                            <select name="invoiceTemplateValue" id="invoiceTemplateValue">
                                <?php echo $select_nvoice_tmpt ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="idValue" class="label-style translate">Id:</label>
                            <input type="number" class="form-control" id="idValue" name="idValue" min="1" required>
                        </div>
                        <div class="col-md-4">
                            <label for="dateDueValue" class="label-style translate">Date Due:</label>
                            <div class="input-group date">
                                <input id="dateDueValue" type="text" class="form-control" name="dateDueValue">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="limitDateDiscountValue" class="label-style translate">Limit Date Discount:</label>
                            <div class="input-group date">
                                <input id="limitDateDiscountValue" type="text" class="form-control" name="limitDateDiscountValue">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="printedValue" class="label-style translate">Printed:</label>
                            <div class="input-group date">
                                <input id="printedValue" type="text" class="form-control" name="printedValue">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <label for="extraInfoValue" class="label-style translate">Info Extra:</label>
                            <textarea class="form-control" rows="4" id="extraInfoValue"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>
