<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Oct 26, 2016
 */

?>
<script type="text/javascript">
    $(document).ready(function(){
        
        var table = $('#department').DataTable( {
            "ajax": {"url": my_url+"Entry/department/getAll"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" }
            ],
            "language": {
                "url": my_url + my_language+".json"
            }
        });
        
        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_department');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/department/getDepartment/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#form_department_edit #idDepartment').val(data.data[0].id);
                        $('#form_department_edit #nameDepartment').val(data.data[0].description);
                        $('#departmentEditModal').modal({show : true});
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_department', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/department/insert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_department').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_department_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/department/update",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_department_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#departmentEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idDepartments : ids};
            deleteItens('Entry/department/deleteDepartments', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#department tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#department');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="<?php echo URL_BASE."Entry/department/index"?>" class="translate">List</a></li>
    </ul>
</div>

<!-- Table list Departaments -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                    <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                    <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> Remove</button>
                </div>
            </div>
            <table id="department" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('department')"></th>
                    <th style="width: 10px">ID</th>
                    <th class="translate">Description</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Department</h4>
            </div>
            <form id="form_department" name="form_department" class="form_department">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameDepartment" class="label-style translate">Department</label>
                            <input type="text" class="form-control" id="nameDepartment" name="nameDepartment" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="departmentEditModal" tabindex="-1" role="dialog" aria-labelledby="labelDepartmentUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelDepartmentUpdate">Update Department</h4>
            </div>
            <form id="form_department_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameDepartment" class="label-style translate">Department</label>
                            <input type="text" id="idDepartment" name="idDepartment" style="display: none">
                            <input type="text" class="form-control" id="nameDepartment" name="nameDepartment" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>