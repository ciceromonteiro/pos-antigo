<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#color').DataTable( {
            "ajax": {"url": my_url+"Entry/Color/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_color');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_color_edit');
            $.ajax({
                url : my_url+"Entry/Color/getColor/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#colorEditModal').modal({show : true});
                        $('#form_color_edit #idColor').val(data.data[0].id);
                        $('#form_color_edit #nameColor').val(data.data[0].description);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_color', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Color/insert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_color').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_color_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Color/update",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_color_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#colorEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idColors : ids};
            deleteItens('Entry/Color/deleteColors', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#color tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#color');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<!-- div do menu -->
<div id ="navbar-three">
	<ul class = "navbar-three">
		<li class="active"><a href="#" class="translate">Lista</a></li>
	</ul>
</div>

<!--tabela listagem de cores-->
<div class = "content">
	<div class = "top-bar-buttons">
		<div class="button-group">
			<button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">Novo</t></button>
            <button id="update" class="btn btn-secondary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Editar</t></button>
            <button id="delete" class="btn btn-secondary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remover</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="color" class="table-dark" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('color')"></th>
                    <th style="width: 10px">ID</th>
                    <th class="translate">Description</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Color</h4>
            </div>
            <form id="form_color">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameColor" class="label-style translate">Color</label>
                            <input type="text" class="form-control" id="nameColor" name="nameColor" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="colorEditModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Color</h4>
            </div>
            <form id="form_color_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameColor" class="label-style translate">Color</label>
                            <input type="text" id="idColor" name="idColor" style="display: none">
                            <input type="text" class="form-control" id="nameColor" name="nameColor" >
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
