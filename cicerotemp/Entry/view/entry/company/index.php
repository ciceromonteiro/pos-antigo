<?php 
    $options = "";
    foreach ($array_answer["payment"] as $value){
        $options .= '<option value="'.$value->getIdpaymentMtd().'">'.$value->getName().'</option>';
    }
?>

<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#company').DataTable( {
            "ajax": {"url": my_url+"Entry/Company/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "name_fantasy" },
                { "data": "register_number" },
                { "data": "info" },
                { "data": "description" },
                { "data": "payment_mtd"}
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_company');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_company_edit');
            $.ajax({
                url : my_url+"Entry/Company/getCompany/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#companyEditModal').modal({show : true});
                        $('#form_company_edit #idCompany').val(output.data[0].id);
                        $('#form_company_edit #nameCompany').val(output.data[0].name);
                        $('#form_company_edit #nameFantasy').val(output.data[0].name_fantasy);
                        $('#form_company_edit #registerNumber').val(output.data[0].register_number);
                        $('#form_company_edit #infoCompany').val(output.data[0].info);
                        $('#form_company_edit #descriptionCompany').val(output.data[0].name);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_company', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Company/insert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_company').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_company_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Company/update",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_company_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#companyEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idCompanys : ids};
            deleteItens('Entry/Company/deleteCompanys', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#company tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#company');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });               
    });
</script>

<!-- Menu -->
<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="company" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('company')"></th>
                    <th style="width: 10px" class="translate">ID</th>
                    <th class="translate">Name</th>
                    <th class="translate">Name Fantasy</th>
                    <th class="translate">Register Number</th>
                    <th class="translate">Info</th>
                    <th class="translate">Description</th>
                    <th class="translate">Payment Method</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Company</h4>
            </div>
            <form id="form_company" name="form_company" class="form_company">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameCompany" class="label-style translate">Name</label>
                            <input type="text" class="form-control" placeholder="Name" id="idCompany" name="idCompany" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="nameCompany" name="nameCompany" value="">
                        </div>
                        <div class="col-md-12">
                            <label for="nameFantasy" class="label-style translate">Name Fantasy</label>
                            <input type="text" class="form-control" placeholder="Name Fantasy" id="nameFantasy" name="nameFantasy" value="">
                        </div>
                        <div class="col-md-6">
                            <label for="nameCompany" class="label-style translate">Register Number</label>
                            <input type="text" class="form-control" placeholder="Register Number" id="register_number" name="registerNumber" value="">
                        </div>
                        <div class="col-md-6">
                            <label for="infoCompany" class="label-style translate">Default Payment Method</label>
                            <select name="paymentMtd">
                                <option value="">Select option</option>
                                <?php echo $options ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="infoCompany" class="label-style translate">Info</label>
                            <input type="text" class="form-control" placeholder="Info" id="infoCompany" name="infoCompany" value="">
                        </div>
                        <div class="col-md-6">
                            <label for="descriptionCompany" class="label-style translate">Description</label>
                            <input type="text" class="form-control" placeholder="Description" id="descriptionCompany" name="descriptionCompany" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="companyEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Company</h4>
            </div>
            <form id="form_company_edit" name="form_company" class="form_company">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameCompany" class="label-style translate">Name</label>
                            <input type="text" class="form-control" placeholder="Name" id="idCompany" name="idCompany" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="nameCompany" name="nameCompany" value="">
                        </div>
                        <div class="col-md-12">
                            <label for="nameFantasy" class="label-style translate">Name Fantasy</label>
                            <input type="text" class="form-control" placeholder="Name Fantasy" id="nameFantasy" name="nameFantasy" value="">
                        </div>
                        <div class="col-md-6">
                            <label for="registerNumber" class="label-style translate">Register Number</label>
                            <input type="text" class="form-control" placeholder="Register Number" id="register_number" name="registerNumber" value="">
                        </div>
                        <div class="col-md-6">
                            <label for="infoCompany" class="label-style translate">Info</label>
                            <input type="text" class="form-control" placeholder="Info" id="infoCompany" name="infoCompany" value="">
                        </div>
                        <div class="col-md-6">
                            <label for="descriptionCompany" class="label-style translate">Description</label>
                            <input type="text" class="form-control" placeholder="Description" id="descriptionCompany" name="descriptionCompany" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>