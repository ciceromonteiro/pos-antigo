<?php 
    $select = "<option value='0'>None</option>";
    $select_printer = "";
    
    foreach ($array_answer['printer'] as $value) {
        $select_printer .= "<option value='".$value->getIdprinter()."'>".$value->getDescription()."</option>";
    }
?>
<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#printSettings').DataTable( {
            "ajax": {"url": my_url+"Entry/Template/getAllPrinterSettings/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "idPrinter" },
                { "data": "numbeofwaystoprint" },
                { "data": "printvalues" },
                { "data": "printpriceswithtaxes" },
                { "data": "hidetotalprice" },
                { "data": "printObs" },
                { "data": "printtheobsinaseparateline" },
                { "data": "lineswithdifferentbackgrounds" },
                { "data": "sendByEmail" },
                { "data": "sendBySMS" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_print_settings_edit');
            $.ajax({
                url : my_url+"Entry/Template/getTemplatePrinterSettings/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#updatePrintSettings').modal({show : true});
                        
                        //checkboxes
                        if(data.data.printtheobsinaseparateline == "on" || data.data.printtheobsinaseparateline == "1"){
                            document.getElementById('printtheobsinaseparateline').checked = true;
                        } else {
                            document.getElementById('printtheobsinaseparateline').checked = false;
                        }

                        if(data.data.printObs == "on" || data.data.printObs == "1"){
                            document.getElementById('printObs').checked = true;
                        } else {
                            document.getElementById('printObs').checked = false;
                        }

                        if(data.data.hidetotalprice == "on" || data.data.hidetotalprice == "1"){
                            document.getElementById('hidetotalprice').checked = true;
                        } else {
                            document.getElementById('hidetotalprice').checked = false;
                        }

                        if(data.data.printpriceswithtaxes == "on" || data.data.printpriceswithtaxes == "1"){
                            document.getElementById('printpriceswithtaxes').checked = true;
                        } else {
                            document.getElementById('printpriceswithtaxes').checked = false;
                        }

                        if(data.data.lineswithdifferentbackgrounds == "on" || data.data.lineswithdifferentbackgrounds == "1"){
                            document.getElementById('lineswithdifferentbackgrounds').checked = true;
                        } else {
                            document.getElementById('lineswithdifferentbackgrounds').checked = false;
                        }

                        if(data.data.sendByEmail == "on" || data.data.sendByEmail == "1"){
                            document.getElementById('sendByEmail').checked = true;
                        } else {
                            document.getElementById('sendByEmail').checked = false;
                        }

                        if(data.data.sendBySMS == "on" || data.data.sendBySMS == "1"){
                            document.getElementById('sendBySMS').checked = true;
                        } else {
                            document.getElementById('sendBySMS').checked = false;
                        }

                        if(data.data.printvalues == "on" || data.data.printvalues == "1"){
                            document.getElementById('printvalues').checked = true;
                        } else {
                            document.getElementById('printvalues').checked = false;
                        }
                        //selects
                        if(data.data.idPrinter == '' || data.data.idPrinter == 0){
                            document.getElementById('printer').value = 0;
                        } else {
                            document.getElementById('printer').value = data.data.idPrinter;
                        }
                        
                        //inputs
                        $('#form_print_settings_edit #idTemplate').val(data.data.idTemplate);
                        $('#form_print_settings_edit #idOrderPrintSettings').val(data.data.idOrder);
                        $('#form_print_settings_edit #template').val(data.data.nameTemplate);
                        $('#form_print_settings_edit #numbeofwaystoprint').val(data.data.numbeofwaystoprint);                        
                        
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_print_settings_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Template/updatePrinterSettings",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_print_settings_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#updatePrintSettings').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idTemplatesPrintersSett : ids};
            deleteItens('Entry/Template/deletePrinterSettings', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#printSettings tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#printSettings');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<!-- Menu -->
<?php
    $nav = "Printer settings";
    include "nav.php";
?>

<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="printSettings" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('printSettings')"></th>
                    <th style="width: 10px" class="translate">ID</th>
                    <th class="translate">Template</th>
                    <th class="translate">Printer</th>
                    <th class="translate">Number of ways to print</th>
                    <th class="translate">Print values</th>
                    <th class="translate">Print prices with taxes</th>
                    <th class="translate">Hide total price</th>
                    <th class="translate">Print obs</th>
                    <th class="translate">Print the obs in a separate line</th>
                    <th class="translate">Lines with different backgrounds</th>
                    <th class="translate">Send by email</th>
                    <th class="translate">Send by SMS</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a href="#" class="translate">Define images of employee cards</a></li> 
                    <li><a href="#" class="translate">Define access card image</a></li> 
                    <li><a href="#" class="translate">Define Label Layout</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="updatePrintSettings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Template</h4>
            </div>
            <form id="form_print_settings_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="template" class="translate">Template</label>
                            <input name="idOrderPrintSettings" id="idOrderPrintSettings" type="text" style="display: none">
                            <input name="idTemplate" id="idTemplate" type="text" style="display: none">
                            <input name="template" id="template" type="text" class="form-control" disabled="true">
                        </div>
                        <div class="col-md-8">
                            <label for="printer" class="translate">Printer</label>
                            <select name="printer" id="printer">
                                <?php echo $select.$select_printer ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="numbeofwaystoprint" class="translate">Number of ways to print</label>
                            <input type="number" name="numbeofwaystoprint" id="numbeofwaystoprint" required="true" class="form-control">
                        </div>
                        <div class="col-md-6" style="margin-top: 20px">
                            <p><input type="checkbox" name="sendByEmail" id="sendByEmail"> <t class="translate">Send by email</t></p>
                            <p><input type="checkbox" name="sendBySMS" id="sendBySMS"> <t class="translate">Send by SMS</t></p>
                            <p><input type="checkbox" name="printvalues" id="printvalues"> <t class="translate">Print values</t></p>
                            <p><input type="checkbox" name="lineswithdifferentbackgrounds" id="lineswithdifferentbackgrounds"> <t class="translate">Lines with different backgrounds</t></p>
                        </div>
                        <div class="col-md-6" style="margin-top: 20px">
                            <p><input type="checkbox" name="printpriceswithtaxes" id="printpriceswithtaxes"> <t class="translate">Print prices with taxes</t></p>
                            <p><input type="checkbox" name="hidetotalprice" id="hidetotalprice"> <t class="translate">Hide total price</t></p>
                            <p><input type="checkbox" name="printObs" id="printObs"> <t class="translate">Print obs</t></p>
                            <p><input type="checkbox" name="printtheobsinaseparateline" id="printtheobsinaseparateline"> <t class="translate">Print the obs in a separate line</t></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>