<?php
/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Dec 1, 2016
 */
?>


<script type="text/javascript">

    /*module-folderview-pagename*/
    var pageName = "Entry-department-Index";

    $(document).ready(function () {

        //GetAll
        var table = $('#department').DataTable({
            "ajax": {"url": "<?php echo URL_BASE ?>Entry/customer/getAllPayment/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "namepay"},
                {"data": "invoices"},
                {"data": "timeinit"},
                {"data": "timeend"}
            ],
            "aoColumnDefs": [
                {"bSortable": true, "aTargets": [-1]}
            ],
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "oLanguage": {
                "sLengthMenu": "Records per page: _MENU_",
                "sInfo": "Total of _TOTAL_ records (showing _START_ to _END_)",
                "sInfoFiltered": "(filtered from _MAX_ total records)"
            }
        });


        function reload_table() {
            table.ajax.reload(null, false);
        }

        // Add button
        $(document).on('click', '#create', function (e) {
            e.preventDefault();
            $('#myModal').modal({
                show: true
            });
        });

        // Select tr
       /* $(document).on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
               //$('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                $(this).removeClass('selected');
            } else {
               // $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });*/

        // Add submit form
         $(document).on('submit', '#form_payment', function(e){
            var name = $('#form_payment #namePayment').val();
            var invoice = $('#form_payment #invoice:checked').val();
            var month = $('#form_payment #month').val();
            var year = $('#form_payment #year').val();
            var path = $('#form_payment #path').val();
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/customer/insertPayment/" + name + '/' + invoice + '/' + month + '/' + year + '/' + path,
                type: "POST",
                dataType: 'JSON',
                data : $('#form_payment').serialize(),
                   success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $("#namePayment").val("");
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
       

        // Get for update
    /*  $(document).on('click', '#update', function (e) {
            e.preventDefault();
            var id = table.$('tr.selected').find('a').data('id');
            var request = $.ajax({
                url: '<?php echo URL_BASE ?>Entry/customer/getPayment/' + id,
                cache: false,
                data: 'id=' + id,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                type: 'get'
            });
            request.done(function (output) {
                if (output.result === 'success') {
                    $('#form_payment_edit #idPayment').val(output.data.idPayment);
                    $('#form_payment_edit #namePayment').val(output.data.namePayment);
                    $('#form_payment_edit #path').val(output.data.path);
                    $('#form_payment_edit #invoice').prop('checked', output.data.invoice);
                    $('#form_payment_edit #month').val(output.data.month);
                    $('#form_payment_edit #year').val(output.data.year);
                    $('#departmentEditModal').modal({show: true});
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                }
            });
            request.fail(function (jqXHR, textStatus) {
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
                console.log(jqXHR.responseText);
            });
        });*/
          $(document).on('click', '#update', function(e){
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            //var id      = table.$('tr.selected').find('a').data('id');
            var request = $.ajax({
                url:          '<?php echo URL_BASE ?>Entry/customer/getPayment/'+id,
                cache:        false,
                data:         'id=' + id,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            request.done(function(output){
                if (output.result == 'success'){
                     $('#form_payment_edit #idPayment').val(output.data.idPayment);
                    $('#form_payment_edit #namePayment').val(output.data.namePayment);
                    $('#form_payment_edit #path').val(output.data.path);
                    $('#form_payment_edit #invoice').prop('checked', output.data.invoice);
                    $('#form_payment_edit #month').val(output.data.month);
                    $('#form_payment_edit #year').val(output.data.year);
                    $('#departmentEditModal').modal({show: true});
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(true);
                }
            });
            request.fail(function(jqXHR, textStatus){
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(false);
                console.log(jqXHR.responseText);
            });
        });


 // Edit submit form
        $(document).on('submit', '#form_payment_edit', function(e){
            e.preventDefault();
            var id = $('#form_payment_edit #idPayment').val();
            var name = $('#form_payment_edit #namePayment').val();
            var invoice = $('#form_payment_edit #invoice:checked').val();
            var month = $('#form_payment_edit #month').val();
            var year = $('#form_payment_edit #year').val();
            var path = $('#form_payment_edit #path').val();
            var form_data = $('#form_payment_edit').serialize();
            var request   = $.ajax({
                url:          '<?php echo URL_BASE ?>Entry/customer/updatePayment/' + id +'/'+ name +'/'+ invoice +'/'+ month +'/'+ year +'/'+path,
                cache:        false,
                data:         form_data,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#departmentEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
            
        });
        // Edit submit form
       /* $(document).on('submit', '#form_payment_edit', function (e) {
            e.preventDefault();
            var id = $('#form_payment_edit #idPayment').val();
            var name = $('#form_payment_edit #namePayment').val();
            var invoice = $('#form_payment_edit #invoice:checked').val();
            var month = $('#form_payment_edit #month').val();
            var year = $('#form_payment_edit #year').val();
            var path = $('#form_payment_edit #path').val();
            var form_data = $('#form_payment_edit').serialize();
            var request = $.ajax({
                url: '<?php echo URL_BASE ?>Entry/customer/updatePayment/'+ id +'/'+ name +'/'+ invoice +'/'+ month +'/'+ year +'/'+path,
                cache: false,
                data: form_data,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                type: 'get'
            });
            request.done(function (output) {
                if (output.result === 'success') {
                    var namePayment = $('#namePayment').val();
                    var title = "Added successfully";
                    var text = "Department '" + namePayment + "' edited successfully";
                    var icon = "glyphicon glyphicon-plus";
                    var type = "success";
                    $('#departmentEditModal').modal('hide');
                    reload_table();
                    notification(title, text, icon, type);
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                }
            });
            request.fail(function (jqXHR, textStatus) {
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
                console.log(jqXHR.responseText);
            });
        });*/

        // Delete
        $(document).on('click', '#delete', function (e) {
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
           // var name = table.$('tr.selected').find('a').data('name');
           // var id = table.$('tr.selected').find('a').data('id');
            var title = "Confirmation Needed";
            var text = "Are you sure you want to delete?";
            var icon = "glyphicon glyphicon-minus";
            confirmDelete(title, text, icon, id);
        });

        // Delete action
       function confirmDelete(title, text, icon, id, name){
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function(){
                $.ajax({
                    url:          '<?php echo URL_BASE ?>Entry/customer/deletePayment/' + id,
                    cache:        false,
                    dataType:     'json',
                    contentType:  'application/json; charset=utf-8',
                    type:         'get',
                     success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#GroupEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }

                });
                $('.ui-pnotify-modal-overlay').remove();
                    })
                    .on('pnotify.cancel', function(){
                        $('.ui-pnotify-modal-overlay').remove();
                    });
                                
                           
        }

        /*
        function confirmDelete(title, text, icon, id, name) {
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function () {
                var request = $.ajax({
                    url: '<?php echo URL_BASE ?>Entry/customer/deletePayment/' + id,
                    cache: false,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    type: 'get'
                });
                request.done(function (output) {
                    if (output.result === 'success') {
                        var title = "Deleted successfully";
                        var text = "Department '" + name + "' deleted successfully";
                        var icon = "glyphicon glyphicon-plus";
                        var type = "success";
                        $('#departmentEditModal').modal('hide');
                        reload_table();
                        notification(title, text, icon, type);
                    } else {
                        var namePayment = $('#namedepartment').val();
                        var title = "Delete request failed";
                        var text = "Department '" + namePayment + "' delete request failed";
                        var icon = "glyphicon glyphicon-minus";
                        var type = "error";
                        $('#departmentEditModal').modal('hide');
                        reload_table();
                        notification(title, text, icon, type);
                    }
                    $('.ui-pnotify-modal-overlay').remove();
                });
                request.fail(function (jqXHR, textStatus) {
                    var title = "Delete request failed";
                    var text = textStatus;
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                    console.log(jqXHR.responseText);
                });
            }).on('pnotify.cancel', function () {
                $('.ui-pnotify-modal-overlay').remove();
            });
        }
*/
        $("#department tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;



            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
            var itemSelected = checkMark('#department');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
               
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);

            }
            
           
        });

    });


</script>

<?php
$nav = "payment";
include 'nav.php';
?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div id="messageAlert" role="alert"></div>
        </div>
        <div class="col-md-12">
            <div class="button-group">
                <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
            </div>
            <table id="department" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('payments')"></th>
                        <th class="translate">ID</th>
                        <th class="translate">Name payment</th>
                        <th class="translate">Invoice</th>
                        <th class="translate">Month quantity</th>
                        <th class="translate">Year quantity</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Payment</h4>
            </div>
            <form id="form_payment" name="form_payment" class="form_payment">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="namePayment" class="label-style translate">Name</label>
                            <input type="text" class="form-control" placeholder="Name" id="idPayment" name="idPayment" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="namePayment" name="namePayment" value="">
                        </div>
                        <div class="col-md-4">
                            <label for="namePayment" class="label-style translate">Invoice</label>
                            <input type="checkbox" value="1" id="invoice" name="invoice" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="namePayment" class="label-style translate">Month quantity</label>
                            <input type="number" min="0" id="month" name="month" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="namePayment" class="label-style translate">Year quantity</label>
                            <input type="number" min="0" id="year" name="year" class="form-control">
                        </div>
                        <div class="col-md-12">
                            <label for="namePayment" class="label-style translate">Path Platform</label>
                            <input type="text" min="0" id="path" name="path" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="departmentEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Payment</h4>
            </div>
            <form id="form_payment_edit" name="form_payment" class="form_payment">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="namePayment" class="label-style translate">Name</label> 
                            <input type="text" class="form-control" placeholder="Name" id="idPayment" name="idPayment" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="namePayment" name="namePayment" value="">
                        </div>
                        <div class="col-md-4">
                            <label for="namePayment" class="label-style translate">Invoice</label>
                            <input type="checkbox" value="1" id="invoice" name="invoice" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="namePayment" class="label-style translate">Month quantity</label>
                            <input type="number" min="0" id="month" name="month" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <label for="namePayment" class="label-style translate">Year quantity</label>
                            <input type="number" min="0" id="year" name="year" class="form-control">
                        </div>
                        <div class="col-md-12">
                            <label for="namePayment" class="label-style translate">Path Platform</label>
                            <input type="text" min="0" id="path" name="path" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>