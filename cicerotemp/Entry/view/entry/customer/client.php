<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Dec 1, 2016
 */
?>


<script type="text/javascript">

    /*module-folderview-pagename*/
    var pageName = "Entry-department-Index";

    $(document).ready(function() {

        //GetAll
        var table = $('#department').DataTable( {
            "ajax": {"url": "<?php echo URL_BASE ?>Entry/customer/getAllClient/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "oLanguage": {
                "sLengthMenu":    "Records per page: _MENU_",
                "sInfo":          "Total of _TOTAL_ records (showing _START_ to _END_)",
                "sInfoFiltered":  "(filtered from _MAX_ total records)"
            }
        });


        function reload_table(){
            table.ajax.reload(null,false);
        }

        // Add button
        $(document).on('click', '#create', function(e){
            e.preventDefault();
            $('#myModal').modal({
                show : true
            });
        });

        // Select tr
        /*$(document).on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                $(this).removeClass('selected');
            }
            else {
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );*/

        // Add submit form
        $(document).on('submit', '#form_cliente', function(e){
            var nameGroup = $('#nameGroup').val();
            //alert(nameGroup);
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/customer/insertClient/"+nameGroup,
                type: "POST",
                dataType: 'JSON',
                data : $('#form_cliente').serialize(),
                   success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                       // $("#nameGroup").val("");
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
          

        // Get for update
        $(document).on('click', '#update', function(e){
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            //var id      = table.$('tr.selected').find('a').data('id');
            var request = $.ajax({
                url:          '<?php echo URL_BASE ?>Entry/customer/getClient/'+id,
                cache:        false,
                data:         'id=' + id,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            request.done(function(output){
                if (output.result == 'success'){
                    $('#form_cliente_edit #idGroup').val(output.data.id);
                    $('#form_cliente_edit #nameGroup').val(output.data.description);
                    $('#GroupEditModal').modal({show : true});
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(true);
                }
            });
            request.fail(function(jqXHR, textStatus){
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(false);
                console.log(jqXHR.responseText);
            });
        });

        // Edit submit form
        $(document).on('submit', '#form_cliente_edit', function(e){
            e.preventDefault();
            var id        = $('#form_cliente_edit #idGroup').val();
            var form_data = $('#form_cliente_edit').serialize();
            var request   = $.ajax({
                url:          '<?php echo URL_BASE ?>Entry/customer/updateClient/' + id,
                cache:        false,
                data:         form_data,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#GroupEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
            
        });

        // Delete
       $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            //var name = table.$('tr.selected').find('a').data('name');
            //var id    = table.$('tr.selected').find('a').data('id');
            var title = "Confirmation Needed";
            var text  = "Are you sure you want to delete?";
            var icon = "glyphicon glyphicon-minus";
            confirmDelete(title, text, icon, id, name);
        })

        // Delete action
       function confirmDelete(title, text, icon, id, name){
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function(){
                $.ajax({
                    url:          '<?php echo URL_BASE ?>Entry/customer/deleteClient/' + id,
                    cache:        false,
                    dataType:     'json',
                    contentType:  'application/json; charset=utf-8',
                    type:         'get',
                     success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#GroupEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }

                });
                $('.ui-pnotify-modal-overlay').remove();
                    })
                    .on('pnotify.cancel', function(){
                        $('.ui-pnotify-modal-overlay').remove();
                    });
                                
                           
        }


        $("#department tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;



            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
            var itemSelected = checkMark('#department');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
               
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);

            }
            
           
        });


    } );


</script>

<?php
$nav = "client";
include 'nav.php';
?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div id="messageAlert" role="alert"></div>
        </div>
        <div class="col-md-12">
            <div class="button-group">
                <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
            </div>
            <table id="department" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('groupCustomers')"></th>
                    <th class="translate">ID</th>
                    <th class="translate">Group Name</th>
                </tr>
                </thead>
            </table>
        </div>
        <div class="col-md-12">
     
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Group name</h4>
            </div>
            <form id="form_cliente" name="form_cliente" class="form_cliente">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameGroup" class="label-style translate">Name</label>
                            <input type="text" class="form-control" placeholder="Name" id="idGroup" name="idGroup" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="nameGroup" name="nameGroup" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="GroupEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Group name</h4>
            </div>
            <form id="form_cliente_edit" name="form_cliente_edit" class="form_cliente_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameGroup" class="label-style translate">Name</label>
                            <input type="text" class="form-control" placeholder="Name" id="idGroup" name="idGroup" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="nameGroup" name="nameGroup" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>