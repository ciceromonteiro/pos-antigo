<?php
/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Nov 29, 2016
 */

$UsuarioOBint = getEm()->getRepository("Users")->findBy(array("idusers" => $_SESSION["user"]));

if(!empty($UsuarioOBint)){
    $logTimeOB = getEm()->getRepository("LogTime")->findBy(array("usersusers" => $UsuarioOBint[0]), array("idlogTime" => "DESC"));
    
}
?>



<script type="text/javascript">
    function b64EncodeUnicode(str) {
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
            return String.fromCharCode('0x' + p1);
        }));
    }
    $(document).ready(function () {
        $(document).on('submit', '#form_customer', function (e) {
            var file = $('#file').val();
            if (file === "") {
                file = "0";
            }
            var name = $('#name').val();
            if (name === "") {
                name = "0";
            } else {
                name = b64EncodeUnicode(name);
            }
            var street = $('#street').val();
            if (street === "") {
                street = "0";
            } else {
                street = b64EncodeUnicode(street);
            }
            var zip = $('#zip').val();
            if (zip === "") {
                zip = "0";
            }
            var countryselect = $('#countryselectf').val();
            if (countryselect === "") {
                countryselect = "0";
            } else {
                countryselect = b64EncodeUnicode(countryselect);
            }
            var employsselect = $('#employsselect').val();
            if (employsselect === "") {
                employsselect = "0";
            }
            var namePerson = $('#namePerson').val();
            if (namePerson === "") {
                namePerson = "0";
            } else {
                namePerson = b64EncodeUnicode(namePerson);
            }
            var phoneclint = $('#phoneclint').val();
            if (phoneclint === "") {
                phoneclint = "0";
            }
            var emailclient = $('#emailclient').val();
            if (emailclient === "") {
                emailclient = "0";
            } else {
                emailclient = b64EncodeUnicode(emailclient);
            }
            var selectgroup = $('#selectgroup').val();
            if (selectgroup === "") {
                selectgroup = "0";
            }
            var idcompany = $('#idcompany').val();
            if (idcompany === "") {
                idcompany = "0";
            }
            var phonecompany = $('#phonecompany').val();
            if (phonecompany === "") {
                phonecompany = "0";
            }
            var emailcompany = $('#emailcompany').val();
            if (emailcompany === "") {
                emailcompany = "0";
            } else {
                emailcompany = b64EncodeUnicode(emailcompany);
            }

            var describe = $('#describe').val();
            if (describe === "") {
                describe = "0";
            }
            var taxlass = $('#taxlass:checked').val();
            if (taxlass === "") {
                taxlass = "0";
            }
            var viewname = $('#viewname:checked').val();
            if (viewname === "") {
                viewname = "0";
            }
            var invoiceg = $('#invoiceg:checked').val();
            if (invoiceg === "") {
                invoiceg = "0";
            }
            var withoutc = $('#withoutc:checked').val();
            if (withoutc === "") {
                withoutc = "0";
            }
            var printcompany = $('#printcompany:checked').val();
            if (printcompany === "") {
                printcompany = "0";
            }
            var hideinvoice = $('#hideinvoice:checked').val();
            if (hideinvoice === "") {
                hideinvoice = "0";
            }
            var invoiceelectro = $('#invoiceelectro:checked').val();
            if (invoiceelectro === "") {
                invoiceelectro = "0";
            }

            var themeinvoice = $('#themeinvoice').val();
            if (themeinvoice === "") {
                themeinvoice = "0";
            }
            var payment = $('#payment').val();
            if (payment === "") {
                payment = "0";
            }
            var sendinvoice = $('#sendinvoice').val();
            if (sendinvoice === "") {
                sendinvoice = "0";
            }
            var trackemail = $('#trackemail:checked').val();
            if (trackemail === "") {
                trackemail = "0";
            }
            var coincustomer = $('#coincustomer').val();
            if (coincustomer === "") {
                coincustomer = "0";
            }
            var redirectinvoice = $('#redirectinvoice').val();
            if (redirectinvoice === "") {
                redirectinvoice = "0";
            }
            var describeinvoice = $('#describeinvoice').val();
            if (describeinvoice === "") {
                describeinvoice = "0";
            }
            var boxcustomer = $('#boxcustomer:checked').val();
            if (boxcustomer === "") {
                boxcustomer = "0";
            }
            var nshowcustomer = $('#nshowcustomer:checked').val();
            if (nshowcustomer === "") {
                nshowcustomer = "0";
            }
            var exchagersend = $('#exchagersend:checked').val();
            if (exchagersend === "") {
                exchagersend = "0";
            }
            var ticket = $('#ticket').val();
            if (ticket === "") {
                ticket = "0";
            }
            var department = $('#department').val();
            if (department === "") {
                department = "0";
            }
            var project = $('#project').val();
            if (project === "") {
                project = "0";
            }
            var limit = $('#limit').val();
            if (limit === "") {
                limit = "0";
            }
            var emailpostcolle = $('#emailpostcolle').val();
            if (emailpostcolle === "") {
                emailpostcolle = "0";
            }
            var fax = $('#fax').val();
            if (fax === "") {
                fax = "0";
            }
            var birth = $('#birth').val();
            if (birth === "") {
                birth = "0";
            }
            var sms = $('#sms:checked').val();
            if (sms === "") {
                sms = "0";
            }
            var sendemailclient = $('#sendemailclient:checked').val();
            if (sendemailclient === "") {
                sendemailclient = "0";
            }
            var identification = $('#identification').val();
            if (identification === "") {
                identification = "0";
            }
            var genre = $('#genre').val();
            if (genre === "") {
                genre = "0";
            }
            var language = $('#language').val();
            if (language === "") {
                language = "0";
            }
            var zzstate = $('#zzstate').val();
            if (zzstate === "") {
                zzstate = "0";
            }
            var numbercustome = $('#numbercustome').val();
            if (numbercustome === "") {
                numbercustome = "0";
            }
            var citycustome = $('#citycustome').val();
            if (citycustome === "") {
                citycustome = "0";
            } else {
                citycustome = b64EncodeUnicode(citycustome);
            }
            var Complementcustome = $('#Complementcustome').val();
            if (Complementcustome === "") {
                Complementcustome = "0";
            }
            var districtcustome = $('#districtcustome').val();
            if (districtcustome === "") {
                districtcustome = "0";
            } else {
                districtcustome = b64EncodeUnicode(districtcustome);
            }
            var userlog = <?php echo $_SESSION["iduser"]; ?>;


            var urlfim = '<?php echo URL_BASE ?>Entry/customer/insertCustomer/' + name + '/'
                    + street + '/' + zip + '/' + countryselect + '/'
                    + employsselect + '/' + namePerson + '/' + phoneclint + '/'
                    + emailclient + '/' + selectgroup + '/'
                    + idcompany + '/' + phonecompany + '/' + emailcompany + '/'
                    + describe + '/' + taxlass + '/' + viewname + '/'
                    + invoiceg + '/' + withoutc + '/' + printcompany + '/'
                    + hideinvoice + '/' + invoiceelectro + '/' + themeinvoice + '/'
                    + payment + '/' + sendinvoice + '/' + trackemail + '/'
                    + coincustomer + '/' + redirectinvoice + '/' + describeinvoice + '/'
                    + boxcustomer + '/' + nshowcustomer + '/' + exchagersend + '/'
                    + ticket + '/' + department + '/' + project + '/' + limit + '/'
                    + emailpostcolle + '/' + fax + '/' + birth + '/' + sms + '/'
                    + sendemailclient + '/' + identification + '/' + genre + '/'
                    + language + '/' + userlog + '/' + zzstate + '/'
                    + numbercustome + '/' + citycustome + '/' + Complementcustome + '/'
                    + districtcustome;
            e.preventDefault();
            $.ajax({
                url: urlfim,
                type: "POST",
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values 
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $("#uploadimage").on('submit', (function (e) {          
            e.preventDefault();
            $("#message").empty();
            $('#loading').show();
            $.ajax({
                url: "ajax_php_file.php", // Url to which the request is send
                type: "POST", // Type of request to be send, called as method
                data: new FormData(this), // Data sent to server, a set of key/value pairs representing form fields and values 
                contentType: false, // The content type used when sending data to the server. Default is: "application/x-www-form-urlencoded"
                cache: false, // To unable request pages to be cached
                processData: false, // To send DOMDocument or non processed data file it is set to false (i.e. data should not be in the form of string)
                success: function (data)  		// A function to be called if request succeeds
                {
                    $('#loading').hide();
                    $("#message").html(data);
                }
            });
        }));
        $(function () {
            $("#file").change(function () {
                $("#message").empty(); // To remove the previous error message
                var file = this.files[0];
                var imagefile = file.type;
                var match = ["image/jpeg", "image/png", "image/jpg"];
                if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2])))
                {
                    $('#previewing').attr('src', 'noimage.png');
                    $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                    return false;
                } else
                {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
        function imageIsLoaded(e) {
            $("#file").css("color", "green");
            $('#image_preview').css("display", "block");
            $('#imagup').css("display", "none");
            $('#previewing').attr('src', e.target.result);
            $('#previewing').attr('width', '210px');
            $('#previewing').attr('height', '150px');
        }
        ;
    });




    function limpaFile() {
        $('#file').val("");
        $('#image_preview').css("display", "none");
        $('#imagup').css("display", "block");
    }

    function showHint() {
        var nameds = $('#nameCompanyModal').val();
        var FantasyName = $('#FantasyNameModal').val();
        var paymentds = $('#paymentCompany').val();
        var registernumber = $('#registernumber').val();
        var describeCompany = $('#describeCompany').val();
        if (nameds == "" || FantasyName == "" || paymentds == "" || registernumber == "") {
            $("#myModalerror").modal();
            setTimeout(function () {
                $("#fechaerromodal").click();
            }, 1500);
        } else {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    $('#idcompany1').val(this.responseText);
                    $('#idcompany').val(this.responseText);
                    $("#myModalsuccess").modal();
                    $("#myModalCompanyclose").click();
                    setTimeout(function () {
                        $("#successmodalclose").click();
                    }, 1500);
                }
            };
            xmlhttp.open("post", "<?php echo URL_BASE ?>Entry/company/insertCompany/"
                    + nameds + "/" + FantasyName + "/" + paymentds + "/" + registernumber
                    + "/" + describeCompany + "/" + <?php echo $logTimeOB[0]->getPosIdpos()->getIdpos(); ?>, true);
            xmlhttp.send();
        }
    }
    
</script>

<style type="text/css">
    input[type=file] {
        float: left;
        display: block;
    }
    .hide {
        display: none;
        float: left;
    }

    @media screen and (min-width: 768px) {

        #myModalCompany .modal-dialog  {width:900px;}

    }
</style>



<?php
$nav = "customer";
include 'nav.php';

$countryArray = getEm()->getRepository('ZzCountry')->findBy(array("active" => 1));
$userArray = getEm()->getRepository('Users')->findBy(array("active" => 1));
$customeGpArray = getEm()->getRepository('CustomerGp')->findBy(array("active" => 1));
$companyGpArray = getEm()->getRepository('Company')->findBy(array("active" => 1));
$invoiceTmptArray = getEm()->getRepository('InvoiceTmpt')->findBy(array("active" => 1));
$paymentArray = getEm()->getRepository('PaymentMtd')->findBy(array("active" => 1));
$ticketArray = getEm()->getRepository('TicketTmpt')->findBy(array("active" => 1));
$DepartmentArray = getEm()->getRepository('Department')->findBy(array("active" => 1));
$ProjectArray = getEm()->getRepository('Project')->findBy(array("active" => 1));
$LanguageArray = getEm()->getRepository('Language')->findBy(array("active" => 1));
$coinArray = getEm()->getRepository('Currency')->findBy(array("active" => 1));
?>


<div class="content">
    <div class="row">
        <div class="col-xs-12">
            <div id="messageAlert" role="alert"></div>
        </div>
        <div class="col-xs-12">
            <div class="form-group-sm">
                <form id="form_customer" name="form_customer" class="form_customer">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-3">
                                <label for="" class="label-style">Name</label>
                                <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="" required="">
                            </div>
                            <div class="col-xs-3">
                                <label for="" class="label-style">Street</label>
                                <input type="text" class="form-control" placeholder="Street" id="street" name="street" value="" required="">
                            </div>
                            <div class="col-xs-2">
                                <label for="" class="label-style">Zip</label> 
                                <input type="text" class="form-control" placeholder="Zip" id="zip" name="zip" value="" required="">
                            </div>
                            <div class="col-xs-2">
                                <label for="" class="label-style">Country</label>
                                <input list="countryselect" class="form-control" placeholder="Country" value="0" id="zzstate" name="zzstate" style="display: none">
                                <input list="countryselect" class="form-control" placeholder="Country" id="countryselectf" name="countryselect">
                                <datalist id="countryselect">
                                    <?Php foreach ($countryArray as $countryValue): ?>
                                        <option value="<?php echo $countryValue->getCode(); ?>"><?php echo $countryValue->getName(); ?></option>
                                    <?php endforeach; ?>
                                </datalist>
                            </div>
                            <div class = "col-xs-2">
                                <label for = "form_customer" class = "label-style">Phone</label>
                                <input type = "text" class = "form-control" placeholder = "Phone" required="" id = "phoneclint" name = "phoneclint" value = "">
                            </div>

                        </div>
                        <div class="row">
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">E-mail</label>
                                <input type = "email" class = "form-control" placeholder = "E-mail" id = "emailclient" name = "emailclient" value = "">
                            </div>
                            <div class = "col-xs-2">
                                <label for = "form_customer" class = "label-style">Number</label>
                                <input type = "text" class = "form-control" placeholder = "Number"  required="" id = "numbercustome" name = "numbercustome" value = "">
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">City</label>
                                <input type = "text" class = "form-control" placeholder = "City" id = "citycustome" name = "citycustome" value = "">
                            </div>
                            <div class = "col-xs-4">
                                <label for = "form_customer" class = "label-style">District</label>
                                <input type = "text" class = "form-control" placeholder = "District" id = "districtcustome" name = "districtcustome" value = "">
                            </div>

                        </div>

                        <div class = "row">
                            <div class = "col-xs-4">
                                <label for = "form_customer" class = "label-style">Complement</label>
                                <input type = "text" class = "form-control" placeholder = "Complement" id = "Complementcustome" name = "Complementcustome" value = "">
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Contact Person</label>
                                <input type = "text" class = "form-control" placeholder = "Contact Person" id = "namePerson" name = "namePerson" value = "">
                            </div>
                            <div class = "col-xs-2">
                                <label for = "form_customer" class = "label-style">Phone Contact</label>
                                <input type = "text" class = "form-control" placeholder = "Phone" id = "phonecompany" name = "phonecompany" value = "">
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">E-mail Contact</label>
                                <input type = "email" class = "form-control" placeholder = "E-mail" id = "emailcompany" name = "emailcompany" value = "">
                            </div>                
                        </div>
                        <div class="row">
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Customer Group</label>
                                <select class = "form-control" id = "selectgroup" name = "selectgroup" >
                                    <?php foreach ($customeGpArray as $customergpvalues): ?>
                                        <option value = "<?php echo $customergpvalues->getIdcustomerGp(); ?>"><?php echo $customergpvalues->getName(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Employs</label>
                                <select class="form-control" id ="employsselect" name="employsselect">
                                    <?php foreach ($userArray as $usersvalue): ?>
                                        <option value = "<?php echo $usersvalue->getIdusers(); ?>"><?php echo $usersvalue->getPersonperson()->getName(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class = "col-xs-4">
                                <label for = "form_customer" class = "label-style">ID Company</label>                      
                                <input list = "idcompany1" class = "form-control" placeholder = "Id Company" id = "idcompany" name = "idcompany1" value = "">
                                <datalist id="idcompany1">
                                    <?Php foreach ($companyGpArray as $companyValue): ?>
                                        <option value="<?php echo $companyValue->getIdcompany(); ?>"><?php echo $companyValue->getName(); ?></option>
                                    <?php endforeach; ?>
                                </datalist>
                            </div>
                            <div class="col-xs-2">

                                <center>
                                    <label for = "form_customer" class = "label-style">New Company</label>  <br>
                                    <a class="btn btn-default" data-toggle="modal" data-target="#myModalCompany">
                                        <span class="glyphicon glyphicon-modal-window"></span>
                                    </a></center>
                            </div>    
                        </div>  


                        <div class = "row">
                            <hr>
                            <div class = "col-xs-12">
                                <label for = "form_customer" class = "label-style">Describe</label>
                                <textarea class = "form-control" id = "describe" name = "describe">

                                </textarea>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-xs-12">
                                <input type = "checkbox" class = "" id = "taxlass" name = "taxlass" value="1"> <span>Without tax</span>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-xs-12">
                                <h2>Invoices</h2>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-xs-3">
                                <input type = "checkbox" class = "" id = "viewname" name = "viewname" value="1"> <span>View name in invoices</span><br>
                                <input type = "checkbox" class = "" id = "invoiceg" name = "invoiceg" value="1"> <span>Group invoice</span><br>
                                <input type = "checkbox" class = "" id = "withoutc" name = "withoutc" value="1"> <span>Without Customer</span><br>
                                <input type = "checkbox" class = "" id = "printcompany" name = "printcompany" value="1"> <span>Invoice define for imprint company</span><br>
                                <input type = "checkbox" class = "" id = "hideinvoice" name = "hideinvoice" value="1"> <span>Hide item's in payment invoiced</span><br>
                                <input type = "checkbox" class = "" id = "invoiceelectro" name = "invoiceelectro" value="1"> <span>Electron invoice</span>
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Theme Invoice</label>                              
                                <select class = "form-control" id = "themeinvoice" name = "themeinvoice" >
                                    <?php foreach ($invoiceTmptArray as $invoiceTmptValues): ?>
                                        <option value = "<?php echo $invoiceTmptValues->getIdinvoiceTmpt(); ?>"><?php echo $invoiceTmptValues->getName(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Payment</label>
                                <select class = "form-control" id = "payment" name = "payment" >
                                    <?php foreach ($paymentArray as $paymentValues): ?>
                                        <option value = "<?php echo $paymentValues->getIdpaymentMtd(); ?>"><?php echo $paymentValues->getName(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Send invoice in email</label>
                                <input type = "email" class = "form-control" placeholder = "Send Invoice in Email" id = "sendinvoice" name = "sendinvoice" value = "">
                                <input type = "checkbox" class = "" id = "trackemail" name = "trackemail" value="1"> <span>Track open email customers</span>

                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Coin Default by Customer</label>
                                <input list="coincustomerselect" class="form-control" placeholder="Coin Default by Customer" id="coincustomer" name="coincustomer" value="">

                                <datalist id="coincustomerselect">
                                    <?php foreach ($coinArray as $coinValue): ?>
                                        <option value="<?php echo $coinValue->getIdcurrency(); ?>"><?php echo $coinValue->getName(); ?></option>
                                    <?php endforeach; ?>
                                </datalist>
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Redirect Invoices for Customer</label>
                               <select class="form-control" id="redirectinvoice" name="redirectinvoice" >
                                   <option value="0">--Select--</option>
                                    <?php  foreach ($view as $viewValue): ?>

                                        <option value="<?php echo $viewValue->getIdperson(); ?>"><?php echo $viewValue->getName(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class = "col-xs-12">
                                <label for = "form_customer" class = "label-style">Describe</label>
                                <textarea class="form-control" id="describeinvoice" name="describeinvoice">

                                </textarea>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-xs-12">
                                <h2>Check Linst</h2>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-xs-3">
                                <input type = "checkbox" class="" id="boxcustomer" name="boxcustomer" value="1"> <span>When the box selects the customer
                                    Automatically opens a Customer describe </span><br>
                                <input type = "checkbox" class="" id="nshowcustomer" name="nshowcustomer" value="1"> <span>D'not show data of the customers in statical</span><br>
                                <input type = "checkbox" class="" id="exchagersend" name="exchagersend" value="1"> <span>Exchage andress company for andress register in send</span>
                            </div>
                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Theme Ticket</label>
                                <select class="form-control" id="ticket" name="ticket">
                                    <?php foreach ($ticketArray as $ticketvalue): ?>
                                        <option value = "<?php echo $ticketvalue->getIdticketTmpt(); ?>"><?php echo $ticketvalue->getTitle(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <br>
                                <label for = "form_customer" class = "label-style">Department</label>
                                <select class="form-control" id="department" name="department">
                                    <?php foreach ($DepartmentArray as $Departmentvalue): ?>
                                        <option value = "<?php echo $Departmentvalue->getIddepartment(); ?>"><?php echo $Departmentvalue->getName(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class = "col-xs-3">
                                <label for = "form_customer" class = "label-style">Project</label>
                                <select class="form-control" id="project" name="project">
                                    <?php foreach ($ProjectArray as $Projectvalue): ?>
                                        <option value = "<?php echo $Projectvalue->getIdproject(); ?>"><?php echo $Projectvalue->getName(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <br>
                                <label for = "form_customer" class = "label-style">Limit of credit in store</label>
                                <input type = "text" class = "form-control" placeholder = "Limit" id="limit" name="limit" value = "">
                            </div>


                        </div>

                        <div class = "row">
                            <div class = "col-xs-3">
                                <h2>Post Collection</h2>
                                <p>Number of times they should be
                                    Sent letters of collection, before
                                    Of sending the ticket to the exchequer
                                    Of collection.</p>
                                <input type = "number" class = "form-control" name = "emailpostcolle" id = "emailpostcolle">
                            </div>
                            <div class = "col-xs-3">
                                <h2>Data Extra</h2>
                                <div class = "thumb-Empty" id="imagup">
                                    <i class = "lnr lnr-picture" ></i>
                                </div>
                                <div id="image_preview">                                 
                                    <img class="img-responsive" id="previewing">
                                </div>
                                <input for = "form_customer"  type="file" name="file" id="file" style="display: none">
                                <label class="btn btn-default" for="file"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Upload</span></label>

                                <a class = "btn btn-danger" onclick="limpaFile();">  <i class="glyphicon glyphicon-remove"></i>Delete</a>


                            </div>
                            <div class = "col-xs-3">
                                <br>
                                <label for = "form_customer" class = "label-style">Fax</label>
                                <input type = "text" class = "form-control" placeholder = "Fax" id="fax" name="fax" value = ""><br>
                                <label for = "form_customer" class = "label-style">Date Birthday</label>
                                <input type = "text" class = "form-control" placeholder = "Date" id="birth" name="birth" value = ""><br>
                                <label for = "form_customer" class = "label-style">Client Authorization</label><br>
                                <input type = "checkbox" class = "" value="1" id = "sms" name = "sms"> <span>Send sms</span><br>
                                <input type = "checkbox" class = "" value="1" id="sendemailclient" name="sendemailclient"> <span>Send E-mail</span>

                            </div>
                            <div class = "col-xs-3">
                                <br>
                                <label for = "form_customer" class = "label-style">Electronic Identification</label>
                                <input type = "text" class = "form-control" placeholder="Electronic Identification" id="identification" name="identification" value=""><br>
                                <label for = "form_customer" class = "label-style">Genre</label>
                                <select class = "form-control" placeholder = "Genre" id = "genre" name = "genre">
                                    <option value = "">select</option>
                                    <option value = "1">Male</option>
                                    <option value = "2">Female</option>
                                    <option value = "3">Other</option>
                                </select><br>
                                <label for = "form_customer" class = "label-style">Language</label>
                                <select class="form-control" id="language" name="language">
                                    <?php foreach ($LanguageArray as $Languagevalue): ?>
                                        <option value = "<?php echo $Languagevalue->getIdlanguage(); ?>"><?php echo $Languagevalue->getName(); ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>
                        </div>

                        <div class = "row">
                            <div class = "col-xs-12">
                                <br>
                                <button class = " btn-sm btn-primary-sm">POS favorite</button>
                                <button class = " btn-sm btn-primary-sm">Invoices of the Customer</button>
                                <button class = " btn-sm btn-primary-sm">Statistic</button>
                                <button class = " btn-sm btn-primary-sm">Check Linst</button>
                            </div>
                        </div>




                    </div>
                    <div class = "modal-footer">
                        <button type = "reset" class = "btn btn-primary" data-dismiss = "modal">Cancel</button>
                        <button type = "submit" class = "btn btn-primary">Insert</button>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="myModalCompany" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="myModalCompanyclose" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">new Company</h4>
            </div>
            <div class="modal-body">
                <form id="form_company" name="form_company" class="form_company">
                    <div class="row">                        
                        <div class = "col-xs-6">
                            <label for = "form_company" class = "label-style">Name</label>
                            <input type="text" class = "form-control" placeholder="Name" id="nameCompanyModal" name="nameCompanyModal" value = "">
                        </div>
                        <div class = "col-xs-6">
                            <label for = "form_company" class = "label-style">Fantasy Name</label>
                            <input type="text" class = "form-control" placeholder="Name" id="FantasyNameModal" name="FantasyNameModal" value = "">
                        </div>
                    </div>
                    <div class="row">
                        <div class = "col-xs-6">
                            <label for = "form_customer" class = "label-style">Payment</label>
                            <select class = "form-control" id="paymentCompany" name="paymentCompany" >
                                <?php foreach ($paymentArray as $paymentValues): ?>
                                    <option value = "<?php echo $paymentValues->getIdpaymentMtd(); ?>"><?php echo $paymentValues->getName(); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for = "form_company" class = "label-style">Register Number</label>
                            <input type="text" class = "form-control" placeholder="Register Number" id="registernumber" name="registernumber" value = "">
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for = "form_customer" class = "label-style">Describe</label>
                            <textarea class = "form-control" wrap="off" cols="30" rows="5 id = "describeCompany" name = "describeCompany">

                        </textarea>
                    </div>    
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" onclick="showHint();" class="btn btn-success">Insert</button>
        </div>
    </div>
</div>
</div>


<div class="modal fade" id="myModalerror" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="fechaerromodal" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Error</h4>
            </div>

            <div class="modal-body">    
                <div class="alert alert-danger">
                    <strong>Danger!</strong> Check all fields.
                </div>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModalsuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="successmodalclose" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Error</h4>
            </div>

            <div class="alert alert-success">
                <strong>Success!</strong> Positive action.
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>




