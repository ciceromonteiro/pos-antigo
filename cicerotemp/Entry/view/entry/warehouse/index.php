<?php

/**
 * @author Paulo Egito <pvegito@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Oct 24, 2016
 */

//phpinfo();

?>

<?php
$warehouses = json_decode($view['warehouses']);
$halls = json_decode($view['halls']);
$wardrobes = json_decode($view['wardrobes']);
$hall_id = $view['hall_id'];
$wardrobe_id = $view['wardrobe_id'];
$shelf_id = $view['shelf_id'];
$section_id = $view['section_id'];


?>

  <style>
  .draggable { /*padding: 0.5em;*/ position:absolute; }
  #warehouse { width: 500px; height: 400px; padding: 0.5em; float: left; margin: 10px; background-color: #ddddff; }
  #open-wardrobe { display:none; z-index:999; position: absolute; width: 700px; height: 400px; left:250px; padding: 0.5em; float: left; margin: 10px; background-color: #ddddff; }
  #open-wardrobe-image {
    width: 200px; 
    height: 400px; 
    margin-left:200px; 
    padding: 0.5em; 
    float: left; 
    margin-top: 10px; 
    background-color: #aacc99; 
    border-top: 18px solid #915E49;
    border-left:10px solid #9D6954;
    border-right:10px solid #9D6954;
  }
  #tools {
    width: 220px;
    height: 380px;
    padding: 0.5em;
    float: left;
    background-color: white;
    border: 2px solid #ccc;
    border-style: dashed;
    margin-left: 30px;
    margin-top: 30px;
  }
  .hall { 
    width: 100px; 
    height: 100px; 
    background-color: #ddffcc; 
    z-index:10; 
    border: 2px solid #ccc; 
    background-image: url("../../../assets/images/bg-hall.png");
    background-repeat: repeat;
  }
  .hall:before {
    content: "Hall";
  }
  .hall-matrix { margin-top: 20px; margin-left: 60px; }
  .wardrobe { 
    width: 70px; 
    height: 90px; 
    background-image: url("../../../assets/images/bg-wardrobe.png");
    background-repeat: no-repeat;
    background-position: absolute;
    background-color: #764831;
    z-index:20; 
    border-top: 8px solid #915E49;
    border-left:2px solid #9D6954;
    border-right:2px solid #9D6954;
    border-spacing: 10px;
  }
  .wardrobe-matrix { margin-top:150px;  margin-left: 75px; }
  .shelf { width: 190px; position: absolute; height: 10px; background-color: #9D6954; z-index:1001; }
  .shelf-matrix { margin-top:260px; margin-left: 10px; }
  .section { width: 10px; position: absolute; height: 20px; background-color: #779966; z-index:1000; }
  .section-matrix { margin-top:290px; margin-left: 105px; }
  .ui-droppable{ padding: 0 !important; }

  #close-wardrobe{ float:right; }
  </style>
  <!--<script src="http://threedubmedia.com/inc/js/jquery.event.drag-2.2.js"></script>-->
  <script>

    var url_base = "<?php echo URL_BASE ?>";
    var next_wardrobe_id = <?= $view['wardrobe_id'] ?>;
    var next_hall_id = <?= $view['hall_id'] ?>;
    var next_shelf_id = <?= $view['shelf_id'] ?>;
    var next_section_id = <?= $view['section_id'] ?>;

    var opened_wardrobe = "";

    var selected_hall = "";
    var selected_wardrobe = "";
    var selected_shelf = "";
    var selected_section = "";

    var selected_element = ""; //a general select

    function select_hall(hall_id){
      selected_hall = hall_id;
      selected_element = hall_id;
    }
    function select_wardrobe(wardrobe_id){
      selected_wardrobe = wardrobe_id;
      selected_element = wardrobe_id;
    }
    function select_shelf(shelf_id){
      selected_shelf = shelf_id;
      selected_element = shelf_id;
    }
    function select_section(section_id){
      selected_section = section_id;
      selected_element = section_id;
    }
    function adjust_sections_height(){
      open_wardrobe_image_position = $("#open-wardrobe-image").offset();
      owip_top = parseInt(open_wardrobe_image_position.top);
      $(".section").each(function(){
        if($(this).hasClass("element_in_map")){
          this_id = $(this).attr("id");
          this_position = $(this).offset();
          this_top = parseInt(this_position.top);

          var margin_top = owip_top;
          var max_top = owip_top;
          $(".shelf").each(function(){
            if($(this).hasClass("element_in_map")){
              this_shelf_position = $(this).offset();
              this_shelf_top = parseInt(this_shelf_position.top);
              if(this_shelf_top>max_top&&this_top>this_shelf_top){
                max_top = this_shelf_top;
                margin_top = this_shelf_top;
              }
            }  
          });
          var height = this_top - max_top;
          margin_top = margin_top - owip_top;

          $(this).css("height",height);
          $(this).css("margin-top",margin_top);
        }
      });
    }

    function get_shelf_below(section_id){
      section_top = $("#"+section_id).offset();
      section_top = section_top.top;

      var shelf_id = 0;
      var margin_top = 999999999;
      $(".shelf").each(function(){
        if($(this).hasClass("element_in_map")){
          this_shelf_position = $(this).offset();
          this_shelf_top = parseInt(this_shelf_position.top);  

          if(this_shelf_top>section_top){
            if(this_shelf_top<margin_top){
              shelf_id = $(this).attr("id");           
              margin_top = this_shelf_top;                  
            }
          }
        }  
      });

      return shelf_id;
    }

    function createRollBackList(){

        var dados = [];
        dados.push(element);

        this_id = $("#warehouse_id").val();

        var element = { name: "warehouse_id", value: this_id };
        dados.push(element);

        var baseUrl = url_base+"Entry/warehouse/createRollBackList";

        $.ajax({

            url: baseUrl,
            method: "POST",
            data: dados,
            async: false,

        }).done(function (html) {
          //
        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });
    }

    function save_hall(element_id,warehouse_id,active){
      this_id = $("#"+element_id).attr("idhall");
      idhall = this_id;

      if(active){

        this_position = $("#"+element_id).offset();
        warehouse_position = $("#warehouse").offset();
        this_position_top = parseInt(this_position.top) - parseInt(warehouse_position.top);
        this_position_left = parseInt(this_position.left) - parseInt(warehouse_position.left);

        this_height = $("#"+element_id).height();
        this_width = $("#"+element_id).width();

      }else{

        this_position_top = 0;
        this_position_left = 0;

        this_height = 0;
        this_width = 0;

      }

      this_active = active;

      var dados = [];
      dados.push(element);

      var element = { name: "hall_id", value: this_id };
      dados.push(element);
      var element = { name: "warehouse_id", value: warehouse_id };
      dados.push(element);
      var element = { name: "position_top", value: this_position_top };
      dados.push(element);
      var element = { name: "position_left", value: this_position_left };
      dados.push(element);
      var element = { name: "height", value: this_height };
      dados.push(element);
      var element = { name: "width", value: this_width };
      dados.push(element);
      var element = { name: "active", value: this_active };
      dados.push(element);

      var baseUrl = url_base+"Entry/warehouse/saveHall";
      console.log("save_hall");
      $.ajax({

          url: baseUrl,
          method: "POST",
          data: dados,
          async: false,

      }).done(function (html) {
          $("#"+element_id).attr("idhall",html);
          idhall = html;
      }).fail(function (jqXHR, textStatus) {
          alert("Request failed: " + textStatus);
      });

      $("#"+element_id).attr("idhall",idhall);

      return idhall;
    }

    function save_wardrobe(element_id,hall_id,active){ //If hall_if is 0, it is a request made by a resize action. When it is not, it is a request made by a drag action.
      this_id = $("#"+element_id).attr("idwardrobe");
      idwardrobe = this_id;

      if(active){

        this_position = $("#"+element_id).offset();
        warehouse_position = $("#warehouse").offset();
        this_position_top = parseInt(this_position.top) - parseInt(warehouse_position.top);
        this_position_left = parseInt(this_position.left) - parseInt(warehouse_position.left);

        this_height = $("#"+element_id).height();
        this_width = $("#"+element_id).width();

      }else{

        this_position_top = 0;
        this_position_left = 0;

        this_height = 0;
        this_width = 0;

      }

      this_active = active;

      var dados = [];
      dados.push(element); 
      var element = { name: "wardrobe_id", value: this_id };
      dados.push(element);
      var element = { name: "hall_id", value: hall_id };
      dados.push(element);      
      var element = { name: "position_top", value: this_position_top };
      dados.push(element);
      var element = { name: "position_left", value: this_position_left };
      dados.push(element);
      var element = { name: "height", value: this_height };
      dados.push(element);
      var element = { name: "width", value: this_width };
      dados.push(element);
      var element = { name: "active", value: this_active };
      dados.push(element);

console.log("save_wardrobe");
      var baseUrl = url_base+"Entry/warehouse/saveWardrobe";

      $.ajax({

          url: baseUrl,
          method: "POST",
          data: dados,
          async: false,

      }).done(function (html) {     
          html = JSON.parse(html);
          $("#"+element_id).attr("idwardrobe",html[0]);
          idwardrobe = html[0];     
      }).fail(function (jqXHR, textStatus) {

          alert("Request failed: " + textStatus);

      });

      $("#"+element_id).attr("idwardrobe",idwardrobe);

      return idwardrobe;
    }

    function save_shelf(element_id,wardrobe_id,active){
      this_id = $("#"+element_id).attr("idshelf");    
      idshelf = this_id;

      if(active){

        this_position = $("#"+element_id).offset();
        open_wardrobe_image_position = $("#open-wardrobe-image").offset();
        this_position_top = parseInt(this_position.top) - parseInt(open_wardrobe_image_position.top);

      }else{
        this_position_top = 0;
      }

      this_active = active;

      var dados = [];
      dados.push(element);

      var element = { name: "shelf_id", value: this_id };
      dados.push(element);
      var element = { name: "wardrobe_id", value: wardrobe_id };
      dados.push(element);
      var element = { name: "position_top", value: this_position_top };
      dados.push(element);
      var element = { name: "active", value: this_active };
      dados.push(element);
//alert(this_id+" x "+wardrobe_id+" x "+this_position_top+" x "+this_active);
      var baseUrl = url_base+"Entry/warehouse/saveShelf";
console.log("save_shelf");
      $.ajax({

          url: baseUrl,
          method: "POST",
          data: dados,
          async: false,

      }).done(function (html) {
          html = JSON.parse(html);   
          $("#"+element_id).attr("idshelf",html[0]);
          idshelf = html[0];
          
      }).fail(function (jqXHR, textStatus) {

          alert("Request failed: " + textStatus);

      });

      $("#"+element_id).attr("idshelf",idshelf);

      return idshelf;
    }

    function save_section(element_id,shelf_id,active){
      this_id = $("#"+element_id).attr("idsection");        
      idsection = this_id;

      if(active){
        this_position = $("#"+element_id).offset();
        open_wardrobe_image_position = $("#open-wardrobe-image").offset();
        this_position_left = parseInt(this_position.left) - parseInt(open_wardrobe_image_position.left);

      }else{
        this_position_left = 0;
      }

      this_active = active;

      var dados = [];
      dados.push(element);

      var element = { name: "section_id", value: this_id };
      dados.push(element);
      var element = { name: "shelf_id", value: shelf_id };
      dados.push(element);
      var element = { name: "position_left", value: this_position_left };
      dados.push(element);
      var element = { name: "active", value: this_active };
      dados.push(element);

      var baseUrl = url_base+"Entry/warehouse/saveSection";
console.log("save_section");
      $.ajax({

          url: baseUrl,
          method: "POST",
          data: dados,
          async: false,

      }).done(function (html) {
          $("#"+element_id).attr("idsection",html);
          idsection = html;
      }).fail(function (jqXHR, textStatus) {

          alert("Request failed: " + textStatus);

      });

      $("#"+element_id).attr("idsection",idsection);

      return idsection;
    }



    function open_wardrobe(open_wardrobe_id){
      $("#open-wardrobe").show();
      opened_wardrobe = open_wardrobe_id;
      open_wardrobe_id = $("#"+open_wardrobe_id).attr("idwardrobe");

      if(1){ //open_wardrobe_id[1]=="saved"

        var dados = [];

        dados.push(element);

        var element = {
            name: "wardrobe_id",
            value: open_wardrobe_id
        };
        dados.push(element);

        var baseUrl = "<?php echo URL_BASE. 'Entry/Warehouse/loadWardrobe' ?>";

        $.ajax({

            url: baseUrl,
            method: "POST",
            data: dados,
            async: false,

        }).done(function (html) {

            var data = html;
            var elements = JSON.parse(data);
            // alert(elements[0].shelf);
            // $("#open-wardrobe-image").append(elements);
            //$("#open-wardrobe-image").append(data);

            if (data != "") {       
                var elements = JSON.parse(data);

                var i, j;

                for (i in elements) {                 
                    var elements2 = elements[i];

                    if(elements2.shelf!=undefined){
                      var shelf_ = elements2.shelf.split("_");
                        shelf_id = shelf_[0];
                        shelf_margin_top = shelf_[1];
                        shelf_fixed = shelf_[2];

                        is_draggable = "draggable";
                        if(shelf_fixed==1){
                          is_draggable = "";
                        }

                        $("#open-wardrobe-image").append("<div id='shelf_saved_"+shelf_id+"' idshelf='"+shelf_id+"' class='shelf shelf_saved "+is_draggable+" element_in_map element_for_wardrobe' onmouseover='select_shelf(\"shelf_saved_"+shelf_id+"\")' style='margin-top:"+shelf_margin_top+"px;'></div>");
                    }

                    for (j in elements2) {
                      var elements3 = elements2[j];

                      if(elements3.section!=undefined){
                        var section_ = elements3.section.split("_");
                        section_id = section_[0];
                        section_margin_left = section_[1];
                        section_margin_top = parseInt(shelf_margin_top) - 10;
                        section_fixed = section_[2];

                        is_draggable = "draggable";
                        if(section_fixed==1){
                          is_draggable = "";
                        }

                        $("#open-wardrobe-image").append("<div id='section_saved_"+section_id+"' idsection='"+section_id+"' class='section section_saved "+is_draggable+" element_in_map element_for_wardrobe' onmouseover='select_section(\"section_saved_"+section_id+"\")' style='margin-left:"+section_margin_left+"px; margin-top:"+section_margin_top+"px;'></div>");
                      }
                      //alert("teste");
                    }
                }

            }



        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);
            console.log(jqXHR.responseText);

        });
      }  
      start();

      adjust_sections_height();
    }

    

    function close_wardrobe(){
      $("#open-wardrobe").hide();

      $(".shelf").each(function(){
          if($(this).hasClass("element_in_map")){
              $(this).remove();
          }
      });
      $(".section").each(function(){
          if($(this).hasClass("element_in_map")){
              $(this).remove();
          }
      });
    }

    function start(){
        $( ".draggable" ).draggable({
            revert: "invalid"
        });
        //$(".draggable").multiDraggable({ group: $(".wardrobe")});

        $( ".resizable" ).resizable({
            handles: "all"
        });

        $(".hall").mouseup(function() {
          warehouse_id = $("#warehouse_id").val();
          save_hall(selected_hall,warehouse_id,1);
        });
        $(".wardrobe").mouseup(function() {
          hall_id = $("#"+selected_hall).attr("idhall");
          save_wardrobe(selected_wardrobe,hall_id,1);
        });

        $( "#warehouse" ).droppable({
            accept: '.hall',
            drop: function(event, ui) {
              warehouse_id = $("#warehouse_id").val();

              if($("#"+selected_hall).hasClass("hall_new")||$("#"+selected_hall).hasClass("hall_saved")){

              }else{
                $("#"+selected_hall).addClass("hall_new");
                $("#"+selected_hall).addClass("element_in_map");

                next_hall_id = parseInt(next_hall_id) + 1;
                $("#tools").append("<div id='hall_new_"+next_hall_id+"' idhall='' class='hall hall-matrix resizable draggable' onmouseover='select_hall(\"hall_new_"+next_hall_id+"\")' ></div>");

                start();
              }
            }
        });

        $( ".hall" ).droppable({
            accept: '.wardrobe',
            drop: function(event, ui) {  
              hall_id = $(this).attr("idhall");  

              if($("#"+selected_wardrobe).hasClass("wardrobe_new")||$("#"+selected_wardrobe).hasClass("wardrobe_saved")){
                //
              }else{
                $("#"+selected_wardrobe).addClass("wardrobe_new");
                $("#"+selected_wardrobe).addClass("element_in_map");

                next_wardrobe_id = parseInt(next_wardrobe_id) + 1;
                $("#tools").append("<div id='wardrobe_new_"+next_wardrobe_id+"' idwardrobe='' class='wardrobe wardrobe-matrix resizable draggable' onmouseover='select_wardrobe(\"wardrobe_new_"+next_wardrobe_id+"\")'  ondblclick=\"open_wardrobe('wardrobe_new_"+next_wardrobe_id+"')\"></div>");
                start();
              }
            }
        });

        $( "#open-wardrobe-image" ).droppable({
            accept: '.element_for_wardrobe',
            drop: function(event, ui) {
              element = $("#"+selected_element).attr("id");
              wardrobe_id = $("#"+opened_wardrobe).attr("idwardrobe");
              save_shelf(element,wardrobe_id,1);

              if($("#"+element).hasClass("shelf")){
                if($("#"+element).hasClass("shelf_new")||$("#"+element).hasClass("shelf_saved")){

                }else{
                  $("#"+element).addClass("shelf_new");
                  $("#"+element).addClass("element_in_map");
                  //$("#"+element).addClass("element_for_wardrobe");

                  next_shelf_id = parseInt(next_shelf_id) + 1;
                  $("#tools").append("<div id='shelf_new_"+next_shelf_id+"' idshelf='' class='shelf shelf-matrix draggable element_for_wardrobe' onmouseover='select_shelf(\"shelf_new_"+next_shelf_id+"\")' ></div>");

                  start();
                }
              }else if($("#"+element).hasClass("section")){
                shelf_below = get_shelf_below(element);
                shelf_id = $("#"+shelf_below).attr("idshelf");      
                save_section(element,shelf_id,1);

                if($("#"+element).hasClass("section_new")||$("#"+element).hasClass("section_saved")){

                }else{
                  $("#"+element).addClass("section_new");
                  $("#"+element).addClass("element_in_map");
                  //$("#"+element).addClass("element_for_wardrobe");

                  next_section_id = parseInt(next_section_id) + 1;
                  $("#tools").append("<div id='section_new_"+next_section_id+"' idsection='' class='section section-matrix draggable element_for_wardrobe' onmouseover='select_section(\"section_new_"+next_section_id+"\")' ></div>");

                  start();
                }
              }
            }
        });

        $( "#tools" ).droppable({
            accept: '.element_in_map',
            drop: function(event, ui) {
              this_id = selected_element;
              this_id = this_id.split("_");
              type = this_id[0];
              new_id = this_id[2];
              
              if(type=="hall"){
                save_hall(selected_element,0,0);
              }else if(type=="wardrobe"){
                save_wardrobe(selected_element,0,0);
              }else if(type=="shelf"){
                save_shelf(selected_element,0,0);
              }else if(type=="section"){
                save_section(selected_element,0,0);
              }

              $("#"+selected_element).remove();
            }
        });
    }


    $( function() {

      start();     

      $("#save_warehouse").click(function(){
        var dados = [];
        dados.push(element);

        this_id = $("#warehouse_id").val();

        var element = { name: "warehouse_id", value: this_id };
        dados.push(element);

        var baseUrl = url_base+"Entry/warehouse/persistWarehouseElements";

        $.ajax({

            url: baseUrl,
            method: "POST",
            data: dados,
            async: false,

        }).done(function (html) {
          //
        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });

      });
    });
  </script>

<?php
  $nav = "physical";
  include "nav.php";
?>

<div class="content">
   <div class="row row-fluid line-form">
        <div class="col-md-4">
           <form id="warehouse_map" action="" method="post">
              <label for="warehouse_id" class="translate">Select warehouse</label>
              <ul class="list-inline">
                <li>
                  <select name="warehouse" id="warehouse_id" class="form-control form-control-select">
                    <?php if($warehouses): ?>
                      <?php foreach($warehouses as $warehouse): ?>
                        <option value="<?= $warehouse->id ?>"><?= $warehouse->name ?></option>
                      <?php endforeach ?>
                    <?php endif ?>
                  </select>
                </li>
                <li>
                  <input type="button" onclick="createRollBackList()" value="Criar Rollback" class="btn btn-primary">
                </li>
                <li>
                  <input type="button" id="save_warehouse" value="Save Warehouse" class="btn btn-primary">
                </li>
              </ul>
            </form>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12">

        <div id="tools" class="ui-widget-header droppable">
          <div id="hall_new_<?= $hall_id ?>" onmouseover="select_hall('hall_new_<?= $hall_id ?>')" class="hall hall-matrix resizable draggable">

          </div>

          <div id="wardrobe_new_<?= $wardrobe_id ?>"  onmouseover="select_wardrobe('wardrobe_new_<?= $wardrobe_id ?>')" class="wardrobe wardrobe-matrix resizable draggable" ondblclick="open_wardrobe('wardrobe_new_<?= $wardrobe_id ?>')">

          </div>

          <div id="shelf_new_<?= $shelf_id ?>" onmouseover="select_shelf('shelf_new_<?= $shelf_id ?>')" class="shelf shelf-matrix draggable element_for_wardrobe">

          </div>

          <div id="section_new_<?= $section_id ?>" onmouseover="select_section('section_new_<?= $shelf_id ?>')" class="section section-matrix draggable element_for_wardrobe">

          </div>
        </div>

        <input type="hidden" id="wardrobe_selected" value="">

        <div id="open-wardrobe" class="ui-widget-header droppable">
          <input type="button" onclick="close_wardrobe()" value="X">
          <div id="open-wardrobe-image"></div>
        </div>

        <div id="warehouse" class="ui-widget-header droppable">
          <?php if($halls): ?>
            <?php foreach($halls as $hall): ?>
              <div id="hall_saved_<?= $hall->id ?>" idhall="<?= $hall->id ?>" class="hall hall_saved resizable draggable element_in_map" onmouseover="select_hall('hall_saved_<?= $hall->id ?>')" style="margin-left:<?= $hall->position_left ?>px; margin-top:<?= $hall->position_top ?>px; height:<?= $hall->height ?>px; width:<?= $hall->width ?>px;">
              </div>
            <?php endforeach ?>
          <?php endif ?>
          <?php if($wardrobes): ?>
            <?php foreach($wardrobes as $wardrobe): ?>
              <div id="wardrobe_saved_<?= $wardrobe->id ?>" idwardrobe="<?= $wardrobe->id ?>" class="wardrobe wardrobe_saved resizable draggable element_in_map" inhall="<?= $wardrobe->hall_id ?>" onmouseover="select_wardrobe('wardrobe_saved_<?= $wardrobe->id ?>')" style="margin-left:<?= $wardrobe->position_left ?>px; margin-top:<?= $wardrobe->position_top ?>px; height:<?= $wardrobe->height ?>px; width:<?= $wardrobe->width ?>px;" ondblclick="open_wardrobe('wardrobe_saved_<?= $wardrobe->id ?>')"></div>
            <?php endforeach ?>
          <?php endif ?>
        </div>

      </div>
    </div>
</div>