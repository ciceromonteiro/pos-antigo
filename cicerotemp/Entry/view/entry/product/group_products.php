<?php
    $select = "<option value='0'>None</option>";
    $select_groups = "";
    $select_printers = "<option value='0'>Do not print</option>";
    $select_department = "";
    $select_rules = "";
    
    foreach ($array_answer['groups'] as $value) {
        $select_groups .= "<option value='".$value->getIdproductGp()."'>".$value->getName()."</option>";
    }
    
    foreach ($array_answer['printers'] as $value) {
        $select_printers .= "<option value='".$value->getIdprinter()."'>".$value->getDescription()."</option>";
    }
    
    foreach ($array_answer['departments'] as $value) {
        $select_department .= "<option value='".$value->getIddepartment()."'>".$value->getName()."</option>";
    }
    
    foreach ($array_answer['rules'] as $value) {
        $select_rules .= "<option value='".$value->getIdorderrule()."'>".$value->getName()."</option>";
    }
    
    $nav = "group_products";
    include 'nav.php';
?>

<script type="text/javascript">
    $(document).ready(function() {
        
        var tableProductGroup = $('#productGroup').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllProductGp/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" },
                { "data": "coreGroup" },
                { "data": "alternatives" },
                { "data": "printer" },
                { "data": "department" },
                { "data": "rules" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });


        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_productGroup');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_productGroup_edit');
            $.ajax({
                url : my_url+"Entry/Product/getProductGp/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + tableProductGroup.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#productGroupEditModal').modal({show : true});
                        $('#form_productGroup_edit #idproductGp').val(data.data[0].id);
                        $('#form_productGroup_edit #description').val(data.data[0].description);
                        
                        $("#form_productGroup_edit #coreGroup").val(data.data[0].coreGroup).prop('selected', true);
                        $("#form_productGroup_edit #printer").val(data.data[0].printer).prop('selected', true);
                        $("#form_productGroup_edit #department").val(data.data[0].department).prop('selected', true);
                        $("#form_productGroup_edit #rules").val(data.data[0].rules).prop('selected', true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_productGroup', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertProductGroup",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_productGroup').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(tableProductGroup);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_productGroup_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateProductGp",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_productGroup_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#productGroupEditModal').modal('hide');
                    reload_table(tableProductGroup);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = tableProductGroup.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idProductGroups : ids};
            deleteItens('Entry/Product/deleteProductGp', array_deletes, tableProductGroup);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#productGroup tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#productGroup');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
        
    
    
    

    $(document).on('click', '#create_alternatives', function(e){
        e.preventDefault();
        resetForm('#form_productGroupAlternatives');
        $('#productGroup_alternatives_createModal').modal({
            show : true
        });
    });


     var tableProductGroupAlternatives = $('#productGroup_alternatives').DataTable( {
        "ajax": {"url": my_url+"Entry/Product/getAllProductGpAlternatives/"},
        "columns": [
            { "data": "checkbox" },
            { "data": "id" },
            { "data": "description" },
            { "data": "priceChange" }
        ],
        retrieve: true,
        paging: false,
        "language": {
            "url": my_url+my_language+".json"
        }
    });

    $(document).on('submit', '#form_productGroupAlternatives', function(e){
        var ifProductGpForAlternatives = $('#idproductGpForAlternatives').val();
        e.preventDefault();
        $.ajax({
            url : my_url+"Entry/Product/insertProductGpAlternatives",
            type: "POST",
            dataType: 'JSON',
            data : 'idproductGp='+ifProductGpForAlternatives+'&'+$('#form_productGroupAlternatives').serialize(),
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
                $('#productGroup_alternatives_createModal').modal('hide');
                reload_table(tableProductGroupAlternatives);
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    });
        
    $('#update_alternatives').attr('disabled', true);
    $('#delete_alternatives').attr('disabled', true);
    $("#productGroup_alternatives tbody").on( 'click', 'tr', function () {
        var checkboxInput = $(this).find('input:checkbox');
        if ($(this).hasClass('hover-select')){
            $(this).removeClass('hover-select');
            checkboxInput[0].checked = false;
        } else {
            $(this).addClass('hover-select');
            checkboxInput[0].checked = true;
        }

        var itemSelected = checkMark('#productGroup_alternatives');
        if(itemSelected == 0){
            $('#update_alternatives').attr('disabled', true);
            $('#delete_alternatives').attr('disabled', true);
        }
        if(itemSelected == 1){
            $('#update_alternatives').attr('disabled', false);
            $('#delete_alternatives').attr('disabled', false);
        }
        if(itemSelected > 1){
            $('#update_alternatives').attr('disabled', true);
            $('#delete_alternatives').attr('disabled', false);
        }
    });

    $(document).on('click', '#update_alternatives', function(e){
        e.preventDefault();
        
        id= tableProductGroupAlternatives.$('tr.hover-select').find('input:checkbox').data('id');
        resetForm('#form_productGroup_edit');
            $.ajax({
                url : my_url+"Entry/Product/getProductGpAlternatives/"+ id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#productGroup_alternatives_editModal').modal({show : true});
                        $('#form_productGroupAlternatives_edit #idproductGpForAlternatives').val(data.data[0].id);
                        $('#form_productGroupAlternatives_edit #description').val(data.data[0].description);
                        
                        $("#form_productGroupAlternatives_edit #changePrice").val(data.data[0].priceChange);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
        });
    });

    $(document).on('submit', '#form_productGroupAlternatives_edit', function(e){
        var id = $('#form_productGroupAlternatives_edit #idproductGpForAlternatives').val();
        alert(id);
        e.preventDefault();
        $.ajax({
            url : my_url+"Entry/Product/updateProductGpAlternatives/"+id,
            type: "POST",
            dataType: 'JSON',
            data : $('#form_productGroupAlternatives_edit').serialize(),
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
                $('#productGroup_alternatives_createModal').modal('hide');
                reload_table(tableProductGroupAlternatives);
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    });


    $(document).on('click', '#delete_alternatives', function(e){
        e.preventDefault();
        var trs = tableProductGroupAlternatives.$('tr.hover-select').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
        });        
        var i=0, ids = [];
        for(i=0; i<trs.length; i++){
            ids[i] = $(trs[i]).find('input:checkbox').data('id');
        }                    
        var array_deletes = {idProductGroupAlternative : ids};
        deleteItens('Entry/Product/deleteProductGpAlternative', array_deletes, tableProductGroupAlternatives);
    });

});

function openModalAlternatives(value){
        $('#idproductGpForAlternatives').val(value);
        $('#productGroup_alternativesModal').modal({show : true});
        /*var tableProductGroupAlternatives = $('#productGroup_alternatives').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllProductGpAlternatives/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" },
                { "data": "priceChange" }
            ],
            retrieve: true,
            paging: false,
            "language": {
                "url": my_url+my_language+".json"
            }
        });*/
    }
</script>

<!-- Table List ProductGroups -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary"><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary"><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="productGroup" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('productGroup')"></th>
                    <th style="width: 10px" class="translate">ID</th>
                    <th class="translate">Description</th>
                    <th class="translate">Core Group</th>
                    <th class="translate">Alternatives</th>
                    <th class="translate">Printer</th>
                    <th class="translate">Department</th>
                    <th class="translate">Rules</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Group</h4>
            </div>
            <form id="form_productGroup">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Description</label>
                            <input type="text" class="form-control" id="description" name="description" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="coreGroup" class="label-style translate">Core Group</label>
                            <select name="coreGroup" id="coreGroup">
                                <?php echo $select.$select_groups ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="printer" class="label-style translate">Printer</label>
                            <select name="printer" id="printer">
                                <?php echo $select_printers ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="department" class="label-style translate">Department</label>
                            <select name="department" id="department">
                                <?php echo $select.$select_department ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="rules" class="label-style translate">Rules</label>
                            <select name="rules" id="rules">
                                <?php echo $select.$select_rules ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Update -->
<div class="modal fade" id="productGroupEditModal" tabindex="-1" role="dialog" aria-labelledby="labelProductGroupUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelProductGroupUpdate">Update Group</h4>
            </div>
            <form id="form_productGroup_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="number" style="display: none" id="idproductGp" name="idproductGp" required="true">
                            <label for="description" class="label-style">Description</label>
                            <input type="text" class="form-control" id="description" name="description" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="coreGroup" class="label-style translate">Core Group</label>
                            <select name="coreGroup" id="coreGroup">
                                <?php echo $select.$select_groups ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="printer" class="label-style translate">Printer</label>
                            <select name="printer" id="printer">
                                <?php echo $select_printers ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="department" class="label-style translate">Department</label>
                            <select name="department" id="department">
                                <?php echo $select.$select_department ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="rules" class="label-style translate">Rules</label>
                            <select name="rules" id="rules">
                                <?php echo $select.$select_rules ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- productGroup_alternatives -->
<div class="modal fade" id="productGroup_alternativesModal" tabindex="-1" role="dialog" aria-labelledby="labelProductGroupUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelProductGroupUpdate">Alternatives</h4>
            </div>
            <form id="form_productGroup_edit">
                <div class="modal-body">
                    <div class="top-bar-buttons">
                        <div class="button-group">
                            <button id="create_alternatives" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                            <button id="update_alternatives" class="btn btn-primary"><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                            <button id="delete_alternatives" class="btn btn-primary"><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="number" style="display: none" value="" id="idproductGpForAlternatives">
                            <table id="productGroup_alternatives" class="table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('productGroup_alternatives')"></th>
                                    <th style="width: 10px" class="translate">ID</th>
                                    <th class="translate">Description</th>
                                    <th class="translate">Change Price</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="productGroup_alternatives_createModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Alternatives</h4>
            </div>
            <form id="form_productGroupAlternatives">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Description</label>
                            <input type="text" class="form-control" id="description" name="description" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="changePrice" class="label-style translate">Change Price</label>
                            <input type="text" class="form-control money" id="changePrice" name="changePrice">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="productGroup_alternatives_editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Alternatives</h4>
            </div>
            <form id="form_productGroupAlternatives_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="number" style="display: none" value="" id="idproductGpForAlternatives">
                            <label for="description" class="label-style translate">Description</label>
                            <input type="text" class="form-control" id="description" name="description" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="changePrice" class="label-style translate">Change Price</label>
                            <input type="text" class="form-control money" id="changePrice" name="changePrice">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>