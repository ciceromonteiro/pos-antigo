<script type="text/javascript">
    $(document).ready(function() {

        var table = $('#manufacturers').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllManufacturers/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "negociated_discount"}
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#manufacturers_create', function(e){
            e.preventDefault();
            resetForm('#form_manufacturer_insert');
            $('#modal_manufacturer_insert').modal({show : true});
        });

        $(document).on('submit', '#form_manufacturer_insert', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertManufacturer",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_manufacturer_insert').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_manufacturer_insert').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#manufacturers_update', function(e){
            e.preventDefault();
            resetForm('#form_manufacturer_edit');
            $.ajax({
                url : my_url+"Entry/Product/getmanufacturers/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#modal_manufacturer_edit').modal({show : true});
                        $('#form_manufacturer_edit #idmanufacturer').val(data.data[0].id);
                        $('#form_manufacturer_edit #name').val(data.data[0].name);
                        $('#form_manufacturer_edit #discount').val(data.data[0].discount+'%');
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_manufacturer_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateManufacturer",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_manufacturer_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_manufacturer_edit').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#manufacturers_delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idManufactures : ids};
            deleteItens('Entry/Product/deleteManufacturer', array_deletes, table);
        });
        
        $('#manufacturers_update').attr('disabled', true);
        $('#manufacturers_delete').attr('disabled', true);
        $("#manufacturers tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#manufacturers');
            if(itemSelected == 0){
                $('#manufacturers_update').attr('disabled', true);
                $('#manufacturers_delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#manufacturers_update').attr('disabled', false);
                $('#manufacturers_delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#manufacturers_update').attr('disabled', true);
                $('#manufacturers_delete').attr('disabled', false);
            }
        });
    });
</script>

<?php
    $nav = "manufacturers";
    include 'nav.php';
?>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <button id="manufacturers_create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                    <button id="manufacturers_update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                    <button id="manufacturers_delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
                </div>
            </div>

            <table id="manufacturers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Negociated discount</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_manufacturer_insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Manufacturer</h4>
            </div>
            <form id="form_manufacturer_insert">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="name" class="translate">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                            
                            <label for="discount" class="translate">Negociated Discount</label>
                            <input type="text" class="form-control percent" id="discount" name="discount">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="modal_manufacturer_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Manufacturer</h4>
            </div>
            <form id="form_manufacturer_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="idmanufacturer" name="idmanufacturer" style="display: none">
                            
                            <label for="name" class="translate">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                            
                            <label for="discount" class="translate">Negociated Discount</label>
                            <input type="text" class="form-control percent" id="discount" name="discount">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>