<?php
    $select_products = "<option value='0'>Select Option</option>";
    foreach ($array_answer['products'] as $value) {
        $select_products .= "<option value='".$value->getIdproduct()."'>".$value->getName()."</option>";
    }
    $nav = "series";
    include 'nav.php';
?>

<script>
    $(document).ready(function () {
        
        var table = $('#serial_table').DataTable( {
            "ajax": {"url": my_url+"Entry/ProductSerial/getAllSerialNumbers/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "product"},
                {"data": "serial"}
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#serial_create', function(e){
            e.preventDefault();
            resetForm('#form_serial_insert');
            $('#modal_serial_insert').modal({
                show : true
            });
        });

        $(document).on('submit', '#form_serial_insert', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/ProductSerial/insertSerialNumber",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_serial_insert').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_serial_insert').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#serial_update', function(e){
            e.preventDefault();
            resetForm('#form_serial_edit');
            $.ajax({
                url : my_url+"Entry/ProductSerial/getSerialNumber/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#modal_serial_edit').modal({show : true});
                        $('#form_serial_edit #productSerialNumber').val(data.data[0].id);
                        $('#form_serial_edit #product').val(data.data[0].product);
                        $('#form_serial_edit #serialNumber').val(data.data[0].serial);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_serial_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/ProductSerial/updateSerialNumber",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_serial_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_serial_edit').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#serial_delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idSerialNumbers : ids};
            deleteItens('Entry/ProductSerial/deleteSerialNumber', array_deletes, table);
        });
        
        $('#serial_update').attr('disabled', true);
        $('#serial_delete').attr('disabled', true);
        $("#serial_table tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#serial_table');
            if(itemSelected == 0){
                $('#serial_update').attr('disabled', true);
                $('#serial_delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#serial_update').attr('disabled', false);
                $('#serial_delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#serial_update').attr('disabled', true);
                $('#serial_delete').attr('disabled', false);
            }
        });
    });

</script>

<div class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="top-bar-buttons">
                <div class="button-group">
                    <button id="serial_create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                    <button id="serial_update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                    <button id="serial_delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
                </div>
            </div>

            <table id="serial_table" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" onclick="markAll('serial_table')"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Product</th>
                        <th class="translate">Serial</th>
                    </tr>
                </thead>
            </table>                        
        </div>
    </div>    
</div>

<div class="modal fade" id="modal_serial_insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Series</h4>
            </div>
            <form id="form_serial_insert">
                <div class="modal-body">                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="product" class="translate">Product</label>
                                <select id="product" name="product" required="true">
                                    <?php echo $select_products ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="serialNumber" class="label-style translate">Serial</label>
                                <input type="text" id="serialNumber" name="serialNumber" class="form-control" required="true">                                
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_serial_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Series</h4>
            </div>
            <form id="form_serial_edit">
                <div class="modal-body">                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="product" class="translate">Product</label>
                                <select id="product" name="product" required="true">
                                    <?php echo $select_products ?>
                                </select>
                                <input type="number" id="productSerialNumber" name="productSerialNumber" style="display: none" required="true">    
                            </div>
                            <div class="form-group">
                                <label for="serialNumber" class="label-style translate">Serial</label>
                                <input type="text" id="serialNumber" name="serialNumber" class="form-control" required="true">                                
                            </div>
                        </div>
                    </div>                    
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>