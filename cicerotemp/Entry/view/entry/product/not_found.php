<?php    
    $nav = "products";
    include 'nav.php';
?>
<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12 text-center" style="background-color: white; padding: 50px; margin-top: 50px">
            <h1>We did not find the product</h1>
            <p>Error 404</p>
            <button class="btn btn-primary" onclick="window.history.back()">Go back</button>
        </div>
    </div>
</div>