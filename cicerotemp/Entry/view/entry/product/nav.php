<ul class="navbar-three">
    <li <?php echo ($nav == "list_products") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/index" ?>" class="translate">All Products</a>
    </li>
    <li <?php echo ($nav == "products") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/product" ?>" class="translate">Product</a>
    </li>
    <li <?php echo ($nav == "group_products") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/group_products" ?>" class="translate">Groups</a>
    </li>
    <li <?php echo ($nav == "unit") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/unit" ?>" class="translate">Unit</a>
    </li>
    <li <?php echo ($nav == "stock") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/stock" ?>" class="translate">Stock</a>
    </li>
    <li <?php echo ($nav == "rules") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/rules" ?>" class="translate">Rules</a>
    </li>
    <li <?php echo ($nav == "manufacturers") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/manufacturers" ?>" class="translate">Manufacturers</a>
    </li>
    <li <?php echo ($nav == "importation_settings") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/importation_settings" ?>" class="translate">Importation Settings</a>
    </li>
    <li <?php echo ($nav == "series") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/series" ?>" class="translate">Series</a>
    </li>
</ul>