<?php
    $section = "";
    foreach ($array_answer['section'] as $value) {
        $section .= "<option value='{$value->getIdsection()}'>{$value->getName()}</option>";
    }

$nav = "stock";
include 'nav.php';
?>

<script type="text/javascript">
    var table = "";
    var tableProductSection = "";
    $(document).ready(function () {
        table = $('#stock').DataTable({
            "ajax": {"url": my_url + "Entry/Product/getAllStock/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "name"},
                {"data": "purchasePrice"},
                {"data": "salePrice"},
                {"data": "mediumPrice"},
                {"data": "group"},
                {"data": "qtdOnStock"},
                {"data": "totalOnStock"},
                {"data": "stock"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });

        tableProductSection = $('#productSectionTable').DataTable({
            "columns": [
                {"data": ""},
                {"data": ""},
                {'data': ""},
                {"data": ""},
                {"data": ""},
                {"data": ""}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });


        $("#stock tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;



            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
            var itemSelected = checkMark('#stock');
            if(itemSelected == 0){
                $('#adjustQtd').addClass('disabled');

                
            }
            if(itemSelected == 1){
                $('#adjustQtd').removeClass('disabled');
               
            }
            if(itemSelected > 1){
                $('#adjustQtd').addClass('disabled');


            }
            
           
        });

        $("#productSectionTable tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
                
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }

            var itemSelected = checkMark('#productSectionTable');
            if(itemSelected == 0){
                $('#updateProductInSection').attr('disabled', true);
                $('#deleteProductInSection').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#updateProductInSection').attr('disabled', false);
                $('#deleteProductInSection').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#updateProductInSection').attr('disabled', true);
                $('#deleteProductInSection').attr('disabled', false);

            }
           
        });     


        $(document).on('click', '#createProductInSection', function(e){
            e.preventDefault();
            resetForm('#formCreateProductInSection');
            $('#modalCreateProductInSection').modal({
                show : true
            });
            $('#modalCreateProductInSection').show();
        });

        $(document).on('submit', '#formCreateProductInSection', function(e){
            var idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
            idProduct = b64EncodeUnicode(idProduct);

            var section = $('#formCreateProductInSection #sectionValue').val();
            section = b64EncodeUnicode(section);

            var quantity = $('#formCreateProductInSection #quantity').val();
            if(quantity == ""){
                quantity = "0";
            }
            quantity = b64EncodeUnicode(quantity);

            var dateValue = $('#formCreateProductInSection #dateValue').val();
            if(dateValue == ""){
                dateValue = "0";
            }
            dateValue = b64EncodeUnicode(dateValue);

            var dateExpirationValue = $('#formCreateProductInSection #dateExpirationValue').val();
            if(dateExpirationValue == ""){
                dateExpirationValue = "0";
            }
            dateExpirationValue = b64EncodeUnicode(dateExpirationValue);

            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertProductInSection/"+  idProduct +"/"+ section +"/" + quantity +"/"+ dateValue +"/"+ dateExpirationValue,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    adjustQtdStock();
                    $('#modalCreateProductInSection').hide();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#updateProductInSection', function(e){
            e.preventDefault();
            var id = tableProductSection.$('tr.hover-select').find('input:checkbox').data('id');
            $.ajax({
                url : my_url+"Entry/Product/getProductInSection/"+id,
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#formUpdateProductInSection #idProductInSection').val(data.data[0].id);
                        $('#formUpdateProductInSection #sectionValue').val(data.data[0].section);
                        $('#formUpdateProductInSection #quantity').val(data.data[0].qty);
                        $('#formUpdateProductInSection #dateValue').val(data.data[0].date);
                        $('#formUpdateProductInSection #dateExpirationValue').val(data.data[0].dateExpiration);
                        $('#modalUpdateProductInSection').modal({
                            show : true
                        });
                        $('#modalUpdateProductInSection').show();
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }); 

        $(document).on('submit', '#formUpdateProductInSection', function(e){
            var id = $('#formUpdateProductInSection #idProductInSection').val();
            id = b64EncodeUnicode(id);

            var idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
            idProduct = b64EncodeUnicode(idProduct);

            var section = $('#formUpdateProductInSection #sectionValue').val();
            section = b64EncodeUnicode(section);

            var quantity = $('#formUpdateProductInSection #quantity').val();
            if(quantity == ""){
                quantity = "0";
            }
            quantity = b64EncodeUnicode(quantity);

            var dateValue = $('#formUpdateProductInSection #dateValue').val();
            if(dateValue == ""){
                dateValue = "0";
            }
            dateValue = b64EncodeUnicode(dateValue);

            var dateExpirationValue = $('#formUpdateProductInSection #dateExpirationValue').val();
            if(dateExpirationValue == ""){
                dateExpirationValue = "0";
            }
            dateExpirationValue = b64EncodeUnicode(dateExpirationValue);

            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateProductInSection/"+ id +"/"+ idProduct +"/"+ section +"/" + quantity +"/"+ dateValue +"/"+ dateExpirationValue,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    adjustQtdStock();
                    $('#modalUpdateProductInSection').hide();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

         $(document).on('click', '#deleteProductInSection', function(e){
            e.preventDefault();
            var trs = tableProductSection.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idProductInSection : ids};
            deleteProductInSection('Entry/Product/deleteProductInSection', array_deletes, tableProductSection);

        });
    });

    function adjustQtdStock(){
        var id = table.$('tr.hover-select').find('input:checkbox').data('id');
        if(tableProductSection != ""){
            tableProductSection.destroy();
        }
        tableProductSection = $('#productSectionTable').DataTable({
            "ajax": {"url": my_url + "Entry/Product/adjustQtdStock/"+id},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {'data': "section"},
                {"data": "qty"},
                {"data": "dateCreate"},
                {"data": "active"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
        $('#modalAdjustQtd').modal({show: true});
        $.ajax({
            url : my_url+"Entry/Product/adjustQtdStock/"+id,
            type: "POST",
            dataType: 'JSON',
            data : "",
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    $('#idProduct').html(data.idProduct);
                    $('#nameProduct').html(data.nameProduct);
                    $('#totalQty').html(data.qty);
                    $('#modalAdjustQtd').modal({show: true});
                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    }

    function deleteProductInSection(ajaxUrl, array_deletes, table){
    PNotify.prototype.options.styling = "bootstrap3";
    (new PNotify({
        title: "Confirmation Needed",
        text: "Are you sure you want to delete?",
        icon: "glyphicon glyphicon-minus",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
    }))
    .get().on('pnotify.confirm', function(){
        $.ajax({
            url : my_url + ajaxUrl,
            type: "POST",
            dataType: 'JSON',
            data : array_deletes,
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
                //reload_table(table);
                adjustQtdStock();
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
        $('.ui-pnotify-modal-overlay').remove();
    })
    .on('pnotify.cancel', function(){
        $('.ui-pnotify-modal-overlay').remove();
    });
}


    function closeModalCreatePIS(){
        $('#modalCreateProductInSection').hide();
    }

    function closeModalUpdatePIS(){
         $('#modalUpdateProductInSection').hide();
    }

    function closeModalAdjustQtd(){
        reload_table(table);
        $('#modalCreateProductInSection').modal('hide');
        $('#modalUpdateProductInSection').modal('hide');
        $('#modalAdjustQtd').modal('hide');


    }
</script>

<style type="text/css">
    
.pointer{
    cursor: pointer;
}
.disabled{
    pointer-events:none;
    opacity:0.4;
}

</style>

<div class="content">
    <div class="row">
        <div class="top-bar-buttons">
            <div class="button-group">
                <div class="btn-group dropdown">
                    <button type="button" class="btn btn-primary translate">More Options</button>
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a id="adjustQtd" class="translate pointer disabled" onclick="adjustQtdStock()">Adjust Qtd</a></li>
                    </ul>
                </div>
                <p><br><input name="inactive_items" id="collectTaxOnInvoice" type="checkbox" onclick="filter();">                        
                    <t class="translate">Show items that are no longer sold</t>
                </p>
            </div>
        </div>
        <div class="col-md-12">
            <table id="stock" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name of item</th>
                        <th class="translate">Purchase price</th>
                        <th class="translate">Sale price</th>
                        <th class="translate">Medium price</th>
                        <th class="translate">Group 1</th>
                        <th class="translate">Qtd on stock</th>
                        <th class="translate">Total on stock</th>
                        <th class="translate">Stock</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Adjust Qtd-->
<div class="modal fade row row-fluid" id="modalAdjustQtd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" id="closeModalAdjustQtd" class="close" onclick="closeModalAdjustQtd()"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Adjust Stock</h4>
            </div>
              
             <div class="modal-header">
                <div class="row">
                    <div class="col-md-4"> 
                        <h5><b class="translate">Id Product: </b><t id="idProduct"></t></h5> 
                    </div>
                    <div class="col-md-8"> 
                        <h5><b class="translate">Name Product: </b><t id="nameProduct"></t></h5> 
                    </div>
                    <div class="col-md-12"> 
                        <h5><b class="translate">Total: </b><t id="totalQty"></t></h5> 
                    </div>
                </div>
                </div>
                <div class="modal-header">
                    <div class="row">
                        <div class="col-md-12"> 
                            <h4 class="translate">Product In Section</h4> 
                            <div class="button-group">
                                <button id="createProductInSection" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                                <button id="updateProductInSection" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                                <button id="deleteProductInSection" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
                            </div> 
                            <table id="productSectionTable" class="table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('productSectionTable')"></th>
                                        <th class="translate">ID</th>
                                        <th class="translate">Section</th>
                                        <th class="translate">Qty</th>
                                        <th class="translate">Date Created</th>
                                        <th class="translate">active</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="modalCreateProductInSection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="closeModalCreateProductInSection" class="close" onclick="closeModalCreatePIS()"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Create Product In Section</h4>
            </div>
            <form id="formCreateProductInSection" name="formCreateProductInSection" class="form_unit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="sectionValue" class="label-style translate">Section</label>
                            <select name="sectionValue" id="sectionValue">
                                <option value="0" class="translate">Select option</option>
                                <?php echo $section ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="quantity" class="label-style translate">Quantity:</label>
                            <input type="number" class="form-control" id="quantity" name="quantity" min="1" >
                        </div>
                        <div class="col-md-5">
                            <label for="dateValue" class="label-style translate">Date:</label>
                            <div class="input-group date">
                                <input id="dateValue" type="text" class="form-control" name="dateValue" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <label for="dateExpirationValue" class="label-style translate">Date expiration:</label>
                            <div class="input-group date">
                                <input id="dateExpirationValue" type="text" class="form-control" name="dateExpirationValue" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" onclick="closeModalCreatePIS()">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="modalUpdateProductInSection" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="closeModalCreateProductInSection" class="close" onclick="closeModalUpdatePIS()"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Product In Section</h4>
            </div>
            <form id="formUpdateProductInSection" name="formCreateProductInSection" class="form_unit">
                <div class="modal-body">
                    <div class="row">
                        <input type="text" class="form-control" id="idProductInSection" name="idProductInSection" min="1" style="display: none;">
                        <div class="col-md-6">
                            <label for="sectionValue" class="label-style translate">Section</label>
                            <select name="sectionValue" id="sectionValue">
                                <option value="0" class="translate">Select option</option>
                                <?php echo $section ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="quantity" class="label-style translate">Quantity:</label>
                            <input type="number" class="form-control" id="quantity" name="quantity" min="1" >
                        </div>
                        <div class="col-md-5">
                            <label for="dateValue" class="label-style translate">Date:</label>
                            <div class="input-group date">
                                <input id="dateValue" type="text" class="form-control" name="dateValue" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <label for="dateExpirationValue" class="label-style translate">Date expiration:</label>
                            <div class="input-group date">
                                <input id="dateExpirationValue" type="text" class="form-control" name="dateExpirationValue" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" onclick="closeModalUpdatePIS()">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>