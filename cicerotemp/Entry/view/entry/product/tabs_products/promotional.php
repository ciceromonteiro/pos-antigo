<style>
    #promotional .dataTables_filter {
        display: none;
    }
</style>

<div id="promotional" class="tab-pane active">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
                <li><h4 class="no-padding translate">Promotional:</h4></li>
                <li>
                    <div class="btn-group" role="group">
                        <button id="create_promotional" type="button" class="btn btn-primary translate">Create</button>
                        <button id="update_promotional" type="button" class="btn btn-primary translate">Update</button>
                        <button id="delete_promotional" type="button" class="btn btn-primary translate">Delete</button>
                    </div>
                </li>
            </ul>

            <table id="promotional_table" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('promotional_table')"></th>
                        <th style="width: 10px">ID</th>
                        <th class="translate">Gross Price</th>
                        <th class="translate">Initial Date</th>
                        <th class="translate">Price without taxes</th>
                        <th class="translate">Final date</th>
                        <th class="translate">Week</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
            
<!-- Create -->
<div class="modal fade" id="modal_product_promotional" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Promotional</h4>
            </div>
            <form id="form_product_promotional">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="grossPrice" class="label-style translate">Gross Price</label>
                            <input type="text" class="form-control money" id="grossPrice" name="grossPrice" require="true">
                            
                            <label for="priceWithoutTaxes" class="label-style translate">Price Without Taxes</label>
                            <input type="text" class="form-control money" id="priceWithoutTaxes" name="priceWithoutTaxes" require="true">
                            
                            <label class="label-style translate">Initial Date:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="initialDate" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                            
                            <label class="label-style translate">Final Date:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="finalDate" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p class="translate">Week Days</p>
                            <ul style="list-style-type: none">
                                <li><input type="checkbox" name="monday" id="monday" checked="true"> <t class="translate">Monday</t></li>
                                <li><input type="checkbox" name="tuesday" id="tuesday" checked="true"> <t class="translate">Tuesday</t></li>
                                <li><input type="checkbox" name="wednesday" id="wednesday" checked="true"> <t class="translate">Wednesday</t></li>
                                <li><input type="checkbox" name="thursday" id="thursday" checked="true"> <t class="translate">Thursday</t></li>
                                <li><input type="checkbox" name="friday" id="friday" checked="true"> <t class="translate">Friday</t></li>
                                <li><input type="checkbox" name="saturday" id="saturday" checked="true"> <t class="translate">Saturday</t></li>
                                <li><input type="checkbox" name="sunday" id="sunday" checked="true"> <t class="translate">Sunday</t></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="product_promotional_edit_modal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Product Promocional</h4>
            </div>
            <form id="form_product_promocional_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="number" style="display: none" id="idProductPromotional" name="idProductPromotional" require="true">
                            <label for="grossPrice" class="label-style translate">Gross Price</label>
                            <input type="text" class="form-control money" id="grossPrice" name="grossPrice" require="true">
                            
                            <label for="priceWithoutTaxes" class="label-style translate">Price Without Taxes</label>
                            <input type="text" class="form-control money" id="priceWithoutTaxes" name="priceWithoutTaxes" require="true">
                            
                            <label class="label-style translate">Initial Date:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" id="initialDate" name="initialDate" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                            
                            <label class="label-style translate">Final Date:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" id="finalDate" name="finalDate" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p class="translate">Week Days</p>
                            <ul style="list-style-type: none">
                                <li><input type="checkbox" name="monday" id="monday"> <t class="translate">Monday</t></li>
                                <li><input type="checkbox" name="tuesday" id="tuesday"> <t class="translate">Tuesday</t></li>
                                <li><input type="checkbox" name="wednesday" id="wednesday"> <t class="translate">Wednesday</t></li>
                                <li><input type="checkbox" name="thursday" id="thursday"> <t class="translate">Thursday</t></li>
                                <li><input type="checkbox" name="friday" id="friday"> <t class="translate">Friday</t></li>
                                <li><input type="checkbox" name="saturday" id="saturday"> <t class="translate">Saturday</t></li>
                                <li><input type="checkbox" name="sunday" id="sunday"> <t class="translate">Sunday</t></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">    
    function loadPromocional(id){
        var tablePromocional = $('#promotional_table').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllPromotional/"+id},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "grossPrice" },
                { "data": "initialDate" },
                { "data": "priceWithoutTaxes" },
                { "data": "finalDate" },
                { "data": "week" }
            ],
            "retrieve": true,
            "paging": false,
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create_promotional', function(e){
            e.preventDefault();
            resetForm('#form_product_promotional');
            $('#modal_product_promotional').modal({
                show : true
            });
        });

        $(document).on('click', '#update_promotional', function(e){
            e.preventDefault();
            resetForm('#form_product_promocional_edit');
            var id = tablePromocional.$('tr.hover-select').find('input:checkbox').data('id');
            $.ajax({
                url : my_url+"Entry/Product/getPromotional/"+id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#product_promotional_edit_modal').modal({show : true});
                        $('#form_product_promocional_edit #idProductPromotional').val(data.data[0].id);
                        $('#form_product_promocional_edit #grossPrice').val(data.data[0].grossPrice);
                        $('#form_product_promocional_edit #priceWithoutTaxes').val(data.data[0].priceWithoutTaxes);
                        $('#form_product_promocional_edit #initialDate').val(data.data[0].initialDate);
                        $('#form_product_promocional_edit #finalDate').val(data.data[0].finalDate);
                        if(data.data[0].week){
                            var string = String(data.data[0].week);
                            var week = string.split("-");
                            if(week[0] == 1){
                                $("#form_product_promocional_edit #monday").prop("checked", true);
                            } else {
                                $("#form_product_promocional_edit #monday").prop("checked", false);
                            }
                            if(week[1] == 1){
                                $("#form_product_promocional_edit #tuesday").prop("checked", true);
                            } else {
                                $("#form_product_promocional_edit #tuesday").prop("checked", false);
                            }
                            if(week[2] == 1){
                                $("#form_product_promocional_edit #wednesday").prop("checked", true);
                            } else {
                                $("#form_product_promocional_edit #wednesday").prop("checked", false);
                            }
                            if(week[3] == 1){
                                $("#form_product_promocional_edit #thursday").prop("checked", true);
                            } else {
                                $("#form_product_promocional_edit #thursday").prop("checked", false);
                            }
                            if(week[4] == 1){
                                $("#form_product_promocional_edit #friday").prop("checked", true);
                            } else {
                                $("#form_product_promocional_edit #friday").prop("checked", false);
                            }
                            if(week[5] == 1){
                                $("#form_product_promocional_edit #saturday").prop("checked", true);
                            } else {
                                $("#form_product_promocional_edit #saturday").prop("checked", false);
                            }
                            if(week[6] == 1){
                                $("#form_product_promocional_edit #sunday").prop("checked", true);
                            } else {
                                $("#form_product_promocional_edit #sunday").prop("checked", false);
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_product_promotional', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertPromotional",
                type: "POST",
                dataType: 'JSON',
                data : 'idproduct='+id+'&'+$('#form_product_promotional').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_product_promotional').modal('hide');
                    reload_table(tablePromocional);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_product_promocional_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updatePromotional",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_product_promocional_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#product_promotional_edit_modal').modal('hide');
                    reload_table(tablePromocional);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete_promotional', function(e){
            e.preventDefault();
            var trs = tablePromocional.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idColors : ids};
            deleteItens('Entry/Color/deleteColors', array_deletes, tablePromocional);
        });

        $('#update_promotional').attr('disabled', true);
        $('#delete_promotional').attr('disabled', true);
        $("#promotional_table tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#promotional_table');
            if(itemSelected == 0){
                $('#update_promotional').attr('disabled', true);
                $('#delete_promotional').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_promotional').attr('disabled', false);
                $('#delete_promotional').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_promotional').attr('disabled', true);
                $('#delete_promotional').attr('disabled', false);
            }
        });
    }
    
</script>