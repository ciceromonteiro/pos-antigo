<style>
    #image_from_product .dataTables_filter {
        display: none;
    }
</style>

<div id="image_from_product" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
                <li><h4 class="no-padding translate">Image from product:</h4></li>
                <li>
                    <div class="btn-group" role="group" aria-label="...">
                        <button id="image_from_product_table_add" type="button" class="btn btn-primary translate">Add</button>
                        <button id="image_from_product_table_update" type="button" class="btn btn-primary translate">Update</button>
                        <button id="image_from_product_table_delete" type="button" class="btn btn-primary translate">Remove</button>
                    </div>
                </li>
            </ul>

            <table id="image_from_product_table" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0" onclick="markAll('image_from_product_table')"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Image</th>
                        <th class="translate">Main</th>
                        <th class="translate">Order</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal Image From Products -->
<div class="modal fade" id="image_from_product_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Image from Product</h4>
            </div>
            <form id="image_from_product_form" name="image_from_product_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label for="order" class="label-style translate">Order:</label>
                                <input type="number" class="form-control" name="order" id="order">
                                
                                <p style="margin-top: 30px"><input type="checkbox" name="main" id="main"> <t class="translate">Is this image the main one ?</t></p>
                            </div>
                            <div class="col-md-6">
                                <label for="group" class="label-style translate">Upload your image:</label>
                                <img width="160" style="margin-left: 30px; margin-top: 15px;" 
                                     id="image_upload_preview" 
                                     src="../../../../assets/images/upload.png" 
                                     alt="your image" 
                                     onclick="$('#uploadImage_from_product').click()" 
                                     />
                                
                                <input type="file" 
                                       id="uploadImage_from_product" 
                                       name="uploadImage_from_product" 
                                       style="display: none" 
                                       onchange="loadImageFileAsURLImageFromProduct()">
                                
                                <input type="text" id="image_from_product" name="image_from_product" style="display: none">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate" id="button_action">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function loadImageFileAsURLImageFromProduct(){
        var filesSelected = document.getElementById("uploadImage_from_product").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image_from_product_form #image_from_product').val(fileLoadedEvent.target.result);
                $('#image_from_product_form #image_upload_preview').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }
</script>


<!-- Update -->
<div class="modal fade" id="image_from_product_modal_update" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Image From Product</h4>
            </div>
            <form id="image_from_product_form_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label for="order" class="label-style translate">Order:</label>
                                <input type="number" class="form-control" name="order" id="order">
                                
                                <p style="margin-top: 30px"><input type="checkbox" name="main" id="main"> <t class="translate">Is this image the main one ?</t></p>
                            </div>
                            <div class="col-md-6">
                                <label for="group" class="label-style translate">Upload your image:</label>
                                <img width="160" style="margin-left: 30px; margin-top: 15px;" 
                                     id="image_upload_preview1" 
                                     src="../../../assets/images/upload.png" 
                                     alt="your image" 
                                     onclick="$('#uploadImage_from_product1').click()" 
                                     />
                                
                                <input type="file" 
                                       id="uploadImage_from_product1" 
                                       name="uploadImage_from_product1" 
                                       style="display: none" 
                                       onchange="loadImageFileAsURLImageFromProductUpdate()">
                                
                                <input type="text" id="id_image_from_product1" name="id_image_from_product1" style="display: none">
                                <input type="text" id="image_from_product1" name="image_from_product1" style="display: none">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function loadImageFileAsURLImageFromProductUpdate(){
        var filesSelected = document.getElementById("uploadImage_from_product1").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image_from_product_form_edit #image_from_product1').val(fileLoadedEvent.target.result);
                $('#image_from_product_form_edit #image_upload_preview1').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }
    
    function loadImageFromProducts(){
        var table_image_from_product = $('#image_from_product_table').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllImageFromProduct/"+idProduct},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "image" },
                { "data": "main" },
                { "data": "order" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            },
            "retrieve": true,
            "paging": false
        });

        $(document).on('click', '#image_from_product_table_add', function(e){
            e.preventDefault();
            resetForm('#image_from_product_form');
            $('#image_from_product_form #image_upload_preview').attr('src', '../../../../assets/images/upload.png');
            $('#image_from_product_modal').modal({
                show : true
            });
        });

        $(document).on('submit', '#image_from_product_form', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertImageFromProduct",
                type: "POST",
                dataType: 'JSON',
                data : 'idProduct='+idProduct+'&'+$('#image_from_product_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#image_from_product_modal').modal('hide');
                    reload_table(table_image_from_product);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#image_from_product_table_delete', function(e){
            e.preventDefault();
            var trs = table_image_from_product.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idImageFromProducts : ids};
            deleteItens('Entry/Product/deleteImageFromProduct', array_deletes, table_image_from_product);
        });

        $(document).on('click', '#image_from_product_table_update', function(e){
            e.preventDefault();
            resetForm('#image_from_product_form_edit');
            $('#image_from_product_form_edit #image_upload_preview1').attr('src', '../../../../assets/images/upload.png');
            $.ajax({
                url : my_url+"Entry/Product/getImageFromProduct/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table_image_from_product.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#image_from_product_modal_update').modal({show : true});

                        $('#image_from_product_form_edit #id_image_from_product1').val(data.data[0].id);
                        $('#image_from_product_form_edit #order').val(data.data[0].order);
                        if(data.data[0].main == 1){
                            $('#image_from_product_form_edit #image_upload_preview1').attr('checked', true);
                        }                        
                        $('#image_from_product_form_edit #image_from_product1').val(data.data[0].image);
                        $('#image_from_product_form_edit #image_upload_preview1').attr('src', data.data[0].image);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#image_from_product_form_edit', function(e){
             var id = table_image_from_product.$('tr.hover-select').find('input:checkbox').data('id');
             alert(id);
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateImageFromProduct/" + id,
                type: "POST",
                dataType: 'JSON',
                data : $('#image_from_product_form_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#image_from_product_modal_update').modal('hide');
                    reload_table(table_image_from_product);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $('#image_from_product_table_update').attr('disabled', true);
        $('#image_from_product_table_delete').attr('disabled', true);
        $("#image_from_product_table tbody").on( 'click', 'tr', function () {
                var checkboxInput = $(this).find('input:checkbox');
                if ($(this).hasClass('hover-select')){
                    $(this).removeClass('hover-select');
                    checkboxInput[0].checked = false;
                } else {
                    $(this).addClass('hover-select');
                    checkboxInput[0].checked = true;
                }

                var itemSelected = checkMark('#image_from_product_table');
                if(itemSelected == 0){
                    $('#image_from_product_table_update').attr('disabled', true);
                    $('#image_from_product_table_delete').attr('disabled', true);
                }
                if(itemSelected == 1){
                    $('#image_from_product_table_update').attr('disabled', false);
                    $('#image_from_product_table_delete').attr('disabled', false);
                }
                if(itemSelected > 1){
                    $('#image_from_product_table_update').attr('disabled', false);
                    $('#image_from_product_table_delete').attr('disabled', false);
                }
            });
    }
</script>