<?php 
    $select_option_currency = "<option value='0'>Select Option</option>";
    foreach ($array_answer['currency'] as $value) {
        $select_option_currency .= "<option value='".$value->getIdcurrency()."'>".$value->getName()."</option>";
    }
?>
<div id="acquisition" class="tab-pane" style="margin-bottom: 30px">
    <div class="row row-fluid">
        <div class="col-md-12">
            <p><input type="checkbox" id="prediction_of_purchase" name="prediction_of_purchase"> <t class="translate">Prediction of purchase</t></p>
        </div>
        <div class="col-md-3">
            <label class="translate">Net weight</label>
            <input type="text" name="net_weight" id="net_weight" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Additional Weight</label>
            <input type="text" name="additional_weight" id="additional_weight" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Volume</label>
            <input type="text" name="volume" id="volume" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Length</label>
            <input type="text" name="lenght" id="lenght" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Width</label>
            <input type="text" name="width" id="width" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Height</label>
            <input type="text" name="height" id="height" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Minimum purchase of packages</label>
            <input type="text" name="minimun_purchase_of_packages" id="minimun_purchase_of_packages" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Minimum purchase</label>
            <input type="text" name="minimum_purchase" id="minimum_purchase" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Min Stock</label>
            <input type="text" name="min_stock" id="min_stock" class="form-control number">
        </div>
        <div class="col-md-3">
            <label class="translate">Max Stock</label>
            <input type="text" name="max_stock" id="max_stock" class="form-control number">
        </div>
    </div>

    <div class="row row-fluid">
        <div class="col-md-12">
            <h4 class="translate">Currency</h4>
        </div>
        <div class="col-md-6">
            <label class="translate">Currency of purchase</label>
            <select name="select_currency" id="select_currency">
                <?php echo $select_option_currency ?>
            </select>
            <p><t class="translate">Exchange rate:</t> 0.00</p>
        </div>
        <div class="col-md-6">
            <label class="translate">Purchase price</label>
            <div class="input-group">
                <input type="text" class="form-control money" id="purchase_price_exchange" name="purchase_price_exchange" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2"><t class="translate">Equals:</t> 0.00</span>
            </div>
        </div>
    </div>
    
    <div class="row row-fluid">
        <div class="col-md-12">
            <h4 class="translate">Import</h4>
        </div>
        <div class="col-md-6">
            <label class="translate">Importing rate</label>
            <div class="input-group">
                <input type="text" class="form-control money" id="import_rate" name="import_rate" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2"><t class="translate">Exchange rate:</t> 0.00</span>
            </div>
        </div>
        <div class="col-md-6">
            <label class="translate">Freight Tax Amount</label>
            <div class="input-group">
                <input type="text" class="form-control money" id="freight_tax_amount" name="freight_tax_amount" aria-describedby="basic-addon2">
                <span class="input-group-addon" id="basic-addon2"><t class="translate">Equals:</t> 0.00</span>
            </div>
            <p><t class="translate">Cost pre customs</t> 0.00</p>
            <p><t class="translate">Cost after customs</t> 0.00</p>
        </div>
    </div>
    
</div>
