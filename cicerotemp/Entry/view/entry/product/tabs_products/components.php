<style>
    #tableComponent_wrapper .dataTables_filter {
        display: none;
    }
</style>

<div id="components" class="tab-pane">
    <div class="row row-fluid">
        <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
            <li><p><input type="checkbox" id="components_compound_product" name="components_compound_product" onchange="enableComponent(this.value)"> <t class="translate">Component product</t></p></li>
            <li><p><input type="checkbox" id="components_show_order_subitens" name="components_show_order_subitens"> <t class="translate">Show in the order of purchase the sub-items of this product</t></p></li>
        </ul>
        <div class="col-md-12">			
            <div class="col-md-6">
                <div class="col-md-6 no-padding-left">
                    <label class="translate">Estimated total cost</label> 
                    <input type="text" class="form-control" name="custo_total_estimado" disabled="true">
                </div>
                <div class="col-md-6 no-padding">
                    <label class="translate">Percentage of sale</label> 
                    <input type="text" class="form-control" name="custo_total_estimado" disabled="true">
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-6 no-padding-left">
                    <label class="translate">Price range</label> 
                    <input type="text" class="form-control" name="custo_total_estimado" disabled="true">
                </div>
                <div class="col-md-6 no-padding">
                    <label class="translate">Final price</label>
                    <input type="text" class="form-control" name="custo_total_estimado" disabled="true" value="0.00">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
                <li><h4 class="no-padding translate">Components:</h4></li>
                <li>
                    <div class="btn-group" role="group">
                        <button id="create_components" type="button" class="btn btn-primary translate">Add</button>
                        <button id="update_components" type="button" class="btn btn-primary translate">Update</button>
                        <button id="delete_components" type="button" class="btn btn-primary translate">Delete</button>
                    </div>
                </li>
            </ul>
            <table id="tableComponent" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name of product</th>
                        <th class="translate">Qtd</th>
                        <th class="translate">Cost</th>
                        <th class="translate">Retail Price</th> 
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="create_components_product_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Component</h4>
            </div>
            <form id="create_components_product_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="idComponent" class="label-style translate">Product</label>
                            <select name="productComponent">
                                <?php echo $select_products ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="qtd" class="label-style translate">Qtd</label>
                            <input type="text" class="form-control number" id="qtd" name="qtd" required="true">
                        </div>
                        <div class="col-md-5">
                            <label for="cost" class="label-style translate">Cost</label>
                            <input type="text" class="form-control money" id="cost" name="cost" required="true">
                        </div>
                        <div class="col-md-5">
                            <label for="retailPrice" class="label-style translate">Retail Price</label>
                            <input type="text" class="form-control money" id="retailPrice" name="retailPrice" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="create_components_product_modal_edit" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Product Promocional</h4>
            </div>
            <form id="create_components_product_form_edit">
                <div class="modal-body">
                    <div class="row">
                        <input type="number" style="display: none" id="idComponent" name="idComponent" required="true">
                        <div class="col-md-12">
                            <label for="idComponent" class="label-style translate">Product</label>
                            <select name="productComponent" id="productComponent">
                                <?php echo $select_products ?>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="qtd" class="label-style translate">Qtd</label>
                            <input type="text" class="form-control number" id="qtd" name="qtd" required="true">
                        </div>
                        <div class="col-md-5">
                            <label for="cost" class="label-style translate">Cost</label>
                            <input type="text" class="form-control money" id="cost" name="cost" required="true">
                        </div>
                        <div class="col-md-5">
                            <label for="retailPrice" class="label-style translate">Retail Price</label>
                            <input type="text" class="form-control money" id="retailPrice" name="retailPrice" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function setMarginComponents(value){
        if($('#traded_value_for_sell_brute').val() == ''){
            $('#profit_gross_profit_value').val('');
            $('#profit_margin').val('');
            return;
        }
        if($('#pricing_of_buy').val() == ''){
            var pricingOfBuy = 0;
        } else {
            var pricingOfBuy = parseFloat($('#pricing_of_buy').val());
        }  
        
        if($('#pricing_of_shipping').val() == ''){
            var pricingOfShipping = 0;
        } else {
            var pricingOfShipping = parseFloat($('#pricing_of_shipping').val());
        }
        
        var tradedOfSellWithTax = parseFloat($('#traded_value_for_sell_brute').val());
        var tradedOfSellWithoutTax = parseFloat($('#traded_value_for_sell_liquid').val());
        
        var grossPrice = (tradedOfSellWithTax - (pricingOfBuy + pricingOfShipping));
        var margin = (grossPrice/tradedOfSellWithTax) * 100;
        
        $('#div_total_final_traded').val(margin+"%");
    }
    
    function enableComponent(value){
        if($('#components').hasClass('component-enable')){
            $('#create_components').prop('disabled', true);
            $('#update_components').prop('disabled', true);
            $('#delete_components').prop('disabled', true);
            $('#components').removeClass('component-enable');
        } else {
            $('#create_components').prop('disabled', false);
            $('#components').addClass('component-enable');
        };
    }
    
    function loadComponent(){
        
        var tableComponent = $('#tableComponent').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllComponent/"+idProduct},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "qtd" },
                { "data": "cost" },
                { "data": "retailPrice" }
            ],
            "retrieve": true,
            "paging": false,
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create_components', function(e){
            e.preventDefault();
            resetForm('#create_components_product_form');
            $('#create_components_product_modal').modal({
                show : true
            });
        });

        $(document).on('submit', '#create_components_product_form', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertComponent",
                type: "POST",
                dataType: 'JSON',
                data : 'idproduct='+idProduct+'&'+$('#create_components_product_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#create_components_product_modal').modal('hide');
                    reload_table(tableComponent);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('click', '#update_components', function(e){
            e.preventDefault();
            resetForm('#create_components_product_form_edit');
            var id = tableComponent.$('tr.hover-select').find('input:checkbox').data('id');
            $.ajax({
                url : my_url+"Entry/Product/getComponent/"+id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#create_components_product_modal_edit').modal({show : true});
                        $('#create_components_product_form_edit #idComponent').val(data.data[0].idComponent);
                        $('#create_components_product_form_edit #productComponent').val(data.data[0].productComponent);
                        $('#create_components_product_form_edit #qtd').val(data.data[0].qtd);
                        $('#create_components_product_form_edit #cost').val(data.data[0].cost);
                        $('#create_components_product_form_edit #retailPrice').val(data.data[0].retailPrice);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#create_components_product_form_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateComponent",
                type: "POST",
                dataType: 'JSON',
                data : $('#create_components_product_form_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#create_components_product_modal_edit').modal('hide');
                    reload_table(tableComponent);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete_components', function(e){
            e.preventDefault();
            var trs = tableComponent.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idComponents : ids};
            deleteItens('Entry/Product/deleteComponent', array_deletes, tableComponent);
        });

        $('#create_components').attr('disabled', true);
        $('#update_components').attr('disabled', true);
        $('#delete_components').attr('disabled', true);
        $("#tableComponent tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#tableComponent');
            if(itemSelected == 0){
                $('#update_components').attr('disabled', true);
                $('#delete_components').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_components').attr('disabled', false);
                $('#delete_components').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_components').attr('disabled', true);
                $('#delete_components').attr('disabled', false);
            }
        });
    }
</script>