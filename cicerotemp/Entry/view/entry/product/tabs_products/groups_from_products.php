<?php 
    $select = "<option value=''>Select Option</option>";
    $select_groups = "";
    foreach ($array_answer['productGp'] as $value) {
        $select_groups .= "<option value='".$value->getIdproductGp()."'>".$value->getName()."</option>";
    }
?>

<style>
    #groups_from_products .dataTables_filter {
        display: none;
    }
</style>

<div id="groups_from_products" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
                <li><h4 class="no-padding translate">Groups from products:</h4></li>
                <li>
                    <div class="btn-group" role="group">
                        <button id="add_group_from_products" class="btn btn-primary translate">Add</button>
                        <button id="remove_group_from_products" class="btn btn-primary translate">Remove</button>
                    </div>
                </li>
            </ul>

            <table id="productGroup" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('productGroup')"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Description</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal Groups From Products -->
<div class="modal fade" id="groups_from_products_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Product Group</h4>
            </div>
            <form id="groups_from_products_form" name="groups_from_products_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="group" class="label-style translate">Group</label>
                            <select name="group" id="group" required="true">
                                <?php echo $select.$select_groups ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate" id="button_action">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function loadGroupsFromProducts(){
        var tableProductGroup = $('#productGroup').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllProductGpByProduct/"+idProduct},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            },
            "retrieve": true,
            "paging": false
        });

        $(document).on('click', '#add_group_from_products', function(e){
            e.preventDefault();
            resetForm('#groups_from_products_form');
            $('#groups_from_products_modal').modal({
                show : true
            });
        });

        $(document).on('submit', '#groups_from_products_form', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertProductGpByProduct",
                type: "POST",
                dataType: 'JSON',
                data : 'idProduct='+idProduct+'&'+$('#groups_from_products_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#groups_from_products_modal').modal('hide');
                    reload_table(tableProductGroup);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#remove_group_from_products', function(e){
            e.preventDefault();
            var trs = tableProductGroup.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idProductsGp : ids};
            deleteItens('Entry/Product/deleteProductGpByProduct', array_deletes, tableProductGroup);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#productGroup tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#productGroup');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    }
</script>