<script type="text/javascript">
    
    //function loadOrdersProduct(){
$(document).ready(function() {
        var substitute_included_products_table = $('#substitute_included_products_table').DataTable({
            "ajax": {"url": my_url + "Entry/Product/getAllSubstituteIncludedProducts/" + idProduct},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "product"},
                {"data": "type"},
                {"data": "price"}
            ],
            "retrieve": true,
            "paging": false,
            "language": {
                "url": my_url + my_language + ".json"
            }
        });

        $(document).on('click', '#substitute_included_products_create', function (e) {
            e.preventDefault();
            resetForm('#substitute_included_products_form');
            $('#substitute_included_products_modal').modal({
                show: true
            });
        });

        $(document).on('click', '#substitute_included_products_update', function (e) {
            e.preventDefault();
            resetForm('#substitute_included_products_form_edit');
            var id = substitute_included_products_table.$('tr.hover-select').find('input:checkbox').data('id');
            $.ajax({
                url: my_url + "Entry/Product/getSubstituteIncludedProducts/" + id,
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#substitute_included_products_modal_edit').modal({show: true});
                        $('#substitute_included_products_form_edit #id_substitute_included_products').val(data.data[0].id);
                        $('#substitute_included_products_form_edit #value_product').val(data.data[0].price);
                        $('#substitute_included_products_form_edit #type').val(data.data[0].type);
                        $('#substitute_included_products_form_edit #product').val(data.data[0].product);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#substitute_included_products_form', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Entry/Product/insertSubstituteIncludedProducts",
                type: "POST",
                dataType: 'JSON',
                data: 'idproduct=' + idProduct + '&' + $('#substitute_included_products_form').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#substitute_included_products_modal').modal('hide');
                    reload_table(substitute_included_products_table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#substitute_included_products_form_edit', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Entry/Product/updateSubstituteIncludedProducts",
                type: "POST",
                dataType: 'JSON',
                data: $('#substitute_included_products_form_edit').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#substitute_included_products_modal_edit').modal('hide');
                    reload_table(substitute_included_products_table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#substitute_included_products_delete', function (e) {
            e.preventDefault();
            var trs = substitute_included_products_table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });
            var i = 0, ids = [];
            for (i = 0; i < trs.length; i++) {
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }
            var array_deletes = {idSubsIncl: ids};
            deleteItens('Entry/Product/deleteSubstituteIncludedProducts', array_deletes, substitute_included_products_table);
        });

        $('#orders #substitute_included_products_update').attr('disabled', true);
        $('#orders #substitute_included_products_delete').attr('disabled', true);
        $("#orders #substitute_included_products_table tbody").on('click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')) {
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#substitute_included_products_table');
            if (itemSelected == 0) {
                $('#orders #substitute_included_products_update').attr('disabled', true);
                $('#orders #substitute_included_products_delete').attr('disabled', true);
            }
            if (itemSelected == 1) {
                $('#orders #substitute_included_products_update').attr('disabled', false);
                $('#orders #substitute_included_products_delete').attr('disabled', false);
            }
            if (itemSelected > 1) {
                $('#orders #substitute_included_products_update').attr('disabled', true);
                $('#orders #substitute_included_products_delete').attr('disabled', false);
            }
        });
    });
</script>

<style>
    #substitute_included_products_table_wrapper .dataTables_filter {
        display: none;
    }
</style>

<div id="orders" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="col-md-2">
                    <label for="percentOrder" class="translate">Percent</label>
                    <input type="text" name="percentOrder" id="percentOrder" class="form-control percent">
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 30px;">
                <div class="col-md-4">
                    <p><input type="checkbox" name="" id=""> <t class="translate">The sale price will only be set at the time of the first purchase</t></p>
                    <p><input type="checkbox" name="" id=""> <t class="translate">Do not put a selling price, ask for every time the price</t></p>
                    <p><input type="checkbox" name="" id=""> <t class="translate">Do not allow discount for this product</t></p>
                    <p><input type="checkbox" name="" id=""> <t class="translate">Product only to buy another product, can not be sold alone</t></p>
                </div>
                <div class="col-md-4">
                    <p><input type="checkbox" name="" id=""> <t class="translate">Print product information in the tax coupon</t></p>
                    <p><input type="checkbox" name="" id=""> <t class="translate">Fill quantity based on the information of a scale integrated to the system</t></p>
                    <p><input type="checkbox" name="" id=""> <t class="translate">Hide from statistics</t></p>
                    <p><input type="checkbox" name="" id=""> <t class="translate">Display Alternate Products at Checkout</t></p>
                </div>
                <div class="col-md-4">
                    <p><input type="checkbox" name="" id=""> <t class="translate">The purchase price will only be set at the time of the first purchase</t></p>
                    <p><input type="checkbox" name="" id=""> <t class="translate">View product description in POS</t></p>
                    <p><input type="checkbox" name="" id=""> <t class="translate">Do not show product on tax coupon</t></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
                <li><h4 class="no-padding translate">Substitute products and products included:</h4></li>
                <li>
                    <div class="btn-group" role="group">
                        <button id="substitute_included_products_create" type="button" class="btn btn-primary translate">Add</button>
                        <button id="substitute_included_products_update" type="button" class="btn btn-primary translate">Update</button>
                        <button id="substitute_included_products_delete" type="button" class="btn btn-primary translate">Delete</button>
                    </div>
                </li>
            </ul>

            <table id="substitute_included_products_table" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('substitute_included_products_table')"></th>
                        <th style="width: 10px"  class="translate">ID</th>
                        <th class="translate">Product</th>
                        <th class="translate">Type</th>
                        <th class="translate">Price</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="substitute_included_products_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Substitute products and products included</h4>
            </div>
            <form id="substitute_included_products_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="product" class="translate">Product</label>
                            <select name="product" id="product" required="true">
                                <?php echo "<option value='0'>Select Option</option>".$select_products ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="type" class="translate">Type</label>
                            <select name="type" id="type" required="true">
                                <option value="1" class="translate">Substitute</option>
                                <option value="2" class="translate">Included</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="value_product" class="translate">Value</label>
                            <input type="text" name="value_product" id="value_product" class="form-control money">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="substitute_included_products_modal_edit" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update</h4>
            </div>
            <form id="substitute_included_products_form_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="product" class="translate">Product</label>
                            <input type="number" style="display: none" id="id_substitute_included_products" name="id_substitute_included_products" require="true">
                            <select name="product" id="product" required="true">
                                <?php echo "<option value='0'>Select Option</option>".$select_products ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="type" class="translate">Type</label>
                            <select name="type" id="type" required="true">
                                <option value="1" class="translate">Substitute</option>
                                <option value="2" class="translate">Included</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="value_product" class="translate">Value</label>
                            <input type="text" name="value_product" id="value_product" class="form-control money">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>