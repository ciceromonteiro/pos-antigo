<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click','#create_alternatives', function(e){
            e.preventDefault();
            $('#generate_modal').modal({show : true});
        });

        $(document).on('click', '#update_alternatives', function(e){
            e.preventDefault();
            $('#newSizeAndColor_modal').modal({show: true});
        })

        var tableAlternatives = $('#product_alternatives').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllAlternatives/"+idProduct},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "nameProduct" },
                { "data": "price" }
            ],
            "retrieve": true,
            "paging": false,
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('submit', '#form_generete_color_size', function(e){
        e.preventDefault();
        $.ajax({
            url : my_url+"Entry/Product/genereteColorsSize",
            type: "POST",
            dataType: 'JSON',
            data : 'idProduct='+idProduct+'&'+$('#form_generete_color_size').serialize(),
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
                $('#generate_modal').modal('hide');
//                reload_table(tableColorSizes);
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    });
    
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#color_sizes_product tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#color_sizes_product');
            if(itemSelected == 0){
                $('#update_sizecolor').attr('disabled', true);
                $('#delete_sizecolor').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_sizecolor').attr('disabled', false);
                $('#delete_sizecolor').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_sizecolor').attr('disabled', true);
                $('#delete_sizecolor').attr('disabled', false);
            }
        });
    });
</script>

<div id="alternatives" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
                <li><h4 class="no-padding translate">Alternatives:</h4></li>
                <li>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary translate">Create</button>
                        <button type="button" class="btn btn-primary translate">Update</button>
                        <button type="button" class="btn btn-primary translate">Delete</button>
                    </div>
                </li>
            </ul>
            <table id="shippingMethod" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name of product</th>
                        <th class="translate">Price</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
