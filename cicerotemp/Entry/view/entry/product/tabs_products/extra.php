<?php
    $select = "<option value='0'>Select Option</option>";
    $select_option_manufacturer = "";
    $select_option_supplier = "";
    $select_option_unit = "";
    $select_option_departments = "";
    $select_option_projects = "";
    $select_stocks = "";
    
    foreach ($array_answer['manufacturers'] as $value) {
        $select_option_manufacturer .= "<option value='" . $value->getIdmanufacturer() . "'>" . $value->getName() . "</option>";
    }

    foreach ($array_answer['suppliers'] as $value) {
        $select_option_supplier .= "<option value='" . $value->getIdsupplier() . "'>" . $value->getCompanycompany()->getName() . "</option>";
    }

    foreach ($array_answer['units'] as $value) {
        $select_option_unit .= "<option value='" . $value->getIdunit() . "'>" . $value->getName() . "</option>";
    }

    foreach ($array_answer['departments'] as $value) {
        $select_option_departments .= "<option value='" . $value->getIddepartment() . "'>" . $value->getName() . "</option>";
    }
    
    foreach ($array_answer['projects'] as $value) {
        $select_option_projects .= "<option value='" . $value->getIdproject() . "'>" . $value->getName() . "</option>";
    }
    
    foreach ($array_answer['stocks'] as $value){
        $select_stocks .= "<option value='".$value->getIdwarehouse()."'>".$value->getName()."</option>";
    }
    
    $select_option_manufacturer = $select . $select_option_manufacturer;
    $select_option_supplier = $select . $select_option_supplier;
    $select_option_unit = $select . $select_option_unit;
    $select_option_departments = $select . $select_option_departments;
    $select_option_projects = $select . $select_option_projects;
?>

<div id="extra" class="tab-pane" style="margin-bottom: 30px">
    <div class="row row-fluid">
        <div class="col-md-3">
            <label class="translate">Number of serie from product</label> 
            <select name="number_of_serie_from_product" id="number_of_serie_from_product">
                <option value="0" class="translate">Not in use</option>
                <option value="" class="translate">When sold</option>
                <option value="" class="translate">When sold and bought</option>
                <option value="" class="translate">Match when sold</option>
                <option value="" class="translate">Match when returned</option>
            </select>
            <select class="form-control form-control-select" disabled="true" style="margin-top: 10px;">
                <option value="">Select option</option>
            </select>
        </div>

        <div class="col-md-3">
            <label class="translate">Manufacturer</label> 
            <select name="select_manufacturer" id="select_manufacturer">
                <?php echo $select_option_manufacturer ?>
            </select>
        </div>

        <div class="col-md-3">
            <label class="translate">Supplier</label> 
            <select name="select_supplier" id="select_supplier">
                <?php echo $select_option_supplier ?>
            </select>
        </div>

        <div class="col-md-3">
            <label class="translate">Unit from product</label> 
            <select name="select_unit_from_product" id="select_unit_from_product">
                <?php echo $select_option_unit ?>
            </select>
        </div>

    </div>

    <div class="row row-fluid">
        <div class="col-md-3">
            <label class="translate">Base of calc</label> 
            <select name="select_base_of_calc" id="select_base_of_calc">
                <option value="0" class="translate">Select option</option>
                <option value="1" class="translate">Sales Amount</option>
                <option value="2" class="translate">Contribution</option>
                <option value="3" class="translate">Cost</option>
            </select>
        </div>

        <div class="col-md-3">
            <label class="translate">Number of manufacture</label> 
            <input type="text" class="form-control" name="number_of_manufacture" id="number_of_manufacture" value="">
        </div>

        <div class="col-md-3">
            <label class="translate">Number of supplier</label> 
            <input type="text" class="form-control" name="number_of_supplier" id="number_of_supplier" value="">
        </div>

        <div class="col-md-3">
            <label class="translate">Multiple units</label> 
            <input type="text" class="form-control" name="multiple_units" id="multiple_units" value="">
        </div>
    </div>

    <div class="row row-fluid">
        <div class="col-md-3">
            <label class="translate">Status of product</label> 
            <select name="status_of_product" id="status_of_product">
                <option value="6" class="translate">Except temporarily</option>
                <option value="1" class="translate">Active</option>
                <option value="2" class="translate">Passive</option>
                <option value="3" class="translate">Expired</option>
                <option value="4" class="translate">Blocked</option>
            </select>
        </div>

        <div class="col-md-3">
            <label class="translate">Departament</label> 
            <select name="select_department" id="select_department">
                <?php echo $select_option_departments ?>
            </select>
        </div>

        <div class="col-md-3">
            <label class="translate">Projects</label> 
            <select name="select_projects" id="select_projects">
                <?php echo $select_option_projects ?>
            </select>
        </div>

        <div class="col-md-3">
            <label class="translate">Stock</label> 
            <select name="place_where_the_product" id="place_where_the_product">
                <option value="0">None</option>
                <?php echo $select_stocks ?>
            </select>
        </div>
    </div>
</div>
