<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#project').DataTable( {
            "ajax": {"url": my_url+"Entry/Project/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "description" },
                { "data": "start" },
                { "data": "price" },
                { "data": "end" },
                { "data": "priceAdditional" },
                { "data": "stop" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
        
        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_project');
            $('#projectModal').modal({
                show : true
            });
        });
        
        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_project_update');
            $.ajax({
                url : my_url+"Entry/Project/getProject/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#projectEditModal').modal({show : true});
                        $('#form_project_update #idProject').val(data.data[0].id);
                        $('#form_project_update #nameProject').val(data.data[0].name);
                        $('#form_project_update #description').val(data.data[0].description);
                        $('#form_project_update #dateStart').val(data.data[0].start);
                        $('#form_project_update #price').val(data.data[0].price);
                        $('#form_project_update #dateEnd').val(data.data[0].end);
                        $('#form_project_update #priceAdditional').val(data.data[0].priceAdditional);
                        $('#form_project_update #dateStop').val(data.data[0].stop);
                        $("#form_project_update #status").val(data.data[0].status).prop('selected', true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_project', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Project/insert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_project').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#projectModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_project_update', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Project/update",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_project_update').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#projectEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idProjects : ids};
            deleteItens('Entry/Project/deleteProjects', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#project tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#project');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<!-- Menu -->
<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>      
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="project" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('project')"></th>
                        <th style="width: 10px">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Description</th>
                        <th class="translate">Start</th>
                        <th class="translate">Price</th>
                        <th class="translate">End</th>
                        <th class="translate">Price additional</th>
                        <th class="translate">Stop</th>
                        <th class="translate">Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Insert -->
<div class="modal fade" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Create Project</h4>
            </div>
            <form id="form_project">
                <div class="modal-body">
                    <div class="row row-fluid line-form">
                        <div class="col-md-12">
                            <label class="label-style translate">Name of project:</label>
                            <input type="text" class="form-control" name="nameProject" required="true">
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Date Start:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="dateStart" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Date Stop:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="dateStop" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Date End:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="dateEnd" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Price:</label>
                            <input type="text" class="form-control money" name="price" data-decimal="." required="true">
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Price Additional:</label>
                            <input type="text" class="form-control money" name="priceAdditional" data-decimal="." required="true">
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Status</label>
                            <select name="status" required="true">
                                <option value="0">Select Option</option>
                                <option value="1">Active</option>
                                <option value="2">Blocked</option>
                                <option value="3">Deleted</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label class="label-style translate">Description:</label>
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="projectEditModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Project</h4>
            </div>
            <form id="form_project_update">
                <input type="text" name="idProject" id="idProject" required="true" style="display: none">
                <div class="modal-body">
                    <div class="row row-fluid line-form">
                        <div class="col-md-12">
                            <label class="label-style translate">Name of project:</label>
                            <input type="text" class="form-control" name="nameProject" id="nameProject" required="true">
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Date Start:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="dateStart" id="dateStart" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Date Stop:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="dateStop" id="dateStop" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Date End:</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" name="dateEnd" id="dateEnd" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Price:</label>
                            <input type="text" class="form-control money" name="price" id="price" data-decimal="." required="true">
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Price Additional:</label>
                            <input type="text" class="form-control money" name="priceAdditional" id="priceAdditional" data-decimal="." required="true">
                        </div>
                        <div class="col-md-4">
                            <label class="label-style translate">Status</label>
                            <select name="status" id="status" required="true">
                                <option value="0">Select Option</option>
                                <option value="1">Active</option>
                                <option value="2">Blocked</option>
                                <option value="3">Deleted</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label class="label-style translate">Description:</label>
                            <textarea class="form-control" name="description" id="description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>