<?php

$options = "<option value='0'>Select Option</option>";
foreach ($array_answer['country'] as $value) {
    $options .= "<option value='".$value->getidzzCountry()."'>".$value->getName()."</option>";
}
if(isset($_GET['id'])){
    $nav = "update";
    $id = $_GET['id'];
} else {
    $nav = "create";
    $id = '';
}
include "nav.php";
?>

<script type="text/javascript">
        var idSupplier = "<?php echo $id ?>";
        if (idSupplier != ''){
            $(document).ready(function(){
                resetForm('#form_supplier');
                $.ajax({
                    url : my_url+"Entry/Supplier/getSupplier",
                    type: "POST",
                    dataType: 'JSON',
                    data : 'id='+idSupplier,
                    success: function(values, textStatus, jqXHR){
                        if (values.result == 'success'){
                            $("input[name='idSupplier']").val(values.data.idSupplier);
                            $("input[name='yourIdOnSupplier']").val(values.data.yourIdOnSupplier);
                            $("input[name='nameFromSupplier']").val(values.data.nameFromSupplier);
                            $("#country").val(values.data.country).prop('selected', true);
                            $("input[name='language']").val(values.data.language);
                            $("input[name='accountOnBank']").val(values.data.accountOnBank);
                            $("input[name='discountDefault']").val(values.data.discountDefault);
                            $("input[name='phone1']").val(values.data.phone1);
                            $("input[name='email1']").val(values.data.email1);
                            $("input[name='webSite1']").val(values.data.webSite1);
                            $("input[name='addressEstablishment']").val(values.data.addressEstablishment);
                            $("input[name='zipCodeEstablishment']").val(values.data.zipCodeEstablishment);
                            $("input[name='mailingAddress']").val(values.data.mailingAddress);
                            $("input[name='zipCodeMailingAddress']").val(values.data.zipCodeMailingAddress);
                            $("input[name='phoneMailing']").val(values.data.phoneMailing);
                            $("input[name='faxMailing']").val(values.data.faxMailing);
                            $("input[name='glnMailing']").val(values.data.glnMailing);
                            $("input[name='contactPersonFullName']").val(values.data.contactPersonFullName);
                            $("input[name='phoneContactPerson']").val(values.data.phoneContactPerson);
                            $("input[name='emailContactPerson']").val(values.data.emailContactPerson);
                            $("input[name='webSiteContactPerson']").val(values.data.webSiteContactPerson);
                            $("input[name='orgNrContactPerson']").val(values.data.orgNrContactPerson);
                            $("input[name='obs']").val(values.data.obs);
                            if(values.data.sendOrder == "on"){
                                $("#sendOrder").prop("checked", true);
                            }
                            $("#idSupplier").prop("disabled", true);
                        } else {
                            notification(false);
                            console.log(values.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        notification(false);
                        console.log(jqXHR.responseText);
                    }
                });
            });
        }
        
        $(document).on('submit', '#form_supplier', function(e){
            if(idSupplier == ''){
                urlTotal = my_url+"Entry/Supplier/insert";
            } else {
                urlTotal = my_url+"Entry/Supplier/edit";
                $("#idSupplier").prop("disabled", false);
            }
            e.preventDefault();
            $.ajax({
                url : urlTotal,
                type: "POST",
                dataType: 'JSON',
                data : $('#form_supplier').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
            $("#idSupplier").prop("disabled", true);
        });
</script>

<style type="text/css">
    .row-fluid {
        padding-left: 25px;
        padding-right: 25px;
    }
    .no-padding {
        padding: 0px;
    }
    .no-padding-left {
        padding-left: 0px;
    }
    .divider {
        margin-bottom: 15px;
        margin-top: 15px;
        margin-right: auto;
        margin-left: auto;
        width: 95%;
        height: 2px;
        background-color: #EDECEC;
    }
    .navbar-three li {
        padding-bottom: 0px; 
        margin-bottom: 0px
    }
</style>

<div class="content">
    <form id="form_supplier">
        <div class="row row-fluid">
            <div class="col-md-12">
                <h4 class="no-padding">Informations</h4>
            </div>
            <div class="col-md-2">
                <label for="idSupplier">ID from Supplier</label>
                <input type="number" name="idSupplier" id="idSupplier" class="form-control">
            </div>
            <div class="col-md-2">
                <label for="yourIdOnSupplier">Your ID on Supplier</label>
                <input type="text" name="yourIdOnSupplier" class="form-control">
            </div>
            <div class="col-md-8">
                <label for="nameFromSupplier">Name from Supplier</label>
                <input required="true" type="text" name="nameFromSupplier" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="country">Country</label>
                <select name="country" id="country">
                    <?php echo $options; ?>
                </select>
            </div>
            <div class="col-md-3">
                <label for="language">Language</label>
                <input type="text" name="language" class="form-control" required="true">
            </div>
            <div class="col-md-3">
                <label for="accountOnBank">Account on bank</label>
                <input type="text" name="accountOnBank" class="form-control number">
            </div>
            <div class="col-md-3">
                <label for="discountDefault">Discount Default</label>
                <input type="text" name="discountDefault" class="form-control percent">
            </div>
            <div class="col-md-3">
                <label for="phone1">Phone</label>
                <input type="text" name="phone1" class="form-control number">
            </div>
            <div class="col-md-3">
                <label for="email1">Email</label>
                <input type="text" name="email1" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="webSite1">Web Site</label>
                <input type="text" name="webSite1" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="orgNrContactPerson">Org. nr</label>
                <input required="true" type="text" name="orgNrContactPerson" class="form-control">
            </div>
        </div>

        <div class="divider"></div>

        <div class="row row-fluid">
            <div class="col-md-12">
                <h4 class="no-padding">Address of establishment</h4>
            </div>
            <div class="col-md-10">
                <label for="addressEstablishment">Address</label>
                <input required="true" type="text" name="addressEstablishment" class="form-control">
            </div>
            <div class="col-md-2">
                <label for="zipCodeEstablishment">ZIP Code</label>
                <input required="true" type="text" name="zipCodeEstablishment" class="form-control number">
            </div>
        </div>

        <div class="divider"></div>

        <div class="row row-fluid">
            <div class="col-md-12">
                <h4 class="no-padding">Mailing address</h4>
            </div>
            <div class="col-md-4">
                <label for="mailingAddress">Address</label>
                <input required="true" type="text" name="mailingAddress" class="form-control">
            </div>
            <div class="col-md-2">
                <label for="zipCodeMailingAddress">ZIP Code</label>
                <input required="true" type="text" name="zipCodeMailingAddress" class="form-control number">
            </div>
            <div class="col-md-2">
                <label for="phoneMailing">Phone</label>
                <input type="text" name="phoneMailing" class="form-control number">
            </div>
            <div class="col-md-2">
                <label for="faxMailing">FAX</label>
                <input type="text" name="faxMailing" class="form-control number">
            </div>

            <div class="col-md-2">
                <label for="glnMailing">GLN</label>
                <input type="text" name="glnMailing" class="form-control">
            </div>
        </div>

        <div class="divider"></div>

        <div class="row row-fluid">
            <div class="col-md-12">
                <h4 class="no-padding">Contact Person</h4>
            </div>
            <div class="col-md-5">
                <label for="contactPersonFullName">Full name</label>
                <input required="true" type="text" name="contactPersonFullName" class="form-control">
            </div>
            <div class="col-md-2">
                <label for="phoneContactPerson">Phone</label>
                <input type="text" name="phoneContactPerson" class="form-control number">
            </div>
            <div class="col-md-3">
                <label for="emailContactPerson">Email</label>
                <input type="text" name="emailContactPerson" class="form-control">
            </div>
            <div class="col-md-2">
                <label for="webSiteContactPerson">Web Site</label>
                <input type="text" name="webSiteContactPerson" class="form-control">
            </div>
        </div>

        <div class="row row-fluid">
            <div class="col-md-12">
                <p><input type="checkbox" name="sendOrder" id="sendOrder">Send orders to xml</p>
                <label for="obs">Obs</label>
                <textarea name="obs" class="form-control" cols="5" rows="4"></textarea>
            </div>
        </div>

        <div class="btns-footer">
            <div class="pull-left">
                <button class="btn btn-primary">Import XML</button>
            </div>
            <div class="pull-right">
                <button class="btn btn-primary">Cancel</button>
                <button class="btn btn-success">Save</button>
            </div>
        </div>
    </form>
</div>