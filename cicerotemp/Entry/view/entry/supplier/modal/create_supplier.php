<?php function modalCreateSupplier($country){ ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).on('click', '#create_modal_supplier', function(e){
                e.preventDefault();
                resetForm('#form_supplier');
                $('#modal_create_supplier').modal({
                    show : true
                });
            });

            $(document).on('submit', '#form_supplier', function(e){
                e.preventDefault();
                $.ajax({
                    url : my_url+"Entry/supplier/insert",
                    type: "POST",
                    dataType: 'JSON',
                    data : $('#form_supplier').serialize(),
                    success: function(data, textStatus, jqXHR){
                        if (data.result == 'success'){
                            notification(true);
                        } else {
                            notification(false);
                            console.log(data.message);
                        }
                        $('#modal_create_supplier').modal('hide');
                        reload_table(table_supplier);
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        notification(false);
                        console.log(jqXHR.responseText);
                    }
                });
            });
        });
    </script>
    
    <style type="text/css">
        .row-fluid {
            padding-left: 25px;
            padding-right: 25px;
        }
        .no-padding {
            padding: 0px;
        }
        .no-padding-left {
            padding-left: 0px;
        }
        .divider {
            margin-bottom: 15px;
            margin-top: 15px;
            margin-right: auto;
            margin-left: auto;
            width: 95%;
            height: 2px;
            background-color: #EDECEC;
        }
        .navbar-three li {
            padding-bottom: 0px; 
            margin-bottom: 0px
        }
    </style>
    
    <div class="modal fade" id="modal_create_supplier" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 870px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title translate" id="myModalLabel">Create Supplier</h4>
                </div>
                <form id="form_supplier">
                    <div class="modal-body">
                        <div class="row row-fluid">
                            <div class="col-md-12">
                                <h4 class="no-padding translate">Informations</h4>
                            </div>
                            <div class="col-md-3">
                                <label for="idSupplier" class="translate">ID from Supplier</label>
                                <input type="number" name="idSupplier" id="idSupplier" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label for="yourIdOnSupplier" class="translate">ID on Supplier</label>
                                <input type="text" name="yourIdOnSupplier" class="form-control">
                            </div>
                            <div class="col-md-7">
                                <label for="nameFromSupplier" class="translate">Name from Supplier</label>
                                <input required="true" type="text" name="nameFromSupplier" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="country" class="translate">Country</label>
                                <select name="country" id="country">
                                    <option value="">Select Option</option>
                                    <?php echo $country; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="language" class="translate">Language</label>
                                <input type="text" name="language" class="form-control" required="true">
                            </div>
                            <div class="col-md-3">
                                <label for="accountOnBank" class="translate">Account on bank</label>
                                <input type="text" name="accountOnBank" class="form-control number">
                            </div>
                            <div class="col-md-3">
                                <label for="discountDefault" class="translate">Discount Default</label>
                                <input type="text" name="discountDefault" class="form-control percent">
                            </div>
                            <div class="col-md-3">
                                <label for="phone1" class="translate">Phone</label>
                                <input type="text" name="phone1" class="form-control number">
                            </div>
                            <div class="col-md-3">
                                <label for="email1" class="translate">Email</label>
                                <input type="text" name="email1" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="webSite1" class="translate">Web Site</label>
                                <input type="text" name="webSite1" class="form-control">
                            </div>
                            <div class="col-md-3">
                                <label for="orgNrContactPerson" class="translate">Org. nr</label>
                                <input required="true" type="text" name="orgNrContactPerson" class="form-control">
                            </div>
                        </div>

                        <div class="divider"></div>

                        <div class="row row-fluid">
                            <div class="col-md-12">
                                <h4 class="no-padding translate">Address of establishment</h4>
                            </div>
                            <div class="col-md-10">
                                <label for="addressEstablishment" class="translate">Address</label>
                                <input required="true" type="text" name="addressEstablishment" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label for="zipCodeEstablishment" class="translate">ZIP Code</label>
                                <input required="true" type="text" name="zipCodeEstablishment" class="form-control number">
                            </div>
                        </div>

                        <div class="divider"></div>

                        <div class="row row-fluid">
                            <div class="col-md-12">
                                <h4 class="no-padding translate">Mailing address</h4>
                            </div>
                            <div class="col-md-4">
                                <label for="mailingAddress" class="translate">Address</label>
                                <input required="true" type="text" name="mailingAddress" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label for="zipCodeMailingAddress" class="translate">ZIP Code</label>
                                <input required="true" type="text" name="zipCodeMailingAddress" class="form-control number">
                            </div>
                            <div class="col-md-2">
                                <label for="phoneMailing" class="translate">Phone</label>
                                <input type="text" name="phoneMailing" class="form-control number">
                            </div>
                            <div class="col-md-2">
                                <label for="faxMailing" class="translate">FAX</label>
                                <input type="text" name="faxMailing" class="form-control number">
                            </div>

                            <div class="col-md-2">
                                <label for="glnMailing" class="translate">GLN</label>
                                <input type="text" name="glnMailing" class="form-control">
                            </div>
                        </div>

                        <div class="divider"></div>

                        <div class="row row-fluid">
                            <div class="col-md-12">
                                <h4 class="no-padding translate">Contact Person</h4>
                            </div>
                            <div class="col-md-5">
                                <label for="contactPersonFullName" class="translate">Full name</label>
                                <input required="true" type="text" name="contactPersonFullName" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label for="phoneContactPerson" class="translate">Phone</label>
                                <input type="text" name="phoneContactPerson" class="form-control number">
                            </div>
                            <div class="col-md-3">
                                <label for="emailContactPerson" class="translate">Email</label>
                                <input type="text" name="emailContactPerson" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label for="webSiteContactPerson" class="translate">Web Site</label>
                                <input type="text" name="webSiteContactPerson" class="form-control">
                            </div>
                        </div>

                        <div class="row row-fluid">
                            <div class="col-md-12">
                                <p><input type="checkbox" name="sendOrder" id="sendOrder"> <t class="translate">Send orders to xml</t></p>
                                <label for="obs">Obs</label>
                                <textarea name="obs" class="form-control" cols="5" rows="4"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary translate">Insert</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>