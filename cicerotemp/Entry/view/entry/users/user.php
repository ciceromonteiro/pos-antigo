<?php 
    $init_select = "<option value=''>Select Option</option>";
    $types_select = "";
    $department_select = "";
    $pos_select = "";
    $jobs_select = "";
    $customer_group_select = "";
    
    foreach ($array_answer['types'] as $value) {
        $types_select .= "<option value='".$value->getIdusersType()."'>".$value->getName()."</option>";
    }
    
    foreach ($array_answer['departments'] as $value) {
        $department_select .= "<option value='".$value->getIddepartment()."'>".$value->getName()."</option>";
    }
    
    foreach ($array_answer['pos'] as $value) {
        $pos_select .= "<option value='".$value->getIdpos()."'>".$value->getIdpos()."</option>";
    }
    
    foreach ($array_answer['jobs'] as $value) {
        $jobs_select .= "<option value='".$value->getIdjob()."'>".$value->getName()."</option>";
    }
    
    foreach ($array_answer['customer_group'] as $value) {
        $customer_group_select .= "<option value='".$value->getIdcustomerGp()."'>".$value->getName()."</option>";
    }
    
    $types_select = $init_select.$types_select;
    $department_select = $init_select.$department_select;
    $pos_select = $init_select.$pos_select;
    $jobs_select = $init_select.$jobs_select;
    $customer_group_select = $init_select.$customer_group_select;
?>

<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#users').DataTable( {
            "ajax": {"url": my_url+"Entry/User/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "login" },
                { "data": "fullName" },
                { "data": "status" },
                { "data": "department" },
                { "data": "typeUser" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_user_insert');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_users_edit');
            $.ajax({
                url : my_url+"Entry/User/getUser/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#usersEditModal').modal({show : true});
                        $('#form_users_edit #idUser').val(data.data[0].id);
                        $('#form_users_edit #fullName').val(data.data[0].fullName);
                        $('#form_users_edit #login').val(data.data[0].login);
                        $('#form_users_edit #password').val(data.data[0].password);
                        $("#form_users_edit #customerGp").val(data.data[0].customerGp).prop('selected', true);
                        $("#form_users_edit #job").val(data.data[0].job).prop('selected', true);
                        $("#form_users_edit #type").val(data.data[0].type).prop('selected', true);
                        $("#form_users_edit #status").val(data.data[0].status).prop('selected', true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_user_insert', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/User/insertUser",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_user_insert').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_users_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/User/updateUser",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_users_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#usersEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idUsers : ids};
            deleteItens('Entry/User/deleteUsers', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#users tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#users');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<?php 
    $nav = "users";
    include "nav.php";
?>

<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="users" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('users')"></th>
                        <th style="width: 10px">ID</th>
                        <th class="translate">Login</th>
                        <th class="translate">Full Name</th>
                        <th class="translate">Status</th>
                        <th class="translate">Department</th>
                        <th class="translate">Type User</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Create User</h4>
            </div>
            <form id="form_user_insert">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="fullName" class="label-style translate">Full Name</label>
                            <input type="text" class="form-control" id="fullName" name="fullName" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="login" class="label-style translate">Login</label>
                            <input type="text" class="form-control" id="login" name="login" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="password" class="label-style translate">Password</label>
                            <input type="password" class="form-control" id="password" name="password" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="customerGp" class="label-style translate">Customer Group</label>
                            <select name="customerGp" required="true">
                                <?php echo $customer_group_select ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="job" class="label-style translate">Job</label>
                            <select name="job" required="true">
                                <?php echo $jobs_select ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="type" class="label-style translate">User Type</label>
                            <select name="type" required="true">
                                <?php echo $types_select ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="status" class="label-style translate">Status</label>
                            <select name="status" required="true">
                                <option value="">Select Option</option>
                                <option value="1">Active</option>
                                <option value="2">Blocked</option>
                                <option value="3">Deleted</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="usersEditModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update User</h4>
            </div>
            <form id="form_users_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="idUser" name="idUser" required="true" style="display: none">
                            <label for="fullName" class="label-style translate">Full Name</label>
                            <input type="text" class="form-control" id="fullName" name="fullName" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="login" class="label-style translate">Login</label>
                            <input type="text" class="form-control" id="login" name="login" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="password" class="label-style translate">Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="col-md-6">
                            <label for="customerGp" class="label-style translate">Customer Group</label>
                            <select name="customerGp" id="customerGp" required="true">
                                <?php echo $customer_group_select ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="job" class="label-style translate">Job</label>
                            <select name="job" id="job" required="true">
                                <?php echo $jobs_select ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="type" class="label-style translate">User Type</label>
                            <select name="type" id="type" required="true">
                                <?php echo $types_select ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="status" class="label-style translate">Status</label>
                            <select name="status" id="status" required="true">
                                <option value="">Select Option</option>
                                <option value="1">Active</option>
                                <option value="2">Blocked</option>
                                <option value="3">Deleted</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>