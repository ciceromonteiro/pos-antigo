<?php 
    $j = 0;
    $col1 = "";
    $col2 = "";
    $checkbox_permissions = "";
    foreach ($array_answer['permissions'] as $value) {
        if((count($array_answer['permissions']) / 2) < $j){
            $col1 .= "<p><input type='checkbox' value='".$value->getIdpermission()."' id='".$value->getIdpermission()."' name='checkbox_permissions[]'> ".$value->getDescription()."</p>";
        } else {
            $col2 .= "<p><input type='checkbox' value='".$value->getIdpermission()."' id='".$value->getIdpermission()."' name='checkbox_permissions[]'> ".$value->getDescription()."</p>";
        }
        $j += 1;
    }
    $checkbox_permissions = "<div class='col-md-6'>".$col1."</div>"."<div class='col-md-6'>".$col2."</div>";
    
    $nav = "permissions";
    include "nav.php";
?>

<div class="content">
    <div class="row">
        <ul class="nav nav-tabs navbar-three-min" style="margin: 0px; padding-left: 30px;">
            <li class="active"><a data-toggle="tab" href="#users" class="translate">Permissions by Users</a></li>
            <li><a data-toggle="tab" href="#typeUsers" class="translate">Permissions by Type Users</a></li>
            <li><a data-toggle="tab" href="#allPermissions" class="translate">All Permissions</a></li>
        </ul>
    </div>
    <div class="tab-content">
        <?php
            include 'tabs_permissions/users.php';
            include 'tabs_permissions/typeUsers.php';
            include 'tabs_permissions/allPermissions.php';
        ?>
    </div>
</div>
