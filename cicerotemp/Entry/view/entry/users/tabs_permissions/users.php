<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#permissions').DataTable( {
            "ajax": {"url": my_url+"Entry/User/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "login" },
                { "data": "fullName" },
                { "data": "status" },
                { "data": "department" },
                { "data": "typeUser" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
        
        $(document).on('click', '#update_permission_by_user', function(e){
            e.preventDefault();
            resetForm('#form_update_permission_by_user');
            $.ajax({
                url : my_url+"Entry/User/getPermissionsByUser/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        var values = data.data.permissions;
                        $('#form_update_permission_by_user #idUser').val(data.data.id);
                        for(var i = 0; i < values.length; i++){
                            var idPermission = values[i];
                            $('#form_update_permission_by_user #'+idPermission).prop("checked", true);
                        }
                        $('#edit_modal_permission_by_user').modal({show : true});
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_update_permission_by_user', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/User/updatePermissionsByUser",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_update_permission_by_user').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#edit_modal_permission_by_user').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('click', '#remove_permission_by_user', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idUsers : ids};
            deleteItens('Entry/User/removePermissionsByUser', array_deletes, table);
        });
        
        $('#update_permission_by_user').attr('disabled', true);
        $('#remove_permission_by_user').attr('disabled', true);
        $("#permissions tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#permissions');
            if(itemSelected == 0){
                $('#update_permission_by_user').attr('disabled', true);
                $('#remove_permission_by_user').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_permission_by_user').attr('disabled', false);
                $('#remove_permission_by_user').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_permission_by_user').attr('disabled', true);
                $('#remove_permission_by_user').attr('disabled', false);
            }
        });
    });
</script>

<div id="users" class="tab-pane active">
    <div class="row">
        <div class="col-md-12">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <button id="update_permission_by_user" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit Permissions</t></button>
                    <button id="remove_permission_by_user" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove All Permissions</t></button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="permissions" class="table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('permissions')"></th>
                                <th style="width: 10px">ID</th>
                                <th class="translate">Login</th>
                                <th class="translate">Full Name</th>
                                <th class="translate">Status</th>
                                <th class="translate">Department</th>
                                <th class="translate">Type User</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="edit_modal_permission_by_user" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate">Update Permissions By User</h4>
            </div>
            <form id="form_update_permission_by_user">
                <div class="modal-body">
                    <div class="row" style="height: 300px; overflow-y: scroll">
                        <input type="text" id="idUser" name="idUser" style="display: none">
                        <?php echo $checkbox_permissions ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>