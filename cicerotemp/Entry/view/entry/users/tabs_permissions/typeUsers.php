<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#listPermissionsByUserType').DataTable( {
            "ajax": {"url": my_url+"Entry/User/getAllTypes/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#update_permission_by_users_type', function(e){
            e.preventDefault();
            resetForm('#form_update_permission_by_users_type');
            $.ajax({
                url : my_url+"Entry/User/getPermissionsByUserType/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        var values = data.data.permissions;
                        $('#form_update_permission_by_users_type #idTypeUsers').val(data.data.id);
                        for(var i = 0; i < values.length; i++){
                            var idPermission = values[i];
                            $('#form_update_permission_by_users_type #'+idPermission).prop("checked", true);
                        }
                        $('#edit_modal_permission_by_users_type').modal({show : true});
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('click', '#remove_all_permission_by_users_type', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idTypes : ids};
            deleteItens('Entry/User/removePermissionsByUserType', array_deletes, table);
        });
        
        $(document).on('submit', '#form_update_permission_by_users_type', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/User/updatePermissionsByUserType",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_update_permission_by_users_type').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#edit_modal_permission_by_users_type').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $('#update_permission_by_users_type').attr('disabled', true);
        $('#remove_all_permission_by_users_type').attr('disabled', true);
        $("#listPermissionsByUserType tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#listPermissionsByUserType');
            if(itemSelected == 0){
                $('#update_permission_by_users_type').attr('disabled', true);
                $('#remove_all_permission_by_users_type').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_permission_by_users_type').attr('disabled', false);
                $('#remove_all_permission_by_users_type').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_permission_by_users_type').attr('disabled', true);
                $('#remove_all_permission_by_users_type').attr('disabled', false);
            }
        });
    });
</script>

<div id="typeUsers" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <button id="update_permission_by_users_type" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit Permissions</t></button>
                    <button id="remove_all_permission_by_users_type" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove All Permissions</t></button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="listPermissionsByUserType" class="table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('listPermissionsByUserType')"></th>
                                <th style="width: 10px">ID</th>
                                <th class="translate">Description</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="edit_modal_permission_by_users_type" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Permissions By Type Users</h4>
            </div>
            <form id="form_update_permission_by_users_type">
                <div class="modal-body">
                    <div class="row" style="height: 300px; overflow-y: scroll">
                        <input type="text" id="idTypeUsers" name="idTypeUsers" style="display: none">
                        <?php echo $checkbox_permissions ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>