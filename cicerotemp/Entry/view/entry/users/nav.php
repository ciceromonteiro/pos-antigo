<ul class="navbar-three">
    <li <?php echo ($nav == "users") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/User/index" ?>" class="translate">Users</a>
    </li>
    <li <?php echo ($nav == "permissions") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/User/permissions" ?>" class="translate">Permissions</a>
    </li>
    <li <?php echo ($nav == "type") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/User/Type" ?>" class="translate">User Type</a>
    </li>
    <li <?php echo ($nav == "jobs") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/User/Jobs" ?>" class="translate">Jobs Type</a>
    </li>
</ul>