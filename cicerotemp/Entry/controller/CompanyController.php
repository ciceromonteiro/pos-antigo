<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Feb 13, 2017
 */

class CompanyController {
  
    public function insertCompany($name, $fantasyname, $paymentd, $registernumber, $describesa, $pospos) {
        try {
            $posOb = getEm()->getRepository("Pos")->findBy(array("idpos" =>$pospos));
            $paymentMtdIdpaymentMtd = getEm()->getRepository('PaymentMtd')->findBy(array("idpaymentMtd" => $paymentd));
            $Company = new Company();
            $Company->setInfo($describesa);
            $Company->setName($name);
            $Company->setPospos($posOb[0]);
            $Company->setFantasyName($fantasyname);
            $Company->setRegisterNumber($registernumber);
            $Company->setPaymentMtdIdpaymentMtd($paymentMtdIdpaymentMtd[0]);
            $Company->setActive(1);
            getEm()->persist($Company);
            getEm()->flush();
            AuthenticationController::insertLog('create', 'company', $name);

            $idcompany = $Company->getIdcompany();
            echo $idcompany;
        } catch (Exception $e) {
            print $e;
            exit(1);
        }
    }
    
    public function index(){
        $navbar = "Entry|Company";
        $PaymentMtd = getEm()->getRepository('PaymentMtd')->findBy(array("active" => 1));
        $array_answer = array (
            "payment" => $PaymentMtd
        );
        GenericController::template("Entry", "company","index", $navbar, $array_answer, 345);
    }
    
    public static function getAll(){
        try{
            $company = getEm()->getRepository('Company')->findBy(array("active" => 1));
            $data = array ();
            foreach ($company as $value){
                if($value->getPaymentMtdIdpaymentMtd()){
                    $payment = $value->getPaymentMtdIdpaymentMtd()->getName();
                } else {
                    $payment = 0;
                }
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdCompany().'">',
                    "id" => $value->getIdCompany(),
                    "name" => $value->getName(),
                    "name_fantasy" => $value->getFantasyName(),
                    "register_number" => $value->getRegisterNumber(),
                    "info" => $value->getInfo(),
                    "description" => $value->getName(),
                    "payment_mtd" => $payment
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function getCompany(){
        if(isset($_POST['id'])){
            try {
                $company = getEm()->getRepository('Company')->findBy(array("idcompany" => $_POST['id']));
                $mysqlData = array ();
                foreach ($company as $value){
                    if($value->getPaymentMtdIdpaymentMtd()){
                        $payment = $value->getPaymentMtdIdpaymentMtd()->getIdpaymentMtd();
                    } else {
                        $payment = "";
                    }
                    $dat = array (
                        "checkbox" => '<input type="checkbox" data-id="'.$value->getIdCompany().'">',
                        "id" => $value->getIdCompany(),
                        "name" => $value->getName(),
                        "name_fantasy" => $value->getFantasyName(),
                        "register_number" => $value->getRegisterNumber(),
                        "info" => $value->getInfo(),
                        "description" => $value->getName(),
                        "payment_mtd" => $payment
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function update($id = false){
        try {
            $company = getEm()->getRepository('Company')->findBy(array("idcompany" => $id));
            $pamentMtd = getEm()->getRepository('PaymentMtd')->findBy(array("idpaymentMtd" => $_POST['paymentMtd']));
            $company[0]->setName($_POST['nameCompany']);
            $company[0]->setPospos(AuthenticationController::getPos());
            $company[0]->setFantasyName($_POST['nameFantasy']);
            $company[0]->setRegisterNumber($_POST['registerNumber']);
            $company[0]->setInfo($_POST['infoCompany']);
            $company[0]->setPaymentMtdIdpaymentMtd($pamentMtd[0]);
            $company[0]->setDateUpdate(new DateTime());
            $company[0]->setActive(true);
            getEm()->persist($company[0]);
            getEm()->flush();
            
            AuthenticationController::insertLog('update', 'company', $_POST['nameCompany']);

            $result  = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function insert(){
        if(isset($_POST['nameCompany'])){
            try {
                $pamentMtd = getEm()->getRepository('PaymentMtd')->findBy(array("idpaymentMtd" => $_POST['paymentMtd']));
                $company = new Company();
                $company->setName($_POST['nameCompany']);
                $company->setPospos(AuthenticationController::getPos());
                $company->setFantasyName($_POST['nameFantasy']);
                $company->setRegisterNumber($_POST['registerNumber']);
                $company->setInfo($_POST['infoCompany']);
                $company->setPaymentMtdIdpaymentMtd($pamentMtd[0]);
                $company->setActive(true);
                getEm()->persist($company);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'company', $_POST['nameCompany']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function deleteCompanys(){
        if($_POST['idCompanys']){
            try{
                $ids = $_POST['idCompanys'];
                for($i=0; $i < count($ids); $i++){
                    $company = getEm()->getRepository("Company")->findOneBy(array("idcompany" => $ids[$i]));
                    $company->setActive('3');
                    $company->setDateDelete(new DateTime());
                    getEm()->persist($company);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'company', $company->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
}
