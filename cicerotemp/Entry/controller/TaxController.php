<?php
/**
 * Created by PhpStorm.
 * User: rocha
 * Date: 11/1/16
 * Time: 11:10 AM
 */

class TaxController {

    public static function index(){
        $navbar = "Entry|tax";
        $array_answer = array ("");
        GenericController::template("Entry", "tax","index", $navbar, $array_answer, 336);
    }

    public function getAll(){
        try{
            $tax = getEm()->getRepository('Tax')->findBy(array("active" => 1));
            $data = array ();
            foreach ($tax as $value){
                if($value->getPercent() != null || $value->getPercent() != ''){
                    $percent = $value->getPercent().'%';
                } else {
                    $percent = "";
                }
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdTax().'" data-name="'.$value->getName().'">',
                    "id" => $value->getIdTax(),
                    "name" => $value->getName(),
                    "percent" => $percent,
                    "mvaAccount" => $value->getMvaAccount(),
                    "accountRelapse" => $value->getAccountRelapse(),
                    "externalTaxCode" => $value->getExternalTaxCode(),
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function insert(){
        if(isset($_POST['nameTax'])){
            try {
                $percent_1 = str_replace('%', '', $_POST['percent']);
                $percent_2 = str_replace('.', '', $percent_1);
                $percent = str_replace(',', '.', $percent_2);
                
                $tax = new Tax();
                $tax->setName($_POST['nameTax']);
                $tax->setPercent($percent);
                $tax->setMvaAccount($_POST['mva_account']);
                $tax->setAccountRelapse($_POST['account_relapse']);
                $tax->setExternalTaxCode($_POST['external_tax_code']);
                $tax->setActive('1');
                getEm()->persist($tax);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'tax', $_POST['nameTax']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getTax(){
        if(isset($_POST['id'])){
            try {
                $tax = getEm()->getRepository('Tax')->findBy(array("idtax" => $_POST['id']));
                $mysqlData = array ();
                foreach ($tax as $value){
                    $dat = array (
                        "id" => $value->getIdTax(),
                        "name" => $value->getName(),
                        "percent" => $value->getPercent(),
                        "mvaAccount" => $value->getMvaAccount(),
                        "accountRelapse" => $value->getAccountRelapse(),
                        "externalTaxCode" => $value->getExternalTaxCode()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function update(){
        if(isset($_POST['nameTax']) && isset($_POST['idTax'])){
            try {
                $tax = getEm()->getRepository('Tax')->findOneBy(array("idtax" => $_POST['idTax']));
                $percent_1 = str_replace('%', '', $_POST['percent']);
                $percent_2 = str_replace('.', '', $percent_1);
                $percent = str_replace(',', '.', $percent_2);
                                
                $tax->setName($_POST['nameTax']);
                $tax->setPercent($percent);
                $tax->setMvaAccount(isset($_POST['mva_account']));
                $tax->setAccountRelapse(isset($_POST['account_relapse']));
                $tax->setExternalTaxCode(isset($_POST['external_tax_code']));
                $tax->setDateUpdate(new Datetime());

                getEm()->persist($tax);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'tax', $tax->getName());

                $result  = 'success';
                $message = 'query success';
                $data = '';
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function delete(){
        if($_POST['idTaxs']){
            try{
                $ids = $_POST['idTaxs'];
                for($i=0; $i < count($ids); $i++){
                    $tax = getEm()->getRepository("Tax")->findOneBy(array("idtax" => $ids[$i]));
                    $tax->setActive(3);
                    $tax->setDateDelete(new DateTime());
                    getEm()->persist($tax);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'tax', $tax->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
}