<?php
/**
 * @author Claudio da Cruz Silva Junior>
 * @version 1.0.0
 * @abstract file created in date september 26, 2017
 */
class InvoiceController {
    /**
     * Index call Invoice
     * 
     * @access public
     * @return void call preview
     */
    public function index() {
        $navbar = "Entry|invoice";
        $orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $Invoice = getEm()->getRepository('Invoice')->findBy(array("active" => 1));
        /*$orderData = array();
        $teste = 0;
        foreach ($orders as $order) {
            $teste = 0;
            foreach ($Invoice as $invoice){
                if($invoice->getOrderorder()->getIdorder() == $order->getIdorder()){
                    $teste++;
                }

            }
            if($teste == 0){
                array_push($orderData, $order);
                $teste = 0;
            }
        }*/



        $invoiceTemplate = getEm()->getRepository('InvoiceTmpt')->findBy(array("active" => 1));



        $array_answer = array(
            "orders" => $orders,
            "invoiceTemplate" => $invoiceTemplate

            );
        GenericController::template("Entry", "invoice", "index", $navbar, $array_answer, 320);
    }

    public function default_alerts() {
        $navbar = "Entry|invoice";
        $array_answer = null;
        GenericController::template("Entry", "invoice", "default_alerts", $navbar, $array_answer, 327);
    }

    public function printer() {
        $navbar = "Entry|invoice";
        $array_answer = null;
        GenericController::template("Entry", "invoice", "printer", $navbar, $array_answer, 323);
    }
    /**
     * Get all invoice by active or by filter with the filterValue (maximun value) and filterDate (date) 
     * @param string $filterValue
     * @param string filterDate
     * @return string json_data 
     */
    public function listInvoice($filterValue, $filterDate){

      if($filterDate != "0"){
            $filterDate = base64_decode($filterDate);
            $filterDate = new DateTime($filterDate);
            $filterDate = $filterDate->format('d/m/Y');

        }

        if($filterValue != "0"){
            $filterValue = (float)$filterValue;

        }

        $Invoice = getEm()->getRepository('Invoice')->findBy(array("active" => 1));
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
        $idCustomer = 0;
        $nameCustomer = null;
        $nameEmployee = null;
        $liquidValue = 0;
        $data = array ();
try{
        if($Invoice != null){
            foreach ($Invoice as $invoice) {
                if($Orders != null){
                    $idCustomer = 0;
                    $nameCustomer = null;
                    $nameEmployee = null;
                    $liquidValue = 0;
                    $discount = 0;
                    $valueGross = 0;
                    foreach ($Orders as $order) {
                        if($invoice->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $idCustomer = $order->getPersonperson()->getIdperson();
                            if($idCustomer != NULL){

                            }
                            $nameCustomer = $order->getPersonperson()->getName();
                            $nameEmployee = $order->getUsersusers()->getPersonperson()->getName();
                            
                            foreach ($OrderLine as $orderLine) {
                                if($order->getIdorder() == $orderLine->getOrderorder()->getIdorder()){
                                    $valueGross = $valueGross + $orderLine->getActualValue();
                                    $discount = $discount + $orderLine->getDiscount();
                                }
                            }
                            $qtyParcelsInvoice = 0;
                            foreach ($Invoice as $value) {
                                if($value->getOrderorder()->getIdorder() == $order->getIdorder()){
                                    $qtyParcelsInvoice++;
                                }
                            }

                             $discount = $discount + $order->getDiscount();
                             $liquidValue = $valueGross - $discount;

                             $liquidValue = $liquidValue/$qtyParcelsInvoice;
                             $liquidValue = round($liquidValue, 2);


                             $date = $invoice->getDateDue()->format('d/m/Y');
                             
                                if($filterValue != "0" && $filterDate != "0"){
                                    if($filterValue >= $liquidValue && $filterDate == $date){
                                        $liquidValue = number_format($liquidValue, 2, ',', '.');

                                        $dat = array (
                                            "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                            "id" => $invoice->getIdinvoice(),
                                            "dataInvoice" => $date,
                                            "idCustomer" => $idCustomer,
                                            "nameCustomer" => $nameCustomer,
                                            "nameEmployee" => $nameEmployee,
                                            "liquidValue" => $liquidValue
                                        );
                                        array_push($data, $dat); 
                                        }
                                }else if($filterValue != "0" && $filterDate == "0"){     
                                    if($filterValue >= $liquidValue){
                                        $liquidValue = number_format($liquidValue, 2, ',', '.');

                                        $dat = array (
                                            "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                            "id" => $invoice->getIdinvoice(),
                                            "dataInvoice" => $date,
                                            "idCustomer" => $idCustomer,
                                            "nameCustomer" => $nameCustomer,
                                            "nameEmployee" => $nameEmployee,
                                            "liquidValue" => $liquidValue
                                        );
                                        array_push($data, $dat); 
                                        }  
                                }else if($filterValue == "0" && $filterDate != "0"){ 
                                    if($filterDate == $date){
                                        $liquidValue = number_format($liquidValue, 2, ',', '.');

                                        $dat = array (
                                            "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                            "id" => $invoice->getIdinvoice(),
                                            "dataInvoice" => $date,
                                            "idCustomer" => $idCustomer,
                                            "nameCustomer" => $nameCustomer,
                                            "nameEmployee" => $nameEmployee,
                                            "liquidValue" => $liquidValue
                                        );
                                        array_push($data, $dat); 
                                        }          
                                }else{
                                    $liquidValue = number_format($liquidValue, 2, ',', '.');
                                     $date = array();
                                     $date = $invoice->getDateDue()->format('d/m/Y');
                                   $dat = array (
                                    "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                    "id" => $invoice->getIdinvoice(),
                                    "dataInvoice" => $date,
                                    "idCustomer" => $idCustomer,
                                    "nameCustomer" => $nameCustomer,
                                    "nameEmployee" => $nameEmployee,
                                    "liquidValue" => $liquidValue
                                );
                                array_push($data, $dat); 
                                }
                                     
                        }
                    }
                }

            }
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

    }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;

}
/**
     * Get the invoice with date deadline late, by active or by filter with the filterValue (maximun value) and filterDate (date) 
     * @param string $filterValue
     * @param string filterDate
     * @return string json_data 
     */
public function listInvoiceCompanyBankruptcy($filterValue, $filterDate){
    if($filterDate != "0"){
            $filterDate = base64_decode($filterDate);
            $filterDate = new DateTime($filterDate);
            $filterDate = $filterDate->format('d/m/Y');

        }

        if($filterValue != "0"){
            $filterValue = (float)$filterValue;

        }
        $Invoice = getEm()->getRepository('Invoice')->findBy(array("active" => 1));
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
        $idCustomer = 0;
        $nameCustomer = null;
        $nameEmployee = null;
        $liquidValue = 0;
        $data = array ();

        $todayDate = new DateTime();
        $todayYear = $todayDate->format('Y');
        $todayDay = $todayDate->format('d');
        $todayMonth = $todayDate->format('m');

try{
        if($Invoice != null){
            foreach ($Invoice as $invoice) {
            $InvoiceDate = $invoice->getDateDue();
                if($InvoiceDate->format('Y') < $todayYear || ($InvoiceDate->format('Y') ==  $todayYear && $InvoiceDate->format('m') <  $todayMonth) || ($InvoiceDate->format('Y') ==  $todayYear && $InvoiceDate->format('m') == $todayMonth && $InvoiceDate->format('d') < $todayDay)){
                    if($Orders != null){
                        $idCustomer = 0;
                        $nameCustomer = null;
                        $nameEmployee = null;
                        $liquidValue = 0;
                        $discount = 0;
                        $valueGross = 0;
                        foreach ($Orders as $order) {
                            if($invoice->getOrderorder()->getIdorder() == $order->getIdorder()){
                                $idCustomer = $order->getPersonperson()->getIdperson();
                                $nameCustomer = $order->getPersonperson()->getName();
                                $nameEmployee = $order->getUsersusers()->getPersonperson()->getName();
                                
                                foreach ($OrderLine as $orderLine) {
                                    if($order->getIdorder() == $orderLine->getOrderorder()->getIdorder()){
                                        $valueGross = $valueGross + $orderLine->getActualValue();
                                        $discount = $discount + $orderLine->getDiscount();
                                    }
                                }

                                 $discount = $discount + $order->getDiscount();
                                 $liquidValue = $valueGross - $discount;
                                   $date = $invoice->getDateDue()->format('d/m/Y');
                             
                                if($filterValue != "0" && $filterDate != "0"){
                                    if($filterValue >= $liquidValue && $filterDate == $date){
                                        $liquidValue = number_format($liquidValue, 2, ',', '.');

                                        $dat = array (
                                            "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                            "id" => $invoice->getIdinvoice(),
                                            "dataInvoice" => $date,
                                            "idCustomer" => $idCustomer,
                                            "nameCustomer" => $nameCustomer,
                                            "nameEmployee" => $nameEmployee,
                                            "liquidValue" => $liquidValue
                                        );
                                        array_push($data, $dat); 
                                        }
                                }else if($filterValue != "0" && $filterDate == "0"){     
                                    if($filterValue >= $liquidValue){
                                        $liquidValue = number_format($liquidValue, 2, ',', '.');

                                        $dat = array (
                                            "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                            "id" => $invoice->getIdinvoice(),
                                            "dataInvoice" => $date,
                                            "idCustomer" => $idCustomer,
                                            "nameCustomer" => $nameCustomer,
                                            "nameEmployee" => $nameEmployee,
                                            "liquidValue" => $liquidValue
                                        );
                                        array_push($data, $dat); 
                                        }  
                                }else if($filterValue == "0" && $filterDate != "0"){ 
                                    if($filterDate == $date){
                                        $liquidValue = number_format($liquidValue, 2, ',', '.');

                                        $dat = array (
                                            "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                            "id" => $invoice->getIdinvoice(),
                                            "dataInvoice" => $date,
                                            "idCustomer" => $idCustomer,
                                            "nameCustomer" => $nameCustomer,
                                            "nameEmployee" => $nameEmployee,
                                            "liquidValue" => $liquidValue
                                        );
                                        array_push($data, $dat); 
                                        }          
                                }else{
                                    $liquidValue = number_format($liquidValue, 2, ',', '.');
                                     $date = array();
                                     $date = $invoice->getDateDue()->format('d/m/Y');
                                   $dat = array (
                                    "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                    "id" => $invoice->getIdinvoice(),
                                    "dataInvoice" => $date,
                                    "idCustomer" => $idCustomer,
                                    "nameCustomer" => $nameCustomer,
                                    "nameEmployee" => $nameEmployee,
                                    "liquidValue" => $liquidValue
                                );
                                array_push($data, $dat); 
                                }
                            }
                        }
                    }

                }
            }
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

    }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;

}
/**
     * Get the invoice with status of the order in open, by active or by filter with the filterValue (maximun value) and filterDate (date) 
     * @param string $filterValue
     * @param string filterDate
     * @return string json_data 
     */
 public function listForOpenTickets($filterValue, $filterDate){
    if($filterDate != "0"){
            $filterDate = base64_decode($filterDate);
            $filterDate = new DateTime($filterDate);
            $filterDate = $filterDate->format('d/m/Y');

        }

        if($filterValue != "0"){
            $filterValue = (float)$filterValue;

        }
        $Invoice = getEm()->getRepository('Invoice')->findBy(array("active" => 1));
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
        $idCustomer = 0;
        $nameCustomer = null;
        $nameEmployee = null;
        $liquidValue = 0;
        $data = array ();
        try{
            if($Invoice != null){
                foreach ($Invoice as $invoice) {
                    if($Orders != null){
                        $idCustomer = 0;
                        $nameCustomer = null;
                        $nameEmployee = null;
                        $liquidValue = 0;
                        $discount = 0;
                        $valueGross = 0;
                        foreach ($Orders as $order) {
                            if($order->getStatus() == 0){

                            
                            if($invoice->getOrderorder()->getIdorder() == $order->getIdorder()){
                                $idCustomer = $order->getPersonperson()->getIdperson();
                                $nameCustomer = $order->getPersonperson()->getName();
                                $nameEmployee = $order->getUsersusers()->getPersonperson()->getName();
                                
                                foreach ($OrderLine as $orderLine) {
                                    if($order->getIdorder() == $orderLine->getOrderorder()->getIdorder()){
                                        $valueGross = $valueGross + $orderLine->getActualValue();
                                        $discount = $discount + $orderLine->getDiscount();
                                    }
                                }

                                 $discount = $discount + $order->getDiscount();
                                 $liquidValue = $valueGross - $discount;
                                 $date = $invoice->getDateDue()->format('d/m/Y');
                                 
                                    if($filterValue != "0" && $filterDate != "0"){
                                        if($filterValue >= $liquidValue && $filterDate == $date){
                                            $liquidValue = number_format($liquidValue, 2, ',', '.');

                                            $dat = array (
                                                "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                                "id" => $invoice->getIdinvoice(),
                                                "dataInvoice" => $date,
                                                "idCustomer" => $idCustomer,
                                                "nameCustomer" => $nameCustomer,
                                                "nameEmployee" => $nameEmployee,
                                                "liquidValue" => $liquidValue
                                            );
                                            array_push($data, $dat); 
                                            }
                                    }else if($filterValue != "0" && $filterDate == "0"){     
                                        if($filterValue >= $liquidValue){
                                            $liquidValue = number_format($liquidValue, 2, ',', '.');

                                            $dat = array (
                                                "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                                "id" => $invoice->getIdinvoice(),
                                                "dataInvoice" => $date,
                                                "idCustomer" => $idCustomer,
                                                "nameCustomer" => $nameCustomer,
                                                "nameEmployee" => $nameEmployee,
                                                "liquidValue" => $liquidValue
                                            );
                                            array_push($data, $dat); 
                                            }  
                                    }else if($filterValue == "0" && $filterDate != "0"){ 
                                        if($filterDate == $date){
                                            $liquidValue = number_format($liquidValue, 2, ',', '.');

                                            $dat = array (
                                                "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                                "id" => $invoice->getIdinvoice(),
                                                "dataInvoice" => $date,
                                                "idCustomer" => $idCustomer,
                                                "nameCustomer" => $nameCustomer,
                                                "nameEmployee" => $nameEmployee,
                                                "liquidValue" => $liquidValue
                                            );
                                            array_push($data, $dat); 
                                            }          
                                    }else{
                                        $liquidValue = number_format($liquidValue, 2, ',', '.');
                                         $date = array();
                                         $date = $invoice->getDateDue()->format('d/m/Y');
                                       $dat = array (
                                        "checkbox" => '<input type="checkbox" data-id="'.$invoice->getIdinvoice().'" data-name="'.$invoice->getIdinvoice().'">',    
                                        "id" => $invoice->getIdinvoice(),
                                        "dataInvoice" => $date,
                                        "idCustomer" => $idCustomer,
                                        "nameCustomer" => $nameCustomer,
                                        "nameEmployee" => $nameEmployee,
                                        "liquidValue" => $liquidValue
                                    );
                                    array_push($data, $dat); 
                                    }
                            }
                        }
                    }
                }

            }
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

    }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

    $data = array(
        "result"  => $result,
        "message" => $message,
        "data"    => $mysqlData
    );
    $json_data = json_encode($data);
    print $json_data;

}
/**
     * Get the tickets_print of the database and generate the PDFs of each ticket storing in archive zip in folder ../data/pdfTicket
     * @return string json_data 
     */
public function generatePdf(){
    include('../libraries/mPDF/mpdf.php');
    $dat = null;
    $TicketPrint = getEm()->getRepository('TicketPrint')->findAll();
    $LogoTmpt = getEm()->getRepository('LogoTmpt')->findBy(array('active' => 1));

    if(file_exists('../data/pdfTicket/TicketsPdfs.zip')){
        unlink('../data/pdfTicket/TicketsPdfs.zip');
    }

    if($LogoTmpt != null){
        $logo = $LogoTmpt[0]->getImgTmpt();
        if($logo == "../../../assets/images/upload.png"){
            $logo = "../assets/images/upload.png";
        }
    }else{
       $logo = "../assets/images/upload.png";
    }

  try{    
    if($TicketPrint != null){
    foreach ($TicketPrint as $ticketPrint) {
        $dat = "Com dados";
        $name = $ticketPrint->getName();
        $content = null;
        $content = ' <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="utf-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1">
                            <title>EasyDrift - Point of Sales</title>

                            <link href="../assets/style.css" rel="stylesheet">
                        </head>
                        <body>
                         <div style="margin-left: 170px; margin-right: 170px;"> 
                                <div style="background: #FFFFFF; box-shadow: 0px 2px 38px 0px rgba(0,0,0,0.07); border-radius: 6px; padding-top: 25px; border: 1px solid #aaa" >
                                    <div style="margin-left: 20px; margin: 5px;">
                                         <div style="float: left;">
                                            <div style="margin-left: 20px">
                                                <img width="50" id="image_preview" src='.$logo.' alt="your image"/>
                                            </div>
                                         </div> 
                                        
                                    </div>
                                      
                                     <div style="margin-left: 10px; margin-right: 10px; margin-bottom: 10px;">
                                        <div class="row" style="font-size: 15px; text-align: center">'
                                        ;
        if($ticketPrint->getTextPrint() != NULL){

            $structure = $ticketPrint->getTextPrint();
            $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?> <root>";
            $xml .= $structure;
            $xml .= "</root>";
            $file = @fopen('../data/pdfTicket/'.$name.'.xml', 'w+');
            fwrite($file, $xml);
            fclose($file);
            $xml = simplexml_load_file('../data/pdfTicket/'.$name.'.xml');
            unlink('../data/pdfTicket/'.$name.'.xml');
            $arrData = json_encode($xml);
            $arrData = json_decode($arrData);
            $arrData = json_decode(json_encode($arrData), True);
  
            foreach ($arrData as $arr) {

                foreach ($arr as $bodyTag) {
                    foreach (array_keys($bodyTag) as $tags) {
                        if($tags == "dado1"){

                            if(count($bodyTag[$tags]) >= 2){ 
                                foreach ($bodyTag[$tags] as $tagText) {  
                                                 
                                    if(key($tagText) == 'JUSTIFY_LEFT'){
                                       $content .= '
                                                    <div class="col-md-12" style="text-align: left;"> 
                                                        <p>'.$tagText['JUSTIFY_LEFT'].'</p> 
                                                    </div>';
                                    }else if(key($tagText) == 'JUSTIFY_CENTER'){
                                       $content .= 
                                                    '
                                                    <div class="col-md-12" style="text-align: center;"> 
                                                        <p>'.$tagText['JUSTIFY_CENTER'].'</p> 
                                                    </div>';
                                    }else if(key($tagText) == 'JUSTIFY_RIGHT'){
                                       $content .= 
                                                    '
                                                    <div class="col-md-12" style="text-align: right;"> 
                                                        <p style="float: right;">'.$tagText['JUSTIFY_RIGHT'].'</p> 
                                                    </div>';
                                    }
                                }

                            }else{
                                if(key($bodyTag[$tags]) == 'JUSTIFY_LEFT'){
                                        $content .= 
                                                    '
                                                    <div class="col-md-12" style="text-align: left;"> 
                                                        <p style="float: left;">'.$bodyTag[$tags]['JUSTIFY_LEFT'].'</p> 
                                                    </div>';
                                }else if(key($bodyTag[$tags]) == 'JUSTIFY_CENTER'){
                                        $content .= 
                                                    '
                                                    <div class="col-md-12" style="text-align: center;">  
                                                        <p>'.$bodyTag[$tags]['JUSTIFY_CENTER'].'</p> 
                                                     </div>';
                                }else if(key($bodyTag[$tags]) == 'JUSTIFY_RIGHT'){
                                         $content .= 
                                                    '
                                                    <div class="col-md-12" style="text-align: right;">  
                                                        <p style="float: right;">'.$bodyTag[$tags]['JUSTIFY_RIGHT'].'</p> 
                                                     </div>';
                                }
                            }
                        }else if($tags == 'line'){
                                        $content .= '   
                                                    <div class="col-md-12"> 
                                                        <p>______________________________________</p> 
                                                    </div>';
                        }else if($tags == 'code1'){
                           // var_dump($bodyTag[$tags]);
                            require_once('../libraries/barCode/barcode.inc.php');
                            new barCodeGenrator($bodyTag[$tags],1,'../data/pdfTicket/codeBarImg/'.$name.'.gif', 200, 100, true);
                                        $content .= '   
                                                    <div class="col-md-12">
                                                        <img width="80" id="image_preview" src="../data/pdfTicket/codeBarImg/'.$name.'.gif" alt="your image"/>
                                                    </div>';

                        }

                    }
                    
                }
            }

        }

        $content .= '                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </body>
                        </html>    ';

     $mpdf = new mPDF('c');
     $css = file_get_contents("../assets/style.css");
     $mpdf->SetDisplayMode('real');
     $mpdf->WriteHTML($css,1);
     $mpdf->WriteHTML($content);
     $mpdf->Output('../data/pdfTicket/'.$name.'.pdf', 'F');
    
     $zip = new ZipArchive();
 
        if( $zip->open( '../data/pdfTicket/TicketsPdfs.zip', ZipArchive::CREATE )  === true){
            $zip->deleteName($name.'.pdf');
            $zip->deleteName('semArquivo.pdf');
             
            $zip->addFile('../data/pdfTicket/'.$name.'.pdf' , $name.'.pdf');
             
            $zip->close();
        }
    if(file_exists('../data/pdfTicket/'.$name.'.pdf')){
        unlink('../data/pdfTicket/'.$name.'.pdf');
    }
    if(file_exists('../data/pdfTicket/codeBarImg/'.$name.'.gif')){
       unlink('../data/pdfTicket/codeBarImg/'.$name.'.gif');
    }     
     
    }
}else{
   $dat = "Sem dados";
}
    $result = "success";
    $message = "query success";
    $mysqlData = $dat;
}catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;

} 
/**
     * Get the string generate by thermal printer and saved in database 
     * @param string $ticketPrint
     * @return string json_data 
     */
public function saveTicketPrinter($ticketPrint){
    try{
    $name = time(); 
    $newTicketPrint = new TicketPrint();
    $newTicketPrint->setName($name);
    $newTicketPrint->setTextPrint($ticketPrint);
    getEm()->persist($newTicketPrint);
    getEm()->flush();
     $result = "success";
        $message = "query success";
        $mysqlData = "";

    }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
}

/**
 * Get all default alerts of invoice
 * @return string json_data 
 */
public static function getAll(){
        try{
            $warningsInCaseOfDelay = getEm()->getRepository('WarningsInCaseOfDelay')->findBy(array("active" => 1));
            $data = array ();
            foreach ($warningsInCaseOfDelay as $value){
                if($value->getSendCopy() != 0){
                    $sendCopy = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $sendCopy = '';
                }
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdWarningsInCaseOfDelay().'">',
                    "id" => $value->getIdWarningsInCaseOfDelay(),
                    "sendAfter" => $value->getSendAfter(),
                    "sendCopy" => $sendCopy,
                    "messageTitle" => $value->getMessageTitle(),
                    "message" => $value->getMessage()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

/**
 * Insert the new default alert of invoice
 * @return string json_data 
 */
public function insertDefaultAlert(){
    try{
        if(isset($_POST['send_copy']) && $_POST['send_copy'] == 'on'){
            $sendCopy = 1;
        }else{
            $sendCopy = 0;
        }
        $warningsInCaseOfDelay = new WarningsInCaseOfDelay();
        $warningsInCaseOfDelay->setSendAfter($_POST['send_after']);
        $warningsInCaseOfDelay->setSendCopy($sendCopy);
        $warningsInCaseOfDelay->setMessageTitle($_POST['message_title']);
        $warningsInCaseOfDelay->setMessage($_POST['message']);
        $warningsInCaseOfDelay->setDateCreate(new DateTime());
        $warningsInCaseOfDelay->setActive(1);
        getEm()->persist($warningsInCaseOfDelay);
        getEm()->flush();
        $result = "success";
        $message = "query success";
        $mysqlData = "";

    }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
}

/**
 * Get one default alert with id equals the $id 
 * @param string $id
 * @return string json_data 
 */
public static function getDefaultAlert($id){
    if(isset($id)){
        try{
            $warningsInCaseOfDelay = getEm()->getRepository('WarningsInCaseOfDelay')->findOneBy(array("idWarningsInCaseOfDelay"=> $id,"active" => 1));
            $data = array ();
                $dat = array (
                    "id" => $warningsInCaseOfDelay->getIdWarningsInCaseOfDelay(),
                    "sendAfter" => $warningsInCaseOfDelay->getSendAfter(),
                    "sendCopy" => $warningsInCaseOfDelay->getSendCopy(),
                    "messageTitle" => $warningsInCaseOfDelay->getMessageTitle(),
                    "message" => $warningsInCaseOfDelay->getMessage()
                );
                array_push($data, $dat);
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
}

/**
 * Update de default Alert of invoice
 * @return string json_data 
 */
public function updateDefaultAlert(){
    try{
        if(isset($_POST['send_copy']) && $_POST['send_copy'] == 'on'){
            $sendCopy = 1;
        }else{
            $sendCopy = 0;
        }
        $warningsInCaseOfDelay = getEm()->getRepository('WarningsInCaseOfDelay')->findOneBy(array("idWarningsInCaseOfDelay"=> $_POST['idDefaultAlert'],"active" => 1));
        $warningsInCaseOfDelay->setSendAfter($_POST['send_after']);
        $warningsInCaseOfDelay->setSendCopy($sendCopy);
        $warningsInCaseOfDelay->setMessageTitle($_POST['message_title']);
        $warningsInCaseOfDelay->setMessage($_POST['message']);
        $warningsInCaseOfDelay->setDateUpdate(new DateTime());
        getEm()->persist($warningsInCaseOfDelay);
        getEm()->flush();
        $result = "success";
        $message = "query success";
        $mysqlData = "";

    }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
}
/**
 * Delete the defaults Alerts selected, set th active for 3
 * @return string json_data 
 */
    public function deleteDefaultAlert(){
        if($_POST['idDefaultAlert']){
            try{
                $ids = $_POST['idDefaultAlert'];
                for($i=0; $i < count($ids); $i++){
                    $warningsInCaseOfDelay = getEm()->getRepository("WarningsInCaseOfDelay")->findOneBy(array("idWarningsInCaseOfDelay" => $ids[$i]));
                    $warningsInCaseOfDelay->setActive('3');
                    $warningsInCaseOfDelay->setDateDelete(new DateTime());
                    getEm()->persist($warningsInCaseOfDelay);
                    getEm()->flush();
                    //AuthenticationController::insertLog('delete', 'ProductUnit', $_POST['idUnit']);
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    /**
     * Update de settings of print of invoices
     * @return string json_data 
     */
    public function insertPrinter($text_closing, $size_font, $description_printed, $number_copies, $number_places, $print_drawing, $enter_customer, $change_location, $print_taxes, $not_print_project, $use_default_printer){
        try{
            $text_closing = base64_decode($text_closing);
            $size_font = base64_decode($size_font);
            $description_printed = base64_decode($description_printed);
            $number_copies = base64_decode($number_copies);
            $number_places = base64_decode($number_places);
            $print_drawing = base64_decode($print_drawing);
            $enter_customer = base64_decode($enter_customer);
            $change_location = base64_decode($change_location);
            $print_taxes = base64_decode($print_taxes);
            $not_print_project = base64_decode($not_print_project);
            $use_default_printer = base64_decode($use_default_printer);

            if($text_closing == "0"){
                $text_closing = NULL;
            }
            if($size_font == "0"){
                $size_font = NULL;
            }
            if($description_printed == "0"){
                $description_printed = NULL;
            }
            if($number_copies == "0"){
                $number_copies = NULL;
            }
            if($number_places == "0"){
                $number_places = NULL;
            }
            if($print_drawing == "true"){
                $print_drawing = 1;
            }else{
                $print_drawing = 0;
            }

            if($enter_customer == "true"){
                $enter_customer = 1;
            }else{
                $enter_customer = 0;
            }

            if($change_location == "true"){
                $change_location = 1;
            }else{
                $change_location = 0;
            }

            if($print_taxes == "true"){
                $print_taxes = 1;
            }else{
                $print_taxes = 0;
            }

            if($not_print_project == "true"){
                $not_print_project = 1;
            }else{
                $not_print_project = 0;
            }

            if($use_default_printer == "true"){
                $use_default_printer = 1;
            }else{
                $use_default_printer = 0;
            }

            $invoicePrinter = getEm()->getRepository("InvoicePrinter")->findOneBy(array("active" => 1));

            if($invoicePrinter == NULL){
                $invoicePrinter = new InvoicePrinter();
            }

            
            $invoicePrinter->setTextIncludeCashier($text_closing);
            $invoicePrinter->setSizeFont($size_font);
            $invoicePrinter->setDescriptionPrinted($description_printed);
            $invoicePrinter->setNumberCopies($number_copies);
            $invoicePrinter->setNumberDecimalPlaces($number_places);
            $invoicePrinter->setPrintDrawingOfColor($print_drawing);
            $invoicePrinter->setEnterTheCustomersPhoneNumber($enter_customer);
            $invoicePrinter->setChangeTheLocation($change_location);
            $invoicePrinter->setPrintTaxesIncluded($print_taxes);
            $invoicePrinter->setDoNotPrintProjectInformation($not_print_project);
            $invoicePrinter->setUseTheDefaultPrinterInstead($use_default_printer);
            $invoicePrinter->setActive(1);
            getEm()->persist($invoicePrinter);
            getEm()->flush();

            $result = "success";
            $message = "query success";
            $mysqlData = "";

        }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    

    }
    /**
     * Get the settings of print of invoice the start the page
     * @param string $id
     * @return string json_data 
     */
    public static function getPrinter(){
        try{
            $invoicePrinter = getEm()->getRepository('InvoicePrinter')->findOneBy(array("active" => 1));
            $data = array ();
                $dat = array (
                    "text_closing" => $invoicePrinter->getTextIncludeCashier(),
                    "size_font" => $invoicePrinter->getSizeFont(),
                    "description_printed" => $invoicePrinter->getDescriptionPrinted(),
                    "number_copies" => $invoicePrinter->getNumberCopies(),
                    "number_places" => $invoicePrinter->getNumberDecimalPlaces(),
                    "print_drawing" => $invoicePrinter->getPrintDrawingOfColor(),
                    "enter_customer" => $invoicePrinter->getEnterTheCustomersPhoneNumber(),
                    "change_location" => $invoicePrinter->getChangeTheLocation(),
                    "print_taxes" => $invoicePrinter->getPrintTaxesIncluded(),
                    "not_print_project" => $invoicePrinter->getDoNotPrintProjectInformation(),
                    "use_default_printer" => $invoicePrinter->getUseTheDefaultPrinterInstead()
                );
                array_push($data, $dat);
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Insert new Invoice 
     * @param string $order
     * @param string $invoiceTemplate
     * @param string $id
     * @param string $dateDue
     * @param string $limitDateDiscount
     * @param string $printed
     * @param string $extraInfo
     * @param string $divideParcelsValue
     * @param string $totalValueOrder
     * @param string $qtyParcels
     * @param string $valueParcels
     * @return string json_data 
     */
    public function insertInvoice($order, $invoiceTemplate, $id, $dateDue, $limitDateDiscount, $printed, $extraInfo, $divideParcelsValue, $totalValueOrder, $qtyParcels, $valueParcels){
        $order = base64_decode($order);
        $invoiceTemplate = base64_decode($invoiceTemplate);
        $id = base64_decode($id);

        $dateDue = base64_decode($dateDue);
        $limitDateDiscount = base64_decode($limitDateDiscount);
        $printed = base64_decode($printed);
        $extraInfo = base64_decode($extraInfo);

        $divideParcelsValue = base64_decode($divideParcelsValue);
        $totalValueOrder = base64_decode($totalValueOrder);
        $qtyParcels = base64_decode($qtyParcels);
        $valueParcels = base64_decode($valueParcels);


        if(($order != "" || $order != "0") && ($invoiceTemplate != "" || $invoiceTemplate != "0") && ($id != "" || $id != "0")){
            try{
                $InvoiceTmpt = getEm()->getRepository('InvoiceTmpt')->findOneBy(array("active" => 1, "idinvoiceTmpt" => $invoiceTemplate));
                $Order = getEm()->getRepository('Order')->findOneBy(array("active" => 1, "idorder" => $order));

                $printedValue = NULL;
                if($printed != "0"){
                    $printedValue = new DateTime($printed);
                }

                $extraInfoValue = "";
                if($extraInfo != "0"){
                    $extraInfoValue = $extraInfo;
                }

                
                if($divideParcelsValue == "2"){
                    $DateDueValue = NULL;
                    $limitDateDiscountValue = NULl;
                    
                    for($i=0; $i<$qtyParcels; $i++){
                        $DateDueValue = "";
                        
                        if($dateDue != "0"){
                            $DateDueValue = strtotime($dateDue . "+".$i." months");
                            $DateDueValue = date('m/d/Y', $DateDueValue);
                        }

                        if($limitDateDiscount != "0"){
                            $limitDateDiscountValue = strtotime($limitDateDiscount . "+".$i." months");
                            $limitDateDiscountValue = date('m/d/Y', $limitDateDiscountValue);   
                        }

                        $invoice = new Invoice();
                        $invoice->setId($id);
                        $invoice->setExtraInfo($extraInfoValue);
                        $invoice->setPrinted($printedValue);
                        if($DateDueValue != NULL){
                           $invoice->setDateDue(new DateTime($DateDueValue)); 
                        }

                        if($limitDateDiscountValue != NULL){
                           $invoice->setLimitDateDiscount(new DateTime($limitDateDiscountValue)); 
                        }
                        
                        $invoice->setDateCreate(new DateTime());
                        $invoice->setActive(1);
                        $invoice->setInvoiceTmptinvoiceTmpt($InvoiceTmpt);
                        $invoice->setOrderorder($Order);
                        $invoice->setStatusBank(0);
                        $invoice->setStatusPayment(0);
                        getEm()->persist($invoice);
                        getEm()->flush();
                    }

                }else{
                    $DateDueValue = NULL;
                    if($dateDue != "0"){
                        $DateDueValue = new DateTime($dateDue);
                    }

                    $limitDateDiscountValue = NULL;
                    if($limitDateDiscount != "0"){
                        $limitDateDiscountValue = new DateTime($limitDateDiscount);
                    }

                    $invoice = new Invoice();
                    $invoice->setId($id);
                    $invoice->setExtraInfo($extraInfoValue);
                    $invoice->setPrinted($printedValue);
                    $invoice->setDateDue($DateDueValue);
                    $invoice->setLimitDateDiscount($limitDateDiscountValue);
                    $invoice->setDateCreate(new DateTime());
                    $invoice->setActive(1);
                    $invoice->setInvoiceTmptinvoiceTmpt($InvoiceTmpt);
                    $invoice->setOrderorder($Order);
                    $invoice->setStatusBank(0);
                    $invoice->setStatusPayment(0);
                    getEm()->persist($invoice);
                    getEm()->flush();
                }

                $result = "success";
                $message = "query success";
                $mysqlData = "";
            } catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );

            $json_data = json_encode($data);
            print $json_data;
        }else{
            var_dump(" $order, $invoiceTemplate or $id is NULL");
        }

    }

    /**
     * Get the price total of the order selected
     * @param string $order
     * @return string json_data 
     */
    public function getValueOrder($order){
        if($order != ""){
            try{
                $valueGross = 0;
                $discount = 0;
                $liquidValue = 0;
                $order = getEm()->getRepository('Order')->findOneBy(array("idorder" => $order));
                $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
                foreach ($OrderLine as $orderLine) {
                    if($order->getIdorder() == $orderLine->getOrderorder()->getIdorder()){
                        $valueGross = $valueGross + $orderLine->getActualValue();
                        $discount = $discount + $orderLine->getDiscount();
                    }
                }

                $discount = $discount + $order->getDiscount();
                $liquidValue = $valueGross - $discount;
                $liquidValue = number_format($liquidValue, 2, ',', ' ');
                $result = "success";
                $message = "query success";
                $mysqlData = $liquidValue;
            } catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );

            $json_data = json_encode($data);
            print $json_data;
        }else{
            var_dump("id order is null.");
        }

    }

    /**
     * If the order divided in parcels, get the value of each.
     * @param string $qtyParcels
     * @param string $totalOrders
     * @return string json_data 
     */
    public function getValuePerParcels($qtyParcels, $totalOrders){
        $qtyParcels = base64_decode($qtyParcels);
        $totalOrders = base64_decode($totalOrders);
        if($qtyParcels != "0" && is_numeric($qtyParcels)){
            try{
                $value = 0;
                $value = $totalOrders/$qtyParcels;
                $value = round($value, 2);
                $value = number_format($value, 2, ',', ' ');
                $result = "success";
                $message = "query success";
                $mysqlData = $value;               
            }catch(Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );

            $json_data = json_encode($data);
            print $json_data;
        }else{
            var_dump("Qty of parcels is NULL or is not number");
        }

    }

    /**
     * Delete invoice(s) with id equal at IdInvoice in $_POST
     * @return string json_data 
     */
    public function deleteInvoice(){
        if($_POST['idInvoice']){
            try{
                $ids = $_POST['idInvoice'];
                for($i=0; $i < count($ids); $i++){
                    $Invoice = getEm()->getRepository('Invoice')->findOneBy(array("idinvoice" => $ids[$i]));
                    $Invoice->setActive(0);
                    $Invoice->setDateDelete(new DateTime());
                    getEm()->persist($Invoice);
                    getEm()->flush();
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

}

?>