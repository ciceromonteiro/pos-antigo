<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Feb 2, 2017
 */
class PersonSettingsController {

    public function __contruct() {
        
    }

    /**
     * Get last id of Person_settings
     * 
     * @param int $language id of language
     * @param boolean $viewname value InvShowCustomerName
     * @param boolean $invoiceg InvGroupInvoices
     * @param boolean $withoutc InvTaxFree
     * @param boolean $printcompany InvInvoicePrintExclusive
     * @param boolean $hideinvoice InvHideProducts
     * @param boolean $invoiceelectro InvElectronicInvoice
     * @param string $describeinvoice InvInfo
     * @param int $themeinvoice InvoiceTmptinvoiceTmpt
     * @param string $sendinvoice InvEmail
     * @param boolean $trackemail InvTrackEmail
     * @param int $coincustomer Currencycurrency
     * @param int $redirectinvoice Personperson
     * @param string $nshowcustomer OrdHideFromStatistics
     * @param boolean $boxcustomer OrdShowInfo
     * @param string $exchagersend OrdUseComAddress
     * @param int $ticket TicketTmptticketTmpt
     * @param int $project Projectproject
     * @param int $limit OrdCreditLimit
     * @param string $emailpostcolle ChargeTimes
     * @param boolean $sms AuthReceiveSms
     * @param boolean $sendemailclient AuthReceiveEmail
     * @param boolean $taxlass tax_free
     * @return int $idInsert in value last insert
     */
    public function insertPersonSettings($language, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $describeinvoice, $themeinvoice, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $nshowcustomer, $boxcustomer, $exchagersend, $ticket, $project, $limit, $emailpostcolle, $sms, $sendemailclient, $taxlass) {
        try {
            $data = new DateTime();
            $languagelanguage = getEm()->getRepository('Language')->findBy(array("idlanguage" => $language)); 
            $InvoiceTmptinvoiceTmpt = getEm()->getRepository('InvoiceTmpt')->findBy(array("idinvoiceTmpt" => $themeinvoice));
            $Currencycurrency = getEm()->getRepository('Currency')->findBy(array("idcurrency" => $coincustomer));
            $TicketTmptticketTmpt = getEm()->getRepository('TicketTmpt')->findBy(array("idticketTmpt" => $ticket));
            $Projectproject = getEm()->getRepository('Project')->findBy(array("idproject" => $project));

           if($viewname == "on"){
                $viewname = '1';
           }else{
                $viewname = '0';
           }

           if($invoiceg == "on"){
                $invoiceg = '1';
           }else{
                $invoiceg = '0';
           }

           if($withoutc == "on"){
                $withoutc = '1';
           }else{
                $withoutc = '0';
           }

           if($printcompany == "on"){
                $printcompany = '1';
           }else{
                $printcompany = '0';
           }

            if($hideinvoice == "on"){
                $hideinvoice = '1';
           }else{
                $hideinvoice = '0';
           }

            if($invoiceelectro == "on"){
                $invoiceelectro = '1';
           }else{
                $invoiceelectro = '0';
           }

            if($trackemail == "on"){
                $trackemail = '1';
           }else{
                $trackemail = '0';
           }

            if($boxcustomer == "on"){
                $boxcustomer = '1';
           }else{
                $boxcustomer = '0';
           }

            if($sms == "on"){
                $sms = '1';
           }else{
                $sms = '0';
           }
            

            if($sendemailclient == "on"){
                $sendemailclient = '1';
           }else{
                $sendemailclient = '0';
           }

           if($taxlass == "on"){
                $taxlass = '1';
           }else{
                $taxlass = '0';
           }
           
            
            
            
            $PersonSettings = new PersonSettings();
            $PersonSettings->setLanguagelanguage($languagelanguage[0]);
            $PersonSettings->setInvShowCustomerName($viewname);
            $PersonSettings->setInvGroupInvoices($invoiceg);
            $PersonSettings->setInvTaxFree($withoutc);
            $PersonSettings->setInvInvoicePrintExclusive($printcompany);
            $PersonSettings->setInvHideProducts($hideinvoice);
            $PersonSettings->setInvElectronicInvoice($invoiceelectro);
            $PersonSettings->setInvInfo($describeinvoice);
            $PersonSettings->setInvoiceTmptinvoiceTmpt($InvoiceTmptinvoiceTmpt[0]);
            $PersonSettings->setInvEmail($sendinvoice);
            $PersonSettings->setInvTrackEmail($trackemail);
            $PersonSettings->setCurrencycurrency($Currencycurrency[0]);
            if ($redirectinvoice != 0):
                $Personperson = getEm()->getRepository('Person')->findBy(array("idperson" => $redirectinvoice));
                $PersonSettings->setPersonperson($Personperson[0]);
            endif;
            $PersonSettings->setOrdShowInfo($boxcustomer);
            $PersonSettings->setOrdHideFromStatistics($nshowcustomer);
            $PersonSettings->setOrdUseComAddress($exchagersend);
            $PersonSettings->setTicketTmptticketTmpt($TicketTmptticketTmpt[0]);
            $PersonSettings->setProjectproject($Projectproject[0]);
            $PersonSettings->setOrdCreditLimit($limit);
            $PersonSettings->setChargeTimes($emailpostcolle);
            $PersonSettings->setAuthReceiveSms($sms);
            $PersonSettings->setAuthReceiveEmail($sendemailclient);
            $PersonSettings->setTaxFree($taxlass);
            $PersonSettings->setDateCreate($data);
            $PersonSettings->setActive(1);
            getEm()->persist($PersonSettings);
            getEm()->flush();
            $idInsert = $PersonSettings->getIdpersonSettings();
            //AuthenticationController::insertLog('create', 'personsettings', $idInsert);
         
        } catch (Exception $e) {
            print $e;
            exit(1);
        }
         
        return $idInsert;
    }

}
