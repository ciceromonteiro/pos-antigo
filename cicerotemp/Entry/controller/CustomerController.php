<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Nov 29, 2016
 */
class CustomerController {


    /**
     * Index call Entry customers
     * 
     * @access public
     * @return void call preview
     */
    public static function index() {
        $navbar = "Entry|customer";
        $array_answer = getEm()->getRepository('Person')->findBy(array("active" => 1));
        GenericController::template("Entry", "customer", "create", $navbar, $array_answer, 262);
    }

    /**
     * List call Entry customers
     * 
     * @access public
     * @return void call preview
     */
//    public static function listed() {
//        $navbar = "Entry|customer";
//        $array_answer = getEm()->getRepository('Person')->findBy(array("active" => 1));
//        GenericController::template("Entry", "customer", "list", $navbar, $array_answer, 264);
//    }

    /**
     * List call Entry customers
     * 
     * @access public
     * @return void call preview
     */
    public static function listed() {
        $navbar = "Entry|customer";
        $array_answer = array(
            "customerGps" => getEm()->getRepository('CustomerGp')->findBy(array('active' => 1)),
            "countrys" => getEm()->getRepository('ZzCountry')->findBy(array('active' => 1)),
            "employees" => getEm()->getRepository('Users')->findBy(array('active' => 1)),
            "customers" => getEm()->getRepository('Person')->findBy(array('active' => 1)),
            "templates" => getEm()->getRepository('OrderTmpt')->findBy(array("active" => 1)),
            "currencies" => getEm()->getRepository('Currency')->findBy(array('active' => 1)),
            "paymentMethods" => getEm()->getRepository('PaymentMtd')->findBy(array('active' => 1)),
            "departments" => getEm()->getRepository('Department')->findBy(array('active' => 1)),
            "projects" => getEm()->getRepository('Project')->findBy(array('active' => 1))
        );
        GenericController::template("Entry", "customer", "list", $navbar, $array_answer, 264);
    }

    /**
     * Discounts call Entry customers
     * 
     * @access public
     * @return void call preview
     */
    public static function discounts() {
        $navbar = "Entry|customer";
        $array_answer = array("");
        GenericController::template("Entry", "customer", "discounts", $navbar, $array_answer, 268);
    }

    /**
     * payment call Entry customers
     * 
     * @access public
     * @return void call preview
     */
    public static function payment() {
        $navbar = "Entry|customer";
        $array_answer = array("");
        GenericController::template("Entry", "customer", "payment", $navbar, $array_answer, 272);
    }

    /**
     * Client call Entry customers
     * 
     * @access public
     * @return void call preview
     */
    public static function client() {
        $navbar = "Entry|customer";
        $array_answer = array("");
        GenericController::template("Entry", "customer", "client", $navbar, $array_answer, 276);
    }

    /**
     * Card call Entry customers
     * 
     * @access public
     * @return void call preview
     */
    public static function card() {
        $navbar = "Entry|customer";
        $array_answer = getEm()->getRepository('Person')->findBy(array("active" => 1));
        GenericController::template("Entry", "customer", "card", $navbar, $array_answer, 280);
    }

    /**
     * Get All data of Customers
     * 
     * @return string json
     */
    public function getAllClient() {
        try {
            $CustomerGp = getEm()->getRepository('CustomerGp')->findBy(array("active" => 1));
            $data = array();
            foreach ($CustomerGp as $value) {
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdcustomerGp().'">',
                    "id" => '<a data-id="' . $value->getIdcustomerGp() . '">' . $value->getIdcustomerGp() . '</a>',
                    "description" => $value->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Insert new customer group
     * @param $nameGroup Nome do grupo
     * @return string json  
     */
    public function insertClient($nameGroup) {
      
        if (isset($nameGroup) && $nameGroup != "") {
            try {
                $Customergp = new CustomerGp();
                $Customergp->setName($nameGroup);
                $Customergp->setActive(1);
                getEm()->persist($Customergp);
                getEm()->flush();
                
                //AuthenticationController::insertLog('create', 'customer group', $nameGroup);

                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * get Customer Group by id
     * 
     * @param type $id
     * @return string Json
     */
    public function getClient($id) {
        try {
            $CustomerGp = getEm()->getRepository('CustomerGp')->find(array("idcustomerGp" => $id));
            $dat = array("description" => $CustomerGp->getName(), "id" => $CustomerGp->getIdcustomerGp());
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $dat);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Update date group of client
     * 
     * @param int $id
     * @access public
     * @return string json result
     */
    public function updateClient($id) {
        if (isset($_GET['nameGroup'])) {
            try {
                $CustomerGp = getEm()->getRepository('CustomerGp')->findBy(array("idcustomerGp" => $id));
                $mysqlData = array();
                $dat = array("description" => $CustomerGp[0]->setName($_GET['nameGroup']));
                array_push($mysqlData, $dat);
                getEm()->persist($CustomerGp[0]);
                getEm()->flush();
                
               // AuthenticationController::insertLog('update', 'customer', $_POST['nameGroup']);
                
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }
             $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );
        
        
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Remove Group Client
     * 
     * @param type $id
     * @access public
     * @return string json string
     */
    public function deleteClient($id) {
        try {
            $CustomerGp = getEm()->getRepository('CustomerGp')->findBy(array("idcustomerGp" => $id));
            $mysqlData = array();
            $dat = array("active" => $CustomerGp[0]->setActive(0));
            array_push($mysqlData, $dat);
            getEm()->persist($CustomerGp[0]);
            getEm()->flush();
            //AuthenticationController::insertLog('delete', 'customer', $id);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $data);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Get Template GiftCard
     * 
     * @return Array object
     */
    public function getTemplateGiftCard() {
        $return = getEm()->getRepository('GiftCardTmpt')->findBy(array("active" => 1));
        return $return;
    }

    /**
     * Insert values GiftCard
     * @return string json
     */
     public function insertGiftCard($nemaTemplate, $valueStart, $person, $info) {
        try {
            $template = getEm()->getRepository('GiftCardTmpt')->findBy(array("idgiftCardTmpt" => $nemaTemplate));
            $companycompany = getEm()->getRepository('Company')->findBy(array("idcompany" => 1));
            $personperson = getEm()->getRepository('Person')->findBy(array("idperson" => $person));
            $Giftcard = new Giftcard();
            $Giftcard->setGiftCardTmptgiftCardTmpt($template[0]);
            $Giftcard->setCompanycompany($companycompany[0]);
            $Giftcard->setPersonperson($personperson[0]);
            $Giftcard->setValue($valueStart);
            $Giftcard->setValueUsed(0);
            $Giftcard->setExtraInfo($info);
            $Giftcard->setActive(1);
            getEm()->persist($Giftcard);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }

 /*  public function insertGiftCard() {
       // if (isset($_GET['nematemplate'])) {
            try {
                $template = getEm()->getRepository('GiftCardTmpt')->findBy(array("idgiftCardTmpt" => $_GET['nematemplate']));
                $companycompany = getEm()->getRepository('Company')->findBy(array("idcompany" => 1));
                $personperson = getEm()->getRepository('Person')->findBy(array("idperson" => $_GET['selectperson']));
                $Giftcard = new GiftCard();
                $Giftcard->setGiftCardTmptgiftCardTmpt($template[0]);
                $Giftcard->setCompanycompany($companycompany[0]);
                $Giftcard->setPersonperson($personperson[0]);
                // $Giftcard->setValue($_GET['valuestart']);
                $Giftcard->setValue($_GET['valuestart']);
                $Giftcard->setValueUsed(0);
                $Giftcard->setExtraInfo($_GET['info']);
                $Giftcard->setActive(1);

                getEm()->persist($Giftcard);
                getEm()->flush();
                AuthenticationController::insertLog('create', 'customer', $_GET['nematemplate']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
       // }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
*/
    /**
     * Get all GiftCard
     * 
     * @return string json
     */
    public function getAllCard() {
        try {
            $GiftCard = getEm()->getRepository('GiftCard')->findBy(array("active" => 1));
            $data = array();
            foreach ($GiftCard as $value) {
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdgiftCard().'">',
                    "id" => '<a data-id="' . $value->getIdgiftCard() . '" data-name="' . $value->getIdgiftCard() . '">' . $value->getIdgiftCard() . '</a>',
                    "card" => $value->getIdgiftCard(),
                    "create" => $value->getDateCreate()->format('Y-m-d H:i'),
                    "valueinit" => $value->getValue(),
                    "balance" => ($value->getValue() - $value->getValueUsed()),
                    "updatev" => $value->getDateUpdate()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Get GiftCArd
     * 
     * @return string Json
     */

  
    public function getCard($id) {
        try {
            $GiftCard = getEm()->getRepository('GiftCard')->findBy(array("idgiftCard" => $id));
            $mysqldata = array();
            $dat = array(
                "idCardedit" => $GiftCard[0]->getIdgiftCard(),
                "templateedit" => $GiftCard[0]->getGiftCardTmptgiftCardTmpt()->getIdgiftCardTmpt(), 
                "selectperson" => $GiftCard[0]->getPersonperson()->getIdperson(),
                 "valuestart" => $GiftCard[0]->getValue(), 
                 "info" => $GiftCard[0]->getExtraInfo());
            array_push($mysqldata, $dat);
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $mysqldata);
        $json_data = json_encode($data);
        print $json_data;
    }


    /**
     * Get all details of GiftCArd
     * @param string $id
     * @return string Json
     */
    public function getCardDetails($id){
        try {
            $GiftCard = getEm()->getRepository('GiftCard')->findOneBy(array("idgiftCard" => $id));
            $person = "";
            if($GiftCard->getPersonperson() != NULL){
                $person = $GiftCard->getPersonperson()->getName();
            }

            $company = "";
            if($GiftCard->getCompanycompany() != NULL){
                $company = $GiftCard->getCompanycompany()->getName();
            }

            $cardIdentity = "";
            if($GiftCard->getGiftCardIdentify() != NULL){
                $cardIdentity = $GiftCard->getGiftCardIdentify();
            }

            $template = "";
            if($GiftCard->getGiftCardTmptgiftCardTmpt() != NULL){
                $template = $GiftCard->getGiftCardTmptgiftCardTmpt()->getName();
            }

            $dateCreated = "";
            if($GiftCard->getDateCreate() != NULL){
                $dateCreated = $GiftCard->getDateCreate()->format('d/m/Y');
            }

            $dateUpdated = "";
            if($GiftCard->getDateUpdate() != NULL){
                $dateUpdated = $GiftCard->getDateUpdate()->format('d/m/Y');
            }

            $dateDeleted = "";
            if($GiftCard->getDateDelete() != NULL){
                $dateDeleted = $GiftCard->getDateDelete()->format('d/m/Y');
            }

            $active = "";
            if($GiftCard->getActive() == 1){
                $active = '<span class="glyphicon glyphicon-ok"></span>';
            }else{
                $active = '<span class="glyphicon glyphicon-remove"></span>';
            }

            $mysqldata = array();
            $dat = array(
                "idCardedit" => $GiftCard->getIdgiftCard(),
                "person" => $person,
                "company" => $company,
                "card_identity" => $cardIdentity, 
                "template" => $template, 
                "value" => $GiftCard->getValue(), 
                "value_used" => $GiftCard->getValueUsed(), 
                "extra_info" => $GiftCard->getExtraInfo(), 
                "date_created" => $dateCreated, 
                "date_updated" => $dateUpdated, 
                "date_deleted" => $dateDeleted,
                "active" => $active
                );
            array_push($mysqldata, $dat);
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $mysqldata = "";
        }

        
        $data = array("result" => $result, "message" => $message, "data" => $mysqldata);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Update of data giftcard
     * @access public
     * @return string data json in success or error 
     */
     
    public function updateCard($id, $nameTemplate, $valueStart , $person, $info) {
            try {
                $GiftCard = getEm()->getRepository('Giftcard')->findBy(array("idgiftCard" => $id));
                $template = getEm()->getRepository('GiftCardTmpt')->findBy(array("idgiftCardTmpt" => $nameTemplate));
                $personperson = getEm()->getRepository('Person')->findBy(array("idperson" => $person));
                $mysqlData = array();
                $dat = array(
                    $GiftCard[0]->setGiftCardTmptgiftCardTmpt($template[0]), 
                    $GiftCard[0]->setPersonperson($personperson[0]), 
                    $GiftCard[0]->setValue($valueStart),
                    $GiftCard[0]->setExtraInfo($info)
                    );
                array_push($mysqlData, $dat);
                getEm()->persist($GiftCard[0]);
                getEm()->flush();
                
                     //AuthenticationController::insertLog('create', 'customercard', $_GET['nematemplate']);
                
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        
             $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );
        
        
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * define active in giftcard in active = 0
     * @param int $id
     * @access public
     * @return string json string
     */
    public function deleteCard($id) {
        try {
            $GiftCard = getEm()->getRepository('GiftCard')->findBy(array("idgiftCard" => $id));
            $dat = array("active" => $GiftCard[0]->setActive(0));
            getEm()->persist($GiftCard[0]);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $dat);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * return values payment's
     * @access public
     * @return string json string
     */
    public function getAllPayment() {
        $confim = array(1 => "yes", 0 => "no");
        try {
            $PaymentMtd = getEm()->getRepository('PaymentMtd')->findBy(array("active" => 1));
            $mysqlData = array();
            foreach ($PaymentMtd as $value) {
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdpaymentMtd().'">',
                    "id" => '<a data-id="' . $value->getIdpaymentMtd() . '">' . $value->getIdpaymentMtd() . '</a>',
                    "namepay" => $value->getName(),
                    "invoices" => $confim[$value->getInvoice()],
                    "timeinit" => $value->getDeadlinemonth(),
                    "timeend" => $value->getDeadlineyear()
                );
                array_push($mysqlData, $dat);
            }
            $result = "success";
            $message = "query success";
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $mysqlData);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Inser of  value's payment's 
     * 
     * @param string $name
     * @param int $invoice
     * @param int $deadlinemonth
     * @param int $deadlineyear
     * @param string $pathPlatform
     * @access public
     * @return string json
     */
    public function insertPayment($name, $invoice, $deadlinemonth, $deadlineyear, $pathPlatform) {
        try {
            $PaymentMtd = new PaymentMtd();
            $PaymentMtd->setName($name);
            $PaymentMtd->setInvoice($invoice);
            $PaymentMtd->setDeadlinemonth($deadlinemonth);
            $PaymentMtd->setDeadlineyear($deadlineyear);
            $PaymentMtd->setPathPlatform($pathPlatform);
            $PaymentMtd->setActive(1);
            getEm()->persist($PaymentMtd);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * define active in Payment in active = 0
     * @param int $id
     * @access public
     * @return string json string
     */
    public function deletePayment($id) {
        try {
            $PaymentMtd = getEm()->getRepository('PaymentMtd')->findBy(array("idpaymentMtd" => $id));
            $dat = array("active" => $PaymentMtd[0]->setActive(0));
            getEm()->persist($PaymentMtd[0]);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $dat);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * 
     * @param type $id
     */
    public function getPayment($id) {
        try {
            $dat = array();
            $PaymentMtd = getEm()->getRepository('PaymentMtd')->findBy(array("idpaymentMtd" => $id));
            if(!empty($PaymentMtd)){
                $dat = array(
                    "namePayment" => $PaymentMtd[0]->getName(),
                    "idPayment" => $PaymentMtd[0]->getIdpaymentMtd(),
                    "invoice" => $PaymentMtd[0]->getInvoice(),
                    "month" => $PaymentMtd[0]->getDeadlinemonth(),
                    "year" => $PaymentMtd[0]->getDeadlineyear(),
                    "path" => $PaymentMtd[0]->getPathPlatform()
                );
            }
            $result = 'success';
            $message = 'query success';
            $data = $dat;
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $data);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Alter value's of Payment
     * 
     * @param int $id
     * @param string $name
     * @param booler $invoice
     * @param int $month
     * @param int $year
     * @param string $path
     * @access public
     * @return string json
     */
    public function updatePayment($id, $name, $invoice, $month, $year, $path) {
        try {
            $PaymentMtd = getEm()->getRepository('PaymentMtd')->findBy(array("idpaymentMtd" => $id));
            $date = new DateTime();
            $PaymentMtd[0]->setName($name);
            $PaymentMtd[0]->setInvoice($invoice);
            $PaymentMtd[0]->setDeadlinemonth($month);
            $PaymentMtd[0]->setDeadlineyear($year);
            $PaymentMtd[0]->setPathPlatform($path);
            $PaymentMtd[0]->setDateUpdate($date);
            getEm()->persist($PaymentMtd[0]);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * return values Discount
     * @access public
     * @return string json string
     */
    public function getAllDiscount() {
        try {
            $PaymentMtd = getEm()->getRepository('Discount')->findBy(array("active" => 1));
            $mysqlData = array();
            foreach ($PaymentMtd as $value) {
                if ($value->getPersonIdperson() != null) {
                    $cname = $value->getPersonIdperson()->getName();
                } else {
                    $cname = "";
                }
                if ($value->getProductGpIdproductGp() != null) {
                    $pgroup = $value->getProductGpIdproductGp()->getName();
                } else {
                    $pgroup = "";
                }
                if ($value->getProductIdproduct() != null) {
                    $product = $value->getProductIdproduct()->getName();
                } else {
                    $product = "";
                }
                if ($value->getPersonIdperson() != null) {
                    $idperson = $value->getPersonIdperson()->getIdperson();
                } else {
                    $idperson = "";
                }
                if ($value->getCustomerGpIdcustomerGp() != null) {
                    $cgroup = $value->getCustomerGpIdcustomerGp()->getName();
                } else {
                    $cgroup = "";
                }
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIddiscount().'">',
                    "id" => '<a data-id="' . $value->getIddiscount() . '">' . $value->getIddiscount() . '</a>',
                    "cgroup" => $cgroup,
                    "idc" => $idperson,
                    "cname" => $cname,
                    "pgroup" => $pgroup,
                    "product" => $product,
                    "percent" => $value->getPercent()." %"
                );
                array_push($mysqlData, $dat);
            }
            $result = "success";
            $message = "query success";
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $mysqlData);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Insert values of the discount
     * 
     * @param int $nameDiscount
     * @param int $clientname
     * @param int $product
     * @param int $productg
     * @param decimal $percent
     * @access public
     * @return string json
     */
    public function insertDiscount($nameDiscount, $clientname, $product, $productg, $percent) {
        $percent = base64_decode($percent);
        try {
            $Discount = new Discount();
            if ($nameDiscount != "0") {
                $CustomeOB = getEm()->getRepository('CustomerGp')->findBy(array("idcustomerGp" => $nameDiscount));
                $Discount->setCustomerGpIdcustomerGp($CustomeOB[0]);
            }
            if ($clientname != "0") {
                $PersonOB = getEm()->getRepository('Person')->findBy(array("idperson" => $clientname));
                $Discount->setPersonIdperson($PersonOB[0]);
            }
            if ($product != "0") {
                $ProductOB = getEm()->getRepository('Product')->findBy(array("idproduct" => $product));
                $Discount->setProductIdproduct($ProductOB[0]);
            }
            if ($productg != "0") {
                $productgOB = getEm()->getRepository('ProductGp')->findBy(array("idproductGp" => $productg));
                $Discount->setProductGpIdproductGp($productgOB[0]);
            }
            $Discount->setPercent($percent);
            $Discount->setActive(1);
            getEm()->persist($Discount);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            echo $e->getMessage();
            die();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * define active in Discount in active = 0
     * @param int $id
     * @access public
     * @return string json string
     */
    public function deleteDiscount($id) {
        try {
            $Discount = getEm()->getRepository('Discount')->findBy(array("iddiscount" => $id));
            $dat = array("active" => $Discount[0]->setActive(0));
            getEm()->persist($Discount[0]);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $dat);
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getDiscount($id) {
        try {
            $Discount = getEm()->getRepository('Discount')->findOneBy(array("iddiscount" => $id));
            if ($Discount->getCustomerGpIdcustomerGp() != null) {
                $customegpvalue = $Discount->getCustomerGpIdcustomerGp()->getIdcustomerGp();
            } else {
                $customegpvalue = "";
            }
            if ($Discount->getPersonIdperson() != null) {
                $ippersonvalue = $Discount->getPersonIdperson()->getIdperson();
            } else {
                $ippersonvalue = "";
            }
            if ($Discount->getProductIdproduct() != null) {
                $idproductvalue = $Discount->getProductIdproduct()->getIdproduct();
            } else {
                $idproductvalue = "";
            }
            if ($Discount->getProductGpIdproductGp() != null) {
                $productgpvalue = $Discount->getProductGpIdproductGp()->getIdproductGp();
            } else {
                $productgpvalue = "";
            }

            $dat = array(
                "idedit" => $Discount->getIddiscount(),
                "nameDiscountedit" => $customegpvalue,
                "clientnameedit" => $ippersonvalue,
                "productedit" => $idproductvalue,
                "productgedit" => $productgpvalue,
                "percentedit" => $Discount->getPercent()
            );
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $dat);
        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateDiscount($id, $nameDiscount, $clientname, $product, $productg, $percent) {
    $percent = base64_decode($percent);
        try {
            $Discount = getEm()->getRepository('Discount')->findBy(array("iddiscount" => $id));
            if ($nameDiscount != 0) {
                $CustomeOB = getEm()->getRepository('CustomerGp')->findBy(array("idcustomerGp" => $nameDiscount));
                $Discount[0]->setCustomerGpIdcustomerGp($CustomeOB[0]);
            } else {
                $Discount[0]->setCustomerGpIdcustomerGp(NULL);
            }
            if ($clientname != 0) {
                $PersonOB = getEm()->getRepository('Person')->findBy(array("idperson" => $clientname));
                $Discount[0]->setPersonIdperson($PersonOB[0]);
            } else {
                $Discount[0]->setPersonIdperson(NULL);
            }
            if ($product != 0) {
                $ProductOB = getEm()->getRepository('Product')->findBy(array("idproduct" => $product));
                $Discount[0]->setProductIdproduct($ProductOB[0]);
            } else {
                $Discount[0]->setProductIdproduct(NULL);
            }
            if ($productg != 0) {
                $productgOB = getEm()->getRepository('ProductGp')->findBy(array("idproductGp" => $productg));
                $Discount[0]->setProductGpIdproductGp($productgOB[0]);
            } else {
                $Discount[0]->setProductGpIdproductGp(NULL);
            }
            $Discount[0]->setPercent($percent);
            getEm()->persist($Discount[0]);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllListCustomers() {
        try {
            $Person = getEm()->getRepository('Person')->findBy(array("active" => 1));
            $mysqlData = array();
            foreach ($Person as $value) {
                $Address = getEm()->getRepository('Address')->findBy(array("personperson" => $value->getIdperson()));
                if (!empty($Address)) {
                    $street = $Address[0]->getAddressStreet();
                    $zip = $Address[0]->getAddressZipcode();
                    $city = $Address[0]->getAddressCity();
                } else {
                    $street = "";
                    $zip = "";
                    $city = "";
                }
                if ($value->getPaymentMtdpaymentMtd() != null) {
                    $payment = $value->getPaymentMtdpaymentMtd()->getName();
                } else {
                    $payment = "";
                }

                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdperson().'">',
                    "id" => $value->getIdperson(),
                    "name" => $value->getName(), 
                    "street" => $street, 
                    "zip" => $zip, 
                    "city" => $city, 
                    "payment" => $payment
                );
                array_push($mysqlData, $dat);
            }
            $result = "success";
            $message = "query success";
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $mysqlData);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * salva crud of the customer
     * 
     * @param type $name
     * @param type $street
     * @param type $zip
     * @param type $countryselect
     * @param type $employsselect
     * @param type $namePerson
     * @param type $phoneclint
     * @param type $emailclient
     * @param type $selectgroup
     * @param type $idcompany
     * @param type $phonecompany
     * @param type $emailcompany
     * @param type $describe
     * @param type $taxlass
     * @param type $viewname
     * @param type $invoiceg
     * @param type $withoutc
     * @param type $printcompany
     * @param type $hideinvoice
     * @param type $invoiceelectro
     * @param type $themeinvoice
     * @param type $payment
     * @param type $sendinvoice
     * @param type $trackemail
     * @param type $coincustomer
     * @param type $redirectinvoice
     * @param type $describeinvoice
     * @param type $boxcustomer
     * @param type $nshowcustomer
     * @param type $exchagersend
     * @param type $ticket
     * @param type $department
     * @param type $project
     * @param type $limit
     * @param type $emailpostcolle
     * @param type $fax
     * @param type $birth
     * @param type $sms
     * @param type $sendemailclient
     * @param type $identification
     * @param type $genre
     * @param type $language
     * @param type $userlog
     * @param type $zzstate
     * @param type $numbercustome
     * @param type $citycustome
     * @param string $Complementcustome
     * @param string $districtcustome
     * @return string json
     */
    
    public function insertCustomer($name, $street, $zip, $countryselect, $employsselect, $namePerson, $phoneclint, $emailclient, $selectgroup, $idcompany, $phonecompany, $emailcompany, $describe, $taxlass, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $themeinvoice, $payment, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $describeinvoice, $boxcustomer, $nshowcustomer, $exchagersend, $ticket, $department, $project, $limit, $emailpostcolle, $fax, $birth, $sms, $sendemailclient, $identification, $genre, $language, $userlog, $zzstate, $numbercustome, $citycustome, $Complementcustome, $districtcustome, $image) {
        
        include 'PersonSettingsController.php';
        include 'ZzStateController.php';
        include 'AddressController.php';
        include 'PersonDataController.php';
        include 'PersonCompanyController.php';

        $name = base64_decode($name);
        $street = base64_decode($street);
        $countryselect = base64_decode($countryselect);
        $citycustome = base64_decode($citycustome);
        $districtcustome = base64_decode($districtcustome);
        $namePerson = base64_decode($namePerson);
        $emailclient = base64_decode($emailclient);
        $emailcompany = base64_decode($emailcompany);
        $birth = base64_decode($birth);
        $image = base64_decode($image);

        //var_dump($name, $street, $zip, $countryselect, $employsselect, $namePerson, $phoneclint, $emailclient, $selectgroup, $idcompany, $phonecompany, $emailcompany, $describe, $taxlass, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $themeinvoice, $payment, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $describeinvoice, $boxcustomer, $nshowcustomer, $exchagersend, $ticket, $department, $project, $limit, $emailpostcolle, $fax, $birth, $sms, $sendemailclient, $identification, $genre, $language, $userlog, $zzstate, $numbercustome, $citycustome, $Complementcustome, $districtcustome, $image);
        //die();

        $pospos = $this->getPosLogin($userlog);
        $customerGpcustomerGp = getEm()->getRepository('CustomerGp')->find(array("idcustomerGp" => $selectgroup));
        $paymentMtdIdpaymentMtd = getEm()->getRepository('PaymentMtd')->find(array("idpaymentMtd" => $payment));
        $departmentIddepartment = getEm()->getRepository('Department')->find(array("iddepartment" => $department));
        $PersonSettings = new PersonSettingsController();
        $personSettingspersonSettings = $PersonSettings->insertPersonSettings($language, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $describeinvoice, $themeinvoice, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $nshowcustomer, $boxcustomer, $exchagersend, $ticket, $project, $limit, $emailpostcolle, $sms, $sendemailclient, $taxlass);
        $personsetting = getEm()->getRepository('PersonSettings')->find(array("idpersonSettings" => $personSettingspersonSettings));
        $ZzStateOB = new ZzStateController();
        $idzzState = $ZzStateOB->insertState($zzstate, $countryselect);
        $zzStatezzState = getEm()->getRepository('ZzState')->find(array("idzzState" => $idzzState));
        $Address = new AddressController();
        $PersonDataOB = new PersonDataController();
        $companycompany = getEm()->getRepository('Company')->find(array("idcompany" => $idcompany)); //ver tabela pos company
        $userOB = getEm()->getRepository('Users')->find(array("idusers" => $employsselect));
        $PersonCompOB = new PersonCompanyController();
        $datateste = new DateTime();
        try {
            $Person = new Person();
            $Person->setName($name);
            $Person->setPospos($pospos);
            $Person->setCustomerGpcustomerGp($customerGpcustomerGp);
            $Person->setPaymentMtdpaymentMtd($paymentMtdIdpaymentMtd);
            $Person->setDepartmentdepartment($departmentIddepartment);
            $Person->setBirthDate($birth);
            $Person->setZzStatezzState($zzStatezzState);
            $Person->setPersonSettingspersonSettings($personsetting);
            $Person->setGenre($genre);
            $Person->setInfo($describe);
            $Person->setDateCreation($datateste);
            $Person->setContactPerson($namePerson);
            $Person->setElectronicIdentification($identification);
            $Person->setUsersusers($userOB);
            $Person->setActive(1);
            getEm()->persist($Person);
            getEm()->flush();
            $PersonDataOB->insertPersonData("phone", $phoneclint, $Person);
            $PersonDataOB->insertPersonData("email", $emailclient, $Person);
            $PersonDataOB->insertPersonData("fax", $fax, $Person);
            $PersonDataOB->insertPersonData("phonecontact", $phonecompany, $Person);
            $PersonDataOB->insertPersonData("phoneconta", $phonecompany, $Person);
            $PersonDataOB->insertPersonData("emailconta", $emailcompany, $Person);
            $Address->insertAddress($zzStatezzState, $Person, $street, $zip, $numbercustome, $citycustome, $Complementcustome, $districtcustome);
            $PersonCompOB->insertPersonCompany($Person, $companycompany);
            $this->setImgCustomer($Person, $image);
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }


    /**
     * pull value of idpos
     * 
     * @access public
     * @param int $id
     * @return int
     */
    public function getPosLogin($id) {
        $user = getEm()->getRepository('Users')->findBy(array("idusers" => $id));
        $userlog = getEm()->getRepository('LogTime')->findBy(array("usersusers" => $user[0]), array('idlogTime' => 'desc'));
        return $userlog[0]->getPosIdpos();
    }

    /**
     * Insert image Customer
     * 
     * @param Array $personperson
     * @param file $file
     * @return void 
     */
    public static function setImgCustomer($personperson, $image) {
        //$nameOLD = $file["file"]["name"];
        //$typeimg = explode("/", $file["file"]["type"]);
       // $typeFile = explode(".", $nameOLD);
        //$nameFile = time() . '.' . $typeFile[1];
       // $uploads_dir = '../data/temp/';
       // if (move_uploaded_file($file["file"]["tmp_name"], $uploads_dir . $nameFile)) {
        //    $imagem = file_get_contents($uploads_dir . $nameFile);

            //$mysqlImg = base64_encode($imagem);
            $dataInit = new DateTime();
            try {
                $PersonImage = new PersonImage();
                $PersonImage->setPersonperson($personperson);
                $PersonImage->setPath($image);
                $PersonImage->setDateCreate($dataInit);
                $PersonImage->setActive(1);
                getEm()->persist($PersonImage);
                getEm()->flush();
            } catch (Exception $e) {

                echo $e->getMessage();
                die();
            }
            
        }
    

    public function removeImgCustomer($personperson) {
        try {
            $PersonImage = getEm()->getRepository('PersonImage')->findby(array("personperson" => $personperson));
            $PersonImage[0]->setActive(0);
            getEm()->persist($PersonImage[0]);
            getEm()->flush();
        } catch (Exception $e) {

            echo $e->getMessage();
            die();
        }
    }

    /**
     * Remove Customer
     * @param int $id
     * @access public
     * @return string json string
     */
    public function deleteCustomer($id) {
        try {
            $person = getEm()->getRepository('Person')->find(array("idperson" => $id));
            $dat = array("active" => $person->setActive(0));
            getEm()->persist($person);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $dat);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * 
     * @param type $id
     * @param type $name
     * @param type $street
     * @param type $zip
     * @param type $countryselect
     * @param type $employsselect
     * @param type $namePerson
     * @param type $phoneclint
     * @param type $emailclient
     * @param type $selectgroup
     * @param type $idcompany
     * @param type $phonecompany
     * @param type $emailcompany
     * @param type $describe
     * @param type $taxlass
     * @param type $viewname
     * @param type $invoiceg
     * @param type $withoutc
     * @param type $printcompany
     * @param type $hideinvoice
     * @param type $invoiceelectro
     * @param type $themeinvoice
     * @param type $payment
     * @param type $sendinvoice
     * @param type $trackemail
     * @param type $coincustomer
     * @param type $redirectinvoice
     * @param type $describeinvoice
     * @param type $boxcustomer
     * @param type $nshowcustomer
     * @param type $exchagersend
     * @param type $ticket
     * @param type $department
     * @param type $project
     * @param type $limit
     * @param type $emailpostcolle
     * @param type $fax
     * @param type $birth
     * @param type $sms
     * @param type $sendemailclient
     * @param type $identification
     * @param type $genre
     * @param type $language
     * @param type $userlog
     * @param type $zzstate
     * @param type $numbercustome
     * @param type $citycustome
     * @param type $Complementcustome
     * @param type $districtcustome
     * @return string Description
     */
    /*public function CustomerEdit($id, $name, $street, $zip, $countryselect, $employsselect, $namePerson, $phoneclint, $emailclient, $selectgroup, $idcompany, $phonecompany, $emailcompany, $describe, $taxlass, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $themeinvoice, $payment, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $describeinvoice, $boxcustomer, $nshowcustomer, $exchagersend, $ticket, $department, $project, $limit, $emailpostcolle, $fax, $birth, $sms, $sendemailclient, $identification, $genre, $language, $userlog, $zzstate, $numbercustome, $citycustome, $Complementcustome, $districtcustome) {
        //var_dump($id, $name, $street, $zip, $countryselect, $employsselect, $namePerson, $phoneclint, $emailclient, $selectgroup, $idcompany, $phonecompany, $emailcompany, $describe, $taxlass, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $themeinvoice, $payment, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $describeinvoice, $boxcustomer, $nshowcustomer, $exchagersend, $ticket, $department, $project, $limit, $emailpostcolle, $fax, $birth, $sms, $sendemailclient, $identification, $genre, $language, $userlog, $zzstate, $numbercustome, $citycustome, $Complementcustome, $districtcustome);

        include 'PersonSettingsController.php';
        include 'ZzStateController.php';
        include 'AddressController.php';
        include 'PersonDataController.php';
        include 'PersonCompanyController.php';

        //ultima variaavel pela url relacionada a imagem  - $removerimg

        $name = base64_decode($name);
        $street = base64_decode($street);
        $countryselect = base64_decode($countryselect);
        $citycustome = base64_decode($citycustome);
        $districtcustome = base64_decode($districtcustome);
        $namePerson = base64_decode($namePerson);
        $emailclient = base64_decode($emailclient);
        $emailcompany = base64_decode($emailcompany);

        $pospos = $this->getPosLogin($userlog);
        $customerGpcustomerGp = getEm()->getRepository('CustomerGp')->find(array("idcustomerGp" => $selectgroup));
        $paymentMtdIdpaymentMtd = getEm()->getRepository('PaymentMtd')->find(array("idpaymentMtd" => $payment));

        $departmentIddepartment = getEm()->getRepository('Department')->find(array("iddepartment" => $department));
        $PersonSettings = new PersonSettingsController();
        $personSettingspersonSettings = $PersonSettings->insertPersonSettings($language, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $describeinvoice, $themeinvoice, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $nshowcustomer, $boxcustomer, $exchagersend, $ticket, $project, $limit, $emailpostcolle, $sms, $sendemailclient, $taxlass);
        $personsetting = getEm()->getRepository('PersonSettings')->find(array("idpersonSettings" => $personSettingspersonSettings));
        $ZzStateOB = new ZzStateController();
        $idzzState = $ZzStateOB->insertState($zzstate, $countryselect);
        $zzStatezzState = getEm()->getRepository('ZzState')->find(array("idzzState" => $idzzState));
        $Address = new AddressController();
        $PersonDataOB = new PersonDataController();
        $companycompany = getEm()->getRepository('Company')->find(array("idcompany" => $idcompany)); //ver tabela pos company
        $userOB = getEm()->getRepository('Users')->find(array("idusers" => $employsselect));
        $PersonCompOB = new PersonCompanyController();
        $datateste = new DateTime();


        try {
            $Person = getEm()->getRepository('Person')->find(array("idperson" => $id));

            $Person->setName($name);
            $Person->setPospos($pospos);
            $Person->setCustomerGpcustomerGp($customerGpcustomerGp);
            $Person->setPaymentMtdpaymentMtd($paymentMtdIdpaymentMtd);
            $Person->setDepartmentdepartment($departmentIddepartment);
            $Person->setBirthDate($birth);

            $Person->setZzStatezzState($zzStatezzState);
            $Person->setPersonSettingspersonSettings($personsetting);
            $Person->setGenre($genre);
            $Person->setInfo($describe);
            $Person->setDateCreation($datateste);
            $Person->setContactPerson($namePerson);
            $Person->setElectronicIdentification($identification);
            $Person->setUsersusers($userOB);
            $Person->setActive(1);
            getEm()->persist($Person);
            getEm()->flush();
            if ($removerimg != 0) {
                $this->removeImgCustomer($Person);
            }
            $PersonDataOB->editPersonData("phone", $phoneclint, $Person);
            $PersonDataOB->editPersonData("email", $emailclient, $Person);
            $PersonDataOB->editPersonData("fax", $fax, $Person);
            $PersonDataOB->editPersonData("phonecontact", $phonecompany, $Person);
            $PersonDataOB->editPersonData("phoneconta", $phonecompany, $Person);
            $PersonDataOB->editPersonData("emailconta", $emailcompany, $Person);
            $Address->editAddress($zzStatezzState, $Person, $street, $zip, $numbercustome, $citycustome, $Complementcustome, $districtcustome);
            $PersonCompOB->editPersonCompany($Person, $companycompany);
            //$this->ediTmgCustomer($Person, $_FILES);
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            echo $message = $e->getMessage();
            die();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }
*/
     public function CustomerEdit($id, $name, $street, $zip, $countryselect, $employsselect, $namePerson, $phoneclint, $emailclient, $selectgroup, $idcompany, $phonecompany, $emailcompany, $describe, $taxlass, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $themeinvoice, $payment, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $describeinvoice, $boxcustomer, $nshowcustomer, $exchagersend, $ticket, $department, $project, $limit, $emailpostcolle, $fax, $birth, $sms, $sendemailclient, $identification, $genre, $language, $userlog, $zzstate, $numbercustome, $citycustome, $Complementcustome, $districtcustome, $image) {
       //var_dump($name, $street, $zip, $countryselect, $employsselect, $namePerson, $phoneclint, $emailclient, $selectgroup, $idcompany, $phonecompany, $emailcompany, $describe, $taxlass, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $themeinvoice, $payment, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $describeinvoice, $boxcustomer, $nshowcustomer, $exchagersend, $ticket, $department, $project, $limit, $emailpostcolle, $fax, $birth, $sms, $sendemailclient, $identification, $genre, $language, $userlog, $zzstate, $numbercustome, $citycustome, $Complementcustome, $districtcustome);
       
        include 'PersonSettingsController.php';
        include 'ZzStateController.php';
        include 'AddressController.php';
        include 'PersonDataController.php';
        include 'PersonCompanyController.php';

        $name = base64_decode($name);
        $street = base64_decode($street);
        $countryselect = base64_decode($countryselect);
        $citycustome = base64_decode($citycustome);
        $districtcustome = base64_decode($districtcustome);
        $namePerson = base64_decode($namePerson);
        $emailclient = base64_decode($emailclient);
        $emailcompany = base64_decode($emailcompany);
        $birth = base64_decode($birth);
        $image = base64_decode($image);


        $pospos = $this->getPosLogin($userlog);
        $customerGpcustomerGp = getEm()->
        getRepository('CustomerGp')->find(array("idcustomerGp" => $selectgroup));
        $paymentMtdIdpaymentMtd = getEm()->getRepository('PaymentMtd')->find(array("idpaymentMtd" => $payment));
        $departmentIddepartment = getEm()->getRepository('Department')->find(array("iddepartment" => $department));
        $PersonSettings = new PersonSettingsController();
        $personSettingspersonSettings = $PersonSettings->insertPersonSettings($language, $viewname, $invoiceg, $withoutc, $printcompany, $hideinvoice, $invoiceelectro, $describeinvoice, $themeinvoice, $sendinvoice, $trackemail, $coincustomer, $redirectinvoice, $nshowcustomer, $boxcustomer, $exchagersend, $ticket, $project, $limit, $emailpostcolle, $sms, $sendemailclient, $taxlass);
        $personsetting = getEm()->getRepository('PersonSettings')->find(array("idpersonSettings" => $personSettingspersonSettings));
        $ZzStateOB = new ZzStateController();
        $idzzState = $ZzStateOB->insertState($zzstate, $countryselect);
        $zzStatezzState = getEm()->getRepository('ZzState')->find(array("idzzState" => $idzzState));
        $Address = new AddressController();
        $PersonDataOB = new PersonDataController();
        $companycompany = getEm()->getRepository('Company')->find(array("idcompany" => $idcompany)); //ver tabela pos company
        $userOB = getEm()->getRepository('Users')->find(array("idusers" => $employsselect));
        $PersonCompOB = new PersonCompanyController();
        $datateste = new DateTime();

        try {
            $Person = getEm()->getRepository('Person')->find(array("idperson" => $id));
            $Person->setName($name);
            $Person->setPospos($pospos);
            $Person->setCustomerGpcustomerGp($customerGpcustomerGp);
            $Person->setPaymentMtdpaymentMtd($paymentMtdIdpaymentMtd);
            $Person->setDepartmentdepartment($departmentIddepartment);
            $Person->setBirthDate($birth);
            $Person->setZzStatezzState($zzStatezzState);
            $Person->setPersonSettingspersonSettings($personsetting);
            $Person->setGenre($genre);
            $Person->setInfo($describe);
            $Person->setDateCreation($datateste);
            $Person->setContactPerson($namePerson);
            $Person->setElectronicIdentification($identification);
            $Person->setUsersusers($userOB);
            $Person->setActive(1);
            getEm()->persist($Person);
            getEm()->flush();
            $PersonDataOB->editPersonData("phone", $phoneclint, $Person);
            $PersonDataOB->editPersonData("email", $emailclient, $Person);
            $PersonDataOB->editPersonData("fax", $fax, $Person);
            $PersonDataOB->editPersonData("phonecontact", $phonecompany, $Person);
            $PersonDataOB->editPersonData("phoneconta", $phonecompany, $Person);
            $PersonDataOB->editPersonData("emailconta", $emailcompany, $Person);
            $Address->editAddress($zzStatezzState, $Person, $street, $zip, $numbercustome, $citycustome, $Complementcustome, $districtcustome);
            $PersonCompOB->editPersonCompany($Person, $companycompany);
            $this->ediTmgCustomer($Person, $image);
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * make json in data of Person
     * 
     * @param int $id id of the Person
     * @return string json info
     */
    public function getCustomer($id) {
        try {
            $Person = getEm()->getRepository('Person')->findOneBy(array("idperson" => $id));
            $PersonData = getEm()->getRepository('PersonData')->findBy(array("personperson" => $Person));
            $PersonCompany = getEm()->getRepository('PersonCompany')->findOneBy(array("personperson" => $Person));
            if (!empty($PersonCompany)) {
                $Company = $PersonCompany->getCompanycompany()->getIdcompany();
            } else {
                $Company = "";
            }
            $phone = $email = $phonecontact = $emailcontact = $fax = "";
            foreach ($PersonData as $values) {
                switch ($values->getDataType()) {
                    case "phone":
                        $phone = $values->getDataValue();
                        break;
                    case "email":
                        $email = $values->getDataValue();
                        break;
                    case "phoneconta":
                        $phonecontact = $values->getDataValue();
                        break;
                    case "emailconta":
                        $emailcontact = $values->getDataValue();
                        break;
                    case "fax":
                        $fax = $values->getDataValue();
                        break;
                }
            }

            $imgreturn = $this->getImagByPerson($Person);
            
            if (!empty($imgreturn)) {
                //$imgreturn = '<img class="img-responsive" id="previewing" src="data:image/' . $imgreturn[2] . ';base64,' . $imgreturn[1] . '" width="210px"/>';
                $imgreturn = $imgreturn[1];
            } else {
                $imgreturn = "";
            }
            //var_dump($imgreturn);
            //die();
            $Address = getEm()->getRepository('Address')->findoneby(array("personperson" => $Person));
            if (!empty($Address)) {
                $street = $Address->getAddressStreet();
                $zip = $Address->getAddressZipcode();
                $number = $Address->getAddressNumber();
                $city = $Address->getAddressCity();
                $district = $Address->getAddressDistrict();
                $complement = $Address->getAddressComplement();
            } else {
                $street = "";
                $zip = "";
                $number = "";
                $city = "";
                $district = "";
                $complement = "";
            }
            if ($Person->getUsersusers() != null) {
                $iduser = $Person->getUsersusers()->getIdusers();
            } else {
                $iduser = "";
            }
            if($Person->getPersonSettingspersonSettings() != null){
                if ($Person->getPersonSettingspersonSettings()->getCurrencycurrency() != null) {
                    $currency = $Person->getPersonSettingspersonSettings()->getCurrencycurrency()->getIdcurrency();
                } else {
                    $currency = "";
                }
                if ($Person->getPersonSettingspersonSettings()->getTicketTmptticketTmpt() != null) {
                    $tmptticket = $Person->getPersonSettingspersonSettings()->getTicketTmptticketTmpt()->getIdticketTmpt();
                } else {
                    $tmptticket = "";
                }
                if ($Person->getPersonSettingspersonSettings()->getProjectproject()) {
                    $project = $Person->getPersonSettingspersonSettings()->getProjectproject()->getIdProject();
                } else {
                    $project = "";
                }
            } else {
                $currency = "";
                $tmptticket = "";
                $project = "";
            }

            $dat = array(
                "id" => $Person->getIdperson(), /* okay */
                "name" => $Person->getName(), /* okay */
                "street" => $street, /* okay */
                "zip" => $zip,
                "country" => $Person->getZzStatezzState() != null ? $Person->getZzStatezzState()->getZzCountryzzCountry()->getIdzzCountry() : '',
                "state" => $Person->getZzStatezzState() != null ? $Person->getZzStatezzState()->getIdzzState(): '',

                "phone" => $phone,
                "email" => $email,

                "number" => $number,
                "city" => $city,
                "company" => $Company,
                "district" => $district,
                "complement" => $complement,

                "contactperson" => $Person->getContactPerson(),
                "phonecontact" => $phonecontact,
                "emailcontact" => $emailcontact,
                "customergp" => $Person->getCustomerGpcustomerGp() != null ? $Person->getCustomerGpcustomerGp()->getIdcustomerGp() : '',
                
                "employs" => $iduser,
                "describe1" => $Person->getInfo(),
                "withouttax" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getTaxFree() : '',
                "viewname" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvShowCustomerName() : '',
                "gpinvoice" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvGroupInvoices() : '',
                "withoutcustomer" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvTaxFree() : '',
                "invoiceprint" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvInvoicePrintExclusive() : '',
                "hidepayment" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvHideProducts() : '',
                "electroninvoice" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvHideProducts() : '',
                "tmptinvoice" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvoiceTmptinvoiceTmpt()->getIdinvoiceTmpt() : '',
                "payment" => $Person->getPaymentMtdpaymentMtd() != null ? $Person->getPaymentMtdpaymentMtd()->getIdpaymentMtd() : '',
                "sendemail" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvEmail() : '',
                "trackemail" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvTrackEmail() : '',
                "currency" => $currency,
                "redirectperson" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getPersonperson()->getIdperson() : '',
                "describe2" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvInfo() : '',
                "showinfo" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getOrdShowInfo() : '',
                "notshowdata" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getOrdHideFromStatistics() : '',
                "andresscompany" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getOrdUseComAddress() : '',
                "tmptticket" => $tmptticket,
                "project" => $project,
                "department" => $Person->getDepartmentdepartment() != null ? $Person->getDepartmentdepartment()->getIddepartment(): '',
                "limit" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getOrdCreditLimit() : '',
                "chargetime" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getChargeTimes() : '',
                "fax" => $fax,
                "birt" => $Person->getBirthDate(),
                "electronicinvoice" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getInvElectronicInvoice() : '',
                "genre" => $Person->getGenre(),
                "language" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getLanguagelanguage()->getIdlanguage() : '',
                "sms" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getAuthReceiveSms() : '',
                "emaily" => $Person->getPersonSettingspersonSettings() != null ? $Person->getPersonSettingspersonSettings()->getAuthReceiveEmail() : '',
               // "birth" => $Person->getBirthDate(),
                "dataimg" => $imgreturn,
                "identification" => $Person->getElectronicIdentification()
            );

            $result = 'success';
            $message = 'query success';
            $data = $dat;
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => $data);
        $json_data = json_encode($data);
        print $json_data;
    }

    public static function getImagByPerson($person) {
        $img = getEm()->getRepository('PersonImage')->findby(array("personperson" => $person));
        if (!empty($img)) {
            $imgdata = $img[0]->getPath();
            $array_return = array(1 => $imgdata, 2 => $img[0]->getTypeimg());
        } else {
            $array_return = "";
        }
        return $array_return;
    }

    /**
     * Edit image Customer
     * 
     * @param Array $personperson
     * @param file $file
     * @return void 
     */
    public static function ediTmgCustomer($personperson, $image) {

        //if (!empty($file["file"]["name"])) {

           // $nameOLD = $file["file"]["name"];
           // $typeimg = explode("/", $file["file"]["type"]);
           // $typeFile = explode(".", $nameOLD);
           // $nameFile = time() . '.' . $typeFile[1];
           // $uploads_dir = '../data/temp/';

           // if (move_uploaded_file($file["file"]["tmp_name"], $uploads_dir . $nameFile)) {
               // sleep(5);
                //$imagem = file_get_contents($uploads_dir . $nameFile);
                //$mysqlImg = base64_encode($imagem);
                try {
                    $PersonImage = getEm()->getRepository('PersonImage')->findby(array("personperson" => $personperson));
                   $PersonImage[0]->setPath($image);
                    $PersonImage[0]->setActive(1);
                    getEm()->persist($PersonImage[0]);
                    getEm()->flush();
                } catch (Exception $e) {

                    echo $e->getMessage();
                    die();
                }
               // unlink($uploads_dir . $nameFile);
            //}
       // }
    }
    
    /**
     * Get All data of Customers for reports
     * 
     * @return string json
     */
    public function getAllClientReports() {
        try {
            $CustomerGp = getEm()->getRepository('Person')->findBy(array("active" => 1));
            $data = array();
            foreach ($CustomerGp as $value) {
                if($value->getCustomerGpcustomerGp()){
                    $name = $value->getName();
                    $address = "T";
                    $zip = "E";
                    $city = "E";
                    $typePayment = "E";
                    
                    $dat = array(
                        "id" => $value->getIdcustomerGp(),
                        "name" => $name,
                        "address" => $address,
                        "zip" => $zip,
                        "city" => $city,
                        "typePayment" => $typePayment
                    );
                    array_push($data, $dat);
                }
                
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }

}
