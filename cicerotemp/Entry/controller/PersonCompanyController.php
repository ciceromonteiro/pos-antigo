<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Feb 13, 2017
 */
class PersonCompanyController {

    public function __contruct() {
        
    }

    /**
     * Default of person company
     * 
     * @param Array $Person
     * @param Array $Company
     * @return void
     */
    public function insertPersonCompany($Person, $Company) {
        try {
            $PersonCompany = new PersonCompany();
            $PersonCompany->setPersonperson($Person);
            $PersonCompany->setCompanycompany($Company);
            getEm()->persist($PersonCompany);
            getEm()->flush();
            //AuthenticationController::insertLog('create', 'personcompany', $Person->getName());
        } catch (Exception $e) {
            print $e;
            exit(1);
        }
    }

    /**
     * Update of Person Company
     * 
     * @param int $Person
     * @param int $Company
     * @return void
     */
    public function editPersonCompany($Person, $Company) {
        try {
            $PersonCompany = getEm()->getRepository('PersonCompany')->findoneby(array("personperson" => $Person));
            $PersonCompany->setCompanycompany($Company);
            getEm()->persist($PersonCompany);
            getEm()->flush();
            //AuthenticationController::insertLog('edit', 'personcompany', $Person->getName());
        } catch (Exception $e) {
            print $e;
            exit(1);
        }
    }

}
