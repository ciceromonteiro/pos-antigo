<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com> 
 * @version 1.0.0
 * @abstract file created in date Oct 22, 2016
 */
class PermissionController {

    public function index() {

        $navbar = "Entry|permission";
        $array_answero = getEm()->getRepository('Permission');
        $array_answeroB = $array_answero->findAll();  
        GenericController::template("Entry", "permission", "index", $navbar, $array_answeroB, 207);
    }

    

    /**
     * 
     * @param string $name
     * @param int $company
     */
    public function addUsertype($name, $company) {
        $usertype = new UserType;

        $usertype->setName($name);

        $usertype->setCompanycompany($company);

        getEm()->persist($usertype);

        getEm()->flush();
    }

    /**
     * 
     * @param int $userTypeuserType
     * @param int $permissionpermission
     */
    public function addDefaultpermission($userTypeuserType, $permissionpermission) {
        $usertypedefault = new UserTypeDefaultPermission;

        $usertypedefault->setUserTypeuserType($userTypeuserType);

        $usertypedefault->setPermissionpermission($permissionpermission);

        getEm()->persist($usertypedefault);

        getEm()->flush();
    }

    /**
     * 
     * @param string $name
     * @param int $show
     * @param int $id
     */
    public function editPermission($name, $show, $id) {

        $permission = getEm()->find('Permission', $id);

        $permission->setName($name);

        $permission->setShow($show);


        getEm()->persist($permission);

        getEm()->flush();
    }

    /**
     * 
     * @param string $name
     * @param int $company
     * @param int $id
     */
    public function editUsertype($name, $company, $id) {
        $usertype = getEm()->find('UserType', $id);

        $usertype->setName($name);

        $usertype->setCompanycompany($company);

        getEm()->persist($usertype);

        getEm()->flush();
    }

    /**
     * 
     * @param int $userTypeuserType
     * @param int $permissionpermission
     * @param int $id
     */
    public function editDefaultpermission($userTypeuserType, $permissionpermission, $id) {
        $usertypedefault = getEm()->find('UserTypeDefaultPermission', $id);

        $usertypedefault->setUserTypeuserType($userTypeuserType);

        $usertypedefault->setPermissionpermission($permissionpermission);

        getEm()->persist($usertypedefault);

        getEm()->flush();
    }

    public function getAll() {
        $em = getEm();
        $custom = $em->getRepository('Permission');
        $colors = $custom->findAll();
        $data = array();

        foreach ($colors as $value) {
            $dat = array(
                "id" => $value->getIddepartment(),
                "name" => $value->getName()
            );
            array_push($data, $dat);
        }

        $values = array(
            "draw" => "1",
            "recordsTotal" => count($colors),
            "recordsFiltered" => count($colors),
            "data" => $data
        );

        echo json_encode($values);
    }

}
