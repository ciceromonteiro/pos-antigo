<?php
/**
 * Created by PhpStorm.
 * User: Rocha
 * Date: 15/11/2016
 * Time: 06:12
 */

class SupplierController {
    public function index(){
        $navbar = "Entry|supplier";
        $country = getEm()->getRepository('ZzCountry')->findBy(array ('active' => 1));
        $array_answer = array (
            'country' => $country
        );
        GenericController::template("Entry", "supplier","index", $navbar, $array_answer, 106);
    }
    
    public function create(){
        $navbar = "Entry|supplier";
        $array_answer = array ("");
        GenericController::template("Entry", "supplier","create_and_update", $navbar, $array_answer, 241);
    }

    public static function getAll($returnArray = false){
        try{
            $supplier = getEm()->getRepository('Supplier')->findBy(array ('active' => 1));
            $data = array ();
            foreach ($supplier as $value){
                $nameCompany = $value->getCompanycompany()->getName();
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdsupplier().'" data-name="'.$nameCompany.'">',
                    "id" => $value->getIdsupplier(),
                    "description" => $nameCompany
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        if ($returnArray == true) {
            return $supplier;
        }

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getSupplier(){
        if($_POST['id']){
            $supplier = getEm()->getRepository('Supplier')->findBy(array('active' => 1, 'idsupplier' => $_POST['id']));
            $company_data = getEm()->getRepository('CompanyData')->findBy(array('active' => 1, 'companycompany' => $supplier[0]->getCompanycompany()));
            $address = getEm()->getRepository('Address')->findBy(array('active' => 1, 'companycompany' => $supplier[0]->getCompanycompany()));
            $personCompany = getEm()->getRepository('PersonCompany')->findBy(array('companycompany' => $supplier[0]->getCompanycompany()));
            $personData = getEm()->getRepository('PersonData')->findBy(array('personperson' => $personCompany[0]->getPersonperson()->getIdperson()));
            
            $values['idSupplier']               = $_POST['id'];
            $values['nameFromSupplier']         = $supplier[0]->getCompanycompany()->getName();
            $values['orgNrContactPerson']       = $supplier[0]->getCompanycompany()->getRegisterNumber();
            $values['obs']                      = $supplier[0]->getCompanycompany()->getInfo();
            $values['contactPersonFullName']    = $personCompany[0]->getPersonperson()->getName();
            
            for($i=0; $i < count($company_data); $i++){
                $type = $company_data[$i]->getDataType();
                $value = $company_data[$i]->getDataValue();
                $values[$type] = $value;
            }
            
            for($i=0; $i < count($address); $i++){
                $typeAddress = $address[$i]->getName();
                if($typeAddress == "Address of establishment"){
                    $values['addressEstablishment'] = $address[$i]->getAddressStreet();
                    $values['zipCodeEstablishment'] = $address[$i]->getAddressZipcode();
                } 
                if($typeAddress == "Mailing address"){
                    $values['mailingAddress'] = $address[$i]->getAddressStreet();
                    $values['zipCodeMailingAddress'] = $address[$i]->getAddressZipcode();
                }               
            }
            
            for($i=0; $i < count($personData); $i++){
                $type = $personData[$i]->getDataType();
                $value = $personData[$i]->getDataValue();
                $values[$type] = $value;
            }
            
            $data = array(
                "result"  => 'success',
                "message" => '',
                "data"    => $values
            );        
            $json_data = json_encode($data);
            print $json_data;
        } else {
            $data = array(
                "result"  => 'error',
                "message" => 'id null',
                "data"    => ''
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function insert(){
        $result = false;
        $idPos = AuthenticationController::getPos();
        $values = array (
            'idSupplier'            => isset($_POST['idSupplier']) && $_POST['idSupplier'] != '' ? $_POST['idSupplier'] : '',
            'yourIdOnSupplier'      => isset($_POST['yourIdOnSupplier']) && $_POST['yourIdOnSupplier'] != '' ? $_POST['yourIdOnSupplier'] : '',
            'nameFromSupplier'      => isset($_POST['nameFromSupplier']) && $_POST['nameFromSupplier'] != '' ? $_POST['nameFromSupplier'] : '',
            'country'               => isset($_POST['country']) && $_POST['country'] != '' ? $_POST['country'] : '',
            'language'              => isset($_POST['language']) && $_POST['language'] != '' ? $_POST['language'] : '',
            'accountOnBank'         => isset($_POST['accountOnBank']) && $_POST['accountOnBank'] != '' ? $_POST['accountOnBank'] : '',
            'discountDefault'       => isset($_POST['discountDefault']) && $_POST['discountDefault'] != '' ? $_POST['discountDefault'] : '',
            'phone1'                => isset($_POST['phone1']) && $_POST['phone1'] != '' ? $_POST['phone1'] : '',
            'email1'                => isset($_POST['email1']) && $_POST['email1'] != '' ? $_POST['email1'] : '',
            'webSite1'              => isset($_POST['webSite1']) && $_POST['webSite1'] != '' ? $_POST['webSite1'] : '',
            'addressEstablishment'  => isset($_POST['addressEstablishment']) && $_POST['addressEstablishment'] != '' ? $_POST['addressEstablishment'] : '',
            'zipCodeEstablishment'  => isset($_POST['zipCodeEstablishment']) && $_POST['zipCodeEstablishment'] != '' ? $_POST['zipCodeEstablishment'] : '',
            'mailingAddress'        => isset($_POST['mailingAddress']) && $_POST['mailingAddress'] != '' ? $_POST['mailingAddress'] : '',
            'zipCodeMailingAddress' => isset($_POST['zipCodeMailingAddress']) && $_POST['zipCodeMailingAddress'] != '' ? $_POST['zipCodeMailingAddress'] : '',
            'phoneMailing'          => isset($_POST['phoneMailing']) && $_POST['phoneMailing'] != '' ? $_POST['phoneMailing'] : '',
            'faxMailing'            => isset($_POST['faxMailing']) && $_POST['faxMailing'] != '' ? $_POST['faxMailing'] : '',
            'glnMailing'            => isset($_POST['glnMailing']) && $_POST['glnMailing'] != '' ? $_POST['glnMailing'] : '',
            'contactPersonFullName' => isset($_POST['contactPersonFullName']) && $_POST['contactPersonFullName'] != '' ? $_POST['contactPersonFullName'] : '',
            'phoneContactPerson'    => isset($_POST['phoneContactPerson']) && $_POST['phoneContactPerson'] != '' ? $_POST['phoneContactPerson'] : '',
            'emailContactPerson'    => isset($_POST['emailContactPerson']) && $_POST['emailContactPerson'] != '' ? $_POST['emailContactPerson'] : '',
            'webSiteContactPerson'  => isset($_POST['webSiteContactPerson']) && $_POST['webSiteContactPerson'] != '' ? $_POST['webSiteContactPerson'] : '',
            'orgNrContactPerson'    => isset($_POST['orgNrContactPerson']) && $_POST['orgNrContactPerson'] != '' ? $_POST['orgNrContactPerson'] : '',
            'obs'                   => isset($_POST['obs']) && $_POST['obs'] != '' ? $_POST['obs'] : '',
            'sendOrder'             => isset($_POST['sendOrder']) && $_POST['sendOrder'] != '' ? $_POST['sendOrder'] : '',
        );
        $key_names = array_keys($values);
        
        try {
            $company = new Company();
            $company->setIdcompany($values['idSupplier']);
            $company->setPospos($idPos);
            $company->setName($values['nameFromSupplier']);
            $company->setFantasyName($values['nameFromSupplier']);
            $company->setRegisterNumber($values['orgNrContactPerson']);
            $company->setActive(true);
            $company->setInfo($values['obs']);
            $endIdCompany = $company;
            getEm()->persist($company);
            getEm()->flush();
            
            AuthenticationController::insertLog('create', 'supplier', $_POST['nameFromSupplier']);
            
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        
        $dataTypes = array(
            0 => 'language',
            1 => 'accountOnBank',
            2 => 'discountDefault',
            3 => 'phone1',
            4 => 'email1',
            5 => 'webSite1',
            6 => 'glnMailing',
            7 => 'yourIdOnSupplier',
            8 => 'country',
        );
        
        try {
            for($i=0; $i < count($dataTypes); $i++){
                for($j = 0; $j < count($key_names); $j++){
                    if($dataTypes[$i] == $key_names[$j]){
                        $key = $key_names[$j];
                        $company_data = new CompanyData();
                        $company_data->setCompanycompany($endIdCompany);
                        $company_data->setDataType($dataTypes[$i]);
                        $company_data->setDataValue($values[$key]);
                        $company_data->setActive(true);
                        getEm()->persist($company_data);
                        getEm()->flush();   
                    }
                }
            }
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        try {
            $address = new Address();
            $address->setCompanycompany($endIdCompany);
            $address->setName('Address of establishment');
            $address->setVisitingAddress('0');
            $address->setAddressStreet($_POST['addressEstablishment']);
            $address->setAddressZipcode($_POST['zipCodeMailingAddress']);
            $address->setDateCreate(new DateTime());
            $address->setActive(true);
            getEm()->persist($address);
            getEm()->flush();
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        
        try {
            $visiting_address = new Address();
            $visiting_address->setCompanycompany($endIdCompany);
            $visiting_address->setName('Mailing address');
            $visiting_address->setVisitingAddress('1');
            $visiting_address->setAddressStreet($_POST['mailingAddress']);
            $visiting_address->setAddressZipcode($_POST['zipCodeMailingAddress']);
            $visiting_address->setActive(true);
            $endIdAddress = $visiting_address->getIdaddress();
            $values['AddressIdAddress'] = $endIdAddress;
            getEm()->persist($visiting_address);
            getEm()->flush();
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $dataTypes_mailing_address = array(
            0 => 'phoneMailing',
            1 => 'faxMailing',
            2 => 'glnMailing',
            3 => 'AddressIdAddress'
        );
        
        try {
            for($i=0; $i < count($dataTypes_mailing_address); $i++){
                for($j = 0; $j < count($key_names); $j++){
                    if($dataTypes_mailing_address[$i] == $key_names[$j]){
                        $key = $key_names[$j];
                        $company_data = new CompanyData();
                        $company_data->setCompanycompany($endIdCompany);
                        $company_data->setDataType($dataTypes_mailing_address[$i]);
                        $company_data->setDataValue($values[$key]);
                        $company_data->setDateCreate(new DateTime());
                        $company_data->setActive(true);
                        getEm()->persist($company_data);
                        getEm()->flush();
                    }
                }
            }
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $dataTypes_person = array(
            0  => 'phoneContactPerson',
            1  => 'emailContactPerson',
            2  => 'webSiteContactPerson',
            3  => 'orgNrContactPerson'
        );
        
        try {
            $customerGroup = getEm()->getRepository('CustomerGp')->findOneBy(array('active' => 1,'name' => 'Default'));
            if(!empty($customerGroup)){
                $person->setCustomerGpcustomerGp($customerGroup);
            }
            $person = new Person();
            $person->setName($_POST['contactPersonFullName']);
            $person->setActive(true);
            $endIdPerson = $person;
            getEm()->persist($person);
            getEm()->flush();
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }
            
        try {
            for($i=0; $i < count($dataTypes_person); $i++){
                for($j = 0; $j < count($key_names); $j++){
                    if($dataTypes_person[$i] == $key_names[$j]){
                        $key = $key_names[$j];
                        $person_data = new PersonData();
                        $person_data->setPersonperson($endIdPerson);
                        $person_data->setDataType($dataTypes_person[$i]);
                        $person_data->setDataValue($values[$key]);
                        $person_data->setActive(true);
                        getEm()->persist($person_data);
                        getEm()->flush();
                    }
                }
            }
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        try {
            $personCompany = new PersonCompany();
            $personCompany->setPersonperson($endIdPerson);
            $personCompany->setCompanycompany($endIdCompany);
            getEm()->persist($personCompany);
            getEm()->flush();
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        try {
            $supplier = new Supplier();
            $supplier->setCompanycompany($endIdCompany);
            $supplier->setDateCreate(new DateTime());
            $supplier->setActive(true);
            getEm()->persist($supplier);
            getEm()->flush();
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    
        
        if($result != "error" || $result != false){
            $result  = 'success';
            $message = 'query success';
            $data = "";
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function update(){
        $navbar = "Entry|supplier";
        $country = getEm()->getRepository('ZzCountry')->findAll();
        $array_answer = array (
            'country' => $country
        );
        GenericController::template("Entry", "supplier","create_and_update", $navbar, $array_answer);
    }
    
    public function edit(){
        $idPos = AuthenticationController::getPos();        
        $supplier = getEm()->getRepository('Supplier')->findBy(array('active' => 1, 'idsupplier' => $_POST['idSupplier']));
        $company_data = getEm()->getRepository('CompanyData')->findBy(array('active' => 1, 'companycompany' => $supplier[0]->getCompanycompany()));
        $address = getEm()->getRepository('Address')->findBy(array('active' => 1, 'companycompany' => $supplier[0]->getCompanycompany()));
        $personCompany = getEm()->getRepository('PersonCompany')->findBy(array('companycompany' => $supplier[0]->getCompanycompany()));
        $personData = getEm()->getRepository('PersonData')->findBy(array('personperson' => $personCompany[0]->getPersonperson()->getIdperson()));
        
        try {
            $company = getEm()->getRepository('Company')->findBy(array('active' => 1, 'idcompany' => $supplier[0]->getCompanycompany()->getIdcompany()));
            $company[0]->setName($_POST['nameFromSupplier']);
            $company[0]->setRegisterNumber($_POST['orgNrContactPerson']);
            $company[0]->setInfo($_POST['obs']);
            $company[0]->setDateUpdate(new DateTime());
            getEm()->persist($company[0]);
            getEm()->flush();
            
            AuthenticationController::insertLog('update', 'supplier', $_POST['nameFromSupplier']);
            
            $dataTypes = array(
                'language'=> $_POST['language'],
                'accountOnBank'=> $_POST['accountOnBank'],
                'discountDefault'=> $_POST['discountDefault'],
                'phone1'=> $_POST['phone1'],
                'email1'=> $_POST['email1'],
                'webSite1'=> $_POST['webSite1'],
                'glnMailing'=> $_POST['glnMailing'],
                'yourIdOnSupplier'=> $_POST['yourIdOnSupplier'],
                'country'=> $_POST['country'],
                'phoneMailing'=> $_POST['phoneMailing'],
                'faxMailing'=> $_POST['faxMailing'],
            );
            $keys = array_keys($dataTypes);
            
            for($i=0; $i < count($dataTypes); $i++){
                $company_data[$i]->setDataType($keys[$i]);
                $company_data[$i]->setDataValue($_POST[$keys[$i]]);
                $company_data[$i]->setDateUpdate(new DateTime());
                getEm()->persist($company_data[$i]);
                getEm()->flush();
            }
            
            for($i=0; $i < count($address); $i++){
                if($address[$i]->getName() == "Address of establishment"){
                    $address[$i]->setAddressStreet($_POST['addressEstablishment']);
                    $address[$i]->setAddressZipcode($_POST['zipCodeMailingAddress']);
                }
                if($address[$i]->getName() == "Mailing address"){
                    $address[$i]->setAddressStreet($_POST['addressEstablishment']);
                    $address[$i]->setVisitingAddress('1');
                    $address[$i]->setAddressZipcode($_POST['zipCodeMailingAddress']);
                    
                }
                $address[$i]->setDateUpdate(new DateTime());
                getEm()->persist($address[$i]);
                getEm()->flush();
            }
            
            $person = getEm()->getRepository('Person')->findBy(array('active' => 1, 'idperson' => $personData[0]->getPersonperson()));
            $person[0]->setName($_POST['contactPersonFullName']);
            $person[0]->setDateUpdate(new DateTime());
            getEm()->persist($person[0]);
            getEm()->flush();
            
            $dataTypes_person = array(
                'phoneContactPerson'    => $_POST['phoneContactPerson'],
                'emailContactPerson'    => $_POST['emailContactPerson'],
                'webSiteContactPerson'  => $_POST['webSiteContactPerson'],
                'orgNrContactPerson'    => $_POST['orgNrContactPerson'],
            );
            
            $keys = array_keys($dataTypes_person);
            for($j = 0; $j < count($dataTypes_person); $j++){
                $nameKey = $keys[$j];
                $personData[$j]->setDataType($nameKey);
                $personData[$j]->setDataValue($dataTypes_person[$nameKey]);
                $personData[$j]->setDateUpdate(new DateTime());
                getEm()->persist($personData[$j]);
                getEm()->flush();
            }
            $supplier[0]->setDateUpdate(new DateTime());
            getEm()->persist($supplier[0]);
            getEm()->flush();
            
            $result  = 'success';
            $message = 'Successful';
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => ""
        );        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function delete(){
        if($_POST['idSuppliers']){
            try{
                $ids = $_POST['idSuppliers'];
                for($i=0; $i < count($ids); $i++){
                    $supplier = getEm()->getRepository("Supplier")->findOneBy(array("idsupplier" => $ids[$i]));
                    $supplier->setActive('3');
                    $supplier->setDateDelete(new DateTime());
                    getEm()->persist($supplier);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'supplier', $supplier->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function apis() {
        $navbar = "Entry|supplier";
        $supplier = getEm()->getRepository('Supplier')->findBy(array('active' => 1));
        $type_api = getEm()->getRepository('SupplierTypeApi')->findBy(array('active' => 1));
        $format_api = getEm()->getRepository('SupplierFormatApi')->findBy(array('active' => 1));
        $array_answer = array (
            "supplier"   => $supplier,
            "type_api"   => $type_api,
            "format_api" => $format_api
        );
        GenericController::template("Entry", "supplier","apis", $navbar, $array_answer, 247);
    }
    
    public function getAllApis(){
        try{
            $supplier = getEm()->getRepository('SupplierApi')->findBy(array ('active' => 1));
            $data = array ();
            foreach ($supplier as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdsupplierapi().'">',
                    "id" => $value->getIdsupplierapi(),
                    "supplier" => $value->getIdsupplier()->getCompanycompany()->getName(),
                    "typeapi" => $value->getIdtypeapi()->getName(),
                    "formatapi" => $value->getIdformatapi()->getName(),
                    "address" => $value->getAddress(),
                    "username" => $value->getUsername(),
                    "password" => '***',
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function insertApi(){
        try {
            $supplier = getEm()->getRepository('Supplier')->findBy(array('active' => 1, 'idsupplier' => $_POST['supplier']));
            $typeApi = getEm()->getRepository('SupplierTypeApi')->findBy(array('active' => 1, 'idsuppliertypeapi' => $_POST['typeApi']));
            $typeFormat = getEm()->getRepository('SupplierFormatApi')->findBy(array('active' => 1, 'idsupplierformatapi' => $_POST['typeFormat']));
            
            $supplierApi = new SupplierApi();
            $supplierApi->setIdsupplier($supplier[0]);
            $supplierApi->setIdtypeapi($typeApi[0]);
            $supplierApi->setIdformatapi($typeFormat[0]);
            $supplierApi->setAddress($_POST['address']);
            $supplierApi->setUsername($_POST['username']);
            $supplierApi->setPassword($_POST['password']);
            $supplierApi->setDateCreate(new DateTime());
            $supplierApi->setActive(true);
            getEm()->persist($supplierApi);
            getEm()->flush();
            
            AuthenticationController::insertLog('create', 'supplier api', $_POST['username']);

            $result  = 'success';
            $message = 'query success';
            $data = "";

        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getSupplierApi(){
        if(isset($_POST['id'])){
            try {
                $supplierApi = getEm()->getRepository('SupplierApi')->findBy(array("idsupplierapi" => $_POST['id']));
                $array_data = array ();
                foreach ($supplierApi as $value){
                    $dat = array (
                        "id" => $value->getIdsupplierapi(),
                        "supplier" => $value->getIdsupplier()->getIdsupplier(),
                        "typeApi" => $value->getIdtypeapi()->getIdsuppliertypeapi(),
                        "typeFormat" => $value->getIdformatapi()->getIdsupplierformatapi(),
                        "address" => $value->getAddress(),
                        "username" => $value->getUsername(),
                        "password" => $value->getPassword()
                    );
                    array_push($array_data, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateApi(){
        try {
            $supplier = getEm()->getRepository('Supplier')->findOneBy(array('active' => 1, 'idsupplier' => $_POST['supplier']));
            $typeApi = getEm()->getRepository('SupplierTypeApi')->findOneBy(array('active' => 1, 'idsuppliertypeapi' => $_POST['typeApi']));
            $typeFormat = getEm()->getRepository('SupplierFormatApi')->findOneBy(array('active' => 1, 'idsupplierformatapi' => $_POST['typeFormat']));  
            $supplierApi = getEm()->getRepository('SupplierApi')->findOneBy(array( "idsupplierapi" => $_POST['idSupplierApi']));
            
            $supplierApi->setIdsupplier($supplier);
            $supplierApi->setIdtypeapi($typeApi);
            $supplierApi->setIdformatapi($typeFormat);
            $supplierApi->setAddress($_POST['address']);
            $supplierApi->setUsername($_POST['username']);
            $supplierApi->setPassword($_POST['password']);
            $supplierApi->setDateUpdate(new DateTime());
            $supplierApi->setActive(true);
            getEm()->persist($supplierApi);
            getEm()->flush();
            
            AuthenticationController::insertLog('update', 'supplier api', $_POST['username']);

            $result  = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteApis(){
        if($_POST['idSuppliersApis']){
            try{
                $ids = $_POST['idSuppliersApis'];
                for($i=0; $i < count($ids); $i++){
                    $supplier = getEm()->getRepository("SupplierApi")->findOneBy(array("idsupplierapi" => $ids[$i]));
                    $supplier->setActive(0);
                    $supplier->setDateDelete(new DateTime());
                    getEm()->persist($supplier);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'supplier api', $supplier->getUsername());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
}