<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Feb 6, 2017
 */
class AddressController {

    public function __contruct() {
        
    }

    /**
     * Insert Address
     * 
     * @param int $zzStatezzState
     * @param int $Person
     * @param string $street
     * @param string $zip
     * @param int $numbercustome
     * @param string $citycustome
     * @param string $Complementcustome
     * @param text $districtcustome
     * @return void
     */
    public function insertAddress($zzStatezzState, $Person, $street, $zip, $numbercustome, $citycustome, $Complementcustome, $districtcustome) {

        try {
            $Address2 = new Address();
            $Address2->setZzStatezzState($zzStatezzState);
            $Address2->setPersonperson($Person);
            $Address2->setAddressStreet($street);
            $Address2->setAddressNumber($numbercustome);
            $Address2->setAddressCity($citycustome);
            $Address2->setAddressZipcode($zip);
            $Address2->setAddressComplement($Complementcustome);
            $Address2->setAddressDistrict($districtcustome);
            $Address2->setActive(1);
            getEm()->persist($Address2);
            getEm()->flush();
           // AuthenticationController::insertLog('create', 'address', $_POST['nameColor']);
        } catch (Exception $e) {
            print $e;
            exit(1);
        }
    }
    
    /**
     * Edit Address
     * 
     * @param int $zzStatezzState
     * @param int $Person
     * @param string $street
     * @param string $zip
     * @param int $numbercustome
     * @param string $citycustome
     * @param text $Complementcustome
     * @param int $districtcustome
     * @return void
     */
    public function editAddress($zzStatezzState, $Person, $street, $zip, $numbercustome, $citycustome, $Complementcustome, $districtcustome) {

        try {
            $Address2 = getEm()->getRepository('Address')->findoneby(array("personperson" => $Person));
            $Address2->setZzStatezzState($zzStatezzState);
            $Address2->setPersonperson($Person);
            $Address2->setAddressStreet($street);
            $Address2->setAddressNumber($numbercustome);
            $Address2->setAddressCity($citycustome);
            $Address2->setAddressZipcode($zip);
            $Address2->setAddressComplement($Complementcustome);
            $Address2->setAddressDistrict($districtcustome);
            $Address2->setActive(1);
            getEm()->persist($Address2);
            getEm()->flush();
        } catch (Exception $e) {
            print $e;
            exit(1);
        }
    }

}
