<?php
/**
 * Created by PhpStorm.
 * User: rocha
 * Date: 11/1/16
 * Time: 2:06 PM
 */

class ProductSizeController{

    public static function index(){
        include 'ColorController.php';
        $navbar = "Entry|ProductSize";
        $array_answer = array(
            'colors' => ColorController::getAll(true),
        );
        GenericController::template("Entry", "productSize","index", $navbar, $array_answer, 231);
    }

    public static function getAll($returnArray = false){
        try{
            $size = getEm()->getRepository('Size')->findBy(array("active" => 1));
            $data = array ();
            foreach ($size as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdSize().'" data-name="'.$value->getName().'">',
                    "id" => $value->getIdSize(),
                    "description" => $value->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        if ($returnArray == true) {
            return $size;
        }
        
        $json_data = json_encode($data);
        print $json_data;

    }

    public function insert(){
        if(isset($_POST['nameSize'])){
            try {
                $size = new Size();
                $size->setName($_POST['nameSize']);
                $size->setDateCreate(new DateTime());
                $size->setActive(true);
                getEm()->persist($size);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'product size', $_POST['nameSize']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getSize(){
        if(isset($_POST['id'])){
            try {
                $size = getEm()->getRepository('Size')->findBy(array("idsize" => $_POST['id']));
                $mysqlData = array ();
                foreach ($size as $value){
                    $dat = array (
                        "description" => $value->getName(),
                        "id" => $value->getIdSize()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function update(){
        if(isset($_POST['nameSize']) && isset($_POST['idProductSize'])){
            try {
                $size = getEm()->getRepository('Size')->findBy(array("idsize" => $_POST['idProductSize']));
                $size[0]->setName($_POST['nameSize']);
                $size[0]->setDateUpdate(new datetime());
                $size[0]->setActive(true);
                getEm()->persist($size[0]);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'product size', $_POST['nameSize']);

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function delete(){
        if($_POST['idProductSizes']){
            try{
                $ids = $_POST['idProductSizes'];
                for($i=0; $i < count($ids); $i++){
                    $size = getEm()->getRepository("Size")->findOneBy(array("idsize" => $ids[$i]));
                    $size->setActive('3');
                    $size->setDateDelete(new DateTime());
                    getEm()->persist($size);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'product size', $size->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
}