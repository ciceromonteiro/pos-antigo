<?php

class ColorController {

    public static function index(){
        $navbar = "Entry|color";
        $array_answer = array ("");
        GenericController::template("Entry", "color","index", $navbar, $array_answer, 235);
    }

    public static function index2(){
        $navbar = "Entry|color";
        $array_answer = array("");
        GenericController::template("Entry", "color", "index2", $navbar, $array_answer, 235);
    }

    public static function getAll($returnArray = false){
        try{
            $colors = getEm()->getRepository('Color')->findBy(array("active" => 1));
            $data = array ();
            foreach ($colors as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdcolor().'" data-name="'.$value->getName().'">',
                    "id" => $value->getIdcolor(),
                    "description" => $value->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        if ($returnArray == true) {
            return $colors;
        }
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public static function insert(){

        if(isset($_POST['nameColor'])){
            try {
                $color = new Color();
                $color->setName($_POST['nameColor']);
                $color->setDateCreate(new DateTime());
                $color->setActive(true);
                getEm()->persist($color);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'color', $_POST['nameColor']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function getColor(){
        if(isset($_POST['id'])){
            try {
                $color = getEm()->getRepository('Color')->findBy(array("idcolor" => $_POST['id']));
                $array_data = array ();
                foreach ($color as $value){
                    $dat = array (
                        "description" => $value->getName(),
                        "id" => $value->getIdcolor()
                    );
                    array_push($array_data, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function update(){
        if(isset($_POST['nameColor']) && isset($_POST['idColor'])){
            try {
                $color = getEm()->getRepository('Color')->findBy(array( "idcolor" => $_POST['idColor']));
                $color[0]->setName($_POST['nameColor']);
                $color[0]->setDateUpdate(new DateTime());
                $color[0]->setActive(true);
                getEm()->persist($color[0]);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'project', $_POST['nameColor']);

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function deleteColors(){
        if($_POST['idColors']){
            try{
                $ids = $_POST['idColors'];
                for($i=0; $i < count($ids); $i++){
                    $color = getEm()->getRepository("Color")->findOneBy(array("idcolor" => $ids[$i]));
                    $color->setActive('3');
                    $color->setDateDelete(new DateTime());
                    getEm()->persist($color);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'color', $color->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

}