<?php
/**
 * Created by PhpStorm.
 * User: rocha
 * Date: 10/10/16
 * Time: 10:00 AM
 */

$menuLeft = $navbar;

if($navbar != null){
    $menu = explode('|', $menuLeft);
} else {
    $menu = array(0 => "", 1 => "");
}

?>

<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li <?php echo ($navbar == NULL) ? 'class="active"' : '' ?>>
            <a href="<?php echo URL_BASE.'Dashboard/home/index'?>">
                <span class="lnr lnr-calendar-full"></span>
                <t class="translate">Dashboard</t>
            </a>
        </li>
        <li <?php echo ($menu[0] == "Setup" || $menu[1] == "currency") ? 'class="active"' : '' ?>>
            <a href="<?php echo URL_BASE.'Setup/setup/index/company'?>">
                <span class="lnr lnr-cog"></span>
                <t class="translate">Setup</t>
            </a>
        </li>
        <li <?php echo ($menu[0] == "Pos") ? 'class="active"' : '' ?>>
            <a href="<?php echo URL_BASE.'Pos/settings/index'?>">
                <span class="lnr lnr-laptop-phone"></span>
                <t class="translate">POS</t>
            </a>
        </li>
        <li <?php echo ($menu[0] == "Entry") ? 'class="active"' : '' ?>>
            <a href="<?php echo URL_BASE.'Entry/department/index'?>">
                <span class="lnr lnr-file-add"></span>
                <t class="translate">Entry</t>
            </a>
        </li>
        <li <?php echo ($menu[0] == "Devices") ? 'class="active"' : '' ?>>
            <a href="<?php echo URL_BASE.'Devices/printers/index'?>">
                <span class="lnr lnr-link"></span>
                <t class="translate">Devices</t>
            </a>
        </li>
        <li <?php echo ($menu[0] == "Reports") ? 'class="active"' : '' ?>>
            <a href="<?php echo URL_BASE.'Reports/product/index'?>">
                <span class="lnr lnr-pie-chart"></span>
                <t class="translate">Reports</t>
            </a>
        </li>
        <li <?php echo ($menu[0] == "Sales") ? 'class="active"' : '' ?>>
            <a href="<?php echo URL_BASE.'Sales/resume/index'?>">
                <span class="lnr lnr-list"></span>
                <t class="translate">Sales</t>
            </a>
        </li>
    </ul>
</div>

<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">