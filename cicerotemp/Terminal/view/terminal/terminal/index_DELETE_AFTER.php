<?php

$products = json_decode($view['products']);
$customers = json_decode($view['customers']);
$users = json_decode($view['users']);
$permissions = $view['permissions'];


// $image[0]['path'] = '';
// $image[0]['height'] = 1;
// $image[0]['width'] = 3;
// $image[0]['column'] = 1;
// $image[0]['row'] = 1;

// $customers[0]['height'] = 1;
// $customers[0]['width'] = 3;
// $customers[0]['column'] = 4;
// $customers[0]['row'] = 1;

// $users[0]['height'] = 1;
// $users[0]['width'] = 3;
// $users[0]['column'] = 7;
// $users[0]['row'] = 1;

// $products[0]['height'] = 3;
// $products[0]['width'] = 9;
// $products[0]['column'] = 1;
// $products[0]['row'] = 2;

// $container[0]['height'] = 1;
// $container[0]['width'] = 9;
// $container[0]['column'] = 1;
// $container[0]['row'] = 5;

// $last_sale[0]['height'] = 1;
// $last_sale[0]['width'] = 3;
// $last_sale[0]['column'] = 1;
// $last_sale[0]['row'] = 6;

// $order[0]['height'] = 1;
// $order[0]['width'] = 3;
// $order[0]['column'] = 7;
// $order[0]['row'] = 6;

// $tab[0]['height'] = 1;
// $tab[0]['width'] = 3;
// $tab[0]['column'] = 10;
// $tab[0]['row'] = 1;

$currency_billet[0] = 1000;
$currency_billet[1] = 500;
$currency_billet[2] = 200;
$currency_billet[3] = 100;
$currency_billet[4] = 50;



$element[0]['type'] = 'image';
$element[0]['path'] = 'mcdonalds.png';
$element[0]['height'] = 1;
$element[0]['width'] = 3;
$element[0]['column'] = 1;
$element[0]['row'] = 1;

$element[1]['type'] = 'customers';
$element[1]['height'] = 1;
$element[1]['width'] = 3;
$element[1]['column'] = 4;
$element[1]['row'] = 1;

$element[2]['type'] = 'users';
$element[2]['height'] = 1;
$element[2]['width'] = 3;
$element[2]['column'] = 7;
$element[2]['row'] = 1;

$element[3]['type'] = 'products';
$element[3]['height'] = 3;
$element[3]['width'] = 9;
$element[3]['column'] = 1;
$element[3]['row'] = 2;

$element[4]['type'] = 'container';
$element[4]['height'] = 1;
$element[4]['width'] = 9;
$element[4]['column'] = 1;
$element[4]['row'] = 5;

    $element[4]['button'][0]['label'] = 'Get parked order';
    $element[4]['button'][0]['height'] = 1;
    $element[4]['button'][0]['width'] = 1;
    $element[4]['button'][0]['column'] = 1;
    $element[4]['button'][0]['row'] = 1;
    $element[4]['button'][0]['class'] = 'button_get_parked_order';

    $element[4]['button'][1]['label'] = 'Extra functions';
    $element[4]['button'][1]['height'] = 1;
    $element[4]['button'][1]['width'] = 1;
    $element[4]['button'][1]['column'] = 2;
    $element[4]['button'][1]['row'] = 1;
    $element[4]['button'][1]['class'] = 'button_extra_functions';

    $element[4]['button'][2]['label'] = 'Discount';
    $element[4]['button'][2]['height'] = 1;
    $element[4]['button'][2]['width'] = 1;
    $element[4]['button'][2]['column'] = 3;
    $element[4]['button'][2]['row'] = 1;
    $element[4]['button'][2]['class'] = 'button_discount';

    $element[4]['button'][3]['label'] = 'Cancel order';
    $element[4]['button'][3]['height'] = 1;
    $element[4]['button'][3]['width'] = 1;
    $element[4]['button'][3]['column'] = 4;
    $element[4]['button'][3]['row'] = 1;
    $element[4]['button'][3]['class'] = 'button_cancel_order';

    $element[4]['button'][4]['label'] = 'Pay';
    $element[4]['button'][4]['height'] = 2;
    $element[4]['button'][4]['width'] = 1;
    $element[4]['button'][4]['column'] = 5;
    $element[4]['button'][4]['row'] = 1;
    $element[4]['button'][4]['class'] = 'button_pay';

    $element[4]['button'][5]['label'] = 'Retur';
    $element[4]['button'][5]['height'] = 1;
    $element[4]['button'][5]['width'] = 1;
    $element[4]['button'][5]['column'] = 6;
    $element[4]['button'][5]['row'] = 1;
    $element[4]['button'][5]['class'] = 'button_retur';

    $element[4]['button'][6]['label'] = 'Get new giftcard';
    $element[4]['button'][6]['height'] = 1;
    $element[4]['button'][6]['width'] = 1;
    $element[4]['button'][6]['column'] = 7;
    $element[4]['button'][6]['row'] = 1;
    $element[4]['button'][6]['class'] = 'button_get_new_giftcard';


    $element[4]['button'][7]['label'] = 'Line info';
    $element[4]['button'][7]['height'] = 1;
    $element[4]['button'][7]['width'] = 1;
    $element[4]['button'][7]['column'] = 1;
    $element[4]['button'][7]['row'] = 2;
    $element[4]['button'][7]['class'] = 'button_line_info';

    $element[4]['button'][8]['label'] = 'Delete line';
    $element[4]['button'][8]['height'] = 1;
    $element[4]['button'][8]['width'] = 1;
    $element[4]['button'][8]['column'] = 2;
    $element[4]['button'][8]['row'] = 2;
    $element[4]['button'][8]['class'] = 'button_delete_line';

    $element[4]['button'][9]['label'] = 'Order info';
    $element[4]['button'][9]['height'] = 1;
    $element[4]['button'][9]['width'] = 1;
    $element[4]['button'][9]['column'] = 3;
    $element[4]['button'][9]['row'] = 2;
    $element[4]['button'][9]['class'] = 'button_order_info';

    $element[4]['button'][10]['label'] = 'Open credit order';
    $element[4]['button'][10]['height'] = 1;
    $element[4]['button'][10]['width'] = 1;
    $element[4]['button'][10]['column'] = 4;
    $element[4]['button'][10]['row'] = 2;
    $element[4]['button'][10]['class'] = 'button_open_credit_order';

    $element[4]['button'][11]['label'] = 'Receipt';
    $element[4]['button'][11]['height'] = 1;
    $element[4]['button'][11]['width'] = 1;
    $element[4]['button'][11]['column'] = 6;
    $element[4]['button'][11]['row'] = 2;
    $element[4]['button'][11]['class'] = 'button_receipt';


$element[5]['type'] = 'last_sale';
$element[5]['height'] = 1;
$element[5]['width'] = 3;
$element[5]['column'] = 1;
$element[5]['row'] = 6;

$element[6]['type'] = 'order';
$element[6]['height'] = 1;
$element[6]['width'] = 3;
$element[6]['column'] = 7;
$element[6]['row'] = 6;

$element[7]['type'] = 'tab';
$element[7]['height'] = 1;
$element[7]['width'] = 3;
$element[7]['column'] = 10;
$element[7]['row'] = 1;

function get_payment_buttons($cb){
    $buttons = "";

    foreach($cb as $button){
        $buttons .="<input type='button' class='btn btn-store btn-primary' style='' value='".$button."' onclick='add_billet(".$button.")'>";
    }

    return $buttons;
}

function get_buttons($ce){
    $buttons = "";

    foreach($ce['button'] as $button){
        $buttons .="<input type='button' class='pos_element col-md-".$button['width']." ".$button['class']."' style='margin-top:".(($button['row'] -1) * 50)."px; margin-left:".(($button['column'] -1) * 8.33)."%; height:".($button['height'] * 50)."px;' value='".$button['label']."'>";
    }

    return $buttons;
}

function create_element($ce){
    $overflow = "";
    $ce=="products" ? "overflow-x:hidden;" : "";

    $el = "<div class='pos_element col-md-".$ce['width']."' style='margin-top:".(($ce['row'] -1) * 100)."px; margin-left:".(($ce['column'] -1) * 8.33)."%; height:".($ce['height'] * 100)."px; ".$overflow.";'>";
        switch($ce['type']){
            case "image": $el .= "<img style='max-height:".($ce['height'] * 100)."px;' src='".URL_BASE."../assets/images/".$ce['path']."'>"; break;
            case "products": $el .= "<input type='text' class='insert_barcode col-md-11'><input type='button' class='col-md-1 new_product' value='+'><table class='pos_table table-bordered' style='width:100%;'><thead><tr><td><input type='checkbox'></td><td></td><td>Product No</td><td>Description</td><td>Qty</td><td>Un Price</td><td>Discount</td><td>Price</td><td>Info</td></tr></thead><tbody><tr class='new_product'><td></td><td></td><td class='product_no'></td><td class='product_description'></td><td></td><td class='product_price'></td><td></td><td></td><td></td></tr></tbody></table>"; break;
            case "container": $el .= get_buttons($ce); break;
            case "order": $el .= "ORDER<br/><div style='width:100%; height:80%;'><div>Total: <span class='total_order'>0</span></div></div>"; break;
            case "customers": $el .= "<img src=''><div><input type='text' readonly placeholder='Customer' class='form-control display_customer_name'><br/><input type='button' class='btn btn-primary change_customer' value='Change'></div>"; break;
            case "users": $el .= "<img src=''><div><input type='text' readonly placeholder='user' class='form-control display_user_name'><br/><input type='button' class='btn btn-primary change_user' value='Change'></div>"; break;
            case "tab": $el .= "<ul class='nav nav-tabs'><li class='active'><a data-toggle='tab' href='#home'>Home</a></li><li><a data-toggle='tab' href='#menu1'>Menu 1</a></li><li><a data-toggle='tab' href='#menu2'>Menu 2</a></li></ul>"; break;
        }
    $el .= "</div>";

    return $el;
}

?>
<link href="<?= URL_BASE ?>../assets/css/select2.css" rel="stylesheet">
<style>
.nav-tabs a{
    color:#333 !important;
}
.billet_line{
    padding-top: 5px;
}
.pos_element{
    position: absolute;
}
.pos_table table, th, td {
    border: 1px solid gray;
}
.pos_table{
    margin-top:5px !important;
}

#chose_customer{
    display: none;
}
#pay_order{
    display: none;
}
.btn-store {
    margin: 5px;
    padding: 15px;
}
.others_btn {
    margin-top: 30px
}

.insert_barcode{
    height: 40px;
    margin-top:25px;
}

.edit_qty, .edit_price, .edit_discount, .edit_final_price, .edit_info{
    display: none;
}

.edit_product_line_attribute{
    width: 60px;
}
</style>

<input type="hidden" name="order" id="order" value="0">
<input type="hidden" name="user" id="user" value="0">
<input type="hidden" name="customer" id="customer" value="0">

<div id="product_list"></div>
<div style="margin-top:100px;">
    <?php if($element){ ?>
        <?php foreach($element as $e){ ?>
            <?php echo create_element($e) ?>
        <?php } ?>
    <?php } ?>
</div>

<div class="modal fade" id="include_new_product" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a new product</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Qtd:</label>
                        <input type="text" id="include_new_product_qty" value="1"> 
                        <input type="button" id="qty_plus" value="+"> 
                        <input type="button" id="qty_minus" value="-">

                        <table id="find_product">
                            <thead>
                            <tr>
                                <th style="width: 80px">ID</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="chose_customer" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Select customer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="find_customer" class="table table-bordered select2">
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Phone</td>
                                </tr>
                            </thead>
                            <?php if($customers){ ?>
                                <?php foreach($customers as $customer){ ?>
                                    <tr onclick="select_customer('<?= $customer->id ?>','<?= $customer->name ?>')"><td><?= $customer->name ?></td><td><?= $customer->phone ?></td></tr>
                                <?php } ?>
                            <?php } ?>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="chose_user" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Select User</h4>
            </div>
            <form id="form_color" name="form_color" class="form_color">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($users){ ?>
                                <?php foreach($users as $user){ ?>
                                    <button type="button" class="btn btn-primary" onclick="select_user('<?= $user->id ?>','<?= $user->name ?>')"><?= $user->name ?></button>
                                <?php } ?>
                            <?php } ?>  
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="chose_parked_order" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Parked Orders</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="find_parked_order" class="table table-bordered select2">
                            <thead>
                                <tr>
                                    <td>Order</td>
                                    <td>Customer</td>
                                    <td>Employee</td>
                                    <td>Time</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>   
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_discount" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Insert discount percentage</h4>
            </div>
            <form id="form_color" name="form_color" class="form_color">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!--  --> 
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="pay_order" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Payment</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4><b>Total:</b> <div id="pay_order_total_amount"></div></h4>
                        <h4><b>Received:</b> <div id="pay_order_received_amount"></div>
                        <h4><b>TotalReceived:</b> <div id="pay_order_received_total_amount">0</div>
                    </div>

                    <div class="col-md-6">
                        <div class="payment_box_right">
                            <p><b>Bills</b></p>
                            <?= get_payment_buttons($currency_billet) ?>
                        </div>

                        <div class="others_btn">
                            <p><b>Other</b></p>
                            <button id="button_pay_all" class="btn btn-store btn-primary" onclick=""></button>
                            <button class="btn btn-store btn-primary">Clear</button>
                            <button class="btn btn-store btn-primary">Giftcard</button>
                            <button class="btn btn-store btn-primary">Credit note</button>
                            <button class="btn btn-store btn-primary">Giftcard Central</button>
                        </div>

                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-store btn-primary">Ok and Print receipt</button>
                <button class="btn btn-store btn-primary">Approve without receipt</button>
                <button class="btn btn-store btn-primary" data-dismiss="modal">Cancel and close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?= URL_BASE ?>../assets/js/select2.js" type="text/javascript"></script>
<script>
    function select_user(user_id,user_name){
        $(".display_user_name").val(user_name);
        $("#user").val(user_id);
        $("#chose_user").modal('hide');

        var baseUrl = url_base+"terminal/terminal/setUser/"+order_id+"/"+user_id;

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (html) {
            $("#order").val(html);

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });
    }








    $("#chose_user").modal({show : true});

    var count_products = 0;
    var payment_billets = 0;
    var count_payment_billets = 0;
    var url_base = "<?php echo URL_BASE ?>";

    $('#find_product').DataTable( {
        "ajax": {"url": "<?php echo URL_BASE ?>Terminal/Terminal/getAllProducts/"},
        /*"dom": 'frtip',*/
        "columns": [
            { "data": "id" },
            { "data": "description" }
        ],
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [-1] }
        ],
        "ordering": false,
        "info":     false,
        "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
    });

    $(".button_get_parked_order").click(function(){
        $('#find_parked_order').DataTable( {
            "ajax": {"url": "<?php echo URL_BASE ?>Terminal/Terminal/getAllParkedOrders/"},
            /*"dom": 'frtip',*/
            "columns": [
                { "data": "order_identify" },
                { "data": "customer" },
                { "data": "employee" },
                { "data": "date_create" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "ordering": false,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        $("#chose_parked_order").modal({show:true});
    });

    $(document).on('click', '#find_parked_order tr', function(){
         conf = $(this).find(".order_id").val();
         if(conf){
            load_parked_order(conf);
         }
    });

    function load_parked_order(order_id){
        var baseUrl = url_base+"terminal/terminal/getOrder/"+order_id;

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (html) {
            var data = html;
            var elements = JSON.parse(data);

            clear_order();
            alert(elements[0][0]);
            $(".display_customer_name").val(elements[2]);
            $(".display_user_name").val(elements[3]);

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });

        $("#chose_parked_order").modal("hide");
    }

    function check(checkbox_id){
        $("#"+checkbox_id) //finalizar isso - teste
    }

    function delete_billet_line(bl,amount){
        $("#billet_line_"+bl).remove();
        payment_billets -= amount;
        $("#pay_order_received_total_amount").html(payment_billets);
    }

    function add_billet(billet){
        payment_billets += billet;
        $("#pay_order_received_total_amount").html(payment_billets);
        $("#pay_order_received_amount").append("<div class='billet_line' id='billet_line_"+count_payment_billets+"' >"+billet+" <input type='button' onclick='delete_billet_line(\""+count_payment_billets+"\",\""+billet+"\")' value='X'></div>");
        count_payment_billets++;
        conf_billets();
    }
    function conf_billets(){
        total_order = parseFloat($(".total_order").html());

        if(payment_billets>=total_order){ //transformar numa funcao - teste
            var dados = [];

            customer_id = $("#customer").val();
            user_id = $("#user").val();

            var products = "";


            $(".product").each(function(){
                products += $(this).val(); //id
                products += "|";
                products += $(this).attr("qty"); //qty
                products += "|";
                products += $(this).attr("price"); //value
                products += "|";
                products += $(this).attr("discount"); //discount
                products += "|";
                products += $(this).attr("info"); //extra_info

                products += "^^^";
            });
            var baseUrl = url_base+"terminal/terminal/payOrder/"+customer_id+"/"+user_id+"/"+total_order+"/"+products;

            $.ajax({

                url: baseUrl,
                method: "POST",
                data: dados,
                async: false,

            }).done(function (html) {
                alert("Success!");
                clear_order();
                $("#chose_user").modal({show : true});

            }).fail(function (jqXHR, textStatus) {

                alert("Request failed: " + textStatus);

            });
        }
    }

    $(".new_product").click(function(){
        $("#include_new_product").modal({show:true});
        $("#find_product").focus();
    });

    $(".change_customer").click(function(){
        $("#chose_customer").modal({show : true});
    });
    $(".change_user").click(function(){
        $("#chose_user").modal({show : true});
    });
   

    $(".close-dialog").click(function(){
        $(".modal-dialog").hide();
    });
    $('select').select2();

    $("#qty_plus").click(function(){
        qty = parseInt($("#include_new_product_qty").val());
        qty++;
        $("#include_new_product_qty").val(qty);
    });

    $("#qty_minus").click(function(){        
        qty = parseInt($("#include_new_product_qty").val());
        if(qty>1){
            qty--;
            $("#include_new_product_qty").val(qty);
        }
    });

    $(".button_cancel_order").click(function(){
        if(confirm('Would you like to remove all the products?')){
            $('.line_product').remove();
            $('.product').remove();

            calculate_order_total();
        }
    });

    $(".button_pay").click(function(){
        if($(".display_customer_name").val()!=""){
            $("#pay_order").modal({show:true});
        }
        else{
            alert("Please, select a customer before payment.");
        }
    });

    $(".button_delete_line").click(function(){
        qty_removed = 0;

        $('.check_product').each(function() {
            if($(this).prop('checked')==true){
                line_number = $(this).attr("which");
                $("#line_product_"+line_number).remove();
                $("#product_"+line_number).remove();

                qty_removed++;
            }
        });

        count = count_products - qty_removed;
        $('.line_product').each(function() {
            this_id = $(this).attr("id");
            $('#'+this_id+' count_product').html(count);
            count--;
        });

        calculate_order_total();
        
    });

    function clear_order(){
        $("#customer").val("");
        $("#user").val("");
        $(".display_customer_name").val("");
        $(".display_user_name").val("");
        $('.line_product').remove();
        $('.product').remove();
        calculate_order_total();
        $("#pay_order").modal('hide');
        $("#pay_order_received_amount").html("");
        $("#pay_order_received_total_amount").html("0");
            
        payment_billets = 0;
        count_payment_billets = 0;
    }

    function select_customer(customer_id,customer_name){
        $(".display_customer_name").val(customer_name);
        $("#customer").val(customer_id);
        $("#chose_customer").modal('hide');
    }
    

    function calculate_order_total(){
        order_total_price = 0;
        $(".label_final_price").each(function(){
            order_total_price += parseFloat($(this).html());
        });
        $(".total_order").html(order_total_price.toFixed(2));
        $("#pay_order_total_amount").html(order_total_price.toFixed(2));
        $("#button_pay_all").html(order_total_price.toFixed(2));
    }

    function open_edition_attribute(column,line_number){
        $(".label_product_line_attribute").show();
        $(".edit_product_line_attribute").hide();

        $("#label_"+column+"_"+line_number).hide();
        $("#edit_"+column+"_"+line_number).show();
        $("#edit_"+column+"_"+line_number).focus();
    }
    function save_edition_attribute(column,line_number){
        $("#label_"+column+"_"+line_number).show();
        $("#edit_"+column+"_"+line_number).hide();

        var value = $("#edit_"+column+"_"+line_number).val();
        $("#label_"+column+"_"+line_number).html(value);
        $("#product_"+line_number).attr(column,value);

        if(column=="qty"||column=="price"||column=="discount"){
            var qty_ = parseInt($("#label_qty_"+line_number).html());
            var price_ = parseFloat($("#label_price_"+line_number).html());
            var discount_ = $("#label_discount_"+line_number).html();

            if(discount_==""){
                discount_ = 0;
            }
            discount_ = parseFloat(discount_);

            var final_price_ = ((qty_ * price_)*((100 - discount_)/100)).toFixed(2);
            $("#label_final_price_"+line_number).html(final_price_);
            $("#edit_final_price_"+line_number).val(final_price_);

            calculate_order_total();
            if(discount_!=0){
                $("#line_product_"+line_number).css("background","#FFDDDD");
            }else{
                $("#line_product_"+line_number).css("background","#EDECEC");
            }
        }else if(column=="final_price"){
            var qty_ = parseInt($("#label_qty_"+line_number).html());
            var final_price_ = parseFloat($("#label_final_price_"+line_number).html());
            var discount_ = $("#label_discount_"+line_number).html();

            if(discount_==""){
                discount_ = 0;
            }
            discount_ = parseFloat(discount_);

            var price_ = (final_price_/(qty_ * ((100 - discount_)/100))).toFixed(2);

            if(((price_*qty_)*(100 - discount_)/100).toFixed(2)>final_price_.toFixed(2)){
                price_ -= 0.01;
            }
            final_price_ = ((qty_ * price_)*((100 - discount_)/100)).toFixed(2);

            $("#label_final_price_"+line_number).html(final_price_);
            $("#edit_final_price_"+line_number).val(final_price_);

            $("#label_price_"+line_number).html(price_);
            $("#edit_price_"+line_number).val(price_);
            $("#product_"+line_number).attr("price",price_);

            calculate_order_total();
        }
    }
    function verifyEnter(event,column,line_number){
        if (event.keyCode == 13) {
            save_edition_attribute(column,line_number);
        }
    }

    function generate_product_line_attribute(line_number,column,value,permission){
        phrase = "";

        if(permission){
            phrase += "<td onclick='open_edition_attribute(\""+column+"\","+line_number+")'><span class='label_"+column+" label_product_line_attribute' id='label_"+column+"_"+line_number+"' which='"+line_number+"'>"+value+"</span><input type='text' class='edit_"+column+" edit_product_line_attribute' id='edit_"+column+"_"+line_number+"' which='"+line_number+"' value='"+value+"' onfocusout='save_edition_attribute(\""+column+"\","+line_number+")' onkeypress='return verifyEnter(event,\""+column+"\","+line_number+")'></td>";
        }else{
            phrase += "<td>"+$("#include_new_product_"+column).val()+"</td>";
        }

        return phrase;
    }

    function insert_new_product(product_id){
        var dados = [];
        dados.push(element);

        var element = { name: "product_id", value: product_id };
        dados.push(element);

        var baseUrl = url_base+"terminal/terminal/getProduct/"+product_id;

        $.ajax({

            url: baseUrl,
            method: "POST",
            data: dados,
            async: false,

        }).done(function (html) {
            var data = html;
            var elements = JSON.parse(data);

            count_products++;

            if (data != "") {
                var phrase = "";
                phrase += "<tr id='line_product_"+count_products+"' class='line_product'><td onclick='check(\"check_product_"+count_products+"\")'><input type='checkbox' which='"+count_products+"' id='check_product_"+count_products+"' class='check_product'></td><td class='count_product'>"+count_products+"</td><td>"+elements[0]['product_number']+"</td><td>"+elements[0]['name']+"</td>";

                phrase += generate_product_line_attribute(count_products,"qty",$("#include_new_product_qty").val(),<?= $permissions['edit_qty'] ?>);

                phrase += generate_product_line_attribute(count_products,"price",elements[0]['price'],<?= $permissions['edit_price'] ?>);

                phrase += generate_product_line_attribute(count_products,"discount","",<?= $permissions['edit_discount'] ?>);

                phrase += generate_product_line_attribute(count_products,"final_price",(elements[0]['price'] * parseInt($("#include_new_product_qty").val())).toFixed(2),<?= $permissions['edit_final_price'] ?>);

                phrase += generate_product_line_attribute(count_products,"info","",<?= $permissions['edit_info'] ?>);

                phrase += "</tr>";

                $(".pos_table tbody").prepend(phrase);

                $("#product_list").append("<input type='hidden' name='products[]' id='product_"+count_products+"' class='product' qty='"+$("#include_new_product_qty").val()+"' price='"+elements[0]['price']+"' discount='0' final_price='"+(elements[0]['price'] * parseInt($("#include_new_product_qty").val())).toFixed(2)+"' info='' value='"+product_id+"'>");
            }

            calculate_order_total();

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });

        $("#include_new_product").modal('hide');
        $("#include_new_product_qty").val(1);
    }

</script>