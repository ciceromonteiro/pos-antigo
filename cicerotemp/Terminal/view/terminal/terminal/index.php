<?php





$products = json_decode($view['products']);
$customers = json_decode($view['customers']);
$users = json_decode($view['users']);
$permissions = $view['permissions'];
$pos_html = $view['pos_html'];


$receipt_title = $view['receipt_title'];
$receipt_text = $view['receipt_text'];
$receipt_two_lines = $view['receipt_two_lines'];//
$receipt_print_actual_prices = $view['receipt_print_actual_prices'];//
$receipt_show_shop_address = $view['receipt_show_shop_address'];//
$receipt_show_each_product_tax = $view['receipt_show_each_product_tax'];//
$receipt_show_detailed_description = $view['receipt_show_detailed_description'];//
$receipt_hide_comments = $view['receipt_hide_comments'];//
$receipt_show_article_number = $view['receipt_show_article_number'];//
$receipt_show_discount_rate = $view['receipt_show_discount_rate'];//
$receipt_show_taxes_only_after = $view['receipt_show_taxes_only_after'];//
$receipt_show_tax_tip = $view['receipt_show_tax_tip'];//

$do_not_allow_payment_by_credit_card = $view['do_not_allow_payment_by_credit_card'];

$currency_billet[0] = 1000;
$currency_billet[1] = 500;
$currency_billet[2] = 200;
$currency_billet[3] = 100;
$currency_billet[4] = 50;

$company2  = getEm()->getRepository('PosCompany')->findBy(array('idposCompany' => $_SESSION['poscompany']));
//$company2 = getEm()->getRepository('PosCompany')->findBy(array("active" => 1));

$company = $company2[0];

$company_image2 = getEm()->getRepository('CompanyImage')->findBy(array('companycompany' => $company),array("sequence"=>"asc"));
if(@$company_image2[0] != ""){
     $company_image = $company_image2[0]['image'];
} else {
  $company_image = "";  
}

$company_name = $company->getName();
$company_address = $company->getAddress();
//$company_phone = $company->getPhone();

function get_payment_buttons($cb){
    $buttons = "";

    foreach($cb as $button){
        $buttons .="<input type='button' class='btn btn-store btn-primary' style='' value='".$button."' onclick='add_billet(".$button.")'>";
    }

    return $buttons;
}

?>

<?php if($_SESSION['mode'] != "Fake"): ?>
    <button style="margin-top:100px;" onclick="thermalPrinter()">Test Thermal Printer</button>
<?php endif; ?>

<input type="hidden" name="order" id="order" value="0">
<input type="hidden" name="article" id="article" value="0">
<input type="hidden" name="empty_order" id="empty_order" value="1">
<input type="hidden" name="user" id="user" value="0">
<input type="hidden" name="customer" id="customer" value="0">

<div id="product_list"></div>
<div style="margin-top:68px;">

    <?php
        if($pos_html){
            // removed generated layout to improve css and features
            //echo $pos_html;
        }
    ?>
    <div class="dropHere row">

        <!-- Customer Widget -->
        <div id="customer-widget" style="width: 164px; height: 96px; position: relative; left: 137px; top: 22px;" class="customer-widget bordered-ui ui-draggable ui-draggable-handle">
            <input readonly="" placeholder="Customer" class="form-control display_customer_name" type="text"><br>
            <input class="btn btn-primary change_customer" value="Change" type="button">
        </div>

        <!-- Employee Widget -->
        <div id="employee-widget" style="width: 164px; height: 96px; position: relative; left: 305px; top: -74px;" class="customer-widget bordered-ui ui-draggable ui-draggable-handle">
            <input readonly="" placeholder="Employee" class="form-control display_user_name" type="text"><br>
            <input class="btn btn-primary change_user" value="Change" type="button">
        </div>

        <!-- Order Info -->
        <div id="order-info-widget" style="width: 164px; height: 96px; background-color: rgb(237, 236, 236); border-radius: 5px; position: relative; left: 679px; top: -169px;" class="order-info-widget bordered-ui ui-draggable ui-draggable-handle ui-resizable">
            <h4>Order</h4>
            <p style="padding-left:15px">Total: <span class="total_order">0.00</span></p>
            <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div>
        </div>

        <!-- Last Sale Widget -->
        <div id="last-sale-widget" style="width: 164px; height: 96px; background-color: rgb(237, 236, 236); border-radius: 5px; position: relative; left: 847px; top: -265px;" class="last-sale-widget bordered-ui ui-draggable ui-draggable-handle ui-resizable">
            <h4>Last sale</h4>
            <p style="padding-left:15px">Total: <span class="total_order">0.00</span></p>
            <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div>
        </div>

        <!-- Table Widget -->
        <div id="table-widget" class="table-widget bordered-ui ui-draggable ui-draggable-handle ui-resizable" style="background-color: rgb(237, 236, 236); width: 738px; height: 268px; position: relative; left: 6px; top: -242px;">
            <div style="width: 100%;">
                <div class="input-group">
                    <input class="form-control insert_barcode" placeholder="Search for..." type="text">
                    <span class="input-group-btn">
                        <button class="btn btn-default new_product" type="button">Add</button>
                    </span>
                </div>
            </div>
            <table id="color" class="table-bordered pos_table" style="margin: 0px;" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input name="all" onclick="markAll(" color ')'="" type="checkbox"></th>
                        <th style="width: 10px">ID</th>
                        <th>Product No</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Un Price</th>
                        <th>Discount</th>
                        <th>Price</th>
                        <th>Info</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="new_product">
                        <td></td>
                        <td></td>
                        <td class="product_no"></td>
                        <td class="product_description"></td>
                        <td></td>
                        <td class="product_price"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div>
        </div>

        <!-- Parked Order -->
        <div id="get_parked_order" class="button_get_parked_order ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 750px; top: -546px;">
            <p>Parked<br> order</p>
        </div>

        <!-- Pay -->
        <div id="pay" class="button_pay ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 134px; width: 60px; position: relative; left: 683px; top: -446px;">
            <p>Pay</p>
        </div>

        <!-- Extra functions -->
        <a data-toggle="tab" href="#extraFunctions" aria-expanded="true" style="color:black">
            <div id="extra_functions" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 747px; top: -547px;">
                <p>Extra<br> functions</p>
            </div>
        </a>

        <!-- Order info -->
        <div id="order_info" class="button_order_info ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 614px; top: -482px;">
            <p>Order<br> info</p>
        </div>

        <!-- Credit order -->
        <div id="open_credit_order" class="button_order ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 612px; top: -482px;">
            <p>Credit<br> order</p>
        </div>

        <!-- Line info -->
        <div id="line_info" class="button_line_info ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 609px; top: -482px;">
            <p>Line info</p>
        </div>

        <!-- Discount -->
        <div id="discount" class="button_discount ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 408px; top: -420px;">
            <p>Discount</p>
        </div>

        <!-- Delete line -->
        <div id="delete_line" class="button_delete_line ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 406px; top: -419px;">
            <p>Delete<br> line</p>
        </div>

        <!-- Cancel Order -->
        <div id="cancel_order" class="button_cancel_order ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 403px; top: -419px;">
            <p>Cancel<br> order</p>
        </div>

        <!-- Keyboard -->
        <div onclick="testCMD()" id="keyboard" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 332px; top: -547px;">
            <span class="lnr lnr-keyboard" style="font-size: 38px"></span>
        </div>

        <!-- Return -->
        <div id="return" class="button_return ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: -208px; top: -655px;">
            <p>Return</p>
        </div>

        <!-- Kvittering -->
        <div id="kvittering" class="button_kvittering ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: -209px; top: -655px;">
            <p>Kvittering</p>
        </div>

        <!-- Lag Nytt Gavekort -->
        <div id="lag_nytt_gavekort" class="button_lag_nytt_gavekort ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: -209px; top: -655px;">
            <p>Lag <br>Nytt <br> Gavekort</p>
        </div>

        <!-- Image -->
        <div id="image-widget" style="width: 150px; height: 129px; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAkFBMVEWwCAi+DAz/1gD/2gC8AAz/2AD/3ACtAAj/3wC6AA2wAAjFXgb/4QCqAAizCQm3AA3ckATKSQrSeAXdhAjUgAayGgjlpAT0vAT6ywK8PAfLZwbAGgzAUAf0wATXcgn5zgLEMwvaiAbtrQXstQPJQgvOVAq4LwfCKQvimgTmrgS6RAfOcQbTYwqxJgjllgfFWAcOz0ZxAAAH3klEQVR4nO2Za3eiOhSGKySBUBBrW6Raq3irrR39///ucMkNSOLGOuucdVbeLzNqSh73PfFh9N/Uw78NYJDjGibHNUyOa5gc1zA5rmFyXMPkuIbJcQ2T4xomxzVMjmuYHNcwOa5hclzD5LiGyXEN0y1cqBRwGWSdVoO5EB3tf/Y7Sq+uW+8v+wW9kWwgV7lblq88b7WcWm2B0Mc29zwv3+xuIxvGhUZHQnAlP84/zCajP8vYr9eROPu8BWwQF1pvQ+wx+f7ZZDL6jH2+zAuXT9d8/ksutPOIpyje6sFoJumrL0BuABvAhdZLwjbym33DTMdFz2H9KS7XNetXT4NdCedCqMHC4WaS5YSB9S1Bi5DRZMWm+QK+txgKBuei57jejzwjSrnpwl7w0z1pWPIdoujDq1+QzV/jQh+NU+KPtHq1mNUvsddxEfpc1SQ4r9OQ/jRfJnweGGJgLprXIGSWNvuvG2+RbXtDdGSG/Gl46bFeh8loiMXGYygXC2a82rHnp8+E2U8Fo3vm7GPKOD/zwZ4cP5SCLUW7xj1EZCBa56rD+LvLrnvpM8vOPRCspoJy0YxZRyYWZQYjZ7kh4u8dpRFTlpNLQIRRBgXlQp/MPdtUeZcZAkvWRROEHlGygZez+KrBxpIKyEU3zA4/yrPTY9c49LnB92cKPnpiGTqzc6lQQC4eXXi5VmPp0nDhFY8w1IRcmQsqApo1f+xdzGDjDhWQi7nCP7ZihNUwj0yaDdG0eY1zFX9Ep6ykbEwR1oOCcq1wqyjxDZkj/Zy9sWGc7ZrAg9MLtd2I6qhAXOijeTD2OzV0z8aLuOZFT2x/f9reP+XZcO4bTA8F46LcX61srEjYlOVvqg/SM8PE6zaXGDCWiw6UkQrChfYrXThXJCxN6zKKFmwZzrv4P4wfz9UHWKBgXM98924FoszB1YwhGnvfXWjH8lQpt4aoGuTHJUuzZXdSR08Ys4K1GKGMR1uvgKItewJGEFPBuNCah3Ov9ZYTLBuYwzUfcJTWLkTPKrItqgZw8fmzdFYvnVDGfBcW6Q/H3y56j+CfkU0EYYJxzfje/QZHC56Ry5S7kRx7y9CIceH8FQp2DYu3t3LDflUUFax0Hl/WrV6VUv6hNw/uxcXTrJf+tSHEwS1bsf/oBi1eUDxSJPfiEv7RnH3KUs4NIf/VNJuUx6i/vBcXr5ZePNVxbVon3SbSuovG4+CRL4uhAXbNXDsWsl6oG1P4lCwlJnsBVW4SvYtvBw2wK1xiY6w9NCNeAYRaJzJe1aMDL3QkAzryCle65WHfq/Y11xPucCnVnspdIvmYCOZIOxZa8MD2t9rP13kbDPs7Xa9JeAHGq2+YI69w7Xmi+f1yWYnPrIKrmVW7rSYpeODj0124pvx5fqGdgkUn4svKLqSZFYK5SJ/JXbiOnIvMtfai5zYXybThExxiAX4PLukm0psSmEE79jKYI+FceJWAAt9urk/MwzrUH7LELMq5XgxcoiHEB5DB7FyiqmKv3x2bFZ24N9TNZMnXASurnWsqwqLfXphwu1AYykCSiUA9giqrlYvKsM8MXKmncmHvjz56Et65y5n791wpbx/asx9bojqyjGr9NsGJc+H8ARL4Vq6RmOdC3TRRapy0Jgqcm7jERAGs+DYqOat2rwA41fghOapc/tbAFV14HGLv8ddccxE7RDPc12U9eFEnHeO4EH2Lr0imv+YSRbN37yw6oOwwtVkLw57Rm2jwBNSJrFxnERT5znCEDy4tLpMtoj8yhUCHNRvXSMR0a/pqteXoW83H2Bw7fAIrSyEkIW1YsjtWB30NVM2lFrD43cSVfMmYgMz4Nq5PEav+poHqTzBK4FTr3kxbJnIgAp09bOH1KVKtupvQX8FEB4ULe2auiagnMaSA2bhE1y6n1dRw21EGtAwwvDoYuQrJBencNi552PEnpqYWvW4VrtzQHksu0YjKYgLokBYu5XBoPr9HD1+Sy18auYK5fBjkrGbjEuXLIyfjo4JMNiJ/awzp4F0a39SsgFzKIZ+YQyJRub6MpSl4E1wYcklh4Ypmsj1eQFwkM+4TvUquHHC2NVKVo4KsAMSc2snEV7iMy6JAxJcla69xVbUqilaSy3xYSAqFyzIjJ0RyAQqY1lR1rYoOCpe5RCcvKpd5Q4XLM0eFmUuU9UDpfL45ItpcFnspfRRQWLWmargu8hIQm/cLTgqXsfyWXLlcB7ij0Jqq2VBOq9izcM2Vumo41dZc8oaFWJb1ubrDQnCCcT3+TS7a/yR4EZHqm4451bKLjBvfMronW1mlTdN2h4tqfxoJ5ARgu0YGc8n6a0tbyaUxVY/LckYO3odzmcuvkPlHpEBOcrZOG7z7MK7jIC6zguNgLtNtTpfr6xdYYK43DOOayKl8C7qiMChSA+IOXIXkmv2G6+EvcoF/7tNJGQstfS94C0PSKAxtXPIiw19WXEFyU/RH8oRs5YoOEynLABNMJVf182gyn0yDG8wG5SonPkXmZQpX/bPtZPo9P97gT/UABrwTtarD9V28nLIT6Cqsy6U0Wsv8cgvX6jX4nr7MindAA9dx4UZeeA+uU+xh/sDX6K2YXooX4zWLjWuL72qvkzw3rV6j4FR8n4qbysX7o5DxOgSu6CAf91i+Tt5O77d9WyXNfo/Vzlv2+g5PdXJycnJycnJycnJycnJycnJycvrf6x+h0J2r/WsjDQAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-size: contain; z-index: 2; left: 5px; top: -783px;" class="bordered-ui item-1 imgSize-1 ui-draggable ui-draggable-handle ui-resizable ui-resizable-autohide" data-itam-id="1"> <input id="inputFileToLoad" onchange="encodeImageFileAsURL();" style="display: none" type="file"> <div class="ui-resizable-handle ui-resizable-n" style="z-index: 90; display: none;"></div>
            <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90; display: block;"></div>
            <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90; display: block;"></div>
            <div class="ui-resizable-handle ui-resizable-w" style="z-index: 90; display: none;"></div>
            <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90; display: block;"></div>
            <div class="ui-resizable-handle ui-resizable-sw" style="z-index: 90; display: none;"></div>
            <div class="ui-resizable-handle ui-resizable-ne" style="z-index: 90; display: none;"></div>
            <div class="ui-resizable-handle ui-resizable-nw" style="z-index: 90; display: none;"></div>
            <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div>
        </div>



        <!-- Calc -->
        <div id="cac0-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 122px; position: relative; left: 752px; top: -314px;">
            <p>0</p>
        </div>

        <div id="cac1-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 626px; top: -378px;">
            <p>1</p>
        </div>

        <div id="cac2-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 625px; top: -379px;">
            <p>2</p>
        </div>

        <div id="cac3-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 623px; top: -378px;">
            <p>3</p>
        </div>

        <div id="cac4-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 421px; top: -441px;">
            <p>4</p>
        </div>

        <div id="cac5-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 419px; top: -441px;">
            <p>5</p>
        </div>

        <div id="cac6-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 417px; top: -441px;">
            <p>6</p>
        </div>

        <div id="cac7-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 215px; top: -504px;">
            <p>7</p>
        </div>

        <div id="cac8-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 213px; top: -504px;">
            <p>8</p>
        </div>

        <div id="cac9-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 211px; top: -505px;">
            <p>9</p>
        </div>

        <div id="cac10-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 76px; top: -567px;">
            <p>%</p>
        </div>

        <div id="cac11-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 74px; top: -567px;">
            <p>*</p>
        </div>

        <div id="cac12-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 72px; top: -567px;">
            <p>-</p>
        </div>

        <div id="cac13-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 3px; top: -504px;">
            <p>+</p>
        </div>

        <div id="cac14-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 877px; top: -437px;">
            <p>,</p>
        </div>

        <div id="cac15-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 183px; width: 65px; position: relative; left: 882px; top: -501px;">
            <p>Ok</p>
        </div>

        <div id="cac16-widget" class="ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle" style="height: 61px; width: 65px; position: relative; left: 615px; top: -688px;">
            <p>&lt;=</p>
        </div>

        <div id="tabs-widget" style="position: relative; left: 6px; top: -742px; width: 739px; height: 333px;" class="ui-draggable ui-draggable-handle ui-resizable">
            <ul class="nav nav-tabs">
                <li class="">
                    <a data-toggle="tab" href="#products" aria-expanded="false">Products</a>
                </li>
                <li id="tab-extraFunctions" class="active">
                    <a data-toggle="tab" href="#extraFunctions" aria-expanded="true">Extra Functions</a>
                </li>
            </ul>
            <div class="tab-content" data-tab-id="6">
                <div id="products" class="tab-pane">
                    <div style="height:150px;">
                    </div>
                </div>
                <div id="extraFunctions" class="tab-pane fade active in">
                    <div style="height:150px;">
                        <div class="btn btn-primary button_withdrawal" style="margin: 5px;">With-drawal / Deposit</div>
                        <div class="btn btn-primary" style="margin: 5px;">Open Cash Drawer</div>
                        <div class="btn btn-primary" style="margin: 5px;">Print Receipt Copy</div>
                        <div class="btn btn-primary" style="margin: 5px;">Cash Statistics</div>
                        <div class="btn btn-primary button_credit_last_sale" style="margin: 5px;">Credit Last Sale</div>
                        <div class="btn btn-primary button_print_temp_receipt" style="margin: 5px;">Print Temp. Receipt</div>
                        <div class="btn btn-primary" style="margin: 5px;">Settle Invoice By Cash</div>
                        <div class="btn btn-primary" style="margin: 5px;">Pre Payment</div>
                        <div class="btn btn-primary button_split_order" style="margin: 5px;">Split Order</div>
                        <div class="btn btn-primary" style="margin: 5px;">Check Giftcard Status</div>
                        <div class="btn btn-primary" style="margin: 5px;">Check Electr. Gift Card</div>
                        <div class="btn btn-primary button_customer_orders" style="margin: 5px;">Customer Orders</div>
                        <div class="btn btn-primary" style="margin: 5px;">Customer Statistics</div>
                        <div class="btn btn-primary" style="margin: 5px;">Customer Updating</div>
                        <div class="btn btn-primary" style="margin: 5px;">Article Statistics</div>
                        <div class="btn btn-primary" style="margin: 5px;">Article Updating</div>
                    </div>
                </div>
            </div>
            <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div>
            <div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="include_new_product" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add a new product</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Qtd:</label>
                        <input type="text" id="include_new_product_qty" value="1"> 
                        <input type="button" id="qty_plus" value="+"> 
                        <input type="button" id="qty_minus" value="-">

                        <table id="find_product">
                            <thead>
                            <tr>
                                <th style="width: 80px">ID</th>
                                <th>Description</th>
                                <th>Stock</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_lag_nytt_gavekort" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Lag Nytt Gavekort</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>Amount: <input type="text" id="lag_nytt_gavekort_amount"></div>
                        <div>Serial: <input type="text" id="lag_nytt_gavekort_serial"></div>
                        <div><input type="button" id="lag_nytt_gavekort_insert" value="Ok"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_print_receipt_copy" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Print Receipt Copy</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="button" id="choose_previous_receipt" value="Previous Receipt">
                        <input type="button" id="choose_previous_receipt_a4" value="Previous Receipt A4">
                        <input type="button" id="choose_another_receipt" value="Another Receipt">
                        <input type="button" id="choose_by_date" value="By Date">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_print_receipt_copy_another" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Print Receipt Copy</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_another_receipt" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Orders</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="choose_order_to_print_receipt">
                            <thead>
                            <tr>
                                <th>Merket</th>
                                <th>Identify</th>
                                <th>Date</th>
                                <th>Customer</th>
                                <th>Employee</th>
                                <th>Amount</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_choose_by_date" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Choose a date</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input class="date" id="date_chosen_to_print_receipt"><br/>
                        <input type="button" id="select_date_chosen_to_print_receipt" value="Ok">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_withdrawal_or_deposit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Withdraw / Deposit</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="button" id="choose_withdraw" value="Withdraw">
                        <input type="button" id="choose_deposit" value="Deposit">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_withdrawal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Withdraw</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>User: <span id="withdrawal_user"></span></div>
                        <div>Description: <input type="text" id="withdrawal_description"></div>
                        <div>Amount: <input type="text" id="withdrawal_amount"></div>
                        <div><input type="button" id="withdrawal_insert" value="Ok"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_deposit" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Deposit</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div>User: <span id="deposit_user"></span></div>
                        <div>Description: <input type="text" id="deposit_description"></div>
                        <div>Amount: <input type="text" id="deposit_amount"></div>
                        <div><input type="button" id="deposit_insert" value="Ok"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_customer_order_lines" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Order Lines</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="table_customer_orders_products">
                            <thead>
                            <tr>
                                <th style="width: 80px">ID</th>
                                <th>Product Number</th>
                                <th>Description</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Amount</th>
                                <th>Amount (inc. tax)</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_customer_orders" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Orders</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label>Customer:</label>
                        <input type="text" id="customer_orders_id" value="">
                        <input type="button" id="button_get_customer_orders" value="Get">
                        <input type="text" id="customer_orders_name" value="">

                        <table id="table_customer_orders">
                            <thead>
                            <tr>
                                <th style="width: 80px">ID</th>
                                <th>Date</th>
                                <th>Order N</th>
                                <th>User</th>
                                <th>Merket</th>
                                <th>Info</th>
                                <th>Lines</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="choose_product_variety" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Choose variety</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="product_varieties">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="choose_customer" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Select customer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="find_customer" class="table table-bordered select2">
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Phone</td>
                                </tr>
                            </thead>
                            <?php if($customers){ ?>
                                <?php foreach($customers as $customer){ ?>
                                    <tr onclick="select_customer('<?= $customer->id ?>','<?= $customer->name ?>')"><td><?= $customer->name ?></td><td><?= $customer->phone ?></td></tr>
                                <?php } ?>
                            <?php } ?>    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="choose_user" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Select User</h4>
            </div>
            <form id="form_color" name="form_color" class="form_color">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($users){ ?>
                                <?php foreach($users as $user){ ?>
                                    <button type="button" class="btn btn-primary" onclick="select_user('<?= $user->id ?>','<?= $user->name ?>')"><?= $user->name ?></button>
                                <?php } ?>
                            <?php } ?>  
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="choose_parked_order" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Parked Orders</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="find_parked_order" class="table table-bordered select2">
                            <thead>
                                <tr>
                                    <td>Order</td>
                                    <td>Customer</td>
                                    <td>Employee</td>
                                    <td>Time</td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>   
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_split_order" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Split Order</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <div>Existing Order</div>
                        <table class='split_current_order_table table-bordered' style='width:100%;'><thead></thead><tbody></tbody></table>
                    </div>
                    <div class="col-md-1">
                        <input type="hidden" id="split_state" value="0">
                        <div><input type="button" state="0" class="change_split_state" value="Whole Line"></div>
                        <div><input type="button" state="1" class="change_split_state" value="1 pc"></div>
                        <div><input type="button" state="2" class="change_split_state" value="1/2 pcs"></div>
                        <div><input type="button" state="3" class="change_split_state" value="1/3 pcs"></div>
                        <div><input type="button" state="4" class="change_split_state" value="1/4 pcs"></div>
                        <div><input type="button" state="5" class="change_split_state" value="1/5 pcs"></div>
                        <div><input type="button" state="6" class="change_split_state" value="1/6 pcs"></div>
                    </div>
                    <div class="col-md-5">
                        <div>New Order</div>
                        <input type="hidden" id="split_new_order" value="">
                        <table class='split_new_order_table table-bordered' style='width:100%;'><thead></thead><tbody></tbody></table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="button" id="split_back" value="Back">
                        <input type="button" id="split_park" value="Park new order">
                        <input type="button" id="split_pay" value="Pay new order ">
                    </div>
                </div>
            </div>

              
            <!-- <div class="col-md-6">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">New Order</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" id="discount_for_many" value="">
                            <input type="button" id="insert_discount_for_many" value="Ok">
                        </div>
                    </div>
                </div>
            </div>  -->
        </div>
    </div>
</div>

<div class="modal fade" id="modal_article_statistics" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Product Statistics</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" id="article_statistics_id" value="">
                        <input type="button" id="button_get_article_statistics" value="Get">
                        <input type="text" id="article_statistics_name" value="">

                        <table id="table_article_statistics">
                            <thead>
                            <tr>
                                <th style="width: 80px">ID</th>
                                <th>Date</th>
                                <th>Product N</th>
                                <th>Description</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Amount (inc tax)</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_customer_statistics" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Customer Statistics</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" id="customer_statistics_id" value="">
                        <input type="button" id="button_get_customer_statistics" value="Get">
                        <input type="text" id="customer_statistics_name" value="">

                        <table id="table_customer_statistics">
                            <thead>
                            <tr>
                                <th style="width: 80px">ID</th>
                                <th>Date</th>
                                <th>Product N</th>
                                <th>Description</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Amount (inc tax)</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_discount" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Insert discount percentage</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" id="discount_for_many" value="">
                        <input type="button" id="insert_discount_for_many" value="Ok">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_line_info" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Insert information</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" id="info_for_many" value="">
                        <input type="button" id="insert_info_for_many" value="Ok">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_order_info" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Insert information</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <textarea id="info_order"></textarea>
                        <input type="button" id="insert_info_order" value="Ok">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pay_order" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Payment</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4><b>Total:</b> <div id="pay_order_total_amount"></div></h4>
                        <h4><b>Received:</b> <div id="pay_order_received_amount"></div>
                        <h4><b>TotalReceived:</b> <div id="pay_order_received_total_amount">0</div>
                    </div>

                    <div class="col-md-6">
                        <div class="payment_box_right">
                            <p><b>Bills</b></p>
                            <?= get_payment_buttons($currency_billet) ?>
                        </div>

                        <div class="others_btn">
                            <p><b>Other</b></p>
                            <button id="button_pay_all" class="btn btn-store btn-primary" onclick=""></button>
                            <button id="button_clear" class="btn btn-store btn-primary">Clear</button>
                            <button class="btn btn-store btn-primary">Giftcard</button>
                            <button class="btn btn-store btn-primary">Credit note</button>
                            <? if(!$do_not_allow_payment_by_credit_card){ ?>
                                <button class="btn btn-store btn-primary">Credit card</button>
                            <? } ?>
                            <button class="btn btn-store btn-primary">Giftcard Central</button>
                        </div>

                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="button_approve_and_print" class="btn btn-store btn-primary">Ok and Print receipt</button>
                <button id="button_approve_and_no_print" class="btn btn-store btn-primary">Approve without receipt</button>
                <button id="button_disapprove" class="btn btn-store btn-primary" data-dismiss="modal">Cancel and close</button>
            </div>
        </div>
    </div>
</div>
<div id="print" style="display: none;"></div>
<script type="text/javascript">
// default php variables ported to javascript
var _permissions = <?=json_encode($permissions)?>;
var receipt_title = '<?= $receipt_title ?>';
var receipt_text = '<?= $receipt_text ?>';
var receipt_two_lines = '<?= $receipt_two_lines ?>';
var receipt_print_actual_prices = '<?= $receipt_print_actual_prices ?>';
var receipt_show_shop_address = '<?= $receipt_show_shop_address ?>';
var receipt_show_each_product_tax = '<?= $receipt_show_each_product_tax ?>';//
var receipt_show_detailed_description = '<?= $receipt_show_detailed_description ?>';//de onde vem essa descricao detalhada?
var receipt_hide_comments = '<?= $receipt_hide_comments ?>';
var receipt_show_article_number = '<?= $receipt_show_article_number ?>';
var receipt_show_discount_rate ='<?= $receipt_show_discount_rate ?>';
var receipt_show_taxes_only_after ='<?= $receipt_show_taxes_only_after ?>';//
var receipt_show_tax_tip = '<?= $receipt_show_tax_tip ?>';//tip?

var _company_name = '<?=$company_name?>';
var _company_image = '<?=$company_image?>';
console.log(_permissions);
</script>
<script src="<?= URL_BASE ?>../assets/js/select2.js" type="text/javascript"></script>
<script src="<?= URL_BASE ?>../assets/js/terminal/index.js" type="text/javascript"></script>
