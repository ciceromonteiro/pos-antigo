<?php

/**
 * @author Paulo Egito <pvegito@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Jan 10, 2016
 */

require_once '../init_autoload.php';


class TerminalController {

    public function __construct() {
        
    }

    public function getCustomerProducts($customer_id){
        try{
            $begin = getEm()->getConnection()->beginTransaction();

            $entityManager = getEm();
            $orders = $entityManager->getRepository('Order')->findBy(array("active"=>1,"personperson"=>$customer_id));

            $data = array ();
            if($orders){
                foreach ($orders as $value){
                    $order_lines = $entityManager->getRepository('OrderLine')->findBy(array("active"=>1,"orderorder"=>$value));

                    if($order_lines){
                        foreach($order_lines as $order_line){
                            $product = $entityManager->getRepository('Product')->findBy(array("active"=>1,"idproduct"=>$order_line->getProductproduct()->getIdproduct()));
                            $product = $product[0];

                            $description = $product->getName();
                            if($order_line->getSizecolorsizecolor()){
                                $color = $order_line->getSizecolorsizecolor()->getColor()->getName();
                                $size = $order_line->getSizecolorsizecolor()->getSize()->getName();
                                $description .= " | ".$color." ".$size;
                            }
                            $dat = array (
                                "id" => $order_line->getIdorderline(),
                                "date_create" => $value->getDateCreate(),
                                "product_number" => $product->getProductNumber(),
                                "description" => $description,
                                "qty" => $order_line->getQty(),
                                "price" => $order_line->getValue(),
                                "discount" => $order_line->getDiscount(),
                                "amount_inc_tax" => $order_line->getAmountIncTax()
                            );
                            array_push($data, $dat);
                        }
                    }
                }
            }
          
            $commit = getEm()->getConnection()->commit();
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $rollback = getEm()->getConnection()->rollback();
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }


        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getSoldProducts($product_id){
        try{
            $begin = getEm()->getConnection()->beginTransaction();

            $entityManager = getEm();

            $product = $entityManager->getRepository('Product')->findBy(array("active"=>1,"idproduct"=>$product_id));
            $order_lines = $entityManager->getRepository('OrderLine')->findBy(array("active"=>1,"productproduct"=>$product));

            $data = array();
            if($order_lines){
                foreach($order_lines as $order_line){
                    //$product = $product[0];
                    $variety = "";
                    if($order_line->getSizecolorsizecolor()){
                        $color = $order_line->getSizecolorsizecolor()->getColor()->getName();
                        $size = $order_line->getSizecolorsizecolor()->getSize()->getName();
                        $variety .= $color." ".$size;
                    }
                    $dat = array (
                        "id" => $order_line->getIdorderline(),
                        "date_create" => $order_line->getDateCreate()->format('d/m/Y'),
                        "product_number" => $product[0]->getProductNumber(),
                        "customer_name" => $order_line->getOrderorder()->getPersonperson()->getName(),
                        "variety" => $variety,
                        "qty" => $order_line->getQty(),
                        "price" => $order_line->getValue(),
                        "discount" => $order_line->getDiscount(),
                        "amount_inc_tax" => $order_line->getAmountIncTax()
                    );
                    array_push($data, $dat);
                }
            }
               
          
            $commit = getEm()->getConnection()->commit();
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $rollback = getEm()->getConnection()->rollback();
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }


        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllProducts($returnArray = false){
        try{
            $begin = getEm()->getConnection()->beginTransaction();

            $entityManager = getEm();
            $arrayFilter = array(
                "active" => 1
            );
            $products = $entityManager->getRepository('Product')->findBy($arrayFilter);

            $data = array ();

            foreach ($products as $value){
                $dat = array (
                    "id" => $value->getIdproduct(),
                    "description" => '<span onclick="insert_new_product('.$value->getIdproduct().')">'.$value->getName().'</span>',
                    "stock" => $value->getStockQty()
                );
                array_push($data, $dat);
            }
          
            $commit = getEm()->getConnection()->commit();
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $rollback = getEm()->getConnection()->rollback();
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }


        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        if ($returnArray == true) {
            return $products;
        }
        
        $json_data = json_encode($data);
        print $json_data;
    }
    public function getAllParkedOrders($current_order,$current_user){
        try{
            $begin = getEm()->getConnection()->beginTransaction();

            $entityManager = getEm();
            $arrayFilter = array(
                "active" => 1,
                "parked" => 1
            );
            $orders = $entityManager->getRepository('Order')->findBy($arrayFilter,array('idorder'=>'desc'));

            $data = array ();

            foreach ($orders as $value){
                if($value->getIdorder()!=$current_order){
                    if($value->getInUse()&&$value->getUsersusers()->getIdusers()!=$current_user){
                        //
                    }else{
                        $customer = 0;
                        $customer = $value->getPersonperson();
                        if($customer){
                            $customer = $customer->getName();
                        }else{
                            $customer = "No Customer Defined";
                        }

                        $dat = array (
                            "order_identify" => $value->getOrderIdentify()."<input type='hidden' class='order_id' value='".$value->getIdorder()."'>",
                            "customer" => $customer,
                            "employee" => $value->getUsersusers()->getPersonperson()->getName(),
                            "date_create" => $value->getDateCreate()
                        );
                        array_push($data, $dat);
                    }    
                }
            }

            $commit = getEm()->getConnection()->commit();
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $rollback = getEm()->getConnection()->rollback();
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getLastOrder($user_id = false,$do_echo = true) {
        $em = getEm();
        
        if($user_id){
            $user = getEm()->getRepository("Users")->findBy(array('active' => 1, 'idusers' => $user_id));
            if($user[0]){
                $orders = getEm()->getRepository("Order")->findBy(array('active' => 1, 'usersusers' => $user[0]),array('idorder'=>'desc'));
            }
        }
        else{
            $orders = getEm()->getRepository("Order")->findBy(array('active' => 1),array('idorder'=>'desc'));
        }

        $last_order = null;

        if($orders){
            foreach($orders as $order){
                if($order->getStatus()!=0){
                    $last_order = $order->getIdorder();
                    break;
                }
            }
        }
        
        if($do_echo){
            echo $last_order;
        }
        
        return $last_order;
    }

    public function getOrderLinesPrices($order_id,$customer_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();

            $orders = getEm()->getRepository("Order")->findBy(array('active' => 1, 'idorder' => $order_id));
            $customer = getEm()->getRepository("Person")->findBy(array('active' => 1, 'idperson' => $customer_id));

            $data = array ();

            foreach ($orders as $value){
                $order_lines = getEm()->getRepository("OrderLine")->findBy(array('active' => 1, 'orderorder' => $value));
                foreach ($order_lines as $value2){
                    $discount = $value2->getDiscount() ? $value2->getDiscount() : 0;
                    $size = $color = "";
                    if($value2->getSizecolorsizecolor()){
                        $size = $value2->getSizecolorsizecolor()->getSizesize()->getName();
                        $color = $value2->getSizecolorsizecolor()->getColorcolor()->getName();
                    }

                    $price = $value2->getProductproduct()->getCurrentPrice($customer[0]);
                    $price = floor(($price * 100) * (1 - ($discount/100)))/100;
                    
                    $dat = array (
                        "id" => $value2->getIdorderLine(),
                        "product_id" => $value2->getProductproduct()->getIdproduct(),
                        "discount" => $discount, 
                        "qty" => $value2->getQty(),
                        "price" => $price,
                        "amount" => $value2->getAmount(),
                        "amount_inc_tax" => $value2->getAmountIncTax(),
                    );
                    array_push($data, $dat);
                }
            }

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = '';
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );     

        $json_data = json_encode($data);
        print $json_data; 
    }

    public function getOrderLines($order_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();
            

            $orders = getEm()->getRepository("Order")->findBy(array('active' => 1, 'idorder' => $order_id));

            $data = array ();

            foreach ($orders as $value){
                $order_lines = getEm()->getRepository("OrderLine")->findBy(array('active' => 1, 'orderorder' => $value));
                foreach ($order_lines as $value2){
                    $discount = $value2->getDiscount() ? $value2->getDiscount() : 0;
                    $size = $color = "";
                    if($value2->getSizecolorsizecolor()){
                        $size = $value2->getSizecolorsizecolor()->getSizesize()->getName();
                        $color = $value2->getSizecolorsizecolor()->getColorcolor()->getName();
                    }
                    
                    $dat = array (
                        "id" => $value2->getIdorderLine(),
                        "description" => $value2->getProductproduct()->getName(),
                        "description_with_variety" => $value2->getProductproduct()->getName()." | ".$size." ".$color,
                        "product_number" => $value2->getProductproduct()->getProductnumber(),
                        "product_id" => $value2->getProductproduct()->getIdproduct(),
                        "variety" => $size." ".$color,
                        "discount" => $discount, 
                        "qty" => $value2->getQty(),
                        "price" => $value2->getValue(),
                        "info" => $value2->getExtraInfo(),
                        "amount" => $value2->getAmount(),
                        "amount_inc_tax" => $value2->getAmountIncTax(),
                    );
                    array_push($data, $dat);
                }
            }

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = '';
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data; 
    }

    public function getOrder($order_id = false,$do_echo = true,$customer_id = false,$bring_order_lines = true,$calling_type = false, $date_ = false) {
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();
            
            if($order_id){
                $orders = getEm()->getRepository("Order")->findBy(array('active' => 1, 'idorder' => $order_id));
            }
            else{
                if($customer_id){
                    $customer = getEm()->getRepository("Person")->findBy(array('active' => 1, 'idperson' => $customer_id));
                    $orders = getEm()->getRepository("Order")->findBy(array('active' => 1,'personperson' => $customer));
                }else{
                    $orders = getEm()->getRepository("Order")->findBy(array('active' => 1));
                }
            }

            $data = array ();
            $products = array ();

            if($date_){
                $date_ = explode(".", $date_);
                $date_ = implode("/", $date_);
            }

            foreach ($orders as $value){
                $ok=1;
                if($date_){
                    if($value->getDateCreate()!=$date_){
                        $ok=0;
                    }
                }

                if($ok){
                    if($bring_order_lines){
                        $order_lines = getEm()->getRepository("OrderLine")->findBy(array('active' => 1, 'orderorder' => $value));
                        foreach ($order_lines as $value2){
                            $discount = $value2->getDiscount() ? $value2->getDiscount() : 0;
                            $size = $color = "";
                            if($value2->getSizecolorsizecolor()){
                                $size = $value2->getSizecolorsizecolor()->getSizesize()->getName();
                                $color = $value2->getSizecolorsizecolor()->getColorcolor()->getName();
                            }
                            
                            $dat2 = array (
                                "id" => $value2->getIdorderLine(),
                                "description" => $value2->getProductproduct()->getName(),
                                "description_with_variety" => $value2->getProductproduct()->getName()." | ".$size." ".$color,
                                "product_number" => $value2->getProductproduct()->getProductnumber(),
                                "product_id" => $value2->getProductproduct()->getIdproduct(),
                                "variety" => $size." ".$color,
                                "discount" => $discount, 
                                "qty" => $value2->getQty(),
                                "price" => $value2->getValue(),
                                "actual_price" => $value2->getActualValue(),
                                "info" => $value2->getExtraInfo()
                            );
                            array_push($products, $dat2);
                        }
                    }

                    $customer = "";
                    if($value->getPersonperson()){
                        $customer = $value->getPersonperson()->getName();
                    }
                    $extra_text = "";
                    if($calling_type=="credit"){
                        $pos = $_SESSION['pos'];
                        $text_additional = getEm()->getRepository("PosSettingsCreditNotes")->findBy(array('active' => 1, 'dataType' => "text_additional", 'idpos' => $pos));
                        if($text_additional[0]){
                            $extra_text = $text_additional[0]->getDataValue();
                        }
                    }

                    $dat = array (
                        "id" => $value->getIdorder(),
                        "order_identify" => $value->getOrderIdentify(),
                        "date_create" => $value->getDateCreate(),
                        "customer_name" => $customer,
                        "user_name" => $value->getUsersusers()->getPersonperson()->getName(),
                        "total_order" => $value->getTotalAmount(),
                        "extra_info" => $value->getExtraInfo(),
                        "merket" => "",
                        "products" => $products,
                        "button_show_order_lines" => "<input type='button' onclick='getOrderLines(".$value->getIdorder().")' value='Order Lines'>",
                        "button_print" => "<input type='button' onclick='print_receipt(".$value->getIdorder().",\"standard\")' value='Print'>",
                        "extra_text" => $extra_text
                    ); //antes: "button_show_order_lines" => "<input type='button' onclick='getOrderLines(".$value->getIdorder().",".@$value->getPersonperson()->getIdperson().")' value='Order Lines'>",
                    array_push($data, $dat);
                }    
            }

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = '';
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;            

        // $data = json_encode($data);

        // if($do_echo){
        //     echo $data;
        // }
        
        // return $data;

    }
    public function getProduct($product_id = false,$do_echo = true) {
        $em = getEm();
        
        if($product_id){
            $products = getEm()->getRepository("Product")->findBy(array('active' => 1, 'idproduct' => $product_id));
        }
        else{
            $products = getEm()->getRepository("Product")->findBy(array('active' => 1));
        }

        $data = array ();

        foreach ($products as $value){
            $dat = array (
                "id" => $value->getIdproduct(),
                "name" => $value->getName(),
                "product_number" => $value->getProductNumber(),
                "price" => $value->getPrice()
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        
        return $data;

    }
    public function getCustomer($person_id = false,$do_echo = true) {
        $em = getEm();
        
        if($person_id){
            $customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'idperson' => $person_id));
        }
        else{
            $customers = getEm()->getRepository("Person")->findBy(array('active' => 1));
            //$customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'customer_gp_idcustomer_gp' => 'IS NOT NULL'));

            // $qb = $this->createQueryBuilder('SELECT * FROM Person');
            // $qb->where('active = 1');
            // $qb->where('customer_gp_idcustomer_gp IS NOT NULL');

            // $customers = $qb->getQuery();

            // $rsm = "";
            // $query = $em->createNativeQuery('SELECT * FROM Person WHERE active = 1, customer_gp_idcustomer_gp IS NOT NULL');
            // $query->setParameter(1, 'romanb');

            // $customers = $query->getResult();

        }

        $data = array ();

        foreach ($customers as $value){
            if($phone = getEm()->getRepository("PersonData")->findBy(array('active' => 1, 'personperson' => $value, 'dataType' => 'Phone'))){
                $phone = $phone[0]->getDataValue();
            }else{
                $phone = "";
            }

            $dat = array (
                "id" => $value->getIdperson(),
                "name" => $value->getName(),
                "phone" => $phone
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        return $data;

    }
    public function getUser($user_id = false,$do_echo = true) {
        $em = getEm();
        
        if($user_id){
            $users = getEm()->getRepository("Users")->findBy(array('active' => 1, 'iduser' => $user_id));
        }
        else{
            $users = getEm()->getRepository("Users")->findBy(array('active' => 1));
            //$customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'customer_gp_idcustomer_gp' => 'IS NOT NULL'));

            // $qb = $this->createQueryBuilder('SELECT * FROM Person');
            // $qb->where('active = 1');
            // $qb->where('customer_gp_idcustomer_gp IS NOT NULL');

            // $customers = $qb->getQuery();

            // $rsm = "";
            // $query = $em->createNativeQuery('SELECT * FROM Person WHERE active = 1, customer_gp_idcustomer_gp IS NOT NULL');
            // $query->setParameter(1, 'romanb');

            // $customers = $query->getResult();

        }

        $data = array ();

        foreach ($users as $value){
            //$phone = getEm()->getRepository("PersonData")->findBy(array('active' => 1, 'personperson' => $value));

            $dat = array (
                "id" => $value->getIdusers(),
                "name" => $value->getLogin()
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        return $data;

    }

    // public function addOrderLine($order,$product,$value,$discount,$info){
    //     $order_line = new OrderLine();

    //     $order_line->setOrder($order);

    //     $order_line->setProduct($product);

    //     $order_line->setValue($value);

    //     $order_line->setDiscount($discount);

    //     $order_line->setExtraInfo($info);

    //     $order_line->setActive(1);

    //     getEm()->persist($order_line);

    //     getEm()->flush();
    // }

    public function payOrder($order_id,$amount_paid,$status){ 
        try {
            $begin = getEm()->getConnection()->beginTransaction();

            $em = getEm();

            $order = $em->getRepository("Order")->findBy(array('idorder' => $order_id));
            $order[0]->setStatus($status);
            if($status>0){
                $order[0]->setInUse(0);
                $order[0]->setParked(0);
            }
            if($amount_paid=="all"){
                $amount_paid = $order[0]->getTotalAmount();
            }
            getEm()->persist($order[0]);
            getEm()->flush();

            $payment_mtd = $em->getRepository("PaymentMtd")->findBy(array('active' => 1, 'idpaymentMtd' => 1)); //test por enquanto
            $date = new datetime();

            $payment = new OrderPayment();
            $payment->setOrderorder($order[0]);
            $payment->setPaymentMtdpaymentMtd($payment_mtd[0]);
            $payment->setValue($amount_paid);
            $payment->setDateCreate($date);
            $payment->setActive(1);
            getEm()->persist($payment);
            getEm()->flush();



            // $customer = $em->getRepository("Person")->findBy(array('active' => 1, 'idperson' => $customer_id));
            // $user = $em->getRepository("Users")->findBy(array('active' => 1, 'idusers' =>$user_id));
            $pos_session = $em->getRepository("PosSession")->findBy(array('idposSession' => 1));//este parametro esta assim por motivos de teste

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = '';
            $data = '';
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function index(){
        $products = $this->getProduct(false,false);
        $customers = $this->getCustomer(false,false);
        $users = $this->getUser(false,false);

        $em = getEm();
        $pos = $em->getRepository("Pos")->findBy(array('active' => 1, 'idpos' => $_SESSION['pos']));
        $pos_tmpt_in_pos = $em->getRepository("PosTmptInPos")->findBy(array('active' => 1, 'pospos' => $_SESSION['pos']));
        if($pos_tmpt_in_pos){
            $pos_tmpt = $em->getRepository("PosTmpt")->findBy(array('active' => 1, 'idposTmpt' =>$pos_tmpt_in_pos[0]->getPosTmptposTmpt()));
           // var_dump($pos_tmpt[0]->getName());
          //  die();
            $pos_html = $pos_tmpt[0]->getStructureHtml();
        } else {
            $pos_html = '<div class="col-md-12 text-center">You need to define the template for your terminal <a href="'.URL_BASE.'Pos/layout/index">Click here</a></div>';
        }

        $permissions = array('edit_price'=>1, 'edit_qty'=>1, 'edit_discount'=>1, 'edit_final_price'=>1, 'edit_info'=>1); //por enquanto esta tudo 1 - teste

        $receipt_byttelapp = '';
        $receipt_title = '';
        $receipt_text = '';
        $receipt_two_lines = '';
        $receipt_print_actual_prices = '';
        $receipt_show_shop_address = '';
        $receipt_show_each_product_tax = '';
        $receipt_show_detailed_description = '';
        $receipt_hide_comments = '';
        $receipt_show_article_number = '';
        $receipt_show_discount_rate = '';
        $receipt_show_taxes_only_after = '';
        $receipt_show_tax_tip = '';

        if($pos_settings_receipts = $em->getRepository("PosSettingsReceipts")->findBy(array('active' => 1, 'idpos' => $pos))){
            foreach($pos_settings_receipts as $psr){
                switch($psr->getDataType()){
                    case "title_coupon": $receipt_title = $psr->getDataValue(); break;
                    case "message_cupom": $receipt_text = $psr->getDataValue(); break;
                    case "message_byttelapp": $receipt_byttelapp = $psr->getDataValue(); break;
                    case "print_two_lines": $receipt_two_lines = $psr->getDataValue(); break;
                    case "when_printing_the_tax_coupon": $receipt_print_actual_prices = $psr->getDataValue(); break;
                    case "print_shop_address_on_tax_coupon": $receipt_show_shop_address = $psr->getDataValue(); break;
                    case "print_the_tax_values_on_each_product": $receipt_show_each_product_tax = $psr->getDataValue(); break;
                    case "print_detailed_product_description": $receipt_show_detailed_description = $psr->getDataValue(); break;
                    case "hide_comments_entered_in_the_purchase": $receipt_hide_comments = $psr->getDataValue(); break;
                    case "always_the_article_number_is_printed": $receipt_show_article_number = $psr->getDataValue(); break;
                    case "print_discount_rate_given_on_the_day": $receipt_show_discount_rate = $psr->getDataValue(); break;
                    case "prints_the_values_without_taxes_and_after": $receipt_show_taxes_only_after = $psr->getDataValue(); break;
                    case "includes_tax_tip_amounts_paid": $receipt_show_tax_tip = $psr->getDataValue(); break;
                }
            }
        }

        $do_not_allow_payment_by_credit_card = '';

        if($pos_settings_features = $em->getRepository("PosSettingsFeatures")->findBy(array('active' => 1, 'idpos' => $pos))){
            foreach($pos_settings_features as $psf){
                switch($psf->getDataType()){
                    case "do_not_allow_payment": $do_not_allow_payment_by_credit_card = $psf->getDataValue(); break;
                }
            }
        }

        $array_answer = array (
            '0'=>'',
            'products'=>$products,
            'customers'=>$customers,
            'users'=>$users,
            'permissions'=>$permissions,
            'pos_html'=>$pos_html,
            'receipt_title' => $receipt_title,
            'receipt_text' => $receipt_text,
            'receipt_two_lines' => $receipt_two_lines,
            'receipt_print_actual_prices' => $receipt_print_actual_prices,
            'receipt_show_shop_address' => $receipt_show_shop_address,
            'receipt_show_each_product_tax' => $receipt_show_each_product_tax,
            'receipt_show_detailed_description' => $receipt_show_detailed_description,
            'receipt_hide_comments' => $receipt_hide_comments,
            'receipt_show_article_number' => $receipt_show_article_number,
            'receipt_show_discount_rate' => $receipt_show_discount_rate,
            'receipt_show_taxes_only_after' => $receipt_show_taxes_only_after,
            'receipt_show_tax_tip' => $receipt_show_tax_tip,
            'do_not_allow_payment_by_credit_card' => $do_not_allow_payment_by_credit_card,
        );

        $navbar = 'navbar-top';
    
        GenericController::template("Terminal", "terminal","index", $navbar, $array_answer, 350);
    }


    public function startOrder($user_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();

            $em = getEm();
            $user = $em->getRepository("Users")->findBy(array('active' => 1, 'idusers' =>$user_id));
            $pos_session = $em->getRepository("PosSession")->findBy(array('idposSession' => 1));//este parametro esta assim por motivos de teste

            $order = new Order();

            $order->setUsersusers($user[0]);
            $order->setActive(1);
            $order->setPosSessionposSession($pos_session[0]);
            $order->setTotalAmount(0);

            getEm()->persist($order);

            getEm()->flush();

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = $order->getIdorder();

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );


        $json_data = json_encode($data);
        print $json_data;
        
    }

    public function parkOrder($order_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();

            $em = getEm();
            $order = $em->getRepository("Order")->findBy(array('active' => 1, 'idorder' =>$order_id));

            $order[0]->setParked(1);
            $order[0]->setInUse(0);

            getEm()->persist($order[0]);
            getEm()->flush();

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = $order[0]->getIdorder();

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function exchangeOrderLine($from_line,$target_order,$qty){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();

            $target_order = $em->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$target_order));
            $current_order_line = $em->getRepository('OrderLine')->findBy(array('active' => 1, 'idorderLine' =>$from_line));
            $current_qty = $current_order_line[0]->getQty();
            if($current_qty>=$qty){
                $current_qty -= $qty;

                $product = $current_order_line[0]->getProductproduct();
                $size_and_color = $current_order_line[0]->getSizecolorsizecolor();
                $warranty = $current_order_line[0]->getWarrantywarranty();
                $value_warranty = $current_order_line[0]->getValueWarranty();
                $date_warranty = $current_order_line[0]->getDateWarranty();
                $info = $current_order_line[0]->getExtraInfo();
                $value = $current_order_line[0]->getValue();
                $actualValue = $current_order_line[0]->getActualValue();
                $tax = $current_order_line[0]->getTax();
                // if($current_qty>0){
                //     $total_qty = $current_qty + $qty;
                //     $percentage_taken = $qty/$total_qty;

                //     $value = $value * $percentage_taken;
                // }
                $discount = $current_order_line[0]->getDiscount();
                $discount_reason = $current_order_line[0]->getDiscountReason();

                $new_order_line = new OrderLine();
                

                $date = new datetime();
                $new_order_line->setDateCreate($date);
                $new_order_line->setOrderorder($target_order[0]);
                $new_order_line->setProductproduct($product);
                $new_order_line->setWarrantywarranty($warranty);
                $new_order_line->setValueWarranty($value_warranty);
                $new_order_line->setDateWarranty($date_warranty);
                $new_order_line->setExtraInfo($info);
                $new_order_line->setValue($value);
                $new_order_line->setActualValue($actualValue);
                $new_order_line->setTax($tax);
                $new_order_line->setQty($qty);
                $new_order_line->setDiscount($discount);
                $new_order_line->setDiscountReason($discount_reason);
                $new_order_line->setSizecolorsizecolor($size_and_color);

                $new_order_line->setActive(1);

                getEm()->persist($new_order_line);
                getEm()->flush();

                // $value = $current_order_line[0]->getValue() - $value;
                $current_order_line[0]->setDateUpdate($date);
                if($current_qty > 0){
                    // $current_order_line[0]->setValue($value);
                    $current_order_line[0]->setQty($current_qty);
                }else{
                    // $current_order_line[0]->setValue(0);
                    $current_order_line[0]->setQty(0);
                    $current_order_line[0]->setActive(3);
                    $current_order_line[0]->setDateDelete($date);
                }

                getEm()->persist($current_order_line[0]);
                getEm()->flush();

                $current_order = $current_order_line[0]->getOrderorder();
                $current_order->recalculate_total_amount();
                $target_order[0]->recalculate_total_amount();
            }

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = "";

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateOrder($update_type,$update_value,$order_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();

            switch($update_type){
                case "order_info":
                $order = $em->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$order_id));
                $date = new datetime();
                $order[0]->setExtraInfo($update_value);
                $order[0]->setDateUpdate($date);
                $em->persist($order[0]);
                $em->flush();
                break;
                case "customer":
                $customer = $em->getRepository("Person")->findBy(array('active' => 1, 'idperson' =>$update_value));

                $order = $em->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$order_id));
                $date = new datetime();
                $order[0]->setPersonperson($customer[0]);
                $order[0]->setDateUpdate($date);

                $em->persist($order[0]);
                $em->flush();

                break;
                case "total_amount":
                $order = $em->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$order_id));
                $date = new datetime();
                $order[0]->setTotalAmount($update_value);
                $order[0]->setDateUpdate($date);

                $em->persist($order[0]);
                $em->flush();

                break;
                case "cancel_order":
                $order = $em->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$order_id));
                $date = new datetime();
                $order[0]->setDateDelete($date);
                $order[0]->setActive(3);

                $em->persist($order[0]);
                $em->flush();

                $order_lines = $em->getRepository('OrderLine')->findBy(array('active' => 1, 'orderorder' =>$order));
                if($order_lines){
                    foreach($order_lines as $order_line){
                        $order_line->setDateDelete($date);
                        $order_line->setActive(3);

                        $em->persist($order_line);
                        $em->flush();

                        $order_line_sections = $em->getRepository('OrderLineSection')->findBy(array('active' => 1, 'orderLineorderLine' =>$order_line));
                        if($order_line_sections){
                            foreach($order_line_sections as $order_line_section){
                                $order_line_section->setDateDelete($date);
                                $order_line_section->setActive(3);

                                $em->persist($order_line_section);
                                $em->flush();

                                $product_in_section = new ProductInSection();

                                $product_in_section->setDate($order_line_section->getDateStock());
                                $product_in_section->setExpirationdate($order_line_section->getExpirationdate());
                                $product_in_section->setQty($order_line_section->getQty());
                                $product_in_section->setProductproduct($order_line->getProductproduct());
                                $product_in_section->setSectionsection($order_line_section->getSectionsection());
                                $product_in_section->setActive(1);

                                getEm()->persist($product_in_section);
                                getEm()->flush();
                            }
                        }
                    }
                }


                break;
            }

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = "";

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
        
    }

    public function insertProduct($product_id,$qty,$order_id,$person_id) {
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();
            $message = "";
            $ok = 1;

            if($product = $em->getRepository("Product")->findBy(array('active' => 1, 'idproduct' => $product_id))){
                 //
            }else{
                $message = "Product not found";
                $ok = 0;
            }
            if($order = $em->getRepository("Order")->findBy(array('active' => 1, 'idorder' => $order_id))){
                 //
            }else{                 
                $message = "Order not found";
                $ok = 0;
            }
            if($person = $em->getRepository("Person")->findBy(array('active' => 1, 'idperson' => $person_id))){
                //
            }else{
                $message = "Customer not found";
                $ok = 0;
            }

            if($ok){                
                if($product[0]->getOutputControl()==1){ //fifo //teste - nao esta funcionando essa ordenacao, aparentemente o doctrine nao esta respondendo como deveria
                    $products_in_sections = $em->getRepository("ProductInSection")->findBy(array('productproduct' => $product[0]),array("idproductInSection"=>"asc"));
                }else if($product[0]->getOutputControl()==2){ //lifo
                    $products_in_sections = $em->getRepository("ProductInSection")->findBy(array('productproduct' => $product[0]),array("date"=>"desc"));
                }else if($product[0]->getOutputControl()==3){ //fefo
                    $products_in_sections = $em->getRepository("ProductInSection")->findBy(array('productproduct' => $product[0]),array("expirationdate"=>"asc"));
                }else { //no control
                    //
                }

                $conf_qty_in_sections = 0;
                if($product[0]->getOutputControl()!=0){
                    if($products_in_sections){
                        $i = 0;
                        while($i<count($products_in_sections)&&$conf_qty_in_sections<$qty){ //verifies whether the qty in stock is enough
                            $conf_qty_in_sections += $products_in_sections[$i]->getQty();
                            $i++;
                        }
                    }

                    if($conf_qty_in_sections<$qty){ //not enough quantity in stock
                        $message = "Not enough quantity in stock";
                        $ok = 0; 
                    }
                }
            }
// $prod = $products_in_sections[0]->getProductproduct();
// var_dump($prod->getIdproduct());
//  die();           

            if($ok){
                $tax = 0;
                $customer_gp = $state = $country = false;
                if($person[0]->getCustomerGpcustomerGp()){
                    $customer_gp = $person[0]->getCustomerGpcustomerGp();
                }
                
                $tribute = $product[0]->getAppliedTax($customer_gp,$state,$country);
                $tax = $tribute->getTax()->getPercent();

                $order_line = new OrderLine();

                $order_line->setOrder($order[0]);
                $order_line->setProduct($product[0]);
                $order_line->setQty($qty);
                $order_line->setValue($product[0]->getCurrentPrice($person));
                $order_line->setActualValue($product[0]->getPrice());
                $order_line->setTax($tax);
                $order_line->setActive(1);
                $date = new datetime();
                $order_line->setDateCreate($date);

                getEm()->persist($order_line);

                getEm()->flush();            
//                var_dump($products_in_sections);
//                die();

                if($product[0]->getOutputControl()!=0){
                    if($products_in_sections){
                        $i = 0;
                        $qty_left_to_insert = $qty;
                        while($i<count($products_in_sections)&&$qty_left_to_insert>0){                    
                            if($products_in_sections[$i]->getQty()>0){
                                $order_line_section = new OrderLineSection();
// echo $products_in_sections[$i]->getIdproductInSection()."<br/>";
                                if($qty_left_to_insert<=$products_in_sections[$i]->getQty()){
                                    $update_section_qty = $products_in_sections[$i]->getQty() - $qty_left_to_insert;
                                    $order_line_section->setQty($qty_left_to_insert);
                                    $products_in_sections[$i]->setQty($update_section_qty);
                                    $qty_left_to_insert = 0;
                                }else{
                                    $qty_left_to_insert -= $products_in_sections[$i]->getQty();
                                    $order_line_section->setQty($products_in_sections[$i]->getQty());
                                    $products_in_sections[$i]->setQty(0);
                                }

                                $order_line_section->setDateStock($products_in_sections[$i]->getDate()); 
                                $order_line_section->setExpirationDate($products_in_sections[$i]->getExpirationdate()); 
                                $order_line_section->setOrderLineorderLine($order_line);                            
                                $order_line_section->setSectionsection($products_in_sections[$i]->getSectionsection());
                                $order_line_section->setActive(1);
                                $date = new datetime();
                                $order_line_section->setDateCreate($date);
    //var_dump($order_line_section);
    // var_dump($order_line_section);

                                getEm()->persist($order_line_section);
                                getEm()->flush();
                                
                            }
                            $i++;
                        }
                    }
                }

                $data = array ();

                // $data = "";

                foreach ($product as $value){
                    $varieties = "";
                    if($value->getHasSizeAndColor()){
                        $sizecolors = $em->getRepository("SizeColor")->findBy(array('active' => 1, 'productproduct' => $value));
                        if($sizecolors){
                            foreach($sizecolors as $sc){
                                $size = $sc->getSizesize();
                                $color = $sc->getColorcolor();
                                $varieties .= "<input type='button' onclick='select_variety(\"".$order_line->getIdorderLine()."\",\"".$sc->getIdsizeColor()."\")' value='".$color->getName()." . ".$size->getName()."'>";
                            }
                        }
                    }

                    $dat = array (
                        "id" => $value->getIdproduct(),
                        "name" => $value->getName(),
                        "product_number" => $value->getProductNumber(),
                        "price" => $value->getCurrentPrice($person),
                        "order_line_id" => $order_line->getIdorderLine(),
                        "has_size_and_color" => $value->getHasSizeAndColor(),
                        "varieties" => $varieties
                    );
                    array_push($data, $dat);
                }

                $result  = 'success';
                $message = 'query success';
            } else {
                $result  = 'error';
                $data = "";
            }
            $commit = getEm()->getConnection()->commit();

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;

    }

    public function selectVariety($order_line_id,$size_and_color_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();

            $size_and_color = $em->getRepository('SizeColor')->findBy(array('active' => 1, 'idsizeColor' =>$size_and_color_id));

            $order_line = $em->getRepository('OrderLine')->findBy(array('active' => 1, 'idorderLine' =>$order_line_id));
            $order_line[0]->setSizecolorsizecolor($size_and_color[0]);

            $em->persist($order_line[0]);
            $em->flush();

            $size = $size_and_color[0]->getSizesize();
            $color = $size_and_color[0]->getColorcolor();
            $complement_name = $color->getName()." ".$size->getName();
            
            $result  = 'success';
            $message = 'query success';
            $data = array ();
            $dat = array (
                "complement_name" => $complement_name
            );
            array_push($data, $dat);
            $commit = getEm()->getConnection()->commit();

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }  

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateProduct($order_line_id,$qty,$discount,$value,$info){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();


            $order_line = $em->getRepository('OrderLine')->findBy(array('active' => 1, 'idorderLine' =>$order_line_id));
            $date = new datetime();
            $order_line[0]->setQty($qty);
            $order_line[0]->setDiscount($discount);
            $order_line[0]->setValue($value);
            $order_line[0]->setExtraInfo($info);
            $order_line[0]->setDateUpdate($date);

            $em->persist($order_line[0]);
            $em->flush();

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = "";

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
        
    }

    public function deleteProduct($order_line_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();

            $order_line = $em->getRepository('OrderLine')->findBy(array('active' => 1, 'idorderLine' =>$order_line_id));
            $date = new datetime();
            $order_line[0]->setDateDelete($date);
            $order_line[0]->setActive(3);

            $em->persist($order_line[0]);
            $em->flush();

            $order_line_sections = $em->getRepository('OrderLineSection')->findBy(array('active' => 1, 'orderLineorderLine' =>$order_line[0]));
            if($order_line_sections){
                foreach($order_line_sections as $order_line_section){
                    $order_line_section->setDateDelete($date);
                    $order_line_section->setActive(3);

                    $em->persist($order_line_section);
                    $em->flush();

                    $product_in_section = new ProductInSection();

                    $product_in_section->setDate($order_line_section->getDateStock());
                    $product_in_section->setExpirationdate($order_line_section->getExpirationdate());
                    $product_in_section->setQty($order_line_section->getQty());
                    $product_in_section->setProductproduct($order_line[0]->getProductproduct());
                    $product_in_section->setSectionsection($order_line_section->getSectionsection());
                    $product_in_section->setActive(1);

                    getEm()->persist($product_in_section);
                    getEm()->flush();

                }
            }

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;    
    }

    public function generateGiftcard($order_id,$serial=false,$amount){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $order = getEm()->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$order_id));
            $gift_card = new GiftCard();

            $gift_card->setOrderorder($order[0]);
            $gift_card->setValue($amount);
            $gift_card->setValueUsed(0);
            $gift_card->setGiftCardIdentify($serial);
            $gift_card->setActive(1);
            $date = new datetime();
            $gift_card->setDateCreate($date);

            getEm()->persist($gift_card);

            getEm()->flush();         

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = $gift_card->getIdgiftCard();
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function generateCreditNote($order_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $credit_note = new CreditNote();
            $order = getEm()->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$order_id));

            $credit_note->setValue($order[0]->getTotalAmount());
            $credit_note->setOrderorder($order[0]);
            $credit_note->setActive(1);
            $date = new datetime();
            $credit_note->setDateCreate($date);

            getEm()->persist($credit_note);

            getEm()->flush();         

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = $credit_note->getIdcreditNote();
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;   
    }

    public function addBillet($order_id, $billet_value){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $billet = new OrderBillet();
            $order = getEm()->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$order_id));

            $billet->setValue($billet_value);
            $billet->setOrderorder($order[0]);
            $billet->setActive(1);
            $date = new datetime();
            $billet->setDateCreate($date);

            getEm()->persist($billet);

            getEm()->flush();         

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = $billet->getIdorderBillet();
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;   
    }

    public function deleteBillet($order_billet_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $order_billet = getEm()->getRepository('OrderBillet')->findBy(array('active' => 1, 'idorderBillet' =>$order_billet_id));

            $order_billet[0]->setActive(3);
            $date = new datetime();
            $order_billet[0]->setDateDelete($date);

            getEm()->persist($order_billet[0]);

            getEm()->flush();         

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;   
    }

    public function deleteAllBillets($order_id){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $order = getEm()->getRepository('Order')->findBy(array('active' => 1, 'idorder' =>$order_id));
            $order_billets = getEm()->getRepository('OrderBillet')->findBy(array('active' => 1, 'orderorder' =>$order[0]));

            if($order_billets){
                foreach($order_billets as $order_billet){
                    $order_billet->setActive(3);
                    $date = new datetime();
                    $order_billet->setDateDelete($date);

                    getEm()->persist($order_billet);

                    getEm()->flush();
                }
            }       

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;   
    }

    // public function getReceiptContent($order_id){
    //     try {                      
    //         $begin = getEm()->getConnection()->beginTransaction(); 
    //         $order = getEm()->getRepository('Order')->findBy(array('idorder' => $order_id));

    //         $total = 0;
    //         $tax = 0;
    //         $user_name = "";
    //         $products = array();

    //         if($order[0]){
    //             $user_name = $order[0]->getUsersusers()->getPersonperson()->getName();

    //             $order_lines = getEm()->getRepository('OrderLine')->findBy(array('active' => 1, 'orderorder' => $order));
    //             if($order_lines){
    //                 $i = 0;
    //                 foreach($order_lines as $order_line){
    //                     $prod = $order_line->getProductproduct();
    //                     $variety = "";

    //                     if($order_line->getSizecolorsizecolor()){
    //                         $variety = " | ".$order_line->getSizecolorsizecolor()->getColor()->getName()." ".$order_line->getSizecolorsizecolor()->getSize()->getName();
    //                     }

    //                     $products[$i]['product_number'] = $prod->getProductNumber();
    //                     $products[$i]['name'] = $prod->getName()." ".$variety;
    //                     $products[$i]['qty'] = $order_line->getQty();
    //                     $products[$i]['unit_price'] = $order_line->getValue();
    //                     $products[$i]['discount'] = $order_line->getDiscount();
    //                     $total_price = ($products[$i]['unit_price'] * (100 - ($products[$i]['discount']/100)));
    //                     $products[$i]['total_price'] = $total_price;
    //                     $total += $products[$i]['total_price'];

    //                     $i++;
    //                 }
    //             }
    //         }          


    //         $data = array (
    //             "total" => $total,
    //             "tax" => $tax,
    //             "products" => $products
    //         );

    //         $result  = 'success';
    //         $message = 'query success';
    //         $data = json_encode($data);
    //         $commit = getEm()->getConnection()->commit();
    //     } catch (Exception $e) {
    //         $rollback = getEm()->getConnection()->rollback();
    //         $result  = 'error';
    //         $message = $e->getMessage();
    //         $data = "";
    //     }
       

    //     $data = array(
    //         "result"  => $result,
    //         "message" => $message,
    //         "data"    => $data
    //     );

    //     $json_data = json_encode($data);
    //     print $json_data;

    // }

    public function generateWithdraw($money_in,$user_id,$amount,$description){ //$money_in = 1 means a deposit; $money_in = 0 means a withdraw
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $deposit = new Deposit();
            $user = getEm()->getRepository('Users')->findBy(array('active' => 1, 'idusers' =>$user_id));

            if($money_in==0){
                $amount *= -1;
            }

            $deposit->setAmount($amount);
            $deposit->setExtraInfo($description);
            $deposit->setUseruser($user[0]);
            $deposit->setActive(1);
            $date = new datetime();
            $deposit->setDateCreate($date);

            getEm()->persist($deposit);

            getEm()->flush();         

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
            $data = $deposit->getIddeposit();
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data; 
    }  

    /**
     * Get the name and value of the product in the thermal printer and show in display VFD
     * 
     * @param string $nameProduct
     * @param string $valueProduct
     * @return $json_data
     */
    public function printDisplayProduct($nameProduct, $valueProduct){
        try {
            $name = $nameProduct;
            $value = $valueProduct;
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, "http://localhost:27432/executer/print_display_product.php?nome=".$name."&preco=".$value);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $return = curl_exec( $ch );
            //var_dump($retorno);
            curl_close( $ch );

            $result  = 'success';
            $message = 'query success';
            $data = $return;
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;     
    }
    /**
     * Get the total of values of the product in the thermal printer and show in display VFD
     * 
     * @param string $totalValue
     * @return $json_data
     */
    public function printDisplayTotal($totalValue){ 
        try {
            $total = $totalValue;
            $ch = curl_init();
            curl_setopt( $ch, CURLOPT_URL, "http://localhost:27432/executer/print_display_total.php?total=".$total);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            $retorno = curl_exec( $ch );
            //var_dump($retorno);
            curl_close( $ch );

            $result  = 'success';
            $message = 'query success';
            $data = $return;
        }catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }    

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;  
    }

}