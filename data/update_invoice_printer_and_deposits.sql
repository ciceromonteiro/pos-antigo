-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 22-Nov-2017 às 12:50
-- Versão do servidor: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `standard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `deposit`
--

CREATE TABLE `deposit` (
  `iddeposit` int(11) NOT NULL,
  `user_iduser` int(11) NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `deposit_number` int(11) NOT NULL DEFAULT '0',
  `extra_info` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `invoice_printer`
--

CREATE TABLE `invoice_printer` (
  `idinvoice_printer` int(11) NOT NULL,
  `text_include_cashier` longtext CHARACTER SET utf8,
  `size_font` int(11) DEFAULT NULL,
  `description_printed` varchar(45) DEFAULT NULL,
  `number_copies` int(11) DEFAULT NULL,
  `number_decimal_places` int(11) DEFAULT NULL,
  `print_drawing_of_color` int(11) DEFAULT NULL,
  `enter_the_customers_phone_number` int(11) DEFAULT NULL,
  `change_the_location` int(11) DEFAULT NULL,
  `print_taxes_included` int(11) DEFAULT NULL,
  `do_not_print_project_information` int(11) DEFAULT NULL,
  `use_the_default_printer_instead` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`iddeposit`),
  ADD KEY `fk_deposit_user1_idx` (`user_iduser`);

--
-- Indexes for table `invoice_printer`
--
ALTER TABLE `invoice_printer`
  ADD PRIMARY KEY (`idinvoice_printer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `iddeposit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `invoice_printer`
--
ALTER TABLE `invoice_printer`
  MODIFY `idinvoice_printer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `deposit`
--
ALTER TABLE `deposit`
  ADD CONSTRAINT `fk_deposit_user1` FOREIGN KEY (`user_iduser`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
