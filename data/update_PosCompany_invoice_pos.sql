/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Claudio da Cruz Silva Junior
 * Created: 14/12/2017
 */


ALTER TABLE pos_company ADD COLUMN bank_br INT NULL;
ALTER TABLE pos_company ADD COLUMN agency_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN account_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN digit_account_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN bank_wallet_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN specie_br INT NULL;
ALTER TABLE pos_company ADD COLUMN interest_day_maturity_invoice_br INT NULL;
ALTER TABLE pos_company ADD COLUMN days_after_maturity_invoice_br INT NULL;
ALTER TABLE pos_company ADD COLUMN value_of_mulct_br DECIMAL(11,2) NULL;
ALTER TABLE pos_company ADD COLUMN code_agreement_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN wallet_variation_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN agency_dv_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN operation_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN sequence_number_file_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN wallet_code_br INT NULL;
ALTER TABLE pos_company ADD COLUMN registered_invoice_br INT NULL;
ALTER TABLE pos_company ADD COLUMN aceite_br VARCHAR(45) NULL;
ALTER TABLE pos_company ADD COLUMN assignor_code_br VARCHAR(45) NULL;



ALTER TABLE invoice ADD COLUMN limit_date_discount TIMESTAMP NULL;
ALTER TABLE invoice ADD COLUMN status_bank INT NOT NULL;
ALTER TABLE invoice ADD COLUMN status_payment INT NOT NULL;
ALTER TABLE invoice ADD COLUMN value_payment DECIMAL(11,2) NULL;



ALTER TABLE pos ADD CONSTRAINT pos_pos_company_idx
FOREIGN KEY(pos_company_idpos_company) REFERENCES pos_company (idpos_company);
ALTER TABLE pos ADD COLUMN pos_number VARCHAR(45) NOT NULL;
ALTER TABLE pos ADD COLUMN valid_until TIMESTAMP NOT NULL;
ALTER TABLE pos ADD COLUMN qty_users VARCHAR(45) NOT NULL;
ALTER TABLE pos ADD COLUMN qty_waiters VARCHAR(45) NOT NULL;









