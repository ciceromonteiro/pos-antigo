<?php

require_once '../../config/database/read_ini.php';
$ini = new read_ini('../../config/database/data.ini');

#== dados do banco

$dsn = "mysql:dbname=".$ini->getdatabase().";host=".$ini->gethost();
$user = $ini->getuser();
$password = $ini->getpassword();
$pdo = new PDO($dsn, $user, $password);
/*
$dbhost = 'localhost';
$dbname = 'standard';
$dbuser = 'root';
$pdo = new PDO( 'mysql:host=' . $dbhost . ';dbname=' . $dbname, $dbuser );
*/
#== consultas
$query_host = "select data_value from settings_additional where data_type = 'ftp_of_provides_host' limit 1";
$query_user = "select data_value from settings_additional where data_type = 'ftp_of_provides_user' limit 1";
$query_pass = "select data_value from settings_additional where data_type = 'ftp_of_provides_password' limit 1";
    
#== dados do ftp
$host = $pdo->query($query_host)->fetch()[0];
$user = $pdo->query($query_user)->fetch()[0];
$pass = $pdo->query($query_pass)->fetch()[0];
$conn = ftp_connect($host); 

#== url ftp
$urlftp = "ftp://$user@$host:21";
if ($pass != ''){
    $urlftp = "ftp://$user:$pass@$host:21";
}

if (ftp_login($conn, $user, $pass)){
    $path = '';
    $contents = ftp_nlist($conn, ".");
    
    create_directories($conn, $contents, $path, $urlftp);
    download_files($conn, $contents, $path, $urlftp);
    
}


function create_directories($conn, $contents, $path, $urlftp){  
    
    foreach ($contents as $content){
        
        if (is_dir($urlftp.$content)){
            if (!file_exists($content)) {
                mkdir($content);
            }
            $contents2 = ftp_nlist($conn, $path.$content);
            create_directories($conn, $contents2, $path, $urlftp);
        }
    }
}

function download_files($conn, $contents, $path, $urlftp){
    
    foreach ($contents as $content){
        if (!is_dir($urlftp.$content)){
            ftp_get($conn, $content, $content, FTP_BINARY);
            //echo "========= $content =========";
        } else {
            $contents2 = ftp_nlist($conn, $path.$content);
            download_files($conn, $contents2, $path, $urlftp);
        }
    }
}

?>