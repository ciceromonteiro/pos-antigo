<?php
    
    // importação da classe 
    require_once '../../config/database/read_ini.php';
    
    $ini = new read_ini('../../config/database/data.ini');
    
    $dsn = "mysql:dbname=".$ini->getdatabase().";host=".$ini->gethost();
    $user = $ini->getuser();
    $password = $ini->getpassword();
    $pdo = new PDO($dsn, $user, $password);
    
    // arquivo .sql que dá truncate em todas as tabelas do banco de dados
    $truncate = file_get_contents("../truncate.sql");
    
    // arquivos .sql de inicialização do banco de dados
    $demo = file_get_contents("../demo.sql");
    $install = file_get_contents("../install.sql");
    $layout = file_get_contents("../layout-pck.sql");
    $permissions = file_get_contents("../permissions.sql");
    $permissions_demo = file_get_contents("../permissions_demo.sql");
    //$gTestData = file_get_contents("../GenerateTestData.sql");
    
    // execução dos SQLs acima
    $pdo->exec($truncate);
    $pdo->exec($install);
    $pdo->exec($layout);
    $pdo->exec($permissions);
    $pdo->exec($permissions_demo);
    $pdo->exec($demo);
    $pdo->exec($gtd);
    
?>