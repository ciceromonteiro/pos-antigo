/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  wandesonpaiva
 * Created: 26/10/2017
 */

ALTER TABLE company_info ADD COLUMN pos_company_idpos_company INT;

ALTER TABLE company_info ADD CONSTRAINT pos_company_idpos_company
FOREIGN KEY(pos_company_idpos_company) REFERENCES pos_company (idpos_company);

ALTER TABLE pos_company ADD default_message_invoice text;
