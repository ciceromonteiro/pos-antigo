-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 29-Nov-2017 às 16:56
-- Versão do servidor: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `standard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `code_zip_code`
--

CREATE TABLE `code_zip_code` (
  `idcode_zip_code` int(11) NOT NULL,
  `person_idperson` int(11) DEFAULT NULL,
  `company_idCompany` int(11) DEFAULT NULL,
  `zipCode` varchar(45) NOT NULL,
  `date_create` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `code_zip_code`
--
ALTER TABLE `code_zip_code`
  ADD PRIMARY KEY (`idcode_zip_code`),
  ADD UNIQUE KEY `person_idperson` (`person_idperson`),
  ADD UNIQUE KEY `company_idCompany` (`company_idCompany`),
  ADD KEY `fk_codeZipCode_person_idx` (`person_idperson`) USING BTREE,
  ADD KEY `fk_codeZipCode_company_idx` (`company_idCompany`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `code_zip_code`
--
ALTER TABLE `code_zip_code`
  MODIFY `idcode_zip_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `code_zip_code`
--
ALTER TABLE `code_zip_code`
  ADD CONSTRAINT `fk_codeZipCode_company_idx` FOREIGN KEY (`company_idCompany`) REFERENCES `company` (`idcompany`),
  ADD CONSTRAINT `fk_codeZipCode_person_idx` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
