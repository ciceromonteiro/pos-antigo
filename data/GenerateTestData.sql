INSERT INTO `currency` (`idcurrency`, `name`, `symbol`, `date_create`, `date_update`, `date_delete`, `active`, `iso`, `exchange_rate`, `exchange_unit`, `smallest_denomination`, `account`, `iban`, `bic`, `account_currency_code`) VALUES (NULL, 'Defaut', 'D$', CURRENT_TIMESTAMP, NULL, NULL, '1', 'trs', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `zz_country` (`idzz_country`, `name`, `date_create`, `date_update`, `date_delete`, `active`, `code`) VALUES (NULL, 'Brazil', CURRENT_TIMESTAMP, NULL, NULL, '1', '55');
INSERT INTO `zz_state` (`idzz_state`, `name`, `zz_country_idzz_country`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, 'Default', '1', NOW(), NULL, NULL, '1');
INSERT INTO `pos_company` (`idpos_company`, `zz_state_idzz_state`, `license`, `name`, `fantasy_name`, `register_number`, `address_street`, `address_number`, `address_district`, `address_city`, `address_zipcode`, `address_complement`, `address_reference`, `vaddress_street`, `vaddress_number`, `vaddress_district`, `vaddress_city`, `vaddress_zipcode`, `vaddress_complement`, `vaddress_reference`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', '987654321', 'Default', 'Easy-default', '123456789', 'street mr crowley', '610', 'Neópolis', 'Natal', '59025040', 'perto da igreja', NULL, 'rua major antunes', NULL, 'Lagoa azul', 'natal', '5925484', NULL, NULL, CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `ticket_tmpt` (`idticket_tmpt`, `title`, `date_create`, `date_update`, `date_delete`, `active`, `info`, `print_two_lines`, `print_promo_and_normal_price`, `print_store_address`, `print_product_tax`, `print_product_details`, `print_product_number`, `print_tip`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1', NULL, '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `temp_ticket_tmpt` (`idtemp_ticket_tmpt`, `title`, `date_create`, `date_update`, `date_delete`, `active`, `info`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1', NULL);
INSERT INTO `pos` (`idpos`, `pos_company_idpos_company`, `pos_number`, `valid_until`, `qty_users`, `qty_waiters`, `date_create`, `date_update`, `date_delete`, `active`, `ticket_tmpt_idticket_tmpt`, `license`, `temp_ticket_tmpt_idtemp_ticket_tmpt`) VALUES (NULL, '1', '124578', CURRENT_TIMESTAMP, '1', '10', CURRENT_TIMESTAMP, NULL, NULL, '1', '1', NULL, '1');
INSERT INTO `customer_gp` (`idcustomer_gp`, `name`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1');

INSERT INTO `payment_mtd` (`idpayment_mtd`, `name`, `path_platform`, `date_create`, `date_update`, `date_delete`, `active`, `invoice`, `deadlinemonth`, `deadlineyear`) VALUES (NULL, 'Defaut', '/var/test/default', CURRENT_TIMESTAMP, NULL, NULL, '1', '58', '45', '69');

INSERT INTO `department` (`iddepartment`, `name`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1');
INSERT INTO `tmpt_footer` (`idtmpt_footer`, `name`, `create_date`, `update_date`, `delete_date`, `active`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1');
INSERT INTO `tmpt_header` (`idtmpt_header`, `name`, `create_date`, `update_date`, `delete_date`, `active`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1');
INSERT INTO `invoice_tmpt` (`idinvoice_tmpt`, `tmpt_header_idtmpt_header`, `tmpt_footer_idtmpt_footer`, `name`, `info`, `font_size`, `alternate_color_line`, `print_customer_phone`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', '1', 'Default', NULL, '12', '1', '0', NOW(), NULL, NULL, '1');
INSERT INTO `language` (`idlanguage`, `name`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1');
INSERT INTO `person_settings` (`idperson_settings`, `inv_show_customer_name`, `inv_group_invoices`, `inv_tax_free`, `inv_invoice_print_exclusive`, `inv_hide_products`, `inv_electronic_invoice`, `inv_info`, `invoice_tmpt_idinvoice_tmpt`, `inv_email`, `inv_track_email`, `currency_idcurrency`, `person_idperson`, `ord_show_info`, `ord_hide_from_statistics`, `ord_use_com_address`, `ticket_tmpt_idticket_tmpt`, `project_idproject`, `ord_credit_limit`, `charge_times`, `language_idlanguage`, `auth_receive_sms`, `auth_receive_email`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', '1', '0', '0', '0', '0', NULL, '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '1', '0', '0', NOW(), NULL, NULL, '1');

INSERT INTO `product_gp` VALUES (NULL, NULL, 'test', NULL, NULL, NULL, CURRENT_TIMESTAMP, NULL, NULL, '1');

#INSERT INTO `person` VALUES ('1', '1', '1', '1', '1', NULL, 'Default', NULL, NULL, NULL, NULL, CURRENT_TIMESTAMP, NULL, NULL, '1', '1');

#INSERT INTO `person_data` VALUES (NULL, 1, 'test', 'value_test', CURRENT_TIMESTAMP, NULL, NULL, '1');

INSERT INTO `company` (`idcompany`, `pos_idpos`, `name`, `fantasy_name`, `register_number`, `info`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', 'Default', 'Default-Fantasy', '12345789', NULL, CURRENT_TIMESTAMP, NULL, NULL, '1');

INSERT INTO `users_type` (`idusers_type`, `name`) VALUES (NULL, 'Default');
INSERT INTO `job` (`idjob`, `department_iddepartment`, `name`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', 'Default', NOW(), NULL, NULL, '1');
#INSERT INTO `users` (`idusers`, `users_type_idusers_type`, `job_idjob`, `person_idperson`, `login`, `password`, `system_version`, `multiple_connections_limit`, `date_creation`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', '1', '1', 'Default', 'default', '1', '1', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `log_time` (`idlog_time`, `users_idusers`, `pos_idpos`, `login_date`, `status`, `current_multiple_connections`) VALUES (NULL, '1', '1', '2017-02-21 00:00:00', '1', '1');
INSERT INTO `project` VALUES (NULL, 'test', 'test', CURRENT_TIMESTAMP, NULL, NULL, '1', '12.00', '2017-02-21 00:00:00', NULL, NULL, '1');
INSERT INTO `manufacturer` (`idmanufacturer`, `name`, `negociated_discount`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, 'Microsoft','1', CURRENT_TIMESTAMP, NULL, NULL, NULL);

INSERT INTO `unit` (`idunit`, `unit_idunit`, `name`, `divisible`, `ratio`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, NULL, 'Default', '0', NULL, NOW(), NULL, NULL, '1');
INSERT INTO `product_collection` (`idcollection`, `name`, `date_create`, `date_update`, `date_delete`, `active`, `date_start`, `date_end`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1', NULL, NULL);
INSERT INTO `supplier` (`idsupplier`, `company_idcompany`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', NOW(), NULL, NULL, '1');
INSERT INTO `product_department` (`idproduct_department`, `name`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1');
INSERT INTO `warranty` (`idwarranty`, `name`, `duration`, `duration_unit`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, 'Default', '1', '0', NOW(), NULL, NULL, '1');
INSERT INTO `warehouse` (`idwarehouse`, `name`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, 'Default', NOW(), NULL, NULL, '1');
INSERT INTO `hall` (`idhall`, `warehouse_idwarehouse`, `name`, `width`, `height`, `position_top`, `position_left`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', 'Default', '100', '100', '10', '10', NOW(), NULL, NULL, '1');
INSERT INTO `wardrobe` (`idwardrobe`, `hall_idhall`, `name`, `width`, `height`, `position_top`, `position_left`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', 'Default', '50', '50', '20', '20', NOW(), NULL, NULL, '1');
INSERT INTO `shelf` (`idshelf`, `wardrobe_idwardrobe`, `name`, `fixed`, `position_top`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', 'Default', '1', '372', NOW(), NULL, NULL, '1');
INSERT INTO `section` (`idsection`, `shelf_idshelf`, `name`, `fixed`, `position_left`, `date_create`, `date_update`, `date_delete`, `active`) VALUES (NULL, '1', 'Default', '1', '0', NOW(), NULL, NULL, '1');

INSERT INTO `software_accounting` VALUES (NULL, 'None', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, '24SevenOffice', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Agresso', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Agro Okonomi', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Ajour', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Akelius', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Axapta', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'BAS Okonomisystem', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'DI Systemer', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Duett Regnskap', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'economic', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Formula Regnskap', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Info Easy Regnskap', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Mamut', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Maritech Regnskap', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Matrix Okonomi', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Microsoft Dyn. 4.0 (n4b)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'PCK', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'PowerOfficeGo', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Procountor', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Sage50', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Scenario', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Tripletex (Mamut)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Uni Micro', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'VB Special (R2,R3,R1)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Visma Avendo', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Visma Business', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Visma Contracting', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Visma Enterprise Okonomi', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Visma Global', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Visma Global(yyyy.MM.dd)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Visma MultiSoft', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Visma Rubicon', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'XLEDGER (AR10)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'XLEDGER (GL02)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `software_accounting` VALUES (NULL, 'Zirius Regnskap', CURRENT_TIMESTAMP, NULL, NULL, '1');

INSERT INTO `credit_management` VALUES (NULL, 'None', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `credit_management` VALUES (NULL, 'Hokas', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `credit_management` VALUES (NULL, 'Ikas', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `credit_management` VALUES (NULL, 'IFS', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `credit_management` VALUES (NULL, 'Predator', CURRENT_TIMESTAMP, NULL, NULL, '1');

INSERT INTO `credit_card` (`idcreditcard`, `bbsid`, `name`, `description`, `accounting`, `show_balancing`, `send_register`, `date_create`, `date_update`, `date_delete`, `active`) VALUES
(1, 1, 'Bankkort', 'Bankkort', '1920', 1, 1, '2017-03-17 18:13:43', NULL, NULL, 1),
(2, 2, 'Visa', 'Visa', '1940', 1, 1, '2017-03-17 18:13:43', NULL, NULL, 1),
(3, 3, 'EuroCard', 'EuroCard', '1940', 1, 1, '2017-03-17 18:13:43', NULL, NULL, 1),
(4, 4, 'AmEx', 'AmEx', '1940', 1, 1, '2017-03-17 18:13:43', NULL, NULL, 1),
(5, 5, 'Diners', 'Diners', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(6, 6, 'JCB', 'JCB', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(7, 7, 'Trumf', 'Trumf', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(8, 8, 'Maestro', 'Maestro', '1940', 1, 1, '2017-03-17 18:13:43', NULL, NULL, 1),
(9, 9, 'Lindex', 'Lindex', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(10, 10, 'Ikano', 'Ikano', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(11, 11, 'NBBL', 'NBBL', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(12, 12, 'Gavekort Senter', 'Gavekort Senter', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(13, 13, 'Gavekort Kjede', 'Gavekort Kjede', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(14, 14, 'Esso Mastercard', 'Esso Mastercard', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(15, 15, 'Sentrumskortet', 'Sentrumskortet', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(16, 16, 'Statoil', 'Statoil', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(17, 17, 'XPonCard', 'XPonCard', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(18, 18, 'Multicard', 'Multicard', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(19, 19, 'Universal', 'Universal', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(20, 20, 'Bankaxept(EMV)', 'Bankaxept(EMV)', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(21, 21, 'Resurs Bank', 'Resurs Bank', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(22, 22, 'NG Bedriftskort', 'NG Bedriftskort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(23, 23, 'BBS Senter Gavekort', 'BBS Senter Gavekort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(24, 24, 'BBS Kjede Gavekort', 'BBS Kjede Gavekort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(25, 25, 'EMV ePurse', 'EMV ePurse', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(26, 26, 'Nokaskort', 'Nokaskort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(27, 27, 'S&S Medlemskort', 'S&S Medlemskort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(28, 28, 'Bring Bedriftskort', 'Bring Bedriftskort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(29, 29, 'Nordea', 'Nordea', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(30, 30, 'Handelsbanken', 'Handelsbanken', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(31, 31, 'Swedbank', 'Swedbank', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(32, 32, 'SEB', 'SEB', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(33, 33, 'Ressurs', 'Ressurs', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(34, 34, 'Dankort', 'Dankort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(35, 35, 'Coop Visa', 'Coop Visa', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(36, 36, 'Payex Gavekort', 'Payex Gavekort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(37, 37, 'BBS - Vot', 'BBS - Vot', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(38, 38, 'Trumf Visa', 'Trumf Visa', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(39, 39, 'Gavekort 1', 'Gavekort 1', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(40, 40, 'Cashcoms presentkort', 'Cashcoms presentkort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(41, 41, 'Storcash Bedrift', 'Storcash Bedrift', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(42, 42, 'PBS Gavekort', 'PBS Gavekort', '0', 0, 0, '2017-03-17 18:13:43', NULL, NULL, 1),
(43, 43, 'Forbrungsforening', 'Forbrungsforening', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(44, 44, 'Sparxpres', 'Sparxpres', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(45, 45, 'China Unionpay', 'China Unionpay', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(46, 46, 'Rikslunchen', 'Rikslunchen', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(47, 47, 'Kjedekort 1', 'Kjedekort 1', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(48, 48, 'Collector Credit', 'Collector Credit', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(49, 49, 'FDM, DK', 'FDM, DK', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(50, 50, 'BBS Senter Gavekort', 'BBS Senter Gavekort', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(51, 51, 'PBS Centerkort', 'PBS Centerkort', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(52, 52, 'LIC Card', 'LIC Card', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(53, 53, 'Accept Card', 'Accept Card', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(54, 54, 'Coop Mastercard', 'Coop Mastercard', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(55, 55, 'Oberthur Gavekort', 'Oberthur Gavekort', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(56, 56, 'Bunnpris Bedrift', 'Bunnpris Bedrift', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(57, 57, 'Rikskortet', 'Rikskortet', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(58, 58, 'Coop Bedrift', 'Coop Bedrift', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(59, 59, 'Swedbank (Debet/Credit)', 'Swedbank (Debet/Credit)', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(60, 60, 'Visa Bankkort', 'Visa Bankkort', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(61, 61, 'Mastercard Bankkort', 'Mastercard Bankkort', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(62, 62, 'Swedbank', 'Swedbank', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(63, 63, 'Visa DK', 'Visa DK', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(64, 64, 'Mastercard DK', 'Mastercard DK', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(65, 65, 'Maestro DK', 'Maestro DK', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(66, 66, 'Diners DK', 'Diners DK', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(67, 67, 'Amex DK', 'Amex DK', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(68, 68, 'Ikano Finans', 'Ikano Finans', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(69, 69, 'KappAhl Club', 'KappAhl Club', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(70, 70, 'MediaMarkt', 'MediaMarkt', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(71, 71, 'Statoil MC', 'Statoil MC', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1),
(72, 72, 'Visa Prepaid', 'Visa Prepaid', '0', 0, 0, '2017-03-17 18:13:44', NULL, NULL, 1);

INSERT INTO `company_factoring` VALUES (NULL, 'None', CURRENT_TIMESTAMP, NULL, NULL, '1' );
INSERT INTO `company_factoring` VALUES (NULL, 'SG Finans', CURRENT_TIMESTAMP, NULL, NULL, '1' );
INSERT INTO `company_factoring` VALUES (NULL, 'Svea Finans', CURRENT_TIMESTAMP, NULL, NULL, '1' );
INSERT INTO `company_factoring` VALUES (NULL, 'Gothia Finans', CURRENT_TIMESTAMP, NULL, NULL, '1' );
INSERT INTO `company_factoring` VALUES (NULL, 'DnB', CURRENT_TIMESTAMP, NULL, NULL, '1' );
INSERT INTO `company_factoring` VALUES (NULL, 'Nordea Finans', CURRENT_TIMESTAMP, NULL, NULL, '1' );

INSERT INTO `tax` VALUES (NULL, 'Høy sats', '25.00', '2700', '0', '3', CURRENT_TIMESTAMP, NULL, NULL, 1);
INSERT INTO `tax` VALUES (NULL, 'Middels sats', '15.00', '2701', '0', '13', CURRENT_TIMESTAMP, NULL, NULL, 1);
INSERT INTO `tax` VALUES (NULL, 'Lav sats', '10.00', '2702', '0', NULL, CURRENT_TIMESTAMP, NULL, NULL, 1);

INSERT INTO `zz_country` VALUES (NULL, 'AF', 'Afghanistan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AL', 'Albania', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'DZ', 'Algeria', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'DS', 'American Samoa', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AD', 'Andorra', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AO', 'Angola', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AI', 'Anguilla', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AQ', 'Antarctica', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AG', 'Antigua and Barbuda', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AR', 'Argentina', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AM', 'Armenia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AW', 'Aruba', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AU', 'Australia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AT', 'Austria', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AZ', 'Azerbaijan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BS', 'Bahamas', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BH', 'Bahrain', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BD', 'Bangladesh', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BB', 'Barbados', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BY', 'Belarus', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BE', 'Belgium', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BZ', 'Belize', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BJ', 'Benin', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BM', 'Bermuda', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BT', 'Bhutan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BO', 'Bolivia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BA', 'Bosnia and Herzegovina', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BW', 'Botswana', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BV', 'Bouvet Island', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BR', 'Brazil', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IO', 'British Indian Ocean Territory', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BN', 'Brunei Darussalam', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BG', 'Bulgaria', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BF', 'Burkina Faso', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'BI', 'Burundi', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KH', 'Cambodia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CM', 'Cameroon', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CA', 'Canada', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CV', 'Cape Verde', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KY', 'Cayman Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CF', 'Central African Republic', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TD', 'Chad', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CL', 'Chile', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CN', 'China', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CX', 'Christmas Island', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CC', 'Cocos (Keeling) Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CO', 'Colombia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KM', 'Comoros', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CG', 'Congo', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CK', 'Cook Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CR', 'Costa Rica', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'HR', 'Croatia (Hrvatska)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CU', 'Cuba', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CY', 'Cyprus', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CZ', 'Czech Republic', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'DK', 'Denmark', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'DJ', 'Djibouti', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'DM', 'Dominica', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'DO', 'Dominican Republic', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TP', 'East Timor', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'EC', 'Ecuador', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'EG', 'Egypt', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SV', 'El Salvador', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GQ', 'Equatorial Guinea', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ER', 'Eritrea', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'EE', 'Estonia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ET', 'Ethiopia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'FK', 'Falkland Islands (Malvinas)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'FO', 'Faroe Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'FJ', 'Fiji', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'FI', 'Finland', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'FR', 'France', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'FX', 'France, Metropolitan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GF', 'French Guiana', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PF', 'French Polynesia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TF', 'French Southern Territories', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GA', 'Gabon', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GM', 'Gambia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GE', 'Georgia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'DE', 'Germany', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GH', 'Ghana', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GI', 'Gibraltar', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GK', 'Guernsey', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GR', 'Greece', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GL', 'Greenland', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GD', 'Grenada', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GP', 'Guadeloupe', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GU', 'Guam', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GT', 'Guatemala', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GN', 'Guinea', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GW', 'Guinea-Bissau', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GY', 'Guyana', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'HT', 'Haiti', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'HM', 'Heard and Mc Donald Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'HN', 'Honduras', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'HK', 'Hong Kong', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'HU', 'Hungary', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IS', 'Iceland', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IN', 'India', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IM', 'Isle of Man', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ID', 'Indonesia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IR', 'Iran (Islamic Republic of)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IQ', 'Iraq', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IE', 'Ireland', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IL', 'Israel', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'IT', 'Italy', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CI', 'Ivory Coast', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'JE', 'Jersey', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'JM', 'Jamaica', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'JP', 'Japan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'JO', 'Jordan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KZ', 'Kazakhstan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KE', 'Kenya', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KI', 'Kiribati', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KP', 'Korea, Democratic People''s Republic of', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KR', 'Korea, Republic of', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'XK', 'Kosovo', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KW', 'Kuwait', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KG', 'Kyrgyzstan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LA', 'Lao People''s Democratic Republic', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LV', 'Latvia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LB', 'Lebanon', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LS', 'Lesotho', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LR', 'Liberia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LY', 'Libyan Arab Jamahiriya', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LI', 'Liechtenstein', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LT', 'Lithuania', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LU', 'Luxembourg', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MO', 'Macau', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MK', 'Macedonia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MG', 'Madagascar', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MW', 'Malawi', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MY', 'Malaysia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MV', 'Maldives', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ML', 'Mali', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MT', 'Malta', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MH', 'Marshall Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MQ', 'Martinique', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MR', 'Mauritania', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MU', 'Mauritius', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TY', 'Mayotte', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MX', 'Mexico', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'FM', 'Micronesia, Federated States of', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MD', 'Moldova, Republic of', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MC', 'Monaco', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MN', 'Mongolia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ME', 'Montenegro', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MS', 'Montserrat', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MA', 'Morocco', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MZ', 'Mozambique', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MM', 'Myanmar', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NA', 'Namibia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NR', 'Nauru', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NP', 'Nepal', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NL', 'Netherlands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AN', 'Netherlands Antilles', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NC', 'New Caledonia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NZ', 'New Zealand', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NI', 'Nicaragua', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NE', 'Niger', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NG', 'Nigeria', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NU', 'Niue', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NF', 'Norfolk Island', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'MP', 'Northern Mariana Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'NO', 'Norway', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'OM', 'Oman', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PK', 'Pakistan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PW', 'Palau', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PS', 'Palestine', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PA', 'Panama', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PG', 'Papua New Guinea', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PY', 'Paraguay', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PE', 'Peru', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PH', 'Philippines', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PN', 'Pitcairn', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PL', 'Poland', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PT', 'Portugal', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PR', 'Puerto Rico', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'QA', 'Qatar', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'RE', 'Reunion', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'RO', 'Romania', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'RU', 'Russian Federation', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'RW', 'Rwanda', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'KN', 'Saint Kitts and Nevis', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LC', 'Saint Lucia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'VC', 'Saint Vincent and the Grenadines', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'WS', 'Samoa', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SM', 'San Marino', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ST', 'Sao Tome and Principe', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SA', 'Saudi Arabia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SN', 'Senegal', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'RS', 'Serbia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SC', 'Seychelles', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SL', 'Sierra Leone', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SG', 'Singapore', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SK', 'Slovakia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SI', 'Slovenia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SB', 'Solomon Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SO', 'Somalia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ZA', 'South Africa', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GS', 'South Georgia South Sandwich Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ES', 'Spain', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'LK', 'Sri Lanka', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SH', 'St. Helena', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'PM', 'St. Pierre and Miquelon', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SD', 'Sudan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SR', 'Suriname', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SJ', 'Svalbard and Jan Mayen Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SZ', 'Swaziland', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SE', 'Sweden', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'CH', 'Switzerland', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'SY', 'Syrian Arab Republic', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TW', 'Taiwan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TJ', 'Tajikistan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TZ', 'Tanzania, United Republic of', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TH', 'Thailand', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TG', 'Togo', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TK', 'Tokelau', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TO', 'Tonga', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TT', 'Trinidad and Tobago', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TN', 'Tunisia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TR', 'Turkey', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TM', 'Turkmenistan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TC', 'Turks and Caicos Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'TV', 'Tuvalu', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'UG', 'Uganda', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'UA', 'Ukraine', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'AE', 'United Arab Emirates', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'GB', 'United Kingdom', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'US', 'United States', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'UM', 'United States minor outlying islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'UY', 'Uruguay', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'UZ', 'Uzbekistan', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'VU', 'Vanuatu', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'VA', 'Vatican City State', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'VE', 'Venezuela', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'VN', 'Vietnam', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'VG', 'Virgin Islands (British)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'VI', 'Virgin Islands (U.S.)', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'WF', 'Wallis and Futuna Islands', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'EH', 'Western Sahara', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'YE', 'Yemen', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ZR', 'Zaire', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ZM', 'Zambia', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `zz_country` VALUES (NULL, 'ZW', 'Zimbabwe', CURRENT_TIMESTAMP, NULL, NULL, '1');

INSERT INTO `supplier_format_api` VALUES (NULL, 'Station', '1');
INSERT INTO `supplier_format_api` VALUES (NULL, 'EFO/NELFO Purchase Order', '1');
INSERT INTO `supplier_format_api` VALUES (NULL, 'Malorama', '1');
INSERT INTO `supplier_format_api` VALUES (NULL, 'RNB', '1');
INSERT INTO `supplier_format_api` VALUES (NULL, 'Forlagssentralen', '1');
INSERT INTO `supplier_format_api` VALUES (NULL, 'SentralDistribusjon', '1');
INSERT INTO `supplier_format_api` VALUES (NULL, 'Yes vi leker', '1');

INSERT INTO `supplier_type_api` VALUES (NULL, 'Send Purchase Order', '1');
INSERT INTO `supplier_type_api` VALUES (NULL, 'Get Purchase Order', '1');
INSERT INTO `supplier_type_api` VALUES (NULL, 'Get Packing list', '1');

INSERT INTO `permission` VALUES (NULL, 'Create Department', 'Create department');
INSERT INTO `permission` VALUES (NULL, 'Update Department', 'Update department');
INSERT INTO `permission` VALUES (NULL, 'Read Department', 'Read department');
INSERT INTO `permission` VALUES (NULL, 'Delete Department', 'Delete department');

INSERT INTO `permission` VALUES (NULL, 'Create Project', 'Create project');
INSERT INTO `permission` VALUES (NULL, 'Update Project', 'Update project');
INSERT INTO `permission` VALUES (NULL, 'Read Project', 'Read project');
INSERT INTO `permission` VALUES (NULL, 'Delete Project', 'Delete project');

INSERT INTO `permission` VALUES (NULL, 'Create User', 'Create user');
INSERT INTO `permission` VALUES (NULL, 'Update User', 'Update user');
INSERT INTO `permission` VALUES (NULL, 'Read User', 'Read user');
INSERT INTO `permission` VALUES (NULL, 'Delete User', 'Delete user');

INSERT INTO `permission` VALUES (NULL, 'Update Permissions', 'Update permissions');
INSERT INTO `permission` VALUES (NULL, 'Read Permissions', 'Read permissions');
INSERT INTO `permission` VALUES (NULL, 'Remove Permissions', 'remove permissions');

INSERT INTO `permission` VALUES (NULL, 'Update permissions by type users', 'Update permissions by type users');
INSERT INTO `permission` VALUES (NULL, 'Read permissions by type users', 'Read permissions by type users');
INSERT INTO `permission` VALUES (NULL, 'Delete permissions by type users', 'Delete permissions by type users');

INSERT INTO `permission` VALUES (NULL, 'Create User Type', 'Create user type');
INSERT INTO `permission` VALUES (NULL, 'Update User Type', 'Update user type');
INSERT INTO `permission` VALUES (NULL, 'Read User Type', 'Read user type');
INSERT INTO `permission` VALUES (NULL, 'Delete User Type', 'Delete user type');

INSERT INTO `permission` VALUES (NULL, 'Create Jobs Type', 'Create jobs type');
INSERT INTO `permission` VALUES (NULL, 'Update Jobs Type', 'Update jobs type');
INSERT INTO `permission` VALUES (NULL, 'Read Jobs Type', 'Read jobs type');
INSERT INTO `permission` VALUES (NULL, 'Delete Jobs Type', 'Delete jobs type');

INSERT INTO `permission` VALUES (NULL, 'Create Stock', 'Create stock');
INSERT INTO `permission` VALUES (NULL, 'Update Stock', 'Update stock');
INSERT INTO `permission` VALUES (NULL, 'Read Stock', 'Read stock');
INSERT INTO `permission` VALUES (NULL, 'Delete Stock', 'Delete stock');

INSERT INTO `permission` VALUES (NULL, 'Create Hall', 'Create hall');
INSERT INTO `permission` VALUES (NULL, 'Update Hall', 'Update hall');
INSERT INTO `permission` VALUES (NULL, 'Read Hall', 'Read hall');
INSERT INTO `permission` VALUES (NULL, 'Delete Hall', 'Delete hall');

INSERT INTO `permission` VALUES (NULL, 'Create Wardrobes', 'Create wardrobes');
INSERT INTO `permission` VALUES (NULL, 'Update Wardrobes', 'Update wardrobes');
INSERT INTO `permission` VALUES (NULL, 'Read Wardrobes', 'Read wardrobes');
INSERT INTO `permission` VALUES (NULL, 'Delete Wardrobes', 'Delete wardrobes');

INSERT INTO `permission` VALUES (NULL, 'Create Product Size', 'Create product size');
INSERT INTO `permission` VALUES (NULL, 'Update Product Size', 'Update product size');
INSERT INTO `permission` VALUES (NULL, 'Read Product Size', 'Read product size');
INSERT INTO `permission` VALUES (NULL, 'Delete Product Size', 'Delete product size');

INSERT INTO `permission` VALUES (NULL, 'Create Color', 'Create color');
INSERT INTO `permission` VALUES (NULL, 'Update Color', 'Update color');
INSERT INTO `permission` VALUES (NULL, 'Read Color', 'Read color');
INSERT INTO `permission` VALUES (NULL, 'Delete Color', 'Delete color');

INSERT INTO `permission` VALUES (NULL, 'Create Country', 'Create country');
INSERT INTO `permission` VALUES (NULL, 'Update Country', 'Update country');
INSERT INTO `permission` VALUES (NULL, 'Read Country', 'Read country');
INSERT INTO `permission` VALUES (NULL, 'Delete Country', 'Delete country');

INSERT INTO `permission` VALUES (NULL, 'Create Supplier', 'Create supplier');
INSERT INTO `permission` VALUES (NULL, 'Update Supplier', 'Update supplier');
INSERT INTO `permission` VALUES (NULL, 'Read Supplier', 'Read supplier');
INSERT INTO `permission` VALUES (NULL, 'Delete Supplier', 'Delete supplier');

INSERT INTO `permission` VALUES (NULL, 'Create APIs', 'Create APIs');
INSERT INTO `permission` VALUES (NULL, 'Update APIs', 'Update APIs');
INSERT INTO `permission` VALUES (NULL, 'Read APIs', 'Read APIs');
INSERT INTO `permission` VALUES (NULL, 'Delete APIs', 'Delete APIs');

INSERT INTO `permission` VALUES (NULL, 'Create Template', 'Create template');
INSERT INTO `permission` VALUES (NULL, 'Update Template', 'Update template');
INSERT INTO `permission` VALUES (NULL, 'Read Template', 'Read template');
INSERT INTO `permission` VALUES (NULL, 'Delete Template', 'Delete template');
INSERT INTO `permission` VALUES (NULL, 'Cards to employeers', 'Defined image from cards to employeers');
INSERT INTO `permission` VALUES (NULL, 'Cards from access to employeers', 'Defined image from cards from access to employeers');
INSERT INTO `permission` VALUES (NULL, 'Layout from labels', 'Defined image for layout from labels');

INSERT INTO `permission` VALUES (NULL, 'Create Printer Settings', 'Create printer settings');
INSERT INTO `permission` VALUES (NULL, 'Update Printer Settings', 'Update printer settings');
INSERT INTO `permission` VALUES (NULL, 'Read Printer Settings', 'Read printer settings');
INSERT INTO `permission` VALUES (NULL, 'Delete Printer Settings', 'Delete printer settings');

INSERT INTO `permission` VALUES (NULL, 'Update E-Post', 'Update e-post');
INSERT INTO `permission` VALUES (NULL, 'Read E-Post', 'Read e-post');

INSERT INTO `product` VALUES (NULL, '1', '1', '1', '1', '1', '1', '1', 'test', '123', NULL, NULL, '12.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', NULL, NULL, '0', '1', '1', '0', '0', '1', CURRENT_TIMESTAMP, NULL, NULL, '1', '1', NULL);

INSERT INTO `license` VALUES (NULL, 'H6DF-8G3F-GJ36-557F-D88G', CURRENT_TIMESTAMP, '0', '10', '10', '0', '0000-00-00 00:00:00.000000');

INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Get parked order', 'get_parked_order', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Pay', 'pay', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Order info', 'order_info', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Open credit order', 'open_credit_order', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Line info', 'line_info', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Discount', 'discount', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Delete Line', 'delete_line', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Cancel order', 'cancel_order', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'With drawal/deposit', 'with_drawal_deposit', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Open Cash drawer', 'open_cash_drawer', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Print receipt copy', 'print_receipt_copy', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Cash Statistics', 'cash_statistics', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Credit last sale', 'credit_last_sale', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Print temp. receipt', 'print_temp_receipt', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Settle invoice by Cash', 'settle_invoice_by_cash', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Pre payment', 'pre_payment', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Split order', 'split_order', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Check giftcard status', 'check_giftcard_status', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Check electr. gift card', 'check_electr_gift_card', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Customer orders', 'customer_orders', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Customer statistics', 'customer_statistics', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Customer updating', 'customer_updating', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Article statistics', 'article_statistics', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Article updating', 'artible_updating', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Price Search', 'price_search', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Search products', 'search_products', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Return', 'return', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Receipt', 'receipt', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_btn_functions` VALUES (NULL, 'Central Gift Vouchers', 'central_gift_vouchers', 'us', CURRENT_TIMESTAMP, NULL, NULL, '1');

INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%A', 'Avslutt kontant med kvittering', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%a', 'Avslutt kontant uten kvittering', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%B', 'Avslutt bankkort med kvittering', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%b', 'Avslutt bankkort uten kvittering', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%C[x]', 'Legg til kontant betaling (x = belop)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%c[x]', 'Legg til bankkort betaling (x = belop)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%d', 'innbetaling av faktura', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%e<varenr/strekkode>.<serienr>', 'Legg til vare med serienr', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%F<x>', 'Sett ordrelinjeregel pa aktiv linje (x=ordrelinjeregel id)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%G', 'Merk linje som gave', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%g', 'Ta vekk merke som gave', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%H[x[.y]]', 'Bytt til favorittgruppe x (0=standard) og evt fane y', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%i', 'Rediger ordreinfo / hent fra tidligere', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%j', 'Midlertidig kvittering', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%K[Kundenummer]', 'Velg kunde fra kundenr. Spor om nr dersom ikke kundenr er fylt ut', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%L[A4|E<x>]', 'Kopi av siste kvittering (%L . %LA4 eller %LEx der x er etikett ID)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%M', 'Vis kart fra kundeadressen', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%N<x>', 'Sett ordremal og lagre ordren (x=ordremal id)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%O', 'Apne kasseskuff', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%P[x]', 'Hent laveste pris pa vare siste x dager (365 dager er standard dersom x er utelatt)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%p[x]', 'Hent laveste pris pa vare siste x dager for aktuell kundle (365 dager er standard dersom x er utelatt)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%Q', 'Bytte firma', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%S', 'Angi totalpris pa ordren', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%T<x>', 'Legg til ekstratekst og vis linjeinfo. (x=tekst)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%t<x>', 'Legg til ekstratekst pa linje. (x=tekst)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%u', 'Sla opp saldo pa aktiv ordrelinje', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%v', 'Sla opp saldo i annen butikk', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%w', 'Uttak i kasse', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%w+', 'Innskudd i kasse', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%X', 'Debiter pa rom (Hotel integrasjon)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%y[.x[.y[.z]]]', 'Velg varer fra varegrupper (x=varegr. 1 id. y=v.gr. 2id. z=v.gr. 3id). Man kan benytte %yy for a benytte varegruppenr i stedet for id', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%Z', 'Ressursallokeringsoversikt', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%z', 'Ny ressursallokering', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%A[x]', 'Skrive ut ordreetiketter (x=antall. x=? vil sporre)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%B<x>', 'Apne et vindu fra admin (x er navn pa vinduet - Sla opp i manualen for oversikt over navn)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%C<ordrefelt>', 'Spor om vardien du onsker a sette i feltet.Feks. %%CYourRef', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%C<ordrefelt>=<verdi>', 'Spor om verdien du onsker a sette i feltet.Feks. %%CYourRef', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%C<ordrefelt>==<annetordrefelt>', 'Setter ordrefeltet lik et annet ordrefelt.Feks: %%CYourRef==Customer.Contact', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%C<ordrefelt>=#<sokefelt>.[ledetekst][,verdi]', 'velger ordrefeltet fra en liste der sokefelt stemmer med verdien Dersom ikke verdi er angitt sporres det etter ved hjelp av ledeteksten. Geks: %%CCustomer=#Telephone,Kundens telefonnr %%CCustomer=#CustomerGroup.ID,,3 Dersom du angir OrderLines.<felt> i <ordrefelt> vil bety felt pa aktiv ordrelinje. Feks: %%cOrderLines.Discount=20 Dersom du angir OrderLinesAll,<felt> vil samtlige ordrelinjer endres.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%D[ordreId]', 'Hent ordre', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%G[link]', 'Apner link fra vare eller angitt link', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%J[D]', 'Overfore hele ordren til bestilling. Dersom D er angitt overfores leveringsdato.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%J1[D]', 'Overfore aktiv ordrelinje til bestilling/eksisterende bestilling', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%J@[D]', 'Overfore hele ordren til eksisterende bestillinger', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%K[printer]', 'Skriver ut byttelapp pa aktiv linje', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%M[Melding]', 'Send SMS', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%O', 'Splitt ordre', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%F[x]', 'Forhandsbetaling (x=prosentforslag)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%L[x]', 'Velg kunde fra kundegrupper (x=kundegruppe Id)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%P', 'Oppslag pa lev. varenr (orgsa sentralt)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%Q', 'Splitt ordre (Feks del regningen pa det hver og en har spist)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%R[$][Merket]', 'Parker/hent ordreDersom $ er angitt vil eksisterende selger ta over bordet/ordren (Feks %%R$)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%S', 'Veksle mellom ink/eks mva i kassebildet', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%Tx<Beskjed>', 'Skriver beskjed ut pa skriver. <br>Der x kan vare:<br> 2 - kasseskriver<br> 3 - plukklisteskriver<br> 4 - skriver<br> 5 - ekstraskriver 1<br> 6 - ekstraskriver 2<br> 7 - ekstraskriver 3<br> 8 - etikettskriver<br> Feks: %%T3BEDES<br> NB! Selger merking on ordrenr hentes fra akriv ordre.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%U', 'Hent vareantall fra handterminal', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%V<uId>', 'Skrolle opp(%%vu) eller ned (%%vd) en linje', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%W', 'Skrive ut kvittering eller faktura ut i fra serienr.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%X', 'Sende e-post til kundle', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%Y', 'Viser dagens omsetning grafisk', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%Z[x]', 'Apner betalingsbildet og klikker evt pa en betalingsknapp x=1 klikker pa gavekort, x=2 klikker pa bankkort. Feks %%Z2', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%N<1|2>', 'Pck Servitor kommandoer<br> 1 - Sett aktiv medarbeiders favorittvarer<br> 2 - Send beskjed til servitorer', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%A[<pakkenavn[:<pakkepris>[;<antall varer>]]]', 'Lagpakkepris. Feks: %%$ASkipakke;1200', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%b[-|+]', 'Toggler fortegn pa aktuell linje. Dersom man angir - eller + bak sa settes antall til negativt eller positivt og gjor ingenting dersom allerede slik.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%c', 'Spor om hvilken selger som skal logges pa', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%d', 'Spor om hvilken selger som skal logges av', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%e<filnavn>', 'Apner en wordmal og fyller inn felter fra aktiv ordre', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%f', 'Lager avtale pa en Exchange server', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%G[ordretype]', 'Lagre ordre (Far tildelt ordrenummer) ordretype: 0 er parkert, 2 er kreditt', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%H', 'Vise kontantautomat menyen', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%Ix', 'Taxfree Skjema x=i Utsted skjemax=v Annuler skjemax=r Usted nytt skjems og annuler det gamle x=p Skriv ut skjema pa nytt dersom papirstopp etc x=c Send flere ordrenummer', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%J[VareID]', 'Se ufakturerte ordre som varen pa aktuell ordrelinje ligger pa, eller pa vare angitt.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%k[kode]', 'Vis slette linje og avbryt ordre i kassa (overstyr navarende selger)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%L<x><y>', 'Legg til tekst i plukklisteinfo der y er teksten og x er funksjon<br> x=0 Legg til tekst<br>x=1 Legg til tekst pa ny linje<br>x=2 Toggle tekst<br>x=3 Legg til tekst og apne linjeinfo<br>Feks %%$L2Ekstra ost', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%M<w>.<h>.<url>', 'Apne webside rett i kassa.<br>w=bredde og h er hoyde, url er url. Husk a starte med<br> <<<<<<<http://>>>>>><br>%%$M1024,700,http://showbooking.no?EmpId=<employee.id>%%$M1024,700,http://webshop.no?ArtNo=<orderlines.article.articleno>', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%N<Beskjed>', 'Viser en beskjed pa skjermen', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%O<felt1>|<Operator>|<Felt2>|<Kommando hvis sant>[|<Kommando hvis usann>]', 'Vis leveringsinformasjon', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%Q<id>[.parameter]', 'Start et ordretillegg, NB! Kun etter avtale', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%R', 'Vis leveringsinformasjon', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%S', 'Splitt en ordre og ta vare pa leveringsinformasjon', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%T[dager]', 'Vis tipsrapport for aktuell medarbeider. Dager i minus gir rapport for denne dagen tilbake i tid. Positivt antall dager gir rapport fra sa mange dager og til dagens dato.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%U', 'Viser omsetningsrapport pa skjerm', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%V[+]', 'Spor om merket hvis ikke utfylt (viser bordkart dersom definert) + bak betyr spor alltid, selv om angitt fra for. Altsa overskriv.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%Y', 'Importer ordre fra fil<br>Pos 9, 10 lang - Kundenummer<br>Pos 40, 20 lang - Varenummer/Strkkode<br> Pos 60, 10 lang - Antall (kommaseparator som regionale innstillinger pa PC`n)', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%W[0|1]', 'Endre "Husk ansatt i kassa"<br>%%%W toggler<br>%%%W0 Skrur den av<br>%%%W1 skrur den pa', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%X', 'Vis webshop status', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%Z', 'Sla opp lojalitetskort status', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%2[Egendefinert sporsmalstekst]', 'Angi eksternt lojalitetskortnummer pa forrige ordre', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%3[Egendefinert sporsmalstekst]', 'Sla opp ordrelinjer som er solgt pa eksternt lojalitetskortnummer', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%4', 'Viser viduet for omregningsfaktor dersom varen har det.', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%5', 'Oppfrisk kunde', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%6<etikett id>[,varenr[,Spor om a repetere[,antall]]]', 'Skriver vareetikett', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `pos_tmpt_commands` VALUES (NULL, '%%%7', 'Velg lager pa aktiv ordrelinje', 'no', CURRENT_TIMESTAMP, NULL, NULL, '1');

INSERT INTO `printer_type` VALUES (NULL, 'Reciept printer', 'Reciept printer', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `printer_type` VALUES (NULL, 'Pick List Printer', 'Pick List Printer', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `printer_type` VALUES (NULL, 'Standard Printer', 'Standard Printer', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `printer_type` VALUES (NULL, 'Additional Printer', 'Additional Printer', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `printer_type` VALUES (NULL, 'Label Printer', 'Label Printer', CURRENT_TIMESTAMP, NULL, NULL, '1');
INSERT INTO `printer_type` VALUES (NULL, 'Invoice Printer', 'Invoice Printer', CURRENT_TIMESTAMP, NULL, NULL, '1');

INSERT INTO `tables` VALUES (NULL, '<div id="section-widget" style="width: 150px; height: 150px; background-color: rgb(204, 204, 204); position: relative; z-index: 1; left: 36px; top: 34px;" class="image-widget bordered-ui item-1 imgSize-1 ui-draggable ui-draggable-handle ui-resizable" data-itam-id="1"> <p class="text-center" style="font-size: 16px;padding-top: 10px;">Level 01</p><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div></div><div id="section-widget" style="width: 362px; height: 156px; background-color: rgb(204, 204, 204); position: relative; z-index: 10; left: 208px; top: -85px;" class="image-widget bordered-ui item-2 imgSize-2 ui-draggable ui-draggable-handle ui-resizable" data-itam-id="2"> <p class="text-center" style="font-size: 16px;padding-top: 10px;">Level 02</p><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div></div><div class="ui-draggable-handle item-3 imgSize-3 ui-resizable text-center ui-draggable" style="width: 92px; height: 43px; z-index: 16; left: 62px; top: -234px;" data-edit="false" data-item-id="3"> <img src="http://192.168.1.101:8888/easydrift/assets/images/table.svg"> <p style="margin: 0px"><b>iD:</b> 3<br> <b>Seats:</b> <span>4</span></p> <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div></div><div class="ui-draggable-handle item-4 imgSize-4 ui-resizable text-center ui-draggable" style="width: 92px; height: 43px; z-index: 13; left: 232px; top: -244px;" data-edit="false" data-item-id="4"> <img src="http://192.168.1.101:8888/easydrift/assets/images/table.svg"> <p style="margin: 0px"><b>iD:</b> 4<br> <b>Seats:</b> <span>5</span></p> <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div></div><div class="ui-draggable-handle item-5 imgSize-5 ui-resizable text-center ui-draggable" style="width: 92px; height: 43px; z-index: 14; left: 341px; top: -288px;" data-edit="false" data-item-id="5"> <img src="http://192.168.1.101:8888/easydrift/assets/images/table.svg"> <p style="margin: 0px"><b>iD:</b> 5<br> <b>Seats:</b> <span>5</span></p> <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div></div><div class="ui-draggable-handle item-6 imgSize-6 ui-resizable text-center ui-draggable" style="width: 92px; height: 43px; z-index: 15; left: 457px; top: -329px;" data-edit="false" data-item-id="6"> <img src="http://192.168.1.101:8888/easydrift/assets/images/table.svg"> <p style="margin: 0px"><b>iD:</b> 6<br> <b>Seats:</b> <span>5</span></p> <div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90;"></div></div>', CURRENT_TIMESTAMP, NULL, NULL, '1')

INSERT INTO `product` VALUES (NULL, '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'Default', '01', NULL, NULL, '121.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', NULL, NULL, '0', '1', '1', '0', '0', '1', CURRENT_TIMESTAMP, NULL, NULL, '1', '1', '1', NULL);
INSERT INTO `product` VALUES (NULL, '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'test', '02', NULL, NULL, '122.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', NULL, NULL, '0', '1', '1', '0', '0', '1', CURRENT_TIMESTAMP, NULL, NULL, '1', '1', '1', NULL);
INSERT INTO `product` VALUES (NULL, '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'Default', '03', NULL, NULL, '123.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', NULL, NULL, '0', '1', '1', '0', '0', '1', CURRENT_TIMESTAMP, NULL, NULL, '1', '1', '1', NULL);
INSERT INTO `product` VALUES (NULL, '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'Big Mac', '04', NULL, NULL, '124.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', NULL, NULL, '0', '1', '1', '0', '0', '1', CURRENT_TIMESTAMP, NULL, NULL, '1', '1', '1', NULL);
INSERT INTO `product` VALUES (NULL, '1', '1', '1', '1', '1', '1', '1', NULL, NULL, 'Coca-Cola Lata', '05', NULL, NULL, '125.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', '0', NULL, NULL, '0', '1', '1', '0', '0', '1', CURRENT_TIMESTAMP, NULL, NULL, '1', '1', '1', NULL);