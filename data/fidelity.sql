-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24-Nov-2017 às 15:36
-- Versão do servidor: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `standard`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `fidelity`
--

CREATE TABLE `fidelity` (
  `idfidelity` int(11) NOT NULL,
  `product_idproduct` int(11) DEFAULT NULL,
  `type_fidelity` int(11) NOT NULL COMMENT 'indicate the type fidelity: 1-By product; 2-By purchase value',
  `value` decimal(11,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `awards_type` int(11) NOT NULL COMMENT 'indicate the type awards: 1-new product; 2-Discount',
  `date_create` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1' COMMENT 'Indicates the status.:1-Active2-Blocked3-Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fidelity`
--
ALTER TABLE `fidelity`
  ADD PRIMARY KEY (`idfidelity`),
  ADD KEY `fk_fidelity_product1_idx` (`product_idproduct`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fidelity`
--
ALTER TABLE `fidelity`
  MODIFY `idfidelity` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `fidelity`
--
ALTER TABLE `fidelity`
  ADD CONSTRAINT `fk_fidelity_product1_idx` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
