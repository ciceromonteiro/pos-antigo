<?php
    $nav = "close_of_the_day";
    include 'nav.php';

    $company = getEm()->getRepository('CompanyInfo')->findAll();
    if(!empty($company)){
        foreach ($company as $value) {
            if($value->getDataType() == "name"){
                $company_name = $value->getDataValue();
            } else if($value->getDataType() == "address_street"){
                $company_address = $value->getDataValue();
            } else if($value->getDataType() == "phone_company"){
                $company_phone = $value->getDataValue();
            }        
        }
    } else {
        $company_name = "";
        $company_address = "";
        $company_phone = "";
    }
        
    $companyImg = getEm()->getRepository('CompanyInfoImg')->findOneBy(array('active' => 1));
    if(!empty($companyImg)){
        $company_image = $companyImg->getImage();
    } else {
        $company_image = "";
    }
?>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-4">
                <label for="id_report" class="translate">Initial date:</label>
                <input type="text" name="id_report" class="form-control">
            </div>
            
            <div class="col-md-4">
                <label for="id_report" class="translate">Close box:</label>
                <button class="btn btn-primary translate" id="button_pos_closure">Close box</button>
            </div>
        </div>        
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <table id="wifi" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Cash Closing Date</th>
                        <th class="translate">Name of official</th>
                        <th class="translate">Total amount received</th>
                        <th class="translate">Total value posted</th>
                        <th class="translate">Amount received in CC</th>
                        <th class="translate">Difference</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a href="#" class="translate">Print details</a></li> 
                    <li><a href="#" class="translate">Print Orders</a></li> 
                    <li><a href="#" class="translate">Reprint Coupons</a></li> 
                    <li><a href="#" class="translate">Tips report</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>

<div class="modal fade" id="pos_closure" tabindex="-1" role="dialog"><!--  -->
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Close box</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form_pos_closure" action="" method="post">
                            <input type="hidden" name="pos_closure" value="1">
                            <label class="translate">amount in cash:</label>
                            <input type="text" name="amount_counted" id="amount_counted" value="0"> 
                            <input type="button" id="confirm_closure" value="Ok"> 

                            <table id="table_payment_methods">
                                <thead>
                                <tr>
                                    <th class="translate">method of payment</th>
                                    <th class="translate">amount</th>
                                </tr>
                                </thead>
                            </table>
                        </form>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="print" style="display: none;"></div>
<script type="text/javascript">
    $(function(){
        $("#button_pos_closure").click(function(){
            $("#pos_closure").modal({show:true});
        });
    });
    $(function(){
        $("#confirm_closure").click(function(){
            amount_counted = $("#amount_counted").val();
            var baseUrl = '<?php echo URL_BASE ?>Sales/CashierClosing/closePos/'+amount_counted;

            $.ajax({

                url: baseUrl,
                method: "POST",
                async: false,

            }).done(function (json) {
                json = JSON.parse(json);
                var elements_for_pdf = JSON.parse(json.data);
                var payments_total = 0;
                payments = "";

                amount_received = elements_for_pdf.amount[1].split("__");
                amount_received = amount_received[1];

                //var payments = elements_for_pdf.payment_method[0]+": "+elements_for_pdf.amount[0];
                for(i=0;i<elements_for_pdf.amount.length;i++){
                    splitarray = elements_for_pdf.amount[i];

                    splitarray = splitarray.split("__");
                    payment_mtd_name = splitarray[0];
                    payment_mtd_value = parseInt(splitarray[1]); 
                    if(i>0){
                        payments_total += payment_mtd_value;
                    }
                    payments += "<br/>"+payment_mtd_name+": "+payment_mtd_value;
                }
                
                var diff = parseInt(amount_counted) - parseInt(amount_received);

                additional_text = "";
                if(elements_for_pdf.text_additional){
                    additional_text = "<div>"+elements_for_pdf.text_additional+"</div>";
                }
                products_sold = "";
                Sales_by_tax_gp = "";

                if(elements_for_pdf.products_sold['name']){
                    for(i=0;i<elements_for_pdf.products_sold['name'].length;i++){
                        products_sold += "<tr><td>"+elements_for_pdf.products_sold['name'][i];
                        if(elements_for_pdf.products_sold['qty']){
                            products_sold += "</td><td>"+elements_for_pdf.products_sold['qty'][i];
                        }
                        products_sold += "</td></tr>";
                    }
                }
                if(products_sold!=""){
                    products_sold = "<div>Produtos vendidos:</div><table>"+products_sold+"</table>";
                }

                if(elements_for_pdf.Sales_by_tax_group[0]){
                    for(i=0;i<elements_for_pdf.Sales_by_tax_group['name'].length;i++){
                        Sales_by_tax_gp += "<tr><td>"+elements_for_pdf.Sales_by_tax_group['name'][i];
                        Sales_by_tax_gp += "</td><td>"+elements_for_pdf.Sales_by_tax_group['amount'][i];
                        
                        Sales_by_tax_gp += "</td></tr>";
                    }
                }
                if(Sales_by_tax_gp!=""){
                    Sales_by_tax_gp = "<div>Produtos vendidos:</div><table>"+products_sold+"</table>";
                }
                //alert(additional_text);

                $("#print").append("<div id='pdf_posclosure'><div><img src='<?= $company_image ?>'></div><div><?= $company_name ?></div><div><?= $company_address ?></div><div><?= $company_phone ?></div><div><?= date('H:m:s d/m/Y') ?></div><div>Fechamento</div><div>"+payments+"</div><div>Total: "+(payments_total+diff)+"</div><div>Diferença: "+diff+"</div><div>"+products_sold+"</div>"+additional_text+"<div>Comentários:</div><div>Assinatura</div></div>");
                printJS('pdf_posclosure', 'html');

                reload_table(table_wifi);
                $("#pos_closure").modal('hide');

            }).fail(function (jqXHR, textStatus) {

                alert("Request failed: " + textStatus);

            });
        });
    });

    var table_payment_methods = $('#table_payment_methods').DataTable( {
        "ajax": {"url": "<?php echo URL_BASE ?>Sales/CashierClosing/getPaymentMtdAmounts/"},
        /*"dom": 'frtip',*/
        "columns": [
            { "data": "name" },
            { "data": "amount" }
        ],
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [-1] }
        ],
        "ordering": false,
        "info":     false,
        "language": {
            "zeroRecords": "Nothing found - sorry",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
    });

    var table_wifi = $('#wifi').DataTable( {
        "ajax": {"url": "<?php echo URL_BASE ?>Sales/CashierClosing/getPosClosures/"},
        /*"dom": 'frtip',*/
        "columns": [
            { "data": "checkbox" },
            { "data": "id" },
            { "data": "date" },
            { "data": "user" },
            { "data": "amount_received" },
            { "data": "amount_counted" },
            { "data": "amount_credit_card" },
            { "data": "difference" }
        ],
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [-1] }
        ],
        "ordering": false,
        "info":     false,
        "language": {
            "zeroRecords": "Nothing found - sorry",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
    });
</script>