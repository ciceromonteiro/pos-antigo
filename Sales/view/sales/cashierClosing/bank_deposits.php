<?php 

$select_option = "<option value='0'>Select Option</option>";
foreach ($array_answer['users'] as $value) {
    $select_option .= "<option value='".$value->getIdusers()."'>".$value->getPersonperson()->getName()."</option>";
}
?>
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#bankDeposit').DataTable( {
            "ajax": {"url": my_url+"Sales/CashierClosing/getAllBankDeposit/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "date" },
                { "data": "employee" },
                { "data": "totalDeposit" },
                { "data": "numeroDeposit" },
                { "data": "groupDeposit" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_bank_deposit');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_bank_deposit_edit');
            $.ajax({
                url : my_url+"Sales/cashierClosing/getBankDeposit",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#myModal_edit').modal({show : true});
                        $('#form_bank_deposit_edit #idDeposit').val(data.data[0].id);
                        $('#form_bank_deposit_edit #dateDeposit').val(data.data[0].date);
                        $('#form_bank_deposit_edit #depositAmount').val(data.data[0].totalDeposit);
                        $('#form_bank_deposit_edit #depositNumber').val(data.data[0].numeroDeposit);
                        $('#form_bank_deposit_edit #employee').val(data.data[0].employee);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_bank_deposit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Sales/cashierClosing/bankDepositsInsert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_bank_deposit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_bank_deposit_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Sales/CashierClosing/updateBankDeposit",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_bank_deposit_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal_edit').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idDeposits : ids};
            deleteItens('Sales/cashierClosing/deleteBankDeposit', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#bankDeposit tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#bankDeposit');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<?php
    $nav = "bank_deposits";
    include 'nav.php';
?>

<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">        
        <div class="col-md-12">
            <table id="bankDeposit" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('bankDeposit')"></th>
                    <th style="width: 80px" class="translate">ID</th>
                    <th class="translate">Date</th>
                    <th class="translate">Employee</th>
                    <th class="translate">Total Deposit</th>
                    <th class="translate">Number Deposit</th>
                    <th class="translate">Group Deposit</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Deposit</h4>
            </div>
            <form id="form_bank_deposit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="dateDeposit" class="label-style">Date</label>
                            <div class="input-group date">
                                <input type="text" class="form-control date-input" name="dateDeposit" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="depositAmount" class="label-style">Deposit amount:</label>
                            <input type="text" class="form-control money" id="depositAmount" name="depositAmount" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="depositNumber" class="label-style">Deposit number:</label>
                            <input type="text" class="form-control number" id="depositNumber" name="depositNumber" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="selectEmployee" class="label-style">Select Employee:</label>
                            <select name="employee">
                                <?php echo $select_option ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="cashValue" class="label-style">Cash value</label>
                            <input type="text" class="form-control money" id="cashValue" name="cashValue" required="true" disabled="true">
                        </div>
                        <div class="col-md-4">
                            <label for="initialValueDesired" class="label-style">Initial value desired:</label>
                            <input type="text" class="form-control money" id="initialValueDesired" name="initialValueDesired" required="true" disabled="true">
                        </div>
                        <div class="col-md-4">
                            <label for="SuggestedDeposit" class="label-style">Suggested deposit amount:</label>
                            <input type="text" class="form-control money" id="SuggestedDeposit" name="SuggestedDeposit" required="true" disabled="true">
                        </div>
                        <div class="col-md-4">
                            <label for="totalValueAfterDeposit" class="label-style">Total value after deposit:</label>
                            <input type="text" class="form-control money" id="totalValueAfterDeposit" name="totalValueAfterDeposit" required="true" disabled="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="myModal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Deposit</h4>
            </div>
            <form id="form_bank_deposit_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="dateDeposit" class="label-style">Date</label>
                            <div class="input-group date">
                                <input type="number" class="form-control date-input" name="idDeposit" id="idDeposit" required="true" style="display: none;">
                                <input type="text" class="form-control date-input" name="dateDeposit" id="dateDeposit" required="true">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-th"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label for="depositAmount" class="label-style">Deposit amount:</label>
                            <input type="text" class="form-control money" id="depositAmount" name="depositAmount" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="depositNumber" class="label-style">Deposit number:</label>
                            <input type="text" class="form-control number" id="depositNumber" name="depositNumber" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="selectEmployee" class="label-style">Select Employee:</label>
                            <select name="employee" id="employee">
                                <?php echo $select_option ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="cashValue" class="label-style">Cash value</label>
                            <input type="text" class="form-control money" id="cashValue" name="cashValue" required="true" disabled="true">
                        </div>
                        <div class="col-md-4">
                            <label for="initialValueDesired" class="label-style">Initial value desired:</label>
                            <input type="text" class="form-control money" id="initialValueDesired" name="initialValueDesired" required="true" disabled="true">
                        </div>
                        <div class="col-md-4">
                            <label for="SuggestedDeposit" class="label-style">Suggested deposit amount:</label>
                            <input type="text" class="form-control money" id="SuggestedDeposit" name="SuggestedDeposit" required="true" disabled="true">
                        </div>
                        <div class="col-md-4">
                            <label for="totalValueAfterDeposit" class="label-style">Total value after deposit:</label>
                            <input type="text" class="form-control money" id="totalValueAfterDeposit" name="totalValueAfterDeposit" required="true" disabled="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
