<div id="navbar-three">
    <ul class="navbar-three">
        <li <?php echo($nav == "close_of_the_day") ? ' class="active"' : "" ?>>
        	<a href="<?php echo URL_BASE."Sales/cashierClosing/index"; ?>" class="translate">Close of the day</a>
        </li>
        <li <?php echo($nav == "bank_deposits") ? ' class="active"' : "" ?>>
        	<a href="<?php echo URL_BASE."Sales/cashierClosing/bank_deposits"; ?>" class="translate">Bank Deposits</a>
        </li>
    </ul>
</div>