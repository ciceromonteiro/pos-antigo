<?php
    $nav = "amount_of_money";
    include 'nav.php';
?>

<div class="content">
    <div class="row line-form" style="margin-bottom: 60px;">
        <div class="col-md-2">
            <label for="id_report" class="translate">Value in box:</label>
            <input type="text" name="id_report" class="form-control">
        </div>
        <div class="col-md-2">
            <label for="id_report" class="translate">Value initial wanted:</label>
            <input type="text" name="id_report" class="form-control">
        </div>
        <div class="col-md-2">
            <label for="id_report" class="translate">Value suggested of the deposit:</label>
            <input type="text" name="id_report" class="form-control">
        </div>
        <div class="col-md-2">
            <label for="id_report" class="translate">Value of the deposit:</label>
            <input type="text" name="id_report" class="form-control">
        </div>
        <div class="col-md-2">
            <label for="id_report" class="translate">Value total after deposit:</label>
            <input type="text" name="id_report" class="form-control">
        </div>
        <div class="col-md-2">
            <label for="id_report" class="translate">Number of deposit:</label>
            <input type="text" name="id_report" class="form-control">
        </div>
        <div class="col-md-2">
            <label for="id_report" class="translate">Select employee:</label>
            <input type="text" name="id_report" class="form-control">
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left translate">Select employee</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>