<?php 
    $nav = "resume";
    include "nav.php";
    extract($array_answer);
 //    $payments = $array_answer['payments'];
	// $clientName = $array_answer['clientName'];
	// $clientNumber = $array_answer['clientNumber'];
	// $products = $array_answer['products'];
	// $orderId = $array_answer['orderId'];
	// $clientAddres = $array_answer['clientAddres'];
	// $clientAddresNumber = $array_answer['clientAddresNumber'];
	// $clientPhone = $array_answer['clientPhone'];
?>
<script type="text/javascript">
function printNota(order_id) {
    var src = my_url+'Sales/resume/printNota/'+order_id;
    console.log(src);
    $.LoadingOverlay('show', {
        text: 'Imprimindo nota fiscal...',
        textResizeFactor: 0.2
    });
    var print_nota = document.getElementById('print-nota');
    print_nota.addEventListener("load", function() {
        window.frames["print-nota"].focus();
        window.frames["print-nota"].print();
        $.LoadingOverlay('hide');
    });
    print_nota.src = src;

}

function returnItem() {
    var order_line_id = $('input[type=radio]:checked').val();
    console.log(order_line_id);

    var url = my_url + 'Sales/resume/returnProduct';
    var data = {
        order_line_id: order_line_id
    };
    var posting = $.post(url, data);

    posting.done(function(data) {
        //
    });
}

$(function(){
    console.log('Version: 1.30.9');
    $('tr.item_row').click(function(){
        console.log('click row');
        $(this).find('td input:radio').prop('checked', true);
        $('#return_button').prop('disabled', false);
    });
    $('.return_item').click(function(e){
        console.log(e.checked);
    });
});
</script>

<style type="text/css">
    
.pointer{
    cursor: pointer;
}

</style>

<div class="content">
    <div class="row">
        <div class="col-md-12">
        	<h1>Detalhes da Compra #<?=$orderId?></h1>
        	<div class="col-md-6">
        		<h3>Produtos</h3>
                <?php if($products):?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                            	<th scope="col">ID</th>
                                <th scope="col">Codigo de Barras</th>
                                <th scope="col">Produto</th>
                                <th scope="col">QTD</th>
                                <th scope="col">Preço</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($products as $product):?>
                                <tr class="item_row">
                                    <td><input type="radio" name="return_item" class="return_item" value="<?=$product['lineId']?>"></td>
                                	<td><?=$product['lineId']?></td>
                                    <td><?=$product['barcode']?></td>
                                    <td><?=$product['name']?></td>
                                    <td><?=$product['QTD']?></td>
                                    <td>R$ <?=$product['lineValue']?></td>
                                </tr>
                            <?php endforeach?>
                        </tbody>
                    </table>
                    <button type="button" onclick="printNota('<?=$orderId?>');" class="btn btn-success">Segunda via da NFCe</button>
                    <button type="button" id="return_button" onclick="returnItem();" class="btn btn-success" disabled="disabled">Devolução de Produto</button>
                <?php else:?>
                    <div class="alert alert-info">Nenhum Resultado encontrado</div>
                <?php endif?>
        	</div>
        	<div class="col-md-6">
	    		<h3>Pagamentos</h3>
                <?php if($payments):?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            	<th scope="col">Método</th>
                                <th scope="col">Data</th>
                                <th scope="col">Valor</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($payments as $payment):?>
                                <tr>
                                	<td><?=$payment['name']?></td>
                                    <td><?=$payment['date']->format('d/m/Y')?></td>
                                    <td><?= format_money($payment['value'])?></td>
                                </tr>
                            <?php endforeach?>
                        </tbody>
                    </table>
                <?php else:?>
                    <div class="alert alert-info">Nenhum Resultado encontrado</div>
                <?php endif?>
        	</div>

        	<div class="col-md-6">
                <div class="view">
					<address contenteditable="false">
						<strong>Dados dos Cliente</strong><br>
						Nome: <?=$clientName?> <br>
						CFP: <?=$clientNumber?> <br>
						Endereço: <?=$clientAddres?>, <?=$clientAddresNumber?> <br>
						<abbr title="Phone">P:</abbr> <?=$clientPhone?>
					</address>
				</div>
        	</div>
        </div>
    </div>
</div>
<iframe src="" id="print-nota" name="print-nota" style="position: absolute; left: -800px; top: -800px;"></iframe>
<br>
