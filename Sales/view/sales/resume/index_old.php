<?php 
    $nav = "resume";
    include "nav.php";
?>
<script type="text/javascript">
var table;
var tableDelivery;
var tableSales;
    $(document).ready(function () {
        tableSales = $('#salesResume').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getAllSalesResume/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "dataPucharse"},
                {"data": "shippingCompany"},
                {"data": "customerAddress"},
                {"data": "liquidValue"},
                {"data": "grossValue"},
                {"data": "info"},
                {"data": "delivery"},
                {"data": "codeTracking"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
        $.ajax({
                url: my_url + "Sales/resume/getSituationFinancialTotalText",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#open').append(data.data.open+"%");
                        $('#notConfirmedPayment').append(data.data.waitingPayment+"%");
                        $('#paid').append(data.data.paid+"%");
                        $('#shipped').append(data.data.shipped+"%");
                        $('#finished').append(data.data.finished+"%");
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });

        $.ajax({
                url: my_url + "Sales/resume/getDeliveryTotalText",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#Total').append(data.data.total);
                        $('#codeTracking').append(data.data.deliveryTracking);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });

        $.ajax({
                url: my_url + "Sales/resume/getOpenTotalText",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#totalOrders').append(data.data.total);
                        $('#openOrders').append(data.data.deliveryTracking);
                    } else {
                        notification(false);
                        console.log(data.message);
          }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });

        table = $('#financialSituationTable').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getSituationFinancialTotalTable"},
            "columns": [
                {"data": "id"},
                {"data": "client"},
                {"data": "value"},
                {"data": "discount"},
                {"data": "valueShipping"},
                {"data": "dataCreate"},
                {"data": "status"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });

         

         tableDelivery = $('#deliveryTable').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getDeliveryTotalTable"},
            "columns": [
                {"data": "id"},
                {"data": "client"},
                {"data": "value"},
                {"data": "discount"},
                {"data": "valueShipping"},
                {"data": "dataCreate"},
                {"data": "status"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });

         tableOpen = $('#openTable').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getOpenTotalTable"},
            "columns": [
                {"data": "id"},
                {"data": "client"},
                {"data": "value"},
                {"data": "discount"},
                {"data": "valueShipping"},
                {"data": "dataCreate"},
                {"data": "status"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });

         


    });
    function modalFinancial(){
        $('#situationFinancialModal').modal({
                show: true
            });
    }
    function modalDelivery(){
        $('#deliveryStatusModal').modal({
                show: true
            });
    }

    function modalOpen(){
        $('#openStatusModal').modal({
                show: true
            });
    }

    function financialfilter(filter){
        table.destroy();
         table = $('#financialSituationTable').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getFinancialByFilter/" + filter},
            "columns": [
                {"data": "id"},
                {"data": "client"},
                {"data": "value"},
                {"data": "discount"},
                {"data": "valueShipping"},
                {"data": "dataCreate"},
                {"data": "status"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
       
         
    }

    function deliveryFilter(filter){
        tableDelivery.destroy();
         tableDelivery = $('#deliveryTable').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getDeliveryByFilter/" + filter},
            "columns": [
                {"data": "id"},
                {"data": "client"},
                {"data": "value"},
                {"data": "discount"},
                {"data": "valueShipping"},
                {"data": "dataCreate"},
                {"data": "status"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
       
         
    }

    function updateTableSales(){
        tableSales.destroy();
         tableSales = $('#salesResume').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getAllSalesResume/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "dataPucharse"},
                {"data": "shippingCompany"},
                {"data": "customerAddress"},
                {"data": "liquidValue"},
                {"data": "grossValue"},
                {"data": "info"},
                {"data": "delivery"},
                {"data": "codeTracking"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
    }



    function financialFunctions(){
        table.destroy();
        table = $('#financialSituationTable').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getSituationFinancialTotalTable"},
            "columns": [
                {"data": "id"},
                {"data": "client"},
                {"data": "value"},
                {"data": "discount"},
                {"data": "valueShipping"},
                {"data": "dataCreate"},
                {"data": "status"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
        modalFinancial();
        
    }

    function deliveryFunctions(){
        tableDelivery.destroy();
        tableDelivery = $('#deliveryTable').DataTable({
            "ajax": {"url": my_url + "Sales/resume/getDeliveryTotalTable"},
            "columns": [
                {"data": "id"},
                {"data": "client"},
                {"data": "value"},
                {"data": "discount"},
                {"data": "valueShipping"},
                {"data": "dataCreate"},
                {"data": "status"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
        modalDelivery();
    }

    function filter(){
      var opened = document.querySelector("#opened").checked;
      var completed = document.querySelector("#completed").checked;
      var productReturn = document.querySelector("#productReturn").checked;

      if(opened == true || completed == true || productReturn == true){

       tableSales.destroy();
         tableSales = $('#salesResume').DataTable({
            "ajax": {"url": my_url + "Sales/resume/filterSalesResume/"+opened+'/'+completed+'/'+productReturn},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "dataPucharse"},
                {"data": "shippingCompany"},
                {"data": "customerAddress"},
                {"data": "liquidValue"},
                {"data": "grossValue"},
                {"data": "info"},
                {"data": "delivery"},
                {"data": "codeTracking"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
      
     }else{
        updateTableSales();
     }

    }


</script>

<style type="text/css">
    
.pointer{
    cursor: pointer;
}

</style>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <ul class="list-inline">
                <li><p><input type="checkbox" id="opened" name="item1" onclick="filter()"> <t class="translate">Opened</t></p></li>
                <li><p><input type="checkbox" id="completed" name="item2" onclick="filter()"> <t class="translate">Completed</t></p></li>
                <li><p><input type="checkbox" id="productReturn" name="item3" onclick="filter()"> <t class="translate">Products returned</t></p></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="salesResume" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                    <th class="translate">ID</th>
                    <th class="translate">Date Purchased</th>
                    <th class="translate">Shipping company</th>
                    <th class="translate">Customer address</th>
                    <th class="translate">Liquid Value</th>
                    <th class="translate">Gross Value</th>
                    <th class="translate">Info</th>
                    <th class="translate">Delivery</th>
                    <th class="translate">Code Tracking</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <table id="color" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Product ID</th>
                    <th>Description</th>
                    <th>Amount</th>
                    <th>Liquid Value</th>
                    <th>Discount</th>
                    <th>Gross Value</th>
                    <th>Total</th>
                    <th>Situation</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
        
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button>
                <ul class="dropdown-menu"> 
                    <li><a class="pointer translate" onclick="financialFunctions()">Financial situation</a></li> 
                    <li><a class="pointer translate" onclick="deliveryFunctions()">Delivery Status</a></li>
                    <li><a class="pointer translate" onclick="modalOpen()">Open orders</a></li>
                    <li><a class="pointer translate" onclick="updateTableSales()">Update</a></li>
                </ul> 
            </div>
        </div>
       
    </div>
</div>


<div class="modal fade row row-fluid" id="situationFinancialModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Situation FInancial</h4>
            </div>
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="translate">ORDERS BY STATUS</h4>
                            <ul id="ulFinancial" style="list-style-type: square; margin-left: 15px;" >
                                <li>0 - <t class="translate">Open:</t> <b id="open"></b></li>
                                <li>1 - <t class="translate">Waiting payment confirmation:</t> <b id="notConfirmedPayment"></b></li>
                                <li>2 - <t class="translate">Paid, waiting shipment:</t> <b  id="paid"></b></li>
                                <li>3 - <t class="translate">Shipped, waiting arrival at customers house:</t> <b  id="shipped"></b></li>
                                <li>4 - <t class="translate">Finished:</t> <b  id="finished"></b></li>
                            </ul>
                    </div>
                 </div>
             </div>  
             <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4 class="translate">ORDERS IN TABLE</h4>  
                        <table id="financialSituationTable" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="translate">ID</th>
                                <th class="translate">Client</th>
                                <th class="translate">Value</th>
                                <th class="translate">Discount</th>
                                <th class="translate">Value Shipping</th>
                                <th class="translate">Date Created</th>
                                <th class="translate">status</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                </div>

                <div class="modal-footer">
                   <div class="pull-left">
                    <div class="btn-group dropup"> 
                        <button type="button" class="btn btn-primary translate">Filter</button> 
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                            <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                        </button> 
                        <ul class="dropdown-menu"> 
                            <li><a class="pointer translate" onclick="financialfilter(0)">Open</a></li> 
                            <li><a class="pointer translate" onclick="financialfilter(1)">Waiting payment confirmation</a></li>
                            <li><a class="pointer translate" onclick="financialfilter(2)">Paid, waiting shipment</a></li>
                            <li><a class="pointer translate" onclick="financialfilter(3)">Shipped, waiting arrival at customers house</a></li>
                            <li><a class="pointer translate" onclick="financialfilter(4)">Finished</a></li>
                        </ul> 
                    </div>
                </div>
                <div class="pull-right">
        </div>
                </div>

        </div>
    </div>
</div>

<div class="modal fade row row-fluid" id="deliveryStatusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Delivery Status</h4>
            </div>
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="translate">ORDERS BY CODE TRACKING</h4>
                            <ul style="list-style-type: square; margin-left: 15px;" >
                                <li><t class="translate">Total:</t> <b id="Total"></b></li>
                                <li><t class="translate">with Code Tracking:</t> <b id="codeTracking"></b></li>
                            </ul>
                    </div>
                 </div>
             </div>   
             <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4 class="translate">ORDERS IN TABLE</h4>  
                        <table id="deliveryTable" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="translate">ID</th>
                                <th class="translate">Client</th>
                                <th class="translate">Value</th>
                                <th class="translate">Discount</th>
                                <th class="translate">Value Shipping</th>
                                <th class="translate">Date Created</th>
                                <th class="translate">status</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                </div>

                <div class="modal-footer">
                   <div class="pull-left">
                    <div class="btn-group dropup"> 
                        <button type="button" class="btn btn-primary translate">Filter</button> 
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                            <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                        </button> 
                        <ul class="dropdown-menu"> 
                            <li><a class="pointer translate" onclick="deliveryFilter(0)">Open</a></li> 
                            <li><a class="pointer translate" onclick="deliveryFilter(1)">Waiting payment confirmation</a></li>
                            <li><a class="pointer translate" onclick="deliveryFilter(2)">Paid, waiting shipment</a></li>
                            <li><a class="pointer translate" onclick="deliveryFilter(3)">Shipped, waiting arrival at customers house</a></li>
                            <li><a class="pointer translate" onclick="deliveryFilter(4)">Finished</a></li>
                        </ul> 
                    </div>
                </div>
                </div>

        </div>
    </div>
</div>

<div class="modal fade row row-fluid" id="openStatusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Open Orders</h4>
            </div>
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="translate">AMOUNT</h4>
                            <ul style="list-style-type: square; margin-left: 15px;" >
                                <li><t class="translate">Total:</t> <b id="totalOrders"></b></li>
                                <li><t class="translate">open orders:</t> <b id="openOrders"></b></li>
                            </ul>
                    </div>
                 </div>
             </div>   
             <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4 class="translate">ORDERS IN TABLE</h4>  
                        <table id="openTable" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="translate">ID</th>
                                <th class="translate">Client</th>
                                <th class="translate">Value</th>
                                <th class="translate">Discount</th>
                                <th class="translate">Value Shipping</th>
                                <th class="translate">Date Created</th>
                                <th class="translate">status</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                </div>


        </div>
    </div>
</div>

<script>
    jQuery("#action-1").click(function(e){
        $("#selected").html($("#action-1").html());
        e.preventDefault();
    });
    
    jQuery("#action-2").click(function(e){
        $("#selected").html($("#action-2").html());
        e.preventDefault();
    });
    
    jQuery("#action-3").click(function(e){
        $("#selected").html($("#action-3").html());
        e.preventDefault();
    });
    
    jQuery("#action-4").click(function(e){
        $("#selected").html($("#action-4").html());
        e.preventDefault();
    });
    
    jQuery("#action-5").click(function(e){
        $("#selected").html($("#action-5").html());
        e.preventDefault();
    });
    
</script>
