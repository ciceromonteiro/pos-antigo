<?php
    $nav = "webshop";
    include 'nav.php';
?>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-2">
                <label for="data_start" class="translate">Initial date:</label>
                <input type="text" name="data_start" class="form-control">
            </div>
            <div class="col-md-2">
                <label for="data_end" class="translate">Final date:</label>
                <input type="text" name="data_end" class="form-control">
            </div>
            <div class="col-md-8">
                <div class="col-md-6">
                    <label></label>
                    <p><input type="checkbox" name="generete_pdf"> <t class="translate">Generate PDF only</t></p>
                    <p><input type="checkbox" name="send_reports"> <t class="translate">Submit report</t></p>
                </div>
                <div class="col-md-6">
                    <label></label>
                    <p><input type="checkbox" name="view_display"> <t class="translate">View on screen</t></p>
                    <p><input type="checkbox" name="print"> <t class="translate">Print</t></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="wifi" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Protocol</th>
                        <th class="translate">Closing date and time</th>
                        <th class="translate">Employee</th>
                        <th class="translate">Value</th>
                        <th class="translate">Due ID</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate">Payment options</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>