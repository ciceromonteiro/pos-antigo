<?php 
    $nav = "resume";
    include "nav.php";
?>
<script type="text/javascript">
var tableSales;
function saleDetails() {
    var data = tableSales.rows( { selected: true } ).data();
    if ((0 in data) == false) {
        return false;
    }
    var id = data[0][1];
    window.location = my_url+'Sales/resume/details/'+id;
    console.log({data});
}
$(document).ready(function () {
    tableSales = $('#salesResume').DataTable({
        searching: false,
        processing: true,
        serverSide: true,
        ajax: my_url + "Sales/resume/getSales",
        columnDefs: [ {
            targets: 0,
            data: null,
            defaultContent: '',
            orderable: false,
            className: 'select-checkbox'
        } ],
        select: {
            style: 'os',
            selector: 'td:first-child'
        }
    });

    $('#filter-results').click(function(){
        console.log('click filter 2');
        var params = $('#filter-form').serialize();
        console.log(params);
        tableSales.ajax.url( my_url + "Sales/resume/getSales?"+params ).load();
    });
});
</script>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-2 col-sm-2">
            <button type="button" onclick="saleDetails();" class="btn btn-primary"><span class="lnr lnr-menu-circle"></span> <t class="translate">Detalhes</t></button>
        </div>
        <div class="col-md-10">
            <form class="form form-inline" id="filter-form" method="get" action="">
                <div class="form-group">
                    <label>CPF:</label>
                    <input name="cpf" type="text" class="form-control" value="<?=@$_GET['cpf']?>">
                </div>
                <div class="form-group">
                    <label>Número da Nota:</label>
                    <input name="nota" type="text" class="form-control" value="<?=@$_GET['nota']?>">
                </div>
                <button type="button" id="filter-results" class="btn btn-primary">Filtrar</button>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="salesResume" class="table table-bordered" style="width: 100%;">
                <thead>
                <tr>
                    <th></th>
                    <th class="translate">ID</th>
                    <th class="translate">Número da Nota</th>
                    <th class="translate">Série</th>
                    <th class="translate">Date da Compra</th>
                    <th class="translate">Nome do Cliente</th>
                    <th class="translate">CPF</th>
                    <th class="translate">Valor Total</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <table id="color" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Product ID</th>
                    <th>Description</th>
                    <th>Amount</th>
                    <th>Liquid Value</th>
                    <th>Discount</th>
                    <th>Gross Value</th>
                    <th>Total</th>
                    <th>Situation</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
