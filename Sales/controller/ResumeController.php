<?php
require_once '../libraries/enotas-php-client-v2/src/eNotasGW.php';

use Doctrine\ORM\Query\ResultSetMapping;
use eNotasGW\Api\Exceptions as Exceptions;

/**
 * @author Claudio da Cruz Silva Junior>
 * @version 1.0.0
 * @abstract file created in date september 25, 2017
 */
class ResumeController {

    public function test(){
        $idEmpresa = '10E298AB-599D-4D31-B1F4-4398663B0400';
        eNotasGW::configure(array(
            'apiKey' => 'NjIwMmE0OTItOTdkMS00NGE0LWI1NTItNmU4YThmMzcwNDAw'
        ));
        $result = eNotasGW::$NFeProdutoApi->consultar($idEmpresa, $_REQUEST['id']);
        _dump($result);
    }

    public function returnProduct() {
        echo "v 1.5.1\n";
        if(!isset($_REQUEST['items']) || count($_REQUEST['items']) <= 0){
            echo 'Erro de POST incorreto.';
            die();
        }
        $items = [];
        $order_line = getEm()->getRepository('OrderLine')->find($_REQUEST['items'][0]);
        $order = $order_line->getOrderorder();
        $person = $order->getPersonperson();
        if($person != null){
            $cliente = [
                "indicadorContribuinteICMS" => "NaoContribuinte",
                "tipoPessoa" => "F",
                "nome" => $person->getName(),
                "cpfCnpj" => $person->getDocumentNumber(),
                "telefone" => "84999064096",
                "endereco" => array(
                    "uf" => "RN",
                    "cidade" => "Natal",
                    "logradouro" => "Rua Francisco Pignataro",
                    "numero" => "1942",
                    "complemento" => "Ap 201",
                    "bairro" => "Capim Macio",
                    "cep" => "59082070"
                )
            ];
        } else {
            $cliente = null;
        }
        $order_nota = getEm()->getRepository('OrderNota')->findBy(['orderIdorder'=>$order->getIdorder()]);
        foreach ($_REQUEST['items'] as $value) {
            $order_line = getEm()->getRepository('OrderLine')->find($value);
            $product = $order_line->getProductproduct();
            $size_color = $order_line->getSizecolorsizecolor();
            $size = $size_color->getSizesize();
            $color = $size_color->getColorcolor();

            $product_name = $product->getName();
            if ($size) {
                $product_name .= ' - '.$size->getName();
            }
            $items[] = [
                "cfop" => "1959",
                "codigo" => "".$product->getIdproduct(),
                "descricao" => $product_name,
                "ncm" => $product->getNcm(),
                "cest" => null,
                "extipi" => null,
                "quantidade" => $order_line->getQty(),
                "quantidadeTributavel" => $order_line->getQty(),
                "sku" => null,
                "unidadeMedida" => "UN",
                "unidadeMedidaTributavel" => "UN",
                "valorUnitario" => $order_line->getValue(),
                "valorTotal" => ($order_line->getQty() * $order_line->getValue()),
                "impostos" => array(
                    "percentualAproximadoTributos" => array(
                        "simplificado" => array(
                            "percentual" => 18.00,
                            "valor" => ($order_line->getQty() * $order_line->getValue()) * 0.18
                        )
                    ),
                    "icms" => array (
                        "origem" => 0, //0 - Nacional
                        "situacaoTributaria" => "400",
                        // "aliquota" => $product->getQtrib(),
                        "modalidadeBaseCalculo" => 0,
                        "baseCalculo" => 1
                    ),
                    "pis" => array(
                        "situacaoTributaria" => "08"
                    ),
                    "cofins" => array(  
                        "situacaoTributaria" => "08"
                    )
                )
            ];
        }

        eNotasGW::configure(array(
            'apiKey' => 'NjIwMmE0OTItOTdkMS00NGE0LWI1NTItNmU4YThmMzcwNDAw'
        ));

        $idEmpresa = '10E298AB-599D-4D31-B1F4-4398663B0400';

        $id = '400'.$order->getIdorder();

        $params = array(
            // identificador único da requisição de emissão de nota fiscal 
            // (normalmente será preenchido com o id único do registro no sistema de origem)
            "id" => str_pad($id, 8, '0', STR_PAD_LEFT),
            "ambienteEmissao" => "Producao", //'Producao' ou 'Homologacao'
            "naturezaOperacao" => "Devolução",
            "tipoOperacao" => "Entrada",
            "numero" => $order->getIdorder(),
            "serie" => "001",
            "nfeReferenciada" => array(
                array("chaveAcesso" => $order_nota[0]->getnotaKey())
            ),
            "finalidade" => "DevolucaoMercadoria",
            "consumidorFinal" => true,
            "indicadorPresencaConsumidor" => "OperacaoPresencial",
            "cliente" => $cliente,
            "enviarPorEmail" => false,
            "itens" => $items,
            "informacoesAdicionais" => "Documento emitido por ME ou EPP optante pelo Simples Nacional. Não gera direito a crédito fiscal de IPI."
        );
        header('Content-Type: application/json');
        // echo json_encode($params, JSON_PRETTY_PRINT);
        try
        {
            $refund = eNotasGW::$NFeProdutoApi->emitir($idEmpresa, $params);
            $result = eNotasGW::$NFeProdutoApi->consultar($idEmpresa, $params['id']);
            // _dump($refund);
            // _dump($result);

            // update order line
            $order_line->setReturned(true);
            $order_line->setDateReturn(new DateTime());
            getEm()->persist($order_line);
            getEm()->flush();

            // echo "Sucesso!";
            foreach ($_REQUEST['items'] as $value) {
                try {
                    $order_line_refund = new OrderLineReturn();
                    $order_line_refund->setIdOrderLineReturn($value);
                    getEm()->persist($order_line_refund);
                    getEm()->flush();
                    echo json_encode($result, JSON_PRETTY_PRINT);
                } catch (Exception $e) {
                    echo 'Erro interno: </br></br>';
                    echo 'Message: ' . $e->getMessage();
                    die();
                }
            }
        }
        catch(Exceptions\invalidApiKeyException $ex) {
            echo 'Erro de autenticação: </br></br>';
            echo $ex->getMessage();
        }
        catch(Exceptions\unauthorizedException $ex) {
            echo 'Acesso negado: </br></br>';
            echo $ex->getMessage();
        }
        catch(Exceptions\apiException $ex) {
            echo 'Erro de validação: </br></br>';
            echo $ex->getMessage();
        }
        catch(Exceptions\requestException $ex) {
            echo 'Erro na requisição web: </br></br>';

            echo 'Requested url: ' . $ex->requestedUrl;
            echo '</br>';
            echo 'Response Code: ' . $ex->getCode();
            echo '</br>';
            echo 'Message: ' . $ex->getMessage();
            echo '</br>';
            echo 'Response Body: ' . $ex->responseBody;
        }

    }
    public function printNota($order_id) {
        $nota = getEm()->getRepository('OrderNota')->findBy(['orderIdorder' => $order_id]);
        $pdf_content = file_get_contents($nota[0]->getnotaPdf());
        //Specify that the content has PDF Mime Type
        header("Content-Type: application/pdf");
        //Display it
        echo $pdf_content;
        return true;
    }
    /**
    * Get sales in a json format for datatables plugin
    *
    * @access public
    * @static
    * @param   
    * @return string Json data
    */
    public function getSales() {
        header('Content-type: application/json');
        $em = getEm();

        $rsm = new ResultSetMapping;

        $sSearch = $_GET['search']['value'];

        $result = [
            'draw' => $_GET['draw']
        ];

        $sql = "SELECT 
            SQL_CALC_FOUND_ROWS
            '',
            O.idorder,
            N.nota_number,
            N.nota_serie,
            O.date_create,
            P.name,
            P.document_number,
            O.total_amount
        FROM `order` O
        LEFT JOIN order_nota N ON (N.order_idorder = O.idorder)
        LEFT JOIN person P ON (P.idperson = O.person_idperson)
        WHERE O.idorder is not null";
        if (isset($_GET['cpf'])) {
            $sql .= " AND P.document_number LIKE '".$_GET['cpf']."%'";
        }
        if (isset($_GET['nota'])) {
            $sql .= " AND N.nota_number = '".$_GET['nota']."'";
        }
        $sql .= " LIMIT {$_GET['length']} OFFSET {$_GET['start']};";
        // die($sql);
        $stmt = getEm()->getConnection()->prepare($sql);
        $stmt->execute();

        if ($query_result = $stmt->fetchAll()){
            foreach ($query_result as $value) {
                $result['data'][] = array_values($value);
            }
        }

        $_sql = "SELECT FOUND_ROWS() AS 'total'";
        $total_rs = getEm()->getConnection()->prepare($_sql);
        $total_rs->execute();
        $total_result = $total_rs->fetch();

        $result['recordsTotal'] = $total_result['total'];
        $result['recordsFiltered'] = $total_result['total'];

        echo json_encode(utf8ize($result), JSON_PRETTY_PRINT);
        return;
    }   
    /**
     * Index call resume sales
     * 
     * @access public
     * @return void call preview
     */
    public function index() {
        $navbar = "Sales|Resume";
        $array_answer = array("");
        GenericController::template("Sales", "resume", "index", $navbar, $array_answer, 352);
    }

    public function details($orderId) {
        // getting data
        $order = getEm()->getRepository("Order")->findBy(['idorder' => $orderId]);
        $order_nota = getEm()->getRepository('OrderNota')->findBy(['orderIdorder'=> $orderId]);
        $client = $order[0]->getPersonperson();
        if ($client) {
           $client_address = getEm()->getRepository('Address')->findBy(['personperson' => $client->getIdperson()]);
           $phone = getEm()->getRepository("PersonData")->findBy(['personperson' => $client->getIdperson(), 'dataType'=>'phone']);
        }
        $payment_method = getEm()->getRepository("OrderPayment")->findBy(['orderorder' => $orderId]);

        // starting variables
        $clientName = "Cliente não Identificado";
        $clientNumber = "";
        $clientPhone = "";
        $payments = [];

        foreach ($payment_method as $payment) {
            $_data = [
                'name' => $payment->getPaymentMtdpaymentMtd()->getName(),
                'date' => $payment->getDateCreate(),
                'value' => $payment->getValue()
            ];
            array_push($payments, $_data);
        }
        if (@$client_address != null) {
            $clientAddres = $client_address[0]->getAddressStreet();
            $clientAddresNumber = $client_address[0]->getAddressNumber();
        }

        if (@$client != null) {
            $clientName = $client->getName();
            $clientNumber = $client->getDocumentNumber();
        }
        if (@$phone != null) {
            $clientPhone = $phone[0]->getDataValue();
        }

        $em = getEm();
        $qb =  $em->getRepository('OrderLine')
            ->createQueryBuilder('ol')
            ->andWhere('ol.orderorder = :orderId')
            ->select('p.name', 'p.barcode', 'ol.value as lineValue', 'ol.qty as QTD', 'ol.idorderLine as lineId')
            ->join('ol.orderorder', 'o')
            ->join('ol.productproduct', 'p')
            ->setParameter('orderId', $orderId)
            ->getQuery()
            ->getResult();
        $products = $qb;

        $navbar = "Sales|Resume";
        $array_answer = array(
            'payments' => @$payments,
            'clientName' => @$clientName,
            'clientNumber' => @$clientNumber,
            'products' => @$products,
            'orderId' => @$orderId,
            'clientAddres' => @$clientAddres,
            'clientAddresNumber' => @$clientAddresNumber,
            'clientPhone' => @$clientPhone,
            'order_nota' => $order_nota[0]
        );
        GenericController::template("Sales", "resume", "details", $navbar, $array_answer, 352);
    }

    public function webshop() {
        $navbar = "Sales|Resume";
        $array_answer = array("");
        GenericController::template("Sales", "resume", "webshop", $navbar, $array_answer, 353);
    }
    /**
     * Get all orders by status and active in text form
     * @return string json_data 
     */
    public function getSituationFinancialTotalText(){
        try{
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $open = 0;
        $waitingPayment = 0;
        $paid = 0;
        $shipped = 0;
        $finished = 0;
        $total = 0;

        if($Orders != null){
            foreach ($Orders as $order) {
                if($order->getStatus() == 0){
                    $open++;
                }elseif($order->getStatus() == 1){
                    $waitingPayment++;

                }elseif($order->getStatus() == 2){
                    $paid++;

                }elseif($order->getStatus() == 3){
                    $shipped++;
                }elseif ($order->getStatus() == 4) {
                    $finished++;
                }
                $total++;
            }
        }
        
        if($open != 0){
            $open = ($open * 100)/$total;
        }
        if($waitingPayment != 0){
            $waitingPayment = ($waitingPayment*100)/$total;
        }
        if($paid != 0){
            $paid = ($paid * 100)/$total;
        }
        if($shipped != 0){
            $shipped = ($shipped * 100)/$total;
        }
        if($finished != 0){
            $finished = ($finished* 100)/$total;
        }
        
        $data = array (
            "open" => $open,
            "waitingPayment" => $waitingPayment,
            "paid" => $paid,
            "shipped" => $shipped,
            "finished" => $finished
        );
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;

        
    }
    /**
     * Get all orders by active in table form
     * @return string json_data 
     */
    public function getSituationFinancialTotalTable(){
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
         $data = array ();
        $valueOrder = 0;
        $discountOrder = 0;
        $valueShipping = 0;
        try{
        if($Orders != null){
            foreach ($Orders as $order) {
            $valueOrder = 0;
            $discountOrder = 0;            
                if($OrderLine != null){
                    foreach ($OrderLine as $orderLine) {
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $valueOrder = $valueOrder + $orderLine->getActualValue();
                            $discountOrder = $discountOrder + $orderLine->getDiscount();
                        }
                    }
                }
                $discountOrder = $discountOrder + $order->getDiscount();
                $discountOrder = number_format($discountOrder, 2, ',', '.');
                $valueOrder = number_format($valueOrder, 2, ',', '.');
                $valueShipping = $order->getValueShipping();
                $valueShipping = number_format($valueShipping, 2, ',', '.');
                
                if($order->getPersonperson() != null){
                    $client = $order->getPersonperson()->getIdperson();
                }else{
                    $client = '';
                }

                $dat = array (
                    "id" => $order->getIdorder(),
                    "client" => $client,
                    "value" => $valueOrder,
                    "discount" => $discountOrder,
                    "valueShipping" => $valueShipping,
                    "dataCreate" => $order->getDateCreate(),
                    "status" => $order->getStatus()
                );
                array_push($data, $dat);
            }
            
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;

    }
    /**
     * Get all orders by status in table form with filter
     * @param int filter
     * @return string json_data 
     */
    public function getFinancialByFilter($filter){
         $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1, "status" => $filter));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
         $data = array ();
        $valueOrder = 0;
        $discountOrder = 0;
        $valueShipping = 0;
        try{
        if($Orders != null){
            foreach ($Orders as $order) {
            $valueOrder = 0;
            $discountOrder = 0;            
                if($OrderLine != null){
                    foreach ($OrderLine as $orderLine) {
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $valueOrder = $valueOrder + $orderLine->getActualValue();
                            $discountOrder = $discountOrder + $orderLine->getDiscount();
                        }
                    }
                }
                $discountOrder = $discountOrder + $order->getDiscount();
                $discountOrder = number_format($discountOrder, 2, ',', '.');
                $valueOrder = number_format($valueOrder, 2, ',', '.');
                $valueShipping = $order->getValueShipping();
                $valueShipping = number_format($valueShipping, 2, ',', '.');
                
                if($order->getPersonperson() != null){
                    $client = $order->getPersonperson()->getIdperson();
                }else{
                    $client = '';
                }

                $dat = array (
                    "id" => $order->getIdorder(),
                    "client" => $client,
                    "value" => $valueOrder,
                    "discount" => $discountOrder,
                    "valueShipping" => $valueShipping,
                    "dataCreate" => $order->getDateCreate(),
                    "status" => $order->getStatus()
                );
                array_push($data, $dat);
            }
            
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;

    }
    /**
     * Get all orders by  active
     * @return string json_data 
     */
    public function getAllSalesResume(){
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $ShippingMtd = getEm()->getRepository('ShippingMtd')->findBy(array("active" => 1));
        $Person = getEm()->getRepository('Person')->findBy(array("active" => 1));
        $Address = getEm()->getRepository('Address')->findBy(array("active" => 1));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
        $User = getEm()->getRepository('Users')->findBy(array("active" => 1));
        $data = array ();
        try{
        if($Orders != null){
            foreach ($Orders as $order) {
                $orderStatus = printOrderStatus($order->getStatus());
                $shippingCompany = "";
                if($order->getShippingMtdshippingMtd() != null){
                    if($ShippingMtd != null){
                        foreach ($ShippingMtd as $shipping) {
                            if($shipping->getIdshippingMtd() == $order->getShippingMtdshippingMtd()->getIdshippingMtd()){
                               $shippingCompany =  $shipping->getShippingCmpshippingCmp()->getName();
                            }
                        }
                    }
                }

                $userName = "Não identificado";
                if($order->getUsersusers() != null){
                    $userName = $order->getUsersusers()->getPersonperson()->getName();
                }

                $customerAddress = "";
                $customerName = "Não identificado";
                if($order->getPersonperson() != null){
                    if($Person != null){
                        foreach ($Person as $person) {
                            if($person->getIdperson() == $order->getPersonperson()->getIdperson()){
                                $customerName = $person->getName();
                                if($Address != null){
                                    foreach ($Address as $address) {
                                        if($address->getPersonperson()->getIdperson() == $person->getIdperson()){
                                            $customerAddress = $address->getAddressDistrict();
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
                $valueBrute = 0;
                $discountOrder = 0;            
                if($OrderLine != null){
                    foreach ($OrderLine as $orderLine) {
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $valueBrute = $valueBrute + $orderLine->getActualValue();
                            $discountOrder = $discountOrder + $orderLine->getDiscount();
                        }
                    }
                }
                //var_dump($customerAddress);
                $valueLiquid = $valueBrute - ($discountOrder + $order->getDiscount());
                $valueBrute = number_format($valueBrute, 2, ',', '.');
                $valueLiquid = number_format($valueLiquid, 2, ',', '.');
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$order->getIdorder().'" data-name="'.$order->getIdorder().'">',
                    "id" => $order->getIdorder(),
                    "dataPucharse" => $order->getDateCreate(),
                    "customerName" => $customerName,
                    "userName" => $userName,
                    "orderStatus" => $orderStatus,
                    "price" => format_money($order->getTotalAmount()),
                    "details" => '<a href="'.URL_BASE.'sales/resume/details/'.$order->getIdorder().'">Detalhes</a>',
                    
                );
                array_push($data, $dat);

            }
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;

    }
    /**
     * Get total of orders and orders with code tracking in text form
     * @return string json_data 
     */
    public function getDeliveryTotalText(){
        try{
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $deliveryTracking = 0;
        $total = 0;
        $data = array ();

        if($Orders != null){
            foreach ($Orders as $order) {
                if($order->getTrackingCode() != null){
                    $deliveryTracking++;
                }
                $total++;
            }
        }
        
        
             $data = array (
            "total" => $total,
            "deliveryTracking" => $deliveryTracking
        );
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get all orders by code tracking 
     * @return string json_data 
     */
     public function getDeliveryTotalTable(){
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
         $data = array ();
        $valueOrder = 0;
        $discountOrder = 0;
        $valueShipping = 0;
        try{
        if($Orders != null){
            foreach ($Orders as $order) {
            $valueOrder = 0;
            $discountOrder = 0;       
            if($order->getTrackingCode() != null){
                if($OrderLine != null){
                    foreach ($OrderLine as $orderLine) {
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $valueOrder = $valueOrder + $orderLine->getActualValue();
                            $discountOrder = $discountOrder + $orderLine->getDiscount();
                        }
                    }
                }
                $discountOrder = $discountOrder + $order->getDiscount();
                $discountOrder = number_format($discountOrder, 2, ',', '.');
                $valueOrder = number_format($valueOrder, 2, ',', '.');
                $valueShipping = $order->getValueShipping();
                $valueShipping = number_format($valueShipping, 2, ',', '.');
                
                if($order->getPersonperson() != null){
                    $client = $order->getPersonperson()->getIdperson();
                }else{
                    $client = '';
                }

                $dat = array (
                    "id" => $order->getIdorder(),
                    "client" => $client,
                    "value" => $valueOrder,
                    "discount" => $discountOrder,
                    "valueShipping" => $valueShipping,
                    "dataCreate" => $order->getDateCreate(),
                    "status" => $order->getStatus()
                );
                array_push($data, $dat);
            }
        }
            
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;

    }
    /**
     * Get all orders by code tracking and status with filter
     * @return string json_data 
     */
    public function getDeliveryByFilter($filter){
         $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1, "status" => $filter));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
         $data = array ();
        $valueOrder = 0;
        $discountOrder = 0;
        $valueShipping = 0;
        try{
        if($Orders != null){
            foreach ($Orders as $order) {
            $valueOrder = 0;
            $discountOrder = 0;   
            if($order->getTrackingCode() != null){         
                if($OrderLine != null){
                    foreach ($OrderLine as $orderLine) {
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $valueOrder = $valueOrder + $orderLine->getActualValue();
                            $discountOrder = $discountOrder + $orderLine->getDiscount();
                        }
                    }
                }
                $discountOrder = $discountOrder + $order->getDiscount();
                $discountOrder = number_format($discountOrder, 2, ',', '.');
                $valueOrder = number_format($valueOrder, 2, ',', '.');
                $valueShipping = $order->getValueShipping();
                $valueShipping = number_format($valueShipping, 2, ',', '.');
                
                if($order->getPersonperson() != null){
                    $client = $order->getPersonperson()->getIdperson();
                }else{
                    $client = '';
                }

                $dat = array (
                    "id" => $order->getIdorder(),
                    "client" => $client,
                    "value" => $valueOrder,
                    "discount" => $discountOrder,
                    "valueShipping" => $valueShipping,
                    "dataCreate" => $order->getDateCreate(),
                    "status" => $order->getStatus()
                );
                array_push($data, $dat);
            }
        }
            
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;

    }
    /**
     * Get the total of orders and orders with status in open in text form
     * @return string json_data 
     */
    public function getOpenTotalText(){
        try{
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $deliveryTracking = 0;
        $total = 0;
        $data = array ();

        if($Orders != null){
            foreach ($Orders as $order) {
                if($order->getStatus() == 0){
                    $deliveryTracking++;
                }
                $total++;
            }
        }
        
        
             $data = array (
            "total" => $total,
            "deliveryTracking" => $deliveryTracking
        );
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get all orders with status with status in open in table form
     * @return string json_data 
     */
    public function getOpenTotalTable(){
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
         $data = array ();
        $valueOrder = 0;
        $discountOrder = 0;
        $valueShipping = 0;
        try{
        if($Orders != null){
            foreach ($Orders as $order) {
            $valueOrder = 0;
            $discountOrder = 0;       
            if($order->getStatus() == 0){
                if($OrderLine != null){
                    foreach ($OrderLine as $orderLine) {
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $valueOrder = $valueOrder + $orderLine->getActualValue();
                            $discountOrder = $discountOrder + $orderLine->getDiscount();
                        }
                    }
                }
                $discountOrder = $discountOrder + $order->getDiscount();
                $discountOrder = number_format($discountOrder, 2, ',', '.');
                $valueOrder = number_format($valueOrder, 2, ',', '.');
                $valueShipping = $order->getValueShipping();
                $valueShipping = number_format($valueShipping, 2, ',', '.');
                
                if($order->getPersonperson() != null){
                    $client = $order->getPersonperson()->getIdperson();
                }else{
                    $client = '';
                }

                $dat = array (
                    "id" => $order->getIdorder(),
                    "client" => $client,
                    "value" => $valueOrder,
                    "discount" => $discountOrder,
                    "valueShipping" => $valueShipping,
                    "dataCreate" => $order->getDateCreate(),
                    "status" => $order->getStatus()
                );
                array_push($data, $dat);
            }
        }
            
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;

    }
    /**
     * Get all orders with status equal opened, completed or with productReturn
     * @return string json_data 
     */
 public function filterSalesResume($opened, $completed, $productReturn){

    $openedNumber = 5;
    $completedNumber = 5;
    $productReturnNumber = 5;
        if($opened == "true"){
            $openedNumber = 0;
        }
        if($completed == "true"){
            $completedNumber = 4;
        }
        $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $ShippingMtd = getEm()->getRepository('ShippingMtd')->findBy(array("active" => 1));
        $Person = getEm()->getRepository('Person')->findBy(array("active" => 1));
        $Address = getEm()->getRepository('Address')->findBy(array("active" => 1));
        $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
        $data = array ();
        try{
        if($Orders != null){
            foreach ($Orders as $order) {
                $shippingCompany = "";
                if($order->getStatus() == $openedNumber || $order->getStatus() == $completedNumber){


                if($order->getShippingMtdshippingMtd() != null){
                    if($ShippingMtd != null){
                        foreach ($ShippingMtd as $shipping) {
                            if($shipping->getIdshippingMtd() == $order->getShippingMtdshippingMtd()->getIdshippingMtd()){
                               $shippingCompany =  $shipping->getShippingCmpshippingCmp()->getName();
                            }
                        }
                    }
                }
                $customerAddress = "";
                if($order->getPersonperson() != null){
                    if($Person != null){
                        foreach ($Person as $person) {
                            if($person->getIdperson() == $order->getPersonperson()->getIdperson()){
                                if($Address != null){
                                    foreach ($Address as $address) {
                                        if($address->getPersonperson()->getIdperson() == $person->getIdperson()){
                                            $customerAddress = $address->getAddressDistrict();
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
                $valueBrute = 0;
                $discountOrder = 0;            
                if($OrderLine != null){
                    foreach ($OrderLine as $orderLine) {
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $valueBrute = $valueBrute + $orderLine->getActualValue();
                            $discountOrder = $discountOrder + $orderLine->getDiscount();
                        }
                    }
                }
                //var_dump($customerAddress);
                $valueLiquid = $valueBrute - ($discountOrder + $order->getDiscount());
                $valueBrute = number_format($valueBrute, 2, ',', '.');
                $valueLiquid = number_format($valueLiquid, 2, ',', '.');
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$order->getIdorder().'" data-name="'.$order->getIdorder().'">',
                    "id" => $order->getIdorder(),
                    "dataPucharse" => $order->getDateCreate(),
                    "shippingCompany" => $shippingCompany,
                    "customerAddress" => $customerAddress,
                    "liquidValue" => $valueLiquid,
                    "grossValue" => $valueBrute,
                    "info" => $order->getExtraInfo(),
                    "delivery" => $order->getStatus(),
                    "codeTracking" => $order->getTrackingCode()
                    
                );
                array_push($data, $dat);


                }
            }
        }
        if($productReturn == "true"){

            if($OrderLine != null){
                foreach ($OrderLine as $orderLine) {
                    
                        if($Orders != null){
                            foreach ($Orders as $order) {
                                if($order->getIdorder() == $orderLine->getOrderorder()->getIdorder()){
                                    if($orderLine->getReturned() != 0){

                                           if($order->getShippingMtdshippingMtd() != null){
                                                if($ShippingMtd != null){
                                                    foreach ($ShippingMtd as $shipping) {
                                                        if($shipping->getIdshippingMtd() == $order->getShippingMtdshippingMtd()->getIdshippingMtd()){
                                                           $shippingCompany =  $shipping->getShippingCmpshippingCmp()->getName();
                                                        }
                                                    }
                                                }
                                            }
                                            $customerAddress = "";
                                            if($order->getPersonperson() != null){
                                                if($Person != null){
                                                    foreach ($Person as $person) {
                                                        if($person->getIdperson() == $order->getPersonperson()->getIdperson()){
                                                            if($Address != null){
                                                                foreach ($Address as $address) {
                                                                    if($address->getPersonperson()->getIdperson() == $person->getIdperson()){
                                                                        $customerAddress = $address->getAddressDistrict();
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                    }
                                                }
                                            }
                                            $valueBrute = 0;
                                            $discountOrder = 0;            
                                            if($OrderLine != null){
                                                foreach ($OrderLine as $orderLine) {
                                                    if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                                                        $valueBrute = $valueBrute + $orderLine->getActualValue();
                                                        $discountOrder = $discountOrder + $orderLine->getDiscount();
                                                    }
                                                }
                                            }
                                            //var_dump($customerAddress);
                                            $valueLiquid = $valueBrute - ($discountOrder + $order->getDiscount());
                                            $valueBrute = number_format($valueBrute, 2, ',', '.');
                                            $valueLiquid = number_format($valueLiquid, 2, ',', '.');
                                            $dat = array (
                                                "checkbox" => '<input type="checkbox" data-id="'.$order->getIdorder().'" data-name="'.$order->getIdorder().'">',
                                                "id" => $order->getIdorder(),
                                                "dataPucharse" => $order->getDateCreate(),
                                                "shippingCompany" => $shippingCompany,
                                                "customerAddress" => $customerAddress,
                                                "liquidValue" => $valueLiquid,
                                                "grossValue" => $valueBrute,
                                                "info" => $order->getExtraInfo(),
                                                "delivery" => $order->getStatus(),
                                                "codeTracking" => $order->getTrackingCode()
                                                
                                            );
                                            if(empty($data)){
                                                array_push($data, $dat); 

                                            }else{
                                                foreach ($data as $dataTeste) {
                                                    if($dataTeste['id'] = $dat['id']){

                                                    }else{
                                                       
                                                    }
                                                }
                                                
                                            }
                                            
                                            

   
                                    }   
                            }//aqui foreach order
                            
                        }
                    }
                }
            }

            
        }
        $result = "success";
        $message = "query success";
        $mysqlData = $data;

        }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;

    }

}

?>