<?php 

    class CashierClosingController {
        public function index(){
            $navbar = "Sales|Cashier Closing";

            $array_answer = array ("");
            GenericController::template("Sales", "cashierClosing","close_of_the_day", $navbar, $array_answer, 189);
        }

        public function closePos($amount_counted){
            try {                      
                $begin = getEm()->getConnection()->beginTransaction(); 
                $pos = getEm()->getRepository('Pos')->findBy(array('idpos' => 1)); //test - mudar depois
                $user = getEm()->getRepository('Users')->findBy(array('idusers' => 1)); //test - mudar depois
                $payment_methods = getEm()->getRepository('PaymentMtd')->findAll(); //test - mudar depois

                //$payment_method[0] = "Total contado";
                $amount[0] = "Total contado__".$amount_counted;

                // foreach($payment_methods as $payment_mtd){
                //     $payment_method[$payment_mtd->getIdpaymentMtd()] = $payment_mtd->getName();
                //     $amount[$payment_mtd->getIdpaymentMtd()] = 0;
                // }

                $amount_money = 0;
                $amount_credit_cards = 0;


                $date_start = new DateTime();
                $date_start->setTime("00", "00", "00");
                $date_start->setDate(date("Y"),date("m"),date("d"));

                $date_end = new DateTime();
                $date_end->setTime("23", "59", "59");
                $date_end->setDate(date("Y"),date("m"),date("d"));

                if($payment_methods){
                    foreach($payment_methods as $pm){
                        $amount[$pm->getIdpaymentMtd()] = 0;
                    }
                }

                //settings
                $print_list_of_products = getEm()->getRepository('PosSettingsClosingParameters')->findBy(array('idpos' => $pos, 'dataType' => 'print_list_of_products_sold_on_the_day'));
                if($print_list_of_products){
                    if($print_list_of_products[0]->getDataValue()){
                        $print_list_of_products = $print_list_of_products[0]->getDataValue();
                    }else{
                        $print_list_of_products = 0;
                    }
                }
                $print_quantity_of_products = getEm()->getRepository('PosSettingsClosingParameters')->findBy(array('idpos' => $pos, 'dataType' => 'print_quantity_of_products_sold_in_the_day'));
                if($print_quantity_of_products){
                    if($print_quantity_of_products[0]->getDataValue()){
                        $print_quantity_of_products = $print_quantity_of_products[0]->getDataValue();
                    }else{
                        $print_quantity_of_products = 0;
                    }
                }
                $include_Sales_by_tax_group = getEm()->getRepository('PosSettingsClosingParameters')->findBy(array('idpos' => $pos, 'dataType' => 'include_Sales_by_tax_group'));
                if($include_Sales_by_tax_group){
                    $include_Sales_by_tax_group = $include_Sales_by_tax_group[0]->getDataValue();
                }

                $Sales_by_tax_group = array();
                if($tax_gps = getEm()->getRepository('Tribute')->findBy(array('active' => 1))){
                    foreach($tax_gps as $tax_gp){
                        $Sales_by_tax_group[$tax_gp->getIdtribute()] = 0;
                    }
                }

                $products_sold = array();

                $orders = getEm()->getRepository('Order')->findBy(array('active' => 1));
                if($orders){                 
                    foreach($orders as $order){
                        if($order->getDateCreate()>$date_start&&$order->getDateCreate()<$date_end){
                            $payments = getEm()->getRepository('OrderPayment')->findBy(array('orderorder' => $order));                          
                            if($payments){                             
                                foreach($payments as $payment){                                 
                                    $payment_mtd = $payment->getPaymentMtdpaymentMtd();
                                    $amount[$payment_mtd->getIdpaymentMtd()] += $payment->getValue();
                                }
                            }

                            if($print_list_of_products){
                                $order_lines = getEm()->getRepository('OrderLine')->findBy(array('active' => 1, 'orderorder' => $order));
                                if($order_lines){
                                    foreach($order_lines as $order_line){
                                        $prod = $order_line->getProductproduct();

   //var_dump($prod->getAppliedTax($order->getCustomer(),false,false));    

                                        $prod_id = $prod->getIdproduct();

                                        $products_sold[$prod_id]['name'] = $prod->getName();

                                        if($print_quantity_of_products){
                                            if(isset($products_sold[$prod_id]['qty'])){
                                                $products_sold[$prod_id]['qty'] += $order_line->getQty();
                                            }else{
                                                $products_sold[$prod_id]['qty'] = $order_line->getQty();
                                            }
                                        }
                                         
                                    }
                                }
                            }

                            if($include_Sales_by_tax_group){
                                $order_lines = getEm()->getRepository('OrderLine')->findBy(array('active' => 1, 'orderorder' => $order));
                                if($order_lines){
                                    foreach($order_lines as $order_line){
                                        $prod = $order_line->getProductproduct();
                                        $tribute = $prod->getTaxGp();
                                        if($tribute){
   //var_dump($prod->getAppliedTax($order->getCustomer(),false,false));    
                                            $Sales_by_tax_group[$tribute->getIdtribute()]['amount'] += $order_line->getValue();
                                            $Sales_by_tax_group[$tribute->getIdtribute()]['name'] += $tribute->getName();   
                                        }                                   
                                    }
                                }
                            }

                        }
                    }
                    $products_sold_aux = $products_sold; //just for order with a int crescent index
                    unset($products_sold);
                    $products_sold = array();
                    $p=0;
    
                    if($products_sold_aux){      
                        foreach($products_sold_aux as $psa){ //just for order with a int crescent index
                            $products_sold['name'][$p] = $psa['name'];
                            if($print_quantity_of_products){
                                $products_sold['qty'][$p] = $psa['qty'];
                            }
                            $p++;
                        }
                    }

                    $Sales_by_tax_group_aux = $Sales_by_tax_group; //just for order with a int crescent index
                    unset($Sales_by_tax_group);
                    $Sales_by_tax_group = array();
                    $s=0;

                    if($Sales_by_tax_group_aux){
                        foreach($Sales_by_tax_group_aux as $sbtga){ //just for order with a int crescent index
                            $Sales_by_tax_group['name'][$s] = $sbtga['name'];
                            if($include_Sales_by_tax_group){
                                $Sales_by_tax_group['amount'][$s] = $sbtga['amount'];
                            }
                            $s++;
                        }
                    }

                }
                if($payment_methods){
                    foreach($payment_methods as $pm){
                        $amount[$pm->getIdpaymentMtd()] = $pm->getName()."__".$amount[$pm->getIdpaymentMtd()];
                    }
                }

                $posClosure = new PosClosure();
                $posClosure->setAmountCounted(str_replace(',', '.', $amount_counted));
                $posClosure->setPospos($pos[0]); 
                $posClosure->setUsersusers($user[0]); 
                $posClosure->setProtocol(1); //campo sem uso
                $posClosure->setAmountReceived($amount_money);
                $posClosure->setAmountCreditCard($amount_credit_cards);
                $posClosure->setDate(new DateTime());
                $posClosure->setDateCreate(new DateTime());
                $posClosure->setActive(1);
                getEm()->persist($posClosure);
                getEm()->flush();


                //settings
                $text_additional = getEm()->getRepository('PosSettingsClosingParameters')->findBy(array('idpos' => $pos, 'dataType' => 'text_additional'));
                if($text_additional){
                    $text_additional = $text_additional[0]->getDataValue();
                }       
                


                $data = array (
                    "amount" => $amount,
                    "text_additional" => $text_additional,
                    "products_sold" => $products_sold,
                    "Sales_by_tax_group" => $Sales_by_tax_group
                );

                $result  = 'success';
                $message = 'query success';
                $data = json_encode($data);
                $commit = getEm()->getConnection()->commit();
            } catch (Exception $e) {
                $rollback = getEm()->getConnection()->rollback();
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
           

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data
            );

            $json_data = json_encode($data);
            print $json_data;

        }
               

        public function bank_deposits(){
            $navbar = "Sales|Cashier Closing";
            $users = getEm()->getRepository('Users')->findAll();
            $array_answer = array (
                "users" => $users
            );
            GenericController::template("Sales", "cashierClosing","bank_deposits", $navbar, $array_answer, 347);			
        }

        public function amount_of_money(){
            $navbar = "Sales|Cashier Closing";
            $array_answer = array ("");
            GenericController::template("Sales", "cashierClosing","amount_of_money", $navbar, $array_answer);				
        }
        
        public function getAllBankDeposit(){
            try{
                $bank_deposit = getEm()->getRepository('Deposit')->findBy(array("active" => 1));
                $data = array ();
                foreach ($bank_deposit as $value){
                    $dat = array (
                        "checkbox" => '<input type="checkbox" data-id="'.$value->getIddeposit().'">',
                        "id" => $value->getIddeposit(),
                        "date" => date_format($value->getDate(), 'm/d/Y'),
                        "employee" => $value->getUseruser()->getPersonperson()->getName(),
                        "totalDeposit" => $value->getAmount(),
                        "numeroDeposit" => $value->getDepositNumber(),
                        "groupDeposit" => ""
                    );
                    array_push($data, $dat);
                }
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );

            $json_data = json_encode($data);
            print $json_data;
        }
        
        public function bankDepositsInsert(){
            if(isset($_POST['depositAmount']) && isset($_POST['depositNumber']) 
                    && isset($_POST['employee']) && isset($_POST['dateDeposit'])){
                try {
                    $employee = getEm()->getRepository('Users')->findBy(array('idusers' => $_POST['employee']));
                    $amount = str_replace('.', '', $_POST['depositAmount']);
                    $depositNumber = str_replace('.', '', $_POST['depositNumber']);
                    
                    $bankDeposit = new Deposit();
                    $bankDeposit->setDepositNumber(str_replace(',', '.', $depositNumber));
                    $bankDeposit->setAmount(str_replace(',', '.', $amount));
                    $bankDeposit->setDate(new DateTime($_POST['dateDeposit']));
                    $bankDeposit->setDateCreate(new DateTime());
                    $bankDeposit->setActive(1);
                    $bankDeposit->setUseruser($employee[0]);
                    getEm()->persist($bankDeposit);
                    getEm()->flush();

                    $result  = 'success';
                    $message = 'query success';
                    $data = "";

                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }

        public function getBankDeposit(){
            if(isset($_POST['id'])){
            try{
                $bank_deposit = getEm()->getRepository('Deposit')->findOneBy(array("iddeposit" => $_POST['id']));
                $data = array ();
                    $dat = array (
                        "id" => $bank_deposit->getIddeposit(),
                        "date" => date_format($bank_deposit->getDate(), 'm/d/Y'),
                        "employee" => $bank_deposit->getUseruser()->getPersonperson()->getIdperson(),
                        "totalDeposit" => $bank_deposit->getAmount(),
                        "numeroDeposit" => $bank_deposit->getDepositNumber(),
                        "groupDeposit" => ""
                    );
                    array_push($data, $dat);
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
            }
        }

        public function updateBankDeposit(){
            if(isset($_POST['depositAmount']) && isset($_POST['depositNumber']) 
                    && isset($_POST['employee']) && isset($_POST['dateDeposit'])){
                try {
                    $employee = getEm()->getRepository('Users')->findBy(array('idusers' => $_POST['employee']));
                    $amount = str_replace('.', '', $_POST['depositAmount']);
                    $depositNumber = str_replace('.', '', $_POST['depositNumber']);
                    
                    $bankDeposit = getEm()->getRepository('Deposit')->findOneBy(array("iddeposit" => $_POST['idDeposit']));
                    $bankDeposit->setDepositNumber(str_replace(',', '.', $depositNumber));
                    $bankDeposit->setAmount(str_replace(',', '.', $amount));
                    $bankDeposit->setDate(new DateTime($_POST['dateDeposit']));
                    $bankDeposit->setDateUpdate(new DateTime());
                    $bankDeposit->setActive(1);
                    $bankDeposit->setUseruser($employee[0]);
                    getEm()->persist($bankDeposit);
                    getEm()->flush();

                    $result  = 'success';
                    $message = 'query success';
                    $data = "";

                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
            };

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }

    public function deleteBankDeposit(){

        if($_POST['idDeposits']){
            try{
                $ids = $_POST['idDeposits'];
                for($i=0; $i < count($ids); $i++){
                    $bankDeposit = getEm()->getRepository("Deposit")->findOneBy(array("iddeposit" => $ids[$i]));
                    $bankDeposit->setActive('3');
                    $bankDeposit->setDateDelete(new DateTime());
                    getEm()->persist($bankDeposit);
                    getEm()->flush();
                    //AuthenticationController::insertLog('delete', 'ProductUnit', $_POST['idUnit']);
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

        public function getPaymentMtdAmounts(){
            try{
                $begin = getEm()->getConnection()->beginTransaction();
                $navbar = "Sales|Cashier Closing";
                $payment_mtds = getEm()->getRepository('PaymentMtd')->findBy(array('active' => 1));

                $data = array ();
                foreach ($payment_mtds as $value){
                    $amount_with = getEm()->getRepository('OrderPayment')->findBy(array('active' => 1, 'paymentMtdpaymentMtd' => $value));

                    $amount = 0;    

                    if($amount_with){
                        $date_start = new DateTime();
                        $date_start->setTime("00", "00", "00");
                        $date_start->setDate(date("Y"),date("m"),date("d"));

                        $date_end = new DateTime();
                        $date_end->setTime("23", "59", "59");
                        $date_end->setDate(date("Y"),date("m"),date("d"));

                        foreach($amount_with as $aw){
                            if($aw->getDateCreate()>$date_start&&$aw->getDateCreate()<$date_end){
                                $amount += $aw->getValue();
                            }
                        }    
                    }

                    $dat = array (
                        "name" => $value->getName(),
                        "amount" => $amount
                    );
                    array_push($data, $dat);
                }               

                $commit = getEm()->getConnection()->commit();
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e){
                $rollback = getEm()->getConnection()->rollback();
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );
         
            $json_data = json_encode($data);
            print $json_data;
        }
        
        public function getPosClosures(){
            try{
                $begin = getEm()->getConnection()->beginTransaction();

                $entityManager = getEm();
                $arrayFilter = array(
                    "active" => 1
                );
                $pos_closures = $entityManager->getRepository('PosClosure')->findBy($arrayFilter,array("idposClosure"=>"desc"));

                $data = array ();

                foreach ($pos_closures as $value){
                    $user = $value->getUsersusers()->getPersonperson()->getName();

                    $difference = $value->getAmountReceived() - $value->getAmountCounted();
                    $dat = array (
                        "checkbox" => '<input type="checkbox" data-id="'.$value->getIdposClosure().'">',
                        "id" => $value->getIdposClosure(),
                        "date" => $value->getDateCreate()->format('d/m/Y - H:i:s'),
                        "user" => $user,
                        "amount_received"=> $value->getAmountReceived(),
                        "amount_counted"=> $value->getAmountCounted(),
                        "amount_credit_card"=> $value->getAmountCreditCard(),
                        "difference"=> $difference
                    );
                    array_push($data, $dat);
                }

                $commit = getEm()->getConnection()->commit();
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e){
                $rollback = getEm()->getConnection()->rollback();
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }    

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    

?>