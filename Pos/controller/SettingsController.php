<?php
/**
 * @author Claudio da Cruz Silva Junior>
 * @version 1.0.0
 * @abstract file created in date september 21, 2017
 */
class SettingsController {

    public function __construct() {
        
    }
    /**
     * Index call closing parameters of the POS chosen
     * 
     * @access public
     * @return void call preview
     */
    public function index() {
        $navbar = "Pos|Settings";
        $allPOS = getEm()->getRepository('Pos')->findBy(array('active' => 1));
        $array_answer = array(
            'listPos' => $allPOS
        );
        GenericController::template("Pos", "settings", "index", $navbar, $array_answer, 47);
    }

    public function delivery_list() {
        $navbar = "Pos|Settings";
        $allPOS = getEm()->getRepository('Pos')->findBy(array('active' => 1));
        $templates = getEm()->getRepository('OrderTmpt')->findBy(array("active" => 1));
        $prints = getEm()->getRepository('Printer')->findBy(array("active" => 1));
        $array_answer = array(
            'listPos' => $allPOS,
            'templates' => $templates,
            'prints' => $prints
        );
        GenericController::template("Pos", "settings", "delivery_list", $navbar, $array_answer, 49);
    }

    public function closing_parameters() {
        $navbar = "Pos|Settings";
        $allPOS = getEm()->getRepository('Pos')->findBy(array('active' => 1));
        $array_answer = array(
            'listPos' => $allPOS
        );
        GenericController::template("Pos", "settings", "closing_parameters", $navbar, $array_answer, 51);
    }

    public function credit_notes() {
        $navbar = "Pos|Settings";
        $allPOS = getEm()->getRepository('Pos')->findBy(array('active' => 1));
        $array_answer = array(
            'listPos' => $allPOS
        );
        GenericController::template("Pos", "settings", "credit_notes", $navbar, $array_answer, 53);
    }

    public function receipts() {
        $navbar = "Pos|Settings";
        $allPOS = getEm()->getRepository('Pos')->findBy(array('active' => 1));
        $array_answer = array(
            'listPos' => $allPOS
        );
        GenericController::template("Pos", "settings", "receipts", $navbar, $array_answer, 55);
    }
    /**
     * update the closing parameters of the POS chosen
     * @param int idPos
     * @return string json_data 
     */
    public function updateClosingParameters($idPos){
        $arrayValues = array (
            'text_additional'                                       => isset($_POST['text_additional']) ? $_POST['text_additional'] : '',
            'print_list_of_products_sold_on_the_day'                => isset($_POST['print_list_of_products_sold_on_the_day']) ? $_POST['print_list_of_products_sold_on_the_day'] : '',
            'print_quantity_of_products_sold_in_the_day'            => isset($_POST['print_quantity_of_products_sold_in_the_day']) ? $_POST['print_quantity_of_products_sold_in_the_day'] : '',
            'print_currency_and_cell_ratio_in_the_box'              => isset($_POST['print_currency_and_cell_ratio_in_the_box']) ? $_POST['print_currency_and_cell_ratio_in_the_box'] : '',
            'include_sales_by_tax_group'                            => isset($_POST['include_sales_by_tax_group']) ? $_POST['include_sales_by_tax_group'] : '',
            'use_the_default_printer_instead_of_the_fiscal_printer' => isset($_POST['use_the_default_printer_instead_of_the_fiscal_printer']) ? $_POST['use_the_default_printer_instead_of_the_fiscal_printer'] : '',
            'print_discount_rate_of_th_day'                         => isset($_POST['print_discount_rate_of_th_day']) ? $_POST['print_discount_rate_of_th_day'] : ''
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            for($i = 0; $i < count($arrayKeys); $i++){
                $key = $arrayKeys[$i];
                $pos = getEm()->getRepository('Pos')->findOneBy(array("idpos" => $idPos));
                $closingParameters = getEm()->getRepository('PosSettingsClosingParameters')->findBy(array('dataType' => $key, 'idpos' => $pos->getIdpos()));
                
                $additional = getEm()->getRepository('SettingsAdditional')->findBy(array('dataType' => $key));
                if($closingParameters){
                    $closingParameters[0]->setDataValue($arrayValues[$key]);
                    $closingParameters[0]->setDateUpdate(new DateTime());
                    $closingParameters[0]->setActive(1);
                    getEm()->persist($closingParameters[0]);
                    getEm()->flush();
                } else {
                    $closingParameters = new PosSettingsClosingParameters();
                    $closingParameters->setIdpos($pos);
                    $closingParameters->setDataType($key);
                    $closingParameters->setDataValue($arrayValues[$key]);
                    $closingParameters->setDateCreate(new DateTime());
                    $closingParameters->setActive(1);
                    getEm()->persist($closingParameters);
                    getEm()->flush();
                }
            }
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * get all closing parameters of the POS chosen
     * @param int idPos
     * @return string json_data 
     */
    public function getClosingParameters($idPos){
        $arrayValues = array ();
        $dat = array ();
        try {
            $pos = getEm()->getRepository('Pos')->findOneBy(array("idpos" => $idPos));
            $closingParameters = getEm()->getRepository('PosSettingsClosingParameters')->findBy(array('idpos' => $pos->getIdpos(), 'active' => 1));
            if($closingParameters != null){
                foreach ($closingParameters as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }else{
                $dat['text_additional'] = '';
                $dat['print_list_of_products_sold_on_the_day'] = '';
                $dat['print_quantity_of_products_sold_in_the_day'] = '';
                $dat['print_currency_and_cell_ratio_in_the_box'] = '';
                $dat['include_sales_by_tax_group'] = '';
                $dat['use_the_default_printer_instead_of_the_fiscal_printer'] = '';
                $dat['print_discount_rate_of_th_day'] = '';
                array_push($arrayValues, $dat);
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * update the closing parameters of the POS chosen
     * @param int idPos
     * @return string json_data 
     */
    public function updateDeliveryList($idPos){
        $arrayValues = array (
            'title'                                     => isset($_POST['title']) ? $_POST['title'] : '',
            'idTemplate'                                => isset($_POST['idTemplate']) ? $_POST['idTemplate'] : '',
            'group_by'                                  => isset($_POST['group_by']) ? $_POST['group_by'] : '',
            'size_font'                                 => isset($_POST['size_font']) ? $_POST['size_font'] : '',
            'text_additional'                           => isset($_POST['text_additional']) ? $_POST['text_additional'] : '',
            'group_identical_products'                  => isset($_POST['group_identical_products']) ? $_POST['group_identical_products'] : '',
            'confirm_print_before_print'                => isset($_POST['confirm_print_before_print']) ? $_POST['confirm_print_before_print'] : '',
            'show_price_in_print'                       => isset($_POST['show_price_in_print']) ? $_POST['show_price_in_print'] : '',
            'print_same_items_on_same_line'             => isset($_POST['print_same_items_on_same_line']) ? $_POST['print_same_items_on_same_line'] : '',
            'place_the_last_two_digits_of_the_purchase' => isset($_POST['place_the_last_two_digits_of_the_purchase']) ? $_POST['place_the_last_two_digits_of_the_purchase'] : '',
            'when_you_modify_a_purchase_order'          => isset($_POST['when_you_modify_a_purchase_order']) ? $_POST['when_you_modify_a_purchase_order'] : '',
            'print_param_per_carrier_title'             => isset($_POST['print_param_per_carrier_title']) ? $_POST['print_param_per_carrier_title'] : '',
            'print_param_idTemplate'                    => isset($_POST['print_param_idTemplate']) ? $_POST['print_param_idTemplate'] : '',
            'print_two_rows_for_each_product'           => isset($_POST['print_two_rows_for_each_product']) ? $_POST['print_two_rows_for_each_product'] : '',
            'omit_price_on_document'                    => isset($_POST['omit_price_on_document']) ? $_POST['omit_price_on_document'] : '',
            'print_group_to_which_the_product_belongs'  => isset($_POST['print_group_to_which_the_product_belongs']) ? $_POST['print_group_to_which_the_product_belongs'] : '',
            'print_two_rows_for_each_product'           => isset($_POST['print_two_rows_for_each_product']) ? $_POST['print_two_rows_for_each_product'] : '',
            'print_the_id_to_which_the_product_belongs' => isset($_POST['print_the_id_to_which_the_product_belongs']) ? $_POST['print_the_id_to_which_the_product_belongs'] : ''
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            for($i = 0; $i < count($arrayKeys); $i++){
                $key = $arrayKeys[$i];
                $pos = getEm()->getRepository('Pos')->findOneBy(array('idpos' => $idPos));
                $deliveryList = getEm()->getRepository('PosSettingsDeliveryList')->findOneBy(array('dataType' => $key, 'idpos' => $idPos));
                if($deliveryList){
                    $deliveryList->setDataValue($arrayValues[$key]);
                    $deliveryList->setDateUpdate(new DateTime());
                    $deliveryList->setActive(1);
                    getEm()->persist($deliveryList);
                    getEm()->flush();
                } else {
                    $deliveryList = new PosSettingsDeliveryList();
                    $deliveryList->setIdpos($pos);
                    $deliveryList->setDataType($key);
                    $deliveryList->setDataValue($arrayValues[$key]);
                    $deliveryList->setDateCreate(new DateTime());
                    $deliveryList->setActive(1);
                    getEm()->persist($deliveryList);
                    getEm()->flush();
                }
            }
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getDeliveryList($idPos){
        $arrayValues = array ();
        $dat = array ();
        try {
            $deliveryList = getEm()->getRepository('PosSettingsDeliveryList')->findBy(array('idpos' => $idPos));
            if($deliveryList != null){
                foreach ($deliveryList as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }else{
                $dat = array (
                    'title'                                     => '',
                    'idTemplate'                                => '',
                    'group_by'                                  => '',
                    'size_font'                                 => '',
                    'text_additional'                           => '',
                    'group_identical_products'                  => '',
                    'confirm_print_before_print'                => '',
                    'show_price_in_print'                       => '',
                    'print_same_items_on_same_line'             => '',
                    'place_the_last_two_digits_of_the_purchase' => '',
                    'when_you_modify_a_purchase_order'          => '',
                    'print_param_per_carrier_title'             => '',
                    'print_param_idTemplate'                    => '',
                    'print_two_rows_for_each_product'           => '',
                    'omit_price_on_document'                    => '',
                    'print_group_to_which_the_product_belongs'  => '',
                    'print_two_rows_for_each_product'           => '',
                    'print_the_id_to_which_the_product_belongs' => ''
                );
                array_push($arrayValues, $dat);
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateCreditNotes($idPos){
        $arrayValues = array (
            'text_additional'                           => isset($_POST['text_additional']) ? $_POST['text_additional'] : '',
            'number_of_ways_to_print'                   => isset($_POST['number_of_ways_to_print']) && $_POST['number_of_ways_to_print'] != "0" ? $_POST['number_of_ways_to_print'] : '',
            //'print_the_receipt_status_on_the_printer'   => isset($_POST['print_the_receipt_status_on_the_printer']) ? $_POST['print_the_receipt_status_on_the_printer'] : '',
            'gift_card_text_additional'                 => isset($_POST['gift_card_text_additional']) ? $_POST['gift_card_text_additional'] : '',
            'print_the_status_of_use_the_card'          => isset($_POST['print_the_status_of_use_the_card']) ? $_POST['print_the_status_of_use_the_card'] : ''
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            for($i = 0; $i < count($arrayKeys); $i++){
                $key = $arrayKeys[$i];
                $pos = getEm()->getRepository('Pos')->findOneBy(array('idpos' => $idPos));
                $creditNotes = getEm()->getRepository('PosSettingsCreditNotes')->findOneBy(array('dataType' => $key, 'idpos' => $idPos));
                if($creditNotes){
                    $creditNotes->setDataValue($arrayValues[$key]);
                    $creditNotes->setDateUpdate(new DateTime());
                    $creditNotes->setActive(1);
                    getEm()->persist($creditNotes);
                    getEm()->flush();
                } else {
                    $creditNotes = new PosSettingsCreditNotes();
                    $creditNotes->setIdpos($pos);
                    $creditNotes->setDataType($key);
                    $creditNotes->setDataValue($arrayValues[$key]);
                    $creditNotes->setDateCreate(new DateTime());
                    $creditNotes->setActive(1);
                    getEm()->persist($creditNotes);
                    getEm()->flush();
                }
            }
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getCreditNotes($idPos){
        $arrayValues = array ();
        $dat = array ();
        try {
            $creditNotes = getEm()->getRepository('PosSettingsCreditNotes')->findBy(array('idpos' => $idPos));
            if($creditNotes != null){
                foreach ($creditNotes as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }else{
                 $dat = array (
                    'text_additional'                           => '',
                    'number_of_ways_to_print'                   => '', 
                    'gift_card_text_additional'                 => '',
                    'print_the_status_of_use_the_card'          => ''
                );
                array_push($arrayValues, $dat);
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateReceipts($idPos){
        $arrayValues = array (
            'title_coupon'                              => isset($_POST['title_coupon']) ? $_POST['title_coupon'] : '',
            'set_to'                                    => isset($_POST['set_to']) ? $_POST['set_to'] : '',
            'message_cupom'                             => isset($_POST['message_cupom']) ? $_POST['message_cupom'] : '',
            'message_byttelapp'                         => isset($_POST['message_byttelapp']) ? $_POST['message_byttelapp'] : '',
            'print_two_lines'                           => isset($_POST['print_two_lines']) ? $_POST['print_two_lines'] : '',
            'when_printing_the_tax_coupon'              => isset($_POST['when_printing_the_tax_coupon']) ? $_POST['when_printing_the_tax_coupon'] : '',
            'print_shop_address_on_tax_coupon'          => isset($_POST['print_shop_address_on_tax_coupon']) ? $_POST['print_shop_address_on_tax_coupon'] : '',
            'print_the_tax_values_on_each_product'      => isset($_POST['print_the_tax_values_on_each_product']) ? $_POST['print_the_tax_values_on_each_product'] : '',
            'print_detailed_product_description'        => isset($_POST['print_detailed_product_description']) ? $_POST['print_detailed_product_description'] : '',
            'hide_comments_entered_in_the_purchase'     => isset($_POST['hide_comments_entered_in_the_purchase']) ? $_POST['hide_comments_entered_in_the_purchase'] : '',
            'always_the_article_number_is_printed'      => isset($_POST['always_the_article_number_is_printed']) ? $_POST['always_the_article_number_is_printed'] : '',
            'print_discount_rate_given_on_the_day'      => isset($_POST['print_discount_rate_given_on_the_day']) ? $_POST['print_discount_rate_given_on_the_day'] : '',
            'prints_the_values_without_taxes_and_after' => isset($_POST['prints_the_values_without_taxes_and_after']) ? $_POST['prints_the_values_without_taxes_and_after'] : '',
            'includes_tax_tip_amounts_paid'             => isset($_POST['includes_tax_tip_amounts_paid']) ? $_POST['includes_tax_tip_amounts_paid'] : ''
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            for($i = 0; $i < count($arrayKeys); $i++){
                $key = $arrayKeys[$i];
                $pos = getEm()->getRepository('Pos')->findOneBy(array('idpos' => $idPos));
                $receipts = getEm()->getRepository('PosSettingsReceipts')->findOneBy(array('dataType' => $key, 'idpos' => $idPos));
                if($receipts){
                    $receipts->setDataValue($arrayValues[$key]);
                    $receipts->setDateUpdate(new DateTime());
                    $receipts->setActive(1);
                    getEm()->persist($receipts);
                    getEm()->flush();
                } else {
                    $receipts = new PosSettingsReceipts();
                    $receipts->setIdpos($pos);
                    $receipts->setDataType($key);
                    $receipts->setDataValue($arrayValues[$key]);
                    $receipts->setDateCreate(new DateTime());
                    $receipts->setActive(1);
                    getEm()->persist($receipts);
                    getEm()->flush();
                }
            }
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getReceipts($idPos){
        $arrayValues = array ();
        $dat = array ();
        try {
            $receipts = getEm()->getRepository('PosSettingsReceipts')->findBy(array('idpos' => $idPos));
            if($receipts != null){
                foreach ($receipts as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }else{
                $dat = array (
                    'title_coupon'                              => '',
                    'set_to'                                    => '',
                    'message_cupom'                             => '',
                    'message_byttelapp'                         => '',
                    'print_two_lines'                           => '',
                    'when_printing_the_tax_coupon'              => '',
                    'print_shop_address_on_tax_coupon'          => '',
                    'print_the_tax_values_on_each_product'      => '',
                    'print_detailed_product_description'        => '',
                    'hide_comments_entered_in_the_purchase'     => '',
                    'always_the_article_number_is_printed'      => '',
                    'print_discount_rate_given_on_the_day'      => '',
                    'prints_the_values_without_taxes_and_after' => '',
                    'includes_tax_tip_amounts_paid'             => ''
                );
                array_push($arrayValues, $dat);
        
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        //var_dump($data);
        //die();
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getFeatures($idPos){
        $arrayValues = array ();
        $dat = array ();
        try {
            $features = getEm()->getRepository('PosSettingsFeatures')->findBy(array('idpos' => $idPos));
            if($features != null){
                foreach ($features as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }else{
                $dat = array (
                    'pos_display_fist_line'                 => '',
                    'pos_display_second_line'               => '',
                    'temporary_receipt_fist_line'           => '',
                    'temporary_receipt_second_line'         => '',
                    
                    'log_purchase_order_canceled'           => '',
                    'log_items_canceled'                    => '',
                    'log_cash_drawer_manual_openings'       => '',
                    'log_record_purchase_orders_deleted'    => '',
                    'show_similar_items'                    => '',
                    'add_remarks_to_prepayment'             => '',
                    'not_emit'                              => '',
                    'replace_the_pos_logo'                  => '',
                    'show_all_orders_during'                => '',
                    'enable_delivery_list'                  => '',
                    'do_not_issue_tax_document'             => '',
                    'request_confirmation_of_product'       => '',
                    'do_not_allow_the_user_terminate'       => '',
                    'do_not_allow_payment'                  => '',
                    'in_case_customer_pays_with_giftcard'   => ''
                );

             array_push($arrayValues, $dat);   
                
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateFeatures($idPos){
        $arrayValues = array (
            'pos_display_fist_line'                 => isset($_POST['pos_display_fist_line']) ? $_POST['pos_display_fist_line'] : '',
            'pos_display_second_line'               => isset($_POST['pos_display_second_line']) ? $_POST['pos_display_second_line'] : '',
            'temporary_receipt_fist_line'           => isset($_POST['temporary_receipt_fist_line']) ? $_POST['temporary_receipt_fist_line'] : '',
            'temporary_receipt_second_line'         => isset($_POST['temporary_receipt_second_line']) ? $_POST['temporary_receipt_second_line'] : '',
            
            'log_purchase_order_canceled'           => isset($_POST['log_purchase_order_canceled']) ? 1 : 0,
            'log_items_canceled'                    => isset($_POST['log_items_canceled']) ? 1 : 0,
            'log_cash_drawer_manual_openings'       => isset($_POST['log_cash_drawer_manual_openings']) ? 1 : 0,
            'log_record_purchase_orders_deleted'    => isset($_POST['log_record_purchase_orders_deleted']) ? 1 : 0,
            'show_similar_items'                    => isset($_POST['show_similar_items']) ? 1 : 0,
            'add_remarks_to_prepayment'             => isset($_POST['add_remarks_to_prepayment']) ? 1 : 0,
            'not_emit'                              => isset($_POST['not_emit']) ? 1 : 0,
            'replace_the_pos_logo'                  => isset($_POST['replace_the_pos_logo']) ? 1 : 0,
            'show_all_orders_during'                => isset($_POST['show_all_orders_during']) ? 1 : 0,
            'enable_delivery_list'                  => isset($_POST['enable_delivery_list']) ? 1 : 0,
            'do_not_issue_tax_document'             => isset($_POST['do_not_issue_tax_document']) ? 1 : 0,
            'request_confirmation_of_product'       => isset($_POST['request_confirmation_of_product']) ? 1 : 0,
            'do_not_allow_the_user_terminate'       => isset($_POST['do_not_allow_the_user_terminate']) ? 1 : 0,
            'do_not_allow_payment'                  => isset($_POST['do_not_allow_payment']) ? 1 : 0,
            'in_case_customer_pays_with_giftcard'   => isset($_POST['in_case_customer_pays_with_giftcard']) ? 1 : 0
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            $pos = getEm()->getRepository('Pos')->findOneBy(array('idpos' => $idPos));
            for($i = 0; $i < count($arrayKeys); $i++){
                $key = $arrayKeys[$i];
                $features = getEm()->getRepository('PosSettingsFeatures')->findOneBy(array('dataType' => $key, 'idpos' => $idPos));
                if($features){
                    $features->setDataValue($arrayValues[$key]);
                    $features->setDateUpdate(new DateTime());
                    $features->setActive(1);
                    getEm()->persist($features);
                    getEm()->flush();
                } else {
                    $features = new PosSettingsFeatures();
                    $features->setIdpos($pos);
                    $features->setDataType($key);
                    $features->setDataValue($arrayValues[$key]);
                    $features->setDateCreate(new DateTime());
                    $features->setActive(1);
                    getEm()->persist($features);
                    getEm()->flush();
                }
            }
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }
}

?>