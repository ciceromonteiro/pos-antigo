<?php

require_once '../init_autoload.php';


class LayoutController {

    public function __construct() {
        
    }
    
    public function index($id){
        $navbar = "Pos|layout";
        $allPOS = getEm()->getRepository('Pos')->findBy(array('active' => 1));
        $allCommands = getEm()->getRepository('PosTmptCommands')->findBy(array('active'=>1, 'language' => 'no'));
        $allFunctions = getEm()->getRepository('PosTmptBtnFunctions')->findBy(array('active' => 1,'language' => 'us'));
        $layouts = getEm()->getRepository('PosTmpt')->findBy(array('active' => 1));

        $posTmptInPos = getEm()->getRepository('PosTmptInPos')->findBy(array('idposTmptInPos' => $id));
        $array_answer = array(
            'listPos' => $allPOS,
            'commands' => $allCommands,
            'functions' => $allFunctions,
            'layout' => '',
            'idLayout' => ''
        );
        foreach ($layouts as $layout) {
            if($posTmptInPos[0]->getPosTmptposTmpt()->getIdposTmpt() == $layout->getIdposTmpt()){
            $array_answer['layout'] = $layout->getStructureHtml();
            $array_answer['idLayout'] = $layout->getIdposTmpt();

            }
        
        }
        
        GenericController::template("Pos", "layout","index3", $navbar, $array_answer, 58);
    }
    
    
    
    public function demo(){
        $navbar = "Pos|layout";
        $array_answer = array();
        GenericController::template("Pos", "layout","demo", $navbar, $array_answer, 58);
    }
    
    public function build(){
        $navbar = "Pos|build";
        $array_answer = array();
        GenericController::template("Pos", "layout","build", $navbar, $array_answer, 58);
    }

    public function createStructure($description){
        $description = base64_decode($description);
       
        $structure = $_POST['structureCodif'];
        $structure = str_replace('<<----->>', " ", $structure);
        $structure = str_replace('<<<<---->>>>', "/", $structure);
        $structure = str_replace('<-->', "%", $structure);
            try {
                $PosPreset = new PosPreset();
                $PosPreset->setDescription($description);
                $PosPreset->setStructure($structure);
                $PosPreset->setActive(1);
                getEm()->persist($PosPreset);
                getEm()->flush();
                $result = 'success';
                $message = 'query success';
                 $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }


    public static function getAll(){
        try{
            $layouts = getEm()->getRepository('PosTmptInPos')->findBy(array("active" => 1));
            $data = array ();
            foreach ($layouts as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdposTmptInPos().'">',
                    "id" => $value->getIdposTmptInPos(),
                    "device" => $value->getPospos()->getNickname(),
                    "resolution" => $value->getPosTmptposTmpt()->getResolution(),
                    "description" => $value->getPosTmptposTmpt()->getName(),
                    "layoutEdit" => '<button class="btn btn-primary" onclick="openLayoutTerminal('.$value->getIdposTmptInPos().')">Edit Layout</button>'
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function listTemplate(){
        $navbar = "Pos|layout";        
        $pos = getEm()->getRepository('Pos')->findBy(array('active' => 1));
        $preset = getEm()->getRepository('PosPreset')->findBy(array('active' => 1));
        $array_answer = array(
            "devices" => $pos,
            "presets" => $preset
        );        
        GenericController::template("Pos", "layout","list", $navbar, $array_answer, 58);
    }
    
    public function getAllCommands(){
        try{
            $commands = getEm()->getRepository('PosTmptCommands')->findBy(array("active" => 1));
            $data = array ();
            foreach ($commands as $value){
                $dat = array (
                    "id" => $value->getIdPosTmptCommands(),
                    "command" => $value->getCommand(),
                    "description" => $value->getDescription()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    /*public static function save($id){
        $structure = $_POST['structureCodif'];
        $structure = str_replace('<<----->>', " ", $structure);
        $structure = str_replace('<<<<---->>>>', "/", $structure);
        $structure = str_replace('<-->', "%", $structure);
            $layout = getEm()->getRepository('PosTmpt')->findBy(array('active' => 1, 'idposTmpt' => $id));

                try {
                    $layout[0]->setStructureHtml($structure);
                    $layout[0]->setDateUpdate(new DateTime());
                    $layout[0]->setActive(1);
                    getEm()->persist($layout[0]);
                    getEm()->flush();
                    $result  = 'success';
                    $message = 'query success';
                    $data = "";
                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
            } else {
                try {
                    $layout = new PosTmpt();
                    $layout->setName('test');
                    $layout->setStructureHtml($_POST['structure']);
                    $layout->setResolution('1024px x 768px');
                    $layout->setDateCreate(new DateTime());
                    $layout->setActive(1);
                    getEm()->persist($layout);
                    getEm()->flush();
                    
                    $layoutInPos = new PosTmptInPos();
                    $pos = getEm()->getRepository('Pos')->findAll();
                    $layoutInPos->setPospos($pos[0]);
                    $layoutInPos->setPosTmptposTmpt($layout);
                    $layoutInPos->setDateCreate(new DateTime());
                    $layoutInPos->setActive(1);
                    getEm()->persist($layoutInPos);
                    getEm()->flush();

                    $result  = 'success';
                    $message = 'query success';
                    $data = "";

                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
            }
            
        }

        $returnStructure = $layout[0]->getStructureHtml();
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data,
            "structure" => $structure
        );

        $json_data = json_encode($data);
        print $json_data;
    }*/
    
    public static function save($id){
        $structure = $_POST['structureCodif'];
        $structure = str_replace('<<----->>', " ", $structure);
        $structure = str_replace('<<<<---->>>>', "/", $structure);
        $structure = str_replace('<-->', "%", $structure);
            $layout = getEm()->getRepository('PosTmpt')->findBy(array('active' => 1, 'idposTmpt' => $id));

                try {
                    $layout[0]->setStructureHtml($structure);
                    $layout[0]->setDateUpdate(new DateTime());
                    $layout[0]->setActive(1);
                    getEm()->persist($layout[0]);
                    getEm()->flush();
                    $result  = 'success';
                    $message = 'query success';
                    $data = "";
                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
        $returnStructure = $layout[0]->getStructureHtml();
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data,
            "structure" => $structure
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function insertTemplate($device, $resolution, $description, $preset, $userlog){
         $user = getEm()->getRepository('Users')->findBy(array('idusers' => $userlog));
        

        if(isset($device) && isset($resolution)){
           
            try {
                if($this->hasPosInTemplate($device) == true){
                    $begin = getEm()->getConnection()->beginTransaction();
                    $layout = new PosTmpt();

                    $pos = getEm()->getRepository('Pos')->findOneBy(array('idpos' => $device));                
                    $layout->setName($description);
                    $layout->setResolution($resolution);
                    $layout->setUsersusers($user[0]);
                    
                    
                   
                    if(isset($preset) && $preset != 0){
                        $presetObj = getEm()->getRepository('PosPreset')->findOneBy(array('idpospreset' => $preset));

                        $layout->setStructureHtml($presetObj->getStructure());
                    } else {
                        $layout->setStructureHtml("");                    
                    }


                    $layout->setDateCreate(new DateTime());
                    $layout->setActive(1);
                    
                    getEm()->persist($layout);
                    getEm()->flush();

                    $layout_in_pos = new PosTmptInPos();
                    $layout_in_pos->setPosTmptposTmpt($layout);
                    $layout_in_pos->setPospos($pos);
                    $layout_in_pos->setDateCreate(new DateTime());
                    $layout_in_pos->setActive(1);
                    getEm()->persist($layout_in_pos);
                    getEm()->flush();

                    //AuthenticationController::insertLog('create', 'layout for terminal', $description);
                    $commit = getEm()->getConnection()->commit();
                    $result  = 'success';
                } else {
                    $result  = 'duplicate';
                }
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                //$rollback = getEm()->getConnection()->rollback();
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }


        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * pull value of idpos
     * 
     * @access public
     * @param int $id
     * @return int
     */
    public function getPosTmptLogin($id) {

        $user = getEm()->getRepository('Users')->findBy(array("idusers" => $id));
        $userlog = getEm()->getRepository('LogTime')->findBy(array("usersusers" => $user[0]), array('idlogTime' => 'desc'));
        return $userlog[0]->getPosIdpos();
    }

    public function getTemplate(){
        if(isset($_POST['id'])){
            try {
                $layout_in_pos = getEm()->getRepository('PosTmptInPos')->findBy(array("idposTmptInPos" => $_POST['id']));
                $array_data = array ();
                foreach ($layout_in_pos as $value) {
                    $dat = array (
                        "id" => $value->getIdposTmptInPos(),
                        "device" => $value->getPospos()->getIdpos(),
                        "resolution" => $value->getPosTmptposTmpt()->getResolution(),
                        "description" => $value->getPosTmptposTmpt()->getName(),
                        "preset" => ''
                    );
                    array_push($array_data, $dat);
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;        
    }
    
    public function updateTemplate(){
        if(isset($_POST['device']) && isset($_POST['resolution'])){
            try {
                $begin = getEm()->getConnection()->beginTransaction();
                
                $pos = getEm()->getRepository('Pos')->findOneBy(array('idpos' => $_POST['device']));
                $layout_in_pos = getEm()->getRepository('PosTmptInPos')->findOneBy(array('idposTmptInPos' => $_POST['idPosTmpt']));
                $layout_in_pos->setPospos($pos);
                $layout_in_pos->getPosTmptposTmpt()->setName($_POST['description']);

                if(isset($_POST['preset']) && $_POST['preset'] != "0" && $_POST['preset'] != ""){
                    $preset = getEm()->getRepository('Pos')->findOneBy(array('idpos' => $_POST['preset']));
                    $layout_in_pos->getPosTmptposTmpt()->setStructureHtml($preset->getStructureHtml());
                    $layout_in_pos->getPosTmptposTmpt()->setResolution($preset->getResolution());
                } else {
                    $layout_in_pos->getPosTmptposTmpt()->setResolution($_POST['resolution']);
                }
                
                $layout_in_pos->setDateUpdate(new DateTime());
                $layout_in_pos->setActive(1);
                getEm()->persist($layout_in_pos);
                getEm()->flush();

                AuthenticationController::insertLog('create', 'layout for terminal', $_POST['description']);
                $commit = getEm()->getConnection()->commit();
                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $rollback = getEm()->getConnection()->rollback();
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function deleteTemplate(){
        if($_POST['idLayouts']){
            try{
                $ids = $_POST['idLayouts'];
                for($i=0; $i < count($ids); $i++){
                    $layout_in_pos = getEm()->getRepository("PosTmptInPos")->findOneBy(array("idposTmptInPos" => $ids[$i]));
                    $layout_in_pos->getPosTmptposTmpt()->setActive('3');
                    $layout_in_pos->setActive('3');
                    $layout_in_pos->setDateDelete(new DateTime());
                    getEm()->persist($layout_in_pos);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'layout for terminal', $layout_in_pos->getPosTmptposTmpt()->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }        
    }
    
    public function hasPosInTemplate($device){
        $pos = getEm()->getRepository('PosTmptInPos')->findOneBy(array('pospos' => $device));
        if($pos == null){
            return true;
           /* if($pos->getPosTmptposTmpt()->getActive() != 1){
                return true;
            } else {
                return false;
            }*/
        } else {
            return false;
        }
    }
}
