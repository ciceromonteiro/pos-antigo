<?php 
	$nav = "Receipts";
	include "nav.php";
?>

<script type="text/javascript">
    $(document).ready(function() {
        var idPos = $('#posSelect').val();
        function getData(){
            var idPos = $('#posSelect').val();
            $.ajax({
                url : my_url+"Pos/Settings/getReceipts/"+idPos,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#pos_settings_receipts_form #title_coupon').val(data.data[0].title_coupon);
                        $('#pos_settings_receipts_form #message_cupom').val(data.data[0].message_cupom);
                        $('#pos_settings_receipts_form #message_byttelapp').val(data.data[0].message_byttelapp);
                        if(data.data[0].print_two_lines == "on" || data.data[0].print_two_lines == "1"){
                            document.getElementById("print_two_lines").checked = true;
                        }else{
                            document.getElementById("print_two_lines").checked = false;
                        }
                        if(data.data[0].when_printing_the_tax_coupon == "on" || data.data[0].when_printing_the_tax_coupon == "1"){
                            document.getElementById("when_printing_the_tax_coupon").checked = true;
                        }else{
                            document.getElementById("when_printing_the_tax_coupon").checked = false;
                        }
                        if(data.data[0].print_shop_address_on_tax_coupon == "on" || data.data[0].print_shop_address_on_tax_coupon == "1"){
                            document.getElementById("print_shop_address_on_tax_coupon").checked = true;
                        }else{
                            document.getElementById("print_shop_address_on_tax_coupon").checked = false;
                        }
                        if(data.data[0].print_the_tax_values_on_each_product == "on" || data.data[0].print_the_tax_values_on_each_product == "1"){
                            document.getElementById("print_the_tax_values_on_each_product").checked = true;
                        }else{
                            document.getElementById("print_the_tax_values_on_each_product").checked = false;
                        }
                        if(data.data[0].print_detailed_product_description == "on" || data.data[0].print_detailed_product_description == "1"){
                            document.getElementById("print_detailed_product_description").checked = true;
                        }else{
                            document.getElementById("print_detailed_product_description").checked = false;
                        }
                        if(data.data[0].hide_comments_entered_in_the_purchase == "on" || data.data[0].hide_comments_entered_in_the_purchase == "1"){
                            document.getElementById("hide_comments_entered_in_the_purchase").checked = true;
                        }else{
                            document.getElementById("hide_comments_entered_in_the_purchase").checked = false;
                        }
                        if(data.data[0].always_the_article_number_is_printed == "on" || data.data[0].always_the_article_number_is_printed == "1"){
                            document.getElementById("always_the_article_number_is_printed").checked = true;
                        }else{
                            document.getElementById("always_the_article_number_is_printed").checked = false;
                        }
                        if(data.data[0].print_discount_rate_given_on_the_day == "on" || data.data[0].print_discount_rate_given_on_the_day == "1"){
                            document.getElementById("print_discount_rate_given_on_the_day").checked = true;
                        }else{
                            document.getElementById("print_discount_rate_given_on_the_day").checked = false;
                        }
                        if(data.data[0].prints_the_values_without_taxes_and_after == "on" || data.data[0].prints_the_values_without_taxes_and_after == "1"){
                            document.getElementById("prints_the_values_without_taxes_and_after").checked = true;
                        }else{
                            document.getElementById("prints_the_values_without_taxes_and_after").checked = false;
                        }
                        if(data.data[0].includes_tax_tip_amounts_paid == "on" || data.data[0].includes_tax_tip_amounts_paid == "1"){
                            document.getElementById("includes_tax_tip_amounts_paid").checked = true;
                        }else{
                            document.getElementById("includes_tax_tip_amounts_paid").checked = false;
                        }

                         document.getElementById("set_to").value = data.data[0].set_to;


                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }
        
        $(document).on('submit', '#pos_settings_receipts_form', function(e){
            var pos = $('#posSelect').val();
            e.preventDefault();
            $.ajax({
                url : my_url+"Pos/Settings/updateReceipts/"+pos,
                type: "POST",
                dataType: 'JSON',
                data : $('#pos_settings_receipts_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        document.getElementById("pos_settings_receipts_form").reset();
        getData();
    });


    function changeReceiptts(){
        var pos = $('#posSelect').val();
            $.ajax({
                url : my_url+"Pos/Settings/getReceipts/"+pos,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                         $('#pos_settings_receipts_form #title_coupon').val(data.data[0].title_coupon);
                        $('#pos_settings_receipts_form #message_cupom').val(data.data[0].message_cupom);
                        $('#pos_settings_receipts_form #message_byttelapp').val(data.data[0].message_byttelapp);
                        if(data.data[0].print_two_lines == "on" || data.data[0].print_two_lines == "1"){
                            document.getElementById("print_two_lines").checked = true;
                        }else{
                            document.getElementById("print_two_lines").checked = false;
                        }
                        if(data.data[0].when_printing_the_tax_coupon == "on" || data.data[0].when_printing_the_tax_coupon == "1"){
                            document.getElementById("when_printing_the_tax_coupon").checked = true;
                        }else{
                            document.getElementById("when_printing_the_tax_coupon").checked = false;
                        }
                        if(data.data[0].print_shop_address_on_tax_coupon == "on" || data.data[0].print_shop_address_on_tax_coupon == "1"){
                            document.getElementById("print_shop_address_on_tax_coupon").checked = true;
                        }else{
                            document.getElementById("print_shop_address_on_tax_coupon").checked = false;
                        }
                        if(data.data[0].print_the_tax_values_on_each_product == "on" || data.data[0].print_the_tax_values_on_each_product == "1"){
                            document.getElementById("print_the_tax_values_on_each_product").checked = true;
                        }else{
                            document.getElementById("print_the_tax_values_on_each_product").checked = false;
                        }
                        if(data.data[0].print_detailed_product_description == "on" || data.data[0].print_detailed_product_description == "1"){
                            document.getElementById("print_detailed_product_description").checked = true;
                        }else{
                            document.getElementById("print_detailed_product_description").checked = false;
                        }
                        if(data.data[0].hide_comments_entered_in_the_purchase == "on" || data.data[0].hide_comments_entered_in_the_purchase == "1"){
                            document.getElementById("hide_comments_entered_in_the_purchase").checked = true;
                        }else{
                            document.getElementById("hide_comments_entered_in_the_purchase").checked = false;
                        }
                        if(data.data[0].always_the_article_number_is_printed == "on" || data.data[0].always_the_article_number_is_printed == "1"){
                            document.getElementById("always_the_article_number_is_printed").checked = true;
                        }else{
                            document.getElementById("always_the_article_number_is_printed").checked = false;
                        }
                        if(data.data[0].print_discount_rate_given_on_the_day == "on" || data.data[0].print_discount_rate_given_on_the_day == "1"){
                            document.getElementById("print_discount_rate_given_on_the_day").checked = true;
                        }else{
                            document.getElementById("print_discount_rate_given_on_the_day").checked = false;
                        }
                        if(data.data[0].prints_the_values_without_taxes_and_after == "on" || data.data[0].prints_the_values_without_taxes_and_after == "1"){
                            document.getElementById("prints_the_values_without_taxes_and_after").checked = true;
                        }else{
                            document.getElementById("prints_the_values_without_taxes_and_after").checked = false;
                        }
                        if(data.data[0].includes_tax_tip_amounts_paid == "on" || data.data[0].includes_tax_tip_amounts_paid == "1"){
                            document.getElementById("includes_tax_tip_amounts_paid").checked = true;
                        }else{
                            document.getElementById("includes_tax_tip_amounts_paid").checked = false;
                        }

                         document.getElementById("set_to").value = data.data[0].set_to;

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }
</script>

<div class="content">
    <form id="pos_settings_receipts_form">
        <div class="row row-fluid" style="margin-bottom: 60px;">
            <div class="col-md-12">
                <div class="col-md-4">
                <h4 class="translate">Coupon</h4>
                <input name="title_coupon" id="title_coupon" type="text" class="form-control" placeholder="Title"><br>
                <textarea class="form-control" cols="5" rows="6" id="message_cupom" name="message_cupom" placeholder="Final message of the tax coupon"></textarea>
            </div>
            <div class="col-md-4">
                <h4 class="translate">Byttelapp</h4>
                <textarea class="form-control" cols="5" rows="6" id="message_byttelapp" name="message_byttelapp" placeholder="Message that will appear in the purchase vouchers"></textarea>
            </div>
            <div class="col-md-4">
                <label for="set_to" class="translate">Set to</label>
                <select name="set_to" id="set_to">
                    <option value="0">Select option</option>
                    <option value="1">Internally in one shop</option>
                    <option value="2">For chain of shops</option>
                </select>
                <p><br><input type="checkbox" name="print_two_lines" id="print_two_lines"> <t class="translate">Print two lines for each item, allowing you to display more information</t></p>
                <p><input type="checkbox" name="when_printing_the_tax_coupon" id="when_printing_the_tax_coupon"> <t class="translate">When printing the tax coupon, the actual promotional values of the products are printed</t></p>
                <p><input type="checkbox" name="print_shop_address_on_tax_coupon" id="print_shop_address_on_tax_coupon"> <t class="translate">Print shop address on tax coupon</t></p>
                <p><input type="checkbox" name="print_the_tax_values_on_each_product" id="print_the_tax_values_on_each_product"> <t class="translate">Print the tax values on each product</t></p>
                <p><input type="checkbox" name="print_detailed_product_description" id="print_detailed_product_description"> <t class="translate">Print detailed product description</t></p>
                <p><input type="checkbox" name="hide_comments_entered_in_the_purchase" id="hide_comments_entered_in_the_purchase"> <t class="translate">Hide comments entered in the purchase order</t></p>
                <p><input type="checkbox" name="always_the_article_number_is_printed" id="always_the_article_number_is_printed"> <t class="translate">Always the article number is printed</t></p>
                <p><input type="checkbox" name="print_discount_rate_given_on_the_day" id="print_discount_rate_given_on_the_day"> <t class="translate">Print discount rate given on the day</t></p>
                <p><input type="checkbox" name="prints_the_values_without_taxes_and_after" id="prints_the_values_without_taxes_and_after"> <t class="translate">Prints the values without taxes and after the end shows and sums the taxes</t></p>
                <p><input type="checkbox" name="includes_tax_tip_amounts_paid" id="includes_tax_tip_amounts_paid"> <t class="translate">Includes tax tip amounts paid</t></p>
            </div>
            </div>
        </div>


        <div class="btns-footer">
            <div class="pull-right">
                <button type="reset" class="btn btn-primary translate">Cancel</button>
                <button type="submit" class="btn btn-success translate">Save</button>
            </div>
        </div>
    </form>
</div>
