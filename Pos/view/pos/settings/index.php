<?php
    $nav = "Features";
    include "nav.php";
?>

<script type="text/javascript">
    $(document).ready(function () {
        function getData() {
            var pos = $('#posSelect').val();
            $.ajax({
                url: my_url + "Pos/Settings/getFeatures/"+pos,
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        var values = data.data[0];
                        for (var prop in values) {
                            if (prop == "pos_display_fist_line" || prop == "pos_display_second_line" || prop == "temporary_receipt_fist_line"
                                    || prop == "temporary_receipt_second_line") {
                                //selects
                                document.getElementById(prop).value = values[prop];
                            } else {
                                //checkboxes
                                if (values[prop] == "on" || values[prop] == "1") {
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }

        $(document).on('submit', '#pos_settings_features_form', function (e) {
            var pos = $('#posSelect').val();
            e.preventDefault();
            $.ajax({
                url: my_url + "Pos/Settings/updateFeatures/"+pos,
                type: "POST",
                dataType: 'JSON',
                data: $('#pos_settings_features_form').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        document.getElementById("pos_settings_features_form").reset();
        getData();
    });

    function getIndex() {
            var pos = $('#posSelect').val();
            $.ajax({
                url: my_url + "Pos/Settings/getFeatures/"+pos,
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        var values = data.data[0];
                        for (var prop in values) {
                            if (prop == "pos_display_fist_line" || prop == "pos_display_second_line" || prop == "temporary_receipt_fist_line"
                                    || prop == "temporary_receipt_second_line") {
                                //selects
                                document.getElementById(prop).value = values[prop];
                            } else {
                                //checkboxes
                                if (values[prop] == "on" || values[prop] == "1") {
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }
</script>

<div class="content">
    <form id="pos_settings_features_form">
        <div class="row row-fluid" style="margin-bottom: 60px;">
            <div class="col-md-12">
                <div class="col-md-4">
                    <h4 style="color:#f00;" class="translate">POS display message to client</h4>
                    <label for="fist_line" class="translate">First line</label>
                    <input type="text" name="pos_display_fist_line" id="pos_display_fist_line" class="form-control">

                    <label for="second_line" class="translate">Second line</label>
                    <input type="text" name="pos_display_second_line" id="pos_display_second_line" class="form-control">
                </div>

                <div class="col-md-4">
                    <h4 class="translate">Temporary Receipt</h4>
                    <label for="fist_line" class="translate">Header:</label>
                    <input type="text" id="temporary_receipt_fist_line" name="temporary_receipt_fist_line" class="form-control">

                    <label for="second_line" class="translate">Message:</label>
                    <textarea cols="5" rows="6" id="temporary_receipt_second_line" name="temporary_receipt_second_line" class="form-control"></textarea>
                </div>

                <div class="col-md-4">
                    <h4 class="translate">Log records will be printed at the daily end of the box</h4>
                    <p><input type="checkbox" name="log_purchase_order_canceled" id="log_purchase_order_canceled"> <t class="translate">Purchase Order Canceled</t></p>
                    <p><input type="checkbox" name="log_items_canceled" id="log_items_canceled"> <t class="translate"> Items Canceled</t></p>
                    <p><input type="checkbox" name="log_cash_drawer_manual_openings" id="log_cash_drawer_manual_openings"> <t class="translate">Cash drawer manual openings (cash register)</t></p>
                    <p><input type="checkbox" name="log_record_purchase_orders_deleted" id="log_record_purchase_orders_deleted"> <t class="translate">Record purchase orders deleted</t></p>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 30px">
                <div class="col-md-4" style="color: red">
                    <p class="translate">Clicking the orders button created in the POS will display a list of orders already created and you can add 
                        columns to this listing through this element.</p>
                    <div class="col-md-12"> 
                        <div class="input-group">
                            <input type="text" class="form-control" style="color: red">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button"><span style="color: red" class="lnr lnr-magnifier"></span></button>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <p><input type="checkbox" name="show_similar_items" id="show_similar_items"> <t class="translate">Show similar items on the shopping list (after typing)</t></p>
                    <p><input type="checkbox" name="add_remarks_to_prepayment" id="add_remarks_to_prepayment"> <t class="translate">Add remarks to pre-payment records (restaurants)</t></p>
                    <p><input type="checkbox" name="not_emit" id="not_emit"> <t class="translate">Do not issue tax document for payment in cash</t></p>

                    <p style="color:#f00;"><input type="checkbox" name="replace_the_pos_logo" id="replace_the_pos_logo"> <t class="translate">Replace the POS logo with bars indicating the sales volume in all stores</t></p>
                    <p><input type="checkbox" name="show_all_orders_during" id="show_all_orders_during"> <t class="translate">Show all orders during search / exact names only</t></p>
                    <p><input type="checkbox" name="enable_delivery_list" id="enable_delivery_list"> <t class="translate">Enable Delivery List Printing</t></p>
                    <p><input type="checkbox" name="do_not_issue_tax_document" id="do_not_issue_tax_document"> <t class="translate">Do not issue tax document for payment in cash</t></p>

                    <p><input type="checkbox" name="request_confirmation_of_product" id="request_confirmation_of_product"> <t class="translate">Request confirmation of product values when changing customer</t></p>
                    <p><input type="checkbox" name="do_not_allow_the_user_terminate" id="do_not_allow_the_user_terminate"> <t class="translate">Do not allow the user to terminate payment before entering the amount the customer paid</t></p>
                    <p><input type="checkbox" name="do_not_allow_payment" id="do_not_allow_payment"> <t class="translate">Do not allow payment by credit card / debit card</t></p>
                    <p><input type="checkbox" name="in_case_customer_pays_with_giftcard" id="in_case_customer_pays_with_giftcard"> <t class="translate">In case customer pays with giftcard, do not show giftcard details</t></p>
                </div>
            </div>
        </div>


        <div class="btns-footer">
            <div class="pull-right">
                <button type="submit" class="btn btn-success translate">Save</button>
            </div>
        </div>
    </form>
</div>