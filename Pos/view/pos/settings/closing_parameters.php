<?php 
	$nav = "Closing parameters";
	include "nav.php";
?>

<script type="text/javascript">
    $(document).ready(function() {
         var pos = $('#posSelect').val();
             $.ajax({
                url : my_url+"Pos/Settings/getClosingParameters/"+pos,
                type: "POST",
                dataType: 'JSON',
                data : $('#pos_settings_closing_parameters_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#form_update_modal_customer #text_additional').val(data.data[0].text_additional);
                        var values = data.data[0];
                        for (var prop in values) {
                            if(prop == "print_list_of_products_sold_on_the_day" || prop == "print_quantity_of_products_sold_in_the_day" ||
                                    prop == "print_currency_and_cell_ratio_in_the_box" || prop == "include_sales_by_tax_group" ||
                                    prop == "use_the_default_printer_instead_of_the_fiscal_printer" ||prop == "print_discount_rate_of_th_day"){
                                //checkboxes
                                if(values[prop] == "on" || values[prop] == "1"){
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            } else {
                                //inputs
                                document.getElementById(prop).value = values[prop];
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        
        $(document).on('submit', '#pos_settings_closing_parameters_form', function(e){
            var pos = $('#posSelect').val();
            e.preventDefault();
            $.ajax({
                url : my_url+"Pos/Settings/updateClosingParameters/"+pos,
                type: "POST",
                dataType: 'JSON',
                data : $('#pos_settings_closing_parameters_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        document.getElementById("pos_settings_closing_parameters_form").reset();
        getData();
    });

    function modalHelp(){
        $('#modalHelp').modal({
                show: true
            });
    }
    function teste(){
             var pos = $('#posSelect').val();
             $.ajax({
                url : my_url+"Pos/Settings/getClosingParameters/"+pos,
                type: "POST",
                dataType: 'JSON',
                data : $('#pos_settings_closing_parameters_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#form_update_modal_customer #text_additional').val(data.data[0].text_additional);
                        var values = data.data[0];
                        for (var prop in values) {
                            if(prop == "print_list_of_products_sold_on_the_day" || prop == "print_quantity_of_products_sold_in_the_day" ||
                                    prop == "print_currency_and_cell_ratio_in_the_box" || prop == "include_sales_by_tax_group" ||
                                    prop == "use_the_default_printer_instead_of_the_fiscal_printer" ||prop == "print_discount_rate_of_th_day"){
                                //checkboxes
                                if(values[prop] == "on" || values[prop] == "1"){
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            } else {
                                //inputs
                                document.getElementById(prop).value = values[prop];
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }
</script>

<style>

</style>

<div class="content">

        <div class="row row-fluid" style="margin-bottom: 60px;">
        
        <form id="pos_settings_closing_parameters_form">
            <div class="col-md-12">
                <div class="col-md-6">
                    <h4 class="translate">Text to be inserted in the cashier closing document</h4>

                    <textarea cols="5" rows="6" id="text_additional" name="text_additional" class="form-control"></textarea>
                    
                </div>
                <div class="col-md-6">
                    <p><input type="checkbox" id="print_list_of_products_sold_on_the_day" name="print_list_of_products_sold_on_the_day"> <t class="translate">Print list of products sold on the day</t></p>
                    <p><input type="checkbox" id="print_quantity_of_products_sold_in_the_day" name="print_quantity_of_products_sold_in_the_day"> <t class="translate">Print quantity of products sold in the day</t></p>
                    <p><input type="checkbox" id="print_currency_and_cell_ratio_in_the_box" name="print_currency_and_cell_ratio_in_the_box"> <t class="translate">Print currency and cell ratio in the box</t></p>
                    <p><input type="checkbox" id="include_sales_by_tax_group" name="include_sales_by_tax_group"> <t class="translate">Include Sales by Tax Group</t></p>
                    <p><input type="checkbox" id="use_the_default_printer_instead_of_the_fiscal_printer" name="use_the_default_printer_instead_of_the_fiscal_printer"> <t class="translate">Use the default printer instead of the fiscal printer</t></p>
                    <p><input type="checkbox" id="print_discount_rate_of_th_day" name="print_discount_rate_of_th_day"> <t class="translate">Print discount rate of the day</t></p>
                </div>
            </div>
        </div>

        <div class="btns-footer">
            <div class="pull-right">
                <button type="reset" class="btn btn-primary translate">Cancel</button>
                <button type="submit" class="btn btn-success translate">Save</button>
            </div>
         </form>   
             <div class="pull-left">
                <button  class="btn btn-primary translate" onclick="modalHelp()">Help</button>
            </div>
        </div>
    
    
</div>

<div class="modal fade" id="modalHelp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Help</h3>
            </div>
                <div class="modal-body">
                    <div class="row" >
                        <div class="col-md-12" >
                            <h4><b>TAGS</b><h4>
                            <ul style="list-style-type: square;">
                                <li><b style="color: red">&lt;closeCashier/&gt;</b> : Used for closer cashier;</li>
                                <li><b style="color: red">&lt;printMessage&gt;&lt;/printMessage &gt;</b> : Used for alert message;</li>
                                <li><b style="color: red">&lt;homePage/&gt;</b> : Used for return the page home;</li>
                                <li><b style="color: red">&lt;logout/&gt; </b>: Used for exit of the sistem;</li>
                                <li><b style="color: red">&lt;userName&gt;&lt;/userName &gt;</b> : Used for start POS with some user;</li>
                            </ul>  
                            <h4><b>MODE OF USE</b><h4>
                                <ul style="list-style-type: square;">
                                    <li> For use of correct form the process of TAGS is required part by <b>";".</b></li>
                                    
                            </ul>  
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    
                </div>
        </div>
    </div>
</div>