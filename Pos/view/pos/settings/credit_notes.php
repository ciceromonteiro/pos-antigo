<?php 
	$nav = "Credit Notes";
	include "nav.php";
?>


<script type="text/javascript">
    $(document).ready(function() {
        //var idPos = $('#posSelect').val();
        function getData(){
            var idPos = $('#posSelect').val();
            $.ajax({
                url : my_url+"Pos/Settings/getCreditNotes/"+idPos,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        var values = data.data[0];
                        for (var prop in values) {
                            if(prop == "print_the_status_of_use_the_card"){
                                //checkboxes
                                if(values[prop] == "on" || values[prop] == "1"){
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            } else {
                                //inputs
                                document.getElementById(prop).value = values[prop];
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }
        
        $(document).on('submit', '#pos_settings_credit_notes_form', function(e){
            var pos = $('#posSelect').val();
            e.preventDefault();
            $.ajax({
                url : my_url+"Pos/Settings/updateCreditNotes/"+pos,
                type: "POST",
                dataType: 'JSON',
                data : $('#pos_settings_credit_notes_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        document.getElementById("pos_settings_credit_notes_form").reset();
        getData();
    });

    function creditNotes(){
            var pos = $('#posSelect').val();
            $.ajax({
                url : my_url+"Pos/Settings/getCreditNotes/"+pos,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        var values = data.data[0];
                        for (var prop in values) {
                            if(prop == "print_the_status_of_use_the_card"){
                                //checkboxes
                                if(values[prop] == "on" || values[prop] == "1"){
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            } else {
                                //inputs
                                document.getElementById(prop).value = values[prop];
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }
</script>

<div class="content">
    <form id="pos_settings_credit_notes_form">
        <div class="row row-fluid" style="margin-bottom: 60px;">
            <div class="col-md-12">
                <div class="col-md-6">
                    <h4 class="translate">Text</h4>
                    <textarea cols="5" rows="6" name="text_additional" id="text_additional" placeholder="Text to be inserted in the cashier closing document" class="form-control"></textarea><br>
                    <label for="number_of_ways_to_print" class="translate">Number of ways to print</label>
                    <input type="number" name="number_of_ways_to_print" id="number_of_ways_to_print" class="form-control"><br>
                    <!--<p><input type="checkbox" name="print_the_receipt_status_on_the_printer" id="print_the_receipt_status_on_the_printer"> Print the receipt status on the printer</p>-->
                </div>
                <div class="col-md-6">
                    <h4 class="translate">GiftCard</h4>
                    <textarea cols="5" rows="6" id="gift_card_text_additional" name="gift_card_text_additional" placeholder="Text to be inserted in the cashier closing document" class="form-control"></textarea><br>
                    <p><input type="checkbox" id="print_the_status_of_use_the_card" name="print_the_status_of_use_the_card"> <t class="translate">Print the status of use of the card present in the tax printer</t></p>
                </div>
            </div>
        </div>

        <div class="btns-footer">
            <div class="pull-right">
                <button class="btn btn-primary translate">Cancel</button>
                <button type="submit" class="btn btn-success translate">Save</button>
            </div>
        </div>
    </form>
</div>
