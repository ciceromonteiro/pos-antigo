<?php
/* Created by PhpStorm.
 * User: rocha
 * Date: 10/24/16
 * Time: 2:39 PM
 */
$menuLeft = $navbar;
$menu = explode('|', $menuLeft);
?>
<div class="horizon-swiper" <?php echo ($play == "layout" || $play == "tables" || $play == "build") ? 'style="margin-top:100px"' : '' ?>>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Pos/settings/index' ?>">
            <div class='<?php echo ($menu[1] == "Settings") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-cog"></span>
                <t class="translate">Settings</t>
            </div>
        </a>
    </div>
    <!--
        <div class="horizon-item">
            <a href="<?php// echo URL_BASE.'Pos/layout/listTemplate' ?>">
                <div class='<?php// echo ($menu[1] == "layout") ? "nav-btn active" : 'nav-btn' ?>'>
                    <span class="lnr lnr-layers"></span>
                    <t class="translate">Layout</t>
                </div>
            </a>
        </div>
        <div class="horizon-item">
            <a href="<?php// echo URL_BASE.'Pos/tables/index' ?>">
                <div class='<?php// echo ($menu[1] == "Tables") ? "nav-btn active" : 'nav-btn' ?>'>
                    <span class="lnr lnr-pie-chart"></span>
                    <t class="translate">Tables</t>        
                </div>
            </a>
        </div>
        <div class="horizon-item">
            <a href="<?php// echo URL_BASE.'Pos/layout/build' ?>">
                <div class='<?php// echo ($menu[1] == "build") ? "nav-btn active" : 'nav-btn' ?>'>
                    <span class="lnr lnr-pushpin"></span>
                    <t class="translate">Build Template</t>
                </div>
            </a>
        </div>
    -->
    <?php if ($play == "layout" || $play == "tables"): ?>
    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'dashboard/home/index' ?>">
            <div class='nav-btn' style="color: green">
                <span class="lnr lnr-chevron-left" style="color: green"></span>
                <t class="translate">Back to Admin</t>
            </div>
        </a>
    </div>
    <?php endif; ?>
</div>

<script type="text/javascript">
    $('.horizon-swiper').horizonSwiper();
</script>
