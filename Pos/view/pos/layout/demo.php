<style>
    .btn {
        font-size: 12px;
    }
    .btn p {
        padding: 0px;
        margin: 0px;
    }
    .dropHere {
        height: 768px;
        width: 1024px;
        border: 2px solid red
    }
    .btn-pos {
        width: 85px;
        height: 75px;
    }
    #get_parked_order, #pay, #extra_functions, #order_info, #open_credit_order, 
    #line_info, #discount, #cancel_order, #delete_line, #customer-widget, 
    #employee-widget, #order-info-widget, #last-sale-widget, 
    #table-widget, #keyboard, #image-widget, #tabs-widget {
        border: 2px solid red;
    }
</style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function () {
        $("#get_parked_order").draggable();
        $("#pay").draggable();
        $("#extra_functions").draggable();
        $("#order_info").draggable();
        $("#open_credit_order").draggable();
        $("#line_info").draggable();
        $("#discount").draggable();
        $("#cancel_order").draggable();
        $("#delete_line").draggable();
        $("#customer-widget").draggable();
        $("#employee-widget").draggable();
        $("#order-info-widget").draggable();
        $("#last-sale-widget").draggable();
        $("#table-widget").draggable();
        $("#keyboard").draggable();
        $("#image-widget").draggable();
        $("#tabs-widget").draggable();
        $("#return").draggable();
        $("#kvittering").draggable();
        $("#lag_nytt_gavekort").draggable();
        
        $("#cac0-widget").draggable();
        $("#cac1-widget").draggable();
        $("#cac2-widget").draggable();
        $("#cac3-widget").draggable();
        $("#cac4-widget").draggable();
        $("#cac5-widget").draggable();
        $("#cac6-widget").draggable();
        $("#cac7-widget").draggable();
        $("#cac8-widget").draggable();
        $("#cac9-widget").draggable();
        $("#cac10-widget").draggable();
        $("#cac11-widget").draggable();
        $("#cac12-widget").draggable();
        $("#cac13-widget").draggable();
        $("#cac14-widget").draggable();
        $("#cac15-widget").draggable();
        $("#cac16-widget").draggable();
        
        
        $("#order-info-widget").resizable();
        $("#last-sale-widget").resizable();
        $("#table-widget").resizable();
        $("#image-widget").resizable();
        $("#tabs-widget").resizable();
    });
</script>

<div class="dropHere">
    
    <!-- Customer Widget -->
    <div id='customer-widget' style='width:164px; height: 96px' class='customer-widget bordered-ui ui-draggable ui-draggable-dragging'>
        <input type='text' readonly='' placeholder='Customer' class='form-control display_customer_name'><br>
        <input type='button' class='btn btn-primary change_customer' value='Change'>
    </div>
    
    <!-- Employee Widget -->
    <div id='employee-widget' style='width:164px; height:96px' class='customer-widget bordered-ui ui-draggable ui-draggable-dragging'>
        <input type='text' readonly='' placeholder='Employee' class='form-control display_user_name'><br>
        <input type='button' class='btn btn-primary change_user' value='Change'>
    </div>
    
    <!-- Order Info -->
    <div id='order-info-widget' style='width:164px; height: 96px; background-color: #EDECEC; border-radius: 5px' class='order-info-widget bordered-ui ui-draggable ui-draggable-dragging'>
        <h4>Order</h4><p style='padding-left:15px'>Total: <span class='total_order'>0.00</span></p>
    </div>
    
    <!-- Last Sale Widget -->
    <div id='last-sale-widget' style='width:164px; height: 96px; background-color: #EDECEC; border-radius: 5px' class='last-sale-widget bordered-ui ui-draggable ui-draggable-dragging'>
        <h4>Last sale</h4><p style='padding-left:15px'>Total: <span class='total_order'>0.00</span></p>
    </div>
    
    <!-- Table Widget -->
    <div id='table-widget' class='table-widget bordered-ui ui-draggable ui-draggable-dragging' style='background-color: rgb(237, 236, 236); width:571px; height:271px'>
        <div style='width: 100%;'>
            <div class='input-group'>
                <input type='text' class='form-control insert_barcode' placeholder='Search for...'>
                <span class='input-group-btn'>
                    <button class='btn btn-default new_product' type='button'>Add</button>
                </span>
            </div>
        </div>
        <table id='color' class='table-bordered pos_table' cellspacing='0' width='100%' style='margin: 0px;'>
            <thead>
            <tr>
                <th class='text-center' style='width: 10px'><input type='checkbox' name='all' onclick='markAll('color')'></th>
                <th style='width: 10px'>ID</th>
                <th>Product No</th>
                <th>Description</th>
                <th>Qty</th>
                <th>Un Price</th>
                <th>Discount</th>
                <th>Price</th>
                <th>Info</th>
            </tr>
            </thead>
        <tbody>
            <tr class='new_product'>
                <td></td>
                <td></td>
                <td class='product_no'></td>
                <td class='product_description'></td>
                <td></td>
                <td class='product_price'></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
        </table>
    </div>
    
    <!-- Parked Order -->
    <div id="get_parked_order" class="button_get_parked_order ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Parked<br> order</p>
    </div>
    
    <!-- Pay -->
    <div id="pay" class="button_pay ui-widget-content btn btn-primary btn-pos" style="height: 134px; width: 60px;">
        <p>Pay</p>
    </div>
    
    <!-- Extra functions -->
    <a data-toggle="tab" href="#extraFunctions" aria-expanded="true" style="color:black">
        <div id="extra_functions" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
            <p>Extra<br> functions</p>
        </div>
    </a>
    
    <!-- Order info -->
    <div id="order_info" class="button_order_info ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Order<br> info</p>
    </div>
    
    <!-- Credit order -->
    <div id="open_credit_order" class="button_order ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Credit<br> order</p>
    </div>
    
    <!-- Line info -->
    <div id="line_info" class="button_line_info ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Line info</p>
    </div>
    
    <!-- Discount -->
    <div id="discount" class="button_discount ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Discount</p>
    </div>
    
    <!-- Delete line -->
    <div id="delete_line" class="button_delete_line ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Delete<br> line</p>
    </div>
    
    <!-- Cancel Order -->
    <div id="cancel_order" class="button_cancel_order ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Cancel<br> order</p>
    </div>
    
    <!-- Keyboard -->
    <div id="keyboard" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <span class="lnr lnr-keyboard" style="font-size: 38px"></span>
    </div>
    
    <!-- Return -->
    <div id="return" class="button_return ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Return</p>
    </div>
    
    <!-- Kvittering -->
    <div id="kvittering" class="button_kvittering ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Kvittering</p>
    </div>
    
    <!-- Lag Nytt Gavekort -->
    <div id="lag_nytt_gavekort" class="button_lag_nytt_gavekort ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>Lag Nytt Gavekort</p>
    </div>
    
    <!-- Image -->
    <div id="image-widget" style="width: 150px; height: 150px; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAkFBMVEWwCAi+DAz/1gD/2gC8AAz/2AD/3ACtAAj/3wC6AA2wAAjFXgb/4QCqAAizCQm3AA3ckATKSQrSeAXdhAjUgAayGgjlpAT0vAT6ywK8PAfLZwbAGgzAUAf0wATXcgn5zgLEMwvaiAbtrQXstQPJQgvOVAq4LwfCKQvimgTmrgS6RAfOcQbTYwqxJgjllgfFWAcOz0ZxAAAH3klEQVR4nO2Za3eiOhSGKySBUBBrW6Raq3irrR39///ucMkNSOLGOuucdVbeLzNqSh73PfFh9N/Uw78NYJDjGibHNUyOa5gc1zA5rmFyXMPkuIbJcQ2T4xomxzVMjmuYHNcwOa5hclzD5LiGyXEN0y1cqBRwGWSdVoO5EB3tf/Y7Sq+uW+8v+wW9kWwgV7lblq88b7WcWm2B0Mc29zwv3+xuIxvGhUZHQnAlP84/zCajP8vYr9eROPu8BWwQF1pvQ+wx+f7ZZDL6jH2+zAuXT9d8/ksutPOIpyje6sFoJumrL0BuABvAhdZLwjbym33DTMdFz2H9KS7XNetXT4NdCedCqMHC4WaS5YSB9S1Bi5DRZMWm+QK+txgKBuei57jejzwjSrnpwl7w0z1pWPIdoujDq1+QzV/jQh+NU+KPtHq1mNUvsddxEfpc1SQ4r9OQ/jRfJnweGGJgLprXIGSWNvuvG2+RbXtDdGSG/Gl46bFeh8loiMXGYygXC2a82rHnp8+E2U8Fo3vm7GPKOD/zwZ4cP5SCLUW7xj1EZCBa56rD+LvLrnvpM8vOPRCspoJy0YxZRyYWZQYjZ7kh4u8dpRFTlpNLQIRRBgXlQp/MPdtUeZcZAkvWRROEHlGygZez+KrBxpIKyEU3zA4/yrPTY9c49LnB92cKPnpiGTqzc6lQQC4eXXi5VmPp0nDhFY8w1IRcmQsqApo1f+xdzGDjDhWQi7nCP7ZihNUwj0yaDdG0eY1zFX9Ep6ykbEwR1oOCcq1wqyjxDZkj/Zy9sWGc7ZrAg9MLtd2I6qhAXOijeTD2OzV0z8aLuOZFT2x/f9reP+XZcO4bTA8F46LcX61srEjYlOVvqg/SM8PE6zaXGDCWiw6UkQrChfYrXThXJCxN6zKKFmwZzrv4P4wfz9UHWKBgXM98924FoszB1YwhGnvfXWjH8lQpt4aoGuTHJUuzZXdSR08Ys4K1GKGMR1uvgKItewJGEFPBuNCah3Ov9ZYTLBuYwzUfcJTWLkTPKrItqgZw8fmzdFYvnVDGfBcW6Q/H3y56j+CfkU0EYYJxzfje/QZHC56Ry5S7kRx7y9CIceH8FQp2DYu3t3LDflUUFax0Hl/WrV6VUv6hNw/uxcXTrJf+tSHEwS1bsf/oBi1eUDxSJPfiEv7RnH3KUs4NIf/VNJuUx6i/vBcXr5ZePNVxbVon3SbSuovG4+CRL4uhAXbNXDsWsl6oG1P4lCwlJnsBVW4SvYtvBw2wK1xiY6w9NCNeAYRaJzJe1aMDL3QkAzryCle65WHfq/Y11xPucCnVnspdIvmYCOZIOxZa8MD2t9rP13kbDPs7Xa9JeAHGq2+YI69w7Xmi+f1yWYnPrIKrmVW7rSYpeODj0124pvx5fqGdgkUn4svKLqSZFYK5SJ/JXbiOnIvMtfai5zYXybThExxiAX4PLukm0psSmEE79jKYI+FceJWAAt9urk/MwzrUH7LELMq5XgxcoiHEB5DB7FyiqmKv3x2bFZ24N9TNZMnXASurnWsqwqLfXphwu1AYykCSiUA9giqrlYvKsM8MXKmncmHvjz56Et65y5n791wpbx/asx9bojqyjGr9NsGJc+H8ARL4Vq6RmOdC3TRRapy0Jgqcm7jERAGs+DYqOat2rwA41fghOapc/tbAFV14HGLv8ddccxE7RDPc12U9eFEnHeO4EH2Lr0imv+YSRbN37yw6oOwwtVkLw57Rm2jwBNSJrFxnERT5znCEDy4tLpMtoj8yhUCHNRvXSMR0a/pqteXoW83H2Bw7fAIrSyEkIW1YsjtWB30NVM2lFrD43cSVfMmYgMz4Nq5PEav+poHqTzBK4FTr3kxbJnIgAp09bOH1KVKtupvQX8FEB4ULe2auiagnMaSA2bhE1y6n1dRw21EGtAwwvDoYuQrJBencNi552PEnpqYWvW4VrtzQHksu0YjKYgLokBYu5XBoPr9HD1+Sy18auYK5fBjkrGbjEuXLIyfjo4JMNiJ/awzp4F0a39SsgFzKIZ+YQyJRub6MpSl4E1wYcklh4Ypmsj1eQFwkM+4TvUquHHC2NVKVo4KsAMSc2snEV7iMy6JAxJcla69xVbUqilaSy3xYSAqFyzIjJ0RyAQqY1lR1rYoOCpe5RCcvKpd5Q4XLM0eFmUuU9UDpfL45ItpcFnspfRRQWLWmargu8hIQm/cLTgqXsfyWXLlcB7ij0Jqq2VBOq9izcM2Vumo41dZc8oaFWJb1ubrDQnCCcT3+TS7a/yR4EZHqm4451bKLjBvfMronW1mlTdN2h4tqfxoJ5ARgu0YGc8n6a0tbyaUxVY/LckYO3odzmcuvkPlHpEBOcrZOG7z7MK7jIC6zguNgLtNtTpfr6xdYYK43DOOayKl8C7qiMChSA+IOXIXkmv2G6+EvcoF/7tNJGQstfS94C0PSKAxtXPIiw19WXEFyU/RH8oRs5YoOEynLABNMJVf182gyn0yDG8wG5SonPkXmZQpX/bPtZPo9P97gT/UABrwTtarD9V28nLIT6Cqsy6U0Wsv8cgvX6jX4nr7MindAA9dx4UZeeA+uU+xh/sDX6K2YXooX4zWLjWuL72qvkzw3rV6j4FR8n4qbysX7o5DxOgSu6CAf91i+Tt5O77d9WyXNfo/Vzlv2+g5PdXJycnJycnJycnJycnJycnJycvrf6x+h0J2r/WsjDQAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-size: contain; z-index: 2; left: 8px; top: 10px;" class="bordered-ui item-1 imgSize-1 ui-draggable ui-draggable-handle ui-resizable ui-resizable-autohide" data-itam-id="1"> <input id="inputFileToLoad" onchange="encodeImageFileAsURL();" style="display: none" type="file"> <div class="ui-resizable-handle ui-resizable-n" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-sw" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-ne" style="z-index: 90; display: none;"></div><div class="ui-resizable-handle ui-resizable-nw" style="z-index: 90; display: none;"></div></div>
    
    
    
    <!-- Calc -->
    <div id="cac0-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 122px;">
        <p>0</p>
    </div>
    
    <div id="cac1-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>1</p>
    </div>
    
    <div id="cac2-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>2</p>
    </div>
    
    <div id="cac3-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>3</p>
    </div>
    
    <div id="cac4-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>4</p>
    </div>
    
    <div id="cac5-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>5</p>
    </div>
    
    <div id="cac6-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>6</p>
    </div>
    
    <div id="cac7-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>7</p>
    </div>
    
    <div id="cac8-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>8</p>
    </div>
    
    <div id="cac9-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>9</p>
    </div>
    
    <div id="cac10-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>%</p>
    </div>
    
    <div id="cac11-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>*</p>
    </div>
    
    <div id="cac12-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>-</p>
    </div>
    
    <div id="cac13-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>+</p>
    </div>
    
    <div id="cac14-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p>,</p>
    </div>
    
    <div id="cac15-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 183px; width: 65px;">
        <p>Ok</p>
    </div>
    
    <div id="cac16-widget" class="ui-widget-content btn btn-primary btn-pos" style="height: 61px; width: 65px;">
        <p><=</p>
    </div>
    
    <div id="tabs-widget">
        <ul class="nav nav-tabs">
            <li class="active">
                <a data-toggle="tab" href="#products" aria-expanded="false">Products</a>
            </li> 
            <li id="tab-extraFunctions">
                <a data-toggle="tab" href="#extraFunctions" aria-expanded="true">Extra Functions</a>
            </li>
        </ul> 
        <div class="tab-content" data-tab-id="6"> 
            <div id="products" class="tab-pane active in"> 
                <div style="height:150px;"> 
                </div> 
            </div> 
            <div id="extraFunctions" class="tab-pane fade"> 
                <div style="height:150px;"> 
                    <div class="btn btn-primary button_withdrawal" style="margin: 5px;">With-drawal / Deposit</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Open Cash Drawer</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Print Receipt Copy</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Cash Statistics</div> 
                    <div class="btn btn-primary button_credit_last_sale" style="margin: 5px;">Credit Last Sale</div> 
                    <div class="btn btn-primary button_print_temp_receipt" style="margin: 5px;">Print Temp. Receipt</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Settle Invoice By Cash</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Pre Payment</div> 
                    <div class="btn btn-primary button_split_order" style="margin: 5px;">Split Order</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Check Giftcard Status</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Check Electr. Gift Card</div> 
                    <div class="btn btn-primary button_customer_orders" style="margin: 5px;">Customer Orders</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Customer Statistics</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Customer Updating</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Article Statistics</div> 
                    <div class="btn btn-primary" style="margin: 5px;">Article Updating</div> 
                </div> 
            </div> 
        </div>             
    </div>
</div>