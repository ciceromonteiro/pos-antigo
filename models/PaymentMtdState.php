<?php



use Doctrine\Mapping as ORM;

/**
 * PaymentMtdState
 *
 * @Table(name="payment_mtd_state", indexes={@Index(name="fk_payment_mtd_state_payment_mtd1_idx", columns={"payment_mtd_idpayment_mtd"}), @Index(name="fk_payment_mtd_state_zz_state1_idx", columns={"zz_state_idzz_state"})})
 * @Entity
 */
class PaymentMtdState
{
    /**
     * @var integer
     *
     * @Column(name="idpayment_mtd_state", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpaymentMtdState;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \PaymentMtd
     *
     * @ManyToOne(targetEntity="PaymentMtd")
     * @JoinColumns({
     *   @JoinColumn(name="payment_mtd_idpayment_mtd", referencedColumnName="idpayment_mtd")
     * })
     */
    private $paymentMtdpaymentMtd;

    /**
     * @var \ZzState
     *
     * @ManyToOne(targetEntity="ZzState")
     * @JoinColumns({
     *   @JoinColumn(name="zz_state_idzz_state", referencedColumnName="idzz_state")
     * })
     */
    private $zzStatezzState;



    /**
     * Get idpaymentMtdState
     *
     * @return integer
     */
    public function getIdpaymentMtdState()
    {
        return $this->idpaymentMtdState;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return PaymentMtdState
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return PaymentMtdState
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return PaymentMtdState
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PaymentMtdState
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set paymentMtdpaymentMtd
     *
     * @param \PaymentMtd $paymentMtdpaymentMtd
     *
     * @return PaymentMtdState
     */
    public function setPaymentMtdpaymentMtd(\PaymentMtd $paymentMtdpaymentMtd = null)
    {
        $this->paymentMtdpaymentMtd = $paymentMtdpaymentMtd;

        return $this;
    }

    /**
     * Get paymentMtdpaymentMtd
     *
     * @return \PaymentMtd
     */
    public function getPaymentMtdpaymentMtd()
    {
        return $this->paymentMtdpaymentMtd;
    }

    /**
     * Set zzStatezzState
     *
     * @param \ZzState $zzStatezzState
     *
     * @return PaymentMtdState
     */
    public function setZzStatezzState(\ZzState $zzStatezzState = null)
    {
        $this->zzStatezzState = $zzStatezzState;

        return $this;
    }

    /**
     * Get zzStatezzState
     *
     * @return \ZzState
     */
    public function getZzStatezzState()
    {
        return $this->zzStatezzState;
    }
}
