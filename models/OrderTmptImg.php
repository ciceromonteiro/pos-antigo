<?php



use Doctrine\Mapping as ORM;

/**
 * OrderTmptImg
 *
 * @Table(name="order_tmpt_img", indexes={@Index(name="idordertmpt_indx", columns={"idordertmpt"})})
 * @Entity
 */
class OrderTmptImg
{
    /**
     * @var integer
     *
     * @Column(name="idordertmptimg", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idordertmptimg;
    
    /**
     * @var text
     *
     * @Column(name="`image`", type="text", length=16777215, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @Column(name="image_type", type="string", length=10, nullable=false)
     */
    private $imageType;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \OrderTmpt
     *
     * @ManyToOne(targetEntity="OrderTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="idordertmpt", referencedColumnName="idordertmpt")
     * })
     */
    private $idordertmpt;

    function getIdordertmptimg() {
        return $this->idordertmptimg;
    }

    function getImage() {
        return $this->image;
    }

    function getImageType() {
        return $this->imageType;
    }

    function getActive() {
        return $this->active;
    }

    function getIdordertmpt() {
        return $this->idordertmpt;
    }

    function setIdordertmptimg($idordertmptimg) {
        $this->idordertmptimg = $idordertmptimg;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setImageType($imageType) {
        $this->imageType = $imageType;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdordertmpt(\OrderTmpt $idordertmpt) {
        $this->idordertmpt = $idordertmpt;
    }

}

