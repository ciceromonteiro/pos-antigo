<?php



use Doctrine\Mapping as ORM;

/**
 * SettingsFactoring
 *
 * @Table(name="settings_factoring", indexes={@Index(name="company_factoring", columns={"company_factoring"})})
 * @Entity
 */
class SettingsFactoring
{
    /**
     * @var integer
     *
     * @Column(name="idsettingsfactoring", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsettingsfactoring;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="address", type="string", length=100, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @Column(name="cod_postal", type="string", length=100, nullable=false)
     */
    private $codPostal;

    /**
     * @var string
     *
     * @Column(name="invoice_text", type="text", length=65535, nullable=false)
     */
    private $invoiceText;

    /**
     * @var string
     *
     * @Column(name="bank_account", type="string", length=50, nullable=false)
     */
    private $bankAccount;

    /**
     * @var string
     *
     * @Column(name="kid_layout", type="string", length=30, nullable=false)
     */
    private $kidLayout;

    /**
     * @var integer
     *
     * @Column(name="client_id", type="integer", nullable=false)
     */
    private $clientId;

    /**
     * @var string
     *
     * @Column(name="email", type="string", length=80, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @Column(name="add_invoice_text", type="text", length=65535, nullable=true)
     */
    private $addInvoiceText;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \CompanyFactoring
     *
     * @ManyToOne(targetEntity="CompanyFactoring")
     * @JoinColumns({
     *   @JoinColumn(name="company_factoring", referencedColumnName="idcompanyfactoring")
     * })
     */
    private $companyFactoring;

    function getIdsettingsfactoring() {
        return $this->idsettingsfactoring;
    }

    function getName() {
        return $this->name;
    }

    function getAddress() {
        return $this->address;
    }

    function getCodPostal() {
        return $this->codPostal;
    }

    function getInvoiceText() {
        return $this->invoiceText;
    }

    function getBankAccount() {
        return $this->bankAccount;
    }

    function getKidLayout() {
        return $this->kidLayout;
    }

    function getClientId() {
        return $this->clientId;
    }

    function getEmail() {
        return $this->email;
    }

    function getAddInvoiceText() {
        return $this->addInvoiceText;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getCompanyFactoring() {
        return $this->companyFactoring;
    }

    function setIdsettingsfactoring($idsettingsfactoring) {
        $this->idsettingsfactoring = $idsettingsfactoring;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    function setCodPostal($codPostal) {
        $this->codPostal = $codPostal;
    }

    function setInvoiceText($invoiceText) {
        $this->invoiceText = $invoiceText;
    }

    function setBankAccount($bankAccount) {
        $this->bankAccount = $bankAccount;
    }

    function setKidLayout($kidLayout) {
        $this->kidLayout = $kidLayout;
    }

    function setClientId($clientId) {
        $this->clientId = $clientId;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setAddInvoiceText($addInvoiceText) {
        $this->addInvoiceText = $addInvoiceText;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setCompanyFactoring(\CompanyFactoring $companyFactoring) {
        $this->companyFactoring = $companyFactoring;
    }


}

