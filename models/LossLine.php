<?php



use Doctrine\Mapping as ORM;

/**
 * LossLine
 *
 * @Table(name="loss_line", indexes={@Index(name="fk_loss_line_loss1_idx", columns={"loss_idloss"}), @Index(name="fk_loss_line_product1_idx", columns={"product_idproduct"})})
 * @Entity
 */
class LossLine
{
    /**
     * @var integer
     *
     * @Column(name="idloss_line", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlossLine;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Loss
     *
     * @ManyToOne(targetEntity="Loss")
     * @JoinColumns({
     *   @JoinColumn(name="loss_idloss", referencedColumnName="idloss")
     * })
     */
    private $lossloss;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;



    /**
     * Get idlossLine
     *
     * @return integer
     */
    public function getIdlossLine()
    {
        return $this->idlossLine;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return LossLine
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return LossLine
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return LossLine
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return LossLine
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return LossLine
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set lossloss
     *
     * @param \Loss $lossloss
     *
     * @return LossLine
     */
    public function setLossloss(\Loss $lossloss = null)
    {
        $this->lossloss = $lossloss;

        return $this;
    }

    /**
     * Get lossloss
     *
     * @return \Loss
     */
    public function getLossloss()
    {
        return $this->lossloss;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return LossLine
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }
}
