<?php



use Doctrine\Mapping as ORM;

/**
 * CombinedSaleProduct
 *
 * @Table(name="combined_sale_product", indexes={@Index(name="fk_combined_sale_product_product1_idx", columns={"product_idproduct"}), @Index(name="fk_combined_sale_product_combined_sale1_idx", columns={"combined_sale_idcombined_sale"})})
 * @Entity
 */
class CombinedSaleProduct
{
    /**
     * @var integer
     *
     * @Column(name="idcombined_sale_product", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcombinedSaleProduct;

    /**
     * @var integer
     *
     * @Column(name="qty", type="integer", nullable=false)
     */
    private $qty;

    /**
     * @var \CombinedSale
     *
     * @ManyToOne(targetEntity="CombinedSale")
     * @JoinColumns({
     *   @JoinColumn(name="combined_sale_idcombined_sale", referencedColumnName="idcombined_sale")
     * })
     */
    private $combinedSalecombinedSale;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;



    /**
     * Get idcombinedSaleProduct
     *
     * @return integer
     */
    public function getIdcombinedSaleProduct()
    {
        return $this->idcombinedSaleProduct;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     *
     * @return CombinedSaleProduct
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return integer
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set combinedSalecombinedSale
     *
     * @param \CombinedSale $combinedSalecombinedSale
     *
     * @return CombinedSaleProduct
     */
    public function setCombinedSalecombinedSale(\CombinedSale $combinedSalecombinedSale = null)
    {
        $this->combinedSalecombinedSale = $combinedSalecombinedSale;

        return $this;
    }

    /**
     * Get combinedSalecombinedSale
     *
     * @return \CombinedSale
     */
    public function getCombinedSalecombinedSale()
    {
        return $this->combinedSalecombinedSale;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return CombinedSaleProduct
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }
}
