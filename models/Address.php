<?php



use Doctrine\Mapping as ORM;

/**
 * Address
 *
 * @Table(name="address", indexes={@Index(name="fk_address_zz_state1_idx", columns={"zz_state_idzz_state"}), @Index(name="fk_address_person1_idx", columns={"person_idperson"}), @Index(name="fk_address_company1_idx", columns={"company_idcompany"})})
 * @Entity
 */
class Address
{
    /**
     * @var integer
     *
     * @Column(name="idaddress", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idaddress;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @Column(name="visiting_address", type="boolean", nullable=false)
     */
    private $visitingAddress;

    /**
     * @var string
     *
     * @Column(name="address_street", type="string", length=100, nullable=false)
     */
    private $addressStreet;

    /**
     * @var string
     *
     * @Column(name="address_number", type="string", length=14, nullable=true)
     */
    private $addressNumber;

    /**
     * @var string
     *
     * @Column(name="address_district", type="string", length=45, nullable=false)
     */
    private $addressDistrict;

    /**
     * @var string
     *
     * @Column(name="address_city", type="string", length=100, nullable=false)
     */
    private $addressCity;

    /**
     * @var string
     *
     * @Column(name="address_zipcode", type="string", length=45, nullable=false)
     */
    private $addressZipcode;

    /**
     * @var string
     *
     * @Column(name="address_complement", type="string", length=200, nullable=true)
     */
    private $addressComplement;

    /**
     * @var string
     *
     * @Column(name="address_reference", type="string", length=200, nullable=true)
     */
    private $addressReference;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;

    /**
     * @var \ZzState
     *
     * @ManyToOne(targetEntity="ZzState")
     * @JoinColumns({
     *   @JoinColumn(name="zz_state_idzz_state", referencedColumnName="idzz_state")
     * })
     */
    private $zzStatezzState;

    /**
     * @return int
     */
    public function getIdaddress()
    {
        return $this->idaddress;
    }

    /**
     * @param int $idaddress
     */
    public function setIdaddress($idaddress)
    {
        $this->idaddress = $idaddress;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function isVisitingAddress()
    {
        return $this->visitingAddress;
    }

    /**
     * @param boolean $visitingAddress
     */
    public function setVisitingAddress($visitingAddress)
    {
        $this->visitingAddress = $visitingAddress;
    }

    /**
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * @param string $addressStreet
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;
    }

    /**
     * @return string
     */
    public function getAddressNumber()
    {
        return $this->addressNumber;
    }

    /**
     * @param string $addressNumber
     */
    public function setAddressNumber($addressNumber)
    {
        $this->addressNumber = $addressNumber;
    }

    /**
     * @return string
     */
    public function getAddressDistrict()
    {
        return $this->addressDistrict;
    }

    /**
     * @param string $addressDistrict
     */
    public function setAddressDistrict($addressDistrict)
    {
        $this->addressDistrict = $addressDistrict;
    }

    /**
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * @param string $addressCity
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;
    }

    /**
     * @return string
     */
    public function getAddressZipcode()
    {
        return $this->addressZipcode;
    }

    /**
     * @param string $addressZipcode
     */
    public function setAddressZipcode($addressZipcode)
    {
        $this->addressZipcode = $addressZipcode;
    }

    /**
     * @return string
     */
    public function getAddressComplement()
    {
        return $this->addressComplement;
    }

    /**
     * @param string $addressComplement
     */
    public function setAddressComplement($addressComplement)
    {
        $this->addressComplement = $addressComplement;
    }

    /**
     * @return string
     */
    public function getAddressReference()
    {
        return $this->addressReference;
    }

    /**
     * @param string $addressReference
     */
    public function setAddressReference($addressReference)
    {
        $this->addressReference = $addressReference;
    }

    /**
     * @return DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * @param DateTime $dateCreate
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    /**
     * @return DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @param DateTime $dateUpdate
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    }

    /**
     * @return DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * @param DateTime $dateDelete
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return Company
     */
    public function getCompanycompany()
    {
        return $this->companycompany;
    }

    /**
     * @param Company $companycompany
     */
    public function setCompanycompany($companycompany)
    {
        $this->companycompany = $companycompany;
    }

    /**
     * @return Person
     */
    public function getPersonperson()
    {
        return $this->personperson;
    }

    /**
     * @param Person $personperson
     */
    public function setPersonperson($personperson)
    {
        $this->personperson = $personperson;
    }

    /**
     * @return ZzState
     */
    public function getZzStatezzState()
    {
        return $this->zzStatezzState;
    }

    /**
     * @param ZzState $zzStatezzState
     */
    public function setZzStatezzState($zzStatezzState)
    {
        $this->zzStatezzState = $zzStatezzState;
    }



    /**
     * Get visitingAddress
     *
     * @return boolean
     */
    public function getVisitingAddress()
    {
        return $this->visitingAddress;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
    
