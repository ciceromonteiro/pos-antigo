<?php

use Doctrine\Mapping as ORM;

/**
 * Order
 *
 * @Table(name="`order`", indexes={@Index(name="fk_order_person1_idx", columns={"person_idperson"}), @Index(name="fk_order_company1_idx", columns={"company_idcompany"}), @Index(name="fk_order_shipping_mtd1_idx", columns={"shipping_mtd_idshipping_mtd"}), @Index(name="fk_order_users1_idx", columns={"users_idusers"}), @Index(name="fk_order_webshop1_idx", columns={"webshop_idwebshop"}), @Index(name="fk_order_pos_session1_idx", columns={"pos_session_idpos_session"}), @Index(name="fk_order_address1_idx", columns={"address_idaddress"})})
 * @Entity
 */
class Order {

    /**
     * @var integer
     *
     * @Column(name="idorder", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idorder;

    /**
     * @var \DateTime
     *
     * @Column(name="date_shipped", type="datetime", nullable=true)
     */
    private $dateShipped;

    /**
     * @var string
     *
     * @Column(name="tracking_code", type="string", length=60, nullable=true)
     */
    private $trackingCode;

    /**
     * @var \DateTime
     *
     * @Column(name="date_cancelled", type="datetime", nullable=true)
     */
    private $dateCancelled;

    /**
     * @var boolean
     *
     * @Column(name="in_use", type="boolean", nullable=false)
     */
    private $inUse = 1;

    /**
     * @var string
     *
     * @Column(name="tip_value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $tipValue = 0.00;

    /**
     * @var string
     *
     * @Column(name="value_shipping", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $valueShipping = 0.00;

    /**
     * @var string
     *
     * @Column(name="discount", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $discount = 0.00;

    /**
     * @var string
     *
     * @Column(name="discount_reason", type="text", nullable=true)
     */
    private $discountReason;

    /**
     * @var string
     *
     * @Column(name="order_identify", type="string", length=45, nullable=true)
     */
    private $order_identify;

    /**
     * @var integer
     *
     * @Column(name="status", type="integer", nullable=false)
     */
    private $status = 0;

    /**
     * @var integer
     *
     * @Column(name="order_type", type="integer", nullable=false)
     */
    private $orderType;

    /**
     * @var integer
     *
     * @Column(name="RefundNote", type="integer", nullable=false)
     */
    private $refundNote;

    /**
     * @var string
     *
     * @Column(name="total_amount", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $totalAmount;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var integer
     *
     * @Column(name="mobile", type="integer", nullable=false)
     */
    private $mobile;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo = "";

    /**
     * @var \Address
     *
     * @ManyToOne(targetEntity="Address")
     * @JoinColumns({
     *   @JoinColumn(name="address_idaddress", referencedColumnName="idaddress")
     * })
     */
    private $addressaddress;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;

    /**
     * @var \PosSession
     *
     * @ManyToOne(targetEntity="PosSession")
     * @JoinColumns({
     *   @JoinColumn(name="pos_session_idpos_session", referencedColumnName="idpos_session")
     * })
     */
    private $posSessionposSession;

    /**
     * @var \ShippingMtd
     *
     * @ManyToOne(targetEntity="ShippingMtd")
     * @JoinColumns({
     *   @JoinColumn(name="shipping_mtd_idshipping_mtd", referencedColumnName="idshipping_mtd")
     * })
     */
    private $shippingMtdshippingMtd;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;

    /**
     * @var \Webshop
     *
     * @ManyToOne(targetEntity="Webshop")
     * @JoinColumns({
     *   @JoinColumn(name="webshop_idwebshop", referencedColumnName="idwebshop")
     * })
     */
    private $webshopwebshop;
    
    /**
     * @var string
     *
     * @Column(name="card", type="string", length=45, nullable=true)
     */
    private $card;
    
    /**
     * @var string
     *
     * @Column(name="viacliente", type="text", nullable=true)
     */
    private $viacliente;
    
    /**
     * @var string
     *
     * @Column(name="vialoja", type="text", nullable=true)
     */
    private $vialoja;

    function setCustomer($customer) {
        $this->personperson = $customer;
    }

    function setUser($user) {
        $this->usersusers = $user;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setPosSession($posSession) {
        $this->posSessionposSession = $posSession;
    }

    function setShippingMtd($shippingMtd) {
        $this->shippingMtdshippingMtd = $shippingMtd;
    }

    function setTrackingCode($trackingCode) {
        $this->trackingCode = $trackingCode;
    }

    /**
     * Get idorder
     *
     * @return integer
     */
    public function getIdorder() {
        return $this->idorder;
    }

    /**
     * Set dateShipped
     *
     * @param \DateTime $dateShipped
     *
     * @return Order
     */
    public function setDateShipped($dateShipped) {
        $this->dateShipped = $dateShipped;

        return $this;
    }

    /**
     * Get dateShipped
     *
     * @return \DateTime
     */
    public function getDateShipped() {
        return $this->dateShipped;
    }

    /**
     * Get trackingCode
     *
     * @return string
     */
    public function getTrackingCode() {
        return $this->trackingCode;
    }

    /**
     * Set dateCancelled
     *
     * @param \DateTime $dateCancelled
     *
     * @return Order
     */
    public function setDateCancelled($dateCancelled) {
        $this->dateCancelled = $dateCancelled;

        return $this;
    }

    /**
     * Get dateCancelled
     *
     * @return \DateTime
     */
    public function getDateCancelled() {
        return $this->dateCancelled;
    }

    /**
     * Set in_use
     *
     * @param boolean $inUse
     *
     * @return boolean
     */
    public function setInUse($inUse) {
        $this->inUse = $inUse;

        return $this;
    }

    /**
     * Get inUse
     *
     * @return boolean
     */
    public function getInUse() {
        return $this->inUse;
    }

    /**
     * Set tipValue
     *
     * @param string $tipValue
     *
     * @return Order
     */
    public function setTipValue($tipValue) {
        $this->tipValue = $tipValue;

        return $this;
    }

    /**
     * Get tipValue
     *
     * @return string
     */
    public function getTipValue() {
        return $this->tipValue;
    }

    /**
     * Set valueShipping
     *
     * @param string $valueShipping
     *
     * @return Order
     */
    public function setValueShipping($valueShipping) {
        $this->valueShipping = $valueShipping;

        return $this;
    }

    /**
     * Get valueShipping
     *
     * @return string
     */
    public function getValueShipping() {
        return $this->valueShipping;
    }

    /**
     * Set total amount
     *
     * @param string $discount
     *
     * @return Order
     */
    public function setTotalAmount($totalAmount) {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get total amount
     *
     * @return string
     */
    public function getTotalAmount() {
        return $this->totalAmount;
    }

    /**
     * Set discountReason
     *
     * @param string $discountReason
     *
     * @return Order
     */
    public function setDiscountReason($discountReason) {
        $this->discountReason = $discountReason;

        return $this;
    }

    /**
     * Get discountReason
     *
     * @return string
     */
    public function getDiscountReason() {
        return $this->discountReason;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return Order
     */
    public function setDiscount($discount) {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount() {
        return $this->discount;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Order
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Set RefundNote
     *
     * @param integer $refundNote
     *
     * @return Order
     */
    public function setRefundNote($refundNote) {
        $this->refundNote = $refundNote;

        return $this;
    }
    
    /**
     * Set order_type
     *
     * @param integer $orderType
     *
     * @return Order
     */
    public function setOrderType($orderType) {
        $this->orderType = $orderType;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set order_identify
     *
     * @param string $order_identify
     *
     * @return Order
     */
    public function setOrderIdentify($order_identify) {
        $this->order_identify = $order_identify;

        return $this;
    }

    /**
     * Get order_identify
     *
     * @return string
     */
    public function getOrderIdentify() {
        return $this->order_identify;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Order
     */
    public function setDateCreate($dateCreate) {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate() {
        return $this->dateCreate->format('d/m/Y');
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Order
     */
    public function setDateUpdate($dateUpdate) {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate() {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Order
     */
    public function setDateDelete($dateDelete) {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete() {
        return $this->dateDelete;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set addressaddress
     *
     * @param \Address $addressaddress
     *
     * @return Order
     */
    public function setAddressaddress(\Address $addressaddress = null) {
        $this->addressaddress = $addressaddress;

        return $this;
    }

    /**
     * Get addressaddress
     *
     * @return \Address
     */
    public function getAddressaddress() {
        return $this->addressaddress;
    }

    /**
     * Set companycompany
     *
     * @param \Company $companycompany
     *
     * @return Order
     */
    public function setCompanycompany(\Company $companycompany = null) {
        $this->companycompany = $companycompany;

        return $this;
    }

    /**
     * Get companycompany
     *
     * @return \Company
     */
    public function getCompanycompany() {
        return $this->companycompany;
    }

    /**
     * Set personperson
     *
     * @param \Person $personperson
     *
     * @return Order
     */
    public function setPersonperson(\Person $personperson = null) {
        $this->personperson = $personperson;

        return $this;
    }

    /**
     * Get personperson
     *
     * @return \Person
     */
    public function getPersonperson() {
        return $this->personperson;
    }

    /**
     * Set posSessionposSession
     *
     * @param \PosSession $posSessionposSession
     *
     * @return Order
     */
    public function setPosSessionposSession(\PosSession $posSessionposSession = null) {
        $this->posSessionposSession = $posSessionposSession;

        return $this;
    }

    /**
     * Get posSessionposSession
     *
     * @return \PosSession
     */
    public function getPosSessionposSession() {
        return $this->posSessionposSession;
    }

    /**
     * Set shippingMtdshippingMtd
     *
     * @param \ShippingMtd $shippingMtdshippingMtd
     *
     * @return Order
     */
    public function setShippingMtdshippingMtd(\ShippingMtd $shippingMtdshippingMtd = null) {
        $this->shippingMtdshippingMtd = $shippingMtdshippingMtd;

        return $this;
    }

    /**
     * Get shippingMtdshippingMtd
     *
     * @return \ShippingMtd
     */
    public function getShippingMtdshippingMtd() {
        return $this->shippingMtdshippingMtd;
    }

    /**
     * Set usersusers
     *
     * @param \Users $usersusers
     *
     * @return Order
     */
    public function setUsersusers(\Users $usersusers = null) {
        $this->usersusers = $usersusers;

        return $this;
    }

    /**
     * Get usersusers
     *
     * @return \Users
     */
    public function getUsersusers() {
        return $this->usersusers;
    }

    /**
     * Set webshopwebshop
     *
     * @param \Webshop $webshopwebshop
     *
     * @return Order
     */
    public function setWebshopwebshop(\Webshop $webshopwebshop = null) {
        $this->webshopwebshop = $webshopwebshop;

        return $this;
    }

    /**
     * Get webshopwebshop
     *
     * @return \Webshop
     */
    public function getWebshopwebshop() {
        return $this->webshopwebshop;
    }

    function getMobile() {
        return $this->mobile;
    }

    function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return Order
     */
    public function setExtraInfo($extraInfo) {
        $this->extraInfo = $extraInfo;

        return $this;
    }
    
    

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo() {
        return $this->extraInfo;
    }

    /**
     * Get RefundNote
     *
     * @return integer
     */
    public function getRefundNote() {
        return $this->refundNote;
    }
    
    /**
     * Get order_type
     *
     * @return integer
     */
    public function getOrderType() {
        return $this->orderType;
    }
    public function recalculate_total_amount() {
        $total = 0;
        $order_lines = getEm()->getRepository('OrderLine')->findBy(array('active' => 1, 'orderorder' => $this));
        if ($order_lines) {
            foreach ($order_lines as $order_line) {
                $total += (($order_line->getValue() * $order_line->getQty()) * ((100 - $order_line->getDiscount()) / 100));
            }
        }
        $this->totalAmount = $total;

        getEm()->persist($this);
        getEm()->flush();
    }

    /**
     * Get total paid
     *
     * @return float
     */
    public function getTotalPaid() {
        $total = 0;
        $order_payments = getEm()->getRepository('OrderPayment')->findBy(array('active' => 1, 'orderorder' => $this));
        if ($order_payments) {
            foreach ($order_payments as $order_payment) {
                $total += $order_payment->getValue();
            }
        }

        return $total;
    }

    /**
     * Get the remaining value
     *
     * @return float
     */
    public function getRemainingValue() {
        $remaining = 0;

        $total_paid = $this->getTotalPaid();
        $remaining = $this->getTotalAmount() - $total_paid;

        return $remaining;
    }
    
    function getOrder_identify() {
        return $this->order_identify;
    }

    function getCard() {
        return $this->card;
    }

    function getViacliente() {
        return $this->viacliente;
    }

    function getVialoja() {
        return $this->vialoja;
    }

    function setOrder_identify($order_identify) {
        $this->order_identify = $order_identify;
    }

    function setCard($card) {
        $this->card = $card;
    }

    function setViacliente($viacliente) {
        $this->viacliente = $viacliente;
    }

    function setVialoja($vialoja) {
        $this->vialoja = $vialoja;
    }



}

