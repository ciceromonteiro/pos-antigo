<?php



use Doctrine\Mapping as ORM;

/**
 * ShippingPlace
 *
 * @Table(name="shipping_place", indexes={@Index(name="fk_shipping_place_shipping_mtd1_idx", columns={"shipping_mtd_idshipping_mtd"}), @Index(name="fk_shipping_place_zz_state1_idx", columns={"zz_state_idzz_state"}), @Index(name="fk_shipping_place_zz_country1_idx", columns={"zz_country_idzz_country"})})
 * @Entity
 */
class ShippingPlace
{
    /**
     * @var integer
     *
     * @Column(name="idshipping_place", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idshippingPlace;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \ShippingMtd
     *
     * @ManyToOne(targetEntity="ShippingMtd")
     * @JoinColumns({
     *   @JoinColumn(name="shipping_mtd_idshipping_mtd", referencedColumnName="idshipping_mtd")
     * })
     */
    private $shippingMtdshippingMtd;

    /**
     * @var \ZzCountry
     *
     * @ManyToOne(targetEntity="ZzCountry")
     * @JoinColumns({
     *   @JoinColumn(name="zz_country_idzz_country", referencedColumnName="idzz_country")
     * })
     */
    private $zzCountryzzCountry;

    /**
     * @var \ZzState
     *
     * @ManyToOne(targetEntity="ZzState")
     * @JoinColumns({
     *   @JoinColumn(name="zz_state_idzz_state", referencedColumnName="idzz_state")
     * })
     */
    private $zzStatezzState;



    /**
     * Get idshippingPlace
     *
     * @return integer
     */
    public function getIdshippingPlace()
    {
        return $this->idshippingPlace;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ShippingPlace
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ShippingPlace
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return ShippingPlace
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ShippingPlace
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set shippingMtdshippingMtd
     *
     * @param \ShippingMtd $shippingMtdshippingMtd
     *
     * @return ShippingPlace
     */
    public function setShippingMtdshippingMtd(\ShippingMtd $shippingMtdshippingMtd = null)
    {
        $this->shippingMtdshippingMtd = $shippingMtdshippingMtd;

        return $this;
    }

    /**
     * Get shippingMtdshippingMtd
     *
     * @return \ShippingMtd
     */
    public function getShippingMtdshippingMtd()
    {
        return $this->shippingMtdshippingMtd;
    }

    /**
     * Set zzCountryzzCountry
     *
     * @param \ZzCountry $zzCountryzzCountry
     *
     * @return ShippingPlace
     */
    public function setZzCountryzzCountry(\ZzCountry $zzCountryzzCountry = null)
    {
        $this->zzCountryzzCountry = $zzCountryzzCountry;

        return $this;
    }

    /**
     * Get zzCountryzzCountry
     *
     * @return \ZzCountry
     */
    public function getZzCountryzzCountry()
    {
        return $this->zzCountryzzCountry;
    }

    /**
     * Set zzStatezzState
     *
     * @param \ZzState $zzStatezzState
     *
     * @return ShippingPlace
     */
    public function setZzStatezzState(\ZzState $zzStatezzState = null)
    {
        $this->zzStatezzState = $zzStatezzState;

        return $this;
    }

    /**
     * Get zzStatezzState
     *
     * @return \ZzState
     */
    public function getZzStatezzState()
    {
        return $this->zzStatezzState;
    }
}
