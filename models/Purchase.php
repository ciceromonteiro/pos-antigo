<?php



use Doctrine\Mapping as ORM;

/**
 * Purchase
 *
 * @Table(name="purchase", indexes={@Index(name="fk_purchase_supplier1_idx", columns={"supplier_idsupplier"}), @Index(name="fk_purchase_users1_idx", columns={"users_idusers"})})
 * @Entity
 */
class Purchase
{
    /**
     * @var integer
     *
     * @Column(name="idpurchase", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpurchase;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delivery", type="datetime", nullable=true)
     */
    private $dateDelivery;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delivered", type="datetime", nullable=true)
     */
    private $dateDelivered;

    /**
     * @var string
     *
     * @Column(name="discount", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $discount;

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var \Supplier
     *
     * @ManyToOne(targetEntity="Supplier")
     * @JoinColumns({
     *   @JoinColumn(name="supplier_idsupplier", referencedColumnName="idsupplier")
     * })
     */
    private $suppliersupplier;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;



    /**
     * Get idpurchase
     *
     * @return integer
     */
    public function getIdpurchase()
    {
        return $this->idpurchase;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return Purchase
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Purchase
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Purchase
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Purchase
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Purchase
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateDelivery
     *
     * @param \DateTime $dateDelivery
     *
     * @return Purchase
     */
    public function setDateDelivery($dateDelivery)
    {
        $this->dateDelivery = $dateDelivery;

        return $this;
    }

    /**
     * Get dateDelivery
     *
     * @return \DateTime
     */
    public function getDateDelivery()
    {
        return $this->dateDelivery;
    }

    /**
     * Set dateDelivered
     *
     * @param \DateTime $dateDelivered
     *
     * @return Purchase
     */
    public function setDateDelivered($dateDelivered)
    {
        $this->dateDelivered = $dateDelivered;

        return $this;
    }

    /**
     * Get dateDelivered
     *
     * @return \DateTime
     */
    public function getDateDelivered()
    {
        return $this->dateDelivered;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return Purchase
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Purchase
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set suppliersupplier
     *
     * @param \Supplier $suppliersupplier
     *
     * @return Purchase
     */
    public function setSuppliersupplier(\Supplier $suppliersupplier = null)
    {
        $this->suppliersupplier = $suppliersupplier;

        return $this;
    }

    /**
     * Get suppliersupplier
     *
     * @return \Supplier
     */
    public function getSuppliersupplier()
    {
        return $this->suppliersupplier;
    }

    /**
     * Set usersusers
     *
     * @param \Users $usersusers
     *
     * @return Purchase
     */
    public function setUsersusers(\Users $usersusers = null)
    {
        $this->usersusers = $usersusers;

        return $this;
    }

    /**
     * Get usersusers
     *
     * @return \Users
     */
    public function getUsersusers()
    {
        return $this->usersusers;
    }
}
