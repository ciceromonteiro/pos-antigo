<?php



use Doctrine\Mapping as ORM;

/**
 * ImportationSettings
 *
 * @Table(name="importation_settings")
 * @Entity
 */
class ImportationSettings
{
    /**
     * @var integer
     *
     * @Column(name="id_importation_settings", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idimportationSettings;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="file", type="text", nullable=true)
     */
    private $file;


    function getIdimportationSettings() {
        return $this->idimportationSettings;
    }

    function getName() {
        return $this->name;
    }

    function getFile() {
        return $this->file;
    }

    function setIdimportationSettings($idimportationSettings) {
        $this->idimportationSettings = $idimportationSettings;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setFile($file) {
        $this->file = $file;
    }
}

