<?php

use Doctrine\Mapping as ORM;

/**
 * SizeColor
 *
 * @Table(name="size_color", indexes={@Index(name="fk_size_color_color1_idx", columns={"color_idcolor"}), @Index(name="fk_size_color_size1_idx", columns={"size_idsize"}), @Index(name="fk_size_color_product1_idx", columns={"product_idproduct"})})
 * @Entity
 */
class SizeColor {

    /**
     * @var integer
     *
     * @Column(name="idsize_color", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsizeColor;

    /**
     * @var string
     *
     * @Column(name="codEan", type="string", length=300, nullable=true)
     */
    private $codean;

    /**
     * @var string
     *
     * @Column(name="codSupplier", type="string", length=50, nullable=true)
     */
    private $codsupplier;

    /**
     * @var integer
     *
     * @Column(name="total_amount", type="integer", nullable=true)
     */
    private $totalamount;

    /**
     * @var integer
     *
     * @Column(name="current_amount", type="integer", nullable=true)
     */
    private $currentamount;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Color
     *
     * @ManyToOne(targetEntity="Color")
     * @JoinColumns({
     *   @JoinColumn(name="color_idcolor", referencedColumnName="idcolor")
     * })
     */
    private $colorcolor;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Size
     *
     * @ManyToOne(targetEntity="Size")
     * @JoinColumns({
     *   @JoinColumn(name="size_idsize", referencedColumnName="idsize")
     * })
     */
    private $sizesize;

    function getIdsizeColor() {
        return $this->idsizeColor;
    }

    function getCodean() {
        return $this->codean;
    }

    function getCodsupplier() {
        return $this->codsupplier;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getColorcolor() {
        return $this->colorcolor;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function getSizesize() {
        return $this->sizesize;
    }

    function setIdsizeColor($idsizeColor) {
        $this->idsizeColor = $idsizeColor;
    }

    function setCodean($codean) {
        $this->codean = $codean;
    }

    function setCodsupplier($codsupplier) {
        $this->codsupplier = $codsupplier;
    }

    function getTotalamount() {
        return $this->totalamount;
    }

    function getCurrentamount() {
        return $this->currentamount;
    }

    function setTotalamount($totalamount) {
        $this->totalamount = $totalamount;
    }

    function setCurrentamount($currentamount) {
        $this->currentamount = $currentamount;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setColorcolor(\Color $colorcolor) {
        $this->colorcolor = $colorcolor;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }

    function setSizesize(\Size $sizesize) {
        $this->sizesize = $sizesize;
    }

}
