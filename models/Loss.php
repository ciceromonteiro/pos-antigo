<?php



use Doctrine\Mapping as ORM;

/**
 * Loss
 *
 * @Table(name="loss", indexes={@Index(name="fk_loss_users1_idx", columns={"users_idusers"})})
 * @Entity
 */
class Loss
{
    /**
     * @var integer
     *
     * @Column(name="idloss", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idloss;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;



    /**
     * Get idloss
     *
     * @return integer
     */
    public function getIdloss()
    {
        return $this->idloss;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return Loss
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Loss
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Loss
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Loss
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Loss
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set usersusers
     *
     * @param \Users $usersusers
     *
     * @return Loss
     */
    public function setUsersusers(\Users $usersusers = null)
    {
        $this->usersusers = $usersusers;

        return $this;
    }

    /**
     * Get usersusers
     *
     * @return \Users
     */
    public function getUsersusers()
    {
        return $this->usersusers;
    }
}
