<?php



use Doctrine\Mapping as ORM;

/**
 * Hall
 *
 * @Table(name="hall", indexes={@Index(name="fk_hall_warehouse1_idx", columns={"warehouse_idwarehouse"})})
 * @Entity
 */
class Hall
{
    /**
     * @var integer
     *
     * @Column(name="idhall", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idhall;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(name="width", type="integer", nullable=false)
     */
    private $width;

    /**
     * @var integer
     *
     * @Column(name="height", type="integer", nullable=false)
     */
    private $height;

    /**
     * @var integer
     *
     * @Column(name="position_top", type="integer", nullable=false)
     */
    private $positionTop;

    /**
     * @var integer
     *
     * @Column(name="position_left", type="integer", nullable=false)
     */
    private $positionLeft;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Warehouse
     *
     * @ManyToOne(targetEntity="Warehouse")
     * @JoinColumns({
     *   @JoinColumn(name="warehouse_idwarehouse", referencedColumnName="idwarehouse")
     * })
     */
    private $warehousewarehouse;

    function getIdhall() {
        return $this->idhall;
    }
    function getIdwarehouse() {
        return $this->warehousewarehouse->getIdwarehouse();
    }

    function getName() {
        return $this->name;
    }

    function getPosition_top() {
        return $this->positionTop;
    }

    function getPosition_left() {
        return $this->positionLeft;
    }

    function getHeight() {
        return $this->height;
    }

    function getWidth() {
        return $this->width;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdhall($idhall) {
        $this->idhall = $idhall;
    }

    function setIdwarehouse($idwarehouse) {
        $this->warehousewarehouse = $idwarehouse;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPosition_top($positionTop) {
        $this->positionTop = $positionTop;
    }

    function setPosition_left($positionLeft) {
        $this->positionLeft = $positionLeft;
    }

    function setHeight($height) {
        $this->height = $height;
    }

    function setWidth($width) {
        $this->width = $width;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


    /**
     * Set positionTop
     *
     * @param integer $positionTop
     *
     * @return Hall
     */
    public function setPositionTop($positionTop)
    {
        $this->positionTop = $positionTop;

        return $this;
    }

    /**
     * Get positionTop
     *
     * @return integer
     */
    public function getPositionTop()
    {
        return $this->positionTop;
    }

    /**
     * Set positionLeft
     *
     * @param integer $positionLeft
     *
     * @return Hall
     */
    public function setPositionLeft($positionLeft)
    {
        $this->positionLeft = $positionLeft;

        return $this;
    }

    /**
     * Get positionLeft
     *
     * @return integer
     */
    public function getPositionLeft()
    {
        return $this->positionLeft;
    }

    /**
     * Set warehousewarehouse
     *
     * @param \Warehouse $warehousewarehouse
     *
     * @return Hall
     */
    public function setWarehousewarehouse(\Warehouse $warehousewarehouse = null)
    {
        $this->warehousewarehouse = $warehousewarehouse;

        return $this;
    }

    /**
     * Get warehousewarehouse
     *
     * @return \Warehouse
     */
    public function getWarehousewarehouse()
    {
        return $this->warehousewarehouse;
    }
}
