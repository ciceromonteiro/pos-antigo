<?php



use Doctrine\Mapping as ORM;

/**
 * OrderTmptFooter
 *
 * @Table(name="order_tmpt_footer", indexes={@Index(name="idordertmpt_indx_1", columns={"idordertmpt"})})
 * @Entity
 */
class OrderTmptFooter
{
    /**
     * @var integer
     *
     * @Column(name="idordertmptfooter", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idordertmptfooter;

    /**
     * @var string
     *
     * @Column(name="footer_1", type="text", length=65535, nullable=true)
     */
    private $footer1;

    /**
     * @var string
     *
     * @Column(name="footer_2", type="text", length=65535, nullable=true)
     */
    private $footer2;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \OrderTmpt
     *
     * @ManyToOne(targetEntity="OrderTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="idordertmpt", referencedColumnName="idordertmpt")
     * })
     */
    private $idordertmpt;

    function getIdordertmptfooter() {
        return $this->idordertmptfooter;
    }

    function getFooter1() {
        return $this->footer1;
    }

    function getFooter2() {
        return $this->footer2;
    }

    function getActive() {
        return $this->active;
    }

    function getIdordertmpt() {
        return $this->idordertmpt;
    }

    function setIdordertmptfooter($idordertmptfooter) {
        $this->idordertmptfooter = $idordertmptfooter;
    }

    function setFooter1($footer1) {
        $this->footer1 = $footer1;
    }

    function setFooter2($footer2) {
        $this->footer2 = $footer2;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdordertmpt(\OrderTmpt $idordertmpt) {
        $this->idordertmpt = $idordertmpt;
    }


}

