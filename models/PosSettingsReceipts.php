<?php



use Doctrine\Mapping as ORM;

/**
 * PosSettingsReceipts
 *
 * @Table(name="pos_settings_receipts", indexes={@Index(name="idpos", columns={"idpos"})})
 * @Entity
 */
class PosSettingsReceipts
{
    /**
     * @var integer
     *
     * @Column(name="idpossettingsreceipts", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpossettingsreceipts;

    /**
     * @var string
     *
     * @Column(name="data_type", type="string", length=150, nullable=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @Column(name="data_value", type="string", length=250, nullable=false)
     */
    private $dataValue;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="idpos", referencedColumnName="idpos")
     * })
     */
    private $idpos;

    function getIdpossettingsreceipts() {
        return $this->idpossettingsreceipts;
    }

    function getDataType() {
        return $this->dataType;
    }

    function getDataValue() {
        return $this->dataValue;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getIdpos() {
        return $this->idpos;
    }

    function setIdpossettingsreceipts($idpossettingsreceipts) {
        $this->idpossettingsreceipts = $idpossettingsreceipts;
    }

    function setDataType($dataType) {
        $this->dataType = $dataType;
    }

    function setDataValue($dataValue) {
        $this->dataValue = $dataValue;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdpos(\Pos $idpos) {
        $this->idpos = $idpos;
    }


}

