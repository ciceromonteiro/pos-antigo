<?php



use Doctrine\Mapping as ORM;

/**
 * PersonData
 *
 * @Table(name="person_data", indexes={@Index(name="fk_person_data_person1_idx", columns={"person_idperson"})})
 * @Entity
 */
class PersonData
{
    /**
     * @var integer
     *
     * @Column(name="idperson_data", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpersonData;

    /**
     * @var string
     *
     * @Column(name="data_type", type="string", length=10, nullable=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @Column(name="data_value", type="string", length=45, nullable=false)
     */
    private $dataValue;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;

    /**
     * @return int
     */
    public function getIdpersonData()
    {
        return $this->idpersonData;
    }

    /**
     * @param int $idpersonData
     */
    public function setIdpersonData($idpersonData)
    {
        $this->idpersonData = $idpersonData;
    }

    /**
     * @return string
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * @param string $dataType
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;
    }

    /**
     * @return string
     */
    public function getDataValue()
    {
        return $this->dataValue;
    }

    /**
     * @param string $dataValue
     */
    public function setDataValue($dataValue)
    {
        $this->dataValue = $dataValue;
    }

    /**
     * @return DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * @param DateTime $dateCreate
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    /**
     * @return DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @param DateTime $dateUpdate
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    }

    /**
     * @return DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * @param DateTime $dateDelete
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return Person
     */
    public function getPersonperson()
    {
        return $this->personperson;
    }

    /**
     * @param Person $personperson
     */
    public function setPersonperson($personperson)
    {
        $this->personperson = $personperson;
    }



    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
