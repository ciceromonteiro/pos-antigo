<?php

use Doctrine\Mapping as ORM;

/**
 * GiftCardTmpt
 *
 * @Table(name="gift_card_tmpt")
 * @Entity
 */
class GiftCardTmpt {

    /**
     * @var integer
     *
     * @Column(name="idgift_card_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idgiftCardTmpt;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="info", type="text", nullable=true)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    function getIdgiftCardTmpt() {
        return $this->idgiftCardTmpt;
    }

    function getName() {
        return $this->name;
    }

    function getInfo() {
        return $this->info;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdgiftCardTmpt($idgiftCardTmpt) {
        $this->idgiftCardTmpt = $idgiftCardTmpt;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setInfo($info) {
        $this->info = $info;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

}
