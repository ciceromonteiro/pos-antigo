<?php



use Doctrine\Mapping as ORM;

/**
 * InvoicePrinter
 *
 * @Table(name="invoice_printer")
 * @Entity
 */
class InvoicePrinter
{
    /**
     * @var integer
     *
     * @Column(name="idinvoice_printer", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idinvoiceprinter;

    /**
     * @var string
     *
     * @Column(name="text_include_cashier", type="text", nullable=true)
     */
    private $textIncludeCashier;

    /**
     * @var integer
     *
     * @Column(name="size_font", type="integer", nullable=false)
     */
    private $sizeFont;

    /**
     * @var string
     *
     * @Column(name="description_printed", type="string", length=45, nullable=false)
     */
    private $descriptionPrinted;

    /**
     * @var integer
     *
     * @Column(name="number_copies", type="integer", nullable=false)
     */
    private $numberCopies;

    /**
     * @var integer
     *
     * @Column(name="number_decimal_places", type="integer", nullable=false)
     */
    private $numberDecimalPlaces;

    /**
     * @var integer
     *
     * @Column(name="print_drawing_of_color", type="integer", nullable=false)
     */
    private $printDrawingOfColor;

    /**
     * @var integer
     *
     * @Column(name="enter_the_customers_phone_number", type="integer", nullable=false)
     */
    private $enterTheCustomersPhoneNumber;

    /**
     * @var integer
     *
     * @Column(name="change_the_location", type="integer", nullable=false)
     */
    private $changeTheLocation;

    /**
     * @var integer
     *
     * @Column(name="print_taxes_included", type="integer", nullable=false)
     */
    private $printTaxesIncluded;

    /**
     * @var integer
     *
     * @Column(name="do_not_print_project_information", type="integer", nullable=false)
     */
    private $doNotPrintProjectInformation;

    /**
     * @var integer
     *
     * @Column(name="use_the_default_printer_instead", type="integer", nullable=false)
     */
    private $useTheDefaultPrinterInstead;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    
    function getIdinvoiceprinter() {
        return $this->idinvoiceprinter;
    }

    function getTextIncludeCashier() {
        return $this->textIncludeCashier;
    }

    function getSizeFont() {
        return $this->sizeFont;
    }

    function getDescriptionPrinted() {
        return $this->descriptionPrinted;
    }

    function getNumberCopies() {
        return $this->numberCopies;
    }

    function getNumberDecimalPlaces() {
        return $this->numberDecimalPlaces;
    }

    function getPrintDrawingOfColor() {
        return $this->printDrawingOfColor;
    }

    function getEnterTheCustomersPhoneNumber() {
        return $this->enterTheCustomersPhoneNumber;
    }

    function getChangeTheLocation() {
        return $this->changeTheLocation;
    }

    function getPrintTaxesIncluded() {
        return $this->printTaxesIncluded;
    }

    function getDoNotPrintProjectInformation() {
        return $this->doNotPrintProjectInformation;
    }

    function getUseTheDefaultPrinterInstead() {
        return $this->useTheDefaultPrinterInstead;
    }

    function getActive() {
        return $this->active;
    }

    function setIdinvoiceprinter($idinvoiceprinter) {
        $this->idinvoiceprinter = $idinvoiceprinter;
    }

    function setTextIncludeCashier($textIncludeCashier) {
        $this->textIncludeCashier = $textIncludeCashier;
    }

    function setSizeFont($sizeFont) {
        $this->sizeFont = $sizeFont;
    }

    function setDescriptionPrinted($descriptionPrinted) {
        $this->descriptionPrinted = $descriptionPrinted;
    }

    function setNumberCopies($numberCopies) {
        $this->numberCopies = $numberCopies;
    }

    function setNumberDecimalPlaces($numberDecimalPlaces) {
        $this->numberDecimalPlaces = $numberDecimalPlaces;
    }

    function setPrintDrawingOfColor($printDrawingOfColor) {
        $this->printDrawingOfColor = $printDrawingOfColor;
    }

    function setEnterTheCustomersPhoneNumber($enterTheCustomersPhoneNumber) {
        $this->enterTheCustomersPhoneNumber = $enterTheCustomersPhoneNumber;
    }

    function setChangeTheLocation($changeTheLocation) {
        $this->changeTheLocation = $changeTheLocation;
    }

    function setPrintTaxesIncluded($printTaxesIncluded) {
        $this->printTaxesIncluded = $printTaxesIncluded;
    }

    function setDoNotPrintProjectInformation($doNotPrintProjectInformation) {
        $this->doNotPrintProjectInformation = $doNotPrintProjectInformation;
    }

    function setUseTheDefaultPrinterInstead($useTheDefaultPrinterInstead) {
        $this->useTheDefaultPrinterInstead = $useTheDefaultPrinterInstead;
    }

    function setActive($active) {
        $this->active = $active;
    }
}
