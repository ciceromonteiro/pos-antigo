<?php



use Doctrine\Mapping as ORM;

/**
 * SizeColorSection
 *
 * @Table(name="size_color_section", indexes={@Index(name="fk_size_color_warehouse_size_color1_idx", columns={"size_color_idsize_color"}), @Index(name="fk_size_color_warehouse_section1_idx", columns={"section_idsection"})})
 * @Entity
 */
class SizeColorSection
{
    /**
     * @var integer
     *
     * @Column(name="idsize_color_section", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsizeColorSection;

    /**
     * @var integer
     *
     * @Column(name="qty", type="integer", nullable=false)
     */
    private $qty;

    /**
     * @var \Section
     *
     * @ManyToOne(targetEntity="Section")
     * @JoinColumns({
     *   @JoinColumn(name="section_idsection", referencedColumnName="idsection")
     * })
     */
    private $sectionsection;

    /**
     * @var \SizeColor
     *
     * @ManyToOne(targetEntity="SizeColor")
     * @JoinColumns({
     *   @JoinColumn(name="size_color_idsize_color", referencedColumnName="idsize_color")
     * })
     */
    private $sizeColorsizeColor;



    /**
     * Get idsizeColorSection
     *
     * @return integer
     */
    public function getIdsizeColorSection()
    {
        return $this->idsizeColorSection;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     *
     * @return SizeColorSection
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return integer
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set sectionsection
     *
     * @param \Section $sectionsection
     *
     * @return SizeColorSection
     */
    public function setSectionsection(\Section $sectionsection = null)
    {
        $this->sectionsection = $sectionsection;

        return $this;
    }

    /**
     * Get sectionsection
     *
     * @return \Section
     */
    public function getSectionsection()
    {
        return $this->sectionsection;
    }

    /**
     * Set sizeColorsizeColor
     *
     * @param \SizeColor $sizeColorsizeColor
     *
     * @return SizeColorSection
     */
    public function setSizeColorsizeColor(\SizeColor $sizeColorsizeColor = null)
    {
        $this->sizeColorsizeColor = $sizeColorsizeColor;

        return $this;
    }

    /**
     * Get sizeColorsizeColor
     *
     * @return \SizeColor
     */
    public function getSizeColorsizeColor()
    {
        return $this->sizeColorsizeColor;
    }
}
