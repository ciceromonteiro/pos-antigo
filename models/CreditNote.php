<?php



use Doctrine\Mapping as ORM;

/**
 * CreditNote
 *
 * @Table(name="credit_note", indexes={@Index(name="fk_credit_note_credit_tmpt1_idx", columns={"credit_tmpt_idcredit_tmpt"})})
 * @Entity
 */
class CreditNote
{
    /**
     * @var integer
     *
     * @Column(name="idcredit_note", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcreditNote;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \CreditTmpt
     *
     * @ManyToOne(targetEntity="CreditTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="credit_tmpt_idcredit_tmpt", referencedColumnName="idcredit_tmpt")
     * })
     */
    private $creditTmptcreditTmpt = null;

    /**
     * @var \Order
     *
     * @ManyToOne(targetEntity="Order")
     * @JoinColumns({
     *   @JoinColumn(name="order_idorder", referencedColumnName="idorder")
     * })
     */
    private $orderorder;



    /**
     * Get idcreditNote
     *
     * @return integer
     */
    public function getIdcreditNote()
    {
        return $this->idcreditNote;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return CreditNote
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return CreditNote
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return CreditNote
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return CreditNote
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return CreditNote
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return CreditNote
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set creditTmptcreditTmpt
     *
     * @param \CreditTmpt $creditTmptcreditTmpt
     *
     * @return CreditNote
     */
    public function setCreditTmptcreditTmpt(\CreditTmpt $creditTmptcreditTmpt = null)
    {
        $this->creditTmptcreditTmpt = $creditTmptcreditTmpt;

        return $this;
    }

    /**
     * Get creditTmptcreditTmpt
     *
     * @return \CreditTmpt
     */
    public function getCreditTmptcreditTmpt()
    {
        return $this->creditTmptcreditTmpt;
    }


    /**
     * Set orderorder
     *
     * @param \Order $orderorder
     *
     * @return CreditNote
     */
    public function setOrderorder(\Order $orderorder = null)
    {
        $this->orderorder = $orderorder;

        return $this;
    }

    /**
     * Get orderorder
     *
     * @return \Order
     */
    public function getOrderorder()
    {
        return $this->orderorder;
    }
}
