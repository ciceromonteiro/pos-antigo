<?php

use Doctrine\Mapping as ORM;

/**
 * Users
 *
 * @Table(name="users", uniqueConstraints={@UniqueConstraint(name="login_UNIQUE", columns={"login"})}, indexes={@Index(name="fk_users_users_type1_idx", columns={"users_type_idusers_type"}), @Index(name="fk_users_job1_idx", columns={"job_idjob"}), @Index(name="fk_users_person1_idx", columns={"person_idperson"})})
 * @Entity
 */
class Users {

    /**
     * @var integer
     *
     * @Column(name="idusers", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idusers;

    /**
     * @var string
     *
     * @Column(name="login", type="string", length=200, nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @Column(name="password", type="string", length=200, nullable=false)
     */
    private $password;

    /**
     * @var integer
     *
     * @Column(name="system_version", type="integer", nullable=false)
     */
    private $systemVersion;

    /**
     * @var integer
     *
     * @Column(name="multiple_connections_limit", type="integer", nullable=false)
     */
    private $multipleConnectionsLimit;

    /**
     * @var \DateTime
     *
     * @Column(name="date_creation", type="datetime", nullable=false)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @var \Job
     *
     * @ManyToOne(targetEntity="Job")
     * @JoinColumns({
     *   @JoinColumn(name="job_idjob", referencedColumnName="idjob")
     * })
     */
    private $jobjob;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;

    /**
     * @var \UsersType
     *
     * @ManyToOne(targetEntity="UsersType")
     * @JoinColumns({
     *   @JoinColumn(name="users_type_idusers_type", referencedColumnName="idusers_type")
     * })
     */
    private $usersTypeusersType;

    function getIdusers() {
        return $this->idusers;
    }

    function getLogin() {
        return $this->login;
    }

    function getPassword() {
        return $this->password;
    }

    function getSystemVersion() {
        return $this->systemVersion;
    }

    function getMultipleConnectionsLimit() {
        return $this->multipleConnectionsLimit;
    }

    function getDateCreation() {
        return $this->dateCreation;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getJobjob() {
        return $this->jobjob;
    }

    function getPersonperson() {
        return $this->personperson;
    }

    function getUsersTypeusersType() {
        return $this->usersTypeusersType;
    }

    function setIdusers($idusers) {
        $this->idusers = $idusers;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setSystemVersion($systemVersion) {
        $this->systemVersion = $systemVersion;
    }

    function setMultipleConnectionsLimit($multipleConnectionsLimit) {
        $this->multipleConnectionsLimit = $multipleConnectionsLimit;
    }

    function setDateCreation(\DateTime $dateCreation) {
        $this->dateCreation = $dateCreation;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setJobjob($jobjob) {
        $this->jobjob = $jobjob;
    }

    function setPersonperson($personperson) {
        $this->personperson = $personperson;
    }

    function setUsersTypeusersType($usersTypeusersType) {
        $this->usersTypeusersType = $usersTypeusersType;
    }

}
