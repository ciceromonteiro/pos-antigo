<?php

use Doctrine\Mapping as ORM;

/**
 * GiftCard
 *
 * @Table(name="gift_card", indexes={@Index(name="fk_gift_card_person1_idx", columns={"person_idperson"}), @Index(name="fk_gift_card_company1_idx", columns={"company_idcompany"}), @Index(name="fk_gift_card_gift_card_tmpt1_idx", columns={"gift_card_tmpt_idgift_card_tmpt"})})
 * @Entity
 */
class GiftCard {

    /**
     * @var integer
     *
     * @Column(name="idgift_card", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idgiftCard;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var string
     *
     * @Column(name="gift_card_identify", type="text", nullable=true)
     */
    private $giftCardIdentify;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @Column(name="value_used", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $valueUsed;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Order
     *
     * @ManyToOne(targetEntity="Order")
     * @JoinColumns({
     *   @JoinColumn(name="order_idorder", referencedColumnName="idorder")
     * })
     */
    private $orderorder;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;

    /**
     * @var \GiftCardTmpt
     *
     * @ManyToOne(targetEntity="GiftCardTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="gift_card_tmpt_idgift_card_tmpt", referencedColumnName="idgift_card_tmpt")
     * })
     */
    private $giftCardTmptgiftCardTmpt;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;

    function getIdgiftCard() {
        return $this->idgiftCard;
    }

    function getExtraInfo() {
        return $this->extraInfo;
    }

    function getGiftCardIdentify() {
        return $this->giftCardIdentify;
    }

    function getValue() {
        return $this->value;
    }

    function getValueUsed() {
        return $this->valueUsed;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getCompanycompany() {
        return $this->companycompany;
    }

    function getGiftCardTmptgiftCardTmpt() {
        return $this->giftCardTmptgiftCardTmpt;
    }

    function getPersonperson() {
        return $this->personperson;
    }

    function getOrderorder() {
        return $this->orderorder;
    }

    function setIdgiftCard($idgiftCard) {
        $this->idgiftCard = $idgiftCard;
    }

    function setExtraInfo($extraInfo) {
        $this->extraInfo = $extraInfo;
    }

    function setGiftCardIdentify($giftCardIdentify) {
        $this->giftCardIdentify = $giftCardIdentify;
    }

    function setValue($value) {
        $this->value = $value;
    }

    function setValueUsed($valueUsed) {
        $this->valueUsed = $valueUsed;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setCompanycompany(\Company $companycompany) {
        $this->companycompany = $companycompany;
    }

    function setGiftCardTmptgiftCardTmpt(\GiftCardTmpt $giftCardTmptgiftCardTmpt) {
        $this->giftCardTmptgiftCardTmpt = $giftCardTmptgiftCardTmpt;
    }

    function setPersonperson(\Person $personperson) {
        $this->personperson = $personperson;
    }

    function setOrderorder(\Order $orderorder) {
        $this->orderorder = $orderorder;
    }

}
