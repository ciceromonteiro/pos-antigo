<?php



use Doctrine\Mapping as ORM;

/**
 * OrderRule
 *
 * @Table(name="order_rule")
 * @Entity
 */
class OrderRule
{
    /**
     * @var integer
     *
     * @Column(name="idorderrule", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idorderrule;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=250, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @Column(name="alternative_price", type="boolean", nullable=true)
     */
    private $alternativePrice;

    /**
     * @var boolean
     *
     * @Column(name="show_popup", type="boolean", nullable=true)
     */
    private $showPopup;

    /**
     * @var boolean
     *
     * @Column(name="alternative_vat", type="boolean", nullable=true)
     */
    private $alternativeVat;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    function getIdorderrule() {
        return $this->idorderrule;
    }

    function getName() {
        return $this->name;
    }

    function getAlternativePrice() {
        return $this->alternativePrice;
    }

    function getShowPopup() {
        return $this->showPopup;
    }

    function getAlternativeVat() {
        return $this->alternativeVat;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdorderrule($idorderrule) {
        $this->idorderrule = $idorderrule;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setAlternativePrice($alternativePrice) {
        $this->alternativePrice = $alternativePrice;
    }

    function setShowPopup($showPopup) {
        $this->showPopup = $showPopup;
    }

    function setAlternativeVat($alternativeVat) {
        $this->alternativeVat = $alternativeVat;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

