<?php



use Doctrine\Mapping as ORM;

/**
 * OrderPayment
 *
 * @Table(name="order_payment", indexes={@Index(name="fk_order_payment_order1_idx", columns={"order_idorder"}), @Index(name="fk_order_payment_payment_mtd1_idx", columns={"payment_mtd_idpayment_mtd"})})
 * @Entity
 */
class OrderPayment
{
    /**
     * @var integer
     *
     * @Column(name="idorder_payment", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idorderPayment;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Order
     *
     * @ManyToOne(targetEntity="Order")
     * @JoinColumns({
     *   @JoinColumn(name="order_idorder", referencedColumnName="idorder")
     * })
     */
    private $orderorder;

    /**
     * @var \PaymentMtd
     *
     * @ManyToOne(targetEntity="PaymentMtd")
     * @JoinColumns({
     *   @JoinColumn(name="payment_mtd_idpayment_mtd", referencedColumnName="idpayment_mtd")
     * })
     */
    private $paymentMtdpaymentMtd;

    /**
     * Get idorderPayment
     *
     * @return integer
     */
    public function getIdorderPayment()
    {
        return $this->idorderPayment;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return OrderPayment
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return OrderPayment
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return OrderPayment
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return OrderPayment
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return OrderPayment
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set orderorder
     *
     * @param \Order $orderorder
     *
     * @return OrderPayment
     */
    public function setOrderorder(\Order $orderorder = null)
    {
        $this->orderorder = $orderorder;

        return $this;
    }

    /**
     * Get orderorder
     *
     * @return \Order
     */
    public function getOrderorder()
    {
        return $this->orderorder;
    }

    /**
     * Set paymentMtdpaymentMtd
     *
     * @param \PaymentMtd $paymentMtdpaymentMtd
     *
     * @return OrderPayment
     */
    public function setPaymentMtdpaymentMtd(\PaymentMtd $paymentMtdpaymentMtd = null)
    {
        $this->paymentMtdpaymentMtd = $paymentMtdpaymentMtd;

        return $this;
    }

    /**
     * Get paymentMtdpaymentMtd
     *
     * @return \PaymentMtd
     */
    public function getPaymentMtdpaymentMtd()
    {
        return $this->paymentMtdpaymentMtd;
    }
}
