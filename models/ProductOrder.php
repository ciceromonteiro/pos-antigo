<?php



use Doctrine\Mapping as ORM;

/**
 * ProductOrder
 *
 * @Table(name="product_order", indexes={@Index(name="product_idproduct", columns={"product_idproduct"})})
 * @Entity
 */
class ProductOrder
{
    /**
     * @var integer
     *
     * @Column(name="idproduct", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproduct;

    /**
     * @var string
     *
     * @Column(name="percent", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $percent;

    /**
     * @var integer
     *
     * @Column(name="the_sale_price_will", type="integer", nullable=true)
     */
    private $theSalePriceWill;

    /**
     * @var integer
     *
     * @Column(name="do_not_put_a_selling_price", type="integer", nullable=true)
     */
    private $doNotPutASellingPrice;

    /**
     * @var integer
     *
     * @Column(name="do_not_allow_discount", type="integer", nullable=true)
     */
    private $doNotAllowDiscount;

    /**
     * @var integer
     *
     * @Column(name="product_only_to_buy", type="integer", nullable=true)
     */
    private $productOnlyToBuy;

    /**
     * @var integer
     *
     * @Column(name="print_product_information_in_tax_coupon", type="integer", nullable=true)
     */
    private $printProductInformationInTaxCoupon;

    /**
     * @var integer
     *
     * @Column(name="fill_quantity_based_on_the_information", type="integer", nullable=true)
     */
    private $fillQuantityBasedOnTheInformation;

    /**
     * @var integer
     *
     * @Column(name="hide_from_statistics", type="integer", nullable=true)
     */
    private $hideFromStatistics;

    /**
     * @var integer
     *
     * @Column(name="display_alternate", type="integer", nullable=true)
     */
    private $displayAlternate;

    /**
     * @var integer
     *
     * @Column(name="the_purchase_price", type="integer", nullable=true)
     */
    private $thePurchasePrice;

    /**
     * @var integer
     *
     * @Column(name="view_product_description", type="integer", nullable=true)
     */
    private $viewProductDescription;

    /**
     * @var integer
     *
     * @Column(name="do_not_show_product", type="integer", nullable=true)
     */
    private $doNotShowProduct;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    function getIdproduct() {
        return $this->idproduct;
    }

    function getPercent() {
        return $this->percent;
    }

    function getTheSalePriceWill() {
        return $this->theSalePriceWill;
    }

    function getDoNotPutASellingPrice() {
        return $this->doNotPutASellingPrice;
    }

    function getDoNotAllowDiscount() {
        return $this->doNotAllowDiscount;
    }

    function getProductOnlyToBuy() {
        return $this->productOnlyToBuy;
    }

    function getPrintProductInformationInTaxCoupon() {
        return $this->printProductInformationInTaxCoupon;
    }

    function getFillQuantityBasedOnTheInformation() {
        return $this->fillQuantityBasedOnTheInformation;
    }

    function getHideFromStatistics() {
        return $this->hideFromStatistics;
    }

    function getDisplayAlternate() {
        return $this->displayAlternate;
    }

    function getThePurchasePrice() {
        return $this->thePurchasePrice;
    }

    function getViewProductDescription() {
        return $this->viewProductDescription;
    }

    function getDoNotShowProduct() {
        return $this->doNotShowProduct;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function setIdproduct($idproduct) {
        $this->idproduct = $idproduct;
    }

    function setPercent($percent) {
        $this->percent = $percent;
    }

    function setTheSalePriceWill($theSalePriceWill) {
        $this->theSalePriceWill = $theSalePriceWill;
    }

    function setDoNotPutASellingPrice($doNotPutASellingPrice) {
        $this->doNotPutASellingPrice = $doNotPutASellingPrice;
    }

    function setDoNotAllowDiscount($doNotAllowDiscount) {
        $this->doNotAllowDiscount = $doNotAllowDiscount;
    }

    function setProductOnlyToBuy($productOnlyToBuy) {
        $this->productOnlyToBuy = $productOnlyToBuy;
    }

    function setPrintProductInformationInTaxCoupon($printProductInformationInTaxCoupon) {
        $this->printProductInformationInTaxCoupon = $printProductInformationInTaxCoupon;
    }

    function setFillQuantityBasedOnTheInformation($fillQuantityBasedOnTheInformation) {
        $this->fillQuantityBasedOnTheInformation = $fillQuantityBasedOnTheInformation;
    }

    function setHideFromStatistics($hideFromStatistics) {
        $this->hideFromStatistics = $hideFromStatistics;
    }

    function setDisplayAlternate($displayAlternate) {
        $this->displayAlternate = $displayAlternate;
    }

    function setThePurchasePrice($thePurchasePrice) {
        $this->thePurchasePrice = $thePurchasePrice;
    }

    function setViewProductDescription($viewProductDescription) {
        $this->viewProductDescription = $viewProductDescription;
    }

    function setDoNotShowProduct($doNotShowProduct) {
        $this->doNotShowProduct = $doNotShowProduct;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }

}

