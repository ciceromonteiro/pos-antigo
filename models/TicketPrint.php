<?php



use Doctrine\Mapping as ORM;

/**
 * TicketPrint
 *
 * @Table(name="ticket_print")
 * @Entity
 */
class TicketPrint
{
    /**
     * @var integer
     *
     * @Column(name="idticket_print", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idticketPrint;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="text_print", type="text", nullable=true)
     */
    private $textPrint;


    function getIdticketPrint() {
        return $this->idticketPrint;
    }

    function getName() {
        return $this->name;
    }

    function getTextPrint() {
        return $this->textPrint;
    }

    function setIdticketPrint($idticketPrint) {
        $this->idticketPrint = $idticketPrint;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setTextPrint($textPrint) {
        $this->textPrint = $textPrint;
    }
}