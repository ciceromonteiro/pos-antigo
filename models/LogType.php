<?php



use Doctrine\Mapping as ORM;

/**
 * LogType
 *
 * @Table(name="log_type")
 * @Entity
 */
class LogType
{
    /**
     * @var integer
     *
     * @Column(name="idlog_type", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlogType;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;



    /**
     * Get idlogType
     *
     * @return integer
     */
    public function getIdlogType()
    {
        return $this->idlogType;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return LogType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
