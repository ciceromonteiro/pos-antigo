<?php

use Doctrine\Mapping as ORM;

/**
 * ZzState
 *
 * @Table(name="zz_state", indexes={@Index(name="fk_zz_state_zz_country1_idx", columns={"zz_country_idzz_country"})})
 * @Entity
 */
class ZzState {

    /**
     * @var integer
     *
     * @Column(name="idzz_state", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idzzState;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \ZzCountry
     *
     * @ManyToOne(targetEntity="ZzCountry")
     * @JoinColumns({
     *   @JoinColumn(name="zz_country_idzz_country", referencedColumnName="idzz_country")
     * })
     */
    private $zzCountryzzCountry;
    
    function getIdzzState() {
        return $this->idzzState;
    }

    function getName() {
        return $this->name;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getZzCountryzzCountry() {
        return $this->zzCountryzzCountry;
    }

    function setIdzzState($idzzState) {
        $this->idzzState = $idzzState;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setZzCountryzzCountry(\ZzCountry $zzCountryzzCountry) {
        $this->zzCountryzzCountry = $zzCountryzzCountry;
    }



}
