<?php



use Doctrine\Mapping as ORM;

/**
 * Deposit
 *
 * @Table(name="deposit", indexes={@Index(name="fk_deposit_user1_idx", columns={"user_iduser"})})
 * @Entity
 */
class Deposit
{
    /**
     * @var integer
     *
     * @Column(name="iddeposit", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $iddeposit;

    /**
     * @var string
     *
     * @Column(name="amount", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $amount;

    /**
     * @var string
     *
     * @Column(name="deposit_number", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $depositNumber;

        /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo = "";

    /**
     * @var \DateTime
     *
     * @Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="user_iduser", referencedColumnName="idusers")
     * })
     */
    private $useruser;

    function getIddeposit() {
        return $this->iddeposit;
    }

    function getAmount() {
        return $this->amount;
    }

    function getDepositNumber() {
        return $this->depositNumber;
    }

    function getDate() {
        return $this->date;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getUseruser() {
        return $this->useruser;
    }

    function setIddeposit($iddeposit) {
        $this->iddeposit = $iddeposit;
    }

    function setAmount($amount) {
        $this->amount = $amount;
    }

    function setDepositNumber($depositNumber) {
        $this->depositNumber = $depositNumber;
    }

    function setDate(\DateTime $date) {
        $this->date = $date;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setUseruser(\Users $useruser) {
        $this->useruser = $useruser;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return Deposit
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }


}

