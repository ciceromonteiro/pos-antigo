<?php



use Doctrine\Mapping as ORM;

/**
 * Currency
 *
 * @Table(name="currency")
 * @Entity
 */
class Currency
{
    /**
     * @var integer
     *
     * @Column(name="idcurrency", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcurrency;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="iso", type="string", length=50, nullable=true)
     */
    private $iso;

    /**
     * @var string
     *
     * @Column(name="symbol", type="string", length=7, nullable=false)
     */
    private $symbol;

    /**
     * @var float
     *
     * @Column(name="exchange_rate", type="float", precision=10, scale=0, nullable=true)
     */
    private $exchangeRate;

    /**
     * @var string
     *
     * @Column(name="exchange_unit", type="string", length=50, nullable=true)
     */
    private $exchangeUnit;

    /**
     * @var float
     *
     * @Column(name="smallest_denomination", type="float", precision=10, scale=0, nullable=true)
     */
    private $smallestDenomination;

    /**
     * @var integer
     *
     * @Column(name="account", type="integer", nullable=true)
     */
    private $account;

    /**
     * @var string
     *
     * @Column(name="iban", type="string", length=50, nullable=true)
     */
    private $iban;

    /**
     * @var string
     *
     * @Column(name="bic", type="string", length=50, nullable=true)
     */
    private $bic;

    /**
     * @var string
     *
     * @Column(name="account_currency_code", type="string", length=50, nullable=true)
     */
    private $accountCurrencyCode;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    function getIdcurrency() {
        return $this->idcurrency;
    }

    function getName() {
        return $this->name;
    }

    function getIso() {
        return $this->iso;
    }

    function getSymbol() {
        return $this->symbol;
    }

    function getExchangeRate() {
        return $this->exchangeRate;
    }

    function getExchangeUnit() {
        return $this->exchangeUnit;
    }

    function getSmallestDenomination() {
        return $this->smallestDenomination;
    }

    function getAccount() {
        return $this->account;
    }

    function getIban() {
        return $this->iban;
    }

    function getBic() {
        return $this->bic;
    }

    function getAccountCurrencyCode() {
        return $this->accountCurrencyCode;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdcurrency($idcurrency) {
        $this->idcurrency = $idcurrency;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setIso($iso) {
        $this->iso = $iso;
    }

    function setSymbol($symbol) {
        $this->symbol = $symbol;
    }

    function setExchangeRate($exchangeRate) {
        $this->exchangeRate = $exchangeRate;
    }

    function setExchangeUnit($exchangeUnit) {
        $this->exchangeUnit = $exchangeUnit;
    }

    function setSmallestDenomination($smallestDenomination) {
        $this->smallestDenomination = $smallestDenomination;
    }

    function setAccount($account) {
        $this->account = $account;
    }

    function setIban($iban) {
        $this->iban = $iban;
    }

    function setBic($bic) {
        $this->bic = $bic;
    }

    function setAccountCurrencyCode($accountCurrencyCode) {
        $this->accountCurrencyCode = $accountCurrencyCode;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

