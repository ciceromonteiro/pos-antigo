<?php



use Doctrine\Mapping as ORM;

/**
 * PurchaseSerialNumber
 *
 * @Table(name="purchase_serial_number", indexes={@Index(name="fk_purchase_serial_number_purchase_line1_idx", columns={"purchase_line_idpurchase_line"}), @Index(name="fk_purchase_serial_number_product_serial_number1_idx", columns={"product_serial_number_idproduct_serial_number"})})
 * @Entity
 */
class PurchaseSerialNumber
{
    /**
     * @var integer
     *
     * @Column(name="idpurchase_serial_number", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpurchaseSerialNumber;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \ProductSerialNumber
     *
     * @ManyToOne(targetEntity="ProductSerialNumber")
     * @JoinColumns({
     *   @JoinColumn(name="product_serial_number_idproduct_serial_number", referencedColumnName="idproduct_serial_number")
     * })
     */
    private $productSerialNumberproductSerialNumber;

    /**
     * @var \PurchaseLine
     *
     * @ManyToOne(targetEntity="PurchaseLine")
     * @JoinColumns({
     *   @JoinColumn(name="purchase_line_idpurchase_line", referencedColumnName="idpurchase_line")
     * })
     */
    private $purchaseLinepurchaseLine;



    /**
     * Get idpurchaseSerialNumber
     *
     * @return integer
     */
    public function getIdpurchaseSerialNumber()
    {
        return $this->idpurchaseSerialNumber;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return PurchaseSerialNumber
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return PurchaseSerialNumber
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return PurchaseSerialNumber
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PurchaseSerialNumber
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set productSerialNumberproductSerialNumber
     *
     * @param \ProductSerialNumber $productSerialNumberproductSerialNumber
     *
     * @return PurchaseSerialNumber
     */
    public function setProductSerialNumberproductSerialNumber(\ProductSerialNumber $productSerialNumberproductSerialNumber = null)
    {
        $this->productSerialNumberproductSerialNumber = $productSerialNumberproductSerialNumber;

        return $this;
    }

    /**
     * Get productSerialNumberproductSerialNumber
     *
     * @return \ProductSerialNumber
     */
    public function getProductSerialNumberproductSerialNumber()
    {
        return $this->productSerialNumberproductSerialNumber;
    }

    /**
     * Set purchaseLinepurchaseLine
     *
     * @param \PurchaseLine $purchaseLinepurchaseLine
     *
     * @return PurchaseSerialNumber
     */
    public function setPurchaseLinepurchaseLine(\PurchaseLine $purchaseLinepurchaseLine = null)
    {
        $this->purchaseLinepurchaseLine = $purchaseLinepurchaseLine;

        return $this;
    }

    /**
     * Get purchaseLinepurchaseLine
     *
     * @return \PurchaseLine
     */
    public function getPurchaseLinepurchaseLine()
    {
        return $this->purchaseLinepurchaseLine;
    }
}
