<?php




/**
 * UsersPermissionInCompany
 *
 * @Table(name="users_permission_in_company", indexes={@Index(name="fk_users_permission_permission1_idx", columns={"permission_idpermission"}), @Index(name="fk_users_permission_in_company_users_to_company_is1_idx", columns={"users_to_company_is_idusers_to_company_is"})})
 * @Entity
 */
class UsersPermissionInCompany
{
    /**
     * @var integer
     *
     * @Column(name="idusers_permission", type="bigint", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idusersPermission;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var \UsersToCompanyIs
     *
     * @ManyToOne(targetEntity="UsersToCompanyIs")
     * @JoinColumns({
     *   @JoinColumn(name="users_to_company_is_idusers_to_company_is", referencedColumnName="idusers_to_company_is")
     * })
     */
    private $usersToCompanyIsusersToCompanyIs;

    /**
     * @var \Permission
     *
     * @ManyToOne(targetEntity="Permission")
     * @JoinColumns({
     *   @JoinColumn(name="permission_idpermission", referencedColumnName="idpermission")
     * })
     */
    private $permissionpermission;



    /**
     * Get idusersPermission
     *
     * @return integer
     */
    public function getIdusersPermission()
    {
        return $this->idusersPermission;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return UsersPermissionInCompany
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return UsersPermissionInCompany
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return UsersPermissionInCompany
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set usersToCompanyIsusersToCompanyIs
     *
     * @param \UsersToCompanyIs $usersToCompanyIsusersToCompanyIs
     *
     * @return UsersPermissionInCompany
     */
    public function setUsersToCompanyIsusersToCompanyIs(\UsersToCompanyIs $usersToCompanyIsusersToCompanyIs = null)
    {
        $this->usersToCompanyIsusersToCompanyIs = $usersToCompanyIsusersToCompanyIs;

        return $this;
    }

    /**
     * Get usersToCompanyIsusersToCompanyIs
     *
     * @return \UsersToCompanyIs
     */
    public function getUsersToCompanyIsusersToCompanyIs()
    {
        return $this->usersToCompanyIsusersToCompanyIs;
    }

    /**
     * Set permissionpermission
     *
     * @param \Permission $permissionpermission
     *
     * @return UsersPermissionInCompany
     */
    public function setPermissionpermission(\Permission $permissionpermission = null)
    {
        $this->permissionpermission = $permissionpermission;

        return $this;
    }

    /**
     * Get permissionpermission
     *
     * @return \Permission
     */
    public function getPermissionpermission()
    {
        return $this->permissionpermission;
    }
}
