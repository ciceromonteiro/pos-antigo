<?php

/**
 * ProductPrices
 *
 * @Table(name="product_prices")
 * @Entity
 */
class ProductPrices {

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductprices;

    /**
     * @var integer
     *
     * @Column(name="users_idusers", type="bigint", nullable=true)
     */
    private $useridusers;

    /**
     * @var \DateTime
     *
     * @Column(name="start_time", type="datetime", nullable=true)
     */
    private $startime;

    /**
     * @var \DateTime
     *
     * @Column(name="end_time", type="datetime", nullable=true)
     */
    private $endtime;

    /**
     * @var float
     *
     * @Column(name="purchase_price", type="float", precision=10, scale=0, nullable=true)
     */
    private $purchase_price;

    /**
     * @var float
     *
     * @Column(name="sales_price", type="float", precision=10, scale=0, nullable=true)
     */
    private $salesprice;

    function getIdproductprices() {
        return $this->idproductprices;
    }

    function getUseridusers() {
        return $this->useridusers;
    }

    function getStartime() {
        return $this->startime;
    }

    function getEndtime() {
        return $this->endtime;
    }

    function getPurchase_price() {
        return $this->purchase_price;
    }

    function getSalesprice() {
        return $this->salesprice;
    }

    function setIdproductprices($idproductprices) {
        $this->idproductprices = $idproductprices;
    }

    function setUseridusers($useridusers) {
        $this->useridusers = $useridusers;
    }

    function setStartime(\DateTime $startime) {
        $this->startime = $startime;
    }

    function setEndtime(\DateTime $endtime) {
        $this->endtime = $endtime;
    }

    function setPurchase_price($purchase_price) {
        $this->purchase_price = $purchase_price;
    }

    function setSalesprice($salesprice) {
        $this->salesprice = $salesprice;
    }

}
