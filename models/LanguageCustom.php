<?php



use Doctrine\Mapping as ORM;

/**
 * LanguageCustom
 *
 * @Table(name="language_custom", indexes={@Index(name="language_derivative_index", columns={"language_derivative"})})
 * @Entity
 */
class LanguageCustom
{
    /**
     * @var integer
     *
     * @Column(name="idlanguagecustom", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlanguagecustom;

    /**
     * @var string
     *
     * @Column(name="data_type", type="text", length=65535, nullable=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @Column(name="data_value", type="text", length=65535, nullable=false)
     */
    private $dataValue;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Language
     *
     * @ManyToOne(targetEntity="Language")
     * @JoinColumns({
     *   @JoinColumn(name="language_derivative", referencedColumnName="idlanguage")
     * })
     */
    private $languageDerivative;

    function getIdlanguagecustom() {
        return $this->idlanguagecustom;
    }

    function getDataType() {
        return $this->dataType;
    }

    function getDataValue() {
        return $this->dataValue;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getLanguageDerivative() {
        return $this->languageDerivative;
    }

    function setIdlanguagecustom($idlanguagecustom) {
        $this->idlanguagecustom = $idlanguagecustom;
    }

    function setDataType($dataType) {
        $this->dataType = $dataType;
    }

    function setDataValue($dataValue) {
        $this->dataValue = $dataValue;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setLanguageDerivative(\Language $languageDerivative) {
        $this->languageDerivative = $languageDerivative;
    }


}

