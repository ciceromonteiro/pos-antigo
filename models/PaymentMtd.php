<?php

use Doctrine\Mapping as ORM;

/**
 * PaymentMtd
 *
 * @Table(name="payment_mtd")
 * @Entity
 */
class PaymentMtd {

    /**
     * @var integer
     *
     * @Column(name="idpayment_mtd", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpaymentMtd;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="path_platform", type="string", length=500, nullable=true)
     */
    private $pathPlatform;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var boolean
     *
     * @Column(name="invoice", type="boolean", nullable=false)
     */
    private $invoice;

    /**
     * @var boolean
     *
     * @Column(name="deadlinemonth", type="integer", nullable=false)
     */
    private $deadlinemonth;

    /**
     * @var boolean
     *
     * @Column(name="deadlineyear", type="integer", nullable=false)
     */
    private $deadlineyear;
    
    function getIdpaymentMtd() {
        return $this->idpaymentMtd;
    }

    function getName() {
        return $this->name;
    }

    function getPathPlatform() {
        return $this->pathPlatform;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getInvoice() {
        return $this->invoice;
    }

    function getDeadlinemonth() {
        return $this->deadlinemonth;
    }

    function getDeadlineyear() {
        return $this->deadlineyear;
    }

    function setIdpaymentMtd($idpaymentMtd) {
        $this->idpaymentMtd = $idpaymentMtd;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPathPlatform($pathPlatform) {
        $this->pathPlatform = $pathPlatform;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setInvoice($invoice) {
        $this->invoice = $invoice;
    }

    function setDeadlinemonth($deadlinemonth) {
        $this->deadlinemonth = $deadlinemonth;
    }

    function setDeadlineyear($deadlineyear) {
        $this->deadlineyear = $deadlineyear;
    }



}
