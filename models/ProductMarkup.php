<?php

use Doctrine\Mapping as ORM;

/**
 * ProductMarkup
 *
 * @Table(name="product_markup")
 * @Entity
 */
class ProductMarkup {

    /**
     * @var integer
     *
     * @Column(name="idproductmarkup", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductmarkup;

    /**
     * @var float
     *
     * @Column(name="frete", type="float", precision=22, scale=0, nullable=true)
     */
    private $frete;

    /**
     * @var string
     *
     * @Column(name="icms", type="string", length=10, nullable=false)
     */
    private $icms;

    /**
     * @var string
     *
     * @Column(name="fecop", type="string", length=10, nullable=false)
     */
    private $fecop;

    /**
     * @var string
     *
     * @Column(name="icms_simples", type="string", length=10, nullable=false)
     */
    private $icmsSimples;

    /**
     * @var string
     *
     * @Column(name="operational_cost", type="string", length=10, nullable=false)
     */
    private $operationalCost;

    /**
     * @var string
     *
     * @Column(name="commission", type="string", length=10, nullable=false)
     */
    private $commission;

    /**
     * @var string
     *
     * @Column(name="profit_intended", type="string", length=10, nullable=false)
     */
    private $profitIntended;

    /**
     * @var float
     *
     * @Column(name="price_suggested", type="float", precision=22, scale=0, nullable=true)
     */
    private $priceSuggested;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    function getIdproductmarkup() {
        return $this->idproductmarkup;
    }

    function getFrete() {
        return $this->frete;
    }

    function getIcms() {
        return $this->icms;
    }

    function getFecop() {
        return $this->fecop;
    }

    function getIcmsSimples() {
        return $this->icmsSimples;
    }

    function getOperationalCost() {
        return $this->operationalCost;
    }

    function getCommission() {
        return $this->commission;
    }

    function getProfitIntended() {
        return $this->profitIntended;
    }

    function getPriceSuggested() {
        return $this->priceSuggested;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function setIdproductmarkup($idproductmarkup) {
        $this->idproductmarkup = $idproductmarkup;
    }

    function setFrete($frete) {
        $this->frete = $frete;
    }

    function setIcms($icms) {
        $this->icms = $icms;
    }

    function setFecop($fecop) {
        $this->fecop = $fecop;
    }

    function setIcmsSimples($icmsSimples) {
        $this->icmsSimples = $icmsSimples;
    }

    function setOperationalCost($operationalCost) {
        $this->operationalCost = $operationalCost;
    }

    function setCommission($commission) {
        $this->commission = $commission;
    }

    function setProfitIntended($profitIntended) {
        $this->profitIntended = $profitIntended;
    }

    function setPriceSuggested($priceSuggested) {
        $this->priceSuggested = $priceSuggested;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

}
