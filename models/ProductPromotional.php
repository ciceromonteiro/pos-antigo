<?php



use Doctrine\Mapping as ORM;

/**
 * ProductPromotional
 *
 * @Table(name="product_promotional", indexes={@Index(name="product", columns={"idproduct"})})
 * @Entity
 */
class ProductPromotional
{
    /**
     * @var integer
     *
     * @Column(name="idproductpromotional", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductpromotional;

    /**
     * @var string
     *
     * @Column(name="gross_price", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $grossPrice;

    /**
     * @var \DateTime
     *
     * @Column(name="initial_date", type="datetime", nullable=false)
     */
    private $initialDate;

    /**
     * @var string
     *
     * @Column(name="price_without_tax", type="decimal", precision=10, scale=0, nullable=false)
     */
    private $priceWithoutTax;

    /**
     * @var \DateTime
     *
     * @Column(name="final_date", type="datetime", nullable=true)
     */
    private $finalDate;

    /**
     * @var string
     *
     * @Column(name="week", type="string", length=30, nullable=true)
     */
    private $week;
    
    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="idproduct", referencedColumnName="idproduct")
     * })
     */
    private $idproduct;

    function getIdproductpromotional() {
        return $this->idproductpromotional;
    }

    function getGrossPrice() {
        return $this->grossPrice;
    }

    function getInitialDate() {
        return $this->initialDate;
    }

    function getPriceWithoutTax() {
        return $this->priceWithoutTax;
    }

    function getFinalDate() {
        return $this->finalDate;
    }

    function getWeek() {
        return explode('-', $this->week);
    }

    function getWeekString() {
        return $this->week;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getIdproduct() {
        return $this->idproduct;
    }

    function setIdproductpromotional($idproductpromotional) {
        $this->idproductpromotional = $idproductpromotional;
    }

    function setGrossPrice($grossPrice) {
        $this->grossPrice = $grossPrice;
    }

    function setInitialDate(\DateTime $initialDate) {
        $this->initialDate = $initialDate;
    }

    function setPriceWithoutTax($priceWithoutTax) {
        $this->priceWithoutTax = $priceWithoutTax;
    }

    function setFinalDate(\DateTime $finalDate) {
        $this->finalDate = $finalDate;
    }

    function setWeek($week) {
        $this->week = $week;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdproduct(\Product $idproduct) {
        $this->idproduct = $idproduct;
    }


}

