<?php



use Doctrine\Mapping as ORM;

/**
 * InvoiceTmpt
 *
 * @Table(name="invoice_tmpt", indexes={@Index(name="fk_invoice_tmpt_tmpt_footer1_idx", columns={"tmpt_footer_idtmpt_footer"}), @Index(name="fk_invoice_tmpt_tmpt_header1_idx", columns={"tmpt_header_idtmpt_header"})})
 * @Entity
 */
class InvoiceTmpt
{
    /**
     * @var integer
     *
     * @Column(name="idinvoice_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idinvoiceTmpt;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="info", type="text", nullable=true)
     */
    private $info;

    /**
     * @var integer
     *
     * @Column(name="font_size", type="integer", nullable=false)
     */
    private $fontSize;

    /**
     * @var boolean
     *
     * @Column(name="alternate_color_line", type="boolean", nullable=false)
     */
    private $alternateColorLine;

    /**
     * @var boolean
     *
     * @Column(name="print_customer_phone", type="boolean", nullable=false)
     */
    private $printCustomerPhone;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \TmptFooter
     *
     * @ManyToOne(targetEntity="TmptFooter")
     * @JoinColumns({
     *   @JoinColumn(name="tmpt_footer_idtmpt_footer", referencedColumnName="idtmpt_footer")
     * })
     */
    private $tmptFootertmptFooter;

    /**
     * @var \TmptHeader
     *
     * @ManyToOne(targetEntity="TmptHeader")
     * @JoinColumns({
     *   @JoinColumn(name="tmpt_header_idtmpt_header", referencedColumnName="idtmpt_header")
     * })
     */
    private $tmptHeadertmptHeader;



    /**
     * Get idinvoiceTmpt
     *
     * @return integer
     */
    public function getIdinvoiceTmpt()
    {
        return $this->idinvoiceTmpt;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InvoiceTmpt
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return InvoiceTmpt
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set fontSize
     *
     * @param integer $fontSize
     *
     * @return InvoiceTmpt
     */
    public function setFontSize($fontSize)
    {
        $this->fontSize = $fontSize;

        return $this;
    }

    /**
     * Get fontSize
     *
     * @return integer
     */
    public function getFontSize()
    {
        return $this->fontSize;
    }

    /**
     * Set alternateColorLine
     *
     * @param boolean $alternateColorLine
     *
     * @return InvoiceTmpt
     */
    public function setAlternateColorLine($alternateColorLine)
    {
        $this->alternateColorLine = $alternateColorLine;

        return $this;
    }

    /**
     * Get alternateColorLine
     *
     * @return boolean
     */
    public function getAlternateColorLine()
    {
        return $this->alternateColorLine;
    }

    /**
     * Set printCustomerPhone
     *
     * @param boolean $printCustomerPhone
     *
     * @return InvoiceTmpt
     */
    public function setPrintCustomerPhone($printCustomerPhone)
    {
        $this->printCustomerPhone = $printCustomerPhone;

        return $this;
    }

    /**
     * Get printCustomerPhone
     *
     * @return boolean
     */
    public function getPrintCustomerPhone()
    {
        return $this->printCustomerPhone;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return InvoiceTmpt
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return InvoiceTmpt
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return InvoiceTmpt
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return InvoiceTmpt
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set tmptFootertmptFooter
     *
     * @param \TmptFooter $tmptFootertmptFooter
     *
     * @return InvoiceTmpt
     */
    public function setTmptFootertmptFooter(\TmptFooter $tmptFootertmptFooter = null)
    {
        $this->tmptFootertmptFooter = $tmptFootertmptFooter;

        return $this;
    }

    /**
     * Get tmptFootertmptFooter
     *
     * @return \TmptFooter
     */
    public function getTmptFootertmptFooter()
    {
        return $this->tmptFootertmptFooter;
    }

    /**
     * Set tmptHeadertmptHeader
     *
     * @param \TmptHeader $tmptHeadertmptHeader
     *
     * @return InvoiceTmpt
     */
    public function setTmptHeadertmptHeader(\TmptHeader $tmptHeadertmptHeader = null)
    {
        $this->tmptHeadertmptHeader = $tmptHeadertmptHeader;

        return $this;
    }

    /**
     * Get tmptHeadertmptHeader
     *
     * @return \TmptHeader
     */
    public function getTmptHeadertmptHeader()
    {
        return $this->tmptHeadertmptHeader;
    }
}
