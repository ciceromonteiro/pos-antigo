<?php



use Doctrine\Mapping as ORM;

/**
 * CompanyImage
 *
 * @Table(name="company_image", indexes={@Index(name="fk_company_image_company1_idx", columns={"company_idcompany"})})
 * @Entity
 */
class CompanyImage
{
    /**
     * @var integer
     *
     * @Column(name="idcompany_image", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcompanyImage;

    /**
     * @var string
     *
     * @Column(name="image", type="text")
     */
    private $image;

    /**
     * @var integer
     *
     * @Column(name="sequence", type="integer", nullable=true)
     */
    private $sequence;
    
    /**
     *
     * @var string
     * 
     * @Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;


    
    function getIdcompanyImage() {
        return $this->idcompanyImage;
    }

    function getImage() {
        return $this->image;
    }
    
    function getImageSrc(){                        
        // put this src in src img tag.
        // example:  <img src='$src' />
        $src = "data:{$this->getType()};base64," . base64_encode( $this->getImage() );
        
        return $src;
    }

    function getSequence() {
        return $this->sequence;
    }
    
    function getType() {
        return $this->type;
    }
    
    function getDateCreate(){
        return $this->dateCreate;
    }

    function getDateUpdate(){
        return $this->dateUpdate;
    }

    function getDateDelete(){
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getCompanycompany(){
        return $this->companycompany;
    }

    function setIdcompanyImage($idcompanyImage) {
        $this->idcompanyImage = $idcompanyImage;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setSequence($sequence) {
        $this->sequence = $sequence;
    }
    
    function setType($type) {
        $this->type = $type;
    }
    
    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setCompanycompany(\Company $companycompany) {
        $this->companycompany = $companycompany;
    }       

}
