<?php

use Doctrine\Mapping as ORM;

/**
 * Product
 *
 * @Table(name="product", indexes={@Index(name="fk_product_currency1_idx", columns={"currency_idcurrency"}), @Index(name="fk_product_unit1_idx", columns={"unit_idunit"}), @Index(name="fk_product_product_collection1_idx", columns={"product_collection_idcollection"}), @Index(name="fk_product_manufacturer1_idx", columns={"manufacturer_idmanufacturer"}), @Index(name="fk_product_supplier1_idx", columns={"supplier_idsupplier"}), @Index(name="fk_product_product_department1_idx", columns={"product_department_idproduct_department"}), @Index(name="fk_product_warranty1_idx", columns={"warranty_idwarranty"}), @Index(name="fk_product_section1_idx", columns={"section_idsection"}), @Index(name="fk_product_create_by_idx", columns={"create_by"}), @Index(name="fk_gp_tax", columns={"tax_gp"}), @Index(name="print_for_pick_ist", columns={"print_for_pick_ist"}), @Index(name="project", columns={"project"})})
 * @Entity
 */
class Product {

    /**
     * @var integer
     *
     * @Column(name="idproduct", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproduct;

    /**
     * @var string
     *
     * @Column(name="currency_purchase_price", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $currencyPurchasePrice;

    /**
     * @var string
     *
     * @Column(name="recom_value_for_sell_brute", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $recomValueForSellBrute;

    /**
     * @var string
     *
     * @Column(name="recom_value_for_sell_liquid", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $recomValueForSellLiquid;

    /**
     * @var string
     *
     * @Column(name="net_weight", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $netWeight;

    /**
     * @var string
     *
     * @Column(name="additional_weight", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $additionalWeight;

    /**
     * @var string
     *
     * @Column(name="volume", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $volume;

    /**
     * @var string
     *
     * @Column(name="length", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $length;

    /**
     * @var string
     *
     * @Column(name="width", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $width;

    /**
     * @var string
     *
     * @Column(name="height", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $height;

    /**
     * @var integer
     *
     * @Column(name="total_amount", type="integer", nullable=false)
     */
    private $totalAmount;

    /**
     * @var integer
     *
     * @Column(name="minimum_purchase_of_packages", type="integer", nullable=true)
     */
    private $minimumPurchaseOfPackages;

    /**
     * @var integer
     *
     * @Column(name="minimum_purchase", type="integer", nullable=false)
     */
    private $minimumPurchase = '1';
    /**
     * @var integer
     *
     * @Column(name="inventory_to_go", type="integer", nullable=false)
     */
    private $inventorytogo = '0';

    /**
     * @var integer
     *
     * @Column(name="numberPack", type="integer", nullable=true)
     */
    private $numberpack = '1';

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=80)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="sku", type="string", length=200)
     */
    private $sku;

    /**
     * @var string
     *
     * @Column(name="product_number", type="string", length=45, nullable=true)
     */
    private $productNumber;

    /**
     * @var string
     *
     * @Column(name="product_number_supplier", type="string", length=45, nullable=true)
     */
    private $productNumberSupplier;

    /**
     * @var string
     *
     * @Column(name="product_number_manufacturer", type="string", length=45, nullable=true)
     */
    private $productNumberManufacturer;

    /**
     * @var string
     *
     * @Column(name="price", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @Column(name="price_purchase", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $pricePurchase;

    /**
     * @var float
     *
     * @Column(name="contribution", type="float", precision=10, scale=0, nullable=true)
     */
    private $contribution;

    /**
     * @var string
     *
     * @Column(name="ncm", type="string", length=20, nullable=true)
     */
    private $ncm;

    /**
     * @var string
     *
     * @Column(name="cfop", type="string", length=20, nullable=true)
     */
    private $cfop;

    /**
     * @var float
     *
     * @Column(name="qtrib", type="float", precision=10, scale=0, nullable=true)
     */
    private $qtrib;

    /**
     * @var string
     *
     * @Column(name="cest", type="string", length=20, nullable=true)
     */
    private $cest;

    /**
     * @var string
     *
     * @Column(name="csosn", type="string", length=20, nullable=true)
     */
    private $csosn;

    /**
     * @var string
     *
     * @Column(name="cst", type="string", length=20, nullable=true)
     */
    private $cst;

    /**
     * @var string
     *
     * @Column(name="promotion_weekdays", type="string", length=13, nullable=true)
     */
    private $promotionWeekdays;

    /**
     * @var string
     *
     * @Column(name="price_recommended", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $priceRecommended;

    /**
     * @var string
     *
     * @Column(name="price_takeaway", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $pricetakeaway;

    /**
     * @var string
     *
     * @Column(name="value_for_travel_brute", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $valueForTravelBrute;

    /**
     * @var string
     *
     * @Column(name="value_for_travel_liquid", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $valueForTravelLiquid;

    /**
     * @var string
     *
     * @Column(name="price_purchase_list", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $pricePurchaseList;

    /**
     * @var string
     *
     * @Column(name="trade_value_for_sell_brute", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $tradeValueForSellBrute;

    /**
     * @var string
     *
     * @Column(name="trade_value_for_sell_liquid", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $tradeValueForSellLiquid;

    /**
     * @var string
     *
     * @Column(name="pricing_of_sell_value_without_tax", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $pricingOfSellValueWithoutTax;

    /**
     * @var string
     *
     * @Column(name="price_limit_bargain", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $priceLimitBargain;

    /**
     * @var string
     *
     * @Column(name="price_large_scale", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $priceLargeScale;

    /**
     * @var string
     *
     * @Column(name="profit_gross_profit_value", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $profitGrossProfitValue;

    /**
     * @var integer
     *
     * @Column(name="barcode", type="bigint", nullable=true)
     */
    private $barcode;

    /**
     * @var boolean
     *
     * @Column(name="stock_enabled", type="boolean", nullable=true)
     */
    private $stockEnabled = '1';

    /**
     * @var integer
     *
     * @Column(name="min_stock", type="integer", nullable=true)
     */
    private $minStock = '0';

    /**
     * @var integer
     *
     * @Column(name="max_stock", type="integer", nullable=true)
     */
    private $maxStock = '0';

    /**
     * @var string
     *
     * @Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="short_description", type="string", length=500, nullable=true)
     */
    private $shortDescription;

    /**
     * @var boolean
     *
     * @Column(name="has_serial_number", type="boolean", nullable=true)
     */
    private $hasSerialNumber = '0';

    /**
     * @var integer
     *
     * @Column(name="factor_purchase", type="integer", nullable=true)
     */
    private $factorPurchase = '1';

    /**
     * @var integer
     *
     * @Column(name="factor_sell", type="integer", nullable=false)
     */
    private $factorSell = '1';

    /**
     * @var boolean
     *
     * @Column(name="has_components", type="boolean", nullable=true)
     */
    private $hasComponents = '0';

    /**
     * @var integer
     *
     * @Column(name="components_show_order_subitens", type="integer", nullable=true)
     */
    private $componentsShowOrderSubitens;

    /**
     * @var boolean
     *
     * @Column(name="has_size_and_color", type="boolean", nullable=true)
     */
    private $hasSizeAndColor = '0';

    /**
     * @var boolean
     *
     * @Column(name="selling_product", type="boolean", nullable=true)
     */
    private $sellingProduct = '1';

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var integer
     *
     * @Column(name="output_control", type="integer", nullable=true)
     */
    private $outputControl;

    /**
     * @var integer
     *
     * @Column(name="type_product", type="integer", nullable=false)
     */
    private $typeProduct = '1';

    /**
     * @var integer
     *
     * @Column(name="prediction_of_purchase", type="integer", nullable=true)
     */
    private $predictionOfPurchase = '0';

    /**
     * @var string
     *
     * @Column(name="Importing_rate", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $importingRate;

    /**
     * @var integer
     *
     * @Column(name="freight_tax_amount", type="integer", nullable=true)
     */
    private $freightTaxAmount;

    /**
     * @var string
     *
     * @Column(name="value_of_shipping", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $valueOfShipping;

    /**
     * @var integer
     *
     * @Column(name="number_of_serie_from_product", type="integer", nullable=true)
     */
    private $numberOfSerieFromProduct;

    /**
     * @var integer
     *
     * @Column(name="base_of_calc", type="integer", nullable=true)
     */
    private $baseOfCalc;

    /**
     * @var integer
     *
     * @Column(name="multiple_units", type="integer", nullable=true)
     */
    private $multipleUnits;

    /**
     * @var \Tribute
     *
     * @ManyToOne(targetEntity="Tribute")
     * @JoinColumns({
     *   @JoinColumn(name="tax_gp", referencedColumnName="idtribute")
     * })
     */
    private $taxGp;

    /**
     * @var \Printer
     *
     * @ManyToOne(targetEntity="Printer")
     * @JoinColumns({
     *   @JoinColumn(name="print_for_pick_ist", referencedColumnName="idprinter")
     * })
     */
    private $printForPickIst;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="create_by", referencedColumnName="idusers")
     * })
     */
    private $createBy;

    /**
     * @var \Currency
     *
     * @ManyToOne(targetEntity="Currency")
     * @JoinColumns({
     *   @JoinColumn(name="currency_idcurrency", referencedColumnName="idcurrency")
     * })
     */
    private $currencycurrency;

    /**
     * @var \Manufacturer
     *
     * @ManyToOne(targetEntity="Manufacturer")
     * @JoinColumns({
     *   @JoinColumn(name="manufacturer_idmanufacturer", referencedColumnName="idmanufacturer")
     * })
     */
    private $manufacturermanufacturer;

    /**
     * @var \ProductCollection
     *
     * @ManyToOne(targetEntity="ProductCollection")
     * @JoinColumns({
     *   @JoinColumn(name="product_collection_idcollection", referencedColumnName="idcollection")
     * })
     */
    private $productCollectioncollection;

    /**
     * @var \Department
     *
     * @ManyToOne(targetEntity="Department")
     * @JoinColumns({
     *   @JoinColumn(name="product_department_idproduct_department", referencedColumnName="iddepartment")
     * })
     */
    private $productDepartmentproductDepartment;

    /**
     * @var \Project
     *
     * @ManyToOne(targetEntity="Project")
     * @JoinColumns({
     *   @JoinColumn(name="project", referencedColumnName="idproject")
     * })
     */
    private $project;

    /**
     * @var \Warehouse
     *
     * @ManyToOne(targetEntity="Warehouse")
     * @JoinColumns({
     *   @JoinColumn(name="section_idsection", referencedColumnName="idwarehouse")
     * })
     */
    private $sectionsection;

    /**
     * @var \Supplier
     *
     * @ManyToOne(targetEntity="Supplier")
     * @JoinColumns({
     *   @JoinColumn(name="supplier_idsupplier", referencedColumnName="idsupplier")
     * })
     */
    private $suppliersupplier;

    /**
     * @var \Unit
     *
     * @ManyToOne(targetEntity="Unit")
     * @JoinColumns({
     *   @JoinColumn(name="unit_idunit", referencedColumnName="idunit")
     * })
     */
    private $unitunit;

    /**
     * @var \Warranty
     *
     * @ManyToOne(targetEntity="Warranty")
     * @JoinColumns({
     *   @JoinColumn(name="warranty_idwarranty", referencedColumnName="idwarranty")
     * })
     */
    private $warrantywarranty;

    function getIdproduct() {
        return $this->idproduct;
    }

    function getCurrencyPurchasePrice() {
        return $this->currencyPurchasePrice;
    }

    function getRecomValueForSellBrute() {
        return $this->recomValueForSellBrute;
    }

    function getRecomValueForSellLiquid() {
        return $this->recomValueForSellLiquid;
    }

    function getNetWeight() {
        return $this->netWeight;
    }

    function getAdditionalWeight() {
        return $this->additionalWeight;
    }

    function getVolume() {
        return $this->volume;
    }

    function getLength() {
        return $this->length;
    }

    function getWidth() {
        return $this->width;
    }

    function getHeight() {
        return $this->height;
    }

    function getTotalAmount() {
        return $this->totalAmount;
    }

    function getInventoryToGo() {
        return $this->inventorytogo;
    }

    function getMinimumPurchaseOfPackages() {
        return $this->minimumPurchaseOfPackages;
    }

    function getMinimumPurchase() {
        return $this->minimumPurchase;
    }

    function getNumberpack() {
        return $this->numberpack;
    }

    function getName() {
        return $this->name;
    }

    function getProductNumber() {
        return $this->productNumber;
    }

    function getProductNumberSupplier() {
        return $this->productNumberSupplier;
    }

    function getProductNumberManufacturer() {
        return $this->productNumberManufacturer;
    }

    function getPrice() {
        return $this->price;
    }

    function getPricePurchase() {
        return $this->pricePurchase;
    }

    function getContribution() {
        return $this->contribution;
    }

    function getNcm() {
        return $this->ncm;
    }

    function getCfop() {
        return $this->cfop;
    }

    function getQtrib() {
        return $this->qtrib;
    }

    function getCest() {
        return $this->cest;
    }

    function getCsosn() {
        return $this->csosn;
    }

    function getCst() {
        return $this->cst;
    }

    function getSku() {
        return $this->sku;
    }

    function getPromotionWeekdays() {
        return $this->promotionWeekdays;
    }

    function getPriceRecommended() {
        return $this->priceRecommended;
    }

    function getPricetakeaway() {
        return $this->pricetakeaway;
    }

    function getValueForTravelBrute() {
        return $this->valueForTravelBrute;
    }

    function getValueForTravelLiquid() {
        return $this->valueForTravelLiquid;
    }

    function getPricePurchaseList() {
        return $this->pricePurchaseList;
    }

    function getTradeValueForSellBrute() {
        return $this->tradeValueForSellBrute;
    }

    function getTradeValueForSellLiquid() {
        return $this->tradeValueForSellLiquid;
    }

    function getPricingOfSellValueWithoutTax() {
        return $this->pricingOfSellValueWithoutTax;
    }

    function getPriceLimitBargain() {
        return $this->priceLimitBargain;
    }

    function getPriceLargeScale() {
        return $this->priceLargeScale;
    }

    function getProfitGrossProfitValue() {
        return $this->profitGrossProfitValue;
    }

    function getBarcode() {
        return $this->barcode;
    }

    function getStockEnabled() {
        return $this->stockEnabled;
    }

    function getMinStock() {
        return $this->minStock;
    }

    function getMaxStock() {
        return $this->maxStock;
    }

    function getDescription() {
        return $this->description;
    }

    function getShortDescription() {
        return $this->shortDescription;
    }

    function getHasSerialNumber() {
        return $this->hasSerialNumber;
    }

    function getFactorPurchase() {
        return $this->factorPurchase;
    }

    function getFactorSell() {
        return $this->factorSell;
    }

    function getHasComponents() {
        return $this->hasComponents;
    }

    function getComponentsShowOrderSubitens() {
        return $this->componentsShowOrderSubitens;
    }

    function getHasSizeAndColor() {
        return $this->hasSizeAndColor;
    }

    function getSellingProduct() {
        return $this->sellingProduct;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getOutputControl() {
        return $this->outputControl;
    }

    function getTypeProduct() {
        return $this->typeProduct;
    }

    function getPredictionOfPurchase() {
        return $this->predictionOfPurchase;
    }

    function getImportingRate() {
        return $this->importingRate;
    }

    function getFreightTaxAmount() {
        return $this->freightTaxAmount;
    }

    function getValueOfShipping() {
        return $this->valueOfShipping;
    }

    function getNumberOfSerieFromProduct() {
        return $this->numberOfSerieFromProduct;
    }

    function getBaseOfCalc() {
        return $this->baseOfCalc;
    }

    function getMultipleUnits() {
        return $this->multipleUnits;
    }

    function getTaxGp() {
        return $this->taxGp;
    }

    function getPrintForPickIst() {
        return $this->printForPickIst;
    }

    function getCreateBy() {
        return $this->createBy;
    }

    function getCurrencycurrency() {
        return $this->currencycurrency;
    }

    function getManufacturermanufacturer() {
        return $this->manufacturermanufacturer;
    }

    function getProductCollectioncollection() {
        return $this->productCollectioncollection;
    }

    function getProductDepartmentproductDepartment() {
        return $this->productDepartmentproductDepartment;
    }

    function getProject() {
        return $this->project;
    }

    function getSectionsection() {
        return $this->sectionsection;
    }

    function getSuppliersupplier() {
        return $this->suppliersupplier;
    }

    function getUnitunit() {
        return $this->unitunit;
    }

    function getWarrantywarranty() {
        return $this->warrantywarranty;
    }

    function setIdproduct($idproduct) {
        $this->idproduct = $idproduct;
    }

    function setCurrencyPurchasePrice($currencyPurchasePrice) {
        $this->currencyPurchasePrice = $currencyPurchasePrice;
    }

    function setRecomValueForSellBrute($recomValueForSellBrute) {
        $this->recomValueForSellBrute = $recomValueForSellBrute;
    }

    function setRecomValueForSellLiquid($recomValueForSellLiquid) {
        $this->recomValueForSellLiquid = $recomValueForSellLiquid;
    }

    function setNetWeight($netWeight) {
        $this->netWeight = $netWeight;
    }

    function setAdditionalWeight($additionalWeight) {
        $this->additionalWeight = $additionalWeight;
    }

    function setVolume($volume) {
        $this->volume = $volume;
    }

    function setLength($length) {
        $this->length = $length;
    }

    function setWidth($width) {
        $this->width = $width;
    }

    function setHeight($height) {
        $this->height = $height;
    }

    function setTotalAmount($totalAmount) {
        $this->totalAmount = $totalAmount;
    }

    function setInventoryToGo($inventorytogo) {
        $this->inventorytogo = $inventorytogo;
    }

    function setMinimumPurchaseOfPackages($minimumPurchaseOfPackages) {
        $this->minimumPurchaseOfPackages = $minimumPurchaseOfPackages;
    }

    function setMinimumPurchase($minimumPurchase) {
        $this->minimumPurchase = $minimumPurchase;
    }

    function setNumberpack($numberpack) {
        $this->numberpack = $numberpack;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setSku($sku) {
        $this->sku = $sku;
    }

    function setProductNumber($productNumber) {
        $this->productNumber = $productNumber;
    }

    function setProductNumberSupplier($productNumberSupplier) {
        $this->productNumberSupplier = $productNumberSupplier;
    }

    function setProductNumberManufacturer($productNumberManufacturer) {
        $this->productNumberManufacturer = $productNumberManufacturer;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setPricePurchase($pricePurchase) {
        $this->pricePurchase = $pricePurchase;
    }

    function setContribution($contribution) {
        $this->contribution = $contribution;
    }

    function setNcm($ncm) {
        $this->ncm = $ncm;
    }

    function setCfop($cfop) {
        $this->cfop = $cfop;
    }

    function setQtrib($qtrib) {
        $this->qtrib = $qtrib;
    }

    function setCest($cest) {
        $this->cest = $cest;
    }

    function setCsosn($csosn) {
        $this->csosn = $csosn;
    }

    function setCst($cst) {
        $this->cst = $cst;
    }

    function setPromotionWeekdays($promotionWeekdays) {
        $this->promotionWeekdays = $promotionWeekdays;
    }

    function setPriceRecommended($priceRecommended) {
        $this->priceRecommended = $priceRecommended;
    }

    function setPricetakeaway($pricetakeaway) {
        $this->pricetakeaway = $pricetakeaway;
    }

    function setValueForTravelBrute($valueForTravelBrute) {
        $this->valueForTravelBrute = $valueForTravelBrute;
    }

    function setValueForTravelLiquid($valueForTravelLiquid) {
        $this->valueForTravelLiquid = $valueForTravelLiquid;
    }

    function setPricePurchaseList($pricePurchaseList) {
        $this->pricePurchaseList = $pricePurchaseList;
    }

    function setTradeValueForSellBrute($tradeValueForSellBrute) {
        $this->tradeValueForSellBrute = $tradeValueForSellBrute;
    }

    function setTradeValueForSellLiquid($tradeValueForSellLiquid) {
        $this->tradeValueForSellLiquid = $tradeValueForSellLiquid;
    }

    function setPricingOfSellValueWithoutTax($pricingOfSellValueWithoutTax) {
        $this->pricingOfSellValueWithoutTax = $pricingOfSellValueWithoutTax;
    }

    function setPriceLimitBargain($priceLimitBargain) {
        $this->priceLimitBargain = $priceLimitBargain;
    }

    function setPriceLargeScale($priceLargeScale) {
        $this->priceLargeScale = $priceLargeScale;
    }

    function setProfitGrossProfitValue($profitGrossProfitValue) {
        $this->profitGrossProfitValue = $profitGrossProfitValue;
    }

    function setBarcode($barcode) {
        $this->barcode = $barcode;
    }

    function setStockEnabled($stockEnabled) {
        $this->stockEnabled = $stockEnabled;
    }

    function setMinStock($minStock) {
        $this->minStock = $minStock;
    }

    function setMaxStock($maxStock) {
        $this->maxStock = $maxStock;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setShortDescription($shortDescription) {
        $this->shortDescription = $shortDescription;
    }

    function setHasSerialNumber($hasSerialNumber) {
        $this->hasSerialNumber = $hasSerialNumber;
    }

    function setFactorPurchase($factorPurchase) {
        $this->factorPurchase = $factorPurchase;
    }

    function setFactorSell($factorSell) {
        $this->factorSell = $factorSell;
    }

    function setHasComponents($hasComponents) {
        $this->hasComponents = $hasComponents;
    }

    function setComponentsShowOrderSubitens($componentsShowOrderSubitens) {
        $this->componentsShowOrderSubitens = $componentsShowOrderSubitens;
    }

    function setHasSizeAndColor($hasSizeAndColor) {
        $this->hasSizeAndColor = $hasSizeAndColor;
    }

    function setSellingProduct($sellingProduct) {
        $this->sellingProduct = $sellingProduct;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setOutputControl($outputControl) {
        $this->outputControl = $outputControl;
    }

    function setTypeProduct($typeProduct) {
        $this->typeProduct = $typeProduct;
    }

    function setPredictionOfPurchase($predictionOfPurchase) {
        $this->predictionOfPurchase = $predictionOfPurchase;
    }

    function setImportingRate($importingRate) {
        $this->importingRate = $importingRate;
    }

    function setFreightTaxAmount($freightTaxAmount) {
        $this->freightTaxAmount = $freightTaxAmount;
    }

    function setValueOfShipping($valueOfShipping) {
        $this->valueOfShipping = $valueOfShipping;
    }

    function setNumberOfSerieFromProduct($numberOfSerieFromProduct) {
        $this->numberOfSerieFromProduct = $numberOfSerieFromProduct;
    }

    function setBaseOfCalc($baseOfCalc) {
        $this->baseOfCalc = $baseOfCalc;
    }

    function setMultipleUnits($multipleUnits) {
        $this->multipleUnits = $multipleUnits;
    }

    function setTaxGp(\Tribute $taxGp) {
        $this->taxGp = $taxGp;
    }

    function setPrintForPickIst(\Printer $printForPickIst) {
        $this->printForPickIst = $printForPickIst;
    }

    function setCreateBy(\Users $createBy) {
        $this->createBy = $createBy;
    }

    function setCurrencycurrency(\Currency $currencycurrency) {
        $this->currencycurrency = $currencycurrency;
    }

    function setManufacturermanufacturer($manufacturermanufacturer) {
        $this->manufacturermanufacturer = $manufacturermanufacturer;
    }

    function setProductCollectioncollection(\ProductCollection $productCollectioncollection) {
        $this->productCollectioncollection = $productCollectioncollection;
    }

    function setProductDepartmentproductDepartment(\Department $productDepartmentproductDepartment) {
        $this->productDepartmentproductDepartment = $productDepartmentproductDepartment;
    }

    function setProject(\Project $project) {
        $this->project = $project;
    }

    function setSectionsection(\Warehouse $sectionsection) {
        $this->sectionsection = $sectionsection;
    }

    function setSuppliersupplier($suppliersupplier) {
        $this->suppliersupplier = $suppliersupplier;
    }

    function setUnitunit(\Unit $unitunit) {
        $this->unitunit = $unitunit;
    }

    function setWarrantywarranty(\Warranty $warrantywarranty) {
        $this->warrantywarranty = $warrantywarranty;
    }

}
