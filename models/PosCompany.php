<?php

use Doctrine\Mapping as ORM;

/**
 * PosCompany
 *
 * @Table(name="pos_company", indexes={@Index(name="fk_pos_company_zz_state1_idx", columns={"zz_state_idzz_state"})})
 * @Entity
 */
class PosCompany {

    /**
     * @var integer
     *
     * @Column(name="idpos_company", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposCompany;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="fantasy_name", type="string", length=100, nullable=false)
     */
    private $fantasyName;

    /**
     * @var string
     *
     * @Column(name="register_number", type="string", length=45, nullable=false)
     */
    private $registerNumber;

    /**
     * @var string
     * 
     * @Column(name="license", type="string", nullable=true)
     * 
     */
    private $license;

    /**
     * @var string
     *
     * @Column(name="address_street", type="string", length=100, nullable=false)
     */
    private $addressStreet;

    /**
     * @var string
     *
     * @Column(name="address_number", type="string", length=14, nullable=true)
     */
    private $addressNumber;

    /**
     * @var string
     *
     * @Column(name="address_district", type="string", length=45, nullable=false)
     */
    private $addressDistrict;

    /**
     * @var string
     *
     * @Column(name="address_city", type="string", length=100, nullable=false)
     */
    private $addressCity;

    /**
     * @var string
     *
     * @Column(name="address_zipcode", type="string", length=45, nullable=false)
     */
    private $addressZipcode;

    /**
     * @var string
     *
     * @Column(name="address_complement", type="string", length=200, nullable=true)
     */
    private $addressComplement;

    /**
     * @var string
     *
     * @Column(name="address_reference", type="string", length=200, nullable=true)
     */
    private $addressReference;

    /**
     * @var string
     *
     * @Column(name="vaddress_street", type="string", length=100, nullable=false)
     */
    private $vaddressStreet;

    /**
     * @var string
     *
     * @Column(name="vaddress_number", type="string", length=14, nullable=true)
     */
    private $vaddressNumber;

    /**
     * @var string
     *
     * @Column(name="vaddress_district", type="string", length=45, nullable=false)
     */
    private $vaddressDistrict;

    /**
     * @var string
     *
     * @Column(name="vaddress_city", type="string", length=100, nullable=false)
     */
    private $vaddressCity;

    /**
     * @var string
     *
     * @Column(name="vaddress_zipcode", type="string", length=45, nullable=false)
     */
    private $vaddressZipcode;

    /**
     * @var string
     *
     * @Column(name="vaddress_complement", type="string", length=200, nullable=true)
     */
    private $vaddressComplement;

    /**
     * @var string
     *
     * @Column(name="vaddress_reference", type="string", length=200, nullable=true)
     */
    private $vaddressReference;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @Column(name="bank_account", type="string", length=45, nullable=true)
     */
    private $bankAccount;

    /**
     * @var string
     *
     * @Column(name="iban", type="string", length=45, nullable=true)
     */
    private $iban;

    /**
     * @var string
     *
     * @Column(name="bic", type="string", length=45, nullable=true)
     */
    private $bic;

    /**
     * @var integer
     *
     * @Column(name="bank_br", type="integer", nullable=true)
     */
    private $bankBr;

    /**
     * @var string
     *
     * @Column(name="agency_br", type="string", length=45, nullable=true)
     */
    private $agencyBr;

    /**
     * @var string
     *
     * @Column(name="account_br", type="string", length=45, nullable=true)
     */
    private $accountBr;

    /**
     * @var string
     *
     * @Column(name="digit_account_br", type="string", length=45, nullable=true)
     */
    private $digitAccountBr;

    /**
     * @var string
     *
     * @Column(name="bank_wallet_br", type="string", length=45, nullable=true)
     */
    private $bankWalletBr;

    /**
     * @var integer
     *
     * @Column(name="specie_br", type="integer", nullable=true)
     */
    private $specieBr;

    /**
     * @var string
     *
     * @Column(name="interest_day_maturity_invoice_br", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $interestDayMaturityInvoiceBr;

    /**
     * @var integer
     *
     * @Column(name="days_after_maturity_invoice_br", type="integer", nullable=true)
     */
    private $daysAfterMaturityInvoiceBr;

    /**
     * @var string
     *
     * @Column(name="value_of_mulct_br", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $valueOfMulctBr;

    /**
     * @var string
     *
     * @Column(name="code_agreement_br", type="string", length=45, nullable=true)
     */
    private $codeAgreementBr;

    /**
     * @var string
     *
     * @Column(name="wallet_variation_br", type="string", length=45, nullable=true)
     */
    private $walletVariationBr;

    /**
     * @var string
     *
     * @Column(name="agency_dv_br", type="string", length=45, nullable=true)
     */
    private $agencyDvBr;

    /**
     * @var string
     *
     * @Column(name="operation_br", type="string", length=45, nullable=true)
     */
    private $operationBr;

    /**
     * @var string
     *
     * @Column(name="sequence_number_file_br", type="string", length=45, nullable=true)
     */
    private $sequenceNumberFileBr;

    /**
     * @var integer
     *
     * @Column(name="wallet_code_br", type="integer", nullable=true)
     */
    private $walletCodeBr;

    /**
     * @var string
     *
     * @Column(name="aceite_br", type="string", length=45, nullable=true)
     */
    private $aceiteBr;

    /**
     * @var string
     *
     * @Column(name="assignor_code_br", type="string", length=45, nullable=true)
     */
    private $assignorCodeBr;

    /**
     * @var integer
     *
     * @Column(name="registered_invoice_br", type="integer", nullable=true)
     */
    private $registeredInvoiceBr;

    /**
     * @var string
     *
     * @Column(name="default_message_invoice", type="text", nullable=true)
     */
    private $defaultMessageInvoice;

    /**
     * @var \ZzState
     *
     * @ManyToOne(targetEntity="ZzState")
     * @JoinColumns({
     *   @JoinColumn(name="zz_state_idzz_state", referencedColumnName="idzz_state")
     * })
     */
    private $zzStatezzState;

    function getIdposCompany() {
        return $this->idposCompany;
    }

    function getName() {
        return $this->name;
    }

    function getFantasyName() {
        return $this->fantasyName;
    }

    function getRegisterNumber() {
        return $this->registerNumber;
    }

    function getLicense() {
        return $this->license;
    }

    function getAddressStreet() {
        return $this->addressStreet;
    }

    function getAddressNumber() {
        return $this->addressNumber;
    }

    function getAddressDistrict() {
        return $this->addressDistrict;
    }

    function getAddressCity() {
        return $this->addressCity;
    }

    function getAddressZipcode() {
        return $this->addressZipcode;
    }

    function getAddressComplement() {
        return $this->addressComplement;
    }

    function getAddressReference() {
        return $this->addressReference;
    }

    function getVaddressStreet() {
        return $this->vaddressStreet;
    }

    function getVaddressNumber() {
        return $this->vaddressNumber;
    }

    function getVaddressDistrict() {
        return $this->vaddressDistrict;
    }

    function getVaddressCity() {
        return $this->vaddressCity;
    }

    function getVaddressZipcode() {
        return $this->vaddressZipcode;
    }

    function getVaddressComplement() {
        return $this->vaddressComplement;
    }

    function getVaddressReference() {
        return $this->vaddressReference;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getBankAccount() {
        return $this->bankAccount;
    }

    function getIban() {
        return $this->iban;
    }

    function getBic() {
        return $this->bic;
    }

    function getBankBr() {
        return $this->bankBr;
    }

    function getAgencyBr() {
        return $this->agencyBr;
    }

    function getAccountBr() {
        return $this->accountBr;
    }

    function getBankWalletBr() {
        return $this->bankWalletBr;
    }

    function getDigitAccountBr() {
        return $this->digitAccountBr;
    }

    function getSpecieBr() {
        return $this->specieBr;
    }

    function getInterestDayMaturityInvoiceBr() {
        return $this->interestDayMaturityInvoiceBr;
    }

    function getDaysAfterMaturityInvoiceBr() {
        return $this->daysAfterMaturityInvoiceBr;
    }

    function getValueOfMulctBr() {
        return $this->valueOfMulctBr;
    }

    function getCodeAgreementBr() {
        return $this->codeAgreementBr;
    }

    function getWalletVariationBr() {
        return $this->walletVariationBr;
    }

    function getAgencyDvBr() {
        return $this->agencyDvBr;
    }

    function getOperationBr() {
        return $this->operationBr;
    }

    function getSequenceNumberFileBr() {
        return $this->sequenceNumberFileBr;
    }

    function getWalletCodeBr() {
        return $this->walletCodeBr;
    }

    function getAceiteBr() {
        return $this->aceiteBr;
    }

    function getAssignorCodeBr() {
        return $this->assignorCodeBr;
    }

    function getRegisteredInvoiceBr() {
        return $this->registeredInvoiceBr;
    }

    function getPhone() {
        $phone = getEm()->getRepository('CompanyInfo')->findOneBy(array('posCompanycompany' => $this->getIdposCompany()));
        var_dump($phone);
        die();
        return $phone;
    }

    function getDefaultMessageInvoice() {
        return $this->defaultMessageInvoice;
    }

    function getZzStatezzState() {
        return $this->zzStatezzState;
    }

    function setIdposCompany($idposCompany) {
        $this->idposCompany = $idposCompany;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setFantasyName($fantasyName) {
        $this->fantasyName = $fantasyName;
    }

    function setRegisterNumber($registerNumber) {
        $this->registerNumber = $registerNumber;
    }

    function setLicense($license) {
        $this->license = $license;
    }

    function setAddressStreet($addressStreet) {
        $this->addressStreet = $addressStreet;
    }

    function setAddressNumber($addressNumber) {
        $this->addressNumber = $addressNumber;
    }

    function setAddressDistrict($addressDistrict) {
        $this->addressDistrict = $addressDistrict;
    }

    function setAddressCity($addressCity) {
        $this->addressCity = $addressCity;
    }

    function setAddressZipcode($addressZipcode) {
        $this->addressZipcode = $addressZipcode;
    }

    function setAddressComplement($addressComplement) {
        $this->addressComplement = $addressComplement;
    }

    function setAddressReference($addressReference) {
        $this->addressReference = $addressReference;
    }

    function setVaddressStreet($vaddressStreet) {
        $this->vaddressStreet = $vaddressStreet;
    }

    function setVaddressNumber($vaddressNumber) {
        $this->vaddressNumber = $vaddressNumber;
    }

    function setVaddressDistrict($vaddressDistrict) {
        $this->vaddressDistrict = $vaddressDistrict;
    }

    function setVaddressCity($vaddressCity) {
        $this->vaddressCity = $vaddressCity;
    }

    function setVaddressZipcode($vaddressZipcode) {
        $this->vaddressZipcode = $vaddressZipcode;
    }

    function setVaddressComplement($vaddressComplement) {
        $this->vaddressComplement = $vaddressComplement;
    }

    function setVaddressReference($vaddressReference) {
        $this->vaddressReference = $vaddressReference;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setBankAccount($bankAccount) {
        $this->bankAccount = $bankAccount;
    }

    function setIban($iban) {
        $this->iban = $iban;
    }

    function setBic($bic) {
        $this->bic = $bic;
    }

//defaultMessageInvoice

    function setBankBr($bankBr) {
        $this->bankBr = $bankBr;
    }

    function setAgencyBr($agencyBr) {
        $this->agencyBr = $agencyBr;
    }

    function setAccountBr($accountBr) {
        $this->accountBr = $accountBr;
    }

    function setBankWalletBr($bankWalletBr) {
        $this->bankWalletBr = $bankWalletBr;
    }

    function setDigitAccountBr($digitAccountBr) {
        $this->digitAccountBr = $digitAccountBr;
    }

    function setSpecieBr($specieBr) {
        $this->specieBr = $specieBr;
    }

    function setInterestDayMaturityInvoiceBr($interestDayMaturityInvoiceBr) {
        $this->interestDayMaturityInvoiceBr = $interestDayMaturityInvoiceBr;
    }

    function setDaysAfterMaturityInvoiceBr($daysAfterMaturityInvoiceBr) {
        $this->daysAfterMaturityInvoiceBr = $daysAfterMaturityInvoiceBr;
    }

    function setValueOfMulctBr($valueOfMulctBr) {
        $this->valueOfMulctBr = $valueOfMulctBr;
    }

    function setCodeAgreementBr($codeAgreementBr) {
        $this->codeAgreementBr = $codeAgreementBr;
    }

    function setWalletVariationBr($walletVariationBr) {
        $this->walletVariationBr = $walletVariationBr;
    }

    function setAgencyDvBr($agencyDvBr) {
        $this->agencyDvBr = $agencyDvBr;
    }

    function setOperationBr($operationBr) {
        $this->operationBr = $operationBr;
    }

    function setSequenceNumberFileBr($sequenceNumberFileBr) {
        $this->sequenceNumberFileBr = $sequenceNumberFileBr;
    }

    function setWalletCodeBr($walletCodeBr) {
        $this->walletCodeBr = $walletCodeBr;
    }

    function setAceiteBr($aceiteBr) {
        $this->aceiteBr = $aceiteBr;
    }

    function setAssignorCodeBr($assignorCodeBr) {
        $this->assignorCodeBr = $assignorCodeBr;
    }

    function setRegisteredInvoiceBr($registeredInvoiceBr) {
        $this->registeredInvoiceBr = $registeredInvoiceBr;
    }

    function setDefaultMessageInvoice($defaultMessageInvoice) {
        $this->defaultMessageInvoice = $defaultMessageInvoice;
    }

    function setPhone($phone) {
        $current_phone = $this->getEm()->getRepository('CompanyInfo')->findOneBy(array('pos_company_idpos_company' => $this->getIdposCompany(), 'data_type' => 'phone'));
        if ($current_phone) {
            $current_phone->setDataValue($phone);
        } else {
            $current_phone = new CompanyInfo();
            $current_phone->setDataType('phone');
            $current_phone->setDataValue($phone);
            $current_phone->setIdCompanyInfo($idCompanyInfo);
        }
    }

   

    function setZzStatezzState(\ZzState $zzStatezzState) {
        $this->zzStatezzState = $zzStatezzState;
    }

    function getAddress() {
        $address = $this->addressStreet . ", " . $this->addressNumber . " - " . $this->addressDistrict;
        return $address;
    }

}
