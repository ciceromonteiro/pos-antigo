<?php



use Doctrine\Mapping as ORM;

/**
 * ZzDefaultUsersType
 *
 * @Table(name="zz_default_users_type")
 * @Entity
 */
class ZzDefaultUsersType
{
    /**
     * @var integer
     *
     * @Column(name="idzz_default_users_type", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idzzDefaultUsersType;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;



    /**
     * Get idzzDefaultUsersType
     *
     * @return integer
     */
    public function getIdzzDefaultUsersType()
    {
        return $this->idzzDefaultUsersType;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ZzDefaultUsersType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
