<?php

use Doctrine\Mapping as ORM;

/**
 * WordOriginal
 *
 * @Table(name="word_original")
 * @Entity
 */
class WordOriginal {

    /**
     * @var integer
     *
     * @Column(name="idword_original", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idwordOriginal;

    /**
     * @var string
     *
     * @Column(name="language", type="string", length=45, nullable=false)
     */
    private $language;

    /**
     * @var string
     *
     * @Column(name="word", type="string", length=45, nullable=false)
     */
    private $Word;

    function getIdwordOriginal() {
        return $this->idwordOriginal;
    }

    function getLanguage() {
        return $this->language;
    }

    function getWord() {
        return $this->Word;
    }

    function setIdwordOriginal($idwordOriginal) {
        $this->idwordOriginal = $idwordOriginal;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setWord($Word) {
        $this->Word = $Word;
    }


}
