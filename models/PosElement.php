<?php



use Doctrine\Mapping as ORM;

/**
 * PosElement
 *
 * @Table(name="pos_element", indexes={@Index(name="fk_pos_element_pos_elmt_type1_idx", columns={"pos_elmt_type_idpos_elmt_type"})})
 * @Entity
 */
class PosElement
{
    /**
     * @var integer
     *
     * @Column(name="idpos_element", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposElement;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \PosElmtType
     *
     * @ManyToOne(targetEntity="PosElmtType")
     * @JoinColumns({
     *   @JoinColumn(name="pos_elmt_type_idpos_elmt_type", referencedColumnName="idpos_elmt_type")
     * })
     */
    private $posElmtTypeposElmtType;



    /**
     * Get idposElement
     *
     * @return integer
     */
    public function getIdposElement()
    {
        return $this->idposElement;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PosElement
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return PosElement
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return PosElement
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return PosElement
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PosElement
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set posElmtTypeposElmtType
     *
     * @param \PosElmtType $posElmtTypeposElmtType
     *
     * @return PosElement
     */
    public function setPosElmtTypeposElmtType(\PosElmtType $posElmtTypeposElmtType = null)
    {
        $this->posElmtTypeposElmtType = $posElmtTypeposElmtType;

        return $this;
    }

    /**
     * Get posElmtTypeposElmtType
     *
     * @return \PosElmtType
     */
    public function getPosElmtTypeposElmtType()
    {
        return $this->posElmtTypeposElmtType;
    }
}
