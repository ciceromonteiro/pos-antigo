<?php



use Doctrine\Mapping as ORM;

/**
 * ProductIncludedAndSubstitute
 *
 * @Table(name="product_included_and_substitute", indexes={@Index(name="product_idproduct", columns={"product_idproduct"}), @Index(name="product_incl_subs", columns={"product_incl_subs"})})
 * @Entity
 */
class ProductIncludedAndSubstitute
{
    /**
     * @var integer
     *
     * @Column(name="`id_product_ included_and_substitute`", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idProductIncludedAndSubstitute;

    /**
     * @var integer
     *
     * @Column(name="`type`", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @Column(name="`price`", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @Column(name="`date_create`", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="`date_update`", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="`date_delete`", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="`active`", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_incl_subs", referencedColumnName="idproduct")
     * })
     */
    private $productInclSubs;

    function getIdProductIncludedAndSubstitute() {
        return $this->idProductIncludedAndSubstitute;
    }

    function getType() {
        return $this->type;
    }

    function getPrice() {
        return $this->price;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function getProductInclSubs() {
        return $this->productInclSubs;
    }

    function setIdProductIncludedAndSubstitute($idProductIncludedAndSubstitute) {
        $this->idProductIncludedAndSubstitute = $idProductIncludedAndSubstitute;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }

    function setProductInclSubs(\Product $productInclSubs) {
        $this->productInclSubs = $productInclSubs;
    }


}

