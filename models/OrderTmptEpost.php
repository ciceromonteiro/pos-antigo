<?php



use Doctrine\Mapping as ORM;

/**
 * OrderTmptEpost
 *
 * @Table(name="order_tmpt_epost")
 * @Entity
 */
class OrderTmptEpost
{
    /**
     * @var integer
     *
     * @Column(name="id_order_tmpt_epost", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idOrderTmptEpost;

    /**
     * @var string
     *
     * @Column(name="data_type", type="string", length=150, nullable=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @Column(name="data_value", type="text", length=65535, nullable=false)
     */
    private $dataValue;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    function getIdOrderTmptEpost() {
        return $this->idOrderTmptEpost;
    }

    function getDataType() {
        return $this->dataType;
    }

    function getDataValue() {
        return $this->dataValue;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdOrderTmptEpost($idOrderTmptEpost) {
        $this->idOrderTmptEpost = $idOrderTmptEpost;
    }

    function setDataType($dataType) {
        $this->dataType = $dataType;
    }

    function setDataValue($dataValue) {
        $this->dataValue = $dataValue;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

