<?php



use Doctrine\Mapping as ORM;

/**
 * PurchaseLine
 *
 * @Table(name="purchase_line", indexes={@Index(name="fk_purchase_line_product1_idx", columns={"product_idproduct"}), @Index(name="fk_purchase_line_purchase1_idx", columns={"purchase_idpurchase"}), @Index(name="fk_purchase_line_section1_idx", columns={"section_idsection"})})
 * @Entity
 */
class PurchaseLine
{
    /**
     * @var integer
     *
     * @Column(name="idpurchase_line", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpurchaseLine;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @Column(name="discount", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $discount;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var boolean
     *
     * @Column(name="not_delivered", type="boolean", nullable=false)
     */
    private $notDelivered;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Purchase
     *
     * @ManyToOne(targetEntity="Purchase")
     * @JoinColumns({
     *   @JoinColumn(name="purchase_idpurchase", referencedColumnName="idpurchase")
     * })
     */
    private $purchasepurchase;

    /**
     * @var \Section
     *
     * @ManyToOne(targetEntity="Section")
     * @JoinColumns({
     *   @JoinColumn(name="section_idsection", referencedColumnName="idsection")
     * })
     */
    private $sectionsection;



    /**
     * Get idpurchaseLine
     *
     * @return integer
     */
    public function getIdpurchaseLine()
    {
        return $this->idpurchaseLine;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return PurchaseLine
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return PurchaseLine
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return PurchaseLine
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return PurchaseLine
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return PurchaseLine
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return PurchaseLine
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PurchaseLine
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set notDelivered
     *
     * @param boolean $notDelivered
     *
     * @return PurchaseLine
     */
    public function setNotDelivered($notDelivered)
    {
        $this->notDelivered = $notDelivered;

        return $this;
    }

    /**
     * Get notDelivered
     *
     * @return boolean
     */
    public function getNotDelivered()
    {
        return $this->notDelivered;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return PurchaseLine
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }

    /**
     * Set purchasepurchase
     *
     * @param \Purchase $purchasepurchase
     *
     * @return PurchaseLine
     */
    public function setPurchasepurchase(\Purchase $purchasepurchase = null)
    {
        $this->purchasepurchase = $purchasepurchase;

        return $this;
    }

    /**
     * Get purchasepurchase
     *
     * @return \Purchase
     */
    public function getPurchasepurchase()
    {
        return $this->purchasepurchase;
    }

    /**
     * Set sectionsection
     *
     * @param \Section $sectionsection
     *
     * @return PurchaseLine
     */
    public function setSectionsection(\Section $sectionsection = null)
    {
        $this->sectionsection = $sectionsection;

        return $this;
    }

    /**
     * Get sectionsection
     *
     * @return \Section
     */
    public function getSectionsection()
    {
        return $this->sectionsection;
    }
}
