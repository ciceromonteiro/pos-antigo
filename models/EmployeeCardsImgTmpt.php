<?php



use Doctrine\Mapping as ORM;

/**
 * EmployeeCardsImgTmpt
 *
 * @Table(name="employee_cards_img_tmpt")
 * @Entity
 */
class EmployeeCardsImgTmpt
{
    /**
     * @var integer
     *
     * @Column(name="idemployee_cards_img_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idemployeeCardsImgTmpt;

    /**
     * @var text
     *
     * @Column(name="`img`", type="text", length=16777215, nullable=true)
     */
    private $img;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    function getIdemployeeCardsImgTmpt() {
        return $this->idemployeeCardsImgTmpt;
    }

    function getImg() {
        return $this->img;
    }

    function getActive() {
        return $this->active;
    }

    function setIdemployeeCardsImgTmpt($idemployeeCardsImgTmpt) {
        $this->idemployeeCardsImgTmpt = $idemployeeCardsImgTmpt;
    }

    function setImgTmpt($img) {
        $this->img = $img;
    }

    function setActive($active) {
        $this->active = $active;
    }
}