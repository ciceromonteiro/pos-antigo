<?php



use Doctrine\Mapping as ORM;

/**
 * Tables
 *
 * @Table(name="tables")
 * @Entity
 */
class Tables
{
    /**
     * @var integer
     *
     * @Column(name="idtables", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idtables;

    /**
     * @var string
     *
     * @Column(name="structure", type="text", nullable=false)
     */
    private $structure;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    function getIdtables() {
        return $this->idtables;
    }

    function getStructure() {
        return $this->structure;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdtables($idtables) {
        $this->idtables = $idtables;
    }

    function setStructure($structure) {
        $this->structure = $structure;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

