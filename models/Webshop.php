<?php



use Doctrine\Mapping as ORM;

/**
 * Webshop
 *
 * @Table(name="webshop")
 * @Entity
 */
class Webshop
{
    /**
     * @var integer
     *
     * @Column(name="idwebshop", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idwebshop;

    /**
     * @var string
     *
     * @Column(name="path_webshop", type="string", length=500, nullable=false)
     */
    private $pathWebshop;

    /**
     * @var string
     *
     * @Column(name="api_url", type="string", length=500, nullable=false)
     */
    private $apiUrl;

    /**
     * @var string
     *
     * @Column(name="api_users", type="string", length=45, nullable=false)
     */
    private $apiUsers;

    /**
     * @var string
     *
     * @Column(name="api_password", type="string", length=60, nullable=false)
     */
    private $apiPassword;

    /**
     * @var integer
     *
     * @Column(name="api_parent_cat", type="integer", nullable=true)
     */
    private $apiParentCat;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;



    /**
     * Get idwebshop
     *
     * @return integer
     */
    public function getIdwebshop()
    {
        return $this->idwebshop;
    }

    /**
     * Set pathWebshop
     *
     * @param string $pathWebshop
     *
     * @return Webshop
     */
    public function setPathWebshop($pathWebshop)
    {
        $this->pathWebshop = $pathWebshop;

        return $this;
    }

    /**
     * Get pathWebshop
     *
     * @return string
     */
    public function getPathWebshop()
    {
        return $this->pathWebshop;
    }

    /**
     * Set apiUrl
     *
     * @param string $apiUrl
     *
     * @return Webshop
     */
    public function setApiUrl($apiUrl)
    {
        $this->apiUrl = $apiUrl;

        return $this;
    }

    /**
     * Get apiUrl
     *
     * @return string
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * Set apiUsers
     *
     * @param string $apiUsers
     *
     * @return Webshop
     */
    public function setApiUsers($apiUsers)
    {
        $this->apiUsers = $apiUsers;

        return $this;
    }

    /**
     * Get apiUsers
     *
     * @return string
     */
    public function getApiUsers()
    {
        return $this->apiUsers;
    }

    /**
     * Set apiPassword
     *
     * @param string $apiPassword
     *
     * @return Webshop
     */
    public function setApiPassword($apiPassword)
    {
        $this->apiPassword = $apiPassword;

        return $this;
    }

    /**
     * Get apiPassword
     *
     * @return string
     */
    public function getApiPassword()
    {
        return $this->apiPassword;
    }

    /**
     * Set apiParentCat
     *
     * @param integer $apiParentCat
     *
     * @return Webshop
     */
    public function setApiParentCat($apiParentCat)
    {
        $this->apiParentCat = $apiParentCat;

        return $this;
    }

    /**
     * Get apiParentCat
     *
     * @return integer
     */
    public function getApiParentCat()
    {
        return $this->apiParentCat;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Webshop
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Webshop
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Webshop
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Webshop
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
