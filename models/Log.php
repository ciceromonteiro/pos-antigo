<?php



use Doctrine\Mapping as ORM;

/**
 * Log
 *
 * @Table(name="log", indexes={@Index(name="fk_log_webshop1_idx", columns={"webshop_idwebshop"}), @Index(name="fk_log_pos1_idx", columns={"pos_idpos"}), @Index(name="fk_log_log_type1_idx", columns={"log_type_idlog_type"})})
 * @Entity
 */
class Log
{
    /**
     * @var integer
     *
     * @Column(name="idlog", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlog;

    /**
     * @var string
     *
     * @Column(name="info", type="text", length=16777215, nullable=false)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \LogType
     *
     * @ManyToOne(targetEntity="LogType")
     * @JoinColumns({
     *   @JoinColumn(name="log_type_idlog_type", referencedColumnName="idlog_type")
     * })
     */
    private $logTypelogType;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \Webshop
     *
     * @ManyToOne(targetEntity="Webshop")
     * @JoinColumns({
     *   @JoinColumn(name="webshop_idwebshop", referencedColumnName="idwebshop")
     * })
     */
    private $webshopwebshop;



    /**
     * Get idlog
     *
     * @return integer
     */
    public function getIdlog()
    {
        return $this->idlog;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return Log
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Log
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {   $date = $this->dateCreate;
        return $date->format('d/m/Y H:i:s');
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Log
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Log
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Log
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set logTypelogType
     *
     * @param \LogType $logTypelogType
     *
     * @return Log
     */
    public function setLogTypelogType(\LogType $logTypelogType = null)
    {
        $this->logTypelogType = $logTypelogType;

        return $this;
    }

    /**
     * Get logTypelogType
     *
     * @return \LogType
     */
    public function getLogTypelogType()
    {
        return $this->logTypelogType;
    }

    /**
     * Set pospos
     *
     * @param \Pos $pospos
     *
     * @return Log
     */
    public function setPospos(\Pos $pospos = null)
    {
        $this->pospos = $pospos;

        return $this;
    }

    /**
     * Get pospos
     *
     * @return \Pos
     */
    public function getPospos()
    {
        return $this->pospos;
    }

    /**
     * Set webshopwebshop
     *
     * @param \Webshop $webshopwebshop
     *
     * @return Log
     */
    public function setWebshopwebshop(\Webshop $webshopwebshop = null)
    {
        $this->webshopwebshop = $webshopwebshop;

        return $this;
    }

    /**
     * Get webshopwebshop
     *
     * @return \Webshop
     */
    public function getWebshopwebshop()
    {
        return $this->webshopwebshop;
    }
}
