<?php



use Doctrine\Mapping as ORM;

/**
 * PosTmptBtnFunctions
 *
 * @Table(name="pos_tmpt_btn_functions")
 * @Entity
 */
class PosTmptBtnFunctions
{
    /**
     * @var integer
     *
     * @Column(name="idposmptbtnfunctions", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposmptbtnfunctions;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="action", type="string", length=150, nullable=false)
     */
    private $action;

    /**
     * @var string
     *
     * @Column(name="language", type="string", length=2, nullable=false)
     */
    private $language;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    function getIdposmptbtnfunctions() {
        return $this->idposmptbtnfunctions;
    }

    function getName() {
        return $this->name;
    }

    function getAction() {
        return $this->action;
    }

    function getLanguage() {
        return $this->language;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdposmptbtnfunctions($idposmptbtnfunctions) {
        $this->idposmptbtnfunctions = $idposmptbtnfunctions;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setAction($action) {
        $this->action = $action;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

