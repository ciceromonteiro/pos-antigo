<?php



use Doctrine\Mapping as ORM;

/**
 * ShippingSettings
 *
 * @Table(name="shipping_settings", indexes={@Index(name="fk_shipping_settings_shipping_place1_idx", columns={"shipping_place_idshipping_place"})})
 * @Entity
 */
class ShippingSettings
{
    /**
     * @var integer
     *
     * @Column(name="idshipping_settings", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idshippingSettings;

    /**
     * @var float
     *
     * @Column(name="weight_limit", type="float", precision=10, scale=0, nullable=true)
     */
    private $weightLimit;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var integer
     *
     * @Column(name="time_taken", type="integer", nullable=true)
     */
    private $timeTaken;

    /**
     * @var boolean
     *
     * @Column(name="working_days", type="boolean", nullable=false)
     */
    private $workingDays;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \ShippingPlace
     *
     * @ManyToOne(targetEntity="ShippingPlace")
     * @JoinColumns({
     *   @JoinColumn(name="shipping_place_idshipping_place", referencedColumnName="idshipping_place")
     * })
     */
    private $shippingPlaceshippingPlace;



    /**
     * Get idshippingSettings
     *
     * @return integer
     */
    public function getIdshippingSettings()
    {
        return $this->idshippingSettings;
    }

    /**
     * Set weightLimit
     *
     * @param float $weightLimit
     *
     * @return ShippingSettings
     */
    public function setWeightLimit($weightLimit)
    {
        $this->weightLimit = $weightLimit;

        return $this;
    }

    /**
     * Get weightLimit
     *
     * @return float
     */
    public function getWeightLimit()
    {
        return $this->weightLimit;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ShippingSettings
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set timeTaken
     *
     * @param integer $timeTaken
     *
     * @return ShippingSettings
     */
    public function setTimeTaken($timeTaken)
    {
        $this->timeTaken = $timeTaken;

        return $this;
    }

    /**
     * Get timeTaken
     *
     * @return integer
     */
    public function getTimeTaken()
    {
        return $this->timeTaken;
    }

    /**
     * Set workingDays
     *
     * @param boolean $workingDays
     *
     * @return ShippingSettings
     */
    public function setWorkingDays($workingDays)
    {
        $this->workingDays = $workingDays;

        return $this;
    }

    /**
     * Get workingDays
     *
     * @return boolean
     */
    public function getWorkingDays()
    {
        return $this->workingDays;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ShippingSettings
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ShippingSettings
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return ShippingSettings
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ShippingSettings
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set shippingPlaceshippingPlace
     *
     * @param \ShippingPlace $shippingPlaceshippingPlace
     *
     * @return ShippingSettings
     */
    public function setShippingPlaceshippingPlace(\ShippingPlace $shippingPlaceshippingPlace = null)
    {
        $this->shippingPlaceshippingPlace = $shippingPlaceshippingPlace;

        return $this;
    }

    /**
     * Get shippingPlaceshippingPlace
     *
     * @return \ShippingPlace
     */
    public function getShippingPlaceshippingPlace()
    {
        return $this->shippingPlaceshippingPlace;
    }
}
