<?php



use Doctrine\Mapping as ORM;

/**
 * LogoTmpt
 *
 * @Table(name="logo_tmpt")
 * @Entity
 */
class LogoTmpt
{
    /**
     * @var integer
     *
     * @Column(name="idlogo_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlogoTmpt;

    /**
     * @var text
     *
     * @Column(name="`img_tmpt`", type="text", length=16777215, nullable=true)
     */
    private $imgTmpt;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    function getIdlogoTmpt() {
        return $this->idlogoTmpt;
    }

    function getImgTmpt() {
        return $this->imgTmpt;
    }

    function getActive() {
        return $this->active;
    }

    function setIdlogoTmpt($idlogoTmpt) {
        $this->idlogoTmpt = $idlogoTmpt;
    }

    function setImgTmpt($imgTmpt) {
        $this->imgTmpt = $imgTmpt;
    }

    function setActive($active) {
        $this->active = $active;
    }
}

