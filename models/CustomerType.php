<?php



use Doctrine\Mapping as ORM;

/**
 * CustomerType
 *
 * @Table(name="customer_type", indexes={@Index(name="fk_costumer_type_company1_idx", columns={"company_idcompany"})})
 * @Entity
 */
class CustomerType
{
    /**
     * @var integer
     *
     * @Column(name="idcustumer_type", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcustumerType;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;



    /**
     * Get idcustumerType
     *
     * @return integer
     */
    public function getIdcustumerType()
    {
        return $this->idcustumerType;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CustomerType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set companycompany
     *
     * @param \Company $companycompany
     *
     * @return CustomerType
     */
    public function setCompanycompany(\Company $companycompany = null)
    {
        $this->companycompany = $companycompany;

        return $this;
    }

    /**
     * Get companycompany
     *
     * @return \Company
     */
    public function getCompanycompany()
    {
        return $this->companycompany;
    }
}
