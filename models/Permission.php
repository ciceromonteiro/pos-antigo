<?php

use Doctrine\Mapping as ORM;

/**
 * Permission
 *
 * @Table(name="permission")
 * @Entity
 */
class Permission {

    /**
     * @var integer
     *
     * @Column(name="idpermission", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpermission;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=250, nullable=false)
     */
    private $description;

    function getIdpermission() {
        return $this->idpermission;
    }

    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function setIdpermission($idpermission) {
        $this->idpermission = $idpermission;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

}
