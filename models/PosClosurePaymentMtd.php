<?php



use Doctrine\Mapping as ORM;

/**
 * PosClosurePaymentMtd
 *
 * @Table(name="pos_closure_payment_mtd")
 * @Entity
 */
class PosClosurePaymentMtd
{
    /**
     * @var integer
     *
     * @Column(name="idpos_closure_payment_mtd", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposClosurePaymentMtd;

    /**
     * @var \PosClosure
     *
     * @ManyToOne(targetEntity="PosClosure")
     * @JoinColumns({
     *   @JoinColumn(name="posClosureposClosure", referencedColumnName="idpos_closure")
     * })
     */
    private $posClosureposClosure;

    /**
     * @var \PaymentMtd
     *
     * @ManyToOne(targetEntity="PaymentMtd")
     * @JoinColumns({
     *   @JoinColumn(name="paymentMtdpaymentMtd", referencedColumnName="idpayment_mtd")
     * })
     */
    private $paymentMtdpaymentMtd;

    /**
     * @var integer
     *
     * @Column(name="amount", type="integer", nullable=false)
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;


    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    function getIdposClosurePaymentMtd() {
        return $this->idposClosurePaymentMtd;
    }
    function getPosClosureposClosure() {
        return $this->idposClosurePaymentMtd;
    }
    function getPaymentMtdpaymentMtd() {
        return $this->paymentMtdpaymentMtd;
    }

    function getAmount() {
        return $this->amount;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdposClosurePaymentMtd($idposClosurePaymentMtd) {
        $this->idposClosurePaymentMtd = $idposClosurePaymentMtd;
    }

    function setPosClosureposClosure($posClosureposClosure) {
        $this->posClosureposClosure = $posClosureposClosure;
    }

    function setPaymentMtdpaymentMtd($paymentMtdpaymentMtd) {
        $this->paymentMtdpaymentMtd = $paymentMtdpaymentMtd;
    }

    function setAmount($amount) {
        $this->amount = $amount;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }



}
