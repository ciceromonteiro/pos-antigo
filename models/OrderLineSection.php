<?php

use Doctrine\Mapping as ORM;

/**
 * OrderLineSection
 *
 * @Table(name="order_line_section", indexes={@Index(name="fk_order_line_section_section1_idx", columns={"section_idsection"}), @Index(name="fk_order_line_section_order_line1_idx", columns={"order_line_idorder_line"})})
 * @Entity
 */ 
class OrderLineSection {

    /**
     * @var integer
     *
     * @Column(name="idorder_line_section", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */

    private $idorderLineSection;

    /**
     * @var integer
     *
     * @Column(name="qty", type="integer", nullable=false)
     */
    private $qty = '1';

    /**
     * @var \DateTime
     *
     * @Column(name="date_stock", type="datetime")
     */
    private $dateStock; 

    /**
     * @var \DateTime
     *
     * @Column(name="expiration_date", type="datetime", nullable=true)
     */
    private $expirationDate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active;    


    /**
     * @var \OrderLine
     *
     * @ManyToOne(targetEntity="OrderLine")
     * @JoinColumns({
     *   @JoinColumn(name="order_line_idorder_line", referencedColumnName="idorder_line")
     * })
     */
    private $orderLineorderLine;

    /**
     * @var \OrderLine
     *

     * @ManyToOne(targetEntity="Section")
     * @JoinColumns({
     *   @JoinColumn(name="section_idsection", referencedColumnName="idsection")
     * })
     */
    private $sectionsection;

    function getIdorderLineSection() {
        return $this->idorderLineSection;
    }

    function getQty() {
        return $this->qty;
    }
    function getExpirationDate() {
        return $this->expirationDate;
    }
    function getDateStock() {
        return $this->dateStock;
    }

    function getOrderLineorderLine() {
        return $this->orderLineorderLine;
    }

    function getSectionsection() {
        return $this->sectionsection;
    }

    function setIdorderLineSection($idorderLineSection) {
        $this->idorderLineSection = $idorderLineSection;
    }

    function setQty($qty) {
        $this->qty = $qty;
    }

    function setExpirationDate($expirationDate) {
        $this->expirationDate = $expirationDate;
    }
    function setDateStock($dateStock) {
        $this->dateStock = $dateStock;
    }

    function setOrderLineorderLine(\OrderLine $orderLineorderLine) {
        $this->orderLineorderLine = $orderLineorderLine;
    }

    function setSectionsection(\Section $sectionsection) {
        $this->sectionsection = $sectionsection;
    }

    function getSectionidsection() {
        return $this->sectionidsection;
    }

    function getOrderlineidorderline() {
        return $this->orderlineidorderline;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return OrderLineSection
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return OrderLineSection
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return OrderLineSection
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    function setActive($active){
        $this->active = $active;
    }


}
