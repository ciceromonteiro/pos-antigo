<?php



use Doctrine\Mapping as ORM;

/**
 * UsedLicenses
 *
 * @Table(name="used_licenses")
 * @Entity
 */
class License
{
    /**
     * @var integer
     *
     * @Column(name="idusedlicenses", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idusedlicenses;

    /**
     * @var integer
     *
     * @Column(name="number", type="integer", nullable=false)
     */
    private $number;

    /**
     * @var string
     *
     * @Column(name="device", type="string", length=200, nullable=false)
     */
    private $device;

    /**
     * @param boolean $active
     */
    public function setidusedlicenses($idusedlicenses)
    {
        $this->idusedlicenses = $idusedlicenses;
    }


    /**
     * Get active
     *
     * @return boolean
     */
    public function getidusedlicenses()
    {
        return $this->idusedlicenses;
    }

      /**
     * @param boolean $active
     */
    public function setnumber($number)
    {
        $this->number = $number;
    }


    /**
     * Get active
     *
     * @return boolean
     */
    public function getinumber()
    {
        return $this->number;
    }


      /**
     * @param boolean $active
     */
    public function setdevice($device)
    {
        $this->device = $device;
    }


    /**
     * Get active
     *
     * @return boolean
     */
    public function getidusedlicenses()
    {
        return $this->device;
    }


}

