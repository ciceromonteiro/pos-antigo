<?php



use Doctrine\Mapping as ORM;

/**
 * WarningsInCaseOfDelay
 *
 * @Table(name="warnings_in_case_of_delay")
 * @Entity
 */
class WarningsInCaseOfDelay
{
    /**
     * @var integer
     *
     * @Column(name="id_warnings_in_case_of_delay", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idWarningsInCaseOfDelay;

    /**
     * @var integer
     *
     * @Column(name="send_after", type="integer", nullable=false)
     */
    private $sendAfter;

    /**
     * @var integer
     *
     * @Column(name="send_copy", type="integer", nullable=false)
     */
    private $sendCopy;

    /**
     * @var string
     *
     * @Column(name="message_title", type="string", length=300, nullable=false)
     */
    private $messageTitle;

    /**
     * @var string
     *
     * @Column(name="message", type="string", length=300, nullable=false)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    function getIdWarningsInCaseOfDelay() {
        return $this->idWarningsInCaseOfDelay;
    }

    function getSendAfter() {
        return $this->sendAfter;
    }

    function getSendCopy() {
        return $this->sendCopy;
    }

    function getMessageTitle() {
        return $this->messageTitle;
    }

    function getMessage() {
        return $this->message;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdWarningsInCaseOfDelay($idWarningsInCaseOfDelay) {
        $this->idWarningsInCaseOfDelay = $idWarningsInCaseOfDelay;
    }

    function setSendAfter($sendAfter) {
        $this->sendAfter = $sendAfter;
    }

    function setSendCopy($sendCopy) {
        $this->sendCopy = $sendCopy;
    }

    function setMessageTitle($messageTitle) {
        $this->messageTitle = $messageTitle;
    }

    function setMessage($message) {
        $this->message = $message;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

