<?php

use Doctrine\Mapping as ORM;

/**
 * PosTmpt
 *
 * @Table(name="pos_tmpt", indexes={@Index(name="fk_pos_tmpt_users1_idx", columns={"users_idusers"})})
 * @Entity
 */
class PosTmpt {

    /**
     * @var integer
     *
     * @Column(name="idpos_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposTmpt;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="structure_html", type="text", nullable=true)
     */
    private $structureHtml;

    /**
     * @var string
     *
     * @Column(name="resolution", type="string", length=100, nullable=false)
     */
    private $resolution;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;

    function getIdposTmpt() {
        return $this->idposTmpt;
    }

    function getName() {
        return $this->name;
    }

    function getStructureHtml() {
        return $this->structureHtml;
    }

    function getResolution() {
        return $this->resolution;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getUsersusers() {
        return $this->usersusers;
    }

    function setIdposTmpt($idposTmpt) {
        $this->idposTmpt = $idposTmpt;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setStructureHtml($structureHtml) {
        $this->structureHtml = $structureHtml;
    }

    function setResolution($resolution) {
        $this->resolution = $resolution;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setUsersusers(\Users $usersusers) {
        $this->usersusers = $usersusers;
    }

}
