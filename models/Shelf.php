<?php



use Doctrine\Mapping as ORM;

/**
 * Shelf
 *
 * @Table(name="shelf", indexes={@Index(name="fk_shelf_wardrobe1_idx", columns={"wardrobe_idwardrobe"})})
 * @Entity
 */
class Shelf
{
    /**
     * @var integer
     *
     * @Column(name="idshelf", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idshelf;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(name="position_top", type="integer", nullable=false)
     */
    private $positionTop;

        /**
     * @var integer
     *
     * @Column(name="fixed", type="integer", nullable=false)
     */

    private $fixed;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Wardrobe
     *
     * @ManyToOne(targetEntity="Wardrobe")
     * @JoinColumns({
     *   @JoinColumn(name="wardrobe_idwardrobe", referencedColumnName="idwardrobe")
     * })
     */

    private $wardrobewardrobe;

    function getIdshelf() {
        return $this->idshelf;
    }
    function getPositionTop() {
        return $this->positionTop;
    }
    function getFixed() {
        return $this->fixed;
    }

    function setName($name) {
        return $this->name = $name;
    }
    function setPositionTop($positionTop) {
        return $this->positionTop = $positionTop;
    }
    function setFixed($fixed) {
        return $this->fixed = $fixed;
    }
    function setActive($active) {
        return $this->active = $active;
    }
    function setWardrobe($wardrobe) {
        $this->wardrobewardrobe = $wardrobe;
    }


    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Shelf
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Shelf
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Shelf
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set wardrobewardrobe
     *
     * @param \Wardrobe $wardrobewardrobe
     *
     * @return Shelf
     */
    public function setWardrobewardrobe(\Wardrobe $wardrobewardrobe = null)
    {
        $this->wardrobewardrobe = $wardrobewardrobe;

        return $this;
    }

    /**
     * Get wardrobewardrobe
     *
     * @return \Wardrobe
     */
    public function getWardrobewardrobe()
    {
        return $this->wardrobewardrobe;
    }
}
