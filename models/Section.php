<?php



use Doctrine\Mapping as ORM;

/**
 * Section
 *
 * @Table(name="section", indexes={@Index(name="fk_section_shelf1_idx", columns={"shelf_idshelf"})})
 * @Entity
 */
class Section
{
    /**
     * @var integer
     *
     * @Column(name="idsection", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsection;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(name="position_left", type="integer", nullable=false)
     */

    private $positionLeft;

    /**
     * @var integer
     *
     * @Column(name="fixed", type="integer", nullable=false)
     */

    private $fixed;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Shelf
     *
     * @ManyToOne(targetEntity="Shelf")
     * @JoinColumns({
     *   @JoinColumn(name="shelf_idshelf", referencedColumnName="idshelf")
     * })
     */
    private $shelfshelf;

    function getIdsection() {
        return $this->idsection;
    }

    function getPositionLeft() {
        return $this->positionLeft;
    }
    function getFixed() {
        return $this->fixed;
    }

    function setName($name) {
        return $this->name = $name;
    }
    function setPositionLeft($positionLeft) {
        return $this->positionLeft = $positionLeft;
    }
    function setFixed($fixed) {
        return $this->fixed = $fixed;
    }
    function setActive($active) {
        return $this->active = $active;
    }
    function setShelf($shelf) {
        $this->shelfshelf = $shelf;
    }


    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Section
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Section
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Section
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set shelfshelf
     *
     * @param \Shelf $shelfshelf
     *
     * @return Section
     */
    public function setShelfshelf(\Shelf $shelfshelf = null)
    {
        $this->shelfshelf = $shelfshelf;

        return $this;
    }

    /**
     * Get shelfshelf
     *
     * @return \Shelf
     */
    public function getShelfshelf()
    {
        return $this->shelfshelf;
    }
}
