<?php



use Doctrine\Mapping as ORM;

/**
 * SupplierTypeApi
 *
 * @Table(name="supplier_type_api")
 * @Entity
 */
class SupplierTypeApi
{
    /**
     * @var integer
     *
     * @Column(name="idsuppliertypeapi", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsuppliertypeapi;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=250, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    function getIdsuppliertypeapi() {
        return $this->idsuppliertypeapi;
    }

    function getName() {
        return $this->name;
    }

    function getActive() {
        return $this->active;
    }

    function setIdsuppliertypeapi($idsuppliertypeapi) {
        $this->idsuppliertypeapi = $idsuppliertypeapi;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setActive($active) {
        $this->active = $active;
    }

}

