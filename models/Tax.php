<?php



use Doctrine\Mapping as ORM;

/**
 * Tax
 *
 * @Table(name="tax")
 * @Entity
 */
class Tax
{
    /**
     * @var integer
     *
     * @Column(name="idtax", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idtax;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @Column(name="percent", type="float", precision=10, scale=0, nullable=false)
     */
    private $percent;

    /**
     * @var integer
     *
     * @Column(name="mva_account", type="integer", nullable=false)
     */
    private $mvaAccount;

    /**
     * @var integer
     *
     * @Column(name="account_relapse", type="integer", nullable=false)
     */
    private $accountRelapse;

    /**
     * @var integer
     *
     * @Column(name="external_tax_code", type="integer", nullable=false)
     */
    private $externalTaxCode;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active = '1';

    function getIdtax() {
        return $this->idtax;
    }

    function getName() {
        return $this->name;
    }

    function getPercent() {
        return $this->percent;
    }

    function getMvaAccount() {
        return $this->mvaAccount;
    }

    function getAccountRelapse() {
        return $this->accountRelapse;
    }

    function getExternalTaxCode() {
        return $this->externalTaxCode;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdtax($idtax) {
        $this->idtax = $idtax;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPercent($percent) {
        $this->percent = $percent;
    }

    function setMvaAccount($mvaAccount) {
        $this->mvaAccount = $mvaAccount;
    }

    function setAccountRelapse($accountRelapse) {
        $this->accountRelapse = $accountRelapse;
    }

    function setExternalTaxCode($externalTaxCode) {
        $this->externalTaxCode = $externalTaxCode;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

