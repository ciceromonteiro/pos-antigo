<?php



use Doctrine\Mapping as ORM;

/**
 * ProductComponent
 *
 * @Table(name="product_component", indexes={@Index(name="fk_product_component_product1_idx", columns={"product_idproduct"}), @Index(name="fk_product_component_product2_idx", columns={"product_idproduct1"})})
 * @Entity
 */
class ProductComponent
{
    /**
     * @var integer
     *
     * @Column(name="idproduct_component", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductComponent;

    /**
     * @var integer
     *
     * @Column(name="qty", type="integer", nullable=false)
     */
    private $qty = '1';

    /**
     * @var string
     *
     * @Column(name="cost_price", type="decimal", precision=11, scale=0, nullable=false)
     */
    private $costPrice;

    /**
     * @var string
     *
     * @Column(name="retail_price", type="decimal", precision=11, scale=0, nullable=false)
     */
    private $retailPrice;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct1", referencedColumnName="idproduct")
     * })
     */
    private $productproduct1;

    function getIdproductComponent() {
        return $this->idproductComponent;
    }

    function getQty() {
        return $this->qty;
    }

    function getCostPrice() {
        return $this->costPrice;
    }

    function getRetailPrice() {
        return $this->retailPrice;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function getProductproduct1() {
        return $this->productproduct1;
    }

    function setIdproductComponent($idproductComponent) {
        $this->idproductComponent = $idproductComponent;
    }

    function setQty($qty) {
        $this->qty = $qty;
    }

    function setCostPrice($costPrice) {
        $this->costPrice = $costPrice;
    }

    function setRetailPrice($retailPrice) {
        $this->retailPrice = $retailPrice;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }

    function setProductproduct1(\Product $productproduct1) {
        $this->productproduct1 = $productproduct1;
    }


}

