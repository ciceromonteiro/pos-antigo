<?php



use Doctrine\Mapping as ORM;

/**
 * SectionTransfer
 *
 * @Table(name="section_transfer", indexes={@Index(name="fk_warehouse_transfer_product1_idx", columns={"product_idproduct"}), @Index(name="fk_warehouse_transfer_product_serial_number1_idx", columns={"product_serial_number_idproduct_serial_number"}), @Index(name="fk_section_transfer_size_color_section1_idx", columns={"size_color_section_idsize_color_section"}), @Index(name="fk_section_transfer_section1_idx", columns={"section_idsection"}), @Index(name="fk_section_transfer_section2_idx", columns={"section_idsection1"})})
 * @Entity
 */
class SectionTransfer
{
    /**
     * @var integer
     *
     * @Column(name="idsection_transfer", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsectionTransfer;

    /**
     * @var \DateTime
     *
     * @Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @Column(name="qty", type="integer", nullable=false)
     */
    private $qty;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var \Section
     *
     * @ManyToOne(targetEntity="Section")
     * @JoinColumns({
     *   @JoinColumn(name="section_idsection", referencedColumnName="idsection")
     * })
     */
    private $sectionsection;

    /**
     * @var \Section
     *
     * @ManyToOne(targetEntity="Section")
     * @JoinColumns({
     *   @JoinColumn(name="section_idsection1", referencedColumnName="idsection")
     * })
     */
    private $sectionsection1;

    /**
     * @var \SizeColorSection
     *
     * @ManyToOne(targetEntity="SizeColorSection")
     * @JoinColumns({
     *   @JoinColumn(name="size_color_section_idsize_color_section", referencedColumnName="idsize_color_section")
     * })
     */
    private $sizeColorSectionsizeColorSection;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \ProductSerialNumber
     *
     * @ManyToOne(targetEntity="ProductSerialNumber")
     * @JoinColumns({
     *   @JoinColumn(name="product_serial_number_idproduct_serial_number", referencedColumnName="idproduct_serial_number")
     * })
     */
    private $productSerialNumberproductSerialNumber;



    /**
     * Get idsectionTransfer
     *
     * @return integer
     */
    public function getIdsectionTransfer()
    {
        return $this->idsectionTransfer;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return SectionTransfer
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     *
     * @return SectionTransfer
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return integer
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return SectionTransfer
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set sectionsection
     *
     * @param \Section $sectionsection
     *
     * @return SectionTransfer
     */
    public function setSectionsection(\Section $sectionsection = null)
    {
        $this->sectionsection = $sectionsection;

        return $this;
    }

    /**
     * Get sectionsection
     *
     * @return \Section
     */
    public function getSectionsection()
    {
        return $this->sectionsection;
    }

    /**
     * Set sectionsection1
     *
     * @param \Section $sectionsection1
     *
     * @return SectionTransfer
     */
    public function setSectionsection1(\Section $sectionsection1 = null)
    {
        $this->sectionsection1 = $sectionsection1;

        return $this;
    }

    /**
     * Get sectionsection1
     *
     * @return \Section
     */
    public function getSectionsection1()
    {
        return $this->sectionsection1;
    }

    /**
     * Set sizeColorSectionsizeColorSection
     *
     * @param \SizeColorSection $sizeColorSectionsizeColorSection
     *
     * @return SectionTransfer
     */
    public function setSizeColorSectionsizeColorSection(\SizeColorSection $sizeColorSectionsizeColorSection = null)
    {
        $this->sizeColorSectionsizeColorSection = $sizeColorSectionsizeColorSection;

        return $this;
    }

    /**
     * Get sizeColorSectionsizeColorSection
     *
     * @return \SizeColorSection
     */
    public function getSizeColorSectionsizeColorSection()
    {
        return $this->sizeColorSectionsizeColorSection;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return SectionTransfer
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }

    /**
     * Set productSerialNumberproductSerialNumber
     *
     * @param \ProductSerialNumber $productSerialNumberproductSerialNumber
     *
     * @return SectionTransfer
     */
    public function setProductSerialNumberproductSerialNumber(\ProductSerialNumber $productSerialNumberproductSerialNumber = null)
    {
        $this->productSerialNumberproductSerialNumber = $productSerialNumberproductSerialNumber;

        return $this;
    }

    /**
     * Get productSerialNumberproductSerialNumber
     *
     * @return \ProductSerialNumber
     */
    public function getProductSerialNumberproductSerialNumber()
    {
        return $this->productSerialNumberproductSerialNumber;
    }
}
