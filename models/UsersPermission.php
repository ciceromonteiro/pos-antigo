<?php


/**
 * UsersPermission
 *
 * @Table(name="users_permission", indexes={@Index(name="fk_users_permission_users1_idx", columns={"users_idusers"}), @Index(name="fk_users_permission_permission2_idx", columns={"permission_idpermission"})})
 * @Entity
 */
class UsersPermission
{
    /**
     * @var integer
     *
     * @Column(name="idusers_permission", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idusersPermission;

    /**
     * @var \Permission
     *
     * @ManyToOne(targetEntity="Permission")
     * @JoinColumns({
     *   @JoinColumn(name="permission_idpermission", referencedColumnName="idpermission")
     * })
     */
    private $permissionpermission;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active;
    
    function getIdusersPermission() {
        return $this->idusersPermission;
    }

    function getPermissionpermission() {
        return $this->permissionpermission;
    }

    function getUsersusers() {
        return $this->usersusers;
    }

    function getActive() {
        return $this->active;
    }

    function setIdusersPermission($idusersPermission) {
        $this->idusersPermission = $idusersPermission;
    }

    function setPermissionpermission(\Permission $permissionpermission) {
        $this->permissionpermission = $permissionpermission;
    }

    function setUsersusers(\Users $usersusers) {
        $this->usersusers = $usersusers;
    }

    function setActive($active) {
        $this->active = $active;
    }

}
