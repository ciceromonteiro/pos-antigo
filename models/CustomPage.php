<?php



use Doctrine\Mapping as ORM;

/**
 * CustomPage
 *
 * @Table(name="custom_page", indexes={@Index(name="fk_custom_page_pos1_idx", columns={"pos_idpos"}), @Index(name="fk_custom_page_users1_idx", columns={"users_idusers"})})
 * @Entity
 */
class CustomPage
{
    /**
     * @var integer
     *
     * @Column(name="idcustom_page", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcustomPage;

    /**
     * @var string
     *
     * @Column(name="page_name", type="string", length=60, nullable=false)
     */
    private $pageName;

    /**
     * @var string
     *
     * @Column(name="json_file", type="string", length=254, nullable=false)
     */
    private $jsonFile;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;
    
    function getIdcustomPage() {
        return $this->idcustomPage;
    }

    function getPageName() {
        return $this->pageName;
    }

    function getJsonFile() {
        return $this->jsonFile;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getPospos() {
        return $this->pospos;
    }

    function getUsersusers() {
        return $this->usersusers;
    }

    function setIdcustomPage($idcustomPage) {
        $this->idcustomPage = $idcustomPage;
    }

    function setPageName($pageName) {
        $this->pageName = $pageName;
    }

    function setJsonFile($jsonFile) {
        $this->jsonFile = $jsonFile;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setPospos(\Pos $pospos) {
        $this->pospos = $pospos;
    }

    function setUsersusers(\Users $usersusers) {
        $this->usersusers = $usersusers;
    }




}
