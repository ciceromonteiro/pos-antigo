<?php



use Doctrine\Mapping as ORM;

/**
 * OrderBillet
 *
 * @Table(name="order_billet", indexes={@Index(name="fk_order_billet_order1_idx", columns={"order_idorder"})})
 * @Entity
 */
class OrderBillet
{
    /**
     * @var integer
     *
     * @Column(name="idorder_billet", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idorderBillet;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Order
     *
     * @ManyToOne(targetEntity="Order")
     * @JoinColumns({
     *   @JoinColumn(name="order_idorder", referencedColumnName="idorder")
     * })
     */
    private $orderorder;

    function getIdorderBillet() {
        return $this->idorderBillet;
    }

    function getValue() {
        return $this->value;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getOrderorder() {
        return $this->orderorder;
    }

    function setIdorderBillet($idorderBillet) {
        $this->idorderBillet = $idorderBillet;
    }

    function setValue($value) {
        $this->value = $value;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setOrderorder(\Order $orderorder) {
        $this->orderorder = $orderorder;
    }


}

