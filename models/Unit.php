<?php



use Doctrine\Mapping as ORM;

/**
 * Unit
 *
 * @Table(name="unit", indexes={@Index(name="fk_unit_unit1_idx", columns={"unit_idunit"})})
 * @Entity
 */
class Unit
{
    /**
     * @var integer
     *
     * @Column(name="idunit", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idunit;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(name="divisible", type="integer", nullable=false)
     */
    private $divisible;

    /**
     * @var float
     *
     * @Column(name="ratio", type="float", precision=10, scale=0, nullable=true)
     */
    private $ratio;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var \Unit
     *
     * @ManyToOne(targetEntity="Unit")
     * @JoinColumns({
     *   @JoinColumn(name="unit_idunit", referencedColumnName="idunit")
     * })
     */
    private $unitunit;



    /**
     * Get idunit
     *
     * @return integer
     */
    public function getIdunit()
    {
        return $this->idunit;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Unit
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set divisible
     *
     * @param boolean $divisible
     *
     * @return Unit
     */
    public function setDivisible($divisible)
    {
        $this->divisible = $divisible;

        return $this;
    }

    /**
     * Get divisible
     *
     * @return boolean
     */
    public function getDivisible()
    {
        return $this->divisible;
    }

    /**
     * Set ratio
     *
     * @param float $ratio
     *
     * @return Unit
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;

        return $this;
    }

    /**
     * Get ratio
     *
     * @return float
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Unit
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Unit
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Unit
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Unit
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set unitunit
     *
     * @param \Unit $unitunit
     *
     * @return Unit
     */
    public function setUnitunit(\Unit $unitunit = null)
    {
        $this->unitunit = $unitunit;

        return $this;
    }

    /**
     * Get unitunit
     *
     * @return \Unit
     */
    public function getUnitunit()
    {
        return $this->unitunit;
    }
}
