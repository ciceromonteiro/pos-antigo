<?php



use Doctrine\Mapping as ORM;

/**
 * License
 *
 * @Table(name="license", uniqueConstraints={@UniqueConstraint(name="key_license", columns={"key_license"})})
 * @Entity
 */
class License
{
    /**
     * @var integer
     *
     * @Column(name="idlicense", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlicense;

    /**
     * @var string
     *
     * @Column(name="key_license", type="string", length=200, nullable=false)
     */
    private $keyLicense;

    /**
     * @var \DateTime
     *
     * @Column(name="validad", type="datetime", nullable=false)
     */
    private $validad = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @Column(name="limit_users", type="integer", nullable=false)
     */
    private $limitUsers;

    /**
     * @var integer
     *
     * @Column(name="limit_connections", type="integer", nullable=false)
     */
    private $limitConnections;

    /**
     * @var integer
     *
     * @Column(name="limit_terminals", type="integer", nullable=false)
     */
    private $limitTerminals;

    /**
     * @var integer
     *
     * @Column(name="limit_mobiles", type="integer", nullable=false)
     */
    private $limitMobiles;

    /**
     * @var integer
     *
     * @Column(name="number_of_companies", type="integer", nullable=true)
     */
    private $numberOfCompanies = '1';

    /**
     * @var \DateTime
     *
     * @Column(name="update_sync", type="datetime", nullable=true)
     */
    private $updateSync;

    function getIdlicense() {
        return $this->idlicense;
    }

    function getKeyLicense() {
        return $this->keyLicense;
    }

    function getValidad() {
        return $this->validad;
    }

    function getLimitUsers() {
        return $this->limitUsers;
    }

    function getLimitConnections() {
        return $this->limitConnections;
    }

    function getLimitTerminals() {
        return $this->limitTerminals;
    }

    function getLimitMobiles() {
        return $this->limitMobiles;
    }

    function getNumberOfCompanies() {
        return $this->numberOfCompanies;
    }

    function getUpdateSync() {
        return $this->updateSync;
    }

    function setIdlicense($idlicense) {
        $this->idlicense = $idlicense;
    }

    function setKeyLicense($keyLicense) {
        $this->keyLicense = $keyLicense;
    }

    function setValidad(\DateTime $validad) {
        $this->validad = $validad;
    }

    function setLimitUsers($limitUsers) {
        $this->limitUsers = $limitUsers;
    }

    function setLimitConnections($limitConnections) {
        $this->limitConnections = $limitConnections;
    }

    function setLimitTerminals($limitTerminals) {
        $this->limitTerminals = $limitTerminals;
    }

    function setLimitMobiles($limitMobiles) {
        $this->limitMobiles = $limitMobiles;
    }

    function setNumberOfCompanies($numberOfCompanies) {
        $this->numberOfCompanies = $numberOfCompanies;
    }

    function setUpdateSync(\DateTime $updateSync) {
        $this->updateSync = $updateSync;
    }


}

