<?php



use Doctrine\Mapping as ORM;

/**
 * DefaultPermission
 *
 * @Table(name="default_permission", indexes={@Index(name="fk_permission_type_permission1_idx", columns={"permission_idpermission"}), @Index(name="fk_permission_type_users_type1_idx", columns={"users_type_idusers_type"})})
 * @Entity
 */
class DefaultPermission
{
    /**
     * @var integer
     *
     * @Column(name="iddefault_permission", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $iddefaultPermission;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var \Permission
     *
     * @ManyToOne(targetEntity="Permission")
     * @JoinColumns({
     *   @JoinColumn(name="permission_idpermission", referencedColumnName="idpermission")
     * })
     */
    private $permissionpermission;

    /**
     * @var \UsersType
     *
     * @ManyToOne(targetEntity="UsersType")
     * @JoinColumns({
     *   @JoinColumn(name="users_type_idusers_type", referencedColumnName="idusers_type")
     * })
     */
    private $usersTypeusersType;



    /**
     * Get iddefaultPermission
     *
     * @return integer
     */
    public function getIddefaultPermission()
    {
        return $this->iddefaultPermission;
    }

    /**
     * Set active
     *
     * @param integer $active
     *
     * @return DefaultPermission
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return DefaultPermission
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return DefaultPermission
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return DefaultPermission
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }


    /**
     * Set permissionpermission
     *
     * @param \Permission $permissionpermission
     *
     * @return DefaultPermission
     */
    public function setPermissionpermission(\Permission $permissionpermission = null)
    {
        $this->permissionpermission = $permissionpermission;

        return $this;
    }

    /**
     * Get permissionpermission
     *
     * @return \Permission
     */
    public function getPermissionpermission()
    {
        return $this->permissionpermission;
    }

    /**
     * Set usersTypeusersType
     *
     * @param \UsersType $usersTypeusersType
     *
     * @return DefaultPermission
     */
    public function setUsersTypeusersType(\UsersType $usersTypeusersType = null)
    {
        $this->usersTypeusersType = $usersTypeusersType;

        return $this;
    }

    /**
     * Get usersTypeusersType
     *
     * @return \UsersType
     */
    public function getUsersTypeusersType()
    {
        return $this->usersTypeusersType;
    }
}
