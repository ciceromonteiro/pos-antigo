<?php



use Doctrine\Mapping as ORM;

/**
 * CompanyHasPermission
 *
 * @Table(name="company_has_permission", indexes={@Index(name="fk_company_has_permission_permission1_idx", columns={"permission_idpermission"}), @Index(name="fk_company_has_permission_company1_idx", columns={"company_idcompany"})})
 * @Entity
 */
class CompanyHasPermission
{
    /**
     * @var integer
     *
     * @Column(name="idcompany_has_permission", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcompanyHasPermission;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;

    /**
     * @var \Permission
     *
     * @ManyToOne(targetEntity="Permission")
     * @JoinColumns({
     *   @JoinColumn(name="permission_idpermission", referencedColumnName="idpermission")
     * })
     */
    private $permissionpermission;



    /**
     * Get idcompanyHasPermission
     *
     * @return integer
     */
    public function getIdcompanyHasPermission()
    {
        return $this->idcompanyHasPermission;
    }

    /**
     * Set companycompany
     *
     * @param \Company $companycompany
     *
     * @return CompanyHasPermission
     */
    public function setCompanycompany(\Company $companycompany = null)
    {
        $this->companycompany = $companycompany;

        return $this;
    }

    /**
     * Get companycompany
     *
     * @return \Company
     */
    public function getCompanycompany()
    {
        return $this->companycompany;
    }

    /**
     * Set permissionpermission
     *
     * @param \Permission $permissionpermission
     *
     * @return CompanyHasPermission
     */
    public function setPermissionpermission(\Permission $permissionpermission = null)
    {
        $this->permissionpermission = $permissionpermission;

        return $this;
    }

    /**
     * Get permissionpermission
     *
     * @return \Permission
     */
    public function getPermissionpermission()
    {
        return $this->permissionpermission;
    }
}
