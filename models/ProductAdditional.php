<?php



use Doctrine\Mapping as ORM;

/**
 * ProductAdditional
 *
 * @Table(name="product_additional", indexes={@Index(name="product_idproduct", columns={"product_idproduct"})})
 * @Entity
 */
class ProductAdditional
{
    /**
     * @var integer
     *
     * @Column(name="idproductadditional", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductadditional;

    /**
     * @var integer
     *
     * @Column(name="`order`", type="integer", nullable=false)
     */
    private $order;

    /**
     * @var integer
     *
     * @Column(name="additional_type", type="integer", nullable=false)
     */
    private $additionalType;

    /**
     * @var string
     *
     * @Column(name="info", type="text", length=65535, nullable=false)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    function getIdproductadditional() {
        return $this->idproductadditional;
    }

    function getOrder() {
        return $this->order;
    }

    function getAdditionalType() {
        return $this->additionalType;
    }

    function getInfo() {
        return $this->info;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function setIdproductadditional($idproductadditional) {
        $this->idproductadditional = $idproductadditional;
    }

    function setOrder($order) {
        $this->order = $order;
    }

    function setAdditionalType($additionalType) {
        $this->additionalType = $additionalType;
    }

    function setInfo($info) {
        $this->info = $info;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setProductproduct($productproduct) {
        $this->productproduct = $productproduct;
    }


}

