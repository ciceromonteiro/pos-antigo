<?php



use Doctrine\Mapping as ORM;

/**
 * AccessCardImgTmpt
 *
 * @Table(name="access_card_img_tmpt")
 * @Entity
 */
class AccessCardImgTmpt
{
    /**
     * @var integer
     *
     * @Column(name="idaccess_card_img_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idaccessCardImgTmpt;

    /**
     * @var text
     *
     * @Column(name="`img`", type="text", length=16777215, nullable=true)
     */
    private $img;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    function getIdaccessCardImgTmpt() {
        return $this->idaccessCardImgTmpt;
    }

    function getImg() {
        return $this->img;
    }

    function getActive() {
        return $this->active;
    }

    function setIdaccessCardImgTmpt($idaccessCardImgTmpt) {
        $this->idaccessCardImgTmpt = $idaccessCardImgTmpt;
    }

    function setImgTmpt($img) {
        $this->img = $img;
    }

    function setActive($active) {
        $this->active = $active;
    }
}