<?php



use Doctrine\Mapping as ORM;

/**
 * PosPrinter
 *
 * @Table(name="pos_printer", indexes={@Index(name="fk_pos_printer_pos1_idx", columns={"pos_idpos"}), @Index(name="fk_pos_printer_printer1_idx", columns={"printer_idprinter"})})
 * @Entity
 */
class PosPrinter
{
    /**
     * @var integer
     *
     * @Column(name="idpos_printer", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposPrinter;

    /**
     * @var string
     *
     * @Column(name="port", type="string", length=20, nullable=true)
     */
    private $port;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \Printer
     *
     * @ManyToOne(targetEntity="Printer")
     * @JoinColumns({
     *   @JoinColumn(name="printer_idprinter", referencedColumnName="idprinter")
     * })
     */
    private $printerprinter;



    /**
     * Get idposPrinter
     *
     * @return integer
     */
    public function getIdposPrinter()
    {
        return $this->idposPrinter;
    }

    /**
     * Set port
     *
     * @param string $port
     *
     * @return PosPrinter
     */
    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    /**
     * Get port
     *
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PosPrinter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pospos
     *
     * @param \Pos $pospos
     *
     * @return PosPrinter
     */
    public function setPospos(\Pos $pospos = null)
    {
        $this->pospos = $pospos;

        return $this;
    }

    /**
     * Get pospos
     *
     * @return \Pos
     */
    public function getPospos()
    {
        return $this->pospos;
    }

    /**
     * Set printerprinter
     *
     * @param \Printer $printerprinter
     *
     * @return PosPrinter
     */
    public function setPrinterprinter(\Printer $printerprinter = null)
    {
        $this->printerprinter = $printerprinter;

        return $this;
    }

    /**
     * Get printerprinter
     *
     * @return \Printer
     */
    public function getPrinterprinter()
    {
        return $this->printerprinter;
    }
}
