<?php

use Doctrine\Mapping as ORM;

/**
 * Discount
 *
 * @Table(name="discount", indexes={@Index(name="fk_discount_customer_gp1", columns={"customer_gp_idcustomer_gp"}), @Index(name="fk_discount_person1", columns={"person_idperson"}), @Index(name="fk_discount_product1", columns={"product_idproduct"}), @Index(name="fk_discount_product_gp1", columns={"product_gp_idproduct_gp"})})
 * @Entity
 */
class Discount {

    /**
     * @var integer
     *
     * @Column(name="iddiscount", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    public $iddiscount;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson", nullable=true)
     * })
     */
    public $personIdperson;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct", nullable=true)
     * })
     */
    public $productIdproduct;

    /**
     * @var \ProductGp
     *
     * @ManyToOne(targetEntity="ProductGp")
     * @JoinColumns({
     *   @JoinColumn(name="product_gp_idproduct_gp", referencedColumnName="idproduct_gp", nullable=true)
     * })
     */
    public $productGpIdproductGp;

    /**
     * @var \CustomerGp
     *
     * @ManyToOne(targetEntity="CustomerGp")
     * @JoinColumns({
     *   @JoinColumn(name="customer_gp_idcustomer_gp", referencedColumnName="idcustomer_gp", nullable=true)
     * })
     */
    public $customerGpIdcustomerGp;

    /**
     * @var percent
     *
     * @Column(name="percent", type="decimal", scale=2, nullable=true)
     */
    public $percent;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    public $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    public $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    public $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    public $active;

    function getIddiscount() {
        return $this->iddiscount;
    }

    function getPersonIdperson() {
        return $this->personIdperson;
    }

    function getProductIdproduct() {
        return $this->productIdproduct;
    }

    function getProductGpIdproductGp() {
        return $this->productGpIdproductGp;
    }

    function getCustomerGpIdcustomerGp() {
        return $this->customerGpIdcustomerGp;
    }

    function getPercent() {
        return $this->percent;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIddiscount($iddiscount) {
        $this->iddiscount = $iddiscount;
    }

    function setPersonIdperson($personIdperson) {
        $this->personIdperson = $personIdperson;
    }

    function setProductIdproduct($productIdproduct) {
        $this->productIdproduct = $productIdproduct;
    }

    function setProductGpIdproductGp($productGpIdproductGp) {
        $this->productGpIdproductGp = $productGpIdproductGp;
    }

    function setCustomerGpIdcustomerGp($customerGpIdcustomerGp) {
        $this->customerGpIdcustomerGp = $customerGpIdcustomerGp;
    }

    function setPercent($percent) {
        $this->percent = $percent;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

}
