<?php



use Doctrine\Mapping as ORM;

/**
 * PosSession
 *
 * @Table(name="pos_session", indexes={@Index(name="fk_pos_session_users1_idx", columns={"users_idusers"})})
 * @Entity
 */
class PosSession
{
    /**
     * @var integer
     *
     * @Column(name="idpos_session", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposSession;

    /**
     * @var \DateTime
     *
     * @Column(name="date_start", type="datetime", nullable=false)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @Column(name="end_date", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var string
     *
     * @Column(name="cash_fund", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $cashFund;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;



    /**
     * Get idposSession
     *
     * @return integer
     */
    public function getIdposSession()
    {
        return $this->idposSession;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return PosSession
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return PosSession
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set cashFund
     *
     * @param string $cashFund
     *
     * @return PosSession
     */
    public function setCashFund($cashFund)
    {
        $this->cashFund = $cashFund;

        return $this;
    }

    /**
     * Get cashFund
     *
     * @return string
     */
    public function getCashFund()
    {
        return $this->cashFund;
    }

    /**
     * Set usersusers
     *
     * @param \Users $usersusers
     *
     * @return PosSession
     */
    public function setUsersusers(\Users $usersusers = null)
    {
        $this->usersusers = $usersusers;

        return $this;
    }

    /**
     * Get usersusers
     *
     * @return \Users
     */
    public function getUsersusers()
    {
        return $this->usersusers;
    }
}
