<?php



use Doctrine\Mapping as ORM;

/**
 * PosTmptCommands
 *
 * @Table(name="pos_tmpt_commands")
 * @Entity
 */
class PosTmptCommands
{
    /**
     * @var integer
     *
     * @Column(name="idpostmptcommands", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idPosTmptCommands;

    /**
     * @var string
     *
     * @Column(name="command", type="string", length=100, nullable=false)
     */
    private $command;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=150, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="language", type="string", length=2, nullable=false)
     */
    private $language;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    function getIdPosTmptCommands() {
        return $this->idPosTmptCommands;
    }

    function getCommand() {
        return $this->command;
    }

    function getDescription() {
        return $this->description;
    }

    function getLanguage() {
        return $this->language;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdPosTmptCommands($idPosTmptCommands) {
        $this->idPosTmptCommands = $idPosTmptCommands;
    }

    function setCommand($command) {
        $this->command = $command;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setLanguage($language) {
        $this->language = $language;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

