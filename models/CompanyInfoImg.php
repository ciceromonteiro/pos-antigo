<?php



use Doctrine\Mapping as ORM;

/**
 * CompanyInfoImg
 *
 * @Table(name="company_info_img")
 * @Entity
 */
class CompanyInfoImg
{
    /**
     * @var integer
     *
     * @Column(name="idcompanyinfoimg", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcompanyinfoimg;
    
    /**
     * @var text
     *
     * @Column(name="`image`", type="text", length=16777215, nullable=true)
     */
    private $image;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    function getIdcompanyinfoimg() {
        return $this->idcompanyinfoimg;
    }

    function getImage() {
        return $this->image;
    }

    function getActive() {
        return $this->active;
    }

    function setIdcompanyinfoimg($idcompanyinfoimg) {
        $this->idcompanyinfoimg = $idcompanyinfoimg;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setActive($active) {
        $this->active = $active;
    }

}

