<?php



use Doctrine\Mapping as ORM;

/**
 * VehicleImeiOrder
 *
 * @Table(name="vehicle_imei_order", indexes={@Index(name="fk_vehicle_imei_order_order1_idx", columns={"order_idorder"}), @Index(name="fk_vehicle_imei_order_vehicle_imei1_idx", columns={"vehicle_imei_idvehicle_imei"}), @Index(name="fk_vehicle_imei_order_vehicle1_idx", columns={"vehicle_idvehicle"})})
 * @Entity
 */
class VehicleImeiOrder
{
    /**
     * @var integer
     *
     * @Column(name="idvehicle_imei_order", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idvehicleImeiOrder;

    /**
     * @var \Order
     *
     * @ManyToOne(targetEntity="Order")
     * @JoinColumns({
     *   @JoinColumn(name="order_idorder", referencedColumnName="idorder")
     * })
     */
    private $orderorder;

    /**
     * @var \Vehicle
     *
     * @ManyToOne(targetEntity="Vehicle")
     * @JoinColumns({
     *   @JoinColumn(name="vehicle_idvehicle", referencedColumnName="idvehicle")
     * })
     */
    private $vehiclevehicle;

    /**
     * @var \VehicleImei
     *
     * @ManyToOne(targetEntity="VehicleImei")
     * @JoinColumns({
     *   @JoinColumn(name="vehicle_imei_idvehicle_imei", referencedColumnName="idvehicle_imei")
     * })
     */
    private $vehicleImeivehicleImei;



    /**
     * Get idvehicleImeiOrder
     *
     * @return integer
     */
    public function getIdvehicleImeiOrder()
    {
        return $this->idvehicleImeiOrder;
    }

    /**
     * Set orderorder
     *
     * @param \Order $orderorder
     *
     * @return VehicleImeiOrder
     */
    public function setOrderorder(\Order $orderorder = null)
    {
        $this->orderorder = $orderorder;

        return $this;
    }

    /**
     * Get orderorder
     *
     * @return \Order
     */
    public function getOrderorder()
    {
        return $this->orderorder;
    }

    /**
     * Set vehiclevehicle
     *
     * @param \Vehicle $vehiclevehicle
     *
     * @return VehicleImeiOrder
     */
    public function setVehiclevehicle(\Vehicle $vehiclevehicle = null)
    {
        $this->vehiclevehicle = $vehiclevehicle;

        return $this;
    }

    /**
     * Get vehiclevehicle
     *
     * @return \Vehicle
     */
    public function getVehiclevehicle()
    {
        return $this->vehiclevehicle;
    }

    /**
     * Set vehicleImeivehicleImei
     *
     * @param \VehicleImei $vehicleImeivehicleImei
     *
     * @return VehicleImeiOrder
     */
    public function setVehicleImeivehicleImei(\VehicleImei $vehicleImeivehicleImei = null)
    {
        $this->vehicleImeivehicleImei = $vehicleImeivehicleImei;

        return $this;
    }

    /**
     * Get vehicleImeivehicleImei
     *
     * @return \VehicleImei
     */
    public function getVehicleImeivehicleImei()
    {
        return $this->vehicleImeivehicleImei;
    }
}
