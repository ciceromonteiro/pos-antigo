<?php



use Doctrine\Mapping as ORM;

/**
 * OrderTmpt
 *
 * @Table(name="order_tmpt")
 * @Entity
 */
class OrderTmpt
{
    /**
     * @var integer
     *
     * @Column(name="idordertmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idordertmpt;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=250, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(name="withdrawal_from_stock", type="integer", nullable=true)
     */
    private $withdrawalFromStock;

    /**
     * @var integer
     *
     * @Column(name="sell_by_invoice", type="integer", nullable=true)
     */
    private $sellByInvoice;

    /**
     * @var integer
     *
     * @Column(name="include_in_stock_statistics", type="integer", nullable=true)
     */
    private $includeInStockStatistics;

    /**
     * @var integer
     *
     * @Column(name="change_print_send_method", type="integer", nullable=true)
     */
    private $changePrintSendMethod;

    /**
     * @var integer
     *
     * @Column(name="print_delivery_list", type="integer", nullable=true)
     */
    private $printDeliveryList;

    /**
     * @var integer
     *
     * @Column(name="print_invoice", type="integer", nullable=true)
     */
    private $printInvoice;

    /**
     * @var integer
     *
     * @Column(name="template_order", type="integer", nullable=true)
     */
    private $templateOrder;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    function getIdordertmpt() {
        return $this->idordertmpt;
    }

    function getName() {
        return $this->name;
    }

    function getWithdrawalFromStock() {
        return $this->withdrawalFromStock;
    }

    function getSellByInvoice() {
        return $this->sellByInvoice;
    }

    function getIncludeInStockStatistics() {
        return $this->includeInStockStatistics;
    }

    function getChangePrintSendMethod() {
        return $this->changePrintSendMethod;
    }

    function getPrintDeliveryList() {
        return $this->printDeliveryList;
    }

    function getPrintInvoice() {
        return $this->printInvoice;
    }

    function getTemplateOrder() {
        return $this->templateOrder;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdordertmpt($idordertmpt) {
        $this->idordertmpt = $idordertmpt;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setWithdrawalFromStock($withdrawalFromStock) {
        $this->withdrawalFromStock = $withdrawalFromStock;
    }

    function setSellByInvoice($sellByInvoice) {
        $this->sellByInvoice = $sellByInvoice;
    }

    function setIncludeInStockStatistics($includeInStockStatistics) {
        $this->includeInStockStatistics = $includeInStockStatistics;
    }

    function setChangePrintSendMethod($changePrintSendMethod) {
        $this->changePrintSendMethod = $changePrintSendMethod;
    }

    function setPrintDeliveryList($printDeliveryList) {
        $this->printDeliveryList = $printDeliveryList;
    }

    function setPrintInvoice($printInvoice) {
        $this->printInvoice = $printInvoice;
    }

    function setTemplateOrder($templateOrder) {
        $this->templateOrder = $templateOrder;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

