<?php



use Doctrine\Mapping as ORM;

/**
 * PosPreset
 *
 * @Table(name="pos_preset")
 * @Entity
 */
class PosPreset
{
    /**
     * @var integer
     *
     * @Column(name="idpospreset", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpospreset;

    /**
     * @var string
     *
     * @Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="structure", type="text", nullable=true)
     */
    private $structure;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active = '1';

    function getIdpospreset() {
        return $this->idpospreset;
    }

    function getDescription() {
        return $this->description;
    }

    function getStructure() {
        return $this->structure;
    }

    function getActive() {
        return $this->active;
    }

    function setIdpospreset($idpospreset) {
        $this->idpospreset = $idpospreset;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setStructure($structure) {
        $this->structure = $structure;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

