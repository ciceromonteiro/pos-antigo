<?php



use Doctrine\Mapping as ORM;

/**
 * TableColumn
 *
 * @Table(name="table_column", indexes={@Index(name="fk_table_column_pos_element1_idx", columns={"pos_element_idpos_element"})})
 * @Entity
 */
class TableColumn
{
    /**
     * @var integer
     *
     * @Column(name="idtable_column", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idtableColumn;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="label", type="string", length=45, nullable=false)
     */
    private $label;

    /**
     * @var integer
     *
     * @Column(name="order", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \PosElement
     *
     * @ManyToOne(targetEntity="PosElement")
     * @JoinColumns({
     *   @JoinColumn(name="pos_element_idpos_element", referencedColumnName="idpos_element")
     * })
     */
    private $posElementposElement;



    /**
     * Get idtableColumn
     *
     * @return integer
     */
    public function getIdtableColumn()
    {
        return $this->idtableColumn;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return TableColumn
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return TableColumn
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return TableColumn
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return TableColumn
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set posElementposElement
     *
     * @param \PosElement $posElementposElement
     *
     * @return TableColumn
     */
    public function setPosElementposElement(\PosElement $posElementposElement = null)
    {
        $this->posElementposElement = $posElementposElement;

        return $this;
    }

    /**
     * Get posElementposElement
     *
     * @return \PosElement
     */
    public function getPosElementposElement()
    {
        return $this->posElementposElement;
    }
}
