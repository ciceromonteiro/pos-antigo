<?php



use Doctrine\Mapping as ORM;

/**
 * ProductProductGp
 *
 * @Table(name="product_product_gp", indexes={@Index(name="fk_product_product_gp_product_gp1_idx", columns={"product_gp_idproduct_gp"}), @Index(name="fk_product_product_gp_product1_idx", columns={"product_idproduct"})})
 * @Entity
 */
class ProductProductGp
{
    /**
     * @var integer
     *
     * @Column(name="idproduct_product_gp", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductProductGp;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \ProductGp
     *
     * @ManyToOne(targetEntity="ProductGp")
     * @JoinColumns({
     *   @JoinColumn(name="product_gp_idproduct_gp", referencedColumnName="idproduct_gp")
     * })
     */
    private $productGpproductGp;



    /**
     * Get idproductProductGp
     *
     * @return integer
     */
    public function getIdproductProductGp()
    {
        return $this->idproductProductGp;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return ProductProductGp
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }

    /**
     * Set productGpproductGp
     *
     * @param \ProductGp $productGpproductGp
     *
     * @return ProductProductGp
     */
    public function setProductGpproductGp(\ProductGp $productGpproductGp = null)
    {
        $this->productGpproductGp = $productGpproductGp;

        return $this;
    }

    /**
     * Get productGpproductGp
     *
     * @return \ProductGp
     */
    public function getProductGpproductGp()
    {
        return $this->productGpproductGp;
    }
}
