<?php



use Doctrine\Mapping as ORM;

/**
 * PosWebshop
 *
 * @Table(name="pos_webshop", indexes={@Index(name="fk_pos_webshop_pos1_idx", columns={"pos_idpos"}), @Index(name="fk_pos_webshop_webshop1_idx", columns={"webshop_idwebshop"})})
 * @Entity
 */
class PosWebshop
{
    /**
     * @var integer
     *
     * @Column(name="idpos_webshop", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposWebshop;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \Webshop
     *
     * @ManyToOne(targetEntity="Webshop")
     * @JoinColumns({
     *   @JoinColumn(name="webshop_idwebshop", referencedColumnName="idwebshop")
     * })
     */
    private $webshopwebshop;



    /**
     * Get idposWebshop
     *
     * @return integer
     */
    public function getIdposWebshop()
    {
        return $this->idposWebshop;
    }

    /**
     * Set pospos
     *
     * @param \Pos $pospos
     *
     * @return PosWebshop
     */
    public function setPospos(\Pos $pospos = null)
    {
        $this->pospos = $pospos;

        return $this;
    }

    /**
     * Get pospos
     *
     * @return \Pos
     */
    public function getPospos()
    {
        return $this->pospos;
    }

    /**
     * Set webshopwebshop
     *
     * @param \Webshop $webshopwebshop
     *
     * @return PosWebshop
     */
    public function setWebshopwebshop(\Webshop $webshopwebshop = null)
    {
        $this->webshopwebshop = $webshopwebshop;

        return $this;
    }

    /**
     * Get webshopwebshop
     *
     * @return \Webshop
     */
    public function getWebshopwebshop()
    {
        return $this->webshopwebshop;
    }
}
