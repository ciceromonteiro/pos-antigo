<?php



use Doctrine\Mapping as ORM;

/**
 * RegisterCustomerType
 *
 * @Table(name="register_customer_type", indexes={@Index(name="fk_register_customer_type_register1_idx", columns={"register_idregister"}), @Index(name="fk_register_customer_type_customer_type1_idx", columns={"customer_type_idcustumer_type"})})
 * @Entity
 */
class RegisterCustomerType
{
    /**
     * @var integer
     *
     * @Column(name="idregister_customer_type", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idregisterCustomerType;

    /**
     * @var \CustomerType
     *
     * @ManyToOne(targetEntity="CustomerType")
     * @JoinColumns({
     *   @JoinColumn(name="customer_type_idcustumer_type", referencedColumnName="idcustumer_type")
     * })
     */
    private $customerTypecustumerType;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="register_idregister", referencedColumnName="idperson")
     * })
     */
    private $registerregister;



    /**
     * Get idregisterCustomerType
     *
     * @return integer
     */
    public function getIdregisterCustomerType()
    {
        return $this->idregisterCustomerType;
    }

    /**
     * Set customerTypecustumerType
     *
     * @param \CustomerType $customerTypecustumerType
     *
     * @return RegisterCustomerType
     */
    public function setCustomerTypecustumerType(\CustomerType $customerTypecustumerType = null)
    {
        $this->customerTypecustumerType = $customerTypecustumerType;

        return $this;
    }

    /**
     * Get customerTypecustumerType
     *
     * @return \CustomerType
     */
    public function getCustomerTypecustumerType()
    {
        return $this->customerTypecustumerType;
    }

    /**
     * Set registerregister
     *
     * @param \Person $registerregister
     *
     * @return RegisterCustomerType
     */
    public function setRegisterregister(\Person $registerregister = null)
    {
        $this->registerregister = $registerregister;

        return $this;
    }

    /**
     * Get registerregister
     *
     * @return \Person
     */
    public function getRegisterregister()
    {
        return $this->registerregister;
    }
}
