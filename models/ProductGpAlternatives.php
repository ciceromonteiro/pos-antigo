<?php



use Doctrine\Mapping as ORM;

/**
 * ProductGpAlternatives
 *
 * @Table(name="product_gp_alternatives", indexes={@Index(name="idproductgp", columns={"idproductgp"})})
 * @Entity
 */
class ProductGpAlternatives
{
    /**
     * @var integer
     *
     * @Column(name="idproductgpalternatives", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductgpalternatives;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=150, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="priceChange", type="decimal", precision=10, scale=0, nullable=true)
     */
    private $pricechange;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \ProductGp
     *
     * @ManyToOne(targetEntity="ProductGp")
     * @JoinColumns({
     *   @JoinColumn(name="idproductgp", referencedColumnName="idproduct_gp")
     * })
     */
    private $idproductgp;

    function getIdproductgpalternatives() {
        return $this->idproductgpalternatives;
    }

    function getDescription() {
        return $this->description;
    }

    function getPricechange() {
        return $this->pricechange;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getIdproductgp() {
        return $this->idproductgp;
    }

    function setIdproductgpalternatives($idproductgpalternatives) {
        $this->idproductgpalternatives = $idproductgpalternatives;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setPricechange($pricechange) {
        $this->pricechange = $pricechange;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdproductgp(\ProductGp $idproductgp) {
        $this->idproductgp = $idproductgp;
    }


}

