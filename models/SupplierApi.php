<?php



use Doctrine\Mapping as ORM;

/**
 * SupplierApi
 *
 * @Table(name="supplier_api", indexes={@Index(name="idsupplier_indx", columns={"idsupplier"}), @Index(name="idsuppliertypeapi_indx", columns={"idtypeapi"}), @Index(name="idformatapi_indx", columns={"idformatapi"})})
 * @Entity
 */
class SupplierApi
{
    /**
     * @var integer
     *
     * @Column(name="idsupplierapi", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsupplierapi;

    /**
     * @var string
     *
     * @Column(name="address", type="text", length=65535, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @Column(name="username", type="string", length=250, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @Column(name="password", type="text", length=65535, nullable=false)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    /**
     * @var \SupplierFormatApi
     *
     * @ManyToOne(targetEntity="SupplierFormatApi")
     * @JoinColumns({
     *   @JoinColumn(name="idformatapi", referencedColumnName="idsupplierformatapi")
     * })
     */
    private $idformatapi;

    /**
     * @var \Supplier
     *
     * @ManyToOne(targetEntity="Supplier")
     * @JoinColumns({
     *   @JoinColumn(name="idsupplier", referencedColumnName="idsupplier")
     * })
     */
    private $idsupplier;

    /**
     * @var \SupplierTypeApi
     *
     * @ManyToOne(targetEntity="SupplierTypeApi")
     * @JoinColumns({
     *   @JoinColumn(name="idtypeapi", referencedColumnName="idsuppliertypeapi")
     * })
     */
    private $idtypeapi;

    function getIdsupplierapi() {
        return $this->idsupplierapi;
    }

    function getAddress() {
        return $this->address;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getIdformatapi() {
        return $this->idformatapi;
    }

    function getIdsupplier() {
        return $this->idsupplier;
    }

    function getIdtypeapi() {
        return $this->idtypeapi;
    }

    function setIdsupplierapi($idsupplierapi) {
        $this->idsupplierapi = $idsupplierapi;
    }

    function setAddress($address) {
        $this->address = $address;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdformatapi(\SupplierFormatApi $idformatapi) {
        $this->idformatapi = $idformatapi;
    }

    function setIdsupplier(\Supplier $idsupplier) {
        $this->idsupplier = $idsupplier;
    }

    function setIdtypeapi(\SupplierTypeApi $idtypeapi) {
        $this->idtypeapi = $idtypeapi;
    }


}

