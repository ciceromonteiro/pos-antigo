<?php



use Doctrine\Mapping as ORM;

/**
 * LanguageData
 *
 * @Table(name="language_data", indexes={@Index(name="language_index", columns={"idlanguage"})})
 * @Entity
 */
class LanguageData
{
    /**
     * @var integer
     *
     * @Column(name="idlanguagedata", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlanguagedata;

    /**
     * @var integer
     *
     * @Column(name="page", type="integer", nullable=false)
     */
    private $page;

    /**
     * @var integer
     *
     * @Column(name="data_type", type="integer", nullable=false)
     */
    private $dataType;

    /**
     * @var integer
     *
     * @Column(name="data_value", type="integer", nullable=false)
     */
    private $dataValue;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Language
     *
     * @ManyToOne(targetEntity="Language")
     * @JoinColumns({
     *   @JoinColumn(name="idlanguage", referencedColumnName="idlanguage")
     * })
     */
    private $idlanguage;

    function getIdlanguagedata() {
        return $this->idlanguagedata;
    }

    function getPage() {
        return $this->page;
    }

    function getDataType() {
        return $this->dataType;
    }

    function getDataValue() {
        return $this->dataValue;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getIdlanguage() {
        return $this->idlanguage;
    }

    function setIdlanguagedata($idlanguagedata) {
        $this->idlanguagedata = $idlanguagedata;
    }

    function setPage($page) {
        $this->page = $page;
    }

    function setDataType($dataType) {
        $this->dataType = $dataType;
    }

    function setDataValue($dataValue) {
        $this->dataValue = $dataValue;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdlanguage(\Language $idlanguage) {
        $this->idlanguage = $idlanguage;
    }


}

