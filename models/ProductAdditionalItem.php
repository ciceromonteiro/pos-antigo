<?php



use Doctrine\Mapping as ORM;

/**
 * ProductAdditionalItem
 *
 * @Table(name="product_additional_item", indexes={@Index(name="product_idproduct", columns={"product_idproduct"}), @Index(name="group_idgroup", columns={"group_idgroup"}), @Index(name="id_product_additional", columns={"id_product_additional"})})
 * @Entity
 */
class ProductAdditionalItem
{
    /**
     * @var integer
     *
     * @Column(name="idproductadditionalitem", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductadditionalitem;

    /**
     * @var integer
     *
     * @Column(name="price_type", type="integer", nullable=false)
     */
    private $priceType;

    /**
     * @var string
     *
     * @Column(name="price", type="decimal", precision=11, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var integer
     *
     * @Column(name="qty", type="integer", nullable=false)
     */
    private $qty;

    /**
     * @var integer
     *
     * @Column(name="bruk_factor", type="integer", nullable=true)
     */
    private $brukFactor;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \ProductGp
     *
     * @ManyToOne(targetEntity="ProductGp")
     * @JoinColumns({
     *   @JoinColumn(name="group_idgroup", referencedColumnName="idproduct_gp")
     * })
     */
    private $groupgroup;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \ProductAdditional
     *
     * @ManyToOne(targetEntity="ProductAdditional")
     * @JoinColumns({
     *   @JoinColumn(name="id_product_additional", referencedColumnName="idproductadditional")
     * })
     */
    private $idProductAdditional;

    function getIdproductadditionalitem() {
        return $this->idproductadditionalitem;
    }

    function getPriceType() {
        return $this->priceType;
    }

    function getPrice() {
        return $this->price;
    }

    function getQty() {
        return $this->qty;
    }

    function getBrukFactor() {
        return $this->brukFactor;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getGroupgroup() {
        return $this->groupgroup;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function getIdProductAdditional() {
        return $this->idProductAdditional;
    }

    function setIdproductadditionalitem($idproductadditionalitem) {
        $this->idproductadditionalitem = $idproductadditionalitem;
    }

    function setPriceType($priceType) {
        $this->priceType = $priceType;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setQty($qty) {
        $this->qty = $qty;
    }

    function setBrukFactor($brukFactor) {
        $this->brukFactor = $brukFactor;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setGroupgroup(\ProductGp $groupgroup) {
        $this->groupgroup = $groupgroup;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }

    function setIdProductAdditional(\ProductAdditional $idProductAdditional) {
        $this->idProductAdditional = $idProductAdditional;
    }


}

