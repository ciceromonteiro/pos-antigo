<?php



use Doctrine\Mapping as ORM;

/**
 * ProductWty
 *
 * @Table(name="product_wty", indexes={@Index(name="fk_product_wty_warranty1_idx", columns={"warranty_idwarranty"}), @Index(name="fk_product_wty_product1_idx", columns={"product_idproduct"})})
 * @Entity
 */
class ProductWty
{
    /**
     * @var integer
     *
     * @Column(name="idproduct_wty", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductWty;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Warranty
     *
     * @ManyToOne(targetEntity="Warranty")
     * @JoinColumns({
     *   @JoinColumn(name="warranty_idwarranty", referencedColumnName="idwarranty")
     * })
     */
    private $warrantywarranty;



    /**
     * Get idproductWty
     *
     * @return integer
     */
    public function getIdproductWty()
    {
        return $this->idproductWty;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ProductWty
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ProductWty
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ProductWty
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return ProductWty
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return ProductWty
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return ProductWty
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }

    /**
     * Set warrantywarranty
     *
     * @param \Warranty $warrantywarranty
     *
     * @return ProductWty
     */
    public function setWarrantywarranty(\Warranty $warrantywarranty = null)
    {
        $this->warrantywarranty = $warrantywarranty;

        return $this;
    }

    /**
     * Get warrantywarranty
     *
     * @return \Warranty
     */
    public function getWarrantywarranty()
    {
        return $this->warrantywarranty;
    }
}
