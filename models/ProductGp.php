<?php



use Doctrine\Mapping as ORM;

/**
 * ProductGp
 *
 * @Table(name="product_gp", indexes={@Index(name="fk_product_gp_product_gp1_idx", columns={"product_gp_idproduct_gp"}), @Index(name="fk_printer_idx", columns={"printer"}), @Index(name="fk_department_idx", columns={"department"}), @Index(name="fk_rules_idx", columns={"rules"})})
 * @Entity
 */
class ProductGp
{
    /**
     * @var integer
     *
     * @Column(name="idproduct_gp", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductGp;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Department
     *
     * @ManyToOne(targetEntity="Department")
     * @JoinColumns({
     *   @JoinColumn(name="department", referencedColumnName="iddepartment")
     * })
     */
    private $department;

    /**
     * @var \Printer
     *
     * @ManyToOne(targetEntity="Printer")
     * @JoinColumns({
     *   @JoinColumn(name="printer", referencedColumnName="idprinter")
     * })
     */
    private $printer;

    /**
     * @var \ProductGp
     *
     * @ManyToOne(targetEntity="ProductGp")
     * @JoinColumns({
     *   @JoinColumn(name="product_gp_idproduct_gp", referencedColumnName="idproduct_gp")
     * })
     */
    private $productGpproductGp;

    /**
     * @var \OrderRule
     *
     * @ManyToOne(targetEntity="OrderRule")
     * @JoinColumns({
     *   @JoinColumn(name="rules", referencedColumnName="idorderrule")
     * })
     */
    private $rules;

    function getIdproductGp() {
        return $this->idproductGp;
    }

    function getName() {
        return $this->name;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getDepartment() {
        return $this->department;
    }

    function getPrinter() {
        return $this->printer;
    }

    function getProductGpproductGp() {
        return $this->productGpproductGp;
    }

    function getRules() {
        return $this->rules;
    }

    function setIdproductGp($idproductGp) {
        $this->idproductGp = $idproductGp;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setDepartment($department) {
        $this->department = $department;
    }

    function setPrinter($printer) {
        $this->printer = $printer;
    }

    function setProductGpproductGp($productGpproductGp) {
        $this->productGpproductGp = $productGpproductGp;
    }

    function setRules($rules) {
        $this->rules = $rules;
    }


}

