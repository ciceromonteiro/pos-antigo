<?php



use Doctrine\Mapping as ORM;

/**
 * Subproduct
 *
 * @Table(name="subproduct", indexes={@Index(name="fk_subproduct_product1_idx", columns={"product_idproduct"})})
 * @Entity
 */
class Subproduct
{
    /**
     * @var integer
     *
     * @Column(name="idsubproduct", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsubproduct;

    /**
     * @var integer
     *
     * @Column(name="qty_allowed", type="integer", nullable=false)
     */
    private $qtyAllowed;

    /**
     * @var integer
     *
     * @Column(name="qty_min", type="integer", nullable=false)
     */
    private $qtyMin;

    /**
     * @var integer
     *
     * @Column(name="order", type="integer", nullable=false)
     */
    private $order;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;



    /**
     * Get idsubproduct
     *
     * @return integer
     */
    public function getIdsubproduct()
    {
        return $this->idsubproduct;
    }

    /**
     * Set qtyAllowed
     *
     * @param integer $qtyAllowed
     *
     * @return Subproduct
     */
    public function setQtyAllowed($qtyAllowed)
    {
        $this->qtyAllowed = $qtyAllowed;

        return $this;
    }

    /**
     * Get qtyAllowed
     *
     * @return integer
     */
    public function getQtyAllowed()
    {
        return $this->qtyAllowed;
    }

    /**
     * Set qtyMin
     *
     * @param integer $qtyMin
     *
     * @return Subproduct
     */
    public function setQtyMin($qtyMin)
    {
        $this->qtyMin = $qtyMin;

        return $this;
    }

    /**
     * Get qtyMin
     *
     * @return integer
     */
    public function getQtyMin()
    {
        return $this->qtyMin;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return Subproduct
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Subproduct
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Subproduct
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Subproduct
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Subproduct
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return Subproduct
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }
}
