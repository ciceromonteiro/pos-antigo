<?php

use Doctrine\Mapping as ORM;

/**
 * WordTranslated
 *
 * @Table(name="word_translated", indexes={@Index(name="fk_word_translated_word_original_idx", columns={"word_original_idword_original"})})
 * @Entity
 */
class WordTranslated {

    /**
     * @var integer
     *
     * @Column(name="idword_translated", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idwordTranslated;

    /**
     * @var string
     *
     * @Column(name="new_word", type="string", length=45, nullable=false)
     */
    private $newWord;

    /**
     * @var \WordOriginal
     *
     * @ManyToOne(targetEntity="WordOriginal")
     * @JoinColumns({
     *   @JoinColumn(name="word_original_idword_original", referencedColumnName="idword_original")
     * })
     */
    private $wordOriginalWordOriginal;

    /**
     * @var \Language
     *
     * @ManyToOne(targetEntity="Language")
     * @JoinColumns({
     *   @JoinColumn(name="language_idlanguage", referencedColumnName="idlanguage")
     * })
     */
    private $languagelanguage;
    
    function getIdwordTranslated() {
        return $this->idwordTranslated;
    }

    function getLanguagelanguage() {
        return $this->languagelanguage;
    }

    function getNewWord() {
        return $this->newWord;
    }

    function getWordOriginalWordOriginal() {
        return $this->wordOriginalWordOriginal;
    }

    function setIdwordTranslated($idwordTranslated) {
        $this->idwordTranslated = $idwordTranslated;
    }

    function setLanguagelanguage($languagelanguage) {
        $this->languagelanguage = $languagelanguage;
    }

    function setNewWord($newWord) {
        $this->newWord = $newWord;
    }

    function setWordOriginalWordOriginal($wordOriginalWordOriginal) {
        $this->wordOriginalWordOriginal = $wordOriginalWordOriginal;
    }

}
