<?php




/**
 * UsersPos
 *
 * @Table(name="users_pos", indexes={@Index(name="fk_users_pos_users1_idx", columns={"users_idusers"}), @Index(name="fk_users_pos_pos1_idx", columns={"pos_idpos"})})
 * @Entity
 */
class UsersPos
{
    /**
     * @var integer
     *
     * @Column(name="idusers_pos", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idusersPos;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;

    /**
     * Get idusersPos
     *
     * @return integer
     */
    public function getIdusersPos()
    {
        return $this->idusersPos;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return UsersPos
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return UsersPos
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return UsersPos
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return UsersPos
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set pospos
     *
     * @param \Pos $pospos
     *
     * @return UsersPos
     */
    public function setPospos(\Pos $pospos = null)
    {
        $this->pospos = $pospos;

        return $this;
    }

    /**
     * Get pospos
     *
     * @return \Pos
     */
    public function getPospos()
    {
        return $this->pospos;
    }

    /**
     * Set usersusers
     *
     * @param \Users $usersusers
     *
     * @return UsersPos
     */
    public function setUsersusers(\Users $usersusers = null)
    {
        $this->usersusers = $usersusers;

        return $this;
    }

    /**
     * Get usersusers
     *
     * @return \Users
     */
    public function getUsersusers()
    {
        return $this->usersusers;
    }
}
