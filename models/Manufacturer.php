<?php



use Doctrine\Mapping as ORM;

/**
 * Manufacturer
 *
 * @Table(name="manufacturer")
 * @Entity
 */
class Manufacturer
{
    /**
     * @var integer
     *
     * @Column(name="idmanufacturer", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idmanufacturer;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @Column(name="negociated_discount", type="float", precision=10, scale=0, nullable=false)
     */
    private $negociatedDiscount;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    function getIdmanufacturer() {
        return $this->idmanufacturer;
    }

    function getName() {
        return $this->name;
    }

    function getNegociatedDiscount() {
        return $this->negociatedDiscount;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdmanufacturer($idmanufacturer) {
        $this->idmanufacturer = $idmanufacturer;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setNegociatedDiscount($negociatedDiscount) {
        $this->negociatedDiscount = $negociatedDiscount;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

