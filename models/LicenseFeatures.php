<?php



use Doctrine\Mapping as ORM;

/**
 * LicenseFeatures
 *
 * @Table(name="license_features", indexes={@Index(name="indx_features", columns={"idfeatures"})})
 * @Entity
 */
class LicenseFeatures
{
    /**
     * @var integer
     *
     * @Column(name="idfeatures", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idfeatures;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=250, nullable=false)
     */
    private $description;

    function getIdfeatures() {
        return $this->idfeatures;
    }

    function getDescription() {
        return $this->description;
    }

    function setIdfeatures($idfeatures) {
        $this->idfeatures = $idfeatures;
    }

    function setDescription($description) {
        $this->description = $description;
    }
}

