<?php

use Doctrine\Mapping as ORM;

/**
 * Fidelity
 *
 * @Table(name="fidelity", indexes={@Index(name="fk_fidelity_product1_idx", columns={"product_idproduct"})})
 * @Entity
 */
class Fidelity {

    /**
     * @var integer
     *
     * @Column(name="idfidelity", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idfidelity;

    /**
     * @var integer
     *
     * @Column(name="type_fidelity", type="integer", nullable=false)
     */
    private $typeFidelity;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $value;

    /**
     * @var integer
     *
     * @Column(name="quantity", type="integer", nullable=false)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @Column(name="awards_type", type="integer", nullable=false)
     */
    private $awardsType;

     /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;
    
    function getIdfidelity() {
        return $this->idfidelity;
    }

    function getTypeFidelity() {
        return $this->typeFidelity;
    }

    function getValue() {
        return $this->value;
    }

    function getQuantity() {
        return $this->quantity;
    }

    function getAwardsType() {
        return $this->awardsType;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function setIdfidelity($idfidelity) {
        $this->idfidelity = $idfidelity;
    }

    function setTypeFidelity($typeFidelity) {
        $this->typeFidelity = $typeFidelity;
    }

    function setValue($value) {
        $this->value = $value;
    }

    function setQuantity($quantity) {
        $this->quantity = $quantity;
    }

    function setAwardsType($awardsType) {
        $this->awardsType = $awardsType;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setProductproduct($productproduct) {
        $this->productproduct = $productproduct;
    }

}
