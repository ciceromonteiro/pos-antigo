<?php

use Doctrine\Mapping as ORM;

/**
 * ProductInSection
 *
 * @Table(name="product_in_section", indexes={@Index(name="fk_product_in_warehouse_product1_idx", columns={"product_idproduct"}), @Index(name="fk_product_in_warehouse_section1_idx", columns={"section_idsection"})})
 * @Entity
 */
class ProductInSection {

    /**
     * @var integer
     *
     * @Column(name="idproduct_in_section", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductInSection;

    /**
     * @var integer
     *
     * @Column(name="qty", type="integer", nullable=false)
     */
    private $qty;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Section
     *
     * @ManyToOne(targetEntity="Section")
     * @JoinColumns({
     *   @JoinColumn(name="section_idsection", referencedColumnName="idsection")
     * })
     */
    private $sectionsection;

    /**
     * @var \DateTime
     *
     * @Column(name="`date`", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @Column(name="expiration_date", type="datetime", nullable=true)
     */
    private $expirationdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * Get idproductInSection
     *
     * @return integer
     */
    public function getIdproductInSection() {
        return $this->idproductInSection;
    }

    /**
     * Set qty
     *
     * @param integer $qty
     *
     * @return ProductInSection
     */
    public function setQty($qty) {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     * @return integer
     */
    public function getQty() {
        return $this->qty;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return ProductInSection
     */
    public function setProductproduct(\Product $productproduct = null) {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct() {
        return $this->productproduct;
    }

    /**
     * Set sectionsection
     *
     * @param \Section $sectionsection
     *
     * @return ProductInSection
     */
    public function setSectionsection(\Section $sectionsection = null) {
        $this->sectionsection = $sectionsection;

        return $this;
    }

    /**
     * Get sectionsection
     *
     * @return \Section
     */
    public function getSectionsection() {
        return $this->sectionsection;
    }
    
    function getDate() {
        return $this->date;
    }

    function getExpirationdate() {
        return $this->expirationdate;
    }

    function setDate(\DateTime $date) {
        $this->date = $date;
    }

    function setExpirationdate($expirationdate) {
        $this->expirationdate = $expirationdate;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return ProductInSection
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return ProductInSection
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return ProductInSection
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Get active
     *
     * @return integer
     */
    public function getActive()
    {
        return $this->active;
    }

    function setActive($active){
        $this->active = $active;
    }
}
