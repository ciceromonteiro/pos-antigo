<?php



use Doctrine\Mapping as ORM;

/**
 * SubproductLine
 *
 * @Table(name="subproduct_line", indexes={@Index(name="fk_subproduct_line_subproduct1_idx", columns={"subproduct_idsubproduct"}), @Index(name="fk_subproduct_line_product1_idx", columns={"product_idproduct"})})
 * @Entity
 */
class SubproductLine
{
    /**
     * @var integer
     *
     * @Column(name="idsubproduct_line", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsubproductLine;

    /**
     * @var integer
     *
     * @Column(name="load", type="integer", nullable=false)
     */
    private $load;

    /**
     * @var string
     *
     * @Column(name="subproduct_linecol", type="string", length=45, nullable=true)
     */
    private $subproductLinecol;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Subproduct
     *
     * @ManyToOne(targetEntity="Subproduct")
     * @JoinColumns({
     *   @JoinColumn(name="subproduct_idsubproduct", referencedColumnName="idsubproduct")
     * })
     */
    private $subproductsubproduct;



    /**
     * Get idsubproductLine
     *
     * @return integer
     */
    public function getIdsubproductLine()
    {
        return $this->idsubproductLine;
    }

    /**
     * Set load
     *
     * @param integer $load
     *
     * @return SubproductLine
     */
    public function setLoad($load)
    {
        $this->load = $load;

        return $this;
    }

    /**
     * Get load
     *
     * @return integer
     */
    public function getLoad()
    {
        return $this->load;
    }

    /**
     * Set subproductLinecol
     *
     * @param string $subproductLinecol
     *
     * @return SubproductLine
     */
    public function setSubproductLinecol($subproductLinecol)
    {
        $this->subproductLinecol = $subproductLinecol;

        return $this;
    }

    /**
     * Get subproductLinecol
     *
     * @return string
     */
    public function getSubproductLinecol()
    {
        return $this->subproductLinecol;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return SubproductLine
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }

    /**
     * Set subproductsubproduct
     *
     * @param \Subproduct $subproductsubproduct
     *
     * @return SubproductLine
     */
    public function setSubproductsubproduct(\Subproduct $subproductsubproduct = null)
    {
        $this->subproductsubproduct = $subproductsubproduct;

        return $this;
    }

    /**
     * Get subproductsubproduct
     *
     * @return \Subproduct
     */
    public function getSubproductsubproduct()
    {
        return $this->subproductsubproduct;
    }
}
