<?php



use Doctrine\Mapping as ORM;

/**
 * SettingsGeneral
 *
 * @Table(name="settings_general")
 * @Entity
 */
class SettingsGeneral
{
    /**
     * @var integer
     *
     * @Column(name="idsettingsgeneral", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsettingsgeneral;

    /**
     * @var string
     *
     * @Column(name="data_type", type="string", length=300, nullable=true)
     */
    private $dataType;
    
    /**
     * @var string
     *
     * @Column(name="data_value", type="string", length=300, nullable=true)
     */
    private $dataValue;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;


    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active;
    function getIdsettingsgeneral() {
        return $this->idsettingsgeneral;
    }

    function getDataType() {
        return $this->dataType;
    }

    function getDataValue() {
        return $this->dataValue;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdsettingsgeneral($idsettingsgeneral) {
        $this->idsettingsgeneral = $idsettingsgeneral;
    }

    function setDataType($dataType) {
        $this->dataType = $dataType;
    }

    function setDataValue($dataValue) {
        $this->dataValue = $dataValue;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}
