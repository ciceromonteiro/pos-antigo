<?php



use Doctrine\Mapping as ORM;

/**
 * AppliedTx
 *
 * @Table(name="applied_tx", indexes={@Index(name="fk_applied_tx_tax1_idx", columns={"tax_idtax"}), @Index(name="fk_applied_tx_customer_gp1_idx", columns={"customer_gp_idcustomer_gp"}), @Index(name="fk_applied_tx_zz_state1_idx", columns={"zz_state_idzz_state"}), @Index(name="fk_applied_tx_zz_country1_idx", columns={"zz_country_idzz_country"}), @Index(name="fk_applied_tx_product1_idx", columns={"product_idproduct"})})
 * @Entity
 */
class AppliedTx
{
    /**
     * @var integer
     *
     * @Column(name="idapplied_tx", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idappliedTx;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \CustomerGp
     *
     * @ManyToOne(targetEntity="CustomerGp")
     * @JoinColumns({
     *   @JoinColumn(name="customer_gp_idcustomer_gp", referencedColumnName="idcustomer_gp")
     * })
     */
    private $customerGpcustomerGp;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Tax
     *
     * @ManyToOne(targetEntity="Tax")
     * @JoinColumns({
     *   @JoinColumn(name="tax_idtax", referencedColumnName="idtax")
     * })
     */
    private $taxtax;

    /**
     * @var \ZzCountry
     *
     * @ManyToOne(targetEntity="ZzCountry")
     * @JoinColumns({
     *   @JoinColumn(name="zz_country_idzz_country", referencedColumnName="idzz_country")
     * })
     */
    private $zzCountryzzCountry;

    /**
     * @var \ZzState
     *
     * @ManyToOne(targetEntity="ZzState")
     * @JoinColumns({
     *   @JoinColumn(name="zz_state_idzz_state", referencedColumnName="idzz_state")
     * })
     */
    private $zzStatezzState;



    /**
     * Get idappliedTx
     *
     * @return integer
     */
    public function getIdappliedTx()
    {
        return $this->idappliedTx;
    }

    /**
     * Set customerGpcustomerGp
     *
     * @param \CustomerGp $customerGpcustomerGp
     *
     * @return AppliedTx
     */
    public function setCustomerGpcustomerGp(\CustomerGp $customerGpcustomerGp = null)
    {
        $this->customerGpcustomerGp = $customerGpcustomerGp;

        return $this;
    }

    /**
     * Get customerGpcustomerGp
     *
     * @return \CustomerGp
     */
    public function getCustomerGpcustomerGp()
    {
        return $this->customerGpcustomerGp;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return AppliedTx
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }

    /**
     * Set taxtax
     *
     * @param \Tax $taxtax
     *
     * @return AppliedTx
     */
    public function setTaxtax(\Tax $taxtax = null)
    {
        $this->taxtax = $taxtax;

        return $this;
    }

    /**
     * Get taxtax
     *
     * @return \Tax
     */
    public function getTaxtax()
    {
        return $this->taxtax;
    }

    /**
     * Set zzCountryzzCountry
     *
     * @param \ZzCountry $zzCountryzzCountry
     *
     * @return AppliedTx
     */
    public function setZzCountryzzCountry(\ZzCountry $zzCountryzzCountry = null)
    {
        $this->zzCountryzzCountry = $zzCountryzzCountry;

        return $this;
    }

    /**
     * Get zzCountryzzCountry
     *
     * @return \ZzCountry
     */
    public function getZzCountryzzCountry()
    {
        return $this->zzCountryzzCountry;
    }

    /**
     * Set zzStatezzState
     *
     * @param \ZzState $zzStatezzState
     *
     * @return AppliedTx
     */
    public function setZzStatezzState(\ZzState $zzStatezzState = null)
    {
        $this->zzStatezzState = $zzStatezzState;

        return $this;
    }

    /**
     * Get zzStatezzState
     *
     * @return \ZzState
     */
    public function getZzStatezzState()
    {
        return $this->zzStatezzState;
    }
}
