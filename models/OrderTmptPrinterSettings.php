<?php



use Doctrine\Mapping as ORM;

/**
 * OrderTmptPrinterSettings
 *
 * @Table(name="order_tmpt_printer_settings", indexes={@Index(name="id_order_tmpt", columns={"id_order_tmpt"}), @Index(name="id_printer", columns={"id_printer"})})
 * @Entity
 */
class OrderTmptPrinterSettings
{
    /**
     * @var integer
     *
     * @Column(name="id_order_tmpt_printer_settings", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idOrderTmptPrinterSettings;

    /**
     * @var integer
     *
     * @Column(name="numbeOfWaysToPrint", type="integer", nullable=true)
     */
    private $numbeofwaystoprint;

    /**
     * @var string
     *
     * @Column(name="printValues", type="string", length=10, nullable=true)
     */
    private $printvalues;

    /**
     * @var string
     *
     * @Column(name="printPricesWithTaxes", type="string", length=10, nullable=true)
     */
    private $printpriceswithtaxes;

    /**
     * @var string
     *
     * @Column(name="hideTotalPrice", type="string", length=10, nullable=true)
     */
    private $hidetotalprice;

    /**
     * @var string
     *
     * @Column(name="printObs", type="string", length=10, nullable=true)
     */
    private $printobs;

    /**
     * @var string
     *
     * @Column(name="printTheObsInASeparateLine", type="string", length=10, nullable=true)
     */
    private $printtheobsinaseparateline;

    /**
     * @var string
     *
     * @Column(name="linesWithDifferentBackgrounds", type="string", length=10, nullable=true)
     */
    private $lineswithdifferentbackgrounds;

    /**
     * @var string
     *
     * @Column(name="sendByEmail", type="string", length=10, nullable=true)
     */
    private $sendbyemail;

    /**
     * @var string
     *
     * @Column(name="sendBySMS", type="string", length=10, nullable=true)
     */
    private $sendbysms;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \OrderTmpt
     *
     * @ManyToOne(targetEntity="OrderTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="id_order_tmpt", referencedColumnName="idordertmpt")
     * })
     */
    private $idOrderTmpt;

    /**
     * @var \Printer
     *
     * @ManyToOne(targetEntity="Printer")
     * @JoinColumns({
     *   @JoinColumn(name="id_printer", referencedColumnName="idprinter")
     * })
     */
    private $idPrinter;

    function getIdOrderTmptPrinterSettings() {
        return $this->idOrderTmptPrinterSettings;
    }

    function getNumbeofwaystoprint() {
        return $this->numbeofwaystoprint;
    }

    function getPrintvalues() {
        return $this->printvalues;
    }

    function getPrintpriceswithtaxes() {
        return $this->printpriceswithtaxes;
    }

    function getHidetotalprice() {
        return $this->hidetotalprice;
    }

    function getPrintobs() {
        return $this->printobs;
    }

    function getPrinttheobsinaseparateline() {
        return $this->printtheobsinaseparateline;
    }

    function getLineswithdifferentbackgrounds() {
        return $this->lineswithdifferentbackgrounds;
    }

    function getSendbyemail() {
        return $this->sendbyemail;
    }

    function getSendbysms() {
        return $this->sendbysms;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getIdOrderTmpt() {
        return $this->idOrderTmpt;
    }

    function getIdPrinter() {
        return $this->idPrinter;
    }

    function setIdOrderTmptPrinterSettings($idOrderTmptPrinterSettings) {
        $this->idOrderTmptPrinterSettings = $idOrderTmptPrinterSettings;
    }

    function setNumbeofwaystoprint($numbeofwaystoprint) {
        $this->numbeofwaystoprint = $numbeofwaystoprint;
    }

    function setPrintvalues($printvalues) {
        $this->printvalues = $printvalues;
    }

    function setPrintpriceswithtaxes($printpriceswithtaxes) {
        $this->printpriceswithtaxes = $printpriceswithtaxes;
    }

    function setHidetotalprice($hidetotalprice) {
        $this->hidetotalprice = $hidetotalprice;
    }

    function setPrintobs($printobs) {
        $this->printobs = $printobs;
    }

    function setPrinttheobsinaseparateline($printtheobsinaseparateline) {
        $this->printtheobsinaseparateline = $printtheobsinaseparateline;
    }

    function setLineswithdifferentbackgrounds($lineswithdifferentbackgrounds) {
        $this->lineswithdifferentbackgrounds = $lineswithdifferentbackgrounds;
    }

    function setSendbyemail($sendbyemail) {
        $this->sendbyemail = $sendbyemail;
    }

    function setSendbysms($sendbysms) {
        $this->sendbysms = $sendbysms;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdOrderTmpt($idOrderTmpt) {
        $this->idOrderTmpt = $idOrderTmpt;
    }

    function setIdPrinter($idPrinter) {
        $this->idPrinter = $idPrinter;
    }


}

