<?php



use Doctrine\Mapping as ORM;

/**
 * Settings
 *
 * @Table(name="settings", indexes={@Index(name="fk_settings_language1_idx", columns={"language_idlanguage"}), @Index(name="fk_settings_screen1_idx", columns={"screen_idscreen"}), @Index(name="fk_settings_sms_platform1_idx", columns={"sms_platform_idsms_platform"}), @Index(name="fk_settings_exchange_platform1_idx", columns={"exchange_platform_idexchange_platform"})})
 * @Entity
 */
class Settings
{
    /**
     * @var integer
     *
     * @Column(name="idsettings", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsettings;

    /**
     * @var boolean
     *
     * @Column(name="import_only_prod_name", type="boolean", nullable=false)
     */
    private $importOnlyProdName;

    /**
     * @var boolean
     *
     * @Column(name="print_sold_prod_at_closure", type="boolean", nullable=true)
     */
    private $printSoldProdAtClosure;

    /**
     * @var integer
     *
     * @Column(name="product_numeration_min", type="integer", nullable=true)
     */
    private $productNumerationMin;

    /**
     * @var integer
     *
     * @Column(name="product_numeration_max", type="integer", nullable=true)
     */
    private $productNumerationMax;

    /**
     * @var boolean
     *
     * @Column(name="apply_discount_invoice", type="boolean", nullable=true)
     */
    private $applyDiscountInvoice;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var integer
     *
     * @Column(name="invoice_sn", type="integer", nullable=false)
     */
    private $invoiceSn;

    /**
     * @var integer
     *
     * @Column(name="pos_closure_log_sn", type="integer", nullable=false)
     */
    private $posClosureLogSn;

    /**
     * @var integer
     *
     * @Column(name="accounting_report_sn", type="integer", nullable=false)
     */
    private $accountingReportSn;

    /**
     * @var integer
     *
     * @Column(name="invoice_report_sn", type="integer", nullable=false)
     */
    private $invoiceReportSn;

    /**
     * @var integer
     *
     * @Column(name="webshop_sale_report_sn", type="integer", nullable=false)
     */
    private $webshopSaleReportSn;

    /**
     * @var boolean
     *
     * @Column(name="show_pos_session_ir", type="boolean", nullable=false)
     */
    private $showPosSessionIr;

    /**
     * @var boolean
     *
     * @Column(name="show_pos_session_ar", type="boolean", nullable=false)
     */
    private $showPosSessionAr;

    /**
     * @var boolean
     *
     * @Column(name="show_project_of_sale", type="boolean", nullable=false)
     */
    private $showProjectOfSale;

    /**
     * @var string
     *
     * @Column(name="password_admin", type="string", length=45, nullable=false)
     */
    private $passwordAdmin;

    /**
     * @var string
     *
     * @Column(name="password_setup", type="string", length=45, nullable=false)
     */
    private $passwordSetup;

    /**
     * @var string
     *
     * @Column(name="password_closure", type="string", length=45, nullable=false)
     */
    private $passwordClosure;

    /**
     * @var string
     *
     * @Column(name="password_products", type="string", length=45, nullable=false)
     */
    private $passwordProducts;

    /**
     * @var string
     *
     * @Column(name="password_cancel_o", type="string", length=45, nullable=false)
     */
    private $passwordCancelO;

    /**
     * @var string
     *
     * @Column(name="password_cancel_p", type="string", length=45, nullable=false)
     */
    private $passwordCancelP;

    /**
     * @var string
     *
     * @Column(name="sender_mail", type="string", length=90, nullable=true)
     */
    private $senderMail;

    /**
     * @var string
     *
     * @Column(name="alerts_mail", type="string", length=45, nullable=true)
     */
    private $alertsMail;

    /**
     * @var boolean
     *
     * @Column(name="use_default_email_soft", type="boolean", nullable=false)
     */
    private $useDefaultEmailSoft;

    /**
     * @var string
     *
     * @Column(name="sms_user", type="string", length=45, nullable=true)
     */
    private $smsUsers;

    /**
     * @var string
     *
     * @Column(name="sms_password", type="string", length=45, nullable=true)
     */
    private $smsPassword;

    /**
     * @var boolean
     *
     * @Column(name="send_sms_at_closure", type="boolean", nullable=false)
     */
    private $sendSmsAtClosure;

    /**
     * @var string
     *
     * @Column(name="mobile_sms_receiver", type="string", length=30, nullable=true)
     */
    private $mobileSmsReceiver;

    /**
     * @var string
     *
     * @Column(name="exchange_password", type="string", length=45, nullable=true)
     */
    private $exchangePassword;

    /**
     * @var string
     *
     * @Column(name="exchange_user", type="string", length=45, nullable=true)
     */
    private $exchangeUsers;

    /**
     * @var string
     *
     * @Column(name="closure_info", type="text", nullable=true)
     */
    private $closureInfo;

    /**
     * @var boolean
     *
     * @Column(name="print_qty_prod_sold_at_closure", type="boolean", nullable=false)
     */
    private $printQtyProdSoldAtClosure;

    /**
     * @var boolean
     *
     * @Column(name="print_money_coin_in_draw", type="boolean", nullable=false)
     */
    private $printMoneyCoinInDraw;

    /**
     * @var boolean
     *
     * @Column(name="include_sale_by_tax", type="boolean", nullable=false)
     */
    private $includeSaleByTax;

    /**
     * @var boolean
     *
     * @Column(name="use_common_printer", type="boolean", nullable=false)
     */
    private $useCommonPrinter;

    /**
     * @var boolean
     *
     * @Column(name="print_discount_at_closure", type="boolean", nullable=false)
     */
    private $printDiscountAtClosure;

    /**
     * @var integer
     *
     * @Column(name="qty_credit_note_to_print", type="integer", nullable=false)
     */
    private $qtyCreditNoteToPrint;

    /**
     * @var string
     *
     * @Column(name="credit_note_extra_info", type="text", nullable=true)
     */
    private $creditNoteExtraInfo;

    /**
     * @var boolean
     *
     * @Column(name="print_credit_note_use_status", type="boolean", nullable=false)
     */
    private $printCreditNoteUseStatus;

    /**
     * @var boolean
     *
     * @Column(name="print_gift_card_use_status", type="boolean", nullable=false)
     */
    private $printGiftCardUseStatus;

    /**
     * @var integer
     *
     * @Column(name="qty_invoice_to_print", type="integer", nullable=false)
     */
    private $qtyInvoiceToPrint;

    /**
     * @var integer
     *
     * @Column(name="inv_qty_decimals", type="integer", nullable=false)
     */
    private $invQtyDecimals;

    /**
     * @var boolean
     *
     * @Column(name="inv_use_common_printer", type="boolean", nullable=false)
     */
    private $invUseCommonPrinter;

    /**
     * @var boolean
     *
     * @Column(name="inv_print_value_inc_tax", type="boolean", nullable=false)
     */
    private $invPrintValueIncTax;

    /**
     * @var boolean
     *
     * @Column(name="inv_print_project_info", type="boolean", nullable=false)
     */
    private $invPrintProjectInfo;

    /**
     * @var string
     *
     * @Column(name="inv_folder", type="string", length=300, nullable=false)
     */
    private $invFolder;

    /**
     * @var boolean
     *
     * @Column(name="closure_canceled_order", type="boolean", nullable=false)
     */
    private $closureCanceledOrder;

    /**
     * @var boolean
     *
     * @Column(name="closure_canceled_prod", type="boolean", nullable=false)
     */
    private $closureCanceledProd;

    /**
     * @var boolean
     *
     * @Column(name="closure_draw_opening", type="boolean", nullable=false)
     */
    private $closureDrawOpening;

    /**
     * @var boolean
     *
     * @Column(name="rep_closure_gen_pdf", type="boolean", nullable=false)
     */
    private $repClosureGenPdf;

    /**
     * @var boolean
     *
     * @Column(name="rep_closure_send_report", type="boolean", nullable=false)
     */
    private $repClosureSendReport;

    /**
     * @var boolean
     *
     * @Column(name="rep_closure_show_screen", type="boolean", nullable=false)
     */
    private $repClosureShowScreen;

    /**
     * @var boolean
     *
     * @Column(name="rep_closure_print", type="boolean", nullable=false)
     */
    private $repClosurePrint;

    /**
     * @var boolean
     *
     * @Column(name="rep_inv_closure_gen_pdf", type="boolean", nullable=false)
     */
    private $repInvClosureGenPdf;

    /**
     * @var boolean
     *
     * @Column(name="rep_inv_closure_send_report", type="boolean", nullable=false)
     */
    private $repInvClosureSendReport;

    /**
     * @var boolean
     *
     * @Column(name="rep_inv_closure_show_screen", type="boolean", nullable=false)
     */
    private $repInvClosureShowScreen;

    /**
     * @var boolean
     *
     * @Column(name="rep_inv_closure_print", type="boolean", nullable=false)
     */
    private $repInvClosurePrint;

    /**
     * @var boolean
     *
     * @Column(name="rep_inv_detailed", type="boolean", nullable=false)
     */
    private $repInvDetailed;

    /**
     * @var boolean
     *
     * @Column(name="rep_inv_send_cred_adm", type="boolean", nullable=false)
     */
    private $repInvSendCredAdm;

    /**
     * @var boolean
     *
     * @Column(name="rep_web_pay_gen_pdf", type="boolean", nullable=false)
     */
    private $repWebPayGenPdf;

    /**
     * @var boolean
     *
     * @Column(name="rep_web_pay_send_report", type="boolean", nullable=false)
     */
    private $repWebPaySendReport;

    /**
     * @var boolean
     *
     * @Column(name="rep_web_pay_show_screen", type="boolean", nullable=false)
     */
    private $repWebPayShowScreen;

    /**
     * @var boolean
     *
     * @Column(name="rep_web_pay_print", type="boolean", nullable=false)
     */
    private $repWebPayPrint;

    /**
     * @var boolean
     *
     * @Column(name="rep_web_pay_detailed", type="boolean", nullable=false)
     */
    private $repWebPayDetailed;

    /**
     * @var \ExchangePlatform
     *
     * @ManyToOne(targetEntity="ExchangePlatform")
     * @JoinColumns({
     *   @JoinColumn(name="exchange_platform_idexchange_platform", referencedColumnName="idexchange_platform")
     * })
     */
    private $exchangePlatformexchangePlatform;

    /**
     * @var \Language
     *
     * @ManyToOne(targetEntity="Language")
     * @JoinColumns({
     *   @JoinColumn(name="language_idlanguage", referencedColumnName="idlanguage")
     * })
     */
    private $languagelanguage;

    /**
     * @var \Screen
     *
     * @ManyToOne(targetEntity="Screen")
     * @JoinColumns({
     *   @JoinColumn(name="screen_idscreen", referencedColumnName="idscreen")
     * })
     */
    private $screenscreen;

    /**
     * @var \SmsPlatform
     *
     * @ManyToOne(targetEntity="SmsPlatform")
     * @JoinColumns({
     *   @JoinColumn(name="sms_platform_idsms_platform", referencedColumnName="idsms_platform")
     * })
     */
    private $smsPlatformsmsPlatform;



    /**
     * Get idsettings
     *
     * @return integer
     */
    public function getIdsettings()
    {
        return $this->idsettings;
    }

    /**
     * Set importOnlyProdName
     *
     * @param boolean $importOnlyProdName
     *
     * @return Settings
     */
    public function setImportOnlyProdName($importOnlyProdName)
    {
        $this->importOnlyProdName = $importOnlyProdName;

        return $this;
    }

    /**
     * Get importOnlyProdName
     *
     * @return boolean
     */
    public function getImportOnlyProdName()
    {
        return $this->importOnlyProdName;
    }

    /**
     * Set printSoldProdAtClosure
     *
     * @param boolean $printSoldProdAtClosure
     *
     * @return Settings
     */
    public function setPrintSoldProdAtClosure($printSoldProdAtClosure)
    {
        $this->printSoldProdAtClosure = $printSoldProdAtClosure;

        return $this;
    }

    /**
     * Get printSoldProdAtClosure
     *
     * @return boolean
     */
    public function getPrintSoldProdAtClosure()
    {
        return $this->printSoldProdAtClosure;
    }

    /**
     * Set productNumerationMin
     *
     * @param integer $productNumerationMin
     *
     * @return Settings
     */
    public function setProductNumerationMin($productNumerationMin)
    {
        $this->productNumerationMin = $productNumerationMin;

        return $this;
    }

    /**
     * Get productNumerationMin
     *
     * @return integer
     */
    public function getProductNumerationMin()
    {
        return $this->productNumerationMin;
    }

    /**
     * Set productNumerationMax
     *
     * @param integer $productNumerationMax
     *
     * @return Settings
     */
    public function setProductNumerationMax($productNumerationMax)
    {
        $this->productNumerationMax = $productNumerationMax;

        return $this;
    }

    /**
     * Get productNumerationMax
     *
     * @return integer
     */
    public function getProductNumerationMax()
    {
        return $this->productNumerationMax;
    }

    /**
     * Set applyDiscountInvoice
     *
     * @param boolean $applyDiscountInvoice
     *
     * @return Settings
     */
    public function setApplyDiscountInvoice($applyDiscountInvoice)
    {
        $this->applyDiscountInvoice = $applyDiscountInvoice;

        return $this;
    }

    /**
     * Get applyDiscountInvoice
     *
     * @return boolean
     */
    public function getApplyDiscountInvoice()
    {
        return $this->applyDiscountInvoice;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Settings
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Settings
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Settings
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Settings
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set invoiceSn
     *
     * @param integer $invoiceSn
     *
     * @return Settings
     */
    public function setInvoiceSn($invoiceSn)
    {
        $this->invoiceSn = $invoiceSn;

        return $this;
    }

    /**
     * Get invoiceSn
     *
     * @return integer
     */
    public function getInvoiceSn()
    {
        return $this->invoiceSn;
    }

    /**
     * Set posClosureLogSn
     *
     * @param integer $posClosureLogSn
     *
     * @return Settings
     */
    public function setPosClosureLogSn($posClosureLogSn)
    {
        $this->posClosureLogSn = $posClosureLogSn;

        return $this;
    }

    /**
     * Get posClosureLogSn
     *
     * @return integer
     */
    public function getPosClosureLogSn()
    {
        return $this->posClosureLogSn;
    }

    /**
     * Set accountingReportSn
     *
     * @param integer $accountingReportSn
     *
     * @return Settings
     */
    public function setAccountingReportSn($accountingReportSn)
    {
        $this->accountingReportSn = $accountingReportSn;

        return $this;
    }

    /**
     * Get accountingReportSn
     *
     * @return integer
     */
    public function getAccountingReportSn()
    {
        return $this->accountingReportSn;
    }

    /**
     * Set invoiceReportSn
     *
     * @param integer $invoiceReportSn
     *
     * @return Settings
     */
    public function setInvoiceReportSn($invoiceReportSn)
    {
        $this->invoiceReportSn = $invoiceReportSn;

        return $this;
    }

    /**
     * Get invoiceReportSn
     *
     * @return integer
     */
    public function getInvoiceReportSn()
    {
        return $this->invoiceReportSn;
    }

    /**
     * Set webshopSaleReportSn
     *
     * @param integer $webshopSaleReportSn
     *
     * @return Settings
     */
    public function setWebshopSaleReportSn($webshopSaleReportSn)
    {
        $this->webshopSaleReportSn = $webshopSaleReportSn;

        return $this;
    }

    /**
     * Get webshopSaleReportSn
     *
     * @return integer
     */
    public function getWebshopSaleReportSn()
    {
        return $this->webshopSaleReportSn;
    }

    /**
     * Set showPosSessionIr
     *
     * @param boolean $showPosSessionIr
     *
     * @return Settings
     */
    public function setShowPosSessionIr($showPosSessionIr)
    {
        $this->showPosSessionIr = $showPosSessionIr;

        return $this;
    }

    /**
     * Get showPosSessionIr
     *
     * @return boolean
     */
    public function getShowPosSessionIr()
    {
        return $this->showPosSessionIr;
    }

    /**
     * Set showPosSessionAr
     *
     * @param boolean $showPosSessionAr
     *
     * @return Settings
     */
    public function setShowPosSessionAr($showPosSessionAr)
    {
        $this->showPosSessionAr = $showPosSessionAr;

        return $this;
    }

    /**
     * Get showPosSessionAr
     *
     * @return boolean
     */
    public function getShowPosSessionAr()
    {
        return $this->showPosSessionAr;
    }

    /**
     * Set showProjectOfSale
     *
     * @param boolean $showProjectOfSale
     *
     * @return Settings
     */
    public function setShowProjectOfSale($showProjectOfSale)
    {
        $this->showProjectOfSale = $showProjectOfSale;

        return $this;
    }

    /**
     * Get showProjectOfSale
     *
     * @return boolean
     */
    public function getShowProjectOfSale()
    {
        return $this->showProjectOfSale;
    }

    /**
     * Set passwordAdmin
     *
     * @param string $passwordAdmin
     *
     * @return Settings
     */
    public function setPasswordAdmin($passwordAdmin)
    {
        $this->passwordAdmin = $passwordAdmin;

        return $this;
    }

    /**
     * Get passwordAdmin
     *
     * @return string
     */
    public function getPasswordAdmin()
    {
        return $this->passwordAdmin;
    }

    /**
     * Set passwordSetup
     *
     * @param string $passwordSetup
     *
     * @return Settings
     */
    public function setPasswordSetup($passwordSetup)
    {
        $this->passwordSetup = $passwordSetup;

        return $this;
    }

    /**
     * Get passwordSetup
     *
     * @return string
     */
    public function getPasswordSetup()
    {
        return $this->passwordSetup;
    }

    /**
     * Set passwordClosure
     *
     * @param string $passwordClosure
     *
     * @return Settings
     */
    public function setPasswordClosure($passwordClosure)
    {
        $this->passwordClosure = $passwordClosure;

        return $this;
    }

    /**
     * Get passwordClosure
     *
     * @return string
     */
    public function getPasswordClosure()
    {
        return $this->passwordClosure;
    }

    /**
     * Set passwordProducts
     *
     * @param string $passwordProducts
     *
     * @return Settings
     */
    public function setPasswordProducts($passwordProducts)
    {
        $this->passwordProducts = $passwordProducts;

        return $this;
    }

    /**
     * Get passwordProducts
     *
     * @return string
     */
    public function getPasswordProducts()
    {
        return $this->passwordProducts;
    }

    /**
     * Set passwordCancelO
     *
     * @param string $passwordCancelO
     *
     * @return Settings
     */
    public function setPasswordCancelO($passwordCancelO)
    {
        $this->passwordCancelO = $passwordCancelO;

        return $this;
    }

    /**
     * Get passwordCancelO
     *
     * @return string
     */
    public function getPasswordCancelO()
    {
        return $this->passwordCancelO;
    }

    /**
     * Set passwordCancelP
     *
     * @param string $passwordCancelP
     *
     * @return Settings
     */
    public function setPasswordCancelP($passwordCancelP)
    {
        $this->passwordCancelP = $passwordCancelP;

        return $this;
    }

    /**
     * Get passwordCancelP
     *
     * @return string
     */
    public function getPasswordCancelP()
    {
        return $this->passwordCancelP;
    }

    /**
     * Set senderMail
     *
     * @param string $senderMail
     *
     * @return Settings
     */
    public function setSenderMail($senderMail)
    {
        $this->senderMail = $senderMail;

        return $this;
    }

    /**
     * Get senderMail
     *
     * @return string
     */
    public function getSenderMail()
    {
        return $this->senderMail;
    }

    /**
     * Set alertsMail
     *
     * @param string $alertsMail
     *
     * @return Settings
     */
    public function setAlertsMail($alertsMail)
    {
        $this->alertsMail = $alertsMail;

        return $this;
    }

    /**
     * Get alertsMail
     *
     * @return string
     */
    public function getAlertsMail()
    {
        return $this->alertsMail;
    }

    /**
     * Set useDefaultEmailSoft
     *
     * @param boolean $useDefaultEmailSoft
     *
     * @return Settings
     */
    public function setUseDefaultEmailSoft($useDefaultEmailSoft)
    {
        $this->useDefaultEmailSoft = $useDefaultEmailSoft;

        return $this;
    }

    /**
     * Get useDefaultEmailSoft
     *
     * @return boolean
     */
    public function getUseDefaultEmailSoft()
    {
        return $this->useDefaultEmailSoft;
    }

    /**
     * Set smsUsers
     *
     * @param string $smsUsers
     *
     * @return Settings
     */
    public function setSmsUsers($smsUsers)
    {
        $this->smsUsers = $smsUsers;

        return $this;
    }

    /**
     * Get smsUsers
     *
     * @return string
     */
    public function getSmsUsers()
    {
        return $this->smsUsers;
    }

    /**
     * Set smsPassword
     *
     * @param string $smsPassword
     *
     * @return Settings
     */
    public function setSmsPassword($smsPassword)
    {
        $this->smsPassword = $smsPassword;

        return $this;
    }

    /**
     * Get smsPassword
     *
     * @return string
     */
    public function getSmsPassword()
    {
        return $this->smsPassword;
    }

    /**
     * Set sendSmsAtClosure
     *
     * @param boolean $sendSmsAtClosure
     *
     * @return Settings
     */
    public function setSendSmsAtClosure($sendSmsAtClosure)
    {
        $this->sendSmsAtClosure = $sendSmsAtClosure;

        return $this;
    }

    /**
     * Get sendSmsAtClosure
     *
     * @return boolean
     */
    public function getSendSmsAtClosure()
    {
        return $this->sendSmsAtClosure;
    }

    /**
     * Set mobileSmsReceiver
     *
     * @param string $mobileSmsReceiver
     *
     * @return Settings
     */
    public function setMobileSmsReceiver($mobileSmsReceiver)
    {
        $this->mobileSmsReceiver = $mobileSmsReceiver;

        return $this;
    }

    /**
     * Get mobileSmsReceiver
     *
     * @return string
     */
    public function getMobileSmsReceiver()
    {
        return $this->mobileSmsReceiver;
    }

    /**
     * Set exchangePassword
     *
     * @param string $exchangePassword
     *
     * @return Settings
     */
    public function setExchangePassword($exchangePassword)
    {
        $this->exchangePassword = $exchangePassword;

        return $this;
    }

    /**
     * Get exchangePassword
     *
     * @return string
     */
    public function getExchangePassword()
    {
        return $this->exchangePassword;
    }

    /**
     * Set exchangeUsers
     *
     * @param string $exchangeUsers
     *
     * @return Settings
     */
    public function setExchangeUsers($exchangeUsers)
    {
        $this->exchangeUsers = $exchangeUsers;

        return $this;
    }

    /**
     * Get exchangeUsers
     *
     * @return string
     */
    public function getExchangeUsers()
    {
        return $this->exchangeUsers;
    }

    /**
     * Set closureInfo
     *
     * @param string $closureInfo
     *
     * @return Settings
     */
    public function setClosureInfo($closureInfo)
    {
        $this->closureInfo = $closureInfo;

        return $this;
    }

    /**
     * Get closureInfo
     *
     * @return string
     */
    public function getClosureInfo()
    {
        return $this->closureInfo;
    }

    /**
     * Set printQtyProdSoldAtClosure
     *
     * @param boolean $printQtyProdSoldAtClosure
     *
     * @return Settings
     */
    public function setPrintQtyProdSoldAtClosure($printQtyProdSoldAtClosure)
    {
        $this->printQtyProdSoldAtClosure = $printQtyProdSoldAtClosure;

        return $this;
    }

    /**
     * Get printQtyProdSoldAtClosure
     *
     * @return boolean
     */
    public function getPrintQtyProdSoldAtClosure()
    {
        return $this->printQtyProdSoldAtClosure;
    }

    /**
     * Set printMoneyCoinInDraw
     *
     * @param boolean $printMoneyCoinInDraw
     *
     * @return Settings
     */
    public function setPrintMoneyCoinInDraw($printMoneyCoinInDraw)
    {
        $this->printMoneyCoinInDraw = $printMoneyCoinInDraw;

        return $this;
    }

    /**
     * Get printMoneyCoinInDraw
     *
     * @return boolean
     */
    public function getPrintMoneyCoinInDraw()
    {
        return $this->printMoneyCoinInDraw;
    }

    /**
     * Set includeSaleByTax
     *
     * @param boolean $includeSaleByTax
     *
     * @return Settings
     */
    public function setIncludeSaleByTax($includeSaleByTax)
    {
        $this->includeSaleByTax = $includeSaleByTax;

        return $this;
    }

    /**
     * Get includeSaleByTax
     *
     * @return boolean
     */
    public function getIncludeSaleByTax()
    {
        return $this->includeSaleByTax;
    }

    /**
     * Set useCommonPrinter
     *
     * @param boolean $useCommonPrinter
     *
     * @return Settings
     */
    public function setUseCommonPrinter($useCommonPrinter)
    {
        $this->useCommonPrinter = $useCommonPrinter;

        return $this;
    }

    /**
     * Get useCommonPrinter
     *
     * @return boolean
     */
    public function getUseCommonPrinter()
    {
        return $this->useCommonPrinter;
    }

    /**
     * Set printDiscountAtClosure
     *
     * @param boolean $printDiscountAtClosure
     *
     * @return Settings
     */
    public function setPrintDiscountAtClosure($printDiscountAtClosure)
    {
        $this->printDiscountAtClosure = $printDiscountAtClosure;

        return $this;
    }

    /**
     * Get printDiscountAtClosure
     *
     * @return boolean
     */
    public function getPrintDiscountAtClosure()
    {
        return $this->printDiscountAtClosure;
    }

    /**
     * Set qtyCreditNoteToPrint
     *
     * @param integer $qtyCreditNoteToPrint
     *
     * @return Settings
     */
    public function setQtyCreditNoteToPrint($qtyCreditNoteToPrint)
    {
        $this->qtyCreditNoteToPrint = $qtyCreditNoteToPrint;

        return $this;
    }

    /**
     * Get qtyCreditNoteToPrint
     *
     * @return integer
     */
    public function getQtyCreditNoteToPrint()
    {
        return $this->qtyCreditNoteToPrint;
    }

    /**
     * Set creditNoteExtraInfo
     *
     * @param string $creditNoteExtraInfo
     *
     * @return Settings
     */
    public function setCreditNoteExtraInfo($creditNoteExtraInfo)
    {
        $this->creditNoteExtraInfo = $creditNoteExtraInfo;

        return $this;
    }

    /**
     * Get creditNoteExtraInfo
     *
     * @return string
     */
    public function getCreditNoteExtraInfo()
    {
        return $this->creditNoteExtraInfo;
    }

    /**
     * Set printCreditNoteUseStatus
     *
     * @param boolean $printCreditNoteUseStatus
     *
     * @return Settings
     */
    public function setPrintCreditNoteUseStatus($printCreditNoteUseStatus)
    {
        $this->printCreditNoteUseStatus = $printCreditNoteUseStatus;

        return $this;
    }

    /**
     * Get printCreditNoteUseStatus
     *
     * @return boolean
     */
    public function getPrintCreditNoteUseStatus()
    {
        return $this->printCreditNoteUseStatus;
    }

    /**
     * Set printGiftCardUseStatus
     *
     * @param boolean $printGiftCardUseStatus
     *
     * @return Settings
     */
    public function setPrintGiftCardUseStatus($printGiftCardUseStatus)
    {
        $this->printGiftCardUseStatus = $printGiftCardUseStatus;

        return $this;
    }

    /**
     * Get printGiftCardUseStatus
     *
     * @return boolean
     */
    public function getPrintGiftCardUseStatus()
    {
        return $this->printGiftCardUseStatus;
    }

    /**
     * Set qtyInvoiceToPrint
     *
     * @param integer $qtyInvoiceToPrint
     *
     * @return Settings
     */
    public function setQtyInvoiceToPrint($qtyInvoiceToPrint)
    {
        $this->qtyInvoiceToPrint = $qtyInvoiceToPrint;

        return $this;
    }

    /**
     * Get qtyInvoiceToPrint
     *
     * @return integer
     */
    public function getQtyInvoiceToPrint()
    {
        return $this->qtyInvoiceToPrint;
    }

    /**
     * Set invQtyDecimals
     *
     * @param integer $invQtyDecimals
     *
     * @return Settings
     */
    public function setInvQtyDecimals($invQtyDecimals)
    {
        $this->invQtyDecimals = $invQtyDecimals;

        return $this;
    }

    /**
     * Get invQtyDecimals
     *
     * @return integer
     */
    public function getInvQtyDecimals()
    {
        return $this->invQtyDecimals;
    }

    /**
     * Set invUseCommonPrinter
     *
     * @param boolean $invUseCommonPrinter
     *
     * @return Settings
     */
    public function setInvUseCommonPrinter($invUseCommonPrinter)
    {
        $this->invUseCommonPrinter = $invUseCommonPrinter;

        return $this;
    }

    /**
     * Get invUseCommonPrinter
     *
     * @return boolean
     */
    public function getInvUseCommonPrinter()
    {
        return $this->invUseCommonPrinter;
    }

    /**
     * Set invPrintValueIncTax
     *
     * @param boolean $invPrintValueIncTax
     *
     * @return Settings
     */
    public function setInvPrintValueIncTax($invPrintValueIncTax)
    {
        $this->invPrintValueIncTax = $invPrintValueIncTax;

        return $this;
    }

    /**
     * Get invPrintValueIncTax
     *
     * @return boolean
     */
    public function getInvPrintValueIncTax()
    {
        return $this->invPrintValueIncTax;
    }

    /**
     * Set invPrintProjectInfo
     *
     * @param boolean $invPrintProjectInfo
     *
     * @return Settings
     */
    public function setInvPrintProjectInfo($invPrintProjectInfo)
    {
        $this->invPrintProjectInfo = $invPrintProjectInfo;

        return $this;
    }

    /**
     * Get invPrintProjectInfo
     *
     * @return boolean
     */
    public function getInvPrintProjectInfo()
    {
        return $this->invPrintProjectInfo;
    }

    /**
     * Set invFolder
     *
     * @param string $invFolder
     *
     * @return Settings
     */
    public function setInvFolder($invFolder)
    {
        $this->invFolder = $invFolder;

        return $this;
    }

    /**
     * Get invFolder
     *
     * @return string
     */
    public function getInvFolder()
    {
        return $this->invFolder;
    }

    /**
     * Set closureCanceledOrder
     *
     * @param boolean $closureCanceledOrder
     *
     * @return Settings
     */
    public function setClosureCanceledOrder($closureCanceledOrder)
    {
        $this->closureCanceledOrder = $closureCanceledOrder;

        return $this;
    }

    /**
     * Get closureCanceledOrder
     *
     * @return boolean
     */
    public function getClosureCanceledOrder()
    {
        return $this->closureCanceledOrder;
    }

    /**
     * Set closureCanceledProd
     *
     * @param boolean $closureCanceledProd
     *
     * @return Settings
     */
    public function setClosureCanceledProd($closureCanceledProd)
    {
        $this->closureCanceledProd = $closureCanceledProd;

        return $this;
    }

    /**
     * Get closureCanceledProd
     *
     * @return boolean
     */
    public function getClosureCanceledProd()
    {
        return $this->closureCanceledProd;
    }

    /**
     * Set closureDrawOpening
     *
     * @param boolean $closureDrawOpening
     *
     * @return Settings
     */
    public function setClosureDrawOpening($closureDrawOpening)
    {
        $this->closureDrawOpening = $closureDrawOpening;

        return $this;
    }

    /**
     * Get closureDrawOpening
     *
     * @return boolean
     */
    public function getClosureDrawOpening()
    {
        return $this->closureDrawOpening;
    }

    /**
     * Set repClosureGenPdf
     *
     * @param boolean $repClosureGenPdf
     *
     * @return Settings
     */
    public function setRepClosureGenPdf($repClosureGenPdf)
    {
        $this->repClosureGenPdf = $repClosureGenPdf;

        return $this;
    }

    /**
     * Get repClosureGenPdf
     *
     * @return boolean
     */
    public function getRepClosureGenPdf()
    {
        return $this->repClosureGenPdf;
    }

    /**
     * Set repClosureSendReport
     *
     * @param boolean $repClosureSendReport
     *
     * @return Settings
     */
    public function setRepClosureSendReport($repClosureSendReport)
    {
        $this->repClosureSendReport = $repClosureSendReport;

        return $this;
    }

    /**
     * Get repClosureSendReport
     *
     * @return boolean
     */
    public function getRepClosureSendReport()
    {
        return $this->repClosureSendReport;
    }

    /**
     * Set repClosureShowScreen
     *
     * @param boolean $repClosureShowScreen
     *
     * @return Settings
     */
    public function setRepClosureShowScreen($repClosureShowScreen)
    {
        $this->repClosureShowScreen = $repClosureShowScreen;

        return $this;
    }

    /**
     * Get repClosureShowScreen
     *
     * @return boolean
     */
    public function getRepClosureShowScreen()
    {
        return $this->repClosureShowScreen;
    }

    /**
     * Set repClosurePrint
     *
     * @param boolean $repClosurePrint
     *
     * @return Settings
     */
    public function setRepClosurePrint($repClosurePrint)
    {
        $this->repClosurePrint = $repClosurePrint;

        return $this;
    }

    /**
     * Get repClosurePrint
     *
     * @return boolean
     */
    public function getRepClosurePrint()
    {
        return $this->repClosurePrint;
    }

    /**
     * Set repInvClosureGenPdf
     *
     * @param boolean $repInvClosureGenPdf
     *
     * @return Settings
     */
    public function setRepInvClosureGenPdf($repInvClosureGenPdf)
    {
        $this->repInvClosureGenPdf = $repInvClosureGenPdf;

        return $this;
    }

    /**
     * Get repInvClosureGenPdf
     *
     * @return boolean
     */
    public function getRepInvClosureGenPdf()
    {
        return $this->repInvClosureGenPdf;
    }

    /**
     * Set repInvClosureSendReport
     *
     * @param boolean $repInvClosureSendReport
     *
     * @return Settings
     */
    public function setRepInvClosureSendReport($repInvClosureSendReport)
    {
        $this->repInvClosureSendReport = $repInvClosureSendReport;

        return $this;
    }

    /**
     * Get repInvClosureSendReport
     *
     * @return boolean
     */
    public function getRepInvClosureSendReport()
    {
        return $this->repInvClosureSendReport;
    }

    /**
     * Set repInvClosureShowScreen
     *
     * @param boolean $repInvClosureShowScreen
     *
     * @return Settings
     */
    public function setRepInvClosureShowScreen($repInvClosureShowScreen)
    {
        $this->repInvClosureShowScreen = $repInvClosureShowScreen;

        return $this;
    }

    /**
     * Get repInvClosureShowScreen
     *
     * @return boolean
     */
    public function getRepInvClosureShowScreen()
    {
        return $this->repInvClosureShowScreen;
    }

    /**
     * Set repInvClosurePrint
     *
     * @param boolean $repInvClosurePrint
     *
     * @return Settings
     */
    public function setRepInvClosurePrint($repInvClosurePrint)
    {
        $this->repInvClosurePrint = $repInvClosurePrint;

        return $this;
    }

    /**
     * Get repInvClosurePrint
     *
     * @return boolean
     */
    public function getRepInvClosurePrint()
    {
        return $this->repInvClosurePrint;
    }

    /**
     * Set repInvDetailed
     *
     * @param boolean $repInvDetailed
     *
     * @return Settings
     */
    public function setRepInvDetailed($repInvDetailed)
    {
        $this->repInvDetailed = $repInvDetailed;

        return $this;
    }

    /**
     * Get repInvDetailed
     *
     * @return boolean
     */
    public function getRepInvDetailed()
    {
        return $this->repInvDetailed;
    }

    /**
     * Set repInvSendCredAdm
     *
     * @param boolean $repInvSendCredAdm
     *
     * @return Settings
     */
    public function setRepInvSendCredAdm($repInvSendCredAdm)
    {
        $this->repInvSendCredAdm = $repInvSendCredAdm;

        return $this;
    }

    /**
     * Get repInvSendCredAdm
     *
     * @return boolean
     */
    public function getRepInvSendCredAdm()
    {
        return $this->repInvSendCredAdm;
    }

    /**
     * Set repWebPayGenPdf
     *
     * @param boolean $repWebPayGenPdf
     *
     * @return Settings
     */
    public function setRepWebPayGenPdf($repWebPayGenPdf)
    {
        $this->repWebPayGenPdf = $repWebPayGenPdf;

        return $this;
    }

    /**
     * Get repWebPayGenPdf
     *
     * @return boolean
     */
    public function getRepWebPayGenPdf()
    {
        return $this->repWebPayGenPdf;
    }

    /**
     * Set repWebPaySendReport
     *
     * @param boolean $repWebPaySendReport
     *
     * @return Settings
     */
    public function setRepWebPaySendReport($repWebPaySendReport)
    {
        $this->repWebPaySendReport = $repWebPaySendReport;

        return $this;
    }

    /**
     * Get repWebPaySendReport
     *
     * @return boolean
     */
    public function getRepWebPaySendReport()
    {
        return $this->repWebPaySendReport;
    }

    /**
     * Set repWebPayShowScreen
     *
     * @param boolean $repWebPayShowScreen
     *
     * @return Settings
     */
    public function setRepWebPayShowScreen($repWebPayShowScreen)
    {
        $this->repWebPayShowScreen = $repWebPayShowScreen;

        return $this;
    }

    /**
     * Get repWebPayShowScreen
     *
     * @return boolean
     */
    public function getRepWebPayShowScreen()
    {
        return $this->repWebPayShowScreen;
    }

    /**
     * Set repWebPayPrint
     *
     * @param boolean $repWebPayPrint
     *
     * @return Settings
     */
    public function setRepWebPayPrint($repWebPayPrint)
    {
        $this->repWebPayPrint = $repWebPayPrint;

        return $this;
    }

    /**
     * Get repWebPayPrint
     *
     * @return boolean
     */
    public function getRepWebPayPrint()
    {
        return $this->repWebPayPrint;
    }

    /**
     * Set repWebPayDetailed
     *
     * @param boolean $repWebPayDetailed
     *
     * @return Settings
     */
    public function setRepWebPayDetailed($repWebPayDetailed)
    {
        $this->repWebPayDetailed = $repWebPayDetailed;

        return $this;
    }

    /**
     * Get repWebPayDetailed
     *
     * @return boolean
     */
    public function getRepWebPayDetailed()
    {
        return $this->repWebPayDetailed;
    }

    /**
     * Set exchangePlatformexchangePlatform
     *
     * @param \ExchangePlatform $exchangePlatformexchangePlatform
     *
     * @return Settings
     */
    public function setExchangePlatformexchangePlatform(\ExchangePlatform $exchangePlatformexchangePlatform = null)
    {
        $this->exchangePlatformexchangePlatform = $exchangePlatformexchangePlatform;

        return $this;
    }

    /**
     * Get exchangePlatformexchangePlatform
     *
     * @return \ExchangePlatform
     */
    public function getExchangePlatformexchangePlatform()
    {
        return $this->exchangePlatformexchangePlatform;
    }

    /**
     * Set languagelanguage
     *
     * @param \Language $languagelanguage
     *
     * @return Settings
     */
    public function setLanguagelanguage(\Language $languagelanguage = null)
    {
        $this->languagelanguage = $languagelanguage;

        return $this;
    }

    /**
     * Get languagelanguage
     *
     * @return \Language
     */
    public function getLanguagelanguage()
    {
        return $this->languagelanguage;
    }

    /**
     * Set screenscreen
     *
     * @param \Screen $screenscreen
     *
     * @return Settings
     */
    public function setScreenscreen(\Screen $screenscreen = null)
    {
        $this->screenscreen = $screenscreen;

        return $this;
    }

    /**
     * Get screenscreen
     *
     * @return \Screen
     */
    public function getScreenscreen()
    {
        return $this->screenscreen;
    }

    /**
     * Set smsPlatformsmsPlatform
     *
     * @param \SmsPlatform $smsPlatformsmsPlatform
     *
     * @return Settings
     */
    public function setSmsPlatformsmsPlatform(\SmsPlatform $smsPlatformsmsPlatform = null)
    {
        $this->smsPlatformsmsPlatform = $smsPlatformsmsPlatform;

        return $this;
    }

    /**
     * Get smsPlatformsmsPlatform
     *
     * @return \SmsPlatform
     */
    public function getSmsPlatformsmsPlatform()
    {
        return $this->smsPlatformsmsPlatform;
    }
}
