<?php



use Doctrine\Mapping as ORM;

/**
 * Job
 *
 * @Table(name="job", indexes={@Index(name="fk_job_department1_idx", columns={"department_iddepartment"})})
 * @Entity
 */
class Job
{
    /**
     * @var integer
     *
     * @Column(name="idjob", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idjob;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=60, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Department
     *
     * @ManyToOne(targetEntity="Department")
     * @JoinColumns({
     *   @JoinColumn(name="department_iddepartment", referencedColumnName="iddepartment")
     * })
     */
    private $departmentdepartment;

    function getIdjob() {
        return $this->idjob;
    }

    function getName() {
        return $this->name;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getDepartmentdepartment() {
        return $this->departmentdepartment;
    }

    function setIdjob($idjob) {
        $this->idjob = $idjob;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setDepartmentdepartment(\Department $departmentdepartment) {
        $this->departmentdepartment = $departmentdepartment;
    }



}
