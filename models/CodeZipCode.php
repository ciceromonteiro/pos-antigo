<?php

use Doctrine\Mapping as ORM;

/**
 * CodeZipCode
 *
 * @Table(name="code_zip_code", indexes={@Index(name="fk_codeZipCode_person_idx", columns={"person_idperson"}), @Index(name="fk_codeZipCode_company_idx", columns={"company_idCompany"})})
 * @Entity
 */
class CodeZipCode {

    /**
     * @var integer
     *
     * @Column(name="idcode_zip_code", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idCodeZipCode;

    /**
     * @var string
     *
     * @Column(name="zipCode", type="string", length=45, nullable=false)
     */
    private $zipCode;

     /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idCompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;
    
    function getIdCodeZipCode() {
        return $this->idCodeZipCode;
    }

    function getZipCode() {
        return $this->zipCode;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getPersonperson() {
        return $this->personperson;
    }

    function getCompanycompany() {
        return $this->companycompany;
    }

    function setIdCodeZipCode($idCodeZipCode) {
        $this->idCodeZipCode = $idCodeZipCode;
    }

    function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setPersonperson($personperson) {
        $this->personperson = $personperson;
    }

    function setCompanycompany($companycompany) {
        $this->companycompany = $companycompany;
    }

}
