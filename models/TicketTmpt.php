<?php



use Doctrine\Mapping as ORM;

/**
 * TicketTmpt
 *
 * @Table(name="ticket_tmpt")
 * @Entity
 */
class TicketTmpt
{
    /**
     * @var integer
     *
     * @Column(name="idticket_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idticketTmpt;

    /**
     * @var string
     *
     * @Column(name="title", type="string", length=150, nullable=false)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var string
     *
     * @Column(name="info", type="text", nullable=true)
     */
    private $info;

    /**
     * @var boolean
     *
     * @Column(name="print_two_lines", type="boolean", nullable=false)
     */
    private $printTwoLines;

    /**
     * @var boolean
     *
     * @Column(name="print_promo_and_normal_price", type="boolean", nullable=false)
     */
    private $printPromoAndNormalPrice;

    /**
     * @var boolean
     *
     * @Column(name="print_store_address", type="boolean", nullable=false)
     */
    private $printStoreAddress;

    /**
     * @var boolean
     *
     * @Column(name="print_product_tax", type="boolean", nullable=false)
     */
    private $printProductTax;

    /**
     * @var boolean
     *
     * @Column(name="print_product_details", type="boolean", nullable=false)
     */
    private $printProductDetails;

    /**
     * @var boolean
     *
     * @Column(name="print_product_number", type="boolean", nullable=false)
     */
    private $printProductNumber;

    /**
     * @var boolean
     *
     * @Column(name="print_tip", type="boolean", nullable=false)
     */
    private $printTip;



    /**
     * Get idticketTmpt
     *
     * @return integer
     */
    public function getIdticketTmpt()
    {
        return $this->idticketTmpt;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return TicketTmpt
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return TicketTmpt
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return TicketTmpt
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return TicketTmpt
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return TicketTmpt
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return TicketTmpt
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set printTwoLines
     *
     * @param boolean $printTwoLines
     *
     * @return TicketTmpt
     */
    public function setPrintTwoLines($printTwoLines)
    {
        $this->printTwoLines = $printTwoLines;

        return $this;
    }

    /**
     * Get printTwoLines
     *
     * @return boolean
     */
    public function getPrintTwoLines()
    {
        return $this->printTwoLines;
    }

    /**
     * Set printPromoAndNormalPrice
     *
     * @param boolean $printPromoAndNormalPrice
     *
     * @return TicketTmpt
     */
    public function setPrintPromoAndNormalPrice($printPromoAndNormalPrice)
    {
        $this->printPromoAndNormalPrice = $printPromoAndNormalPrice;

        return $this;
    }

    /**
     * Get printPromoAndNormalPrice
     *
     * @return boolean
     */
    public function getPrintPromoAndNormalPrice()
    {
        return $this->printPromoAndNormalPrice;
    }

    /**
     * Set printStoreAddress
     *
     * @param boolean $printStoreAddress
     *
     * @return TicketTmpt
     */
    public function setPrintStoreAddress($printStoreAddress)
    {
        $this->printStoreAddress = $printStoreAddress;

        return $this;
    }

    /**
     * Get printStoreAddress
     *
     * @return boolean
     */
    public function getPrintStoreAddress()
    {
        return $this->printStoreAddress;
    }

    /**
     * Set printProductTax
     *
     * @param boolean $printProductTax
     *
     * @return TicketTmpt
     */
    public function setPrintProductTax($printProductTax)
    {
        $this->printProductTax = $printProductTax;

        return $this;
    }

    /**
     * Get printProductTax
     *
     * @return boolean
     */
    public function getPrintProductTax()
    {
        return $this->printProductTax;
    }

    /**
     * Set printProductDetails
     *
     * @param boolean $printProductDetails
     *
     * @return TicketTmpt
     */
    public function setPrintProductDetails($printProductDetails)
    {
        $this->printProductDetails = $printProductDetails;

        return $this;
    }

    /**
     * Get printProductDetails
     *
     * @return boolean
     */
    public function getPrintProductDetails()
    {
        return $this->printProductDetails;
    }

    /**
     * Set printProductNumber
     *
     * @param boolean $printProductNumber
     *
     * @return TicketTmpt
     */
    public function setPrintProductNumber($printProductNumber)
    {
        $this->printProductNumber = $printProductNumber;

        return $this;
    }

    /**
     * Get printProductNumber
     *
     * @return boolean
     */
    public function getPrintProductNumber()
    {
        return $this->printProductNumber;
    }

    /**
     * Set printTip
     *
     * @param boolean $printTip
     *
     * @return TicketTmpt
     */
    public function setPrintTip($printTip)
    {
        $this->printTip = $printTip;

        return $this;
    }

    /**
     * Get printTip
     *
     * @return boolean
     */
    public function getPrintTip()
    {
        return $this->printTip;
    }
}
