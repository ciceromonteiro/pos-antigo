<?php



use Doctrine\Mapping as ORM;

/**
 * Pos
 *
 * @Table(name="pos", indexes={@Index(name="fk_pos_ticket_tmpt1_idx", columns={"ticket_tmpt_idticket_tmpt"}), @Index(name="fk_pos_temp_ticket_tmpt1_idx", columns={"temp_ticket_tmpt_idtemp_ticket_tmpt"}), @Index(name="idlicense", columns={"idlicense"})})
 * @Entity
 */
class Pos
{
    /**
     * @var integer
     *
     * @Column(name="idpos", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpos;

    /**
     * @var string
     *
     * @Column(name="nickname", type="string", length=300, nullable=false)
     */
    private $nickname;

    /**
     * @var string
     *
     * @Column(name="macAddress", type="string", length=200, nullable=true)
     */
    private $macaddress;

    /**
     * @var string
     *
     * @Column(name="srv", type="text", length=65535, nullable=false)
     */
    private $srv;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active = '1';

    /**
     * @var \License
     *
     * @ManyToOne(targetEntity="License")
     * @JoinColumns({
     *   @JoinColumn(name="idlicense", referencedColumnName="idlicense")
     * })
     */
    private $idlicense;

    /**
     * @var \TempTicketTmpt
     *
     * @ManyToOne(targetEntity="TempTicketTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="temp_ticket_tmpt_idtemp_ticket_tmpt", referencedColumnName="idtemp_ticket_tmpt")
     * })
     */
    private $tempTicketTmpttempTicketTmpt;

    /**
     * @var \TicketTmpt
     *
     * @ManyToOne(targetEntity="TicketTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="ticket_tmpt_idticket_tmpt", referencedColumnName="idticket_tmpt")
     * })
     */
    private $ticketTmptticketTmpt;

    function getIdpos() {
        return $this->idpos;
    }

    function getNickname() {
        return $this->nickname;
    }

    function getMacaddress() {
        return $this->macaddress;
    }

    function getSrv() {
        return $this->srv;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getIdlicense() {
        return $this->idlicense;
    }

    function getTempTicketTmpttempTicketTmpt() {
        return $this->tempTicketTmpttempTicketTmpt;
    }

    function getTicketTmptticketTmpt() {
        return $this->ticketTmptticketTmpt;
    }

    function setIdpos($idpos) {
        $this->idpos = $idpos;
    }

    function setNickname($nickname) {
        $this->nickname = $nickname;
    }

    function setMacaddress($macaddress) {
        $this->macaddress = $macaddress;
    }

    function setSrv($srv) {
        $this->srv = $srv;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdlicense($idlicense) {
        $this->idlicense = $idlicense;
    }

    function setTempTicketTmpttempTicketTmpt($tempTicketTmpttempTicketTmpt) {
        $this->tempTicketTmpttempTicketTmpt = $tempTicketTmpttempTicketTmpt;
    }

    function setTicketTmptticketTmpt($ticketTmptticketTmpt) {
        $this->ticketTmptticketTmpt = $ticketTmptticketTmpt;
    }


}

