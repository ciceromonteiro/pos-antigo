<?php



use Doctrine\Mapping as ORM;

/**
 * Requests
 *
 * @Table(name="requests")
 * @Entity
 */
class Requests
{
    /**
     * @var integer
     *
     * @Column(name="idrequests", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idrequests;

    /**
     * @var string
     *
     * @Column(name="query", type="text", nullable=false)
     */
    private $query;

    /**
     * @var \DateTime
     *
     * @Column(name="date", type="datetime", nullable=false)
     */
    private $date = 'CURRENT_TIMESTAMP';

    /**
     * @var integer
     *
     * @Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @Column(name="direction", type="integer", nullable=false)
     */
    private $direction;

    function getIdrequests() {
        return $this->idrequests;
    }

    function getQuery() {
        return $this->query;
    }

    function getDate() {
        return $this->date;
    }

    function getStatus() {
        return $this->status;
    }

    function getDirection() {
        return $this->direction;
    }

    function setIdrequests($idrequests) {
        $this->idrequests = $idrequests;
    }

    function setQuery($query) {
        $this->query = $query;
    }

    function setDate(\DateTime $date) {
        $this->date = $date;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setDirection($direction) {
        $this->direction = $direction;
    }


}

