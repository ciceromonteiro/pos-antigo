<?php

/**
 * ZzCountry
 *
 * @Table(name="zz_country")
 * @Entity
 */
class ZzCountry {

    /**
     * @var integer
     *
     * @Column(name="idzz_country", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idzzCountry;

    /**
     * @var string
     *
     * @Column(name="code", type="string", length=2, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=60, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var string
     *
     * @Column(name="active", type="string", length=45, nullable=false)
     */
    private $active;

    /**
     * @return int
     */
    public function getIdzzCountry() {
        return $this->idzzCountry;
    }

    /**
     * @param int $idzzCountry
     */
    public function setIdzzCountry($idzzCountry) {
        $this->idzzCountry = $idzzCountry;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return DateTime
     */
    public function getDateCreate() {
        return $this->dateCreate;
    }

    /**
     * @param DateTime $dateCreate
     */
    public function setDateCreate($dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    /**
     * @return DateTime
     */
    public function getDateUpdate() {
        return $this->dateUpdate;
    }

    /**
     * @param DateTime $dateUpdate
     */
    public function setDateUpdate($dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    /**
     * @return DateTime
     */
    public function getDateDelete() {
        return $this->dateDelete;
    }

    /**
     * @param DateTime $dateDelete
     */
    public function setDateDelete($dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    /**
     * @return string
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * @param string $active
     */
    public function setActive($active) {
        $this->active = $active;
    }
    
    function getCode() {
        return $this->code;
    }

    function setCode($code) {
        $this->code = $code;
    }



}
