<?php



use Doctrine\Mapping as ORM;

/**
 * PosTmptInPos
 *
 * @Table(name="pos_tmpt_in_pos", indexes={@Index(name="fk_pos_tmpt_in_pos_pos1_idx", columns={"pos_idpos"}), @Index(name="fk_pos_tmpt_in_pos_pos_tmpt1_idx", columns={"pos_tmpt_idpos_tmpt"})})
 * @Entity
 */
class PosTmptInPos
{
    /**
     * @var integer
     *
     * @Column(name="idpos_tmpt_in_pos", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposTmptInPos;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \PosTmpt
     *
     * @ManyToOne(targetEntity="PosTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="pos_tmpt_idpos_tmpt", referencedColumnName="idpos_tmpt")
     * })
     */
    private $posTmptposTmpt;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active;



    /**
     * Get idposTmptInPos
     *
     * @return integer
     */
    public function getIdposTmptInPos()
    {
        return $this->idposTmptInPos;
    }

    /**
     * Set pospos
     *
     * @param \Pos $pospos
     *
     * @return PosTmptInPos
     */
    public function setPospos(\Pos $pospos = null)
    {
        $this->pospos = $pospos;

        return $this;
    }

    /**
     * Get pospos
     *
     * @return \Pos
     */
    public function getPospos()
    {
        return $this->pospos;
    }

    /**
     * Set posTmptposTmpt
     *
     * @param \PosTmpt $posTmptposTmpt
     *
     * @return PosTmptInPos
     */
    public function setPosTmptposTmpt(\PosTmpt $posTmptposTmpt = null)
    {
        $this->posTmptposTmpt = $posTmptposTmpt;

        return $this;
    }

    /**
     * Get posTmptposTmpt
     *
     * @return \PosTmpt
     */
    public function getPosTmptposTmpt()
    {
        return $this->posTmptposTmpt;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return PosTmpt
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return PosTmpt
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return PosTmpt
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PosTmpt
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
