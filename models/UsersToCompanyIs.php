<?php


/**
 * UsersToCompanyIs
 *
 * @Table(name="users_to_company_is", indexes={@Index(name="fk_users_to_company_is_users1_idx", columns={"users_idusers"}), @Index(name="fk_users_to_company_is_company1_idx", columns={"company_idcompany"}), @Index(name="fk_users_to_company_is_users_type1_idx", columns={"users_type_idusers_type"})})
 * @Entity
 */
class UsersToCompanyIs
{
    /**
     * @var integer
     *
     * @Column(name="idusers_to_company_is", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idusersToCompanyIs;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;

    /**
     * @var \UsersType
     *
     * @ManyToOne(targetEntity="UsersType")
     * @JoinColumns({
     *   @JoinColumn(name="users_type_idusers_type", referencedColumnName="idusers_type")
     * })
     */
    private $usersTypeusersType;



    /**
     * Get idusersToCompanyIs
     *
     * @return integer
     */
    public function getIdusersToCompanyIs()
    {
        return $this->idusersToCompanyIs;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return UsersToCompanyIs
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return UsersToCompanyIs
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return UsersToCompanyIs
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set companycompany
     *
     * @param \Company $companycompany
     *
     * @return UsersToCompanyIs
     */
    public function setCompanycompany(\Company $companycompany = null)
    {
        $this->companycompany = $companycompany;

        return $this;
    }

    /**
     * Get companycompany
     *
     * @return \Company
     */
    public function getCompanycompany()
    {
        return $this->companycompany;
    }

    /**
     * Set usersusers
     *
     * @param \Users $usersusers
     *
     * @return UsersToCompanyIs
     */
    public function setUsersusers(\Users $usersusers = null)
    {
        $this->usersusers = $usersusers;

        return $this;
    }

    /**
     * Get usersusers
     *
     * @return \Users
     */
    public function getUsersusers()
    {
        return $this->usersusers;
    }

    /**
     * Set usersTypeusersType
     *
     * @param \UsersType $usersTypeusersType
     *
     * @return UsersToCompanyIs
     */
    public function setUsersTypeusersType(\UsersType $usersTypeusersType = null)
    {
        $this->usersTypeusersType = $usersTypeusersType;

        return $this;
    }

    /**
     * Get usersTypeusersType
     *
     * @return \UsersType
     */
    public function getUsersTypeusersType()
    {
        return $this->usersTypeusersType;
    }
}
