<?php



use Doctrine\Mapping as ORM;

/**
 * Wardrobe
 *
 * @Table(name="wardrobe", indexes={@Index(name="fk_wardrobe_hall1_idx", columns={"hall_idhall"})})
 * @Entity
 */
class Wardrobe
{
    /**
     * @var integer
     *
     * @Column(name="idwardrobe", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idwardrobe;

    /**
     * @var integer
     *
     * @Column(name="hall_idhall", type="integer", nullable=false)
     */
    private $hallIdhall;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @Column(name="width", type="integer", nullable=false)
     */
    private $width;

    /**
     * @var integer
     *
     * @Column(name="height", type="integer", nullable=false)
     */
    private $height;

    /**
     * @var integer
     *
     * @Column(name="position_top", type="integer", nullable=false)
     */
    private $positionTop;

    /**
     * @var integer
     *
     * @Column(name="position_left", type="integer", nullable=false)
     */
    private $positionLeft;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';


    function getIdwardrobe() {
        return $this->idwardrobe;
    }

    function getHallIdhall() {
        return $this->hallIdhall;
    }

    function getName() {
        return $this->name;
    }

    function getWidth() {
        return $this->width;
    }

    function getHeight() {
        return $this->height;
    }

    function getPositionTop() {
        return $this->positionTop;
    }

    function getPositionLeft() {
        return $this->positionLeft;
    }

    function getDateCreate(){
        return $this->dateCreate;
    }

    function getDateUpdate(){
        return $this->dateUpdate;
    }

    function getDateDelete(){
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdwardrobe($idwardrobe) {
        $this->idwardrobe = $idwardrobe;
    }

    function setHallIdhall($hallIdhall) {
        $this->hallIdhall = $hallIdhall;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setWidth($width) {
        $this->width = $width;
    }

    function setHeight($height) {
        $this->height = $height;
    }

    function setPositionTop($positionTop) {
        $this->positionTop = $positionTop;
    }

    function setPositionLeft($positionLeft) {
        $this->positionLeft = $positionLeft;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

}

