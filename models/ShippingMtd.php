<?php



use Doctrine\Mapping as ORM;

/**
 * ShippingMtd
 *
 * @Table(name="shipping_mtd", indexes={@Index(name="fk_shipping_mtd_shipping_cmp1_idx", columns={"shipping_cmp_idshipping_cmp"})})
 * @Entity
 */
class ShippingMtd
{
    /**
     * @var integer
     *
     * @Column(name="idshipping_mtd", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idshippingMtd;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="shipping_cmp_idshipping_cmp", referencedColumnName="idcompany")
     * })
     */
    private $shippingCmpshippingCmp;

    function getIdshippingMtd() {
        return $this->idshippingMtd;
    }

    function getName() {
        return $this->name;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getShippingCmpshippingCmp() {
        return $this->shippingCmpshippingCmp;
    }

    function setIdshippingMtd($idshippingMtd) {
        $this->idshippingMtd = $idshippingMtd;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setShippingCmpshippingCmp(\Company $shippingCmpshippingCmp) {
        $this->shippingCmpshippingCmp = $shippingCmpshippingCmp;
    }


}

