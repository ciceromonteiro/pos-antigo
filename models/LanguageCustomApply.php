<?php



use Doctrine\Mapping as ORM;

/**
 * LanguageCustomApply
 *
 * @Table(name="language_custom_apply", indexes={@Index(name="idlanguagecustom", columns={"idlanguagecustom"}), @Index(name="idpos", columns={"idpos"})})
 * @Entity
 */
class LanguageCustomApply
{
    /**
     * @var integer
     *
     * @Column(name="idlanguagecustomapply", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlanguagecustomapply;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    /**
     * @var \LanguageCustom
     *
     * @ManyToOne(targetEntity="LanguageCustom")
     * @JoinColumns({
     *   @JoinColumn(name="idlanguagecustom", referencedColumnName="idlanguagecustom")
     * })
     */
    private $idlanguagecustom;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="idpos", referencedColumnName="idpos")
     * })
     */
    private $idpos;

    function getIdlanguagecustomapply() {
        return $this->idlanguagecustomapply;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getIdlanguagecustom() {
        return $this->idlanguagecustom;
    }

    function getIdpos() {
        return $this->idpos;
    }

    function setIdlanguagecustomapply($idlanguagecustomapply) {
        $this->idlanguagecustomapply = $idlanguagecustomapply;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setIdlanguagecustom(\LanguageCustom $idlanguagecustom) {
        $this->idlanguagecustom = $idlanguagecustom;
    }

    function setIdpos(\Pos $idpos) {
        $this->idpos = $idpos;
    }


}

