<?php



use Doctrine\Mapping as ORM;

/**
 * CreditTmpt
 *
 * @Table(name="credit_tmpt", indexes={@Index(name="fk_credit_tmpt_tmpt_header1_idx", columns={"tmpt_header_idtmpt_header"}), @Index(name="fk_credit_tmpt_tmpt_footer1_idx", columns={"tmpt_footer_idtmpt_footer"})})
 * @Entity
 */
class CreditTmpt
{
    /**
     * @var integer
     *
     * @Column(name="idcredit_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcreditTmpt;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="info", type="text", nullable=true)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \TmptFooter
     *
     * @ManyToOne(targetEntity="TmptFooter")
     * @JoinColumns({
     *   @JoinColumn(name="tmpt_footer_idtmpt_footer", referencedColumnName="idtmpt_footer")
     * })
     */
    private $tmptFootertmptFooter;

    /**
     * @var \TmptHeader
     *
     * @ManyToOne(targetEntity="TmptHeader")
     * @JoinColumns({
     *   @JoinColumn(name="tmpt_header_idtmpt_header", referencedColumnName="idtmpt_header")
     * })
     */
    private $tmptHeadertmptHeader;



    /**
     * Get idcreditTmpt
     *
     * @return integer
     */
    public function getIdcreditTmpt()
    {
        return $this->idcreditTmpt;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CreditTmpt
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return CreditTmpt
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return CreditTmpt
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return CreditTmpt
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return CreditTmpt
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return CreditTmpt
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set tmptFootertmptFooter
     *
     * @param \TmptFooter $tmptFootertmptFooter
     *
     * @return CreditTmpt
     */
    public function setTmptFootertmptFooter(\TmptFooter $tmptFootertmptFooter = null)
    {
        $this->tmptFootertmptFooter = $tmptFootertmptFooter;

        return $this;
    }

    /**
     * Get tmptFootertmptFooter
     *
     * @return \TmptFooter
     */
    public function getTmptFootertmptFooter()
    {
        return $this->tmptFootertmptFooter;
    }

    /**
     * Set tmptHeadertmptHeader
     *
     * @param \TmptHeader $tmptHeadertmptHeader
     *
     * @return CreditTmpt
     */
    public function setTmptHeadertmptHeader(\TmptHeader $tmptHeadertmptHeader = null)
    {
        $this->tmptHeadertmptHeader = $tmptHeadertmptHeader;

        return $this;
    }

    /**
     * Get tmptHeadertmptHeader
     *
     * @return \TmptHeader
     */
    public function getTmptHeadertmptHeader()
    {
        return $this->tmptHeadertmptHeader;
    }
}
