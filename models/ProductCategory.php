<?php



use Doctrine\Mapping as ORM;

/**
 * ProductCategory
 *
 * @Table(name="product_category", indexes={@Index(name="fk_product_category_product1_idx", columns={"product_idproduct"}), @Index(name="fk_product_category_category1_idx", columns={"category_idcategory"})})
 * @Entity
 */
class ProductCategory
{
    /**
     * @var integer
     *
     * @Column(name="idproduct_category", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductCategory;

    /**
     * @var \Category
     *
     * @ManyToOne(targetEntity="Category")
     * @JoinColumns({
     *   @JoinColumn(name="category_idcategory", referencedColumnName="idcategory")
     * })
     */
    private $categorycategory;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;



    /**
     * Get idproductCategory
     *
     * @return integer
     */
    public function getIdproductCategory()
    {
        return $this->idproductCategory;
    }

    /**
     * Set categorycategory
     *
     * @param \Category $categorycategory
     *
     * @return ProductCategory
     */
    public function setCategorycategory(\Category $categorycategory = null)
    {
        $this->categorycategory = $categorycategory;

        return $this;
    }

    /**
     * Get categorycategory
     *
     * @return \Category
     */
    public function getCategorycategory()
    {
        return $this->categorycategory;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return ProductCategory
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }
}
