<?php



use Doctrine\Mapping as ORM;

/**
 * PosCompanyData
 *
 * @Table(name="pos_company_data", indexes={@Index(name="fk_pos_company_data_pos_company1_idx", columns={"pos_company_idpos_company"})})
 * @Entity
 */
class PosCompanyData
{
    /**
     * @var integer
     *
     * @Column(name="idpos_company_data", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposCompanyData;

    /**
     * @var string
     *
     * @Column(name="data_type", type="string", length=20, nullable=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @Column(name="data_value", type="string", length=45, nullable=false)
     */
    private $dataValue;

    /**
     * @var string
     *
     * @Column(name="reference_name", type="string", length=45, nullable=true)
     */
    private $referenceName;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \PosCompany
     *
     * @ManyToOne(targetEntity="PosCompany")
     * @JoinColumns({
     *   @JoinColumn(name="pos_company_idpos_company", referencedColumnName="idpos_company")
     * })
     */
    private $posCompanyposCompany;



    /**
     * Get idposCompanyData
     *
     * @return integer
     */
    public function getIdposCompanyData()
    {
        return $this->idposCompanyData;
    }

    /**
     * Set dataType
     *
     * @param string $dataType
     *
     * @return PosCompanyData
     */
    public function setDataType($dataType)
    {
        $this->dataType = $dataType;

        return $this;
    }

    /**
     * Get dataType
     *
     * @return string
     */
    public function getDataType()
    {
        return $this->dataType;
    }

    /**
     * Set dataValue
     *
     * @param string $dataValue
     *
     * @return PosCompanyData
     */
    public function setDataValue($dataValue)
    {
        $this->dataValue = $dataValue;

        return $this;
    }

    /**
     * Get dataValue
     *
     * @return string
     */
    public function getDataValue()
    {
        return $this->dataValue;
    }

    /**
     * Set referenceName
     *
     * @param string $referenceName
     *
     * @return PosCompanyData
     */
    public function setReferenceName($referenceName)
    {
        $this->referenceName = $referenceName;

        return $this;
    }

    /**
     * Get referenceName
     *
     * @return string
     */
    public function getReferenceName()
    {
        return $this->referenceName;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return PosCompanyData
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return PosCompanyData
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return PosCompanyData
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PosCompanyData
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set posCompanyposCompany
     *
     * @param \PosCompany $posCompanyposCompany
     *
     * @return PosCompanyData
     */
    public function setPosCompanyposCompany(\PosCompany $posCompanyposCompany = null)
    {
        $this->posCompanyposCompany = $posCompanyposCompany;

        return $this;
    }

    /**
     * Get posCompanyposCompany
     *
     * @return \PosCompany
     */
    public function getPosCompanyposCompany()
    {
        return $this->posCompanyposCompany;
    }
}
