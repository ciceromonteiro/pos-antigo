<?php



use Doctrine\Mapping as ORM;

/**
 * Person
 *
 * @Table(name="person", uniqueConstraints={@UniqueConstraint(name="document_number_UNIQUE", columns={"document_number"}), @UniqueConstraint(name="electronic_identification_UNIQUE", columns={"electronic_identification"})}, indexes={@Index(name="fk_person_customer_gp1_idx", columns={"customer_gp_idcustomer_gp"}), @Index(name="fk_person_zz_state1_idx", columns={"zz_state_idzz_state"}), @Index(name="fk_person_pos1_idx", columns={"pos_idpos"}), @Index(name="fk_person_payment_mtd1_idx", columns={"payment_mtd_idpayment_mtd"}), @Index(name="fk_person_person_settings1_idx", columns={"person_settings_idperson_settings"}), @Index(name="fk_person_department1_idx", columns={"department_iddepartment"}), @Index(name="fk_person_users1_idx", columns={"users_idusers"})})
 * @Entity
 */
class Person
{
    /**
     * @var integer
     *
     * @Column(name="idperson", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idperson;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=200, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="document_type", type="string", length=20, nullable=true)
     */
    private $documentType;

    /**
     * @var string
     *
     * @Column(name="document_number", type="string", length=45, nullable=true)
     */
    private $documentNumber;

    /**
     * @var string
     *
     * @Column(name="birth_date", type="date", nullable=true)
     */
    private $birthDate;

    /**
     * @var string
     *
     * @Column(name="personal_number", type="string", length=45, nullable=true)
     */
    private $personalNumber;

    /**
     * @var string
     *
     * @Column(name="contact_person", type="string", length=45, nullable=true)
     */
    private $contactPerson;

    /**
     * @var string
     *
     * @Column(name="info", type="text", nullable=true)
     */
    private $info;

    /**
     * @var integer
     *
     * @Column(name="genre", type="integer", nullable=false)
     */
    private $genre;

    /**
     * @var string
     *
     * @Column(name="electronic_identification", type="string", length=45, nullable=true)
     */
    private $electronicIdentification;

    /**
     * @var \DateTime
     *
     * @Column(name="date_creation", type="datetime", nullable=false)
     */
    private $dateCreation = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \CustomerGp
     *
     * @ManyToOne(targetEntity="CustomerGp")
     * @JoinColumns({
     *   @JoinColumn(name="customer_gp_idcustomer_gp", referencedColumnName="idcustomer_gp")
     * })
     */
    private $customerGpcustomerGp;

    /**
     * @var \Department
     *
     * @ManyToOne(targetEntity="Department")
     * @JoinColumns({
     *   @JoinColumn(name="department_iddepartment", referencedColumnName="iddepartment")
     * })
     */
    private $departmentdepartment;

    /**
     * @var \PaymentMtd
     *
     * @ManyToOne(targetEntity="PaymentMtd")
     * @JoinColumns({
     *   @JoinColumn(name="payment_mtd_idpayment_mtd", referencedColumnName="idpayment_mtd")
     * })
     */
    private $paymentMtdpaymentMtd;

    /**
     * @var \PersonSettings
     *
     * @ManyToOne(targetEntity="PersonSettings")
     * @JoinColumns({
     *   @JoinColumn(name="person_settings_idperson_settings", referencedColumnName="idperson_settings")
     * })
     */
    private $personSettingspersonSettings;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;

    /**
     * @var \ZzState
     *
     * @ManyToOne(targetEntity="ZzState")
     * @JoinColumns({
     *   @JoinColumn(name="zz_state_idzz_state", referencedColumnName="idzz_state")
     * })
     */
    private $zzStatezzState;

    function getIdperson() {
        return $this->idperson;
    }

    function getName() {
        return $this->name;
    }

    function getDocumentType() {
        return $this->documentType;
    }

    function getDocumentNumber() {
        return $this->documentNumber;
    }

    function getBirthDate() {
        return $this->birthDate;
    }

    function getPersonalNumber() {
        return $this->personalNumber;
    }

    function getContactPerson() {
        return $this->contactPerson;
    }

    function getInfo() {
        return $this->info;
    }

    function getGenre() {
        return $this->genre;
    }

    function getElectronicIdentification() {
        return $this->electronicIdentification;
    }

    function getDateCreation() {
        return $this->dateCreation;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getCustomerGpcustomerGp() {
        return $this->customerGpcustomerGp;
    }

    function getDepartmentdepartment() {
        return $this->departmentdepartment;
    }

    function getPaymentMtdpaymentMtd() {
        return $this->paymentMtdpaymentMtd;
    }

    function getPersonSettingspersonSettings() {
        return $this->personSettingspersonSettings;
    }

    function getPospos() {
        return $this->pospos;
    }

    function getUsersusers() {
        return $this->usersusers;
    }

    function getZzStatezzState() {
        return $this->zzStatezzState;
    }

    function setIdperson($idperson) {
        $this->idperson = $idperson;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDocumentType($documentType) {
        $this->documentType = $documentType;
    }

    function setDocumentNumber($documentNumber) {
        $this->documentNumber = $documentNumber;
    }

    function setBirthDate($birthDate) {
        $this->birthDate = $birthDate;
    }

    function setPersonalNumber($personalNumber) {
        $this->personalNumber = $personalNumber;
    }

    function setContactPerson($contactPerson) {
        $this->contactPerson = $contactPerson;
    }

    function setInfo($info) {
        $this->info = $info;
    }

    function setGenre($genre) {
        $this->genre = $genre;
    }

    function setElectronicIdentification($electronicIdentification) {
        $this->electronicIdentification = $electronicIdentification;
    }

    function setDateCreation(\DateTime $dateCreation) {
        $this->dateCreation = $dateCreation;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setCustomerGpcustomerGp($customerGpcustomerGp) {
        $this->customerGpcustomerGp = $customerGpcustomerGp;
    }

    function setDepartmentdepartment($departmentdepartment) {
        $this->departmentdepartment = $departmentdepartment;
    }

    function setPaymentMtdpaymentMtd($paymentMtdpaymentMtd) {
        $this->paymentMtdpaymentMtd = $paymentMtdpaymentMtd;
    }

    function setPersonSettingspersonSettings($personSettingspersonSettings) {
        $this->personSettingspersonSettings = $personSettingspersonSettings;
    }

    function setPospos($pospos) {
        $this->pospos = $pospos;
    }

    function setUsersusers($usersusers) {
        $this->usersusers = $usersusers;
    }

    function setZzStatezzState($zzStatezzState) {
        $this->zzStatezzState = $zzStatezzState;
    }


}

