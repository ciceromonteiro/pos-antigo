<?php



use Doctrine\Mapping as ORM;

/**
 * SettingsAccounting
 *
 * @Table(name="settings_accounting")
 * @Entity
 */
class SettingsAccounting
{
    /**
     * @var integer
     *
     * @Column(name="idsettingsaccounting", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsettingsaccounting;

    /**
     * @var string
     *
     * @Column(name="data_type", type="string", length=300, nullable=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @Column(name="data_value", type="string", length=300, nullable=true)
     */
    private $dataValue;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    function getIdsettingsaccounting() {
        return $this->idsettingsaccounting;
    }

    function getDataType() {
        return $this->dataType;
    }

    function getDataValue() {
        return $this->dataValue;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdsettingsaccounting($idsettingsaccounting) {
        $this->idsettingsaccounting = $idsettingsaccounting;
    }

    function setDataType($dataType) {
        $this->dataType = $dataType;
    }

    function setDataValue($dataValue) {
        $this->dataValue = $dataValue;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

}

