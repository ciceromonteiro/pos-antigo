<?php



use Doctrine\Mapping as ORM;

/**
 * Tribute
 *
 * @Table(name="tribute", indexes={@Index(name="vat_alternative", columns={"vat_alternative"}), @Index(name="tax", columns={"tax"})})
 * @Entity
 */
class Tribute
{
    /**
     * @var integer
     *
     * @Column(name="idtribute", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idtribute;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @Column(name="pay_vat", type="boolean", nullable=true)
     */
    private $payVat;

    /**
     * @var integer
     *
     * @Column(name="treasury", type="integer", nullable=false)
     */
    private $treasury;

    /**
     * @var integer
     *
     * @Column(name="treasury_without_vat", type="integer", nullable=false)
     */
    private $treasuryWithoutVat;

    /**
     * @var integer
     *
     * @Column(name="cod_additional", type="integer", nullable=false)
     */
    private $codAdditional;

    /**
     * @var integer
     *
     * @Column(name="treasury_alternative", type="integer", nullable=false)
     */
    private $treasuryAlternative;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Tax
     *
     * @ManyToOne(targetEntity="Tax")
     * @JoinColumns({
     *   @JoinColumn(name="tax", referencedColumnName="idtax")
     * })
     */
    private $tax;

    /**
     * @var \Tax
     *
     * @ManyToOne(targetEntity="Tax")
     * @JoinColumns({
     *   @JoinColumn(name="vat_alternative", referencedColumnName="idtax")
     * })
     */
    private $vatAlternative;

    function getIdtribute() {
        return $this->idtribute;
    }

    function getName() {
        return $this->name;
    }

    function getPayVat() {
        return $this->payVat;
    }

    function getTreasury() {
        return $this->treasury;
    }

    function getTreasuryWithoutVat() {
        return $this->treasuryWithoutVat;
    }

    function getCodAdditional() {
        return $this->codAdditional;
    }

    function getTreasuryAlternative() {
        return $this->treasuryAlternative;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getTax() {
        return $this->tax;
    }

    function getVatAlternative() {
        return $this->vatAlternative;
    }

    function setIdtribute($idtribute) {
        $this->idtribute = $idtribute;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setPayVat($payVat) {
        $this->payVat = $payVat;
    }

    function setTreasury($treasury) {
        $this->treasury = $treasury;
    }

    function setTreasuryWithoutVat($treasuryWithoutVat) {
        $this->treasuryWithoutVat = $treasuryWithoutVat;
    }

    function setCodAdditional($codAdditional) {
        $this->codAdditional = $codAdditional;
    }

    function setTreasuryAlternative($treasuryAlternative) {
        $this->treasuryAlternative = $treasuryAlternative;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setTax($tax) {
        $this->tax = $tax;
    }

    function setVatAlternative($vatAlternative) {
        $this->vatAlternative = $vatAlternative;
    }


}

