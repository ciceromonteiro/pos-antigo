<?php

use Doctrine\Mapping as ORM;

/**
 * Company
 *
 * @Table(name="company", indexes={@Index(name="fk_company_pos1_idx", columns={"pos_idpos"}), @Index(name="fk_company_payment_mtd1_idx", columns={"payment_mtd_idpayment_mtd"})})
 * @Entity
 */
class Company {

    /**
     * @var integer
     *
     * @Column(name="idcompany", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcompany;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="fantasy_name", type="string", length=100, nullable=false)
     */
    private $fantasyName;

    /**
     * @var string
     *
     * @Column(name="register_number", type="string", length=45, nullable=false)
     */
    private $registerNumber;

    /**
     * @var string
     *
     * @Column(name="info", type="text", nullable=true)
     */
    private $info;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \PaymentMtd
     *
     * @ManyToOne(targetEntity="PaymentMtd")
     * @JoinColumns({
     *   @JoinColumn(name="payment_mtd_idpayment_mtd", referencedColumnName="idpayment_mtd")
     * })
     */
    private $paymentMtdIdpaymentMtd;

    /**
     * @return int
     */
    public function getIdcompany() {
        return $this->idcompany;
    }

    /**
     * @param int $idcompany
     */
    public function setIdcompany($idcompany) {
        $this->idcompany = $idcompany;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFantasyName() {
        return $this->fantasyName;
    }

    /**
     * @param string $fantasyName
     */
    public function setFantasyName($fantasyName) {
        $this->fantasyName = $fantasyName;
    }

    /**
     * @return string
     */
    public function getRegisterNumber() {
        return $this->registerNumber;
    }

    /**
     * @param string $registerNumber
     */
    public function setRegisterNumber($registerNumber) {
        $this->registerNumber = $registerNumber;
    }

    /**
     * @return string
     */
    public function getInfo() {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo($info) {
        $this->info = $info;
    }

    /**
     * @return DateTime
     */
    public function getDateCreate() {
        return $this->dateCreate;
    }

    /**
     * @param DateTime $dateCreate
     */
    public function setDateCreate($dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    /**
     * @return DateTime
     */
    public function getDateUpdate() {
        return $this->dateUpdate;
    }

    /**
     * @param DateTime $dateUpdate
     */
    public function setDateUpdate($dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    /**
     * @return DateTime
     */
    public function getDateDelete() {
        return $this->dateDelete;
    }

    /**
     * @param DateTime $dateDelete
     */
    public function setDateDelete($dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    /**
     * @return boolean
     */
    public function isActive() {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active) {
        $this->active = $active;
    }

    /**
     * @return Pos
     */
    public function getPospos() {
        return $this->pospos;
    }

    /**
     * @param Pos $pospos
     */
    public function setPospos($pospos) {
        $this->pospos = $pospos;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }
    
    /**
     * @param PaymentMtd $paymentMtdIdpaymentMtd
     */
    public function getPaymentMtdIdpaymentMtd() {
        return $this->paymentMtdIdpaymentMtd;
    }
    
    /**
     * @param PaymentMtd $paymentMtdIdpaymentMtd
     */
    public function setPaymentMtdIdpaymentMtd($paymentMtdIdpaymentMtd) {
        $this->paymentMtdIdpaymentMtd = $paymentMtdIdpaymentMtd;
    }



    /**
     * @return string
     */
    public function getPhone() {
        $phones = getEm()->getRepository('CompanyData')->findBy(array("companycompany"=>$this, "dataType"=>"phone"));
        return $phones;
    }

    /**
     * @return string
     */
    public function getAddress() {
        $addresses = getEm()->getRepository('Address')->findBy(array("companycompany"=>$this));
        return $addresses;
    }

}
