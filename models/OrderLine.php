<?php



use Doctrine\Mapping as ORM;

/**
 * OrderLine
 *
 * @Table(name="order_line", indexes={@Index(name="fk_order_line_order1_idx", columns={"order_idorder"}), @Index(name="fk_order_line_product1_idx", columns={"product_idproduct"}), @Index(name="fk_order_line_warranty1_idx", columns={"warranty_idwarranty"})})
 * @Entity
 */
class OrderLine
{
    /**
     * @var integer
     *
     * @Column(name="idorder_line", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idorderLine;

    /**
     * @var string
     *
     * @Column(name="value_warranty", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $valueWarranty;

    /**
     * @var \DateTime
     *
     * @Column(name="date_warranty", type="datetime", nullable=true)
     */
    private $dateWarranty;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var string
     *
     * @Column(name="value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @Column(name="actual_value", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $actualValue;

    /**
     * @var string
     *
     * @Column(name="tax", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $tax;

    /**
   * @var float
   *
   * @Column(name="qty", type="float", precision=10, scale=0, nullable=false)
   */
   private $qty = 1;

    /**
     * @var string
     *
     * @Column(name="discount", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $discount;

    /**
     * @var string
     *
     * @Column(name="discount_reason", type="text", nullable=true)
     */
    private $discountReason;

    /**
     * @var string
     *
     * @Column(name="returned", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $returned;

    /**
     * @var string
     *
     * @Column(name="returned_reason", type="text", nullable=true)
     */
    private $returnedReason;

    /**
     * @var \DateTime
     *
     * @Column(name="date_return", type="datetime", nullable=true)
     */
    private $dateReturn;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active;

    /**
     * @var \Order
     *
     * @ManyToOne(targetEntity="Order")
     * @JoinColumns({
     *   @JoinColumn(name="order_idorder", referencedColumnName="idorder")
     * })
     */
    private $orderorder;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \SizeColor
     *
     * @ManyToOne(targetEntity="SizeColor")
     * @JoinColumns({
     *   @JoinColumn(name="size_color_idsize_color", referencedColumnName="idsize_color")
     * })
     */
    private $sizecolorsizecolor;
    
    /**
     * @var integer
     *
     * @Column(name="user_iduser", type="integer", nullable=false)
     */
    private $useriduser;

    /**
     * @var \Warranty
     *
     * @ManyToOne(targetEntity="Warranty")
     * @JoinColumns({
     *   @JoinColumn(name="warranty_idwarranty", referencedColumnName="idwarranty")
     * })
     */
    private $warrantywarranty;


    function setOrder($order){
        $this->orderorder = $order;
    }
    function setProduct($product){
        $this->productproduct = $product;
    }
    function setActive($active){
        $this->active = $active;
    }



    /**
     * Get idorderLine
     *
     * @return integer
     */
    public function getIdorderLine()
    {
        return $this->idorderLine;
    }

    /**
     * Set valueWarranty
     *
     * @param string $valueWarranty
     *
     * @return OrderLine
     */
    public function setValueWarranty($valueWarranty)
    {
        $this->valueWarranty = $valueWarranty;

        return $this;
    }

    /**
     * Get valueWarranty
     *
     * @return string
     */
    public function getValueWarranty()
    {
        return $this->valueWarranty;
    }

    /**
     * Set dateWarranty
     *
     * @param \DateTime $dateWarranty
     *
     * @return OrderLine
     */
    public function setDateWarranty($dateWarranty)
    {
        $this->dateWarranty = $dateWarranty;

        return $this;
    }

    /**
     * Get dateWarranty
     *
     * @return \DateTime
     */
    public function getDateWarranty()
    {
        return $this->dateWarranty;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return OrderLine
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return OrderLine
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Set actualValue
     *
     * @param string $actualValue
     *
     * @return OrderLine
     */
    public function setActualValue($actualValue)
    {
        $this->actualValue = $actualValue;

        return $this;
    }

    /**
     * Set tax
     *
     * @param string $tax
     *
     * @return OrderLine
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Set qty
     *
     * @param string $qty
     *
     * @return OrderLine
     */
    public function setQty($qty)
    {
        $this->qty = $qty;

        return $this;
    }

    /**
     * Get qty
     *
     *
     * @return OrderLine
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get actualValue
     *
     * @return string
     */
    public function getActualValue()
    {
        return $this->actualValue;
    }

    /**
     * Get tax
     *
     * @return string
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set discount
     *
     * @param string $discount
     *
     * @return OrderLine
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set discountReason
     *
     * @param string $discountReason
     *
     * @return OrderLine
     */
    public function setDiscountReason($discountReason)
    {
        $this->discountReason = $discountReason;

        return $this;
    }

    /**
     * Get discountReason
     *
     * @return string
     */
    public function getDiscountReason()
    {
        return $this->discountReason;
    }

    /**
     * Set returned
     *
     * @param string $returned
     *
     * @return OrderLine
     */
    public function setReturned($returned)
    {
        $this->returned = $returned;

        return $this;
    }

    /**
     * Get returned
     *
     * @return string
     */
    public function getReturned()
    {
        return $this->returned;
    }

    /**
     * Set returnedReason
     *
     * @param string $returnedReason
     *
     * @return OrderLine
     */
    public function setReturnedReason($returnedReason)
    {
        $this->returnedReason = $returnedReason;

        return $this;
    }

    /**
     * Get returnedReason
     *
     * @return string
     */
    public function getReturnedReason()
    {
        return $this->returnedReason;
    }

    /**
     * Set dateReturn
     *
     * @param \DateTime $dateReturn
     *
     * @return OrderLine
     */
    public function setDateReturn($dateReturn)
    {
        $this->dateReturn = $dateReturn;

        return $this;
    }

    /**
     * Get dateReturn
     *
     * @return \DateTime
     */
    public function getDateReturn()
    {
        return $this->dateReturn;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return OrderLine
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return OrderLine
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return OrderLine
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set orderorder
     *
     * @param \Order $orderorder
     *
     * @return OrderLine
     */
    public function setOrderorder(\Order $orderorder = null)
    {
        $this->orderorder = $orderorder;

        return $this;
    }

    /**
     * Get orderorder
     *
     * @return \Order
     */
    public function getOrderorder()
    {
        return $this->orderorder;
    }

    /**
     * Set productproduct
     *
     * @param \Product $productproduct
     *
     * @return OrderLine
     */
    public function setProductproduct(\Product $productproduct = null)
    {
        $this->productproduct = $productproduct;

        return $this;
    }

    /**
     * Get productproduct
     *
     * @return \Product
     */
    public function getProductproduct()
    {
        return $this->productproduct;
    }


    /**
     * Set sizecolorsizecolor
     *
     * @param \SizeColor $sizecolorsizecolor
     *
     * @return OrderLine
     */
    public function setSizecolorsizecolor(\SizeColor $sizecolorsizecolor = null)
    {
        $this->sizecolorsizecolor = $sizecolorsizecolor;

        return $this;
    }
    
    /**
     * Get sizecolorsizecolor
     *
     * @return \SizeColor
     */
    public function getSizecolorsizecolor()
    {
        return $this->sizecolorsizecolor;
    }
    
    
    /**
     * Set useriduser
     *
     * @param $useriduser
     *
     * @return user_iduser
     */
    public function setUserIduser($useriduser)
    {
        $this->useriduser = $useriduser;

        return $this;
    }
    

  
    
    
    /**
     * Get useriduser
     *
     * @return $useriduser
     */
    public function getUserIduser()
    {
        return $this->useriduser;
    }

    /**
     * Set warrantywarranty
     *
     * @param \Warranty $warrantywarranty
     *
     * @return OrderLine
     */
    public function setWarrantywarranty(\Warranty $warrantywarranty = null)
    {
        $this->warrantywarranty = $warrantywarranty;

        return $this;
    }

    /**
     * Get warrantywarranty
     *
     * @return \Warranty
     */
    public function getWarrantywarranty()
    {
        return $this->warrantywarranty;
    }

    public function getAmount() {
        $amount = 0;
        $value = $this->value;
        $qty = $this->qty;
        $discount = $this->discount;

        $amount = ($value * $qty) * (1 - ($discount/100));

        return $amount;
    }

    public function getAmountIncTax() {
        $amount = 0;
        $amount_excl_tax = $this->getAmount();


        return $amount;
    }
}
