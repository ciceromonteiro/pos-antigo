<?php

use Doctrine\Mapping as ORM;

/**
 * StockChanges
 *
 * @Table(name="stock_changes", indexes={@Index(name="fk_product_id_stock_changes", columns={"product_id"}),@Index(name="fk_user_id_stock_changes", columns={"user_id"}),@Index(name="fk_size_color_id_stock_changes", columns={"size_color_id"})})
 * @Entity
 */
class StockChanges {

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idstockchanges;

    /**
     * @var integer
     *
     * @Column(name="type", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $type;

    /**
     * @var \SizeColor
     *
     * @ManyToOne(targetEntity="SizeColor")
     * @JoinColumns({
     *   @JoinColumn(name="size_color_id", referencedColumnName="idsize_color")
     * })
     */
    private $sizecolorid;
    
    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_id", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;
    
     /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="user_id", referencedColumnName="idusers")
     * })
     */
    private $usersusers;

    /**
     * @var integer
     *
     * @Column(name="old_value", type="integer", nullable=true)
     */
    private $oldvalue;
    
    /**
     * @var integer
     *
     * @Column(name="new_value", type="integer", nullable=true)
     */
    private $newvalue;

    /**
     * @var string
     *
     * @Column(name="comment", type="string", length=255, nullable=false)
     */
    private $comment;

     /**
     * @var \DateTime
     *
     * @Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdat = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updateat;

    /**
     * @var \DateTime
     *
     * @Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deleteat;
    
    function getType() {
        return $this->type;
    }
    function getIdstockchanges() {
        return $this->idstockchanges;
    }
    
    function getSizecolorid() {
        return $this->sizecolorid;
    }

    function setSizecolorid(\SizeColor $sizecolorid) {
        $this->sizecolorid = $sizecolorid;
    }

    
    function getProductproduct() {
        return $this->productproduct;
    }

    function getUsersusers() {
        return $this->usersusers;
    }

    function getOldvalue() {
        return $this->oldvalue;
    }

    function getNewvalue() {
        return $this->newvalue;
    }

    function getComment() {
        return $this->comment;
    }

    function getCreatedat() {
        return $this->createdat;
    }

    function getUpdateat() {
        return $this->updateat;
    }

    function getDeleteat() {
        return $this->deleteat;
    }

    function setType($type) {
        $this->type = $type;
    }

    function setIdstockchanges($idstockchanges) {
        $this->idstockchanges = $idstockchanges;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }

    function setUsersusers(\Users $usersusers) {
        $this->usersusers = $usersusers;
    }

    function setOldvalue($oldvalue) {
        $this->oldvalue = $oldvalue;
    }

    function setNewvalue($newvalue) {
        $this->newvalue = $newvalue;
    }

    function setComment($comment) {
        $this->comment = $comment;
    }

    function setCreatedat(\DateTime $createdat) {
        $this->createdat = $createdat;
    }

    function setUpdateat(\DateTime $updateat) {
        $this->updateat = $updateat;
    }

    function setDeleteat(\DateTime $deleteat) {
        $this->deleteat = $deleteat;
    }

}
