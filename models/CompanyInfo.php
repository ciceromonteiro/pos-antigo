<?php



use Doctrine\ORM\Mapping as ORM;

/**
 * CompanyInfo
 *
 * @Table(name="company_info", indexes={@Index(name="pos_company_idpos_company", columns={"pos_company_idpos_company"})})
 * @Entity
 */
class CompanyInfo
{
    /**
     * @var integer
     *
     * @Column(name="id_company_info", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idCompanyInfo;

    /**
     * @var string
     *
     * @Column(name="data_type", type="string", length=150, nullable=false)
     */
    private $dataType;

    /**
     * @var string
     *
     * @Column(name="data_value", type="string", length=300, nullable=false)
     */
    private $dataValue;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @var \PosCompany
     *
     * @ManyToOne(targetEntity="PosCompany")
     * @JoinColumns({
     *   @JoinColumn(name="pos_company_idpos_company", referencedColumnName="idpos_company")
     * })
     */
    private $posCompanyposCompany;

    function getIdCompanyInfo() {
        return $this->idCompanyInfo;
    }

    function getDataType() {
        return $this->dataType;
    }

    function getDataValue() {
        return $this->dataValue;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getPosCompanyposCompany() {
        return $this->posCompanyposCompany;
    }

    function setIdCompanyInfo($idCompanyInfo) {
        $this->idCompanyInfo = $idCompanyInfo;
    }

    function setDataType($dataType) {
        $this->dataType = $dataType;
    }

    function setDataValue($dataValue) {
        $this->dataValue = $dataValue;
    }

    function setDateCreate($dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate($dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete($dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setPosCompanyposCompany($posCompanyposCompany) {
        $this->posCompanyposCompany = $posCompanyposCompany;
    }   
}
