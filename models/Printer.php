<?php



use Doctrine\Mapping as ORM;

/**
 * Printer
 *
 * @Table(name="printer", indexes={@Index(name="id_type_printer", columns={"id_type_printer"}), @Index(name="installed_in", columns={"installed_in"})})
 * @Entity
 */
class Printer
{
    /**
     * @var integer
     *
     * @Column(name="idprinter", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idprinter;

    /**
     * @var string
     *
     * @Column(name="port", type="string", length=30, nullable=false)
     */
    private $port;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=150, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="installed_in", referencedColumnName="idpos")
     * })
     */
    private $installedIn;

    /**
     * @var \PrinterType
     *
     * @ManyToOne(targetEntity="PrinterType")
     * @JoinColumns({
     *   @JoinColumn(name="id_type_printer", referencedColumnName="idprintertype")
     * })
     */
    private $idTypePrinter;

    function getIdprinter() {
        return $this->idprinter;
    }

    function getPort() {
        return $this->port;
    }

    function getDescription() {
        return $this->description;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getInstalledIn() {
        return $this->installedIn;
    }

    function getIdTypePrinter() {
        return $this->idTypePrinter;
    }

    function setIdprinter($idprinter) {
        $this->idprinter = $idprinter;
    }

    function setPort($port) {
        $this->port = $port;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setInstalledIn(\Pos $installedIn) {
        $this->installedIn = $installedIn;
    }

    function setIdTypePrinter(\PrinterType $idTypePrinter) {
        $this->idTypePrinter = $idTypePrinter;
    }


}

