<?php

use Doctrine\Mapping as ORM;

/**
 * ProductSerialNumber
 *
 * @Table(name="product_serial_number", indexes={@Index(name="fk_product_serial_number_product1_idx", columns={"product_idproduct"}), @Index(name="fk_product_serial_number_order_line1_idx", columns={"order_line_idorder_line"}), @Index(name="fk_product_serial_number_section1_idx", columns={"section_idsection"})})
 * @Entity
 */
class ProductSerialNumber
{
    /**
     * @var integer
     *
     * @Column(name="idproduct_serial_number", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproductSerialNumber;

    /**
     * @var string
     *
     * @Column(name="serial", type="string", length=200, nullable=false)
     */
    private $serial;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \OrderLine
     *
     * @ManyToOne(targetEntity="OrderLine")
     * @JoinColumns({
     *   @JoinColumn(name="order_line_idorder_line", referencedColumnName="idorder_line")
     * })
     */
    private $orderLineorderLine;

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    /**
     * @var \Section
     *
     * @ManyToOne(targetEntity="Section")
     * @JoinColumns({
     *   @JoinColumn(name="section_idsection", referencedColumnName="idsection")
     * })
     */
    private $sectionsection;

    function getIdproductSerialNumber() {
        return $this->idproductSerialNumber;
    }

    function getSerial() {
        return $this->serial;
    }

    function getDateCreate(){
        return $this->dateCreate;
    }

    function getDateUpdate(){
        return $this->dateUpdate;
    }

    function getDateDelete(){
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getOrderLineorderLine() {
        return $this->orderLineorderLine;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function getSectionsection(){
        return $this->sectionsection;
    }

    function setIdproductSerialNumber($idproductSerialNumber) {
        $this->idproductSerialNumber = $idproductSerialNumber;
    }

    function setSerial($serial) {
        $this->serial = $serial;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setOrderLineorderLine(\OrderLine $orderLineorderLine) {
        $this->orderLineorderLine = $orderLineorderLine;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }

    function setSectionsection(\Section $sectionsection) {
        $this->sectionsection = $sectionsection;
    }

    
}
