<?php



use Doctrine\Mapping as ORM;

/**
 * CreditCard
 *
 * @Table(name="credit_card")
 * @Entity
 */
class CreditCard
{
    /**
     * @var integer
     *
     * @Column(name="idcreditcard", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idcreditcard;

    /**
     * @var integer
     *
     * @Column(name="bbsid", type="integer", nullable=false)
     */
    private $bbsid;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="description", type="string", length=100, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @Column(name="accounting", type="string", length=100, nullable=true)
     */
    private $accounting;

    /**
     * @var integer
     *
     * @Column(name="show_balancing", type="integer", nullable=false)
     */
    private $showBalancing;

    /**
     * @var integer
     *
     * @Column(name="send_register", type="integer", nullable=false)
     */
    private $sendRegister;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active = '1';

    function getIdcreditcard() {
        return $this->idcreditcard;
    }

    function getBbsid() {
        return $this->bbsid;
    }

    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getAccounting() {
        return $this->accounting;
    }

    function getShowBalancing() {
        return $this->showBalancing;
    }

    function getSendRegister() {
        return $this->sendRegister;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdcreditcard($idcreditcard) {
        $this->idcreditcard = $idcreditcard;
    }

    function setBbsid($bbsid) {
        $this->bbsid = $bbsid;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setAccounting($accounting) {
        $this->accounting = $accounting;
    }

    function setShowBalancing($showBalancing) {
        $this->showBalancing = $showBalancing;
    }

    function setSendRegister($sendRegister) {
        $this->sendRegister = $sendRegister;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }


}

