<?php

use Doctrine\Mapping as ORM;

/**
 * LogTime
 *
 * @Table(name="log_time", indexes={@Index(name="fk_log_time_user1_idx", columns={"users_idusers"}),@Index(name="fk_log_time_pos1_idx", columns={"pos_idpos"})})
 * @Entity
 */
class LogTime {

    /**
     * @var integer
     *
     * @Column(name="idlog_time", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idlogTime;

    /**
     * @var \DateTime
     *
     * @Column(name="login_date", type="datetime", nullable=true)
     */
    private $loginDate;

    /**
     * @var boolean
     *
     * @Column(name="status", type="boolean", nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @Column(name="current_multiple_connections", type="integer", nullable=false)
     */
    private $currentMultipleConnections;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $posIdpos;

    /**
     * Get idlogTime
     *
     * @return integer
     */
    public function getIdlogTime() {
        return $this->idlogTime;
    }

    /**
     * Set loginDate
     *
     * @param \DateTime $loginDate
     *
     * @return LogTime
     */
    public function setLoginDate($loginDate) {
        $this->loginDate = $loginDate;

        return $this;
    }

    /**
     * Get loginDate
     *
     * @return \DateTime
     */
    public function getLoginDate() {
        return $this->loginDate;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return LogTime
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set currentMultipleConnections
     *
     * @param integer $currentMultipleConnections
     *
     * @return LogTime
     */
    public function setCurrentMultipleConnections($currentMultipleConnections) {
        $this->currentMultipleConnections = $currentMultipleConnections;

        return $this;
    }

    /**
     * Get currentMultipleConnections
     *
     * @return integer
     */
    public function getCurrentMultipleConnections() {
        return $this->currentMultipleConnections;
    }

    /**
     * Set usersusers
     *
     * @param \Users $usersusers
     *
     * @return Users
     */
    public function setUsersusers(\Users $usersusers = null) {
        $this->usersusers = $usersusers;

        return $this;
    }

    /**
     * Get usersusers
     *
     * @return \Users
     */
    public function getUsersusers() {
        return $this->usersusers;
    }
    
    /**
     * getPosid
     * 
     * @return type
     */
    function getPosIdpos() {
        return $this->posIdpos;
    }
    
    /**
     * SetPosId
     * 
     * @param \Pos $posIdpos
     */
    function setPosIdpos(\Pos $posIdpos) {
        $this->posIdpos = $posIdpos;
    }



}
