<?php



use Doctrine\Mapping as ORM;

/**
 * Invoice
 *
 * @Table(name="invoice", indexes={@Index(name="fk_invoice_order1_idx", columns={"order_idorder"}), @Index(name="fk_invoice_invoice_tmpt1_idx", columns={"invoice_tmpt_idinvoice_tmpt"})})
 * @Entity
 */
class Invoice
{
    /**
     * @var integer
     *
     * @Column(name="idinvoice", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idinvoice;

    /**
     * @var integer
     *
     * @Column(name="id", type="integer", nullable=false)
     */
    private $id;

    /**
     * @var string
     *
     * @Column(name="extra_info", type="text", nullable=true)
     */
    private $extraInfo;

    /**
     * @var \DateTime
     *
     * @Column(name="printed", type="datetime", nullable=true)
     */
    private $printed;
    /**
     * @var \DateTime
     *
     * @Column(name="date_due", type="datetime", nullable=true)
     */
    private $dateDue;
    /**
     * @var \DateTime
     *
     * @Column(name="limit_date_discount", type="datetime", nullable=true)
     */
    private $limitDateDiscount;

    /**
     * @var integer
     *
     * @Column(name="status_bank", type="integer", nullable=false)
     */
    private $statusBank;

    /**
     * @var integer
     *
     * @Column(name="status_payment", type="integer", nullable=false)
     */
    private $statusPayment;

    /**
     * @var string
     *
     * @Column(name="value_payment", type="decimal", precision=11, scale=0, nullable=true)
     */
    private $valuePayment;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \InvoiceTmpt
     *
     * @ManyToOne(targetEntity="InvoiceTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="invoice_tmpt_idinvoice_tmpt", referencedColumnName="idinvoice_tmpt")
     * })
     */
    private $invoiceTmptinvoiceTmpt;

    /**
     * @var \Order
     *
     * @ManyToOne(targetEntity="Order")
     * @JoinColumns({
     *   @JoinColumn(name="order_idorder", referencedColumnName="idorder")
     * })
     */
    private $orderorder;



    /**
     * Get idinvoice
     *
     * @return integer
     */
    public function getIdinvoice()
    {
        return $this->idinvoice;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Invoice
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set extraInfo
     *
     * @param string $extraInfo
     *
     * @return Invoice
     */
    public function setExtraInfo($extraInfo)
    {
        $this->extraInfo = $extraInfo;

        return $this;
    }

    /**
     * Get extraInfo
     *
     * @return string
     */
    public function getExtraInfo()
    {
        return $this->extraInfo;
    }

    /**
     * Set printed
     *
     * @param \DateTime $printed
     *
     * @return Invoice
     */
    public function setPrinted($printed)
    {
        $this->printed = $printed;

        return $this;
    }

    /**
     * Get printed
     *
     * @return \DateTime
     */
    public function getPrinted()
    {
        return $this->printed;
    }


    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Invoice
     */
    public function setDateDue($dateDue)
    {
        $this->dateDue = $dateDue;

        return $this;
    }
    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateDue()
    {
        return $this->dateDue;
    }

    /**
     * Set limitDateDiscount
     *
     * @param \DateTime $limitDateDiscount
     *
     * @return Invoice
     */
    public function setLimitDateDiscount($limitDateDiscount)
    {
        $this->limitDateDiscount = $limitDateDiscount;

        return $this;
    }


     /**
     * Get limitDateDiscount
     *
     * @return \DateTime
     */
    public function getLimitDateDiscount()
    {
        return $this->limitDateDiscount;
    }

     /**
     * Set limitDateDiscount
     *
     * @param integer $statusBank
     *
     * @return Invoice
     */
    public function setStatusBank($statusBank)
    {
        $this->statusBank = $statusBank;

        return $this;
    }

    /**
     * Get statusBank
     *
     * @return integer
     */
    public function getStatusBank()
    {
        return $this->statusBank;
    }

    /**
     * Set statusPayment
     *
     * @param integer $statusPayment
     *
     * @return Invoice
     */
    public function setStatusPayment($statusPayment)
    {
        $this->statusPayment = $statusPayment;

        return $this;
    }

    /**
     * Get statusPayment
     *
     * @return integer
     */
    public function getStatusPayment()
    {
        return $this->statusPayment;
    }

    /**
     * Set valuePayment
     *
     * @param integer $valuePayment
     *
     * @return Invoice
     */
    public function setValuePayment($valuePayment)
    {
        $this->valuePayment = $valuePayment;

        return $this;
    }

    /**
     * Get valuePayment
     *
     * @return string
     */
    public function getValuePayment()
    {
        return $this->valuePayment;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return Invoice
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Invoice
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return Invoice
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Invoice
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set invoiceTmptinvoiceTmpt
     *
     * @param \InvoiceTmpt $invoiceTmptinvoiceTmpt
     *
     * @return Invoice
     */
    public function setInvoiceTmptinvoiceTmpt(\InvoiceTmpt $invoiceTmptinvoiceTmpt = null)
    {
        $this->invoiceTmptinvoiceTmpt = $invoiceTmptinvoiceTmpt;

        return $this;
    }

    /**
     * Get invoiceTmptinvoiceTmpt
     *
     * @return \InvoiceTmpt
     */
    public function getInvoiceTmptinvoiceTmpt()
    {
        return $this->invoiceTmptinvoiceTmpt;
    }

    /**
     * Set orderorder
     *
     * @param \Order $orderorder
     *
     * @return Invoice
     */
    public function setOrderorder(\Order $orderorder = null)
    {
        $this->orderorder = $orderorder;

        return $this;
    }

    /**
     * Get orderorder
     *
     * @return \Order
     */
    public function getOrderorder()
    {
        return $this->orderorder;
    }
}
