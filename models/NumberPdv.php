<?php

use Doctrine\Mapping as ORM;

/**
 * NumberPdv
 *
 * @Table(name="number_pdv", indexes={@Index(name="fk_pos_company", columns={"pos_company_idpos_company"})})
 * @Entity
 */
class NumberPdv {

    /**
     * @var integer
     *
     * @Column(name="idnumber_pdv", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idnumberpdv;

      /**
     * @var string
     *
     * @Column(name="mac", type="string", length=80)
     */
    private $mac;

    /**
     * @var \PosCompany
     *
     * @ManyToOne(targetEntity="PosCompany")
     * @JoinColumns({
     *   @JoinColumn(name="pos_company_idpos_company", referencedColumnName="idpos_company")
     * })
     */
    private $poscompany;

    /**
     * @var integer
     *
     * @Column(name="number", type="integer", nullable=false)
     */
    private $number;
    
    function getIdnumberpdv() {
        return $this->idnumberpdv;
    }

    function getMac() {
        return $this->mac;
    }

    function getPoscompany() {
        return $this->poscompany;
    }

    function getNumber() {
        return $this->number;
    }

    function setIdnumberpdv($idnumberpdv) {
        $this->idnumberpdv = $idnumberpdv;
    }

    function setMac($mac) {
        $this->mac = $mac;
    }

    function setPoscompany(\PosCompany $poscompany) {
        $this->poscompany = $poscompany;
    }

    function setNumber($number) {
        $this->number = $number;
    }



}
