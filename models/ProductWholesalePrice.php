<?php



use Doctrine\Mapping as ORM;

/**
 * ProductWholesalePrice
 *
 * @Table(name="product_wholesale_price", indexes={@Index(name="fk_wholesale_product", columns={"product_idproduct"})})
 * @Entity
 */
class ProductWholesalePrice
{
    /**
     * @var integer
     *
     * @Column(name="id_product_wholesale_price", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idProductWholesalePrice;

    /**
     * @var string
     *
     * @Column(name="unit_price", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $unitPrice;

    /**
     * @var integer
     *
     * @Column(name="qtd", type="integer", nullable=false)
     */
    private $qtd = '1';

    /**
     * @var string
     *
     * @Column(name="price_for_quantity", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $priceForQuantity;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=true)
     */
    private $dateCreate = 'CURRENT_TIMESTAMP';

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=true)
     */
    private $active = '1';

    /**
     * @var \Product
     *
     * @ManyToOne(targetEntity="Product")
     * @JoinColumns({
     *   @JoinColumn(name="product_idproduct", referencedColumnName="idproduct")
     * })
     */
    private $productproduct;

    function getIdProductWholesalePrice() {
        return $this->idProductWholesalePrice;
    }

    function getUnitPrice() {
        return $this->unitPrice;
    }

    function getQtd() {
        return $this->qtd;
    }

    function getPriceForQuantity() {
        return $this->priceForQuantity;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getProductproduct() {
        return $this->productproduct;
    }

    function setIdProductWholesalePrice($idProductWholesalePrice) {
        $this->idProductWholesalePrice = $idProductWholesalePrice;
    }

    function setUnitPrice($unitPrice) {
        $this->unitPrice = $unitPrice;
    }

    function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    function setPriceForQuantity($priceForQuantity) {
        $this->priceForQuantity = $priceForQuantity;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setProductproduct(\Product $productproduct) {
        $this->productproduct = $productproduct;
    }


}

