<?php



use Doctrine\Mapping as ORM;

/**
 * PersonSettings
 *
 * @Table(name="person_settings", indexes={@Index(name="fk_person_settings_invoice_tmpt1_idx", columns={"invoice_tmpt_idinvoice_tmpt"}), @Index(name="fk_person_settings_currency1_idx", columns={"currency_idcurrency"}), @Index(name="fk_person_settings_person1_idx", columns={"person_idperson"}), @Index(name="fk_person_settings_ticket_tmpt1_idx", columns={"ticket_tmpt_idticket_tmpt"}), @Index(name="fk_person_settings_project1_idx", columns={"project_idproject"}), @Index(name="fk_person_settings_language1_idx", columns={"language_idlanguage"})})
 * @Entity
 */
class PersonSettings
{
    /**
     * @var integer
     *
     * @Column(name="idperson_settings", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpersonSettings;

    /**
     * @var boolean
     *
     * @Column(name="tax_free", type="boolean", nullable=true)
     */
    private $taxFree;

    /**
     * @var boolean
     *
     * @Column(name="inv_show_customer_name", type="boolean", nullable=true)
     */
    private $invShowCustomerName = '1';

    /**
     * @var boolean
     *
     * @Column(name="inv_group_invoices", type="boolean", nullable=true)
     */
    private $invGroupInvoices = '1';

    /**
     * @var boolean
     *
     * @Column(name="inv_tax_free", type="boolean", nullable=true)
     */
    private $invTaxFree = '0';

    /**
     * @var boolean
     *
     * @Column(name="inv_invoice_print_exclusive", type="boolean", nullable=true)
     */
    private $invInvoicePrintExclusive = '0';

    /**
     * @var boolean
     *
     * @Column(name="inv_hide_products", type="boolean", nullable=true)
     */
    private $invHideProducts = '0';

    /**
     * @var integer
     *
     * @Column(name="inv_electronic_invoice", type="integer", nullable=true)
     */
    private $invElectronicInvoice = '0';

    /**
     * @var string
     *
     * @Column(name="inv_info", type="text", length=16777215, nullable=true)
     */
    private $invInfo;

    /**
     * @var string
     *
     * @Column(name="inv_email", type="string", length=100, nullable=true)
     */
    private $invEmail;

    /**
     * @var boolean
     *
     * @Column(name="inv_track_email", type="boolean", nullable=true)
     */
    private $invTrackEmail;

    /**
     * @var boolean
     *
     * @Column(name="ord_show_info", type="boolean", nullable=true)
     */
    private $ordShowInfo;

    /**
     * @var string
     *
     * @Column(name="ord_hide_from_statistics", type="string", length=45, nullable=true)
     */
    private $ordHideFromStatistics;

    /**
     * @var string
     *
     * @Column(name="ord_use_com_address", type="string", length=45, nullable=true)
     */
    private $ordUseComAddress;

    /**
     * @var string
     *
     * @Column(name="ord_credit_limit", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $ordCreditLimit;

    /**
     * @var integer
     *
     * @Column(name="charge_times", type="integer", nullable=true)
     */
    private $chargeTimes = '1';

    /**
     * @var boolean
     *
     * @Column(name="auth_receive_sms", type="boolean", nullable=true)
     */
    private $authReceiveSms = '0';

    /**
     * @var boolean
     *
     * @Column(name="auth_receive_email", type="boolean", nullable=true)
     */
    private $authReceiveEmail = '0';

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var \Currency
     *
     * @ManyToOne(targetEntity="Currency")
     * @JoinColumns({
     *   @JoinColumn(name="currency_idcurrency", referencedColumnName="idcurrency")
     * })
     */
    private $currencycurrency;

    /**
     * @var \InvoiceTmpt
     *
     * @ManyToOne(targetEntity="InvoiceTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="invoice_tmpt_idinvoice_tmpt", referencedColumnName="idinvoice_tmpt")
     * })
     */
    private $invoiceTmptinvoiceTmpt;

    /**
     * @var \Language
     *
     * @ManyToOne(targetEntity="Language")
     * @JoinColumns({
     *   @JoinColumn(name="language_idlanguage", referencedColumnName="idlanguage")
     * })
     */
    private $languagelanguage;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;

    /**
     * @var \Project
     *
     * @ManyToOne(targetEntity="Project")
     * @JoinColumns({
     *   @JoinColumn(name="project_idproject", referencedColumnName="idproject")
     * })
     */
    private $projectproject;

    /**
     * @var \TicketTmpt
     *
     * @ManyToOne(targetEntity="TicketTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="ticket_tmpt_idticket_tmpt", referencedColumnName="idticket_tmpt")
     * })
     */
    private $ticketTmptticketTmpt;

    function getIdpersonSettings() {
        return $this->idpersonSettings;
    }

    function getTaxFree() {
        return $this->taxFree;
    }

    function getInvShowCustomerName() {
        return $this->invShowCustomerName;
    }

    function getInvGroupInvoices() {
        return $this->invGroupInvoices;
    }

    function getInvTaxFree() {
        return $this->invTaxFree;
    }

    function getInvInvoicePrintExclusive() {
        return $this->invInvoicePrintExclusive;
    }

    function getInvHideProducts() {
        return $this->invHideProducts;
    }

    function getInvElectronicInvoice() {
        return $this->invElectronicInvoice;
    }

    function getInvInfo() {
        return $this->invInfo;
    }

    function getInvEmail() {
        return $this->invEmail;
    }

    function getInvTrackEmail() {
        return $this->invTrackEmail;
    }

    function getOrdShowInfo() {
        return $this->ordShowInfo;
    }

    function getOrdHideFromStatistics() {
        return $this->ordHideFromStatistics;
    }

    function getOrdUseComAddress() {
        return $this->ordUseComAddress;
    }

    function getOrdCreditLimit() {
        return $this->ordCreditLimit;
    }

    function getChargeTimes() {
        return $this->chargeTimes;
    }

    function getAuthReceiveSms() {
        return $this->authReceiveSms;
    }

    function getAuthReceiveEmail() {
        return $this->authReceiveEmail;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function getCurrencycurrency() {
        return $this->currencycurrency;
    }

    function getInvoiceTmptinvoiceTmpt() {
        return $this->invoiceTmptinvoiceTmpt;
    }

    function getLanguagelanguage() {
        return $this->languagelanguage;
    }

    function getPersonperson() {
        return $this->personperson;
    }

    function getProjectproject() {
        return $this->projectproject;
    }

    function getTicketTmptticketTmpt() {
        return $this->ticketTmptticketTmpt;
    }

    function setIdpersonSettings($idpersonSettings) {
        $this->idpersonSettings = $idpersonSettings;
    }

    function setTaxFree($taxFree) {
        $this->taxFree = $taxFree;
    }

    function setInvShowCustomerName($invShowCustomerName) {
        $this->invShowCustomerName = $invShowCustomerName;
    }

    function setInvGroupInvoices($invGroupInvoices) {
        $this->invGroupInvoices = $invGroupInvoices;
    }

    function setInvTaxFree($invTaxFree) {
        $this->invTaxFree = $invTaxFree;
    }

    function setInvInvoicePrintExclusive($invInvoicePrintExclusive) {
        $this->invInvoicePrintExclusive = $invInvoicePrintExclusive;
    }

    function setInvHideProducts($invHideProducts) {
        $this->invHideProducts = $invHideProducts;
    }

    function setInvElectronicInvoice($invElectronicInvoice) {
        $this->invElectronicInvoice = $invElectronicInvoice;
    }

    function setInvInfo($invInfo) {
        $this->invInfo = $invInfo;
    }

    function setInvEmail($invEmail) {
        $this->invEmail = $invEmail;
    }

    function setInvTrackEmail($invTrackEmail) {
        $this->invTrackEmail = $invTrackEmail;
    }

    function setOrdShowInfo($ordShowInfo) {
        $this->ordShowInfo = $ordShowInfo;
    }

    function setOrdHideFromStatistics($ordHideFromStatistics) {
        $this->ordHideFromStatistics = $ordHideFromStatistics;
    }

    function setOrdUseComAddress($ordUseComAddress) {
        $this->ordUseComAddress = $ordUseComAddress;
    }

    function setOrdCreditLimit($ordCreditLimit) {
        $this->ordCreditLimit = $ordCreditLimit;
    }

    function setChargeTimes($chargeTimes) {
        $this->chargeTimes = $chargeTimes;
    }

    function setAuthReceiveSms($authReceiveSms) {
        $this->authReceiveSms = $authReceiveSms;
    }

    function setAuthReceiveEmail($authReceiveEmail) {
        $this->authReceiveEmail = $authReceiveEmail;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

    function setCurrencycurrency(\Currency $currencycurrency) {
        $this->currencycurrency = $currencycurrency;
    }

    function setInvoiceTmptinvoiceTmpt(\InvoiceTmpt $invoiceTmptinvoiceTmpt) {
        $this->invoiceTmptinvoiceTmpt = $invoiceTmptinvoiceTmpt;
    }

    function setLanguagelanguage(\Language $languagelanguage) {
        $this->languagelanguage = $languagelanguage;
    }

    function setPersonperson(\Person $personperson) {
        $this->personperson = $personperson;
    }

    function setProjectproject(\Project $projectproject) {
        $this->projectproject = $projectproject;
    }

    function setTicketTmptticketTmpt(\TicketTmpt $ticketTmptticketTmpt) {
        $this->ticketTmptticketTmpt = $ticketTmptticketTmpt;
    }


}

