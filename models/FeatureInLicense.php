<?php



use Doctrine\Mapping as ORM;

/**
 * FeatureInLicense
 *
 * @Table(name="feature_in_license", indexes={@Index(name="license", columns={"license"}), @Index(name="feature", columns={"feature"})})
 * @Entity
 */
class FeatureInLicense
{
    /**
     * @var integer
     *
     * @Column(name="idfeaturesinlicense", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idfeaturesinlicense;

    /**
     * @var \LicenseFeatures
     *
     * @ManyToOne(targetEntity="LicenseFeatures")
     * @JoinColumns({
     *   @JoinColumn(name="feature", referencedColumnName="idfeatures")
     * })
     */
    private $feature;

    /**
     * @var \License
     *
     * @ManyToOne(targetEntity="License")
     * @JoinColumns({
     *   @JoinColumn(name="license", referencedColumnName="idlicense")
     * })
     */
    private $license;

    function getIdfeaturesinlicense() {
        return $this->idfeaturesinlicense;
    }

    function getFeature() {
        return $this->feature;
    }

    function getLicense() {
        return $this->license;
    }

    function setIdfeaturesinlicense($idfeaturesinlicense) {
        $this->idfeaturesinlicense = $idfeaturesinlicense;
    }

    function setFeature(\LicenseFeatures $feature) {
        $this->feature = $feature;
    }

    function setLicense(\License $license) {
        $this->license = $license;
    }


}

