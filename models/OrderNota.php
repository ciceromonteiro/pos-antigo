<?php



use Doctrine\Mapping as ORM;

/**
 * OrderNota
 *
 * @Table(name="order_nota")
 * @Entity
 */
class OrderNota
{
    /**
     * @var integer
     *
     * @Column(name="idorder_nota", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idorderNote;

     /**
     * @var integer
     *
     * @Column(name="order_idorder", type="integer", nullable=false)
     */
    private $orderIdorder;

    /**
     * @var string
     *
     * @Column(name="nota_number", type="string", length=100, nullable=true)
     */
    private $notaNumber;

    /**
     * @var string
     *
     * @Column(name="nota_serie", type="string", length=100, nullable=true)
     */
    private $notaSerie;

    /**
     * @var string
     *
     * @Column(name="nota_key", type="string", length=250, nullable=true)
     */
    private $notaKey;

    /**
     * @var string
     *
     * @Column(name="nota_xml", type="text", nullable=true)
     */
    private $notaXml;


    /**
     * @var string
     *
     * @Column(name="return_xml", type="text", nullable=true)
     */
    private $returnXml;

    /**
     * @var integer
     *
     * @Column(name="nota_pdf", type="text", nullable=true)
     */
    private $notaPdf;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

   /**
     * @return int
     */
    public function getidorderNote() {
        return $this->idorderNote;
    }

    /**
     * @param int $idorderNote
     */
    public function setidorderNote($idorderNote) {
        $this->idorderNote = $idorderNote;
    }

    /**
     * @return int
     */
    public function getorderIdorder() {
        return $this->orderIdorder;
    }

    /**
     * @param int $orderIdorder
     */
    public function setorderIdorder($orderIdorder) {
        $this->orderIdorder = $orderIdorder;
    }

    /**
     * @return String
     */
    public function getnotaNumber() {
        return $this->notaNumber;
    }

    /**
     * @param String $notaNumber
     */
    public function setnotaNumber($notaNumber) {
        $this->notaNumber = $notaNumber;
    }

    /**
     * @return String
     */
    public function getnotaSerie() {
        return $this->notaSerie;
    }

    /**
     * @param String $notaSerie
     */
    public function setnotaSerie($notaSerie) {
        $this->notaSerie = $notaSerie;
    }

    /**
     * @return int
     */
    public function getnotaKey() {
        return $this->notaKey;
    }

    /**
     * @param int $notaKey
     */
    public function setnotaKey($notaKey) {
        $this->notaKey = $notaKey;
    }

   /**
     * @return int
     */
    public function getnotaXml() {
        return $this->notaXml;
    }

    /**
     * @param int $notaXml
     */
    public function setnotaXml($notaXml) {
        $this->notaXml = $notaXml;
    }
   /**
     * @return int
     */
    public function getreturnXml() {
        return $this->returnXml;
    }

    /**
     * @param int $returnXml
     */
    public function setreturnXml($returnXml) {
        $this->returnXml = $returnXml;
    }
   /**
     * @return text
     */
    public function getnotaPdf() {
        return $this->notaPdf;
    }

    /**
     * @param text $notaPdf
     */
    public function setnotaPdf($notaPdf) {
        $this->notaPdf = $notaPdf;
    }
   function getDateCreate() {
        return $this->dateCreate;
    }

   function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }


}
