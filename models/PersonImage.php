<?php

use Doctrine\Mapping as ORM;

/**
 * PersonImage
 *
 * @Table(name="person_image", indexes={@Index(name="fk_person_image_person1_idx", columns={"person_idperson"})})
 * @Entity
 */
class PersonImage {

    /**
     * @var integer
     *
     * @Column(name="idperson_image", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpersonImage;

    /**
     * @var text
     *
     * @Column(name="`path`", type="text", length=16777215, nullable=true)
     */
    private $path;

    /**
     * @var integer
     *
     * @Column(name="`order`", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;

    /**
     * @var string
     *
     * @Column(name="type_img", type="string", length=10, nullable=true)
     */
    private $typeimg;

    /**
     * Get idpersonImage
     *
     * @return integer
     */
    public function getIdpersonImage() {
        return $this->idpersonImage;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return PersonImage
     */
    public function setPath($path) {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return PersonImage
     */
    public function setOrder($order) {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return PersonImage
     */
    public function setDateCreate($dateCreate) {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate() {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return PersonImage
     */
    public function setDateUpdate($dateUpdate) {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate() {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return PersonImage
     */
    public function setDateDelete($dateDelete) {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete() {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PersonImage
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set personperson
     *
     * @param \Person $personperson
     *
     * @return PersonImage
     */
    public function setPersonperson(\Person $personperson = null) {
        $this->personperson = $personperson;

        return $this;
    }

    /**
     * Get personperson
     *
     * @return \Person
     */
    public function getPersonperson() {
        return $this->personperson;
    }
    
    function getTypeimg() {
        return $this->typeimg;
    }

    function setTypeimg($typeimg) {
        $this->typeimg = $typeimg;
    }



}
