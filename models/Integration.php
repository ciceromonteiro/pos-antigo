<?php



use Doctrine\Mapping as ORM;

/**
 * Integration
 *
 * @Table(name="integration")
 * @Entity
 */
class Integration
{
    /**
     * @var integer
     *
     * @Column(name="idintegration", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idintegration;

    /**
     * @var string
     *
     * @Column(name="exchange_platform", type="string", length=150, nullable=true)
     */
    private $exchangePlatform;

    /**
     * @var string
     *
     * @Column(name="exchange_user", type="string", length=200, nullable=true)
     */
    private $exchangeUser;

    /**
     * @var string
     *
     * @Column(name="exchange_password", type="string", length=200, nullable=true)
     */
    private $exchangePassword;

    /**
     * @var integer
     *
     * @Column(name="address_number", type="integer", nullable=true)
     */
    private $addressNumber;

    /**
     * @var string
     *
     * @Column(name="address_message_return", type="string", length=255, nullable=true)
     */
    private $addressMessageReturn;

    /**
     * @var string
     *
     * @Column(name="bestseller_file", type="string", length=150, nullable=true)
     */
    private $bestsellerFile;

    /**
     * @var string
     *
     * @Column(name="bestseller_server", type="string", length=150, nullable=true)
     */
    private $bestsellerServer;

    /**
     * @var string
     *
     * @Column(name="bestseller_user", type="string", length=45, nullable=true)
     */
    private $bestsellerUser;

    /**
     * @var string
     *
     * @Column(name="bestseller_password", type="string", length=150, nullable=true)
     */
    private $bestsellerPassword;

    /**
     * @var integer
     *
     * @Column(name="bestseller_port", type="integer", length=5, nullable=true)
     */
    private $bestsellerPort;    

    /**
     * @var string
     *
     * @Column(name="bestseller_catalog", type="string", length=150, nullable=true)
     */
    private $bestsellerCatalog;  

    /**
     * @var string
     *
     * @Column(name="barcode_file", type="string", length=150, nullable=true)
     */
    private $barcodeFile;

    /**
     * @var integer
     *
     * @Column(name="barcode_customerno", type="integer", nullable=true)
     */
    private $barcodeCustomerno;   

    /**
     * @var string
     *
     * @Column(name="barcode_password", type="string", length=150, nullable=true)
     */
    private $barcodePassword;

    /**
     * @var integer
     *
     * @Column(name="barcode_review", type="integer", length=1, nullable=true)
     */
    private $barcodeReview;   

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;
    
    function getIdintegration() {
        return $this->idintegration;
    }

    function getExchangePlatform() {
        return $this->exchangePlatform;
    }

    function getExchangeUser() {
        return $this->exchangeUser;
    }

    function getExchangePassword() {
        return $this->exchangePassword;
    }

    function getAddressNumber() {
        return $this->addressNumber;
    }

    function getAddressMessageReturn() {
        return $this->addressMessageReturn;
    }

    function getBestsellerFile() {
        return $this->bestsellerFile;
    }

    function getBestsellerServer() {
        return $this->bestsellerServer;
    }

    function getBestsellerUser() {
        return $this->bestsellerUser;
    }

    function getBestsellerPassword() {
        return $this->bestsellerPassword;
    }

    function getBestsellerPort() {
        return $this->bestsellerPort;
    }

    function getBestsellerCatalog() {
        return $this->bestsellerCatalog;
    }

    function getBarcodeFile() {
        return $this->barcodeFile;
    }

    function getBarcodeCustomerno() {
        return $this->barcodeCustomerno;
    }

    function getBarcodePassword() {
        return $this->barcodePassword;
    }

    function getBarcodeReview() {
        return $this->barcodeReview;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function setIdintegration($idintegration) {
        $this->idintegration = $idintegration;
    }

    function setExchangePlatform($exchangePlatform) {
        $this->exchangePlatform = $exchangePlatform;
    }

    function setExchangeUser($exchangeUser) {
        $this->exchangeUser = $exchangeUser;
    }

    function setExchangePassword($exchangePassword) {
        $this->exchangePassword = $exchangePassword;
    }

    function setAddressNumber($addressNumber) {
        $this->addressNumber = $addressNumber;
    }

    function setAddressMessageReturn($addressMessageReturn) {
        $this->addressMessageReturn = $addressMessageReturn;
    }

    function setBestsellerFile($bestsellerFile) {
        $this->bestsellerFile = $bestsellerFile;
    }

    function setBestsellerServer($bestsellerServer) {
        $this->bestsellerServer = $bestsellerServer;
    }

    function setBestsellerUser($bestsellerUser) {
        $this->bestsellerUser = $bestsellerUser;
    }

    function setBestsellerPassword($bestsellerPassword) {
        $this->bestsellerPassword = $bestsellerPassword;
    }

    function setBestsellerPort($bestsellerPort) {
        $this->bestsellerPort = $bestsellerPort;
    }

    function setBestsellerCatalog($bestsellerCatalog) {
        $this->bestsellerCatalog = $bestsellerCatalog;
    }

    function setBarcodeFile($barcodeFile) {
        $this->barcodeFile = $barcodeFile;
    }

    function setBarcodeCustomerno($barcodeCustomerno) {
        $this->barcodeCustomerno = $barcodeCustomerno;
    }

    function setBarcodePassword($barcodePassword) {
        $this->barcodePassword = $barcodePassword;
    }

    function setBarcodeReview($barcodeReview) {
        $this->barcodeReview = $barcodeReview;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }


}
