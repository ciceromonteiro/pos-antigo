<?php



use Doctrine\Mapping as ORM;

/**
 * PosElementInTmpt
 *
 * @Table(name="pos_element_in_tmpt", indexes={@Index(name="fk_pos_element_in_tmpt_pos_tmpt1_idx", columns={"pos_tmpt_idpos_tmpt"}), @Index(name="fk_pos_element_in_tmpt_pos_element1_idx", columns={"pos_element_idpos_element"})})
 * @Entity
 */
class PosElementInTmpt
{
    /**
     * @var integer
     *
     * @Column(name="idpos_element_in_tmpt", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposElementInTmpt;

    /**
     * @var integer
     *
     * @Column(name="position_x", type="integer", nullable=false)
     */
    private $positionX;

    /**
     * @var integer
     *
     * @Column(name="position_y", type="integer", nullable=false)
     */
    private $positionY;

    /**
     * @var integer
     *
     * @Column(name="width", type="integer", nullable=false)
     */
    private $width;

    /**
     * @var integer
     *
     * @Column(name="height", type="integer", nullable=false)
     */
    private $height;

    /**
     * @var \PosElement
     *
     * @ManyToOne(targetEntity="PosElement")
     * @JoinColumns({
     *   @JoinColumn(name="pos_element_idpos_element", referencedColumnName="idpos_element")
     * })
     */
    private $posElementposElement;

    /**
     * @var \PosTmpt
     *
     * @ManyToOne(targetEntity="PosTmpt")
     * @JoinColumns({
     *   @JoinColumn(name="pos_tmpt_idpos_tmpt", referencedColumnName="idpos_tmpt")
     * })
     */
    private $posTmptposTmpt;



    /**
     * Get idposElementInTmpt
     *
     * @return integer
     */
    public function getIdposElementInTmpt()
    {
        return $this->idposElementInTmpt;
    }

    /**
     * Set positionX
     *
     * @param integer $positionX
     *
     * @return PosElementInTmpt
     */
    public function setPositionX($positionX)
    {
        $this->positionX = $positionX;

        return $this;
    }

    /**
     * Get positionX
     *
     * @return integer
     */
    public function getPositionX()
    {
        return $this->positionX;
    }

    /**
     * Set positionY
     *
     * @param integer $positionY
     *
     * @return PosElementInTmpt
     */
    public function setPositionY($positionY)
    {
        $this->positionY = $positionY;

        return $this;
    }

    /**
     * Get positionY
     *
     * @return integer
     */
    public function getPositionY()
    {
        return $this->positionY;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return PosElementInTmpt
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     * @return PosElementInTmpt
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set posElementposElement
     *
     * @param \PosElement $posElementposElement
     *
     * @return PosElementInTmpt
     */
    public function setPosElementposElement(\PosElement $posElementposElement = null)
    {
        $this->posElementposElement = $posElementposElement;

        return $this;
    }

    /**
     * Get posElementposElement
     *
     * @return \PosElement
     */
    public function getPosElementposElement()
    {
        return $this->posElementposElement;
    }

    /**
     * Set posTmptposTmpt
     *
     * @param \PosTmpt $posTmptposTmpt
     *
     * @return PosElementInTmpt
     */
    public function setPosTmptposTmpt(\PosTmpt $posTmptposTmpt = null)
    {
        $this->posTmptposTmpt = $posTmptposTmpt;

        return $this;
    }

    /**
     * Get posTmptposTmpt
     *
     * @return \PosTmpt
     */
    public function getPosTmptposTmpt()
    {
        return $this->posTmptposTmpt;
    }
}
