<?php



use Doctrine\Mapping as ORM;

/**
 * Supplier
 *
 * @Table(name="supplier", indexes={@Index(name="fk_supplier_company1_idx", columns={"company_idcompany"})})
 * @Entity
 */
class Supplier
{
    /**
     * @var integer
     *
     * @Column(name="idsupplier", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsupplier;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=true)
     */
    private $active;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;

    /**
     * @return int
     */
    public function getIdsupplier()
    {
        return $this->idsupplier;
    }

    /**
     * @param int $idsupplier
     */
    public function setIdsupplier($idsupplier)
    {
        $this->idsupplier = $idsupplier;
    }

    /**
     * @return DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * @param DateTime $dateCreate
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    /**
     * @return DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @param DateTime $dateUpdate
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;
    }

    /**
     * @return DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * @param DateTime $dateDelete
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return Company
     */
    public function getCompanycompany()
    {
        return $this->companycompany;
    }

    /**
     * @param Company $companycompany
     */
    public function setCompanycompany($companycompany)
    {
        $this->companycompany = $companycompany;
    }




    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
}
