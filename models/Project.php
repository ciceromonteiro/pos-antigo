<?php

use Doctrine\Mapping as ORM;

/**
 * Project
 *
 * @Table(name="project")
 * @Entity
 */
class Project {
    /**
     * @var integer
     *
     * @Column(name="idproject", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idproject;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @Column(name="date_start", type="datetime", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @Column(name="date_stop", type="datetime", nullable=true)
     */
    private $dateStop;

    /**
     * @var \DateTime
     *
     * @Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var string
     *
     * @Column(name="price", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var string
     *
     * @Column(name="priceAdditional", type="decimal", precision=11, scale=2, nullable=false)
     */
    private $priceadditional;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", nullable=false)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var integer
     *
     * @Column(name="active", type="integer", nullable=false)
     */
    private $active;

    function getIdproject() {
        return $this->idproject;
    }

    function getName() {
        return $this->name;
    }

    function getDescription() {
        return $this->description;
    }

    function getDateStart() {
        return $this->dateStart;
    }

    function getDateStop() {
        return $this->dateStop;
    }

    function getDateEnd() {
        return $this->dateEnd;
    }

    function getPrice() {
        return $this->price;
    }

    function getPriceadditional() {
        return $this->priceadditional;
    }

    function getDateCreate() {
        return $this->dateCreate;
    }

    function getDateUpdate() {
        return $this->dateUpdate;
    }

    function getDateDelete() {
        return $this->dateDelete;
    }

    function getActive() {
        return $this->active;
    }

    function setIdproject($idproject) {
        $this->idproject = $idproject;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setDescription($description) {
        $this->description = $description;
    }

    function setDateStart(\DateTime $dateStart) {
        $this->dateStart = $dateStart;
    }

    function setDateStop(\DateTime $dateStop) {
        $this->dateStop = $dateStop;
    }

    function setDateEnd(\DateTime $dateEnd) {
        $this->dateEnd = $dateEnd;
    }

    function setPrice($price) {
        $this->price = $price;
    }

    function setPriceadditional($priceadditional) {
        $this->priceadditional = $priceadditional;
    }

    function setDateCreate(\DateTime $dateCreate) {
        $this->dateCreate = $dateCreate;
    }

    function setDateUpdate(\DateTime $dateUpdate) {
        $this->dateUpdate = $dateUpdate;
    }

    function setDateDelete(\DateTime $dateDelete) {
        $this->dateDelete = $dateDelete;
    }

    function setActive($active) {
        $this->active = $active;
    }

}