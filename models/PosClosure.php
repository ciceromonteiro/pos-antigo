<?php



use Doctrine\Mapping as ORM;

/**
 * PosClosure
 *
 * @Table(name="pos_closure", indexes={@Index(name="fk_pos_closure_users1_idx", columns={"users_idusers"}), @Index(name="fk_pos_closure_pos1_idx", columns={"pos_idpos"})})
 * @Entity
 */
class PosClosure
{
    /**
     * @var integer
     *
     * @Column(name="idpos_closure", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposClosure;

    /**
     * @var integer
     *
     * @Column(name="protocol", type="integer", nullable=false)
     */
    private $protocol;

    /**
     * @var \DateTime
     *
     * @Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @Column(name="amount_counted", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $amountCounted;

    /**
     * @var string
     *
     * @Column(name="amount_received", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $amountReceived;

    /**
     * @var string
     *
     * @Column(name="amount_credit_card", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $amountCreditCard;

    /**
     * @var \DateTime
     *
     * @Column(name="date_create", type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    private $dateCreate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_update", type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var \DateTime
     *
     * @Column(name="date_delete", type="datetime", nullable=true)
     */
    private $dateDelete;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \Pos
     *
     * @ManyToOne(targetEntity="Pos")
     * @JoinColumns({
     *   @JoinColumn(name="pos_idpos", referencedColumnName="idpos")
     * })
     */
    private $pospos;

    /**
     * @var \Users
     *
     * @ManyToOne(targetEntity="Users")
     * @JoinColumns({
     *   @JoinColumn(name="users_idusers", referencedColumnName="idusers")
     * })
     */
    private $usersusers;



    /**
     * Get idposClosure
     *
     * @return integer
     */
    public function getIdposClosure()
    {
        return $this->idposClosure;
    }

    /**
     * Set protocol
     *
     * @param integer $protocol
     *
     * @return PosClosure
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * Get protocol
     *
     * @return integer
     */
    public function getProtocol()
    {
        return $this->protocol;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return PosClosure
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set amountCounted
     *
     * @param string $amountCounted
     *
     * @return PosClosure
     */
    public function setAmountCounted($amountCounted)
    {
        $this->amountCounted = $amountCounted;

        return $this;
    }

    /**
     * Get amountCounted
     *
     * @return string
     */
    public function getAmountCounted()
    {
        return $this->amountCounted;
    }

    /**
     * Set amountReceived
     *
     * @param string $amountReceived
     *
     * @return PosClosure
     */
    public function setAmountReceived($amountReceived)
    {
        $this->amountReceived = $amountReceived;

        return $this;
    }

    /**
     * Get amountReceived
     *
     * @return string
     */
    public function getAmountReceived()
    {
        return $this->amountReceived;
    }

    /**
     * Set amountCreditCard
     *
     * @param string $amountCreditCard
     *
     * @return PosClosure
     */
    public function setAmountCreditCard($amountCreditCard)
    {
        $this->amountCreditCard = $amountCreditCard;

        return $this;
    }

    /**
     * Get amountCreditCard
     *
     * @return string
     */
    public function getAmountCreditCard()
    {
        return $this->amountCreditCard;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return PosClosure
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return PosClosure
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set dateDelete
     *
     * @param \DateTime $dateDelete
     *
     * @return PosClosure
     */
    public function setDateDelete($dateDelete)
    {
        $this->dateDelete = $dateDelete;

        return $this;
    }

    /**
     * Get dateDelete
     *
     * @return \DateTime
     */
    public function getDateDelete()
    {
        return $this->dateDelete;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return PosClosure
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set pospos
     *
     * @param \Pos $pospos
     *
     * @return PosClosure
     */
    public function setPospos(\Pos $pospos = null)
    {
        $this->pospos = $pospos;

        return $this;
    }

    /**
     * Get pospos
     *
     * @return \Pos
     */
    public function getPospos()
    {
        return $this->pospos;
    }

    /**
     * Set usersusers
     *
     * @param \Users $usersusers
     *
     * @return PosClosure
     */
    public function setUsersusers(\Users $usersusers = null)
    {
        $this->usersusers = $usersusers;

        return $this;
    }

    /**
     * Get usersusers
     *
     * @return \Users
     */
    public function getUsersusers()
    {
        return $this->usersusers;
    }
}
