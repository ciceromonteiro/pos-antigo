<?php



use Doctrine\Mapping as ORM;

/**
 * SupplierFormatApi
 *
 * @Table(name="supplier_format_api")
 * @Entity
 */
class SupplierFormatApi
{
    /**
     * @var integer
     *
     * @Column(name="idsupplierformatapi", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idsupplierformatapi;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=250, nullable=false)
     */
    private $name;

    /**
     * @var boolean
     *
     * @Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    function getIdsupplierformatapi() {
        return $this->idsupplierformatapi;
    }

    function getName() {
        return $this->name;
    }

    function getActive() {
        return $this->active;
    }

    function setIdsupplierformatapi($idsupplierformatapi) {
        $this->idsupplierformatapi = $idsupplierformatapi;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setActive($active) {
        $this->active = $active;
    }

}

