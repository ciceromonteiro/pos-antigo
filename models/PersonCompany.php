<?php



use Doctrine\Mapping as ORM;

/**
 * PersonCompany
 *
 * @Table(name="person_company", indexes={@Index(name="fk_person_company_person1_idx", columns={"person_idperson"}), @Index(name="fk_person_company_company1_idx", columns={"company_idcompany"})})
 * @Entity
 */
class PersonCompany
{
    /**
     * @var integer
     *
     * @Column(name="idperson_company", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idpersonCompany;

    /**
     * @var \Company
     *
     * @ManyToOne(targetEntity="Company")
     * @JoinColumns({
     *   @JoinColumn(name="company_idcompany", referencedColumnName="idcompany")
     * })
     */
    private $companycompany;

    /**
     * @var \Person
     *
     * @ManyToOne(targetEntity="Person")
     * @JoinColumns({
     *   @JoinColumn(name="person_idperson", referencedColumnName="idperson")
     * })
     */
    private $personperson;



    /**
     * Get idpersonCompany
     *
     * @return integer
     */
    public function getIdpersonCompany()
    {
        return $this->idpersonCompany;
    }

    /**
     * Set companycompany
     *
     * @param \Company $companycompany
     *
     * @return PersonCompany
     */
    public function setCompanycompany(\Company $companycompany = null)
    {
        $this->companycompany = $companycompany;

        return $this;
    }

    /**
     * Get companycompany
     *
     * @return \Company
     */
    public function getCompanycompany()
    {
        return $this->companycompany;
    }

    /**
     * Set personperson
     *
     * @param \Person $personperson
     *
     * @return PersonCompany
     */
    public function setPersonperson(\Person $personperson = null)
    {
        $this->personperson = $personperson;

        return $this;
    }

    /**
     * Get personperson
     *
     * @return \Person
     */
    public function getPersonperson()
    {
        return $this->personperson;
    }
}
