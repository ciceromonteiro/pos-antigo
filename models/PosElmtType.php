<?php



use Doctrine\Mapping as ORM;

/**
 * PosElmtType
 *
 * @Table(name="pos_elmt_type")
 * @Entity
 */
class PosElmtType
{
    /**
     * @var integer
     *
     * @Column(name="idpos_elmt_type", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $idposElmtType;

    /**
     * @var string
     *
     * @Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;



    /**
     * Get idposElmtType
     *
     * @return integer
     */
    public function getIdposElmtType()
    {
        return $this->idposElmtType;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PosElmtType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
