<?php
extract($view);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<script type="text/javascript" >
    // CALCULADORA
    
    var vez = '';
    var vezExibir = '';
    var operadorAux;
    var operador1 = '';
    var operador2 = '';
    var operacao = '';
    var podeUsarOperador = false;
    var podeUsarPonto = false;
    var podeUsarZero = true;
    var naoUsouPonto = true;
    
    //digitos
    function clicou0(){
        if (podeUsarZero && vez.length == 0 ){
            vez = vez + '0';
            vezExibir = vezExibir + '0';
            document.getElementById('result').innerHTML = vezExibir;
            podeUsarZero = false;
        } else if (podeUsarZero && vez.length > 0){
            vez = vez + '0';
            vezExibir = vezExibir + '0';
            document.getElementById('result').innerHTML = vezExibir;
        }
    };
    function clicou1(){
        vez = vez + '1';
        vezExibir = vezExibir + '1';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    function clicou2(){
        vez = vez + '2';
        vezExibir = vezExibir + '2';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    function clicou3(){
        vez = vez + '3';
        vezExibir = vezExibir + '3';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    function clicou4(){
        vez = vez + '4';
        vezExibir = vezExibir + '4';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    function clicou5(){
        vez = vez + '5';
        vezExibir = vezExibir + '5';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    function clicou6(){
        vez = vez + '6';
        vezExibir = vezExibir + '6';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    function clicou7(){
        vez = vez + '7';
        vezExibir = vezExibir + '7';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    function clicou8(){
        vez = vez + '8';
        vezExibir = vezExibir + '8';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    function clicou9(){
        vez = vez + '9';
        vezExibir = vezExibir + '9';
        document.getElementById('result').innerHTML = vezExibir;
        podeUsarOperador = true;
        podeUsarPonto = true;
        podeUsarZero = true;
    };
    //operadores
    
    function clicouPonto(){
        if (naoUsouPonto && podeUsarPonto){
            vez = vez + '.';
            vezExibir = vezExibir + '.';
            document.getElementById('result').innerHTML = vezExibir;
            podeUsarPonto = false;
            naoUsouPonto = false;
            podeUsarZero = true;
        }
    };
    function clicouSoma(){
        if (podeUsarOperador){
            if (operador1 == ''){
                operador1 = vez;
                vezExibir = vez + ' + ';
                vez = '';
                operacao = '+';
                document.getElementById('result').innerHTML = vezExibir;
            } else if (operador2 == ''){
                operador2 = vez;
                operadorAux = fazerOperacao(operador1, operador2, operacao);
                document.getElementById('teste').innerHTML = operador1 + ' ' + operacao + ' ' + operador2 + ' = '+operadorAux;
                operador1 = operadorAux;
                operador2 = '';
                vez = '';
                operacao = '+';
                vezExibir = operador1.toString() + ' + ';
                document.getElementById('result').innerHTML = vezExibir;
            }
            podeUsarPonto = true;
            podeUsarOperador = false;
            ponto = false;
            usouPonto = false;
            naoUsouPonto = true;
        }
    };
    function clicouDivi(){
        if (podeUsarOperador){
            if (operador1 == ''){
                operador1 = vez;
                vezExibir = vez + ' / ';
                vez = '';
                operacao = '/';
                document.getElementById('result').innerHTML = vezExibir;
            } else if (operador2 == ''){
                operador2 = vez;
                operadorAux = fazerOperacao(operador1, operador2, operacao);
                document.getElementById('teste').innerHTML = operador1 + ' ' + operacao + ' ' + operador2 + ' = '+operadorAux;
                operador1 = operadorAux;
                operador2 = '';
                vez = '';
                operacao = '/';
                vezExibir = operador1.toString() + ' / ';
                document.getElementById('result').innerHTML = vezExibir;
            }
            podeUsarPonto = true;
            podeUsarOperador = false;
            ponto = false;
            usouPonto = false;
            naoUsouPonto = true;
        }
    };
    function clicouMult(){
        if (podeUsarOperador){
            if (operador1 == ''){
                operador1 = vez;
                vezExibir = vez + ' x ';
                vez = '';
                operacao = 'x';
                document.getElementById('result').innerHTML = vezExibir;
            } else if (operador2 == ''){
                operador2 = vez;
                operadorAux = fazerOperacao(operador1, operador2, operacao);
                document.getElementById('teste').innerHTML = operador1 + ' ' + operacao + ' ' + operador2;
                operador1 = operadorAux;
                operador2 = '';
                vez = '';
                operacao = 'x';
                vezExibir = operador1.toString() + ' x ';
                document.getElementById('result').innerHTML = vezExibir;
            }
            podeUsarPonto = true;
            podeUsarOperador = false;
            ponto = false;
            usouPonto = false;
            naoUsouPonto = true;
        }
    };
    function clicouSubt(){
        if (podeUsarOperador){
            if (operador1 == ''){
                operador1 = vez;
                vezExibir = vez + ' - ';
                vez = '';
                operacao = '-';
                document.getElementById('result').innerHTML = vezExibir;
            } else if (operador2 == ''){
                operador2 = vez;
                operadorAux = fazerOperacao(operador1, operador2, operacao);
                document.getElementById('teste').innerHTML = operador1 + ' ' + operacao + ' ' + operador2;
                operador1 = operadorAux;
                operador2 = '';
                vez = '';
                operacao = '-';
                vezExibir = operador1.toString() + ' - ';
                document.getElementById('result').innerHTML = vezExibir;
            }
            podeUsarPonto = true;
            podeUsarOperador = false;
            ponto = false;
            usouPonto = false;
            naoUsouPonto = true;
        }
    };
    function clicouIgual(){
        if (podeUsarOperador){
            if (operador1 != '' && vez != '' && operacao != ''){
                operador2 = vez;
                operadorAux = fazerOperacao(operador1, operador2, operacao);
                document.getElementById('teste').innerHTML = operador1 + ' ' + operacao + ' ' + operador2;
                vez = operadorAux.toString();
                vezExibir = operadorAux.toString();
                document.getElementById('result').innerHTML = vezExibir;
                operador1 = '';
                operador2 = ''
                operacao = '';
                podeUsarPonto = true;
                operador = false;
                ponto = false;
                usouPonto = false;
                naoUsouPonto = true;
            }
        }
    }
    function fazerOperacao(operador1, operador2, operacao){
        var resultado;
        switch(operacao){
            case '+':
                return verificarResultado(parseFloat(operador1) + parseFloat(operador2));
            case '-':
                return verificarResultado(parseFloat(operador1) - parseFloat(operador2));
            case 'x':
                return verificarResultado(parseFloat(operador1) * parseFloat(operador2));
            case '/':
                return verificarResultado(parseFloat(operador1) / parseFloat(operador2));
        }
    }
    function verificarResultado(resultado){
        var resultadoString = resultado.toString();
        var posicaoPonto = resultadoString.indexOf(".");
        if (posicaoPonto === -1){
        } else {
            resultado = resultado.toFixed(2);
        }
        return resultado;
    }
    
</script>

<div style="width: 30%; height: 50%; border: 1px solid black; margin: auto;">
    <h5 style="width: 95%; margin: auto;" id="teste" >0</h5>
    <h1 style="width: 95%; margin: auto;" id="result" >0</h1>
    <!--<tr>
        <td><span class="lnr lnr-arrow-left"></span></td>
        <td></td>
        <td></td>
    </tr>-->
    <div style="width: 90%; height: 15%; margin: auto;">
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou7()" >7</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou8()" >8</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou9()" >9</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicouMult()" >x</div>
    </div>
    <div style="width: 90%; height: 15%; margin: auto;" >
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou4()" >4</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou5()" >5</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou6()" >6</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicouSubt()" > - </div>
    </div>
    <div style="width: 90%; height: 15%; margin: auto;" >
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou1()" >1</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou2()" >2</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou3()" >3</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicouSoma()" >+</div>
    </div>
    <div style="width: 90%; height: 15%; margin: auto;" >
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicou0()" >0</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicouPonto()" > . </div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicouIgual()">=</div>
        <div style="width: 25%; height: 100%; float: left;" class="btn btn-calc btn-primary" onclick="clicouDivi()" >/</div>
    </div>
</div>

<p id="teste2" ></p>