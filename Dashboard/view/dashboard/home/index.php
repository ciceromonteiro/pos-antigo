<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Oct 26, 2016
 */
$orders = $array_answer['orders'];
$start = $array_answer['start'];
$end = $array_answer['end'];
?>
<script type="text/javascript">
    $(document).ready(function(){
        base_url = '<?=URL_BASE?>';
        function legendLabelFormatter(label, series) {
            return '<div class="pie-data">'+series.data[0][1]+'</div> <div class="pie-data">('+ Math.round(series.percent) +'%) </div>' + label + '';
        }  
        $.ajax({
            url: base_url+'dashboard/home/getPaymentMethods',
            type: "GET",
            dataType: "json",
            success: function(data) {
                // console.log(data);
                var placeholder = $("#sales_by_payment"); 
                $.plot(placeholder, data, {
                    series: {
                        pie: { 
                            show: true
                        }
                    },
                    legend: {
                        show: true,
                        labelFormatter: legendLabelFormatter
                    }
                });
            }
        });
       $.plot($("#evolution"), [ [[0, 0], [1, 1]] ], { yaxis: { max: 3 } });
    });
</script>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="<?php echo URL_BASE."Dashboard/home/index"?>" class="translate">Painel</a></li>
        <li class=""><a href="<?php echo URL_BASE."Dashboard/home/status"?>" class="translate">Relatório de Status</a></li>
        <li class=""><a href="<?php echo URL_BASE."Dashboard/home/productPerformance"?>" class="translate">Performace de Produto</a></li>
    </ul>
</div>

<!--relatorios -->
<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <h3>Filtros</h3>
            <form class="form-inline">
                <span style="font-size: 20px;" class="lnr lnr-calendar-full"></span>
                <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0 date" autocomplete="off" value="<?=$start?>" id="start" name ="start">
                <span style="font-size: 20px;" class="lnr lnr-calendar-full"></span>
                <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0 date" id="end" autocomplete="off" value="<?=$end?>" name="end">
                <button type="submit" class="btn btn-primary">Buscar</button>
            </form>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-12">
                    <h3>Relatório de Vendas</h3>
                    <?php if($orders):?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Codigo de Barras</th>
                                    <th scope="col">Produto</th>
                                    <th scope="col">Total Vendido</th>
                                    <th scope="col">Valor Vendido</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0; 
                                foreach($orders as $order):
                                    $total += $order['total'];
                                    ?>
                                    <tr>
                                        <th><?=$order['barcode']?></th>
                                        <td><?=$order['name']?></td>
                                        <td><?=$order['qtd']?></td>
                                        <td>R$ <?=$order['total']?></td>
                                    </tr>
                                <?php endforeach?>
                                <tr><th></th><th></th><th>Total:</th><th>R$ <?=number_format((float)$total, 2, '.', '');?></th></tr>
                            </tbody>
                        </table>
                    <?php else:?>
                        <div class="alert alert-info">Nenhum Resultado encontrado</div>
                    <?php endif?>
                </div>
                <div class="col-md-12">
                    <h5>Relatório por Método de Pagamento</h5>
                    <div id="sales_by_payment" style="height: 55em;">

                    </div>
                </div>
                <div class="col-md-12 hide">
                    <h5>Evolution of Sales (in the period)</h5>
                    <dir id="evolution" style="height: 10em">
                        
                    </dir>
                </div>
            </div>
        </div>
        <div class="col-md-4 hide">
            <dir class="row">
                <div class="col-md-12">
                    <h3>Stock Overview</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Barcode</th>
                                <th scope="col">Product</th>
                                <th scope="col">Current Amount</th>
                                <th scope="col">Minumun</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>789081726732</th>
                                <td>Pants Woman L</td>
                                <td>33</td>
                                <td>10</td>
                            </tr>
                            <tr>
                                <th>789085556732</th>
                                <td>Kids Hat - 8/Black</td>
                                <td>30</td>
                                <td>15</td>
                            </tr>
                            <tr>
                                <th>789085326732</th>
                                <td>Nike Shoes</td>
                                <td>75</td>
                                <td>12</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </dir>
        </div>
    </div>
</div>