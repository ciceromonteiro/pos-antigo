<script type="text/javascript">
    $(document).ready(function(){
      $.ajax({
        type:'post',        //Definimos o método HTTP usado
        dataType: 'json',   //Definimos o tipo de retorno
        url: my_url+"Dashboard/Home/getshipping",//Definindo o arquivo onde serão buscados os dados
        success: function(dados){
        document.getElementById("totalShipping").innerHTML = dados.totalShipping;
        document.getElementById("freeShipping").innerHTML = dados.qtdFree+"%";
        document.getElementById("shippingValorTotal").innerHTML = dados.valorTotal;

                            
        }
    });
});
</script>
<div class="panel panel-default"> 
    <div class="panel-heading"> 
        <h3 class="panel-title translate">Shipping</h3> 
    </div> 
    <div class="panel-body">
        <ul class="list-inline">
            <li>
                <p class="translate">Total</p>
                <h2 id="totalShipping" name="totalShipping"></h2>
            </li>
            <li>
                <p class="translate">Free Shipping</p>
                <h2 id="freeShipping" name="freeShipping">54.3%</h2>
            </li>
            <li>
                <p class="translate">Average Shipping</p>
                <h2 id="shippingValorTotal" name="shippingValorTotal"></h2>
            </li>
        </ul>
    </div> 
</div>