<script type="text/javascript">
    $(document).ready(function(){
      $.ajax({
        type:'post',        //Definimos o método HTTP usado
        dataType: 'json',   //Definimos o tipo de retorno
        url: my_url+"Dashboard/Home/getitems",//Definindo o arquivo onde serão buscados os dados
        success: function(dados){
        document.getElementById("itemsByOrders").innerHTML = dados.itemsByOrders;
        document.getElementById("totalItems").innerHTML = dados.total;

                            
        }
    });
});
</script>

<div class="panel panel-default"> 
    <div class="panel-heading"> 
        <h3 class="panel-title translate">Items</h3> 
    </div> 
    <div class="panel-body">
        <ul class="list-inline">
            <li>
                <p class="translate" >Items per Order</p>
                <h2 id="itemsByOrders" name="itemsByOrders"></h2>
            </li>
            <li>
                <p class="translate">Total</p>
                <h2 id="totalItems" name="totalItems"></h2>
            </li>
        </ul>
    </div> 
</div>