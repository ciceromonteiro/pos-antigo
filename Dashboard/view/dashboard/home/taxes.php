<script type="text/javascript">

    $(document).ready(function() {
        $.ajax({
            url : my_url+"Dashboard/Home/getOrdersValueTaxes",
            type: "POST",
            dataType: 'JSON',   
              success: function(data, textStatus, jqXHR){
                    document.getElementById("totalOrdersValue").innerHTML = data.totalOrdersValue;
                    document.getElementById("totalOrdersTax").innerHTML = data.totalOrdersTax;
                    document.getElementById("percentualTax").innerHTML = data.percentualTax+"%";
                    },

                    error: function (jqXHR, textStatus, errorThrown){
                        notification(false);
                        console.log(jqXHR.responseText);
                    }
        });
    });

</script>

<div class="panel panel-default"> 
    <div class="panel-heading"> 
        <h3 class="panel-title translate">Taxes</h3> 
    </div> 
    <div class="panel-body">
        <ul class="list-inline">
            <li>
                <p class="translate">Total</p>
                <h2 id="totalOrdersValue"></h2>
            </li>
            <li>
                <p class="translate">Orders Taxed</p>
                <h2 id="percentualTax"></h2>
            </li>
            <li>
                <p class="translate">Avg Tax</p>
                <h2 id="totalOrdersTax"></h2>
            </li>

        </ul>
    </div> 
</div>