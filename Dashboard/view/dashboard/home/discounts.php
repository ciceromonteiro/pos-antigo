<script type="text/javascript">
    $(document).ready(function(){
      $.ajax({
        type:'post',        //Definimos o método HTTP usado
        dataType: 'json',   //Definimos o tipo de retorno
        url: my_url+"Dashboard/Home/getdiscounts",//Definindo o arquivo onde serão buscados os dados
        success: function(dados){
        document.getElementById("coupons").innerHTML = dados.totalCoupons;
        document.getElementById("discounts").innerHTML = dados.orderDiscount;
        document.getElementById("ordersPorcentagem").innerHTML = dados.porcentagemOrderDiscount + "%";
        document.getElementById("totalDiscounts").innerHTML = dados.discountTotal;

                            
        }
    });
});
</script>
<div class="panel panel-default"> 
    <div class="panel-heading"> 
        <h3 class="panel-title translate">Discounts</h3> 
    </div> 
    <div class="panel-body">
        <ul class="list-inline">
            <li>
                <p class="translate">Coupons</p>
                <h2 id="coupons" name="coupons"></h2>
            </li>
            <li>
                <p class="translate">Discounts</p>
                <h2 id="discounts" name="discounts"></h2>
            </li>

            <li>
                <p class="translate">Orders</p>
                <h2 id="ordersPorcentagem" name="ordersPorcentagem"></h2>
            </li>
            <li>
                <p class="translate">Total</p>
                <h2 id="totalDiscounts" name="totalDiscounts"></h2>
            </li>
        </ul>
    </div> 
</div>