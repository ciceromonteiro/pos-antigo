<div class="panel panel-default"> 
    <div class="panel-heading"> 
        <h3 class="panel-title translate">Sales Breakdown by Account/Guest</h3> 
    </div> 
    <div class="panel-body">
        <script type="text/javascript">
            google.charts.load('current', {'packages': ['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                $.ajax({
                type:'post',        //Definimos o método HTTP usado
                dataType: 'json',   //Definimos o tipo de retorno
                url: my_url+"Dashboard/Home/getsalesBreakdown",//Definindo o arquivo onde serão buscados os dados
                success: function(dados){
                    var data = new google.visualization.DataTable();                 
                    data.addColumn('string', 'Topping');
                    data.addColumn('number', 'Slices');
                    data.addRows([
                        ['New Accounts', dados.newCount],
                        ['Existing Accounts', dados.oldCount],
                        ['Guests', dados.unCount]
                    ]); 
                    var options = {'title': 'Sales Breakdown by Account/Guest'};

                    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
                    chart.draw(data, options); 
                    }

                
                });
                 
            }
        </script>
        <div id="chart_div">
            
        </div>
        
    </div> 
</div>