<?php

/**
 * @author Diego Barbosa <diego@easy-step.me>
 * @version 1.0.0
 * @abstract file created in date Oct 26, 2016
 */
$products = @$array_answer['products'];
$purchassReport = @$array_answer['purchassReport'][0];
$salesReport = @$array_answer['salesReport'][0];
$select_product = @$array_answer['select_product'][0];

?>
<script type="text/javascript">

</script>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class=""><a href="<?php echo URL_BASE."Dashboard/home/index"?>" class="translate">Painel</a></li>
        <li class=""><a href="<?php echo URL_BASE."Dashboard/home/status"?>" class="translate">Relatório de Status</a></li>
        <li class="active"><a href="<?php echo URL_BASE."Dashboard/home/productPerformance"?>" class="translate">Performace de Produto</a></li>
    </ul>
</div>

<!--relatorios -->
<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h3>Performance de Produto</h3>
                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Produtos
                            </div>
                            <div class="panel-body">
                                <select class="select2" id="select_products">
                                    <option value="0" class="select2">Selecione um Produto</option>
                                    <?php foreach($products as $p):?>
                                        <?php $sel = $p['idproduct'] == $_GET['productId'] ? 'selected="true"' : ''?>
                                        <option <?=$sel?> value="<?=$p['idproduct']?>"><?=$p['name']?></option>
                                    <?php endforeach?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php if ($select_product):?>
                        <div class="col-md-12">
                            <h1>Inventário</h1>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            Em estoque:
                                        </div>
                                        <div class="panel-body">
                                            <?php echo $totalAmount = $select_product['totalAmount'] ? $select_product['totalAmount'] : '0'?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            Estoque minimo:
                                        </div>
                                        <div class="panel-body">
                                            <?=$select_product['minStock']?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">
                                            Reservados:
                                        </div>
                                        <div class="panel-body">
                                            <?php echo $inventorytogo = $select_product['inventorytogo'] ? $select_product['inventorytogo'] : '0'?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif?>
                </div>
                <?php if ($select_product):?>
                    <div class="col-md-6">
                        <h1>Vendas</h1>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    Numero de vendas
                                </div>
                                <div class="panel-body">
                                    <?php echo $qtd = $salesReport['qtd'] ? $salesReport['qtd'] : '0'?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Valor de vendas
                                </div>
                                <div class="panel-body">
                                    <?= format_money($salesReport['total'])?>
                                </div>
                            </div>
                        </div>                    
                    </div>
                    <div class="col-md-6">
                        <h1>Compras</h1>
                        <div class="col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    Numero de compras
                                </div>
                                <div class="panel-body">
                                    <?php echo $qtd = $purchassReport['qtd'] ? $purchassReport['qtd'] : '0'?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Valor de compras
                                </div>
                                <div class="panel-body">
                                    <?= format_money($purchassReport['total'])?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif?>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="url_base" value="<?=URL_BASE?>">
<script type="text/javascript">
    $('#select_products').on('change', function (e) {
        var productId = $("#select_products").val(); 
        var url_base = $("#url_base").val();
        console.log(productId);
        if (productId == 0) {
            url = url_base+'Dashboard/home/productPerformance';
        }
        else {
          var url = url_base+'Dashboard/home/productPerformance?productId='+productId;  
        }

        window.location = url;
    });
</script>