<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Oct 26, 2016
 */
$report = $array_answer['report'];
$reOrder = $array_answer['reOrder'];

?>
<script type="text/javascript">

</script>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class=""><a href="<?php echo URL_BASE."Dashboard/home/index"?>" class="translate">Painel</a></li>
        <li class="active"><a href="<?php echo URL_BASE."Dashboard/home/status"?>" class="translate">Relatório de Status</a></li>
        <li class=""><a href="<?php echo URL_BASE."Dashboard/home/productPerformance"?>" class="translate">Performace de Produto</a></li>
    </ul>
</div>

<!--relatorios -->
<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h3>Status</h3>
                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                Items em Estoque
                            </div>
                            <div class="panel-body">
                                <?=$report[0]['on_hands']?>
                            </div>
                        </div>
                    </div>
                   <div class="col-md-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Disponivel para venda
                            </div>
                            <div class="panel-body">
                                <?= $disponiveis = ((int)$report[0]['on_hands'] - (int)$report[0]['to_go']) > 0 ? (int)$report[0]['on_hands'] - (int)$report[0]['to_go'] : "0" ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Items Reservados
                            </div>
                            <div class="panel-body">
                                <?php echo $to_go = $report[0]['to_go']? $report[0]['to_go']: '0'?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                Items para Re-compra
                            </div>
                            <div class="panel-body">
                                <?=$reOrder?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Variação de items
                            </div>
                            <div class="panel-body">
                                <?=$report[0]['total_products']?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                Valor de Mercadoria
                            </div>
                            <div class="panel-body">
                                <?=format_money($report[0]['invetory_value'])?>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>