<?php
//require_once '../init_autoload.php';
use Doctrine\ORM\Query\ResultSetMapping;
// require_once '../libraries/default_helper.php';
/**
 * @author Claudio da Cruz Silva Junior>
 * @version 1.0.0
 * @abstract file created in date Ago 18, 2017
 */
class HomeController {
    /**
     * Index call Entry customers
     * 
     * @access public
     * @return void call preview
     */

    public function index() {
        $navbar = '';
        ini_set('display_errors', 1);// 0 for production

        // fill dates
        $now = new DateTime();
        $start = isset($_GET['start']) ? format_date($_GET['start']) : $now->modifY('- 15 Days')->format('Y-m-d');
        $end = isset($_GET['end']) ? format_date($_GET['end']) : $now->modify('+ 20 Days')->format('Y-m-d');

        if (isset($start)) {
            $em = getEm();
            $qb =  $em->getRepository('OrderLine')
                ->createQueryBuilder('ol')
                ->andWhere('o.dateCreate BETWEEN :start AND :end')
                ->select('SUM(ol.value) as total', 'COUNT(ol) as qtd', 'p.name', 'p.barcode')
                ->join('ol.orderorder', 'o')
                ->join('ol.productproduct', 'p')
                ->setParameter('start', ''.$start.' 00:00:00')
                ->setParameter('end', ''.$end.' 23:59:59')
                ->groupBy('p')
                ->getQuery()
                ->getResult();
            $result = $qb;
        }
        $array_answer = array(
            'orders' => $result,
            'start' => format_date($start, 'd/m/Y'),
            'end' => format_date($end, 'd/m/Y')
        );
        GenericController::template("Dashboard", "home", "index", $navbar, $array_answer, 1);


    }
    /**
     * Get all orders by active with date today
     * @return string json_data 
     */
    public function getorders() {
        try {
            $orderSales = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $datateste = new DateTime();
            $count = 0;
             foreach ($orderSales as $order) {
             	if($order->getDateCreate() == $datateste->format('d/m/Y')){

             		$count++;
             	}
            }
          
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $json_data = json_encode($count);
        print $json_data;
    }
    /**
     * Get all orders closed by active with date today
     * @return string json_data   
     */
    public function getsales() {
        try {
            $orderSales = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $datateste = new DateTime();
            $count = 0;
             foreach ($orderSales as $order) {
             	if($order->getDateCreate() == $datateste->format('d/m/Y') && $order->getStatus() == 4){

             		$count++;
             	}
            }
          
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $json_data = json_encode($count);
        print $json_data;
    }
    /**
     * Get all orders closed by person
     * @return string json_data 
     */

    public function getsalesBreakdown() {
        try {
            $salesBreakdown = getEm()->getRepository('Order')->findBy(array("active" => 1, "status" => 4));
            $datateste = new DateTime();
            $saleSemConta = 0;
            $saleComNewCount = 0;
            $saleComOldCount = 0;
             foreach ($salesBreakdown as $sales) {
             	if($sales->getPersonperson() == null){
             		$saleSemConta++;

             	}else if($sales->getPersonperson() != null){
                    if($sales->getPersonperson()->getDateCreation()->format('d/m/Y') == $datateste->format('d/m/Y')){

                        $saleComNewCount++;
                    }elseif($sales->getPersonperson()->getDateCreation()->format('d/m/Y') != $datateste->format('d/m/Y')){
                        $saleComOldCount++;
                    }


                }
            }
            
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "unCount" => $saleSemConta,
            "newCount" => $saleComNewCount,
            "oldCount" => $saleComOldCount
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get all orders by person 
     * @return string json_data  
     */
    public function getordersBreakdown() {
        try {
            $ordersBreakdown = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $datateste = new DateTime();
            $orderSemConta = 0;
            $orderComNewCount = 0;
            $orderComOldCount = 0;
             foreach ($ordersBreakdown as $orders) {
                if($orders->getPersonperson() == null){
                    $orderSemConta++;

                }else if($orders->getPersonperson() != null){
                    if($orders->getPersonperson()->getDateCreation()->format('d/m/Y') == $datateste->format('d/m/Y')){

                        $orderComNewCount++;
                    }elseif($orders->getPersonperson()->getDateCreation()->format('d/m/Y') != $datateste->format('d/m/Y')){
                        $orderComOldCount++;
                    }


                }
            }
            
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "unCount" => $orderSemConta,
            "newCount" => $orderComNewCount,
            "oldCount" => $orderComOldCount
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get value of all orderLines e orders by person with date today
     * @return string $json_data  
     */
    public function getaverageOrderValue() {
        try {
            $orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $orderLines = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
            $datateste = new DateTime();

            $orderSemConta = 0;
            $orderComNewCount = 0;
            $orderComOldCount = 0;

            $valueSemCount = 0;
            $valueComNewCount = 0;
            $valueComOldCount = 0;

            $mediaTotal = 0;

            foreach ($orders as $order) {
                if($order->getDateCreate() == $datateste->format('d/m/Y')){
                    foreach($orderLines as $orderLine){
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            if($order->getPersonperson() == null){

                                $valueSemCount = $valueSemCount + ($orderLine->getActualValue() - ($orderLine->getDiscount() + $order->getDiscount()));

                            }else if($order->getPersonperson() != null){
                                if($order->getPersonperson()->getDateCreation()->format('d/m/Y') == $datateste->format('d/m/Y')){

                                    $valueComNewCount = $valueComNewCount + ($orderLine->getActualValue() - ($orderLine->getDiscount() + $order->getDiscount()));
                                }else if($order->getPersonperson()->getDateCreation()->format('d/m/Y') != $datateste->format('d/m/Y')){

                                    $valueComOldCount = $valueComOldCount + ($orderLine->getActualValue() - ($orderLine->getDiscount() + $order->getDiscount()));

                                }
                            }
                        }    
                    }
                }
            }
          
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        
        $mediaTotal = $valueSemCount + $valueComNewCount + $valueComOldCount;
        $valueSemCount = number_format($valueSemCount, 2, ',', '.');
        $valueComNewCount = number_format($valueComNewCount, 2, ',', '.');
        $valueComOldCount = number_format($valueComOldCount, 2, ',', '.');
        $mediaTotal = number_format($mediaTotal, 2, ',', '.');

        $data = array(
            "mediaUnCount" => $valueSemCount,
            "mediaNewCount" => $valueComNewCount,
            "mediaOldCount" => $valueComOldCount,
            "mediaTotal" => $mediaTotal
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get all items and average by orders with date today
     * @return string json_data  
     */
     public function getitems() {
        try {
            $orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $orderLines = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
            $datateste = new DateTime();
            $itensByOrders = 0;
            $totalItems = 0;
            $totalOrders = 0;
            
            foreach($orders as $order){
                if($order->getDateCreate() == $datateste->format('d/m/Y')){
                    foreach ($orderLines as $orderLine) {
                            if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                                    $totalItems = $totalItems + $orderLine->getQty();
                               }

                        }
                        $totalOrders++;
                }
                
            }

            
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        if($totalOrders != 0){
            $itensByOrders = $totalItems/$totalOrders;
            $itensByOrders = number_format($itensByOrders, 2, ',', '.');
            $data = array(
            "total" => $totalItems,
            "itemsByOrders" => $itensByOrders
            ); 
        }else{
            $data = array(
            "total" => 0,
            "itemsByOrders" => 0
            ); 
        }
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get all orders by shipping with date today
     * @return string json_data  
     */
    public function getshipping() {
        try {
            $orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $datateste = new DateTime();
            $totalShipping = 0;
            $valorTotal = 0;
            $qtdFree = 0;
            $freePercentual = 0;
             foreach ($orders as $order) {
                if($order->getDateCreate() == $datateste->format('d/m/Y')){
                    if($order->getTrackingCode() != null){
                        if($order->getValueShipping() != 0){
                            $valorTotal = $valorTotal + $order->getValueShipping();
                        }else{
                            $qtdFree++;
                        }

                        $totalShipping++;

                    }


                }
            }
            
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        if($totalShipping != 0){
            $freePercentual = ($qtdFree * 100)/$totalShipping;
            $valorTotal = number_format($valorTotal, 2, ',', '.');
            $data = array(
                "valorTotal" => $valorTotal,
                "qtdFree" => $freePercentual,
                "totalShipping" => $totalShipping
            );
        }else{
            $data = array(
                "valorTotal" => "0,00",
                "qtdFree" => "0",
                "totalShipping" => "0"
            );
        }
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get all orders e all orders with discount with date today
     * @return string json_data  
     */

    public function getdiscounts() {
        try {
            $orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $ordersLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
            $datateste = new DateTime();

            $totalCoupons = 0;
            $orderDiscount = 0;
            $orderDiscountValor = 0;
            $porcentagemOrderDiscount = 0;
            $orderLineDiscountValor = 0;
            $discountTotal = 0;

            
             foreach ($orders as $order) {
                if($order->getDateCreate() == $datateste->format('d/m/Y')){
                    foreach($ordersLine as $orderLine){
                        if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                            $orderLineDiscountValor = $orderLineDiscountValor + $orderLine->getDiscount();

                         }   
                    }
                    if($order->getDiscount() != 0){
                     $orderDiscount++;   
                     $orderDiscountValor = $orderDiscountValor + $order->getDiscount();
                    }
                    $totalCoupons++;//equivle a todas as orders do dia
                }
            }
            
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        
        $discountTotal = $orderDiscountValor + $orderLineDiscountValor;
        $discountTotal = number_format($discountTotal, 2, ',', '.');

        if($totalCoupons != 0){
        $porcentagemOrderDiscount = ($orderDiscount * 100) / $totalCoupons;
        }else{
        $porcentagemOrderDiscount = 0;  
        }

       
        $data = array(
                "totalCoupons" => $totalCoupons,
                "orderDiscount" => $orderDiscount,
                "porcentagemOrderDiscount" => $porcentagemOrderDiscount,
                "discountTotal" => $discountTotal
            );
       
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get the value of all orders, percentage of tax e the value total of tax of the day
     * @return string json_data  
     */
    public function getOrdersValueTaxes(){
        $totalOrdersValue = 0;
        $totalOrdersTax = 0;
        $percentualTax = 0;
        $orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
        $ordersLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
        $datateste = new DateTime();
        try{
            if($orders != null){
                foreach ($orders as $order) {
                    if($order->getDateCreate() == $datateste->format('d/m/Y')){
                         if($ordersLine != null){
                        foreach ($ordersLine as $orderLine) {
                            if($orderLine->getOrderorder()->getIdorder() == $order->getIdorder()){
                                $totalOrdersValue = $totalOrdersValue + ($orderLine->getActualValue() - $orderLine->getDiscount()); 
                                $totalOrdersTax = $totalOrdersTax + $orderLine->getTax();
                            }
                        }
                    }
                    $totalOrdersValue = $totalOrdersValue - $order->getDiscount();
                    }
                   
                }

            $totalOrdersValue = number_format($totalOrdersValue, 2, ',', '.');    
           $totalOrdersTax = number_format($totalOrdersTax, 2, ',', '.'); 
           if($totalOrdersValue != 0){
            $percentualTax = ($totalOrdersTax * 100) / $totalOrdersValue;
           }
           
           $percentualTax = number_format($percentualTax, 1, ',', '.'); 
            }

        }catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
                "totalOrdersValue" => $totalOrdersValue,
                "totalOrdersTax" => $totalOrdersTax,
                "percentualTax" => $percentualTax

            );
        $json_data = json_encode($data);
        print $json_data;

    }

    public function calculator(){
        $array_answer = [
            'var1' => 'value1'
        ];
        GenericController::template("Dashboard", "home", "calculator", $navbar, $array_answer, 1);
    }
    
    public function keyboardCaller(){
        $array_answer = array("teste2");
        GenericController::template("Dashboard", "home", "keyboardCaller", $navbar, $array_answer, 1);
    }
    
    public function keyboardWrite($txt){
        $myfile = fopen("../data/keyboard/keyboardText.txt", "w+");
        fwrite($myfile, base64_decode($txt));
        fclose($myfile);
    }
    
    public function keyboardRead(){
        GenericController::template("Dashboard", "home", "keyboardRead", $navbar, $array_answer, 1);
    }

    public function getPaymentMethods() {
        header('Content-type: application/json');

        $em = getEm();
        $qb =  $em->getRepository('OrderPayment')
            ->createQueryBuilder('op')
            ->select('COUNT(pm) AS qtd', 'SUM(op.value) as total', 'pm.name')
            ->join('op.paymentMtdpaymentMtd', 'pm') // look at the model how the tables are related, wtf
            ->where('pm.idpaymentMtd != 9999')
            ->groupBy('pm') // must have for select sql functions
            ->getQuery()
            ->getResult();
       $result = $qb;
       $report = [];
       if ($result) {
        foreach ($result as $value) {
           $report[] = [
            'label' => $value['name'],
            'data' => $value['qtd']
           ];
        }
       }
       echo json_encode($report);
    }

    public function status() {
        $navbar = '';
        ini_set('display_errors', 1);// 0 for production
        
        $em = getEm();
            $qb =  $em->getRepository('Product')
            ->createQueryBuilder('P')
            ->select('SUM(P.totalAmount) as on_hands', 'SUM(P.price) as invetory_value','COUNT(P) as total_products', 'SUM(P.inventorytogo) as to_go')
            ->getQuery()
            ->getResult(2);
        $report = $qb;

        $em = getEm();
            $qb =  $em->getRepository('Product')
            ->createQueryBuilder('P')
            ->select('P.minStock', 'P.totalAmount')
            ->getQuery()
            ->getResult(2);
        $re_orders = $qb;
        $reOrder = 0;

        foreach ($re_orders as $key => $value) {
            if ($value['minStock'] == '0') continue;
            if ($value['totalAmount'] < $value['minStock']) {
                $reOrder++;
            }
        }

        $array_answer = array(
            'report' => $report,
            'reOrder' => $reOrder,
        );
        GenericController::template("Dashboard", "home", "status", $navbar, $array_answer, 1);
    }

    public function productPerformance() {
        $navbar = "";
        ini_set('display_errors', 1);// 0 for production

        $em = getEm();
        $qb =  $em->getRepository('Product')
            ->createQueryBuilder('P')
            ->select('P')
            ->getQuery()
            ->getResult(2);
        $products = $qb;

        if (isset($_GET['productId'])) {
            // getting sale report
            $em = getEm();
            $qb =  $em->getRepository('OrderLine')
                ->createQueryBuilder('ol')
                ->select('SUM(ol.value) as total', 'COUNT(o) as qtd', 'p.totalAmount as on_hands', 'p.inventorytogo', 'p.minStock')
                ->join('ol.orderorder', 'o')
                ->join('ol.productproduct', 'p')
                ->where("ol.productproduct = '".$_GET['productId']."'")
                ->groupBy('p')
                ->getQuery()
                ->getResult();
            $salesReport = $qb;

            // getting purchase report
            $em = getEm();
            $qb =  $em->getRepository('PurchaseLine')
                ->createQueryBuilder('pl')
                ->select('SUM(pl.value) as total', 'COUNT(pc) as qtd', 'p.totalAmount as on_hands', 'p.inventorytogo', 'p.minStock')
                ->join('pl.purchasepurchase', 'pc')
                ->join('pl.productproduct', 'p')
                ->where("pl.productproduct = '".$_GET['productId']."'")
                ->groupBy('p')
                ->getQuery()
                ->getResult();
            $purchassReport = $qb;

            // getting selected product
            $em = getEm();
            $qb =  $em->getRepository('Product')
                ->createQueryBuilder('P')
                ->select('P')
                ->andWhere('P.idproduct = :idproduct')
                ->setParameter('idproduct', $_GET['productId'])
                ->getQuery()
                ->getResult(2);
            $select_product = $qb;
        }

        $em = getEm();
        $qb =  $em->getRepository('Product')
            ->createQueryBuilder('P')
            ->select('P.minStock', 'P.totalAmount')
            ->getQuery()
            ->getResult(2);
        $re_orders = $qb;
        $reOrder = 0;

        foreach ($re_orders as $key => $value) {
            if ($value['minStock'] == '0') continue;
            if ($value['totalAmount'] < $value['minStock']) {
                $reOrder++;
            }
        }

        $array_answer = array(
            'products' => @$products,
            'purchassReport' => @$purchassReport,
            'salesReport' => @$salesReport,
            'select_product' => @$select_product,
        );
        GenericController::template("Dashboard", "home", "productPerformance", $navbar, $array_answer, 1);
    }

}
?>