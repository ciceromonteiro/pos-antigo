%A /*/= Avslutt kontant med kvittering
%a /*/= Avslutt kontant uten kvittering
%B /*/= Avslutt bankkort med kvittering
%b /*/= Avslutt bankkort uten kvittering
%C[x] /*/= Legg til kontant betaling (x = belop)
%c[x] /*/= Legg til bankkort betaling (x = belop)
%d /*/= innbetaling av faktura
%e<varenr/strekkode>.<serienr> /*/= Legg til vare med serienr
%F<x> /*/= Sett ordrelinjeregel pa aktiv linje (x=ordrelinjeregel id)
%G /*/= Merk linje som gave
%g /*/= Ta vekk merke som gave
%H[x[.y]] /*/= Bytt til favorittgruppe x (0=standard) og evt fane y
%i /*/= Rediger ordreinfo / hent fra tidligere
%j /*/= Midlertidig kvittering
%K[Kundenummer] /*/= Velg kunde fra kundenr. Spor om nr dersom ikke kundenr er fylt ut
%L[A4|E<x>] /*/= Kopi av siste kvittering (%L . %LA4 eller %LEx der x er etikett ID)
%M /*/= Vis kart fra kundeadressen
%N<x> /*/= Sett ordremal og lagre ordren (x=ordremal id)
%O /*/= Apne kasseskuff
%P[x] /*/= Hent laveste pris pa vare siste x dager (365 dager er standard dersom x er utelatt)
%p[x] /*/= Hent laveste pris pa vare siste x dager for aktuell kundle (365 dager er standard dersom x er utelatt)
%Q /*/= Bytte firma
%S /*/= Angi totalpris pa ordren
%T<x> /*/= Legg til ekstratekst og vis linjeinfo. (x=tekst)
%t<x> /*/= Legg til ekstratekst pa linje. (x=tekst)
%u /*/= Sla opp saldo pa aktiv ordrelinje
%v /*/= Sla opp saldo i annen butikk
%w /*/= Uttak i kasse
%w+ /*/= Innskudd i kasse
%X /*/= Debiter pa rom (Hotel integrasjon)
%y[.x[.y[.z]]] /*/= Velg varer fra varegrupper (x=varegr. 1 id. y=v.gr. 2id. z=v.gr. 3id). Man kan benytte %yy for a benytte varegruppenr i stedet for id
%Z /*/= Ressursallokeringsoversikt
%z /*/= Ny ressursallokering
%%A[x] /*/= Skrive ut ordreetiketter (x=antall. x=? vil sporre)
%%B<x> /*/= Apne et vindu fra admin (x er navn pa vinduet - Sla opp i manualen for oversikt over navn)
%%C<ordrefelt> /*/= Spor om vardien du onsker a sette i feltet.Feks. %%CYourRef
%%C<ordrefelt>=<verdi> /*/= Spor om verdien du onsker a sette i feltet.Feks. %%CYourRef
%%C<ordrefelt>==<annetordrefelt> /*/= Setter ordrefeltet lik et annet ordrefelt.Feks: %%CYourRef==Customer.Contact
%%C<ordrefelt>=#<sokefelt>.[ledetekst][,verdi] /*/= velger ordrefeltet fra en liste der sokefelt stemmer med verdien Dersom ikke verdi er angitt sporres det etter ved hjelp av ledeteksten. Geks: %%CCustomer=#Telephone,Kundens telefonnr %%CCustomer=#CustomerGroup.ID,,3 Dersom du angir OrderLines.<felt> i <ordrefelt> vil bety felt pa aktiv ordrelinje. Feks: %%cOrderLines.Discount=20 Dersom du angir OrderLinesAll,<felt> vil samtlige ordrelinjer endres.
%%D[ordreId] /*/= Hent ordre
%%G[link] /*/= Apner link fra vare eller angitt link
%%J[D] /*/= Overfore hele ordren til bestilling. Dersom D er angitt overfores leveringsdato.
%%J1[D] /*/= Overfore aktiv ordrelinje til bestilling/eksisterende bestilling
%%J@[D] /*/= Overfore hele ordren til eksisterende bestillinger
%%K[printer] /*/= Skriver ut byttelapp pa aktiv linje
%%M[Melding] /*/= Send SMS
%%O /*/= Splitt ordre
%%F[x] /*/= Forhandsbetaling (x=prosentforslag)
%%L[x] /*/= Velg kunde fra kundegrupper (x=kundegruppe Id)
%%P /*/= Oppslag pa lev. varenr (orgsa sentralt)
%%Q /*/= Splitt ordre (Feks del regningen pa det hver og en har spist)
%%R[$][Merket] /*/= Parker/hent ordreDersom $ er angitt vil eksisterende selger ta over bordet/ordren (Feks %%R$)
%%S /*/= Veksle mellom ink/eks mva i kassebildet
%%Tx<Beskjed> /*/= Skriver beskjed ut pa skriver. <br>Der x kan vare:<br> 2 - kasseskriver<br> 3 - plukklisteskriver<br> 4 - skriver<br> 5 - ekstraskriver 1<br> 6 - ekstraskriver 2<br> 7 - ekstraskriver 3<br> 8 - etikettskriver<br> Feks: %%T3BEDES<br> NB! Selger merking on ordrenr hentes fra akriv ordre.
%%U /*/= Hent vareantall fra handterminal
%%V<uId> /*/= Skrolle opp(%%vu) eller ned (%%vd) en linje
%%W /*/= Skrive ut kvittering eller faktura ut i fra serienr.
%%X /*/= Sende e-post til kundle
%%Y /*/= Viser dagens omsetning grafisk
%%Z[x] /*/= Apner betalingsbildet og klikker evt pa en betalingsknapp x=1 klikker pa gavekort, x=2 klikker pa bankkort. Feks %%Z2
%%N<1|2> /*/= Pck Servitor kommandoer<br> 1 - Sett aktiv medarbeiders favorittvarer<br> 2 - Send beskjed til servitorer
%%%A[<pakkenavn[:<pakkepris>[;<antall varer>]]] /*/= Lagpakkepris. Feks: %%$ASkipakke;1200
%%%b[-|+] /*/= Toggler fortegn pa aktuell linje. Dersom man angir - eller + bak sa settes antall til negativt eller positivt og gjor ingenting dersom allerede slik.
%%%c /*/= Spor om hvilken selger som skal logges pa
%%%d /*/= Spor om hvilken selger som skal logges av
%%%e<filnavn> /*/= Apner en wordmal og fyller inn felter fra aktiv ordre
%%%f /*/= Lager avtale pa en Exchange server
%%%G[ordretype] /*/= Lagre ordre (Far tildelt ordrenummer) ordretype: 0 er parkert, 2 er kreditt
%%%H /*/= Vise kontantautomat menyen
%%%Ix /*/= Taxfree Skjema x=i Utsted skjemax=v Annuler skjemax=r Usted nytt skjems og annuler det gamle x=p Skriv ut skjema pa nytt dersom papirstopp etc x=c Send flere ordrenummer
%%%J[VareID] /*/= Se ufakturerte ordre som varen pa aktuell ordrelinje ligger pa, eller pa vare angitt.
%%%k[kode] /*/= Vis slette linje og avbryt ordre i kassa (overstyr navarende selger)
%%%L<x><y> /*/= Legg til tekst i plukklisteinfo der y er teksten og x er funksjon<br> x=0 Legg til tekst<br>x=1 Legg til tekst pa ny linje<br>x=2 Toggle tekst<br>x=3 Legg til tekst og apne linjeinfo<br>Feks %%$L2Ekstra ost
%%%M<w>.<h>.<url> /*/= Apne webside rett i kassa.<br>w=bredde og h er hoyde, url er url. Husk a starte med<br> <<<<<<<http://>>>>>><br>%%$M1024,700,http://showbooking.no?EmpId=<employee.id>%%$M1024,700,http://webshop.no?ArtNo=<orderlines.article.articleno>
%%%N<Beskjed> /*/= Viser en beskjed pa skjermen
%%%O<felt1>|<Operator>|<Felt2>|<Kommando hvis sant>[|<Kommando hvis usann>] /*/= Vis leveringsinformasjon
%%%Q<id>[.parameter] /*/= Start et ordretillegg, NB! Kun etter avtale
%%%R /*/= Vis leveringsinformasjon
%%%S /*/= Splitt en ordre og ta vare pa leveringsinformasjon
%%%T[dager] /*/= Vis tipsrapport for aktuell medarbeider. Dager i minus gir rapport for denne dagen tilbake i tid. Positivt antall dager gir rapport fra sa mange dager og til dagens dato.
%%%U /*/= Viser omsetningsrapport pa skjerm
%%%V[+] /*/= Spor om merket hvis ikke utfylt (viser bordkart dersom definert) + bak betyr spor alltid, selv om angitt fra for. Altsa overskriv.
%%%Y /*/= Importer ordre fra fil<br>Pos 9, 10 lang - Kundenummer<br>Pos 40, 20 lang - Varenummer/Strkkode<br> Pos 60, 10 lang - Antall (kommaseparator som regionale innstillinger pa PC'n)
%%%W[0|1] /*/= Endre "Husk ansatt i kassa"<br>%%%W toggler<br>%%%W0 Skrur den av<br>%%%W1 skrur den pa
%%%X /*/= Vis webshop status
%%%Z /*/= Sla opp lojalitetskort status
%%%2[Egendefinert sporsmalstekst] /*/= Angi eksternt lojalitetskortnummer pa forrige ordre
%%%3[Egendefinert sporsmalstekst] /*/= Sla opp ordrelinjer som er solgt pa eksternt lojalitetskortnummer
%%%4 /*/= Viser viduet for omregningsfaktor dersom varen har det.
%%%5 /*/= Oppfrisk kunde
%%%6<etikett id>[,varenr[,Spor om a repetere[,antall]]] /*/= Skriver vareetikett
%%%7 /*/= Velg lager pa aktiv ordrelinje