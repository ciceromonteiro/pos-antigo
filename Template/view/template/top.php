<?php
$url = $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
if(stripos($_SERVER['SERVER_SIGNATURE'], "443")) {
    $protocol = "https://";
}
else {
    $protocol = "http://";
}
$url_base = $protocol.substr($url, 0, -16);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EasyDrift - Point of Sales</title>

    <style type="text/css">
    .flot {
        left: 0px;
        top: 0px;
        width: 610px;
        height: 250px;
    }
    #flotTip {
        padding: 3px 5px;
        background-color: #000;
        z-index: 100;
        color: #fff;
        opacity: .80;
        filter: alpha(opacity=85);
    }
    .pieLabel div {
        color: white !important;
        text-shadow: 0 0 4px #000;
    }
    @font-face {
	    font-family: 'Roboto Regular';
	    font-style: normal;
	    font-weight: 400;
	    src: url("<?=$url_base?>assets/fonts/Roboto-Regular.woff") format('woff');
	}

	@font-face {
	    font-family: 'Roboto Bold';
	    font-style: normal;
	    font-weight: 700;
	    src: url("<?=$url_base?>assets/fonts/Roboto-Bold.woff") format('woff');
	}

	@font-face {
	    font-family: 'Roboto Italic';
	    font-style: italic;
	    font-weight: 400;
	    src: url("<?=$url_base?>assets/fonts/Roboto-Italic.woff") format('woff');
	}

	@font-face {
	    font-family: 'Source Pro Regular';
	    font-style: normal;
	    font-weight: normal;
	    src: url("<?=$url_base?>assets/fonts/SourceSansPro-Regular.woff") format('woff');
	}
    </style>
    <link href="<?= URL_BASE ?>../assets/css/select2.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= URL_BASE ?>../assets/plugins/font-awesome/css/all.css">

    <link rel="stylesheet" href="<?php echo $url_base.'assets/plugins/Datatables/datatables.css' ?>">
    <link rel="stylesheet" href="<?php echo $url_base.'assets/plugins/Datatables/Select-1.2.6/css/select.bootstrap.css' ?>">

    <link href="<?php echo $url_base.'assets/style.css' ?>" rel="stylesheet">
    <?php if (@$_GET['url'] == 'terminal/terminal/index'):?>
    <link href="<?php echo $url_base.'assets/themes/interface.terminal.css' ?>" rel="stylesheet">
    <?php else:?>
    <link href="<?php echo $url_base.'assets/themes/interface.css' ?>" rel="stylesheet">
    <link href="<?php echo $url_base.'assets/themes/components.css' ?>" rel="stylesheet">
	<?php endif?>

    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/jQuery/jquery-2.2.0.min.js'?>"></script>

    <script>
        if (typeof module === 'object') {
            window.jQuery = window.$ = module.exports;
        };
        var my_url = "<?php echo URL_BASE ?>";
        var my_language = "English";

        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>
    <!--<script type="text/javascript" src="<?php // echo $url_base.'assets/plugins/jQueryUI/external/jquery/jquery.js'?>"></script>-->
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/jQueryUI/jquery-ui.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/Bootstrap/bootstrap.min.js'?>"></script>
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/PNotify/pnotify.custom.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/sweetalert/sweetalert.js' ?>"></script>
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/DatePicker/bootstrap-datepicker.js' ?>"></script>
    <!-- <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/Swiper/horizon-swiper.min.js' ?>"></script> -->
    <!-- <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/PrintJs/print.min.js' ?>"></script> -->
    <!-- <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/doubletap/jquery.doubletap.js' ?>"></script> -->
    <!-- keyboard -->
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/mlkeyboard/jquery.ml-keyboard.min.js' ?>"></script>
    <script type="text/javascript" src="<?php echo  $url_base.'assets/plugins/flot/jquery.flot.js'?>"></script>
    <script type="text/javascript" src="<?php echo  $url_base.'assets/plugins/flot/jquery.flot.pie.js'?>"></script>

    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/loader/loader.min.js' ?>"></script>

    <!-- datatables stuff -->
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/Datatables/datatables.js'?>"></script>
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/Datatables/Select-1.2.6/js/dataTables.select.js'?>"></script>

    <?php if (strtolower($_GET['url']) == 'sales/resume/index'):?>
    <link href="<?php echo $url_base.'assets/js/sales/index.css' ?>" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $url_base.'assets/js/sales/index.js' ?>"></script>
    <?php endif?>

    <script type="text/javascript" DEFER="DEFER">
    Object.size = function(obj) {
	    var size = 0, key;
	    for (key in obj) {
	        if (obj.hasOwnProperty(key)) size++;
	    }
	    return size;
	};

	Number.prototype.formatMoney = function(c, d, t){
	    var n = this, 
	    c = isNaN(c = Math.abs(c)) ? 2 : c, 
	    d = d == undefined ? "." : d, 
	    t = t == undefined ? "," : t, 
	    s = n < 0 ? "-" : "", 
	    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
	    j = (j = i.length) > 3 ? j % 3 : 0;
	   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};
    function b64EncodeUnicode(str) {
        // first we use encodeURIComponent to get percent-encoded UTF-8,
        // then we convert the percent encodings into raw bytes which
        // can be fed into btoa.
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            function toSolidBytes(match, p1) {
                return String.fromCharCode('0x' + p1);
            })
        );
    }

    function verifyLanguage(){
        $.ajax({
            url : my_url+"Setup/additional/verifyLanguage/",
            type: "POST",
            dataType: 'JSON',
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    if(data.data != 'english'){
                        translateSystem();
                    }

                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    }

    function translateSystem(){
    /*  var elements = document.getElementsByClassName('translate'); 
    var i;
    for(i = 0; i<elements.length; i++){
    var string = $('.translate').eq(i).html();
    string = b64EncodeUnicode(string);

    $.ajax({
    url : my_url+"Setup/additional/translate/" + string + "/" + i,
    type: "POST",
    dataType: 'JSON',
    success: function(data, textStatus, jqXHR){
    if (data.result == 'success'){
    $('.translate').eq(data.pos).html(data.data);
    //notification(true);

    } else {
    notification(false);
    console.log(data.message);
    }
    },
    error: function (jqXHR, textStatus, errorThrown){
    notification(false);
    console.log(jqXHR.responseText);
    }
    });
    */      }


    </script>
    <script type="text/javascript">
    function initAutocomplete(value) {
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementsByClassName('autocomplete')[value]));
    }

    function completeFields(value) {
        var place = null;
        var country = null;
        var city = null;
        var zip_code = null;
        var initials = null;
        var state = null;
        var district = null;
        var number = null;
        var street = null;
        string = document.getElementsByClassName('autocomplete')[value].value;

        if (string.match(/ - /)) {
            string = string.split(", ");
            country = string[string.length - 1];

            if (string[string.length - 2] != undefined && string[string.length - 2] != null) {
                penulPos_string = string[string.length - 2];
                if (penulPos_string.length == 2) {
                    initials = penulPos_string;
                }
                else {
                    if (penulPos_string.match(/ - /)) {
                        penulPos_string = penulPos_string.split(" - ");
                        city = penulPos_string[0];
                        penulPos_string_fim = penulPos_string[1];
                        if (penulPos_string_fim == 2) {
                            initials = penulPos_string_fim;
                        }
                        else {
                            penulPos_string_fim = penulPos_string_fim.split(" ");
                            initials = penulPos_string_fim[0];
                            if (penulPos_string_fim[1] != undefined && penulPos_string_fim[1] != null) {
                                zip_code = penulPos_string_fim[1];
                            }
                        }
                    }
                    else {
                        city = penulPos_string;
                    }
                }
            }

            if (string[string.length - 3] != undefined && string[string.length - 3] != null) {
                antepenulPos_string = string[string.length - 3];
                split_antepenult = antepenulPos_string.split(" ");
                for (i = 0; i < split_antepenult.length - 1; i++) {
                    if (!isNaN(split_antepenult[i])) {
                        number = split_antepenult[i];
                    }
                }

                if (antepenulPos_string.match(/ - /)) {
                    antepenulPos_string = antepenulPos_string.split(" - ");

                    if (antepenulPos_string.length == 2) {
                        if (isNaN(antepenulPos_string[0])) {
                            street = antepenulPos_string[0];
                        }

                        district = antepenulPos_string[1];
                    }
                    else if (antepenulPos_string.length == 3) {
                        place = antepenulPos_string[0];
                        if (isNaN(antepenulPos_string[1])) {
                            street = antepenulPos_string[1];
                        }

                        district = antepenulPos_string[2];
                    }

                }
                else {
                    district = antepenulPos_string;
                }
            }

            if (string[string.length - 4] != undefined && string[string.length - 4] != null) {
                street = string[string.length - 4];

            }
        }
        else {
            string = string.split(", ");
            country = string[string.length - 1];
            if (string[string.length - 2] != undefined && string[string.length - 2] != null) {
                penulPos_string = string[string.length - 2];
                if (penulPos_string.length == 2) {
                    initials = penulPos_string;
                }
                else {
                    if (penulPos_string.match(/ /)) {
                        split_penulPos_string = penulPos_string.split(" ");
                        for (i = 0; i < split_penulPos_string.length; i++) {
                            if (!isNaN(split_penulPos_string[i])) {
                                if (zip_code == null) {
                                    zip_code = split_penulPos_string[i];
                                }
                                else {
                                    state = state.concat(" ", split_penulPos_string[i]);
                                }

                            }
                            else {
                                if (state == null) {
                                    state = split_penulPos_string[i];
                                }
                                else {
                                    state = state.concat(" ", split_penulPos_string[i]);
                                }
                            }
                        }
                    }
                    else {

                        state = penulPos_string;
                    }


                }
            }

            if (string[string.length - 3] != undefined && string[string.length - 3] != null) {
                city = string[string.length - 3];
            }

            if (string[string.length - 4] != undefined && string[string.length - 4] != null) {
                inic_string = string[string.length - 4].split(" ");

                for (i = 0; i < inic_string.length; i++) {
                    if (!isNaN(inic_string[i])) {
                        if (number == null) {
                            number = inic_string[i];
                        }
                        else {
                            street = street.concat(" ", inic_string[i]);
                        }
                    }
                    else {
                        if (street == null) {
                            street = inic_string[i];
                        }
                        else {
                            street = street.concat(" ", inic_string[i]);
                        }
                    }
                }
            }

            if (string[string.length - 5] != undefined && string[string.length - 5] != null) {
                place = string[string.length - 5];
            }

        }
        document.getElementsByClassName('place')[value].value = place;
        document.getElementsByClassName('street')[value].value = street; //street or place
        document.getElementsByClassName('number')[value].value = number;
        document.getElementsByClassName('district')[value].value = district;
        document.getElementsByClassName('city')[value].value = city;
        document.getElementsByClassName('initials')[value].value = initials;
        document.getElementsByClassName('zip_code')[value].value = zip_code;
        document.getElementsByClassName('state')[value].value = state;
        document.getElementsByClassName('country')[value].value = country;
    }
    </script>
<style>
/*.btn {
    font-size: 12px;
}
.btn p {
    padding: 0px;
    margin: 0px;
}*/
.nav-tabs a{
    color:#333 !important;
}
.billet_line{
    padding-top: 5px;
}
.pos_element{
    position: absolute;
}
/*.pos_table table, th, td {
    border: 1px solid gray;
}
.pos_table{
    margin-top:5px !important;
}*/

#choose_customer{
    display: none;
}
#pay_order{
    display: none;
}
.btn-store {
    margin: 5px;
    padding: 15px;
}
.others_btn {
    margin-top: 30px
}

/*.insert_barcode{
    height: 40px;
    margin-top:25px;
}*/

.edit_qty, .edit_price, .edit_discount, .edit_final_price, .edit_info{
    display: none;
}

.edit_product_line_attribute{
    width: 60px;
}

.dropHere {
    max-width: 100%;   
}
</style>
    
</head>
<body onload="">
    <div class="loader"></div>
   <!-- <div class="navbar-fixed-top text-center" style="background-color: red; color: white"><t class="translate">Warning, system in development</t></div>-->
