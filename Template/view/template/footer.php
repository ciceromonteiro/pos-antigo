                    </div>
                </div>
            </div>
        </div>
    </div> <!-- wrapper -->
    
    <script type="text/javascript">        
        $('.date').datepicker({
            todayBtn: "linked",
            clearBtn: true,
            language: "pt-BR",
            autoclose: true,
            toggleActive: true,
            format: 'dd/mm/yyyy'
        });
    </script>
    <script type="text/javascript" src="<?php echo $url_base.'assets/plugins/jQueryMask/jquery.mask.min.js'?>"></script>
    <?php
        if(isset($menu_top)):
            if($menu_top != "addDevice"):
    ?>
    <script type="text/javascript" src="<?php echo $url_base.'assets/js/ui.js' ?>"></script>
    <script type="text/javascript" src="<?php echo $url_base.'assets/js/page-builder.js' ?>"></script>
    <script type="text/javascript" src="<?php echo $url_base.'assets/js/select2.js' ?>"></script>
    <?php 
        endif; 
    endif; 
    ?>

    <script type="text/javascript">
         $(".select2").select2();
    </script>