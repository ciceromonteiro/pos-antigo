<nav class="navbar navbar-fixed-top navbar-inverse navbar-top" style="margin-top: 0px; margin-bottom: 0px">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  aria-expanded="false" aria-controls="navbar" onclick="window.location.href=`<?php echo URL_BASE.'Application/Authentication/logout/' ?>`;">
                <span class="lnr lnr-exit" style="color: #fff"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#dropdown-menu" aria-expanded="false" aria-controls="navbar" onclick="window.location.href=`#`;">
                <span class="lnr lnr-bookmark" style="color: #fff"></span>
            </button>
             
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  aria-expanded="false" aria-controls="navbar" onclick="window.location.href=`#`;">
                <span class="lnr lnr-download" style="color: #fff"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <a class="navbar-brand" href="<?php echo URL_BASE . 'Dashboard/home/index' ?>">
                <img src="<?php echo $url_base . 'assets/images/logo-02.png'; ?>" class="text-center">
            </a>
        </div>
        <!--
            <form class="navbar-form navbar-left">
                <div class="row">
                    <div class="inner-addon right-addon">
                        <span class="lnr lnr-magnifier"></span>
                        <input type="text" class="form-control" placeholder="Search...">
                    </div>
                </div>
            </form>
            --> 
                <div id="navbar" class="collapse navbar-collapse">
            <ul class="notifications navbar-right">
                <li>
                    <a href="#">
                        <span class="lnr lnr-download"></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="lnr lnr-bookmark"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">No notification</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo URL_BASE.'Application/Authentication/logout/' ?>">
                        <span class="lnr lnr-exit"></span>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right navbar-modules" style="margin-right: 20px">
                <li <?php echo($menu_top != "Terminal" && $menu_top != "Delivery") ? " class='active'" : "" ?>>
                    <a href="<?php echo URL_BASE . 'Dashboard/home/index' ?>"><t class="translate">Admin</t></a>
                </li>
                <li <?php echo($menu_top == "Terminal") ? " class='active'" : "" ?>>
                    <a href="<?php echo URL_BASE . 'terminal/terminal/index' ?>" style="border-radius: 0px">POS</a>
                </li>
                <!-- <li <?php// echo($menu_top == "Delivery") ? " class='active'" : "" ?>>
                    <a href="<?php// echo URL_BASE . 'Delivery/delivery/index' ?>" style="color:red; border-top-right-radius: 8px; border-top-left-radius: 0px"><t class="translate">Delivery</t></a>
                </li> -->
                <!--
                    <li style="border-right: none;"><a style="cursor: pointer;" id="editionMode">Edition Mode</a></li>
                    <li style="border-right: none;"><a style="display: none; cursor: pointer;" id="saveEdition">Save Edition</a></li>
                -->
            </ul>
        </div>
    </div><!-- /.container -->
</nav><!-- /.navbar -->