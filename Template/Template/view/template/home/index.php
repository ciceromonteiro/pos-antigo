<?php
    $options = "";
    foreach ($users as $value) {
        $options .= "<option value='".$value->getIdusers()."'>".$value->getLogin()."</option>";
    }
?>

<style>
    img {
        padding-bottom: 30px
    }
    .login, .footer {
        color: #fff;
    }
    .btn-group {
        margin-top: 20px;
    }
    .home {
        background-image: url("<?php echo str_replace('public/', '', URL_BASE).'/assets/images/home-background.png' ?>");
        background-size: cover;
        background-position: left top;
        padding: 50px 50px 10px 50px;
        border: 2px solid #ccc
    }
    .footer {
        color: #000;
        margin-top: 120px;
        margin-bottom: 0px;
    }
</style>

<div class="container home" style="margin-top: 100px;">
    <div class="row">
        <div class="col-md-6">
            <div class="login">
                <img src="<?php echo str_replace('public/', '', URL_BASE).'/assets/images/logo-01.png' ?>" width="180">
                <!-- <p>Company</p>
                    <select id="company_login">
                        <option value="">Select your company</option>
                        <option value="1">Test</option>
                    </select>
                -->
                <br>
                <div class="btn-group" role="group">
                    <button id="btn_admin" class="btn btn-success translate" onclick="openModal(this.value)" value="0">Admin</button>
                    <button id="btn_pos" class="btn btn-success translate" onclick="openModal(this.value)" value="1">POS</button>
                    <button id="btn_cashierClosing" class="btn btn-success translate" onclick="openModal(this.value)" value="2">Balancing the cash</button>
                    <button id="btn_delivery" class="btn btn-success translate" onclick="openModal(this.value)" value="3">Delivery</button>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="footer">
                <div class="col-md-4">
                    <p><br><br>
                        
                        <t class="translate">Store data is provided by</t> Easy Step AS<br>
                        <a href="#">www.easy-step.no</a><br>
                        Brukerstottle. 72 60 60 60 - <a href="#">support@easy-step.no</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Users -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">User</h4>
            </div>
            <form id="form-login">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="redirect-to" id="redirect-to" style="display: none">
                            <label for="user" class="label-style translate">Select your user:</label>
                            <select name="form-login-user" id="user">
                                <?php echo $options; ?>
                            </select>
                            <label id="label-password" for="password-user" class="label-style translate">Password:</label>
                            <input type="password" name="form-login-password" id="password-user" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Enter</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    var url_base = "<?php echo URL_BASE ?>";
    var url_redirect = "";
    var array_link = new Array(
        url_base + "dashboard/home/index",
        url_base + "terminal/terminal/index",
        url_base + "sales/cashierClosing/index",
        url_base + "Delivery/delivery/index"
    );

    function openModal(value){
        url_redirect = array_link[value];
        $('#myModal').modal({show : true});
    }

    $(document).on('submit', '#form-login', function(e){
        e.preventDefault();
         $.ajax({
             url : my_url+"Application/Authentication/login",
             type: "POST",
             dataType: 'JSON',
             data : $('#form-login').serialize(),
             success: function(data, textStatus, jqXHR){
                 if (data.result == 'success'){
                     $('#myModal').modal('hide');
                     window.location = url_redirect;
                 } else {
                     alert('Your login or password is incorrect, try again');
                     console.log(data.message);
                 }
             },
             error: function (jqXHR, textStatus, errorThrown){          
                 alert('Unable to connect, please contact support or try again.');
                 console.log(jqXHR.responseText);
             }
         });
    });
</script>