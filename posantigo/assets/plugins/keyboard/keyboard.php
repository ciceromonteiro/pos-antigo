<?php
    $url = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    if(stripos($_SERVER['SERVER_SIGNATURE'], "443")) {
        $protocol = "https://";
    } else {
        $protocol = "http://";
    }

    $url_base = $protocol.substr($url, 0, -16);
    define("URL_BASE", $url_base . "public/");
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/jsKeyboard.css" type="text/css" media="screen"/>

<script src="js/vendor/jquery-1.9.1.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jsKeyboard.js"></script>
<script src="js/main.js"></script>
<script src="js/bsa_script.js"></script>

<script type="text/javascript">
    (function(){
        var bsa = document.createElement('script');
           	bsa.type = 'text/javascript';
           	bsa.async = true;
           	bsa.src = 'bsa.js';
        (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);
    })();

    function onClickDiv(){
    	var obj = {text: document.getElementById('text').value};
    	document.getElementById('pa').innerHTML = obj.text;
    }
    
    var my_url = "<?php echo URL_BASE ?>";
    
    $(document).on('click', '#virtualKeyboard', function(e){
    e.preventDefault();
        $.ajax({
            url : 'http://localhost/app/public/Dashboard/home/keyboardWrite/' + btoa($('#text').val()),
            type: "POST",
            dataType: 'JSON',
            //data : 'text=' + $('#text').val(),
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    console.log(data.message);
                } else {
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                console.log(jqXHR.responseText);
            }
        });
    });

</script>

<div style="width: 50%; margin: 0 auto" >
    <input id="text" type="text" name="text" style="height: 5%; width: 100%; margin: 0 auto;" >
</div>

<div style="border:1px solid black" id="virtualKeyboard"></div>

<p id="pa" ></p>