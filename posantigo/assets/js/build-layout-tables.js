/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    var counts = [0];
    var resizeOpts = {
        handles: "all", autoHide: true
    };

    $(".tables-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });

    $(".section-widget").draggable({
        helper: "clone",
        snap: false, 
        cursor: "move", 
        cursorAt: { top: 56, left: 56 },
        //Create counter
        start: function () {
            counts[0]++;
        }
    });

    $("#dropHere").droppable({
        drop: function (event, ui) {
            
            if (ui.draggable.hasClass("tables-widget")) {
                $(this).append('<div \n\
                class="ui-draggable-handle item-'+counts[0]+' imgSize-'+counts[0]+' ui-draggable ui-resizable text-center" \n\
                style="width:92px; height:43px" data-edit="false" data-item-id="'+counts[0]+'">\n\
                <img src="../../../assets/images/table.svg">\n\
                <p style="margin: 0px"><b>iD:</b> '+counts[0]+'<br>\n\
                <b>Seats:</b> <span>0</span></p>\n\
                </div>');
                
                $("#dropHere .item-" + counts[0]).removeClass("tables-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $('#tablesModal').modal({show : true});
                    $('#tablesModal #idElemTable').val(counts[0]);
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    snap: true,
                    containment: "#dropHere",
                    minHeight: 42,
                    minWidth: 50
                });
            }

            if (ui.draggable.hasClass("section-widget")) {
                $(this).append("<div id='section-widget' style='width: 150px; height:150px; background-color: #ccc;' \n\
                class='image-widget bordered-ui ui-draggable ui-draggable-dragging item-"+counts[0]+" imgSize-"+counts[0]+"' data-itam-id='"+counts[0]+"'>\n\
                <p class='text-center' style='font-size: 16px;padding-top: 10px;'>Section</p></div>");

                $("#dropHere .item-" + counts[0]).removeClass("section-widget ui-draggable ui-draggable-dragging");

                //remeve o item
                $(".item-" + counts[0]).dblclick(function () {
                    $('#sectionModal').modal({show : true});
                    $('#sectionModal #idElemSection').val(counts[0]);
                });

                //adiciona a opção de arrastar ao item
                make_draggable($(".item-" + counts[0]));
                $(".imgSize-" + counts[0]).resizable({
                    containment: "#dropHere",
                    minHeight: 150,
                    minWidth: 150
                });
            }
        }
    });

    var zIndex = 0;
    function make_draggable(elements){
        elements.draggable({
            containment: 'parent',
            start: function (e, ui) {
                ui.helper.css('z-index', ++zIndex);
            },
            stop: function (e, ui) {
            }
        });
    }

});