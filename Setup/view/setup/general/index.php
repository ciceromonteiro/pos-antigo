<?php
    $options = "<option value='0'>Select option</option>";
    
    foreach ($array_answer["taxes"] as $value){
        $options .= '<option value="'.$value->getIdtax().'">'.$value->getName().'</option>';
    }
    
    include __DIR__."/../nav/menu.php"; 
?>

<script type="text/javascript">
    $(document).ready(function () {
        
        function getData() {
            $.ajax({
                url: my_url + "Setup/General/getGeneralSettings/",
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        var values = data.data[0];
                        for (var prop in values) {
                            if (prop == "inv_default_value" || prop == "inv_type_tax" || prop == "inv_minimum_value_for_tax_free") {
                                //selects
                                document.getElementById(prop).value = values[prop];
                            } else {
                                //checkboxes
                                if (values[prop] == "on" || values[prop] == "1") {
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }

        $(document).on('submit', '#form_general_settings', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Setup/general/update/",
                type: "POST",
                dataType: 'JSON',
                data: $('#form_general_settings').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        document.getElementById("form_general_settings").reset();
        getData();
    });
</script>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12"> 
            <div class="container-fluid">
                <form id="form_general_settings" name="form_general_settings">
                    <div class="row-fluid">
                        <div class="col-md-4">
                            <h4 class="translate">Invoices</h4>
                            <div class="form-group">
                                <label for="inv_default_value" class="label-style translate">Default Value</label>
                                <input type="text" class="form-control" name="inv_default_value" id="inv_default_value">
                            </div>
                            <div class="form-group">
                                <label for="inv_type_tax" class="label-style translate">Type of Tax</label>
                                <select name="inv_type_tax" id="inv_type_tax">
                                    <?php echo $options ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inv_minimum_value_for_tax_free" class="label-style translate">Minimum Value For Tax Free</label>
                                <input type="text" class="form-control" name="inv_minimum_value_for_tax_free" id="inv_minimum_value_for_tax_free">
                            </div>
                            <div class="form-group">
                                <p>
                                    <input type="checkbox" name="inv_always_collect_tax_on_inv" id="inv_always_collect_tax_on_inv">
                                    <t class="translate">Always collect tax on invoice</t>
                                </p>                   
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h4 class="translate">Other Informations</h4>
                            <div class="form-group">                    
                                <input type="checkbox" name="oth_impt_only_product_name_acquired" id="oth_impt_only_product_name_acquired">
                                <t class="translate">Import only product name acquired from providers (Don't import amount).</t>                    
                            </div>           
                            <div class="form-group">                    
                                <input type="checkbox" name="oth_print_list_of_all_sold" id="oth_print_list_of_all_sold">
                                <t class="translate">Print list of all sold products after cash register closure.</t>                    
                            </div>                                           
                        </div>
                        <div class="col-md-4">
                            <h4 class="translate">Purchase order</h4>
                            <div class="form-group">                    
                                <input type="checkbox" name="pur_when_customer_is_change" id="pur_when_customer_is_change">
                                <t class="translate">When customer is change, request confirmation before recalculate the values.</t>                    
                            </div>           
                            <div class="form-group">
                                <input type="checkbox" name="pur_send_message_to_customer" id="pur_send_message_to_customer">
                                <t class="translate">Send message to customer when there is change on object, project or session.</t>                    
                            </div> 
                            <div class="form-group">                    
                                <input type="checkbox" name="pur_apply_default_discount" id="pur_apply_default_discount">
                                <t class="translate">Apply default discounts of products to customers paid with invoices.</t>             
                            </div> 
                            <div class="form-group">                    
                                <input type="checkbox" name="pur_automatically_put_in_all_docs" id="pur_automatically_put_in_all_docs">
                                <t class="translate">Automatically put in all docs the date of delivery items like date of purchase.</t>                    
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 navbar-bottom">
                            <div class="pull-right">                    
                                <button type="submit" class="btn btn-success translate">Save</button>
                            </div>
                        </div>
                    </div>        
                </form>
            </div>
        </div>
    </div>
</div>