<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#creditCards').DataTable( {
            "ajax": {"url": "<?php echo URL_BASE ?>Setup/Accounting/getAllCreditCards/"},
            /*"dom": 'frtip',*/
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "bbsid" },
                { "data": "name" },
                { "data": "accounting" },
                { "data": "showBalancing" },
                { "data": "sendRegister" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "ordering": false,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });

        function reload_table(){
            table.ajax.reload(null,false);
        }
        
        $(document).on('click', '#create_creditCards', function(e){
            e.preventDefault();
            $('#creditCards_insert_modal').modal({
                show : true
            });
        });

        // Select tr
        $("#creditCards tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ( $(this).hasClass('hover-select') ) {
                $('#update_creditCards').attr('disabled', true);
                $('#delete_creditCards').attr('disabled', true);
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            }
            else {
                $('#update_creditCards').attr('disabled', false);
                $('#delete_creditCards').attr('disabled', false);
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
        } );

        // Add submit form
        $(document).on('submit', '#form_insert_credit_card', function(e){
            e.preventDefault();
            var form_data = $('#form_insert_credit_card').serialize();
            var request   = $.ajax({
                url:          '<?php echo URL_BASE ?>Setup/Accounting/insertCreditCard',
                cache:        false,
                data:         form_data,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            request.done(function(output){
                if (output.result == 'success'){
                    var title = "Added successfully";
                    var text = "Added successfully";
                    var icon = "glyphicon glyphicon-plus";
                    var type = "success";
                    reload_table();
                    notification(title, text, icon, type);
                } else {
                    var title = "Add request failed";
                    var text = "Add request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                }
            });
            request.fail(function(jqXHR, textStatus){
                var title = "Add request failed";
                var text = textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
                console.log(jqXHR.responseText);
            });
            $('#creditCards_insert_modal').modal('hide');
        });

        // Get for update
        $(document).on('click', '#update_creditCards', function(e){
            e.preventDefault();
            var id      = table.$('tr.hover-select').find('input:checkbox').data('id');
            var request = $.ajax({
                url:          '<?php echo URL_BASE ?>Setup/Accounting/getCreditCard',
                cache:        false,
                data:         'id=' + id,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            request.done(function(output){
                if (output.result == 'success'){
                    document.getElementById("form_update_credit_card").reset();
                    $('#creditCards_edit_modal').modal({show : true});
                    $('#form_update_credit_card #idcreditCard').val(output.data[0].id);
                    $('#form_update_credit_card #BBSID').val(output.data[0].bbsid);
                    $('#form_update_credit_card #name').val(output.data[0].name);
                    $('#form_update_credit_card #description').val(output.data[0].description);
                    $('#form_update_credit_card #accounting').val(output.data[0].accounting);
                    if(output.data[0].showBalancing == 1){
                        $("#form_update_credit_card #showBalancing").attr("checked", true);
                    }
                    if(output.data[0].sendRegister == 1){
                        $("#form_update_credit_card #sendRegister").attr("checked", true);
                    }
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                }
            });
            request.fail(function(jqXHR, textStatus){
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
                console.log(jqXHR.responseText);
            });
        });

        // Edit submit form
        $(document).on('submit', '#form_update_credit_card', function(e){
            e.preventDefault();
            var id        = $('#form_update_credit_card #idcreditCard').val();
            var form_data = $('#form_update_credit_card').serialize();
            var request   = $.ajax({
                url:          '<?php echo URL_BASE ?>Setup/Accounting/updateCreditCard&id=' + id,
                cache:        false,
                data:         form_data,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            request.done(function(output){
                if (output.result == 'success'){
                    var title = "Added successfully";
                    var text = "Edited successfully";
                    var icon = "glyphicon glyphicon-plus";
                    var type = "success";
                    $('#creditCards_edit_modal').modal('hide');
                    reload_table();
                    notification(title, text, icon, type);
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                }
            });
            request.fail(function(jqXHR, textStatus){
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
                console.log(jqXHR.responseText);
            });
        });

        // Delete
        $(document).on('click', '#delete_creditCards', function(e){
            e.preventDefault();
            confirmDelete(this);
        });
        
        /* Delete Modal */
        function confirmDelete(btnDelete){        
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        

            var i=0, ids = [];

            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var arrayDelete = {idCreditCards : ids};
            var title = "Confirmation Needed";
            var text  = "Are you sure you want to delete?";
            var icon = "glyphicon glyphicon-minus";

            deleteItems(title, text, icon, arrayDelete);
        }
        
        // Delete action
        function deleteItems(title, text, icon, arrayDelete){
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function(){
                var request = $.ajax({
                    url:          '<?php echo URL_BASE ?>Setup/Accounting/deleteCreditCard',
                    cache:        false,
                    data:         arrayDelete,
                    dataType:     'json',
                    contentType:  'application/json; charset=utf-8',
                    type:         'get'
                });
                request.done(function(output){
                    if (output.result == 'success'){
                        var title = "Deleted successfully";
                        var text = "Deleted successfully";
                        var icon = "glyphicon glyphicon-plus";
                        var type = "success";
                        reload_table();
                        notification(title, text, icon, type);
                    } else {
                        var title = "Delete request failed";
                        var text = "Delete request failed";
                        var icon = "glyphicon glyphicon-minus";
                        var type = "error";
                        reload_table();
                        notification(title, text, icon, type);
                    }
                    $('.ui-pnotify-modal-overlay').remove();
                });
                request.fail(function(jqXHR, textStatus){
                    var title = "Delete request failed";
                    var text = textStatus;
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                    console.log(jqXHR.responseText);
                });
            }).on('pnotify.cancel', function(){
                $('.ui-pnotify-modal-overlay').remove();
            });
        }
        
    });
</script>

<style>
    .modal-dialog-first {
        width: 800px;
    }
</style>

<!-- Table -->
<div class="modal fade" id="creditcards_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-first" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Credit cards</h4>
            </div>
            <div class="row row-fluid">
                <div class="col-md-12">
                    <div class="top-bar-buttons">
                        <div class="button-group">
                            <button id="create_creditCards" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> New</button>
                            <button id="update_creditCards" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> Edit</button>
                            <button id="delete_creditCards" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> Remove</button>
                        </div>
                    </div>
                    <table id="creditCards" class="table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                                <th style="width: 80px">ID</th>
                                <th style="width: 80px">BBSID</th>
                                <th>Name</th>
                                <th>Accounting</th>
                                <th>Show balancing cash</th>
                                <th>Send money to account</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Insert -->
<div class="modal fade" id="creditCards_insert_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Insert Credit Card</h4>
            </div>
            <form id="form_insert_credit_card" name="form_insert_credit_card">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="BBSID" class="label-style">BBSID</label>
                            <input type="text" class="form-control" id="BBSID" name="BBSID">
                        </div>
                        <div class="col-md-4">
                            <label for="name" class="label-style">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="col-md-4">
                            <label for="description" class="label-style">Description</label>
                            <input type="text" class="form-control" id="description" name="description">
                        </div>
                        <div class="col-md-4">
                            <label for="accounting" class="label-style">Accounting</label>
                            <input type="text" class="form-control" id="accounting" name="accounting">
                        </div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-md-6">
                            <p><input type="checkbox" name="showBalancing" id="showBalancing"> Send money record to the specified account</p>
                        </div>
                        <div class="col-md-6">
                            <p><input type="checkbox" name="sendRegister" id="sendRegister"> Show at box closure</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="creditCards_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Update Credit Card</h4>
            </div>
            <form id="form_update_credit_card" name="form_update_credit_card">
                <div class="modal-body">
                    <div class="row">
                        <input type="text" class="form-control" id="idcreditCard" name="idcreditCard" style="display: none">
                        <div class="col-md-4">
                            <label for="BBSID" class="label-style">BBSID</label>
                            <input type="text" class="form-control" id="BBSID" name="BBSID">
                        </div>
                        <div class="col-md-4">
                            <label for="name" class="label-style">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="col-md-4">
                            <label for="description" class="label-style">Description</label>
                            <input type="text" class="form-control" id="description" name="description">
                        </div>
                        <div class="col-md-4">
                            <label for="accounting" class="label-style">Accounting</label>
                            <input type="text" class="form-control" id="accounting" name="accounting">
                        </div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-md-6">
                            <p><input type="checkbox" name="showBalancing" id="showBalancing"> Send money record to the specified account</p>
                        </div>
                        <div class="col-md-6">
                            <p><input type="checkbox" name="sendRegister" id="sendRegister"> Show at box closure</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var checkboxAll = $("#creditCards thead tr").find('input:checkbox');
    checkboxAll[0].checked = false;
    $('#update_creditCards').attr('disabled', true);
    $('#delete_creditCards').attr('disabled', true);

    $('#creditCards thead').on('click', '#all-checkbox', function () {        
        var idTable = "creditCards";
        var i;
        var checkboxArray = $("#"+idTable+" tbody tr").find('input:checkbox');
        var isChecked = $("#"+idTable+" thead tr").find('input:checkbox');

        if(isChecked[0].checked == false){
            for(i = 0; i < checkboxArray.length; i++){
                $("#"+idTable+" tbody").find('tr').removeClass('hover-select');
                checkboxArray[i].checked = false;
            }

            $('#update_creditCards').attr('disabled', true);
            $('#delete_creditCards').attr('disabled', true);            
        } else {
            for(i = 0; i < checkboxArray.length; i++){
                $("#"+idTable+" tbody").find('tr').addClass('hover-select');
                checkboxArray[i].checked = true;
            }


            $('#delete_creditCards').attr('disabled', false);
        }
    });
</script>