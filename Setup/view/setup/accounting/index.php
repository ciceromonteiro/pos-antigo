<?php
$options_software_accounting = '<option value="0">Select Option</option>';
$options_credit_management = '<option value="0">Select Option</option>';
$options_customer = '<option value="0">Select Option</option>';

foreach ($array_answer['software_accounting'] as $value) {
    $options_software_accounting .= "<option value='".$value->getIdsoftwareaccounting()."'>".$value->getName()."</option>";
}

foreach ($array_answer['credit_management'] as $value) {
    $options_credit_management .= "<option value='".$value->getIdcreditmanegement()."'>".$value->getName()."</option>";
}

foreach ($array_answer['customers'] as $value) {
    $options_customer .= "<option value='".$value->getIdperson()."'>".$value->getName()."</option>";
}

include __DIR__ . "/../nav/menu.php"; 
?>

<script type="text/javascript">
    $(document).ready(function () {
        
        function getData() {
            $.ajax({
                url: my_url + "Setup/Accounting/getAccounting/",
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        var values = data.data[0];
                        for (var prop in values) {
                            document.getElementById(prop).value = values[prop];
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }

        $(document).on('submit', '#form_accounting_settings', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Setup/accounting/update/",
                type: "POST",
                dataType: 'JSON',
                data: $('#form_accounting_settings').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('click', '#creditcards', function(e){
            e.preventDefault();
            $('#creditcards_modal').modal({
                show : true
            });
        });

        document.getElementById("form_accounting_settings").reset();
        getData();
    });
</script>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12"> 
            
        <div class="container-fluid">    
            <form id="form_accounting_settings">
                <div class="row-fluid">
                    <div class="col-md-4">
                        <h4 class="translate">Informations</h4>
                        <div class="form-group">
                            <div class="row">
                                <label for="info_cash_acc" class="control-label col-md-6 translate">Cash acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_cash_acc" id="info_cash_acc">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="info_cash_withdrawl_acc" class="control-label col-md-6 translate">Cash withdrawal acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_cash_withdrawl_acc" id="info_cash_withdrawl_acc">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="info_rounding_acc" class="control-label col-md-6 translate">Rounding acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_rounding_acc" id="info_rounding_acc">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="info_credit_note_acc" class="control-label col-md-6 translate">Credit note acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_credit_note_acc" id="info_credit_note_acc">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="info_gift_voucher_acc" class="control-label col-md-6 translate">Gift voucher acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_gift_voucher_acc" id="info_gift_voucher_acc">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="info_overages" class="control-label col-md-6 translate">Overages/Shortages acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_overages" id="info_overages">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="info_credit_card_acc" class="control-label col-md-6 translate">Credit Card acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_credit_card_acc" id="info_credit_card_acc">
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="row">
                                <label for="info_payment_automatic_account" class="control-label col-md-6 translate">Payment Automatic account</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_payment_automatic_account" id="info_payment_automatic_account">
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="row">
                                <label for="info_hotel_booking_account" class="control-label col-md-6 translate">Hotel Booking Account</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_hotel_booking_account" id="info_hotel_booking_account">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label for="info_free_acc" class="control-label col-md-6 translate">Invoice Free Acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="info_free_acc" id="info_free_acc">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <button style="color: red" id="creditcards" type="button" class="btn btn-primary translate">Card Accounts/Type of Operations</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4 class="translate">Checkout Configurations</h4>
                        <div class="form-group">
                            <div class="row">
                                <label for="checkout_default_customer" class="control-label col-md-6 translate">Default Customer</label>
                                <div class="col-md-12">
                                    <select name="checkout_default_customer" id="checkout_default_customer">
                                        <?php echo $options_customer ?>
                                    </select>
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="checkout_starting_petty_cash" class="control-label col-md-6 translate">Starting Petty cash</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="checkout_starting_petty_cash" id="checkout_starting_petty_cash">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="checkout_current_cash_on_hand" class="control-label col-md-6 translate">Current cash-on-hand</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="checkout_current_cash_on_hand" id="checkout_current_cash_on_hand">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="checkout_divergence" class="control-label col-md-6 translate">Divergence Alert Limit</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="checkout_divergence" id="checkout_divergence">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="checkout_gift_voucher" class="control-label col-md-6 translate">Gift voucher acc</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="checkout_gift_voucher" id="checkout_gift_voucher">
                                </div>
                            </div>
                        </div> 
                        
                        <h4 style="color: red" class="translate">Default Report to software accounting</h4>

                        <div class="form-group">                    
                            <select style="color: red" name="credit_manag_default_report_software_id" id="credit_manag_default_report_software_id">
                                <?php echo $options_software_accounting ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label style="color: red" for="credit_manag_email_of_contact" class="control-label col-md-6 translate">Email of Contact</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="credit_manag_email_of_contact" id="credit_manag_email_of_contact">
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="row">
                                <label style="color: red" for="credit_manag_id_customer" class="control-label col-md-6 translate">ID Customer</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="credit_manag_id_customer" id="credit_manag_id_customer">
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="row">
                                <label style="color: red" for="credit_manag_id_invoice" class="control-label col-md-6 translate">ID Invoice</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="credit_manag_id_invoice" id="credit_manag_id_invoice">
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="row">
                                <label style="color: red" for="credit_manag_id_credit_note" class="control-label col-md-6 translate">ID Credit Note</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="credit_manag_id_credit_note" id="credit_manag_id_credit_note">
                                </div>
                            </div>
                        </div>                 
                    </div>
                    <div class="col-md-4"> 
                        <h4 class="translate">Initial Parameters</h4>
                        <div class="form-group">
                            <div class="row">
                                <label for="init_param_number_of_first_inv" class="control-label col-md-6 translate">Number of first Invoice</label>
                                <div class="col-md-6">
                                    <input type="number" min="1" class="form-control" name="init_param_number_of_first_inv" id="init_param_number_of_first_inv">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="init_param_number_of_first_log_of_cash" class="control-label col-md-6 translate">Number of first log of cash register</label>
                                <div class="col-md-6">
                                    <input type="number" min="1" class="form-control" name="init_param_number_of_first_log_of_cash" id="init_param_number_of_first_log_of_cash">
                                </div>
                            </div>
                        </div>    
                        <div class="form-group">
                            <div class="row">
                                <label for="init_param_number_of_first_report_acc" class="control-label col-md-6 translate">Number of first Report Accounting</label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="init_param_number_of_first_report_acc" id="init_param_number_of_first_report_acc">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="init_param_number_of_first_report_inv" class="control-label col-md-6 translate">Number of first Report Invoice</label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="init_param_number_of_first_report_inv" id="init_param_number_of_first_report_inv">
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <label for="init_param_number_of_first_report_sales" class="control-label col-md-6 translate">Number of first Report Sales</label>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" name="init_param_number_of_first_report_sales" id="init_param_number_of_first_report_sales">
                                </div>
                            </div>
                        </div>    

                        <h4 style="color: red" class="translate">Credit Management</h4>
                        <div class="form-group">                    
                            <select style="color: red" name="credit_management_id" id="credit_management_id">
                                <?php echo $options_credit_management ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label style="color: red" for="credit_management_email_or_idaccount" class="control-label col-md-6 translate">Email or ID</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="credit_management_email_or_idaccount" id="credit_management_email_or_idaccount">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label style="color: red" for="credit_management_user" class="control-label col-md-6 translate">User</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="credit_management_user" id="credit_management_user">
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <div class="row">
                                <label style="color: red" for="credit_management_password" class="control-label col-md-6 translate">Password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="credit_management_password" id="credit_management_password">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 navbar-bottom">
                        <div class="pull-left"></div>
                        <div class="pull-right">                    
                            <button type="submit" class="btn btn-success translate">Save</button>
                            <button type="button" class="btn btn-primary translate">Cancel</button>
                        </div>
                    </div>
                </div>        
            </form>
        </div>
            
        </div>
    </div>
</div>

<?php 
    include "creditcards.php";
?>