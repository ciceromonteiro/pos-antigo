<?php
/**
 * Description of index_additional
 *
 * @abstract file index for view Additional
 * 
 * @author Renato Rocha <engirocha@gmail.com>
 * @data 11/2016
 * @version 1.0
 * 
 */
include __DIR__ . "/../nav/menu.php"; 
?>
<script type="text/javascript">
    $(document).ready(function() {
        function getData(){
            $.ajax({
                url : my_url+"Setup/additional/getAdditionalSettings",
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        var values = data.data[0];
                        for (var prop in values) {
                            if(prop == "showPosSessionIr" || prop == "showPlaceSale" ||prop == "showSessionPOSacc" ||
                                    prop == "UseStandardEmailSoftware" ||prop == "password_to_access_showDataSale" ||
                                    prop == "ftp_of_provides_includeImg"){
                                //checkboxes
                                if(values[prop] == "on" || values[prop] == "1"){
                                    document.getElementById(prop).checked = true;
                                } else {
                                    document.getElementById(prop).checked = false;
                                }
                            } else if (prop == "language" || prop == "initialPage" || prop == "ftp_of_provides_layout"){
                                //selects
                                document.getElementById(prop).value = values[prop];
                            } else {
                                //inputs
                                document.getElementById(prop).value = values[prop];
                            }
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }
        
        $(document).on('submit', '#form_additional_settings', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Setup/additional/updateAdditionalSettings",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_additional_settings').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        document.getElementById("form_additional_settings").reset();
        getData();
    });
</script>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12"> 
            <div class="container-fluid">
                <form id="form_additional_settings">
                    <div class="row-fluid">            
                        <div class="col-md-4">
                            <h4 class="translate">Additional options of system</h4>
                            <div class="form-group">                    
                                <input type="checkbox" name="showPosSessionIr" id="showPosSessionIr">
                                <t class="translate">Show in internal report the session where POS is operating.</t>
                            </div>           
                            <div class="form-group">
                                <input type="checkbox" name="showPlaceSale" id="showPlaceSale">
                                <t class="translate">Show in report the project where was the sale.</t>                      
                            </div> 
                            <div class="form-group">
                                <input type="checkbox" name="showSessionPOSacc" id="showSessionPOSacc">
                                <t class="translate">Show in accounting report the session where POS is operating.</t>                      
                            </div> 
                            <div class="form-group">
                                <input type="checkbox" name="UseStandardEmailSoftware" id="UseStandardEmailSoftware">
                                <t class="translate">Use standard email software of operational system.</t>                    
                            </div> 
                            <div class="form-group">
                                <p class="translate">Language of system</p>
                                <select id="language" name="language">
                                    <option value="0" class="translate">Select option</option>
                                    <option value="1">English</option>
                                    <option value="2">Portuguese</option>
                                </select>
                            </div> 
                            <div class="form-group">
                                <p class="translate">Initial page of system</p>
                                <select id="initialPage" name="initialPage">
                                    <option value="0" class="translate">Select option</option>
                                    <option value="1" class="translate">Pos</option>
                                    <option value="2" class="translate">Dashboard</option>
                                    <option value="3" class="translate">Label Printing</option>
                                </select>
                            </div> 
                            <h4 class="translate">Parameters of Send Emails</h4>
                            <div class="form-group">
                                <label for="sendersEmail" class="label-style translate">Senders Email</label>
                                <input type="email" class="form-control" name="sendersEmail" id="sendersEmail">
                            </div>
                            <div class="form-group">
                                <label for="alertEmail" class="label-style translate">Email to send alert system</label>
                                <input type="email" class="form-control" name="alertEmail" id="alertEmail">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <h4 class="translate">Password to Access</h4>
                            <div class="form-group">
                                <label for="password_to_access_admin" class="label-style translate">Administrator</label>
                                <input type="text" class="form-control" name="password_to_access_admin" id="password_to_access_admin">
                            </div>
                            <div class="form-group">
                                <label for="password_to_access_employee" class="label-style translate">Employee and Setup</label>
                                <input type="text" class="form-control" name="password_to_access_employee" id="password_to_access_employee">
                            </div>
                            <div class="form-group">
                                <label for="password_to_access_cashRegisterClos" class="label-style translate">Cash register closing</label>
                                <input type="text" class="form-control" name="password_to_access_cashRegisterClos" id="password_to_access_cashRegisterClos">
                            </div>
                            <div class="form-group">
                                <label for="password_to_access_product" class="label-style translate">Product</label>
                                <input type="text" class="form-control" name="password_to_access_product" id="password_to_access_product">
                            </div>
                            <div class="form-group">
                                <label for="password_to_access_cancelItem" class="label-style translate">Cancel Items</label>
                                <input type="text" class="form-control" name="password_to_access_cancelItem" id="password_to_access_cancelItem">
                            </div>
                            <div class="form-group">
                                <label for="password_to_access_cancelPurchase" class="label-style translate">Cancel purchase order</label>
                                <input type="text" class="form-control" name="password_to_access_cancelPurchase" id="password_to_access_cancelPurchase">
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="password_to_access_showDataSale" id="password_to_access_showDataSale">
                                <t class="translate">Show sale data from this system to the panel.</t>                     
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <h4 class="translate">Shortcuts</h4>
                            <div class="btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-primary margin-bottom-10 col-sm-12 translate" onclick="openModalCreditAdm()">Credit administrators</button>
                                <button type="button" class="btn btn-primary col-sm-12 translate" onclick="$('#import_data_product_modal').modal({show : true});">Import data of products</button>
                            </div>

                            <h4 class="translate">FTP of provides</h4>

                            <div class="form-group">
                                <label for="ftp_of_provides_host" class="label-style translate">Host</label>
                                <input type="text" class="form-control" name="ftp_of_provides_host" id="ftp_of_provides_host">

                                <label for="ftp_of_provides_user" class="label-style translate">User</label>
                                <input type="text" class="form-control" name="ftp_of_provides_user" id="ftp_of_provides_user">

                                <label for="ftp_of_provides_password" class="label-style translate">Password</label>
                                <input type="password" class="form-control" name="ftp_of_provides_password" id="ftp_of_provides_password">
                                
                                <label for="ftp_of_provides_layout" class="label-style translate">Layout</label>
                                <select id="ftp_of_provides_layout" name="ftp_of_provides_layout">
                                    <option value="0" class="translate">Select Option</option>
                                    <option value="1">PTech</option>
                                    <option value="2">Emagine</option>
                                    <option value="3">Novita</option>
                                    <option value="3">PCK</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="ftp_of_provides_includeImg" id="ftp_of_provides_includeImg">
                                <t class="translate">Include images.</t>                     
                            </div> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 navbar-bottom">
                            <div class="pull-left"></div>
                            <div class="pull-right">                    
                                <button type="reset" class="btn btn-primary translate">Cancel</button>
                                <button type="submit" class="btn btn-success translate">Save</button>
                            </div>
                        </div>
                    </div>        
                </form>
            </div>
        </div>
    </div>
</div>

<?php 
require 'tabs/creditAdministrator.php';
require 'tabs/ordersServers.php';
require 'tabs/import_data_product.php';
?>
