<?php

$options = "";
if($array_answer['company_refactoring']){
    foreach ($array_answer['company_refactoring'] as $value){
        $options .= '<option value="'.$value->getIdcompanyfactoring().'">'.$value->getName().'</option>';
    }
}
?>

<script type="text/javascript">
    function getDataCreditAdministrator(){
        $.ajax({
            url : my_url+"Setup/additional/getCreditAdministrator",
            type: "POST",
            dataType: 'JSON',
            data : '',
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    var values = data.data[0];
                    for (var prop in values) {
                        if(prop == "includeTextDefault"){
                            //checkboxes
                            if(values[prop] == "on" || values[prop] == "1"){
                                document.getElementById(prop).checked = true;
                            } else {
                                document.getElementById(prop).checked = false;
                            }
                        } else if (prop == "companyfactoring"){
                            //selects
                            document.getElementById(prop).value = values[prop];
                            if(values[prop] == 1){
                                disableElem();
                            }
                        } else {
                            //inputs
                            document.getElementById(prop).value = values[prop];
                        }
                    }
                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    }
    
    
    $(document).on('submit', '#form_credit_administrator', function(e){
        e.preventDefault();
        $.ajax({
            url : my_url+"Setup/Additional/updateCreditAdministrator",
            type: "POST",
            dataType: 'JSON',
            data : $('#form_credit_administrator').serialize(),
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
                $('#credit_administrator_modal').modal('hide');
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    });
        
    function defineNone(e){
        if(document.getElementById("companyfactoring").value == 1){
            document.getElementById("form_credit_administrator").reset();
            disableElem();
        } else {
            enableElem();
        }
    }
    
    function disableElem(){
        $("#name").attr('disabled', true);
        $("#clientId").attr('disabled', true);
        $("#address").attr('disabled', true);
        $("#codPostal").attr('disabled', true);
        $("#bankAccount").attr('disabled', true);
        $("#sendEmail").attr('disabled', true);
        $("#definitionKid").attr('disabled', true);
        $("#invoiceText").attr('disabled', true);
        $("#includeTextDefault").attr('disabled', true);
        
        $("#name").prop('required', false);
        $("#clientId").prop('required', false);
        $("#address").prop('required', false);
        $("#codPostal").prop('required', false);
        $("#bankAccount").prop('required', false);
        $("#sendEmail").prop('required', false);
        $("#definitionKid").prop('required', false);
        $("#invoiceText").prop('required', false);
    }
    
    function enableElem(){
        $("#name").attr('disabled', false);
        $("#clientId").attr('disabled', false);
        $("#address").attr('disabled', false);
        $("#codPostal").attr('disabled', false);
        $("#bankAccount").attr('disabled', false);
        $("#sendEmail").attr('disabled', false);
        $("#definitionKid").attr('disabled', false);
        $("#invoiceText").attr('disabled', false);
        $("#includeTextDefault").attr('disabled', false);
        
        $("#name").attr('required', true);
        $("#clientId").attr('required', true);
        $("#address").attr('required', true);
        $("#codPostal").attr('required', true);
        $("#bankAccount").attr('required', true);
        $("#sendEmail").attr('required', true);
        $("#definitionKid").attr('required', true);
        $("#invoiceText").attr('required', true);   
    }
    
    function openModalCreditAdm(){
        $('#credit_administrator_modal').modal({show : true});
        document.getElementById("form_credit_administrator").reset();
        getDataCreditAdministrator();
    }
    
</script>

<!-- Update -->
<div class="modal fade" id="credit_administrator_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Credit Administrator</h4>
            </div>
            <form id="form_credit_administrator" name="form_credit_administrator">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="companyfactoring" class="label-style translate">Company Factoring</label>
                            <select name="companyfactoring" id="companyfactoring" onchange="defineNone(this)" required="true">
                                <?php echo $options ?>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label for="name" class="label-style translate">Name</label>
                            <input type="text" name="name" id="name" class="form-control" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="clientId" class="label-style translate">Client Id</label>
                            <input type="text" name="clientId" id="clientId" class="form-control" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="address" class="label-style translate">Address</label>
                            <input type="text" name="address" id="address" class="form-control" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="codPostal" class="label-style translate">Cod Postal</label>
                            <input type="text" name="codPostal" id="codPostal" class="form-control" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="bankAccount" class="label-style translate">Bank Account</label>
                            <input type="text" name="bankAccount" id="bankAccount" class="form-control" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="sendEmail" class="label-style translate">Send email</label>
                            <input type="text" name="sendEmail" id="sendEmail" class="form-control" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="definitionKid" class="label-style translate">Definition KID</label>
                            <input type="text" name="definitionKid" id="definitionKid" class="form-control" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="invoiceText" class="label-style translate">Invoice Text</label>
                            <textarea name="invoiceText" cols="3" rows="4" class="form-control" id="invoiceText" required="true"></textarea>
                            <p><input type="checkbox" name="includeTextDefault" id="includeTextDefault"> <t class="translate">Tick to include the original invoice text</t></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>