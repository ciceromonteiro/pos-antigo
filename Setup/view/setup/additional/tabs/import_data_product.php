<script type="text/javascript">
    $(document).ready(function () {        
     
         $(document).on('click', '#downloadProduct', function(e){

            e.preventDefault();
            $.ajax({
                url : my_url+"Setup/additional/downloadProductsData/",
                type: "POST",
                dataType: 'JSON',
                   success: function(data, textStatus, jqXHR){
                    //alert(data.data.element0.idProduct);                    
                    if (data.result == 'success'){
                        document.getElementById('link').click();
                        //notification(true);

                    } else {
                        notification(false);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#link', function(e){

            e.preventDefault();
            $.ajax({
                url : my_url+"Setup/additional/downloadExec/",
                type: "POST",
                dataType: 'JSON',
                   success: function(data, textStatus, jqXHR){
                    //alert(data.data.element0.idProduct);                    
                    if (data.result == 'success'){
                         window.location.href = '../../../data/temp/product.zip'
                        notification(true);

                    } else {
                        notification(false);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        document.getElementById('formUpload').onsubmit = function() {
            return false;
        }

        $("#formUpload").submit(function(){
            var formData = new FormData($("form[name='formUpload']")[0]);
             $.ajax({
                url : my_url+"Setup/additional/upload/",
                type: "POST",
                dataType: 'JSON',
                data: formData,
                processData: false,
                contentType: false,
                   success: function(data, textStatus, jqXHR){             
                    if (data.result == 'success'){
                        notification(true);

                    } else {
                        notification(false);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
           
        });



    });
</script>
<style>

.pointer{
    cursor:pointer;
}
</style>


<a href="../data/temp/productData.json" id="link" style="display: none"></a>
<div class="modal fade" id="import_data_product_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-center" role="document">
        <div class="modal-content">
            <form id="hjkhjk" name="hjk">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="widgets-import list-inline text-center">
                                <li id="downloadProduct" class="pointer"><span class="lnr lnr-download"></span><p class="translate">Download sample file</p></li>
                                <li id="importProduct" class="pointer" onclick="$('#import_data_product_modal_internal').modal({show : true});"><span class="lnr lnr-exit-up"></span><p class="translate">Select file for import</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="import_data_product_modal_internal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-center" role="document">
        <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <form method="Post" enctype="multipart/form-data" id="formUpload" name="formUpload">
                        <div class="col-md-12">
                            <div style="float: left;">
                                <input type="file" name="file" id="file">
                            </div>
                            <div style="float: right;">
                                <button id="upload" type = "submit" class = "btn btn-success translate">Insert</button>

                            </div>
                        </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>