<?php

$url = $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
if(stripos($_SERVER['SERVER_SIGNATURE'], "443")) {
    $protocol = "https://";
} else {
    $protocol = "http://";
    
}
$url_base = $protocol.substr($url, 0, -16);
$auth = new AuthenticationController();
$auth->getIp();
$auth->getAddress();
$auth->getMac();
$auth->getNameMachine();

?>

<script type="text/javascript">
    $('#install_form').trigger("reset");
    $(document).on('submit', '#install_form', function(e){
        e.preventDefault();
        $.ajax({
            url : my_url+"Setup/AddDevice/install/",
            type: "POST",
            dataType: 'JSON',
            data : $('#install_form').serialize(),
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notificationCustom(true, data.message);
                    location.reload();
                } else if(data.result == 'error-license'){
                    notificationCustom(false, data.message);
                } else {
                    notificationCustom(false, data.message);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notificationCustom(false, textStatus);
                console.log(jqXHR.responseText);
            }
        });
    });
        
    function notificationCustom(action, message){
        if(action == true){
            var title = "Done";
            var text = message;
            var icon = "glyphicon glyphicon-plus";
            var type = "success";
        } else {
            var title = "Error";
            var text = message;
            var icon = "glyphicon glyphicon-minus";
            var type = "error";
        }
        var notice = new PNotify({
            title: title,
            text: text,
            icon: icon,
            type: type,
            confirm: {
                confirm: false
            },
            buttons: {
                closer: false,
                sticker: false
            }
        });
        PNotify.prototype.options.styling = "bootstrap3";
        notice.get().click(function() {
            notice.remove();
        }); 
    }
    
    function changeLanguage(){
        var languageSelect = document.getElementById('language').value;
        var language = {
            "english":[
                {"title":"Welcome"},
                {"description": 'Welcome to the famous system of point of sales EasyDrift! You may want to browse the <a href="#">ReadMe documentation</a> at your leisure. Otherwise, just fill in the information below.'},
                {"label1":"Please select your language"},
                {"label2":"Your license"},
                {"button":"Check and Install"}
            ],
            "portugues":[
                {"title":"Bem vindo"},
                {"description": 'Bem-vindo ao famoso sistema de ponto de venda EasyDrift! Você pode <a href="#">ler a documentação</a> quando quiser. Caso contrário, basta preencher as informações abaixo.'},
                {"label1":"Selecione seu idioma"},
                {"label2":"Sua licença"},
                {"button":"Verificar e instalar"}
            ],
            "norwegian":[
                {"title":"Velkommen"},
                {"description": 'Velkommen til den berømte system av poenget med salg EasyDrift! Det kan være lurt å søke på <a href="#">Viktig dokumentasjon</a> på ferie. Ellers, bare fylle ut informasjonen nedenfor.'},
                {"label1":"Vennligst velg språk"},
                {"label2":"din lisens"},
                {"button":"Sjekk og installer"}
            ]};
        if(languageSelect == 1){
            document.getElementById('title').innerHTML = language.english[0].title;
            document.getElementById('description').innerHTML = language.english[1].description;
            document.getElementById('label1').innerHTML = language.english[2].label1;
            document.getElementById('label2').innerHTML = language.english[3].label2;
            document.getElementById('installBtn').innerHTML = language.english[4].button;
        } else if(languageSelect == 2){
            document.getElementById('title').innerHTML = language.norwegian[0].title;
            document.getElementById('description').innerHTML = language.norwegian[1].description;
            document.getElementById('label1').innerHTML = language.norwegian[2].label1;
            document.getElementById('label2').innerHTML = language.norwegian[3].label2;
            document.getElementById('installBtn').innerHTML = language.norwegian[4].button;
        } else if(languageSelect == 3){
            document.getElementById('title').innerHTML = language.portugues[0].title;
            document.getElementById('description').innerHTML = language.portugues[1].description;
            document.getElementById('label1').innerHTML = language.portugues[2].label1;
            document.getElementById('label2').innerHTML = language.portugues[3].label2;
            document.getElementById('installBtn').innerHTML = language.portugues[4].button;
        } else {
            document.getElementById('title').innerHTML = language.english[0].title;
            document.getElementById('description').innerHTML = language.english[1].description;
            document.getElementById('label1').innerHTML = language.english[2].label1;
            document.getElementById('label2').innerHTML = language.english[3].label2;
            document.getElementById('installBtn').innerHTML = language.english[4].button;
        }
    }
</script>
<style>
    body {
        background-color: #F8F8F8;
        color: #000;
    }
    .container {
        background-color: #fff;
        padding: 50px 50px 10px 50px;
        border: 1px solid #ccc
    }
    .logo {
        text-align: center;
    }
    .btn-group {
        margin-top: 20px;
    }
    .footer {
        color: #000;
        margin-top: 120px;
        margin-bottom: 0px;
    }
</style>

<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col-md-12">
            <div class="logo">
                <img src="<?php echo $url_base.'assets/images/logo-black.png';?>" width="180">
            </div>
            <form name="install_form" id="install_form">
                <h2 id="title" style="padding-left: 0px;">Welcome</h2>
                <p id="description">Welcome to the famous system of point of sales EasyDrift! You may want to browse the <a href="#">ReadMe documentation</a> at your 
                    leisure. Otherwise, just fill in the information below.</p>
                <div class="form-group">
                    <label><strong id="label1">Please select your language</strong></label>
                    <select name="language" id="language" required="true" onchange="changeLanguage()">
                        <option value="">Select Language</option>
                        <option value="1">English</option>
                        <option value="2">Norwegian</option>
                        <option value="3">Português</option>
                    </select>
                    <label id="label2">Your license</label>
                    <input class="form-control license" name="license" placeholder="XXXX-XXXX-XXXX-XXXX-XXXX" required="true"><br>
                    <button id="installBtn" type="submit" class="btn btn-primary pull-right" style="margin-bottom: 30px">Check and Install</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.license').mask('####-####-####-####-####', {reverse: true});
</script>