<?php include __DIR__ . "/../nav/menu.php"; 

$productOB = getEm()->getRepository('Product')->findBy(array("active" => 1));
$personOB = getEm()->getRepository('Person')->findBy(array("active" => 1));
$companyOB = getEm()->getRepository('Company')->findBy(array("active" => 1));

?>

<script type="text/javascript">
    var fidelityTable;
    var zipCodeTable;
    $(document).ready(function () {        
        $(document).on('click', '#reading_of_log', function(e){
            e.preventDefault();
            window.location = my_url+"Reports/log/index";
        });

         $(document).on('click', '#verifyDuplicates', function(e){
            document.getElementById("release").style.display = "none";
            e.preventDefault();
            $('#myModal').modal({
                show: true
            });
             $("#loading").html("<img class='text-center' id='image_loading' src='../../../assets/images/default.gif' alt='your image'  width='50' value='1'>");

            $.ajax({
                url : my_url+"Setup/external/verifyDuplicate/",
                type: "POST",
                dataType: 'JSON',
                   success: function(data, textStatus, jqXHR){
                    $("#loading").html("");
                    if (data.result == 'success'){
                         $("#loading").html("<h4> Sem dados duplicados</h4>");
                        //notification(true);

                    } else {
                        $("#loading").html("<h4> Dados duplicados</h4>");
                        document.getElementById("release").style.display = "block";

                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

      $(document).on('click', '#updateDuplicate', function(e){
            document.getElementById("release").style.display = "none";
            $("#loading").html("");
            $("#loading").html("<img class='text-center' id='image_loading' src='../../../assets/images/default.gif' alt='your image'  width='50' value='1'>");
            e.preventDefault();
            $.ajax({
                url : my_url+"Setup/external/releaseDuplicate/",
                type: "POST",
                dataType: 'JSON',
                   success: function(data, textStatus, jqXHR){
                    $("#loading").html("");
                    if (data.result == 'success'){
                         $("#loading").html("<h4> Dados corrigidos com Sucesso</h4>");
                        //notification(true);

                    } else {
                        $("#loading").html("<h4> Erro durante a correção</h4>");
                        document.getElementById("release").style.display = "block";

                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
           
        });

      document.getElementById('formUpload').onsubmit = function() {
            return false;
        }

        $("#formUpload").submit(function(){
            var formData = new FormData($("form[name='formUpload']")[0]);
             $.ajax({
                url : my_url+"Setup/external/uploadXlsXmlJson",
                type: "POST",
                dataType: 'JSON',
                data: formData,
                processData: false,
                contentType: false,
                   success: function(data, textStatus, jqXHR){             
                    if (data.result == 'success'){
                        notification(true);

                    } else {
                        notification(false);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
           
        });

        $(document).on('click', '#programFidelity', function(e){
            e.preventDefault();
            resetForm('#form_fidelity');
            $('#typeFidelity').val(0);
            fidelity();
            $('#modalFidelity').modal({
                show : true
            });
        });

        $(document).on('submit', '#form_fidelity', function(e){
            var typeFidelity = $('#form_fidelity #typeFidelity').val();
            if (typeFidelity == "0") {
                typeFidelity = "0";
            } else {
                typeFidelity = b64EncodeUnicode(typeFidelity);
            }

            var product = $('#form_fidelity #product').val();   
            if (product == "" || product == "0") {
                product = "0";
            } else {
                product = b64EncodeUnicode(product);
            }

            var quantity = $('#form_fidelity #quantity').val();
            if (quantity === "" || quantity == "0") {
                quantity = "0";
            } else {
                quantity = b64EncodeUnicode(quantity);
            }

            var value = $('#form_fidelity #value').val();
            if (value === "" || value == "0") {
                value = "0";

            } else {
                value = b64EncodeUnicode(value);
            }

            var typeAwards = $('#form_fidelity #typeAwards').val();
            if (typeAwards === "0") {
                typeAwards = "0";
            } else {
                typeAwards = b64EncodeUnicode(typeAwards);
            }
            e.preventDefault();
            $.ajax({
                url : my_url+"Setup/external/insertFidelity/"+ typeFidelity +"/"+ product + "/"+ quantity + "/" + value+ "/" + typeAwards,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    resetForm('#form_fidelity');
                    fidelity();
                    reload_table(fidelityTable);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        fidelityTable = $('#fidelityTable').DataTable( {
            "ajax": {"url": my_url+"Setup/external/getAllFidelity/"},
            "columns": [
                {"data": "checkbox" },
                {"data": "id"},
                {"data": "fidelityProduct"},
                {"data": "typeFidelity"},
                {"data": "value"},
                {"data": "fidelityQtd"},
                {"data": "awardsType"}
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $('#form_fidelity_edit #updateFidelitySubmit').attr('disabled', true);
        $('#form_fidelity #insertFidelity').attr('disabled', true);
        $('#form_fidelity #update').attr('disabled', true);
        $('#form_fidelity #delete').attr('disabled', true);
        $("#fidelityTable tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#fidelityTable');
            if(itemSelected == 0){
                $('#form_fidelity #update').attr('disabled', true);
                $('#form_fidelity #delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#form_fidelity #update').attr('disabled', false);
                $('#form_fidelity #delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#form_fidelity #update').attr('disabled', true);
                $('#form_fidelity #delete').attr('disabled', false);
            }
        });


        $(document).on('click', '#form_fidelity #update', function(e){
            e.preventDefault();
            var id = fidelityTable.$('tr.hover-select').find('input:checkbox').data('id');
            resetForm('#form_fidelity_edit');
            $.ajax({
                url : my_url+"Setup/external/getFidelity/"+ id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#modalFidelityEdit').modal({show : true});
                        $('#modalFidelityEdit').show();
                        $("html,body").css({"overflow":"hidden"});

                        $('#form_fidelity_edit #idFidelity').val(data.data[0].id);
                        $('#form_fidelity_edit #typeFidelity').val(data.data[0].typeFidelity);
                        fidelity1();
                        $('#form_fidelity_edit #value').val(data.data[0].value);
                        $('#form_fidelity_edit #quantity').val(data.data[0].fidelityQtd);
                        $('#form_fidelity_edit #product').val(data.data[0].fidelityProduct);
                        $('#form_fidelity_edit #typeAwards').val(data.data[0].awardsType);
                        disabledUpdate();
                        displayAwardsUpdate();

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });


          $(document).on('submit', '#form_fidelity_edit', function(e){
            var id = $('#form_fidelity_edit #idFidelity').val();
            if (id == "") {
                id = "0";
            } else {
                id = b64EncodeUnicode(id);
            }

            var typeFidelity = $('#form_fidelity_edit #typeFidelity').val();
            if (typeFidelity == "0") {
                typeFidelity = "0";
            } else {
                typeFidelity = b64EncodeUnicode(typeFidelity);
            }

            var product = $('#form_fidelity_edit #product').val();   
            if (product === "") {
                product = "0";
            } else {
                product = b64EncodeUnicode(product);
            }

            var quantity = $('#form_fidelity_edit #quantity').val();
            if (quantity === "") {
                quantity = "0";
            } else {
                quantity = b64EncodeUnicode(quantity);
            }

            var value = $('#form_fidelity_edit #value').val();
            if (value === "") {
                value = "0";

            } else {
                value = b64EncodeUnicode(value);
            }

            var typeAwards = $('#form_fidelity_edit #typeAwards').val();
            if (typeAwards === "0") {
                typeAwards = "0";
            } else {
                typeAwards = b64EncodeUnicode(typeAwards);
            }
            e.preventDefault();
            $.ajax({
                url : my_url+"Setup/external/updateFidelity/"+ id +"/"+ typeFidelity +"/"+ product + "/"+ quantity + "/" + value+ "/" + typeAwards,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modalFidelityEdit').hide();
                    $("html,body").css({"overflow":"auto"});
                    $('#form_fidelity #update').attr('disabled', true);
                    $('#form_fidelity #delete').attr('disabled', true);
                    reload_table(fidelityTable);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });


        $(document).on('click', '#form_fidelity #delete', function(e){
            e.preventDefault();
            var trs = fidelityTable.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idFidelity : ids};
            deleteItens('Setup/external/deleteFidelity', array_deletes, fidelityTable);
            $('#form_fidelity #update').attr('disabled', true);
            $('#form_fidelity #delete').attr('disabled', true);
        });

        $(document).on('click', '#codeZipCode', function(e){
            e.preventDefault();
            resetForm('#form_zip_code');
            $('#form_zip_code #typeCodeZipCode').val(0);
            typeZipCode();
            $('#modalZipCode').modal({
                show : true
            });
            $('#form_zip_code #update').attr('disabled', true);
            $('#form_zip_code #delete').attr('disabled', true);
        });


        $(document).on('submit', '#form_zip_code', function(e){
            var typeCodeZipCode = $('#form_zip_code #typeCodeZipCode').val();
            if (typeCodeZipCode == "0") {
                typeCodeZipCode = "0";
            } else {
                typeCodeZipCode = b64EncodeUnicode(typeCodeZipCode);
            }

            var client = $('#form_zip_code #client').val();   
            if (client == "" || client == "0") {
                client = "0";
            } else {
                client = b64EncodeUnicode(client);
            }

            var company = $('#form_zip_code #company').val();
            if (company === "" || company == "0") {
                company = "0";
            } else {
                company = b64EncodeUnicode(company);
            }

            var zipCode = $('#form_zip_code #zipCode').val();
            if (zipCode === "") {
                zipCode = "0";

            } else {
                zipCode = b64EncodeUnicode(zipCode);
            }
            e.preventDefault();
            $.ajax({
                url : my_url+"Setup/external/insertCodeZipCode/"+ typeCodeZipCode +"/"+ client + "/"+ company + "/" + zipCode,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    resetForm('#form_zip_code');
                    typeZipCode();
                    reload_table(zipCodeTable);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });



        zipCodeTable = $('#zipCodeTable').DataTable( {
            "ajax": {"url": my_url+"Setup/external/getAllCodeZipCode/"},
            "columns": [
                {"data": "checkbox" },
                {"data": "id"},
                {"data": "client"},
                {"data": "company"},
                {"data": "zipCode"}
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });


        $(document).on('click', '#form_zip_code #update', function(e){
            e.preventDefault();
            var id = zipCodeTable.$('tr.hover-select').find('input:checkbox').data('id');
            resetForm('#form_zip_code_edit');
            $.ajax({
                url : my_url+"Setup/external/getCodeZipCode/"+ id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#modalZipCodeEdit').modal({show : true});
                        $('#modalZipCodeEdit').show();
                        $('#form_zip_code_edit #idCodeZipCode').val(data.data[0].id);
                        if(data.data[0].client != 0 && data.data[0].company == 0){
                            $('#form_zip_code_edit #typeCodeZipCode').val("Client");
                            typeZipCodeUpdate()
                            $('#form_zip_code_edit #client').val(data.data[0].client);
                        }else if(data.data[0].client == 0 && data.data[0].company != 0){
                            $('#form_zip_code_edit #typeCodeZipCode').val("Company");
                            typeZipCodeUpdate()
                            $('#form_zip_code_edit #company').val(data.data[0].company);
                            
                        }else{
                            $('#form_zip_code_edit #typeCodeZipCode').val('');
                            typeZipCodeUpdate()
                        }

                        enableZipCodeUpdate();

                        $('#form_zip_code_edit #zipCode').val(data.data[0].zipCode);
                        $('#form_zip_code_edit #typeCodeZipCode').attr('disabled', true);
                        $('#form_zip_code_edit #client').attr('disabled', true);
                        $('#form_zip_code_edit #company').attr('disabled', true);


                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_zip_code_edit', function(e){
            var id = $('#form_zip_code_edit #idCodeZipCode').val();
            if (id == "0") {
                id = "0";
            } else {
                id = b64EncodeUnicode(id);
            }

            var zipCode = $('#form_zip_code_edit #zipCode').val();
            if (zipCode === "") {
                zipCode = "0";

            } else {
                zipCode = b64EncodeUnicode(zipCode);
            }
            e.preventDefault();
            $.ajax({
                url : my_url+"Setup/external/updateCodeZipCode/"+ id +"/" + zipCode,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modalZipCodeEdit').hide();
                    $('#form_zip_code #update').attr('disabled', true);
                    $('#form_zip_code #delete').attr('disabled', true);
                    reload_table(zipCodeTable);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $('#form_zip_code #update').attr('disabled', true);
        $('#form_zip_code #delete').attr('disabled', true);
        $("#zipCodeTable tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#zipCodeTable');
            if(itemSelected == 0){
                $('#form_zip_code #update').attr('disabled', true);
                $('#form_zip_code #delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#form_zip_code #update').attr('disabled', false);
                $('#form_zip_code #delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#form_zip_code #update').attr('disabled', true);
                $('#form_zip_code #delete').attr('disabled', false);
            }
        });

        $(document).on('click', '#form_zip_code #delete', function(e){
            e.preventDefault();
            var trs = zipCodeTable.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idZipCode : ids};
            deleteItens('Setup/external/deleteCodeZipCode', array_deletes, zipCodeTable);
            $('#form_zip_code #update').attr('disabled', true);
            $('#form_zip_code #delete').attr('disabled', true);
        });



    });

    function disabledInsert(){
        var awards = $('#form_fidelity #typeAwards').val();

        if(awards == 0){
            $('#form_fidelity #insertFidelity').attr('disabled', true);
        }else{
            $('#form_fidelity #insertFidelity').attr('disabled', false);
        }
    }

    function disabledUpdate(){
        var awards = $('#form_fidelity_edit #typeAwards').val();

        if(awards == 0){
            $('#form_fidelity_edit #updateFidelitySubmit').attr('disabled', true);
        }else{
            $('#form_fidelity_edit #updateFidelitySubmit').attr('disabled', false);
        }
    }

    function displayAwardsInsert(){
        var product = $('#form_fidelity #product').val();

        if(product == 0){
            document.getElementById("awards_type").style.display = "none";
        }else{
            document.getElementById("awards_type").style.display = "block";
        }
 
    }

    function displayAwardsUpdate(){
        var product = $('#form_fidelity_edit #product').val();

        if(product == 0){
            $("#form_fidelity_edit #awards_type").css("display","none");
        }else{
            $("#form_fidelity_edit #awards_type").css("display","block");
        }
 
    }


    function fidelity(){
       var type = $('#form_fidelity #typeFidelity').val();
        if(type == 1){
            document.getElementById("byProduct").style.display = "block";
            document.getElementById("byPurchaseValue").style.display = "none";
            
            $('#form_fidelity #typeAwards').val("0");
            disabledInsert();
            $('#form_fidelity #product').val("0");
            $('#form_fidelity #quantity').val("");
            $('#form_fidelity #value').val("");

            $('#form_fidelity #product').attr('required', true);
            $('#form_fidelity #quantity').attr('required', true);
            $('#form_fidelity #value').attr('required', false);
        }else if(type == 2){
            document.getElementById("byProduct").style.display = "none";
            document.getElementById("awards_type").style.display = "block";
            document.getElementById("byPurchaseValue").style.display = "block";
            $('#form_fidelity #typeAwards').val("0");
            disabledInsert();
            $('#form_fidelity #product').val("0");
            $('#form_fidelity #quantity').val("");
            $('#form_fidelity #value').val("");

            $('#form_fidelity #product').attr('required', false);
            $('#form_fidelity #quantity').attr('required', false);
            $('#form_fidelity #value').attr('required', true);

        }else{
            document.getElementById("byProduct").style.display = "none";
            document.getElementById("awards_type").style.display = "none";
            document.getElementById("byPurchaseValue").style.display = "none";
            $('#form_fidelity #typeAwards').val("0");  
            disabledInsert();
            $('#form_fidelity #product').val("0");
            $('#form_fidelity #quantity').val("");
            $('#form_fidelity #value').val("");
            $('#form_fidelity #product').attr('required', false);
            $('#form_fidelity #quantity').attr('required', false);
            $('#form_fidelity #value').attr('required', false);
        }

    }

    function fidelity1(){
       var type = $('#form_fidelity_edit #typeFidelity').val();
        if(type == 1){
            $("#form_fidelity_edit #byProduct").css("display","block");
            $("#form_fidelity_edit #byPurchaseValue").css("display","none");
            
            $('#form_fidelity_edit #typeAwards').val("0");
            disabledUpdate();
            $('#form_fidelity_edit #product').val("0");
            $('#form_fidelity_edit #quantity').val("");
            $('#form_fidelity_edit #value').val("");

            $('#form_fidelity_edit #product').attr('required', true);
            $('#form_fidelity_edit #quantity').attr('required', true);
            $('#form_fidelity_edit #value').attr('required', false);
            displayAwardsUpdate();
        }else if(type == 2){
            $("#form_fidelity_edit #byProduct").css("display","none");
            $("#form_fidelity_edit #awards_type").css("display","block");
            $("#form_fidelity_edit #byPurchaseValue").css("display","block");
            
            $('#form_fidelity_edit #typeAwards').val("0");
            disabledUpdate();
            $('#form_fidelity_edit #product').val("0");
            $('#form_fidelity_edit #quantity').val("");
            $('#form_fidelity_edit #value').val("");

            $('#form_fidelity_edit #product').attr('required', false);
            $('#form_fidelity_edit #quantity').attr('required', false);
            $('#form_fidelity_edit #value').attr('required', true);
        }else{
            $("#form_fidelity_edit #byProduct").css("display","none");
            $("#form_fidelity_edit #awards_type").css("display","none");
            $("#form_fidelity_edit #byPurchaseValue").css("display","none");
            
            $('#form_fidelity_edit #typeAwards').val("0");
            disabledUpdate();
            $('#form_fidelity_edit #product').val("0");
            $('#form_fidelity_edit #quantity').val("");
            $('#form_fidelity_edit #value').val("");

            $('#form_fidelity_edit #product').attr('required', false);
            $('#form_fidelity_edit #quantity').attr('required', false);
            $('#form_fidelity_edit #value').attr('required', false);
        }

    }

    function closeModalUpdate(){
        $('#modalFidelityEdit').hide();
    }

    function closeModal(){
        $('#modalFidelityEdit').modal('hide');
    }

    function typeZipCode(){
        var type = $('#form_zip_code #typeCodeZipCode').val();
        if(type == 1){
            $("#form_zip_code #byCLient").css("display","block");
            $("#form_zip_code #byCompany").css("display","none");
            $("#form_zip_code #byZipCode").css("display","block");
            
            $('#form_zip_code #client').val("0");
            $('#form_zip_code #company').val("0");
            $('#form_zip_code #zipCode').val("");

            $('#form_zip_code #client').attr('required', true);
            $('#form_zip_code #company').attr('required', false);
            $('#form_zip_code #zipCode').attr('required', true);
            $('#form_zip_code #zipCode').attr('disabled', true);
            $('#form_zip_code #insertCodeZipCode').attr('disabled', true);
            //displayAwardsUpdate();
        }else if(type == 2){
            $("#form_zip_code #byCLient").css("display","none");
            $("#form_zip_code #byCompany").css("display","block");
            $("#form_zip_code #byZipCode").css("display","block");
            
            $('#form_zip_code #client').val("0");
            $('#form_zip_code #company').val("0");
            $('#form_zip_code #zipCode').val("");

            $('#form_zip_code #client').attr('required', false);
            $('#form_zip_code #company').attr('required', true);
            $('#form_zip_code #zipCode').attr('required', true);
            $('#form_zip_code #zipCode').attr('disabled', true);
            $('#form_zip_code #insertCodeZipCode').attr('disabled', true);
        }else{
            $("#form_zip_code #byCLient").css("display","none");
            $("#form_zip_code #byCompany").css("display","none");
            $("#form_zip_code #byZipCode").css("display","none");
            
            $('#form_zip_code #client').val("0");
            $('#form_zip_code #company').val("0");
            $('#form_zip_code #zipCode').val("");

            $('#form_zip_code #client').attr('required', false);
            $('#form_zip_code #company').attr('required', false);
            $('#form_zip_code #zipCode').attr('required', false);
            $('#form_zip_code #zipCode').attr('disabled', false);
            $('#form_zip_code #insertCodeZipCode').attr('disabled', true);
        }
    }

     function typeZipCodeUpdate(){
        var type = $('#form_zip_code_edit #typeCodeZipCode').val();
        if(type == 'Client'){
            $("#form_zip_code_edit #byCLient").css("display","block");
            $("#form_zip_code_edit #byCompany").css("display","none");
            $("#form_zip_code_edit #byZipCode").css("display","block");
            
            $('#form_zip_code_edit #client').val("");
            $('#form_zip_code_edit #company').val("");
            $('#form_zip_code_edit #zipCode').val("");

            $('#form_zip_code_edit #client').attr('required', true);
            $('#form_zip_code_edit #company').attr('required', false);
            $('#form_zip_code_edit #zipCode').attr('required', true);
            $('#form_zip_code_edit #zipCode').attr('disabled', true);
            $('#form_zip_code_edit #insertCodeZipCode').attr('disabled', true);
            //displayAwardsUpdate();
        }else if(type == 'Company'){
            $("#form_zip_code_edit #byCLient").css("display","none");
            $("#form_zip_code_edit #byCompany").css("display","block");
            $("#form_zip_code_edit #byZipCode").css("display","block");
            
            $('#form_zip_code_edit #client').val("");
            $('#form_zip_code_edit #company').val("");
            $('#form_zip_code_edit #zipCode').val("");

            $('#form_zip_code_edit #client').attr('required', false);
            $('#form_zip_code_edit #company').attr('required', true);
            $('#form_zip_code_edit #zipCode').attr('required', true);
            $('#form_zip_code_edit #zipCode').attr('disabled', true);
            $('#form_zip_code_edit #insertCodeZipCode').attr('disabled', true);
        }else{
            $("#form_zip_code_edit #byCLient").css("display","none");
            $("#form_zip_code_edit #byCompany").css("display","none");
            $("#form_zip_code_edit #byZipCode").css("display","none");
            
            $('#form_zip_code_edit #client').val("");
            $('#form_zip_code_edit #company').val("");
            $('#form_zip_code_edit #zipCode').val("");

            $('#form_zip_code_edit #client').attr('required', false);
            $('#form_zip_code_edit #company').attr('required', false);
            $('#form_zip_code_edit #zipCode').attr('required', false);
            $('#form_zip_code_edit #zipCode').attr('disabled', false);
            $('#form_zip_code_edit #insertCodeZipCode').attr('disabled', true);
        }

        enableZipCodeUpdate();
    }

    function enableZipCode(){
        var valueCompany = $('#form_zip_code #company').val();
        var valueClient = $('#form_zip_code #client').val();

        if(valueCompany != "" || valueClient != ""){
            
            $('#form_zip_code #zipCode').attr('disabled', false);
            $('#form_zip_code #insertCodeZipCode').attr('disabled', false);
        }else{
            $('#form_zip_code #insertCodeZipCode').attr('disabled', true);
            $('#form_zip_code #zipCode').attr('disabled', true);
        }

    }

    function enableZipCodeUpdate(){
        var valueCompany = $('#form_zip_code_edit #company').val();
        var valueClient = $('#form_zip_code_edit #client').val();

        if(valueCompany != "0" || valueClient != "0"){
            
            $('#form_zip_code_edit #zipCode').attr('disabled', false);
            $('#form_zip_code_edit #updateCodeZipCode').attr('disabled', false);
        }else{
            $('#form_zip_code_edit #updateCodeZipCode').attr('disabled', true);
            $('#form_zip_code_edit #zipCode').attr('disabled', true);
        }

    }


    function closeModalZipCodeEdit(){
        $('#modalZipCodeEdit').hide();
    }

    function closeModalZipCode(){
        $('#modalZipCodeEdit').modal('hide');
    }

    $(document).on('click', '#importBanking', function(e){
            e.preventDefault();
            resetForm('#import_Banking_form');
            $('#import_Banking_modal').modal({
                show : true
            });
        });


    $(document).on('click', '#exportShippingFile', function(e){
        e.preventDefault();
        $.ajax({
            url : my_url+"Setup/external/exportShippingFile/",
            type: "POST",
            dataType: 'JSON',
               success: function(data, textStatus, jqXHR){                 
                if (data.result == 'success'){
                    window.location.href = '../../../data/shipping_and_return_bank_file/shipping_bank_file.zip';
                    notification(true);

                } else {
                    notification(false);

                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    });
     $(document).ready(function () {  
        document.getElementById('formUploadReturn').onsubmit = function() {
            return false;
        }


         $("#formUploadReturn").submit(function(){
                var formData = new FormData($("form[name='formUploadReturn']")[0]);
                 $.ajax({
                    url : my_url+"Setup/external/importReturnFile/",
                    type: "POST",
                    dataType: 'JSON',
                    data: formData,
                    processData: false,
                    contentType: false,
                       success: function(data, textStatus, jqXHR){             
                        if (data.result == 'success'){
                            notification(true);

                        } else {
                            notification(false);

                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        notification(false);
                        console.log(jqXHR.responseText);
                    }
                });
               
            });
         });   
    


</script>

<style>

.pointer{
    cursor:pointer;
}
</style>

<div class="content">
    <div class="row row-fluid" style="padding-bottom: 50px">
        <div class="col-md-6">
            <button id="reading_of_log" class="form-control btn btn-primary translate">Reading of log</button>
            <button style="color: red" class="form-control btn btn-primary translate">Webshop</button>
            <button class="form-control btn btn-primary translate" onclick="$('#import_data').modal({show : true});">Import data (xls, xml, json)</button>
            <button id="verifyDuplicates" class="form-control btn btn-primary translate">Verify duplicates clients</button>
        </div>
        <div class="col-md-6">
            <button id="importBanking" class="form-control btn btn-primary translate">Import banking return</button>
            <button style="color: red" class="form-control btn btn-primary translate">Change existing shopping orders</button>
            <button id="codeZipCode" class="form-control btn btn-primary translate">Codes of Zip code</button>
            <button id="programFidelity" class="form-control btn btn-primary translate">Program of fidelity</button>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Verify Duplicates Clients</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                     <div id="loading" class="col-md-12 text-center">
                     <!--<img class="text-center" id="image_loading" src="../../../assets/images/default.gif" alt="your image"  width="50" value="1">-->
                     
                </div>
                <div id="release" class="text-center" style="display: none">
                            <button id="updateDuplicate" class="btn btn-success translate">repair</button>
                    </div>
                    </div>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="import_data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-center" role="document">
        <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <form method="Post" enctype="multipart/form-data" id="formUpload" name="formUpload">
                        <div class="col-md-12">
                            <div style="float: left;">
                                <input type="file" name="file" id="file">
                            </div>
                            <div style="float: right;">
                                <button id="upload" type = "submit" class = "btn btn-success translate">Insert</button>

                            </div>
                        </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- Create Fidelity-->
<div class="modal fade" id="modalFidelity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="closeModalFidelity" class="close" data-dismiss="modal" aria-label="Close" onclick="closeModal()"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Program Fidelity</h4>
            </div>
            <h4>New Program Fidelity</h4>
            <form id="form_fidelity">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="typeFidelity" class="translate">Type Fidelity:</label>
                            <select name="typeFidelity" id="typeFidelity" onchange="fidelity();" required>

                                <option value="0" class="translate">Select option</option>
                                <option value="1" class="translate">by product</option>
                                <option value="2" class="translate">by purchase value</option>
                            ?>
                            </select>
                        </div>
                    </div>    
                </div>   

                <div class="modal-body" id="byProduct" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="product" class="translate">Product for fidelity:</label>
                            <select name="product" id="product" onclick="displayAwardsInsert()">
                                <option value="0" class="translate">Select option</option>
                                <?php
                            foreach ($productOB as $valuesProductArray):
                                ?><option value="<?php echo $valuesProductArray->getIdproduct(); ?>"><?php echo $valuesProductArray->getName(); ?></option>
                                <?php
                            endforeach;
                            ?>
                            </select>
                        </div>
                        
                        <div class="col-md-3">
                            <label for="quantity" class="label-style translate">Quantity:</label>
                            <input type="number" class="form-control" id="quantity" name="quantity" min="1" >
                        </div>


                    </div>
                </div>

                <div class="modal-body" id="byPurchaseValue" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">                    
                        <div class="col-md-3">
                            <label for="quantity" class="label-style translate">Value:</label>
                            <input type="number" class="form-control" step="0.01" min="0.01" id="value" name="value">
                        </div>

                    </div>
                </div>

                <div class="modal-body" id="awards_type" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">                    
                        <div class="col-md-6">
                            <label for="typeAwards" class="translate">awards type:</label>
                            <select name="typeAwards" id="typeAwards" onchange="disabledInsert()" required>

                                <option value="0" class="translate">Select option</option>
                                <option value="1" class="translate">New product</option>
                                <option value="2" class="translate">discount</option>
                            ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="insertFidelity" class="btn btn-primary translate" disabled>Insert</button>
                </div>
                <div class="modal-body" id="awards_type">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                <h4>Program Fidelity Existent</h4>
                    <div class="row">                    
                        <div class="col-md-12">
                            <div class="button-group">
                                <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                                <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
                            </div>

                            <table id="fidelityTable" class="table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>                      
                                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('customers')"></th>
                                        <th style="width: 80px" class="translate">ID</th>
                                        <th class="translate">Product</th>
                                        <th class="translate">type Fidelity</th>
                                        <th class="translate">Value</th>
                                        <th class="translate">Quantity</th>
                                        <th class="translate">Awards Type</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!--update Fidelity-->
<div class="modal fade" id="modalFidelityEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="closeModalFidelityEdit" class="close" onclick="closeModalUpdate()"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Program Fidelity</h4>
            </div>
            <h4>New Program Fidelity</h4>
            <form id="form_fidelity_edit">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="idFidelity" id="idFidelity" style="display: none;">
                            <label for="typeFidelity" class="translate">Type Fidelity:</label>
                            <select name="typeFidelity" id="typeFidelity" onchange="fidelity1();">

                                <option value="0" class="translate">Select option</option>
                                <option value="1" class="translate">by product</option>
                                <option value="2" class="translate">by purchase value</option>
                            ?>
                            </select>
                        </div>
                    </div>    
                </div>   

                <div class="modal-body" id="byProduct" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="product" class="translate">Product for fidelity:</label>
                            <select name="product" id="product" onchange="displayAwardsUpdate()">
                                <option value="0" class="translate">Select option</option>
                                <?php
                            foreach ($productOB as $valuesProductArray):
                                ?><option value="<?php echo $valuesProductArray->getIdproduct(); ?>"><?php echo $valuesProductArray->getName(); ?></option>
                                <?php
                            endforeach;
                            ?>
                            </select>
                        </div>
                        
                        <div class="col-md-3">
                            <label for="quantity" class="label-style translate">Quantity:</label>
                            <input type="number" class="form-control" id="quantity" name="quantity" min="1">
                        </div>


                    </div>
                </div>

                <div class="modal-body" id="byPurchaseValue" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">                    
                        <div class="col-md-3">
                            <label for="quantity" class="label-style translate">Value:</label>
                            <input type="number" class="form-control" step="0.01" min="0.01" id="value" name="value">
                        </div>

                    </div>
                </div>

                <div class="modal-body" id="awards_type" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">                    
                        <div class="col-md-6">
                            <label for="typeAwards" class="translate">awards type:</label>
                            <select name="typeAwards" id="typeAwards" onchange="disabledUpdate()">

                                <option value="0" class="translate">Select option</option>
                                <option value="1" class="translate">New product</option>
                                <option value="2" class="translate">discount</option>
                            ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="updateFidelitySubmit" class="btn btn-primary translate" disabled>Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Create code of zip Codes-->
<div class="modal fade" id="modalZipCode" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="closeModalFidelity" class="close" data-dismiss="modal" aria-label="Close" onclick="closeModalZipCode()"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Code of Zip Code</h4>
            </div>
            <h4>New Code of Zip Code</h4>
            <form id="form_zip_code">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="typeCodeZipCode" class="translate">Client or Company:</label>
                            <select name="typeCodeZipCode" id="typeCodeZipCode" onchange="typeZipCode()" required>

                                <option value="0" class="translate">Select option</option>
                                <option value="1" class="translate">Client</option>
                                <option value="2" class="translate">Company</option>
                            ?>
                            </select>
                        </div>
                    </div>    
                </div>   

                <div class="modal-body" id="byCLient" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="client" class="translate">Zip Code of Client:</label>
                            <select name="client" id="client" onclick="enableZipCode()">
                                <option value="0" class="translate">Select option</option>
                                <?php
                            foreach ($personOB as $valuesPersonArray):
                                ?><option value="<?php echo $valuesPersonArray->getIdperson(); ?>"><?php echo $valuesPersonArray->getName(); ?></option>
                                <?php
                            endforeach;
                            ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-body" id="byCompany" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="company" class="translate">Zip Code of Company:</label>
                            <select name="company" id="company" onclick="enableZipCode()">
                                <option value="0" class="translate">Select option</option>
                                <?php
                            foreach ($companyOB as $valuesCompanyArray):
                                ?><option value="<?php echo $valuesCompanyArray->getIdcompany(); ?>"><?php echo $valuesCompanyArray->getName(); ?></option>
                                <?php
                            endforeach;
                            ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-body" id="byZipCode" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">                    
                        <div class="col-md-3">
                            <label for="zipCode" class="label-style translate">Zip Code:</label>
                            <input type="text" class="form-control" id="zipCode" name="zipCode" disabled>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="insertCodeZipCode" class="btn btn-primary translate" disabled>Insert</button>
                </div>
                <div class="modal-body" id="awards_type">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                <h4>Code Zip Code existent</h4>
                    <div class="row">                    
                        <div class="col-md-12">
                            <div class="button-group">
                                <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                                <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
                            </div>

                            <table id="zipCodeTable" class="table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>                      
                                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('customers')"></th>
                                        <th style="width: 80px" class="translate">ID</th>
                                        <th class="translate">Client</th>
                                        <th class="translate">Company</th>
                                        <th class="translate">zipCode</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Update code of zip Codes-->
<div class="modal fade" id="modalZipCodeEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" id="closeModalFidelity" class="close" onclick="closeModalZipCodeEdit()"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Code of Zip Code</h4>
            </div>
            <h4>New Code of Zip Code</h4>
            <form id="form_zip_code_edit">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="text" name="idCodeZipCode" id="idCodeZipCode" style="display: none;">
                            <label for="typeCodeZipCode" class="translate">Client or Company:</label>
                            <input type="text" class="form-control" id="typeCodeZipCode" name="typeCodeZipCode" disabled>
                        </div>
                    </div>    
                </div>   

                <div class="modal-body" id="byCLient" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="client" class="translate">Zip Code of Client:</label>
                            <input type="text" class="form-control" id="client" name="client" disabled>
                        </div>
                    </div>
                </div>

                <div class="modal-body" id="byCompany" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">
                        <div class="col-md-9">
                            <label for="company" class="translate">Zip Code of Company:</label>
                            <input type="text" class="form-control" id="company" name="company" disabled>
                        </div>
                    </div>
                </div>
                <div class="modal-body" id="byZipCode" style="display: none;">
                <hr style="height:1px; color:#ddd; background-color:#ddd; margin-top: 0px; margin-bottom: 0px;"/>
                    <div class="row">                    
                        <div class="col-md-3">
                            <label for="zipCode" class="label-style translate">Zip Code:</label>
                            <input type="text" class="form-control" id="zipCode" name="zipCode" disabled>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="updateCodeZipCode" class="btn btn-primary translate" disabled>Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--import Banking-->
<div class="modal fade" id="import_Banking_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-center" role="document">
        <div class="modal-content">
            <form id="import_Banking_form" name="hjk">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="widgets-import list-inline text-center">
                                <li id="exportShippingFile" class="pointer"><span class="lnr lnr-download"></span><p class="translate">Export shipping file</p></li>
                                <li id="importProduct" class="pointer" onclick="$('#import_data_product_modal_internal').modal({show : true});"><span class="lnr lnr-exit-up"></span><p class="translate">Import shipping file</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="import_data_product_modal_internal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-center" role="document">
        <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <form method="Post" enctype="multipart/form-data" id="formUploadReturn" name="formUploadReturn">
                        <div class="col-md-12">
                            <div style="float: left;">
                                <input type="file" name="file" id="file">
                            </div>
                            <div style="float: right;">
                                <button id="upload" type="submit" class = "btn btn-success translate">Insert</button>

                            </div>
                        </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>

