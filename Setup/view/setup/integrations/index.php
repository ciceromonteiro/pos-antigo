<?php include __DIR__ . "/../nav/menu.php"; ?>

<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({
            url : my_url+"Setup/integrations/getData",
            type: "POST",
            dataType: 'JSON',
            data : '',
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    var values = data.data;
                    
                    for (var prop in values) {
                        console.log(prop + ": " +  values[prop]);
                        document.getElementById(prop).value = values[prop];
                    }
                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
        
    });

    
    function importBarcodeBrazil(){
        //console.log($('#barcodeFile'));
        if ($('#barcodeFile').files !== null){
            
            var reader = new FileReader();
            var file = document.getElementById('barcodeFile').files[0];
            reader.readAsDataURL(file);
            var fd = new FormData();
            fd.append('barcode', file);
            reader.onload = function () {
                //console.log(reader.result);
                //console.log(reader.result);
                $.ajax({
                    url : my_url+"Setup/integrations/importBarcodeBrazil",
                    type: "POST",
                    dataType: 'JSON',
                    data : reader.result,
                    success: function(data, textStatus, jqXHR){
                        if (data.result == 'success'){
                            notification(true);
                        } else {
                            notification(false);
                            console.log(data.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        notification(false);
                        console.log(jqXHR.responseText);
                    }
                });
            };
            
        }
    }
    
    // import bestseller
    function importBestseller(){
        
        // salvar dados do arquivo passado
        if (document.getElementById("bestsellerFile").files.length != 0 ){
            var reader = new FileReader();
            var file = document.getElementById('bestsellerFile').files[0];
            reader.readAsDataURL(file);
            var fd = new FormData();
            fd.append('barcode', file);
            reader.onload = function () {
                $.ajax({
                    url : my_url+"Setup/integrations/importBestseller",
                    type: "POST",
                    dataType: 'JSON',
                    data : reader.result,
                    success: function(data, textStatus, jqXHR){
                        if (data.result == 'success'){
                            notification(true);
                        } else {
                            notification(false);
                            console.log(data.message);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        notification(false);
                        console.log(jqXHR.responseText);
                    }
                });
            };
            
        } else {
            // salvar dados do FTP
            $.ajax({
                url : my_url+"Setup/integrations/importBestseller",
                type: "PUT",
                dataType: 'JSON',
                data: '',
                success: function(data){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
            
        }
    }
    
    $(document).on('submit', '#form_integration_settings', function(e){
        e.preventDefault();
        console.log($('#form_integration_settings').serialize());
        $.ajax({
            url : my_url+"Setup/integrations/saveData",
            type: "POST",
            dataType: 'JSON',
            data : $('#form_integration_settings').serialize(),
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    });
    
</script>
<div class="content">
    <form id="form_integration_settings" >
        <div class="row row-fluid" style="padding-bottom: 50px">
           

            <div class="col-md-6">

            <?php 
            //var_dump($traivew);
            if ($view["country"] == "brazil"){ ?>
                <h4 class="translate" >Import Barcode</h4>
                <div class="col-md-12">
                    <label for="barcodeFile" class="label-style translate">Barcode Folder</label>
                    <input style="margin-bottom: 3%" type="file" id="barcodeFile">
                </div>
                <div class="col-md-12">
                    <button type="button" onclick="importBarcodeBrazil()" class="btn btn-primary translate">Import Barcode</button>
                </div>
            <?php } else { ?>
                <h4 style="color: red">Import Barcode</h4>
                <div class="col-md-12">
                    <label style="color: red" for="barcodeFile" class="label-style translate">Barcode Folder</label>
                    <input type="file" id="barcodeFile">
                </div>
                <div class="col-md-12">
                    <label style="color: red" for="barcodeCustomerno" class="label-style translate">Customer No</label>
                    <input type="number" min="0" class="form-control" name="barcodeCustomerno" id="barcodeCustomerno">
                </div>
                <div class="col-md-12">
                    <label style="color: red" for="barcodePassword" class="label-style translate">Password</label>
                    <input type="password" class="form-control" name="barcodePassword" id="barcodePassword">
                </div>
                <div class="col-md-12">
                    <input type="checkbox" name="barcodeReview" id="barcodeReview" value="1">
                    <label style="color: red" for="barcodeReview" class="translate" >Review import list</label>
                </div>
                <div class="col-md-12">
                    <button style="color: red" class="btn btn-primary translate">Import Barcode</button>
                </div>
            <?php } ?>
                
                <div class="clearfix" ></div>
                <h4>Server of exchange</h4>
                
                <div class="col-md-12">                
                    <label for="bestsellerUser" class="label-style translate">Web Service URL</label>
                    <input type="text" class="form-control" name="webServiceUrl" id="webServiceUrl" placeholder="Ex: https://'exchangeserver'/ews/Exchange.asmx">

                    <label for="bestsellerUser" class="label-style translate">User</label>
                    <input type="text" class="form-control" name="webServiceUser" id="webServiceUser">

                    <label for="bestsellerPassword" class="label-style translate">Password</label>
                    <input type="password" class="form-control" name="webServicePassword" id="webServicePassword">
                </div>
                
            </div>
            
            <div class="col-md-6">
                <h4>Import Bestseller (XML Simple)</h4>
                <div class="col-md-12">
                    <label for="bestsellerFile" class="label-style translate">Local Catalog</label>
                    <input type="file" id="bestsellerFile">

                    <label for="bestsellerServer" class="label-style translate">Server</label>
                    <input type="text" class="form-control" name="bestsellerServer" id="bestsellerServer">

                    <label for="bestsellerUser" class="label-style translate">User</label>
                    <input type="text" class="form-control" name="bestsellerUser" id="bestsellerUser">

                    <label for="bestsellerPassword" class="label-style translate">Password</label>
                    <input type="password" class="form-control" name="bestsellerPassword" id="bestsellerPassword">

                    <label for="bestsellerFtpCatalog" class="label-style translate">FTP Catalog</label>
                    <input type="text" min="0" step="1" class="form-control" name="bestsellerFtpCatalog" id="bestsellerFtpCatalog">
                    <br>
                    <a onclick="importBestseller()" class="btn btn-primary translate">Import Bestseller</a>
                </div>
            </div>
            
        </div>
        
        <div class="row row-fluid" style="padding-bottom: 50px">
            <div class="col-md-6">
                
                
                
            </div>
            
        </div>
        <div class="btns-footer">
            <div class="pull-left">
                <button style="color: red" class="btn btn-primary">System Booking</button>
            </div>
            <div class="pull-right">
                <button class="btn btn-primary">Cancel</button>
                <button class="btn btn-success" type="submit" onclick="insertIntegration()" >Save</button>
            </div>
        </div>
    </form>
</div>
