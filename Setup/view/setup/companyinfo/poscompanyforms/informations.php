<?php

/**
 * Description of informations
 * 
 * @abstract file created in 25/01/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 */

/*@var $pc PosCompany*/
$pc = $view["posCompany"];
$pcd = $view["pcdArray"];
if($pc){
    $name = $pc->getName();
    $fantasyName = $pc->getFantasyName();
    $registerNumber = $pc->getRegisterNumber();
    $addressStreet = $pc->getAddressStreet();
    $addressNumber = $pc->getAddressNumber();
    $addressDistrict = $pc->getAddressDistrict();
    $address_city = $pc->getAddressCity();
    $address_zipCode = $pc->getAddressZipcode();
    $address_complement = $pc->getAddressComplement();
    $address_reference = $pc->getAddressReference();
    $vaddress_street = $pc->getVaddressStreet();
    $vaddress_number = $pc->getVaddressNumber();
    $vaddress_district = $pc->getVaddressDistrict();
    $vaddress_city = $pc->getVaddressCity();
    $vaddress_zipCode = $pc->getVaddressZipcode();
    $vaddress_complement = $pc->getVaddressComplement();
    $vaddress_reference = $pc->getVaddressReference();
} else {
    $name = "";
    $fantasyName = "";
    $registerNumber = "";
    $addressStreet = "";
    $addressNumber = "";
    $addressDistrict = "";
    $address_city = "";
    $address_zipCode = "";
    $address_complement = "";
    $address_reference = "";
    $vaddress_street = "";
    $vaddress_number = "";
    $vaddress_district = "";
    $vaddress_city = "";
    $vaddress_zipCode = "";
    $vaddress_complement = "";
    $vaddress_reference = "";
}

?>

<div class="col-md-12">
    <h4>Informations</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <label for="name">Name</label>
                <input type="text" value="<?php echo $name ?>" class="form-control span-full" 
                       name="pc_name" id="name" placeholder="Company Name" maxlength="100" required="true">                
            </div>

            <div class="col-md-6">
                <label for="name">Fantasy Name</label>
                <input type="text" value="<?php echo $fantasyName ?>" class="form-control span-full" 
                       name="pc_fantasy_name" id="fantasy_name" placeholder="Company Fantasy Name" maxlength="100" required="true">    
            </div>

            <div class="col-md-4">
                <label for="companyId">Register Number</label>
                <input type="text" value="<?php echo $registerNumber ?>" class="form-control span-full" 
                       name="pc_register_number" id="register_number" placeholder="Company Register Number" maxlength="45" required="true">
            </div>
            <div class="col-md-4">
                <label for="phone">Phone</label>
                <input type="text" value="<?php if(array_key_exists("phone", $pcd)){echo $pcd["phone"];} ?>"
                       class="form-control" name="pcd_phone" id="phone" placeholder="Company Phone" required="true">
            </div>
            <div class="col-md-4">
                <label for="fax">Fax</label>
                <input type="text" value="<?php if(array_key_exists("fax", $pcd)){echo $pcd["fax"];} ?>"
                       class="form-control" name="pcd_fax" id="fax" placeholder="Fax" required="true">
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <h4>Address</h4>
            <div class="col-md-12">
                <label for="address_street">Address Street</label>
                <input type="text" value="<?php echo $addressStreet ?>" class="form-control span-full" 
                       name="pc_address_street" id="address_street" placeholder="Address Street" maxlength="100" required="true">
            </div>

            <div class="col-md-12">
                <label for="address_number">Address Number</label>
                <input type="number" value="<?php echo $addressNumber; ?>" class="form-control" 
                       name="pc_address_number" id="address_number" placeholder="Address Number">
            </div>

            <div class="col-md-12">
                <label for="address_district">Address District</label>
                <input type="text" value="<?php echo $addressDistrict ?>" class="form-control" 
                       name="pc_address_district" id="address_district" placeholder="Address District" maxlength="45" required="true">
            </div>

            <div class="col-md-12">
                <label for="address_city">Address City</label>
                <input type="text" value="<?php echo $address_city ?>" class="form-control" 
                       name="pc_address_city" id="address_city" placeholder="Address City" maxlength="100" required="true">
            </div>

            <div class="col-md-12">
                <label for="address_zipCode">ZIP Code</label>
                <input type="text" value="<?php echo $address_zipCode; ?>" class="form-control" 
                       name="pc_address_zipCode" id="address_zipCode" placeholder="Address Zip Code" maxlength="45" required="true">
            </div>

            <div class="col-md-12">
                <label for="address_complement">Addres Complement</label>
                <input type="text" value="<?php echo $address_complement; ?>" class="form-control" 
                       name="pc_address_complement" id="address_complement" placeholder="Address Complement" maxlength="200">
            </div>

            <div class="col-md-12">
                <label for="address_reference">Address Reference</label>
                <input type="text" value="<?php echo $address_reference; ?>" class="form-control" 
                       name="pc_address_reference" id="address_reference" placeholder="Address Reference" maxlength="200">
            </div>
        </div>
        <div class="col-md-6">
            <h4>Visiting Address</h4>
            <div class="col-md-12">
                <label for="vaddress_street">Visiting Address Street</label>
                <input type="text" value="<?php echo $vaddress_street; ?>" class="form-control" 
                       name="pc_vaddress_street" id="vaddress_street" placeholder="Visiting Address Street" maxlength="100" required="true">
            </div>

            <div class="col-md-12">
                <label for="vaddress_number">Visiting Address Number</label>
                <input type="number" value="<?php echo $vaddress_number; ?>"class="form-control" 
                       name="pc_vaddress_number" id="vaddress_number" placeholder="Visiting Address Number" maxlength="45" required="true">
            </div>

            <div class="col-md-12">
                <label for="vaddress_district">Visiting Address District</label>
                <input type="text" value="<?php echo $vaddress_district; ?>" class="form-control" 
                       name="pc_vaddress_district" id="vaddress_district" placeholder="Visiting Address District" maxlength="100" required="true">
            </div>

            <div class="col-md-12">
                <label for="vaddress_city">Visiting Address City</label>
                <input type="text" value="<?php echo $vaddress_city; ?>" class="form-control" 
                       name="pc_vaddress_city" id="vaddress_city" placeholder="Visiting Address City" maxlength="100" required="true">
            </div>

            <div class="col-md-12">
                <label for="vaddress_zipCode">ZIP Code</label>
                <input type="text" value="<?php echo $vaddress_zipCode; ?>" class="form-control" 
                       name="pc_vaddress_zipCode" id="vaddress_zipCode" placeholder="Visiting Address Zip Code" maxlength="45" required="true">
            </div>

            <div class="col-md-12">
                <label for="vaddress_complement">Visiting Address Complement</label>
                <input type="text" value="<?php echo $vaddress_complement; ?>" class="form-control" 
                       name="pc_vaddress_complement" id="vaddress_complement" placeholder="Visiting Address Complement" maxlength="200">
            </div>

            <div class="col-md-12">
                <label for="vaddress_reference">Visiting Address Reference</label>
                <input type="text" value="<?php echo $vaddress_reference; ?>" class="form-control" 
                       name="pc_vaddress_reference" id="vaddress_reference" placeholder="Visinting Address Reference" maxlength="200">
            </div>       
        </div>
    </div>
</div>