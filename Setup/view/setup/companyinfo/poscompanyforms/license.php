<?php

/**
 * Description of license
 * 
 * @abstract file created in 25/01/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 */

/*@var $pc PosCompany*/
    $pc = $view["posCompany"];
    if($pc){
        $license = $pc->getLicense();
    } else {
        $license = "";
    }
?>

<div class="col-md-6">
    <h4>License</h4>
    <div class="form-group">
        <label for="test" class="label-style">License number</label>
        <input type="text" class="form-control" value="<?php echo $license; ?>"
               name="pc_license" id="license" placeholder="License number">
    </div>
    <div class="form-group">
        <label for="test" class="label-style">Validad</label>
        <label id="validad"></label>
    </div>                            
    <div class="form-group">
        <label for="test" class="label-style">Number of Users</label>
        <label id="numberUsers"></label>
    </div>
    <div class="form-group">
        <label for="test" class="label-style">Number of Terminals</label>
        <label id="numberTerminals"></label>
    </div>
    <div class="form-group">
        <label for="test" class="label-style">Number of Mobiles Terminals</label>
        <label id="numberMobileTerminals"></label>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-primary">Check Modules</button>
            </div>
        </div>
    </div>
    <h4>Default Message</h4>
    <div class="form-group">
        <textarea style="resize: none;" class="form-control" placeholder="Default message of invoice" name="_pc_defaultMessage" rows="8"></textarea>
    </div>
</div> 