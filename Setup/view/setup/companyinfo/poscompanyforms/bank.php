<?php

/**
 * Description of bank
 * 
 * @abstract file created in 25/01/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 */

/*@var $pc PosCompany*/
$pc = $view["posCompany"];

$pcd = $view["pcdArray"];


/*@var $ci CompanyImage*/
$ci = $view["companyImage"];

?>

<div class="col-md-6">
    <h4>Bank</h4>
    <div class="col-md-12">
        <label for="bankAccountNumber">Bank Account Number</label>
        <input type="text" value="<?php if(array_key_exists("bank_account", $pcd)){echo $pcd["bank_account"];} ?>" 
               class="form-control" name="pcd_bank_account" id="bank_account" placeholder="Bank Account Number">
    </div>
    <div class="col-md-12">
        <label for="iban">IBAN</label>
        <input type="text" value="<?php if(array_key_exists("iban", $pcd)){echo $pcd["iban"];} ?>"
               class="form-control" name="pcd_iban" id="iban" placeholder="IBAN">
    </div>
    <div class="col-md-12">
        <label for="bic">BIC(Swift)</label>
        <input type="text" value="<?php if(array_key_exists("bic", $pcd)){echo $pcd["bic"];} ?>"
               class="form-control" name="pcd_bic" id="bic" placeholder="BIC(Swift)">
    </div>
    <div class="col-md-12">
        <label for="email">Email Address</label>
        <input type="email" value="<?php if(array_key_exists("email", $pcd)){echo $pcd["email"];} ?>"
               class="form-control" name="pcd_email" id="email" placeholder="Email Address" required="true">
    </div>            
    <div class="col-md-12">
        <div class="form-group">
            <label for="logo">LOGO</label>
        </div> 
        <div class="form-group">
            <div class="thumb-Empty">                
                <?php if($ci != NULL && $ci != FALSE && $ci != "" && !empty($ci)){?>
                    <img id="view-img" src="<?php echo $ci->getImageSrc()?>"/>
                <?php }else{?>
                    <i class="lnr lnr-picture"></i>
                <?php };?>                    
                    <input onchange="previewCompanyImage(this)" type="file" id="pcl_image" name="pcl_image" style="display: none;">
            </div>            
        </div> 
        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <input type="button" class="form-control btn btn-primary" value="Upload" name="upload" id="upload" onclick="$('#pcl_image').click()">
                </div>
                <div class="col-md-6">
                    <input type="button" class="form-control btn btn-danger" value="Delete" name="delete" id="delete">
                </div>
            </div>                   
        </div>     
    </div>
</div>