<div id="informations" class="tab-pane active">
    <div class="row">
        <div class="col-md-3">
            <img width="160" style="margin-left: 30px; margin-top: 15px; cursor:pointer" id="image_upload_preview" src="../../../assets/images/upload.png" alt="your image" onclick="$('#uploadImage').click()" />
            <input type="file" id="uploadImage" name="uploadImage" style="display: none" onchange="loadImageFileAsURL()">
            <input type="text" id="image" name="image" style="display: none">
        </div>
        
        <div class="col-md-9" style="margin-top: 15px">
            <div class="col-md-6">
                <label for="name" class="translate">Nome</label>
                <input type="text" class="form-control" name="name" id="name" maxlength="100" required="true">                
            </div>

            <div class="col-md-6">
                <label for="fantasy_name" class="translate">Nome fantasia</label>
                <input type="text" class="form-control" name="fantasy_name" id="fantasy_name" maxlength="100" required="true">    
            </div>
            
            <div class="col-md-4">
                <label for="register_number" class="translate">CNPJ</label>
                <input type="text" class="form-control text" name="register_number2" id="register_number2"  required="true">
            </div>
            <div class="col-md-4">
                <label for="phone_company" class="translate">Telefone</label>
                <input type="text" class="form-control number" name="phone_company" id="phone_company" required="true">
            </div>
            <div class="col-md-4">
                <label for="fax_company" class="translate">PDV número</label>
                <input type="text" class="form-control text" name="fax_company2" id="fax_company2" required="true">
            </div>
           <!-- <div class="col-md-12">
                <h4 class="translate">Default Message</h4>
                <div class="form-group">
                    <textarea style="resize: none;" class="form-control" placeholder="Default message of invoice" id="default_message_invoice" name="default_message_invoice" rows="8" maxlength="235"></textarea>
                </div>
            </div>-->
        </div>
       
         
    </div>
     <!--<div class="container">
            <div class="row">
                 <div class="col-md-3">
                <label for="fantasy_name" class="translate">Número PDV</label>
                  <input type="number" class="form-control number" name="pdvnumber" id="pdvnumber" min="0" required="true">
                 </div>
                </div>
        </div>-->
</div>

<script type="text/javascript">
    function loadImageFileAsURL(){
        var filesSelected = document.getElementById("uploadImage").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image').val(fileLoadedEvent.target.result);
                $('#image_upload_preview').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

</script>