<?php $stateOB = getEm()->getRepository('ZzState')->findAll();?>

<div id="address" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <h4 class="translate">Address</h4>
                <div class="col-md-12">
                    <label for="address_street" class="translate">Address Street</label>
                    <input type="text" class="form-control" name="address_street" id="address_street" maxlength="100" required="true">
                </div>

                <div class="col-md-12">
                    <label for="address_number" class="translate">Address Number</label>
                    <input type="text" class="form-control number" name="address_number" id="address_number">
                </div>

                <div class="col-md-12">
                    <label for="address_district" class="translate">Address District</label>
                    <input type="text" class="form-control" name="address_district" id="address_district" maxlength="45" required="true">
                </div>

                <div class="col-md-12">
                    <label for="address_city" class="translate">Address City</label>
                    <input type="text" class="form-control" name="address_city" id="address_city" maxlength="100" required="true">
                </div>
                
                <div class="col-md-12">
                    <label for="address_city">Address State</label>
                    <select required="required" name="address_state" id="address_state">
                        <!--<option value="0">Select option</option>-->
                        <?php //echo $countrys ?>
                         <option value="0" class="translate">Select option</option>
                        <?php //echo $customerGps ?>
                        <?php
                    foreach ($stateOB as $valueStateOB):
                        ?><option value="<?php echo $valueStateOB->getIdzzState(); ?>"><?php echo $valueStateOB->getName(); ?></option>
                        <?php
                    endforeach;
                    ?>
                    </select>
                </div>

                <div class="col-md-12">
                    <label for="address_zipCode" class="translate">ZIP Code</label>
                    <input type="text" class="form-control number" name="address_zipCode" id="address_zipCode" maxlength="45" required="true">
                </div>

                <div class="col-md-12">
                    <label for="address_complement" class="translate">Address Complement</label>
                    <input type="text" class="form-control" name="address_complement" id="address_complement" maxlength="200">
                </div>

                <div class="col-md-12">
                    <label for="address_reference" class="translate">Address Reference</label>
                    <input type="text" class="form-control" name="address_reference" id="address_reference" maxlength="200">
                </div>
            </div>
            <div class="col-md-6">
                <h4 class="translate">Visiting Address</h4>
                <div class="col-md-12">
                    <label for="vaddress_street" class="translate">Visiting Address Street</label>
                    <input type="text" class="form-control" name="vaddress_street" id="vaddress_street" maxlength="100" required="true">
                </div>

                <div class="col-md-12">
                    <label for="vaddress_number" class="translate">Visiting Address Number</label>
                    <input type="text" class="form-control number" name="vaddress_number" id="vaddress_number" maxlength="45" required="true">
                </div>

                <div class="col-md-12">
                    <label for="vaddress_district" class="translate">Visiting Address District</label>
                    <input type="text" class="form-control" name="vaddress_district" id="vaddress_district" maxlength="100" required="true">
                </div>

                <div class="col-md-12">
                    <label for="vaddress_city" class="translate">Visiting Address City</label>
                    <input type="text" class="form-control" name="vaddress_city" id="vaddress_city" maxlength="100" required="true">
                </div>

                <div class="col-md-12">
                    <label for="vaddress_zipCode" class="translate">ZIP Code</label>
                    <input type="text" class="form-control number" name="vaddress_zipCode" id="vaddress_zipCode" maxlength="45" required="true">
                </div>

                <div class="col-md-12">
                    <label for="vaddress_complement" class="translate">Visiting Address Complement</label>
                    <input type="text" class="form-control" name="vaddress_complement" id="vaddress_complement" maxlength="200">
                </div>

                <div class="col-md-12">
                    <label for="vaddress_reference" class="translate">Visiting Address Reference</label>
                    <input type="text" class="form-control" name="vaddress_reference" id="vaddress_reference" maxlength="200">
                </div>      
            </div>
        </div>
    </div>
</div>