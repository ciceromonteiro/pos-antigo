<script type="text/javascript">
    function typeBank(){
        var value = $("#bank").val();
        $("#ItauSpecie").css("display","none");
        $("#BBspecie").css("display","none");
        $("#cefSpecie").css("display","none");

        if(value == '1'){
            $("#ItauSpecie").css("display","block");
            $('#bankCef').val("0");
            $('#bankBB').val("0");

            $('#code_agreement_br').val("");
            $('#wallet_variation_br').val("");
            $('#wallet_code_br').val("0");
            $('#registered_invoice_br').val("0");
            $('#agency_dv_br').val("");
            $('#operation_br').val("");
            $('#sequence_number_file_br').val("");
            $('#aceite_br').val("");
            $('#assignor_code_br').val("");
            $('#agency_dv_br_cef').val("");
            $('#operation_br_cef').val("");
            $('#sequence_number_file_br_cef').val("");

        }else if(value == '2'){
            $("#BBspecie").css("display","block");
            $('#bankItau').val("0");
            $('#bankCef').val("0");
            $('#agency_dv_br_cef').val("");
            $('#operation_br_cef').val("");
            $('#sequence_number_file_br_cef').val("");

        }else if(value == '3'){
            $("#cefSpecie").css("display","block");
            $('#bankItau').val("0");
            $('#bankBB').val("0");

            $('#code_agreement_br').val("");
            $('#wallet_variation_br').val("");
            $('#wallet_code_br').val("0");
            $('#registered_invoice_br').val("0");
            $('#agency_dv_br').val("");
            $('#operation_br').val("");
            $('#sequence_number_file_br').val("");
            $('#aceite_br').val("");
        }

    }


    function enableBBSpecies(){
        var value = $("#bank_wallet_br").val();
            $("#bb1").attr("disabled", true);
            $("#bb2").attr("disabled", true);
            $("#bb4").attr("disabled", true);
            $("#bb6").attr("disabled", true);
            $("#bb7").attr("disabled", true);
            $("#bb12").attr("disabled", true);
            $("#bb17").attr("disabled", true);
            $("#bb19").attr("disabled", true);
            $("#bb26").attr("disabled", true);
            $("#bb27").attr("disabled", true);
            $("#bb28").attr("disabled", true);
            $("#bb29").attr("disabled", true);
            $("#bb16").attr("disabled", true);
            $("#bb20").attr("disabled", true);

        if(value=="11" || value=="17"){
            $("#bb1").attr("disabled", false);
            $("#bb2").attr("disabled", false);
            $("#bb4").attr("disabled", false);
            $("#bb6").attr("disabled", false);
            $("#bb7").attr("disabled", false);
            $("#bb12").attr("disabled", false);
            $("#bb17").attr("disabled", false);
            $("#bb19").attr("disabled", false);
            $("#bb26").attr("disabled", false);
            $("#bb27").attr("disabled", false);
            $("#bb28").attr("disabled", false);
            $("#bb29").attr("disabled", false);

        }else if(value == "12" ){
            $("#bb2").attr("disabled", false);
            $("#bb4").attr("disabled", false);
            $("#bb7").attr("disabled", false);
            $("#bb12").attr("disabled", false);
            $("#bb17").attr("disabled", false);
            $("#bb19").attr("disabled", false);

        }else if(value == "15"){
            $("#bb16").attr("disabled", false);
            $("#bb20").attr("disabled", false);
        }else if(value == "31"){
            $("#bb2").attr("disabled", false);
            $("#bb4").attr("disabled", false);
        }else if(value == "51"){
            $("#bb2").attr("disabled", false);
            $("#bb4").attr("disabled", false);
            $("#bb7").attr("disabled", false);

        }

    }


</script>

<style type="text/css">
    #table_license_wrapper #table_license_length, #table_license_filter, #table_license_info, #table_license_paginate {
        display: none
    }
</style>
<div id="bank_and_license" class="tab-pane">
    <div class="row row-fluid">
        <div class="col-md-12">
            <?php 
            if ($array_answer["country"] == "BR"){ 
            ?>
           <div class="col-md-6">
                <h4 class="translate">Bank</h4>
                <div class="col-md-12">
                    <label for="bank" class="translate">Bank</label>
                    <select name="bank" id="bank" required="true" onchange="typeBank()">
                        <option value="0">Select Option</option>
                        <option value="1">ITAU</option>
                        <option value="2">BANCO DO BRASIL</option>
                        <option value="3" disabled="true">CEF</option>
                        <option value="4" disabled="true">SANTANDER</option>
                        <option value="5" disabled="true">BRADESCO</option>
                    </select>
                </div>
                <div class="col-md-12">
                    <label for="agency">Agency</label>
                    <input type="text" class="form-control number" name="agency" id="agency">
                </div>
                <div class="col-md-12">
                    <label for="account">Account</label>
                    <input type="text" class="form-control number" name="account" id="account">
                </div>
                <div class="col-md-12">
                    <label for="digit_account" class="translate">Digit Account</label>
                    <input type="email" class="form-control" name="digit_account" id="digit_account" required="true">
                </div>
                <div class="col-md-12">
                    <label for="bank_wallet_br" class="translate">Wallet Bank</label>
                    <input type="email" class="form-control" name="bank_wallet_br" id="bank_wallet_br" required="true" onkeyup="enableBBSpecies()">
                </div>
                <div class="col-md-12" id="ItauSpecie" style="display: none;">
                    <label for="bankItau" class="translate">Itau Specie</label>
                    <select name="bankItau" id="bankItau" required="true">
                        <option value="0">Select Option</option>
                        <option value="1">ITAU DUPLICATE MERCANTILE</option>
                        <option value="2">ITAU NOTE PROMISSORY</option>
                        <option value="3">ITAU NOTE INSURANCE</option>
                        <option value="4">ITAU MONTHLY PAYMENT OF SCHOOL</option>
                        <option value="5">ITAU RECEIPT</option>
                        <option value="6">ITAU CONTRACT</option>
                        <option value="7">ITAU COSSEGUROS</option>
                        <option value="8">ITAU DUPLICATE OF SERVICE</option>
                        <option value="9">ITAU LETTER EXCHANGE</option>
                        <option value="13">ITAU NOTE DEBIT</option>
                        <option value="15">ITAU DOCUMENT OF DEBT</option>
                        <option value="16">ITAU CHARGE CONDOMINIUMS</option>
                        <option value="17">ITAU CONTRACT SUPPLY OF SERVICES</option>
                        <option value="99">ITAU SEVERAL</option>
                    </select>
                </div>
                <div class="col-md-12" id="BBspecie" style="display: none;">
                    <label for="bankBB" class="translate">BB Specie</label>
                    <select name="bankBB" id="bankBB" required="true">
                        <option value="0">Select Option</option>
                        <option id="bb1" value="1">BB CHECK</option>
                        <option id="bb2" value="2">BB DUPLICATE MERCANTILE</option>
                        <option id="bb4" value="4">BB DUPLICATE OF SERVICE</option>
                        <option id="bb6" value="6">BB DUPLICATE RURAL</option>
                        <option id="bb7" value="7">BB LETTER EXCHANGE</option>
                        <option id="bb12" value="12">BB NOTE PROMISSORY</option>
                        <option id="bb17" value="17">BB RECEIPT</option>
                        <option id="bb19" value="19">BB NOTE DEBIT</option>
                        <option id="bb26" value="26">BB WARRANT</option>
                        <option id="bb27" value="27">BB DEBT ACTIVE OF STATE</option>
                        <option id="bb28" value="28">BB DEBT ACTIVE OF CITY</option>
                        <option id="bb29" value="29">BB DEBT ACTIVE UNION</option>
                        <option id="bb16" value="16">BB NOTE INSURANCE</option>
                        <option id="bb20" value="20">BB POLICY INSURANCE</option>
                    </select>
                    <label for="code_agreement_br" class="translate">Code Agreement</label>
                    <input type="text" class="form-control" name="code_agreement_br" id="code_agreement_br" required="true">
                    <label for="wallet_variation_br" class="translate">Wallet Variation</label>
                    <input type="text" class="form-control" name="wallet_variation_br" id="wallet_variation_br" required="true">
                    <label for="wallet_code_br" class="translate">Wallet Code</label>
                    <select name="wallet_code_br" id="wallet_code_br" required="true">
                        <option value="0">Select Option</option>
                        <option id="wc1" value="1">COBRANCA SIMPLES</option>
                        <option id="wc2" value="2">COBRANCA VINCULADA</option>
                        <option id="wc3" value="3">COBRANCA CAUCIONADA</option>
                        <option id="wc4" value="4">COBRANCA DESCONTADA</option>
                        <option id="wc5" value="5">COBRANCA VENDOR</option>
                    </select>
                    <label for="registered_invoice_br" class="translate">Registered Invoice</label>
                    <select name="registered_invoice_br" id="registered_invoice_br" required="true">
                        <option value="0">Select Option</option>
                        <option id="ri1" value="1">REGISTERED</option>
                        <option id="ri2" value="2">NOT REGISTERED</option>
                    </select>
                    <label for="agency_dv_br" class="translate">Digit Agency</label>
                    <input type="text" class="form-control" name="agency_dv_br" id="agency_dv_br" required="true">
                    <label for="operation_br" class="translate">Operation</label>
                    <input type="text" class="form-control" name="operation_br" id="operation_br" required="true">
                    <label for="sequence_number_file_br" class="translate">Sequence Number File</label>
                    <input type="text" class="form-control" name="sequence_number_file_br" id="sequence_number_file_br" required="true">
                    <label for="aceite_br" class="translate">Aceite</label>
                    <input type="text" class="form-control" name="aceite_br" id="aceite_br" required="true">
                </div>
                <div class="col-md-12" id="cefSpecie" style="display: none;">
                    <label for="bankCef" class="translate">Cef Specie</label>
                    <select name="bankCef" id="bankCef" required="true">
                        <option value="0">Select Option</option>
                        <option value="2">CEF DUPLICATA MERCANTIL</option>
                        <option value="12">CEF NOTA PROMISSORIA</option>
                        <option value="4">CEF DUPLICATA DE PRESTACAO DE SERVICOS</option>
                        <option value="16">CEF NOTA DE SEGURO</option>
                        <option value="7">CEF LETRA DE CAMBIO</option>
                        <option value="99">CEF OUTROS</option>
                    </select>
                    <label for="assignor_code_br" class="translate">Assignor Code</label>
                    <input type="text" class="form-control" name="assignor_code_br" id="assignor_code_br" required="true">
                    <label for="agency_dv_br_cef" class="translate">Digit Agency</label>
                    <input type="text" class="form-control" name="agency_dv_br_cef" id="agency_dv_br_cef" required="true">
                    <label for="operation_br_cef" class="translate">Operation</label>
                    <input type="text" class="form-control" name="operation_br_cef" id="operation_br_cef" required="true">
                    <label for="sequence_number_file_br_cef" class="translate">Sequence Number File</label>
                    <input type="text" class="form-control" name="sequence_number_file_br_cef" id="sequence_number_file_br_cef" required="true">
                </div>
                <div class="col-md-8">
                    <label for="interestDay" class="translate">Interest per day after date of maturity invoices:</label>
                    <div class="input-group">
                        <input type="number" name="interestDay" id="interestDay" class="form-control" max='100' required>
                        <span class="input-group-addon">%</span>
                    </div>
                </div>
                <div class="col-md-12">
                    <label for="deadlineDay" class="translate">Days for payment after date of maturity invoices:</label>
                    <input type="text" class="form-control number" name="deadlineDay" id="deadlineDay" required="true">
                </div>
                <div class="col-md-12">
                    <label for="valueMulct" class="translate">Value of mulct:</label>
                    <input type="text" class="form-control money" name="valueMulct" id="valueMulct" required="true">
                </div>
            </div>

            <?php
            }else{
            ?>    
            <div class="col-md-6">
                <h4 class="translate">Bank</h4>
                <div class="col-md-12">
                    <label for="bank_account" class="translate">Bank Account Number</label>
                    <input type="text" class="form-control number" name="bank_account" id="bank_account">
                </div>
                <div class="col-md-12">
                    <label for="iban">IBAN</label>
                    <input type="text" class="form-control number" name="bank_iban" id="bank_iban">
                </div>
                <div class="col-md-12">
                    <label for="bic">BIC(Swift)</label>
                    <input type="text" class="form-control number" name="bank_bic" id="bank_bic">
                </div>
                <div class="col-md-12">
                    <label for="email" class="translate">Email Address</label>
                    <input type="email" class="form-control" name="bank_email" id="bank_email" required="true">
                </div>
            </div>
            <?php } ?>
            
            <div class="col-md-6">
                <h4 class="translate">License</h4>
                <div class="form-group">
                    <label for="test" class="label-style translate">License number</label>
                    <input type="text" class="form-control" name="license_key" id="license_key" disabled="true">
                </div>
                <div class="form-group">
                    <p>
                        <b class="translate">Validad:</b> <span id="validad_license"></span><br>
                        <b class="translate">Number of Companies:</b> <span id="validad_number_of_companies"></span><br>
                        <b class="translate">Number of Users:</b> <span id="validad_of_users"></span><br>
                        <b class="translate">Number of Terminals:</b> <span id="validad_of_terminals"></span><br>
                        <b class="translate">Number of Mobiles Terminals:</b> <span id="validad_of_mobiles_terminals"></span>
                    </p>
                </div> 
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary translate" value="" id="checkModules">Check Modules</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="modal_modules" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Modules for your license</h4>
            </div>
            <form id="form_color">
                <div class="modal-body">
                    <div class="row">
                        <table id="table_license" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="translate">Description</th>
                                <th class="translate">Status</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        function getData() {
            $.ajax({
                url: my_url + "Setup/company/getLicense",
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        var values = data.data[0];
                        document.getElementById('license_key').value = values.key_license;
                        document.getElementById('validad_license').innerHTML = values.validad;
                        document.getElementById('validad_number_of_companies').innerHTML = values.number_of_companies;
                        document.getElementById('validad_of_users').innerHTML = values.limit_users;
                        document.getElementById('validad_of_terminals').innerHTML = values.limit_connections;
                        document.getElementById('validad_of_mobiles_terminals').innerHTML = values.limit_terminals;
                        document.getElementById('validad_of_mobiles_terminals').innerHTML = values.limit_mobiles;
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }

        $(document).on('click', '#checkModules', function(e){
            e.preventDefault();
            $('#modal_modules').modal({
                show : true
            });
        });
        
        var table_license = $('#table_license').DataTable( {
            "ajax": {"url": my_url+"Setup/company/getFeaturesInLicense/"},
            "columns": [
                { "data": "description" },
                { "data": "status" }
            ],
            "paginate": false,
            "language": {
                "url": my_url+my_language+".json"
            }
        });
        
        getData();
    });
</script>