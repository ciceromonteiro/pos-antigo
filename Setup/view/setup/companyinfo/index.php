<?php //require "../module/Setup/view/setup/nav/menu.php"; ?>


<script type="text/javascript">
    $(document).ready(function () {
        function getData() {
            $.ajax({
                url: my_url + "Setup/company/getCompanyInfo",
                type: "POST",
                dataType: 'JSON',
                data: '',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        var values = data.data[0];
                        for (var prop in values) {
                            console.log(prop);
                            if (prop == "image") {
                                //image
                                if (values[prop] == "" || values[prop] == null) {
                                    $('#image_upload_preview').attr('src', '../../../assets/images/upload.png');
                                } else {
                                    $('#image_upload_preview').attr('src', values[prop]);
                                    $('#image').attr('value', values[prop]);
                                }
                            } else if (document.getElementById(prop).value != null && prop != "license_key" && prop != "image") {
                                //inputs
                                document.getElementById(prop).value = values[prop];
                            }
                        }

                        typeBank();
                        enableBBSpecies();
                    } else {
                        notification(false);
                        
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                   
                }
            });
        }

        document.getElementById("form_Setup_company_info").reset();
        getData();
    });
    
    function saveInfo(){
        var values = {};
        values['name'] = $('#name').val();
        values['fantasy_name'] = $('#fantasy_name').val();
        values['register_number2'] = $('#register_number2').val();
        values['phone_company'] = $('#phone_company').val();
        values['fax_company2'] = $('#fax_company2').val();
       // values['pdvnumber'] = $('pdvnumber').val();
       // values['default_message_invoice'] = $('#default_message_invoice').val();
        //console.log($('#default_message_invoice').val());

       // values['address_street'] = $('#address_street').val();
       // values['address_number'] = $('#address_number').val();
      //  values['address_district'] = $('#address_district').val();
      //  values['address_city'] = $('#address_city').val();
       // values['address_state'] = $('#address_state').val();
      //  values['address_zipCode'] = $('#address_zipCode').val();
      //  values['address_complement'] = $('#address_complement').val();
       // values['address_reference'] = $('#address_reference').val();

     //   values['vaddress_street'] = $('#vaddress_street').val();
     //   values['vaddress_number'] = $('#vaddress_number').val();
     //   values['vaddress_district'] = $('#vaddress_district').val();
      //  values['vaddress_city'] = $('#vaddress_city').val();
     //   values['vaddress_zipCode'] = $('#vaddress_zipCode').val();
    //    values['vaddress_complement'] = $('#vaddress_complement').val();
    //    values['vaddress_reference'] = $('#vaddress_reference').val();
            //Em outros países
   /*     if(document.getElementById('bank_account') != null){
            values['bank_account'] = $('#bank_account').val();
        }

        if(document.getElementById('bank_iban') != null){
            values['bank_iban'] = $('#bank_iban').val();
        }

        if(document.getElementById('bank_bic') != null){
            values['bank_bic'] = $('#bank_bic').val();
        }

        if(document.getElementById('bank_email') != null){
            values['bank_email'] = $('#bank_email').val();
        }     

        //Para o Brasil 
        if(document.getElementById('bank') != null){
            values['bank'] = $('#bank').val();
        }

        if(document.getElementById('agency') != null){
            values['agency'] = $('#agency').val();
        }

        if(document.getElementById('account') != null){
            values['account'] = $('#account').val();
        }

        if(document.getElementById('digit_account') != null){
            values['digit_account'] = $('#digit_account').val();
        }  

        if(document.getElementById('bank_wallet_br') != null){
            values['bank_wallet_br'] = $('#bank_wallet_br').val();
        }  

        if(document.getElementById('bankItau') != null || document.getElementById('bankCef') != null ||document.getElementById('bankBB') != null){
            if($('#bankItau').val() != 0){
                values['bankSpecie'] = $('#bankItau').val();
            }else if($('#bankCef').val() != 0){
                values['bankSpecie'] = $('#bankCef').val();
            }else if($('#bankBB').val() != 0){
                values['bankSpecie'] = $('#bankBB').val();
            }
        }

        if(document.getElementById('interestDay') != null){
            values['interestDay'] = $('#interestDay').val();
        }  

        if(document.getElementById('deadlineDay') != null){
            values['deadlineDay'] = $('#deadlineDay').val();
        }  

        if(document.getElementById('valueMulct') != null){
            values['valueMulct'] = $('#valueMulct').val();
        }  

        if(document.getElementById('code_agreement_br') != null){
            values['code_agreement_br'] = $('#code_agreement_br').val();
        }  

        if(document.getElementById('wallet_variation_br') != null){
            values['wallet_variation_br'] = $('#wallet_variation_br').val();
        }  

        if(document.getElementById('agency_dv_br') != null){
            values['agency_dv_br'] = $('#agency_dv_br').val();
        }  

        if(document.getElementById('operation_br') != null){
            values['operation_br'] = $('#operation_br').val();
        }  

        if(document.getElementById('sequence_number_file_br') != null){
            values['sequence_number_file_br'] = $('#sequence_number_file_br').val();
        }  
        
        if(document.getElementById('wallet_code_br') != null){
            values['wallet_code_br'] = $('#wallet_code_br').val();
        }  

        if(document.getElementById('registered_invoice_br') != null){
            values['registered_invoice_br'] = $('#registered_invoice_br').val();
        } 

        if(document.getElementById('aceite_br') != null){
            values['aceite_br'] = $('#aceite_br').val();
        } 

        if(document.getElementById('assignor_code_br') != null){
            values['assignor_code_br'] = $('#assignor_code_br').val();
        } 

        if(document.getElementById('agency_dv_br_cef') != null){
            values['agency_dv_br_cef'] = $('#agency_dv_br_cef').val();
        } 

        if(document.getElementById('operation_br_cef') != null){
            values['operation_br_cef'] = $('#operation_br_cef').val();
        }

        if(document.getElementById('sequence_number_file_br_cef') != null){
            values['sequence_number_file_br_cef'] = $('#sequence_number_file_br_cef').val();
        } 
        */
        values['image'] = $('#image').val();

        var jsonString = JSON.stringify(values);

        $.ajax({
            url : my_url+"Setup/company/updateCompanyInfo/",
            type: "POST",
            dataType: 'JSON',
            data : {data: jsonString},
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(true);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(true);
               
            }
        });
    }
</script>
<p id="teste" ></p>

<div class="content">
    <form id="form_Setup_company_info" name="form_Setup_company_info">
        <div class="row row-fluid" style="margin-bottom: 60px">
            <div class="row">
                <div class="col-md-12">
                    <ul class="nav nav-tabs navbar-three" style="margin: 0px; padding-left: 30px;">
                        <li class="active"><a data-toggle="tab" href="#informations" class="translate">Informations</a></li>
                       <!-- <li><a data-toggle="tab" href="#address" class="translate">Address</a></li>
                        <li><a data-toggle="tab" href="#bank_and_license" class="translate">Bank and License</a></li>
                    -->
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                <?php
                    require 'tabs/informations.php';
                    //require 'tabs/address.php';
                  //  require 'tabs/bank_and_license.php';
                ?>
            </div>
        </div>
        <div class="btns-footer">
            <div class="pull-right">
                <button class="btn btn-primary translate">Cancel</button>
                <button type="submit" class="btn btn-success translate" onclick="saveInfo()">Save</button>
            </div>
        </div>
    </form>
</div>