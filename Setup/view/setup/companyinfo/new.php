<div class="content">
    <div class="row">
        <div class="col-xs-6">
            <div style="overflow: auto; width: auto"> 
                <div style="overflow: auto;  height: 600px; border:solid 1px"> 
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($view['infos']): ?>
                                <?php foreach($view['infos'] as $info): ?>
                                    <tr class="info_line" id_company_info="<?=$info->getIdCompanyInfo()?>">
                                        <td><?=$info->getDataType()?></td>
                                        <td><?=$info->getDataValue()?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-xs-5">
            <form action="" method="post">
                <div class="form-group row">
                    <input type="hidden" id="id_company_info" name="id_company_info" value="new">
                    <label class="col-2 ">Text</label>
                    <div class="col-3">
                        <input class="form-control" type="text"  id="data_type" name="data_type" required="true">
                    </div>
                    <label class="col-2 ">Value</label>
                    <div class="col-3">
                        <textarea class="form-control" id="data_value" name="data_value"  required="true"></textarea>
                    </div>


                </div>
                <div class="row">
                    <div class="text-left">
                        <button class="btn-big btn-default">Submit</button>
                    </div>
                </div>
            </form>
        </div>

    </div>
</div>
<script>
$(function(){
    $(".info_line").click(function(){
        id_company_info = $(this).attr("id_company_info");
        $("#id_company_info").val(id_company_info);

        data_type = $(this).find(':nth-child(1)').html();
        data_value = $(this).find(':nth-child(2)').html();

        $("#data_type").val(data_type);
        $("#data_value").val(data_value);
    });
});
</script>