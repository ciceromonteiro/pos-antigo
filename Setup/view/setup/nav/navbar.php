<?php
$menuLeft = $navbar;
$menu = explode('|', $menuLeft);
?>

<div class="horizon-swiper">
    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Setup/Setup/index/company' ?>">
            <div class='<?php echo ($menu[1] == "configuration") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-cog"></span>
                <div class="translate">Configurations</div>
            </div>
        </a>
    </div>

    <!--<div class="horizon-item">
        <a href="<?php echo URL_BASE.'Setup/currency/index' ?>">
            <div class='<?php echo ($menu[1] == "currency") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-list"></span>
                <div class="translate">Currencies</div>
            </div>
        </a>
    </div>
-->
</div>

<script type="text/javascript">
    $('.horizon-swiper').horizonSwiper();
</script>