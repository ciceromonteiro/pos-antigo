<?php
/**
 * Description of menu
 *
 * @author deyvison
 */
$active = array("", "", "", "", "", "", "");

switch ($view["menu-active"]) {
    case "company":
        $active[0] = "active";
        break;
    case "general":
        $active[1] = "active";
        break;
    case "accounting":
        $active[2] = "active";
        break;
    case "additional":
        $active[3] = "active";
        break;
    case "integrations":
        $active[4] = "active";
        break;
    case "database":
        $active[5] = "active";
        break;
    case "external":
        $active[6] = "active";
        break;
}

?>

<ul class="navbar-three">
    <li class="<?php echo $active[0]; ?>"><a href="<?php echo URL_BASE."Setup/posCompany/index" ?>"><t class="translate">Informações da Companhia</t><span class="sr-only">(current)</span></a></li>
    <li class="<?php echo $active[1]; ?>"><a href="<?php echo URL_BASE."Setup/general/index" ?>"><at class="translate">Configurações Geral</t></a></li>
    <!-- <li class="<?php echo $active[2]; ?>"><a href="<?php echo URL_BASE."Setup/accounting/index" ?>"><t class="translate">Accounting</t></a></li> -->
    <!-- <li class="<?php echo $active[3]; ?>"><a href="<?php echo URL_BASE."Setup/additional/index" ?>"><t class="translate">Additional Settings</t></a></li> -->
    <li class="<?php echo $active[4]; ?>"><a href="<?php echo URL_BASE."Setup/integrations/index" ?>"><t class="translate">Integrações</t></a></li>
    <li class="<?php echo $active[5]; ?>"><a href="<?php echo URL_BASE."Setup/database/index" ?>"><t class="translate">Banco de Dados</t></a></li>
    <li class="<?php echo $active[6]; ?>"><a href="<?php echo URL_BASE."Setup/external/index" ?>"><t class="translate">Recursos Externos</t></a></li>               
</ul>
