<?php include __DIR__ . "/../nav/menu.php"; ?>

<script type="text/javascript">

   


    $(document).ready(function () {        
        $(document).on('click', '#create_demo', function(e){
            e.preventDefault();
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: "Confirmation Needed",
                text: "This will erase all your data and create new records?",
                icon: "glyphicon glyphicon-minus",
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false 
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            }))
            .get().on('pnotify.confirm', function(){
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.open("GET", "<?php echo substr(URL_BASE, 0, strlen(URL_BASE) - 7)?>data/script/resetDatabase.php");
                xmlhttp.send();
                alert('Ok');
                $('.ui-pnotify-modal-overlay').remove();
            })
            .on('pnotify.cancel', function(){
                $('.ui-pnotify-modal-overlay').remove();
            });
        });
        
        $(document).on('click', '#reset_database', function(e){
            e.preventDefault();
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: "Confirmation Needed",
                text: "This will erase all your data and you will need to reinstall all devices?",
                icon: "glyphicon glyphicon-minus",
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            }))
            .get().on('pnotify.confirm', function(){
                alert('Ok');
                $('.ui-pnotify-modal-overlay').remove();
            })
            .on('pnotify.cancel', function(){
                $('.ui-pnotify-modal-overlay').remove();
            });
        });

         var table = $('#database').DataTable({
            "ajax": {"url": my_url + "Setup/database/getAllDatabase/"},
            "columns": [
                {"data": "database"},
                {"data": "user"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });


        
        $(document).on('click', '#import_clients', function(e){
            e.preventDefault();
            resetForm('#form_import_clients');
            $('#myModal').modal({
                show : true
            });
        });
        
        $(document).on('submit', '#form_import_clients', function(e){
            e.preventDefault();
            var file = $('#importXML').val 
            var formdata = new FormData($("form[name='form_import']")[0]);
            $.ajax({
                type: 'POST',
                url : my_url+"Setup/Database/importClients/",
                data: formdata,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#myModal').modal('hide');
                        notification(true);
                    } else {
                        notification(false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
    });

    
</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Import Clients</h4>
            </div>
            <form id="form_import_clients" name="form_import" class="form_department">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="importXML" class="label-style">Select XML</label>
                            <input type="file"  id="importXML" name="importXML" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Table List -->
<div class="content">
    <div class="row row-fluid">
        <div class="col-md-6">
            <h4 class="translate">Tools</h4>
            <div class="col-md-12">
                <button id="create_demo" class="form-control btn btn-primary translate">Create demo</button>
                <button class="form-control btn btn-primary translate">Specify number initial of the order of buy</button>
                <button id="reset_database" class="form-control btn btn-primary translate">Reset database</button>
                <button class="form-control btn btn-primary translate">Import clients</button>
                <button style="color: red" class="form-control btn btn-primary translate">Import requests</button>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <label class="label-style translate">Plane of numeration of the items in the system</label>
                <div class="col-md-5">
                    <input type="text" class="form-control" placeholder="1000" name="planeNumerationStart" id="planeNumerationStart">
                </div>
                <div class="col-md-2">
                    <p class="translate">until</p>
                </div>
                <div class="col-md-5">
                    <input type="text" class="form-control" placeholder="9999" name="planeNumerationEnd" id="planeNumerationEnd">
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 15px">
                <button style="color: red" class="form-control btn btn-primary translate">Search for clients sex in base national</button>
                <button style="color: red" class="form-control btn btn-primary translate">Search for clients age in base national</button>
                <button style="color: red" class="form-control btn btn-primary translate">Search for weather information</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4 class="translate">Database existing</h4>
            <table id="database" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">Database</th>
                        <th class="translate">User</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>


