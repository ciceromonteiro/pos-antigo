<script type="text/javascript">

    $(document).ready(function() {
        
        var table = $('#currency').DataTable( {
            "ajax": {"url": my_url+"Setup/Currency/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "iso" },
                { "data": "symbol" },
                { "data": "exchange_rate" },
                { "data": "exchange_unit" },
                { "data": "smallest_denomination" },
                { "data": "account" },
                { "data": "iban" },
                { "data": "bic" },
                { "data": "account_currency_code" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });


        function reload_table(){
            table.ajax.reload(null,false);
        }

        // Add button
        $(document).on('click', '#create', function(e){
            e.preventDefault();
            $('#myModal').modal({
                show : true
            });
        });

        // Select tr
        $(document).on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                $(this).removeClass('selected');
            }
            else {
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );

        // Add submit form
        $(document).on('submit', '#form_color', function(e){
            e.preventDefault();
            var form_data = $('#form_color').serialize();
              e.preventDefault();
            var form_data = $('#form_color').serialize();
        $.ajax({
                url:          '<?php echo URL_BASE ?>Setup/Currency/insert',
                cache:        false,
                data:         form_data,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get',
                   success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){

                        notification(true);

                    } else {
                        notification(false);

                    }
                    $('#myModal').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
          

        // Get for update
        $(document).on('click', '#update', function(e){
            e.preventDefault();
            var id      = table.$('tr.selected').find('a').data('id');
            var request = $.ajax({
                url:          '<?php echo URL_BASE ?>Setup/currency/getCurrency',
                cache:        false,
                data:         'id=' + id,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            request.done(function(output){
                if (output.result == 'success'){
                    $('#form_color_edit #idCurrency').val(output.data[0].idCurrency);
                    $('#form_color_edit #nameCurrency').val(output.data[0].nameCurrency);
                    $('#form_color_edit #isoCurrency').val(output.data[0].isoCurrency);
                    $('#form_color_edit #symbolCurrency').val(output.data[0].symbolCurrency);
                    $('#form_color_edit #exchangeRate').val(output.data[0].exchangeRate);
                    $('#form_color_edit #exchangeUnit').val(output.data[0].exchangeUnit);
                    $('#form_color_edit #smallestDenomination').val(output.data[0].smallestDenomination);
                    $('#form_color_edit #account').val(output.data[0].account);
                    $('#form_color_edit #iban').val(output.data[0].iban);
                    $('#form_color_edit #bic').val(output.data[0].bic);
                    $('#form_color_edit #accountCurrencyCode').val(output.data[0].accountCurrencyCode);
                    $('#colorEditModal').modal({show : true});
                    } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                    }
        });
            request.fail(function(jqXHR, textStatus){
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
                console.log(jqXHR.responseText);
            });
        });

        // Edit submit form
        $(document).on('submit', '#form_color_edit', function(e){
            e.preventDefault();
            var id        = $('#form_color_edit #idCurrency').val();
            var form_data = $('#form_color_edit').serialize();
            var request   = $.ajax({
                url:          '<?php echo URL_BASE ?>Setup/currency/update&id=' + id,
                cache:        false,
                data:         form_data,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            request.done(function(output){
                if (output.result == 'success'){
                    $('#colorEditModal').modal('hide');
                    reload_table();
                    notification(true);
                } else {
                    notification(false);
                }
            });
            request.fail(function(jqXHR, textStatus){
                notification(false);
                console.log(jqXHR.responseText);
            });
        });

        // Delete
        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var name = table.$('tr.selected').find('a').data('name');
            var id    = table.$('tr.selected').find('a').data('id');
            var title = "Confirmation Needed";
            var text  = "Are you sure you want to delete '" + name + "'?";
            var icon = "glyphicon glyphicon-minus";
            confirmDelete(title, text, icon, id, name);
        });

        // Delete action
        function confirmDelete(title, text, icon, id, name){
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function(){
                var request = $.ajax({
                    url:          '<?php echo URL_BASE ?>Setup/currency/delete&id=' + id,
                    cache:        false,
                    dataType:     'json',
                    contentType:  'application/json; charset=utf-8',
                    type:         'get'
                });
                request.done(function(output){
                    if (output.result == 'success'){
                        reload_table();
                        notification(true);
                    } else {
                        reload_table();
                        notification(false);
                    }
                    $('.ui-pnotify-modal-overlay').remove();
                });
                request.fail(function(jqXHR, textStatus){

                    notification(false);
                    console.log(jqXHR.responseText);
                });
            }).on('pnotify.cancel', function(){
                $('.ui-pnotify-modal-overlay').remove();
            });
        }

        $("#currency tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;



            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
           
        });


    } );


</script>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div id="messageAlert" role="alert"></div>
        </div>
        <div class="col-md-12">

            <div class="button-group">
                <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
            </div>
            <table id="currency" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('currency')"></th>
                    <th>ID</th>
                    <th class="translate">Coin</th>
                    <th>ISO</th>
                    <th class="translate">Symbol</th>
                    <th class="translate">Exchange rate</th>
                    <th class="translate">Exchange Unit</th>
                    <th class="translate">Minimum value</th>
                    <th class="translate">Treasury Account</th>
                    <th>IBAN</th>
                    <th>BIC</th>
                    <th class="translate">Currency Code</th>
                </tr>
                </thead>
            </table>
        </div>
        <div class="col-md-12">
            <div class="col-md-12 navbar-bottom">
                <div class="pull-right">
                    <button type="button" class="btn btn-primary translate">Close</button>
                    <button type="button" class="btn btn-success translate">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Currency</h4>
            </div>
            <form id="form_color" name="form_color" class="form_color">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="nameCurrency" class="label-style translate">Name</label>
                            <input type="text" class="form-control" placeholder="ID" id="idCurrency" name="idCurrency" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="nameCurrency" name="nameCurrency" value="" required="true" maxlength="45">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="isoCurrency" class="label-style">ISO</label>
                            <input type="text" class="form-control" placeholder="ISO" id="isoCurrency" name="isoCurrency" value="" required="true" maxlength="3">
                        </div>
                        <div class="col-md-3">
                            <label for="symbolCurrency" class="label-style translate">Symbol</label>
                            <input type="text" class="form-control" placeholder="Symbol" id="symbolCurrency" name="symbolCurrency" value="" required="true" maxlength="7">
                        </div>
                        <div class="col-md-5">
                            <label for="exchangeRate" class="label-style translate">Exchange Rate</label>
                            <input type="text" class="form-control" placeholder="Exchange Rate" id="exchangeRate" name="exchangeRate" value="" data-thousands="" data-decimal=".">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="exchangeUnit" class="label-style translate">Exchange Unit</label>
                            <input type="number" class="form-control" placeholder="Exchange Unit" id="exchangeUnit" name="exchangeUnit" value="">
                        </div>
                        <div class="col-md-4">
                            <label for="smallestDenomination" class="label-style translate">Smallest Denomination</label>
                            <input type="text" class="form-control" placeholder="Smallest Denomination" id="smallestDenomination" name="smallestDenomination" value="" data-thousands="" data-decimal=".">
                        </div>
                        <div class="col-md-4">
                            <label for="account" class="label-style translate">Account</label>
                            <input type="number" class="form-control" placeholder="Account" id="account" name="account" value="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="iban" class="label-style">IBAN</label>
                            <input type="text" class="form-control" placeholder="IBAN" id="iban" name="iban" value="" maxlength="34">
                        </div>
                        <div class="col-md-4">
                            <label for="bic" class="label-style">BIC</label>
                            <input type="text" class="form-control" placeholder="BIC" id="bic" name="bic" value="" maxlength="11">
                        </div>
                        <div class="col-md-4">
                            <label for="accountCurrencyCode" class="label-style translate">Account Currency Code</label>
                            <input type="text" class="form-control" placeholder="Account Currency Code" id="accountCurrencyCode" name="accountCurrencyCode" value="" maxlength="10">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="colorEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update</h4>
            </div>
            <form id="form_color_edit" name="form_color" class="form_color">
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="nameCurrency" class="label-style translate">Name</label>
                            <input type="text" class="form-control" placeholder="ID" id="idCurrency" name="idCurrency" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="nameCurrency" name="nameCurrency" value="" required="true" maxlength="45">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="isoCurrency" class="label-style">ISO</label>
                            <input type="text" class="form-control" placeholder="ISO" id="isoCurrency" name="isoCurrency" value="" required="true" maxlength="3">
                        </div>
                        <div class="col-md-3">
                            <label for="symbolCurrency" class="label-style translate">Symbol</label>
                            <input type="text" class="form-control" placeholder="Symbol" id="symbolCurrency" name="symbolCurrency" value="" required="true" maxlength="7">
                        </div>
                        <div class="col-md-5">
                            <label for="exchangeRate" class="label-style translate">Exchange Rate</label>
                            <input type="text" class="form-control" placeholder="Exchange Rate" id="exchangeRate" name="exchangeRate" value="" data-thousands="" data-decimal=".">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="exchangeUnit" class="label-style translate">Exchange Unit</label>
                            <input type="number" class="form-control" placeholder="Exchange Unit" id="exchangeUnit" name="exchangeUnit" value="">
                        </div>
                        <div class="col-md-4">
                            <label for="smallestDenomination" class="label-style translate">Smallest Denomination</label>
                            <input type="text" class="form-control" placeholder="Smallest Denomination" id="smallestDenomination" name="smallestDenomination" value="" data-thousands="" data-decimal=".">
                        </div>
                        <div class="col-md-4">
                            <label for="account" class="label-style translate">Account</label>
                            <input type="number" class="form-control" placeholder="Account" id="account" name="account" value="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <label for="iban" class="label-style">IBAN</label>
                            <input type="text" class="form-control" placeholder="IBAN" id="iban" name="iban" value="" maxlength="34">
                        </div>
                        <div class="col-md-4">
                            <label for="bic" class="label-style">BIC</label>
                            <input type="text" class="form-control" placeholder="BIC" id="bic" name="bic" value="" maxlength="11">
                        </div>
                        <div class="col-md-4">
                            <label for="accountCurrencyCode" class="label-style translate">Account Currency Code</label>
                            <input type="text" class="form-control" placeholder="Account Currency Code" id="accountCurrencyCode" name="accountCurrencyCode" value="" maxlength="10">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
