<?php

/**
 * Description of PosCompanyController
 * 
 * @abstract file created in 23/01/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 * 
 */

class PosCompanyController {
    
    private $entityManager;
    
    function __construct() {
        $this->entityManager = $this->getEntityManager();
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    function getEntityManager() {
        return getEm();
    }
    
    /**
     * 
     * @param string $result
     * @param string $mesage
     * @param mixed $dataArray
     */
    private function sendResult($result, $mesage, $dataArray){
        $data = array(
            "result"  => $result,
            "mesage" => $mesage,
            "data"    => $dataArray
        );

        $json_data = json_encode($data);
        print $json_data;
    }


    /**
     * 
     * @param $idPosCompany
     * @return PosCompany
     */
    private function getPosCompany($idPosCompany){
        $posCompanyRepository = $this->entityManager->getRepository("PosCompany");
        return $posCompanyRepository->find($idPosCompany);  
    }
    
    /**
     * 
     * @param PosCompany $posCompany
     * @return array()
     */
    private function getPosCompanyData($posCompany) {
        $criteria = array(
            "active"    =>  true,
            "posCompanyposCompany"  =>  $posCompany
        );
        $pcdArray = $this->entityManager->getRepository("PosCompanyData")->findBy($criteria);

        if(count($pcdArray)>0){
            /*@var $poscompanydata PosCompanyData*/
            foreach ($pcdArray as $poscompanydata) {
                $return[$poscompanydata->getDataType()] = $poscompanydata->getDataValue();
            }
        } else {
            $return = array();
        }
        
        return $return;
    }
    
    /**
     * 
     * @param PosCompany $posCompany
     * @return CompanyImage
     * 
     */
    private function getCI($posCompany){
        require_once __DIR__.'/CompanyImageController.php';
        
        $pos = $this->getPos($posCompany);
        $company = $this->getCompany($pos);
        
        $ci = $this->getCompanyImage($company);
        
        return $ci;
    }
    
    /**
     * 
     * @param array $img
     * @return array
     */
    private function validityImage($img){
        require_once __DIR__.'/CompanyImageController.php';
        
        $companyImage = new CompanyImageController();
        
        return $companyImage->validityImage($img);
    }
    
    /**
     * 
     * @param PosCompany $posCompany
     * @return Pos
     */
    private function getPos($posCompany) {
        $criteria = array(            
            "active"    =>  TRUE,
            "posCompanyposCompany"  => $posCompany,
        );

        $p = $this->entityManager->getRepository("Pos")->findBy($criteria);
        
        return array_pop($p);        
    }
    
    /**
     * 
     * @param Pos $pos
     * @return Company
     */
    private function getCompany($pos) {
        $criteria = array(            
            "active"    =>  TRUE,
            "pospos"  => $pos,
        );

        $c = $this->entityManager->getRepository("Company")->findBy($criteria);
        
        return array_pop($c);   
    }
    
    /**
     * 
     * @param Company $company
     * @return CompanyImage
     */
    private function getCompanyImage($company) {
        $filter = array(            
            "active"    =>  TRUE,
            "companycompany"  => $company,
        );

        $c = $this->entityManager->getRepository("CompanyImage")->findBy($filter);
        
        return array_pop($c);   
    }
    

    /**
     * Method pricipal
     * 
     */
    public function index($result="") {
        $navbar = "Setup|configuration";
        $posCompanyId = "3";
        $idPosCompany = "";
        $posCompany = $this->getPosCompany($posCompanyId);
        $pcdArray = $this->getPosCompanyData($posCompany);
//        $companyImage = $this->getCI($posCompany);  
        
        if($posCompany){
            $idPosCompany = $posCompany->getIdposCompany();
        }

        $posCompany = getEm()->getRepository('PosCompany')->findOneBy(array('active' => true));
        if($posCompany != null){
            $country = $posCompany->getZzStatezzState()->getZzCountryzzCountry()->getCode();  
        }else{
            $country = "";
        }



        $array_answer = array (    
            "menu-active"   =>  "company",
            "pcId"          =>  $idPosCompany,
            "posCompany"    =>  $posCompany,
            "companyImage"  =>  "",//$companyImage,
            "pcdArray" => $pcdArray,    
            "response"  =>  $result,
            "country" => $country
        );
        
        GenericController::template("Setup", "companyinfo","index", $navbar, $array_answer, 2);
    }
        
    /**
     * Update pos company
     * @param integer $idPosCompany
     */
    public function update($idPosCompany) {                
        
        try{
            
            $myinputs = filter_input_array(INPUT_POST);        
            $pcdArray = array();
            $companyImage = $this->validityImage($_FILES["pcl_image"]);
            $posCompany = $this->getPosCompany($idPosCompany);
            
            foreach ($myinputs as $key => $value) {
                
                if(strpos($key, "pc_") === 0){  
                    
                    $nameArray = explode("_", $key);

                    $firstW = ucfirst($nameArray[1]);
                    $secondW = "";
                    
                    if(array_key_exists(2, $nameArray)){
                        $secondW = ucfirst($nameArray[2]);
                    }
                    
                    $method = "set".$firstW.$secondW;
                    $posCompany->$method($value); 
                    
                }else if(strpos($key, "pcd_") === 0){                         
                    $key = trim(substr($key, strlen("pcd_")));
                    $pcdArray[$key] = $value;                    
                }                              
            }
            
            $posCompany->setActive(true);             
            $this->entityManager->flush();
            
            $this->updateOtherTables($posCompany, $companyImage, $pcdArray, $reference_name="");            
            
            $result = "success";
            
        } catch (Exception $ex) {
            
            $result = "error";
            var_dump($ex);
            die();
            
        }        
        
        $redirect = "Location: ".URL_BASE."Setup/posCompany/index/$result";
        
        header($redirect);
        
    }
        
    private function updateOtherTables($posCompany, $companyImage, $pcdArray, $reference_name) {
        
        require_once __DIR__.'/PosCompanyDataController.php';
        require_once __DIR__.'/CompanyImageController.php';
        
        
        //Insert/update  posCompanyData
        $pcdController = new \PosCompanyDataController();                    
        $pcdController->updatePosCompanyData($posCompany, $pcdArray, $reference_name);
        //#####

        if($companyImage !== false){
            //Insert/update CompanyImage
            $ci = new \CompanyImageController();
            $ci->updatePosCompanyLogo($posCompany, $companyImage, $sequence = 1);
        }
    }
    
    
     /**
     * Method pricipal
     * 
     */
    public function index2($result = "") {
        $navbar = "Setup|configuration";
        $posCompanyId = "3";
        $idPosCompany = "";
        $posCompany = $this->getPosCompany($posCompanyId);
        $pcdArray = $this->getPosCompanyData($posCompany);
//        $companyImage = $this->getCI($posCompany);  



        if ($posCompany) {
            $idPosCompany = $posCompany->getIdposCompany();
        }

        $posCompany = getEm()->getRepository('PosCompany')->findOneBy(array('active' => true));
        
        if ($posCompany != null) {
            $country = $posCompany->getZzStatezzState()->getZzCountryzzCountry()->getCode();
        } else {
            $country = "";
        }

        if($_POST){                 
            //if($companyInfo = getEm()->getRepository('CompanyInfo')->findBy(array('idCompanyInfo' => $_POST['id_company_info'], 'active' => true))) {
            if($companyInfo = getEm()->find('CompanyInfo', $_POST['id_company_info'])){                
                    $companyInfo->setDataType($_POST['data_type']);
                    $companyInfo->setDataValue($_POST['data_value']);
                    $companyInfo->setPosCompanyposCompany($posCompany);
                   
                    $teste = getEm()->persist($companyInfo);
                  
                    getEm()->flush();                                  
            }     
            else {
                $companyInfo = new CompanyInfo();
                $companyInfo->setDataType($_POST['data_type']);
                $companyInfo->setDataValue($_POST['data_value']);
                $companyInfo->setPosCompanyposCompany($posCompany);
                $companyInfo->setDateCreate(new DateTime());
                $companyInfo->setActive("1");
                getEm()->persist($companyInfo);
                getEm()->flush();
            }           
        }
        

        $infos = getEm()->getRepository('CompanyInfo')->findBy(array('posCompanyposCompany' => 1, 'active' => true));        
//echo "<pre>"; var_dump($infos); echo "</pre>"; echo "AAAA<br>AAAA<br>AAAA<br>AAAA<br>AAAA<br>";
        $array_answer = array(
            "menu-active" => "company",
            "pcId" => $idPosCompany,
            "posCompany" => $posCompany,
            "companyImage" => "", //$companyImage,
            "pcdArray" => $pcdArray,
            "response" => $result,
            "country" => $country,
            "infos" => $infos,
        );
// echo "<pre>";
// var_dump($array_answer);
// echo "</pre>";
// die();
        GenericController::template("Setup", "companyinfo", "new", $navbar, $array_answer, 2);
    }

}