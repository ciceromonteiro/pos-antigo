<?php

/**
 * Description of companyController
 *
 * @author rocha
 * 
 */
class CompanyController {

    public function index() {
        $navbar = "Setup|configuration";
        $array_answer = array();
        GenericController::template("Setup", "company", "index", $navbar, $array_answer, 2);
    }

    public function getCompanyInfo() {
        $arrayValues = array();
        $dat = array();
        try {
            $posCompany = getEm()->getRepository('PosCompany')->findOneBy(array('active' => true));
            //$numeropdv =  getEm()->getRepository('NumberPdv')->findOnBy(array('pos_company_idpos_company'=>$posCompany));
           // $dat['pdvnumber'] = $numeropdv->getNumber();
            //$dat['address_city'] = $posCompany->getAddressCity();
            //$dat['address_complement'] = $posCompany->getAddressComplement();
            //$dat['address_district'] = $posCompany->getAddressDistrict();
            //$dat['address_number'] =  $posCompany->getAddressNumber();
            // $dat['address_reference'] = $posCompany->getAddressReference();
            // $dat['address_street'] = $posCompany->getAddressStreet();
            //  $dat['address_zipCode'] = $posCompany->getAddressZipcode();
            /* if($posCompany->getBankAccount() != null){
              $dat['bank_account'] = $posCompany->getBankAccount();
              }
              if($posCompany->getIban() != null){
              $dat['bank_iban'] = $posCompany->getIban();
              }
              if($posCompany->getBic() != null){
              $dat['bank_bic'] = $posCompany->getBic();
              }
             */
            //$dat['bank_bic'] = $posCompany->getBic();
            // $dat['address_state'] = $posCompany->getZzStatezzState()->getIdzzState();
            //$dat['bank_email'] = $posCompany->getBankEmail();
            //$dat['bank_iban'] = $posCompany->getIban();
            // $dat['default_message_invoice'] = $posCompany->getDefaultMessageInvoice();
            $dat['fantasy_name'] = $posCompany->getFantasyName();
            // $dat['license_key'] = $posCompany->getLicense();
            $dat['name'] = $posCompany->getName();
            $dat['register_number2'] = $posCompany->getRegisterNumber();
            // $dat['vaddress_city'] = $posCompany->getVaddressCity();
            // $dat['vaddress_complement'] = $posCompany->getVaddressComplement();
            //  $dat['vaddress_district'] = $posCompany->getVaddressDistrict();
            //  $dat['vaddress_number'] = $posCompany->getVaddressNumber();
            // $dat['vaddress_reference'] = $posCompany->getVaddressReference();
            //   $dat['vaddress_street'] = $posCompany->getVaddressStreet();
            //   $dat['vaddress_zipCode'] = $posCompany->getVaddressZipcode();
            /*  if($posCompany->getBankBr() != null){
              $dat['bank'] = $posCompany->getBankBr();

              if($posCompany->getBankBr() == 1){
              $dat['bankItau'] = $posCompany->getSpecieBr();
              }else if($posCompany->getBankBr() == 2){
              $dat['bankBB'] = $posCompany->getSpecieBr();
              }else if($posCompany->getBankBr() == 3){
              $dat['bankCef'] = $posCompany->getSpecieBr();
              }

              }
              if($posCompany->getAgencyBr() != null){
              $dat['agency'] = $posCompany->getAgencyBr();
              }
              if($posCompany->getAccountBr() != null){
              $dat['account'] = $posCompany->getAccountBr();
              }
              if($posCompany->getDigitAccountBr() != null){
              $dat['digit_account'] = $posCompany->getDigitAccountBr();
              }

              if($posCompany->getBankWalletBr() != null){
              $dat['bank_wallet_br'] = $posCompany->getBankWalletBr();
              }

              if($posCompany->getInterestDayMaturityInvoiceBr() != null){
              $dat['interestDay'] = $posCompany->getInterestDayMaturityInvoiceBr();
              }

              if($posCompany->getDaysAfterMaturityInvoiceBr() != null){
              $dat['deadlineDay'] = $posCompany->getDaysAfterMaturityInvoiceBr();
              }

              if($posCompany->getValueOfMulctBr() != null){
              $valueMulct = str_replace(',', '', $posCompany->getValueOfMulctBr());
              $valueMulct = str_replace('.', ',', $valueMulct);
              $dat['valueMulct'] = $valueMulct;
              }

              if($posCompany->getCodeAgreementBr() != null){
              $dat['code_agreement_br'] = $posCompany->getCodeAgreementBr();
              }
              if($posCompany->getWalletVariationBr() != null){
              $dat['wallet_variation_br'] = $posCompany->getWalletVariationBr();
              }
              if($posCompany->getAgencyDvBr() != null){
              $dat['agency_dv_br'] = $posCompany->getAgencyDvBr();
              }
              if($posCompany->getOperationBr() != null){
              $dat['operation_br'] = $posCompany->getOperationBr();
              }
              if($posCompany->getSequenceNumberFileBr() != null){
              $dat['sequence_number_file_br'] = $posCompany->getSequenceNumberFileBr();
              }

              if($posCompany->getAgencyDvBr() != null){
              $dat['agency_dv_br_cef'] = $posCompany->getAgencyDvBr();
              }
              if($posCompany->getOperationBr() != null){
              $dat['operation_br_cef'] = $posCompany->getOperationBr();
              }
              if($posCompany->getSequenceNumberFileBr() != null){
              $dat['sequence_number_file_br_cef'] = $posCompany->getSequenceNumberFileBr();
              }


              if($posCompany->getAceiteBr() != null){
              $dat['aceite_br'] = $posCompany->getAceiteBr();
              }
              if($posCompany->getAssignorCodeBr() != null){
              $dat['assignor_code_br'] = $posCompany->getAssignorCodeBr();
              }
              if($posCompany->getRegisteredInvoiceBr() != null){
              $dat['registered_invoice_br'] = $posCompany->getRegisteredInvoiceBr();
              }
              if($posCompany->getWalletCodeBr() != null){
              $dat['wallet_code_br'] = $posCompany->getWalletCodeBr();
              }
             */
            $phone = getEm()->getRepository('CompanyInfo')->findOneBy(array('posCompanyposCompany' => $posCompany, 'dataType' => 'phone'));
            if (@$phone->getIdCompanyInfo() != null) {
                $dat['phone_company'] = "{$phone->getDataValue()}";
            }

            $fax = getEm()->getRepository('CompanyInfo')->findOneBy(array('posCompanyposCompany' => $posCompany, 'dataType' => 'fax'));
            if (@$fax->getIdCompanyInfo() != null) {
                $dat['fax_company2'] = "{$fax->getDataValue()}";
            }

            /* $state = $posCompany->getZzStatezzState();
              if ($state){
              $dat['address_state'] = $state->getName();
              } */

            array_push($arrayValues, $dat);

            /* $companyInfo = getEm()->getRepository('CompanyInfo')->findAll();
              if($companyInfo){
              foreach ($companyInfo as $value) {
              $dataType = $value->getDataType();
              $dataValue = $value->getDataValue();
              $dat[$dataType] = $dataValue;
              }

              } */

            $companyInfoImg = getEm()->getRepository('CompanyInfoImg')->findBy(array('active' => 1));
            if ($companyInfoImg) {
                $arrayValues[0]['image'] = $companyInfoImg[0]->getImage();
            }

            $result = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $values = '';
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $values
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateCompanyInfo() {
        $json = json_decode($_REQUEST['data'], true);
        //var_dump($json);

        /* $arrayValues = array (
          'name'                      => isset($json['name']) ? $json['name'] : '',
          'fantasy_name'              => isset($json['fantasy_name']) ? $json['fantasy_name'] : '',
          'register_number'           => isset($json['register_number']) ? $json['register_number'] : '',
          'phone_company'             => isset($json['phone_company']) ? $json['phone_company'] : '',
          'fax_company'               => isset($json['fax_company']) ? $json['fax_company'] : '',
          'default_message_invoice'   => isset($json['default_message_invoice']) ? $json['default_message_invoice'] : '',
          'address_street'            => isset($json['address_street']) ? $json['address_street'] : '',
          'address_number'            => isset($json['address_number']) ? $json['address_number'] : '',
          'address_district'          => isset($json['address_district']) ? $json['address_district'] : '',
          'address_city'              => isset($json['address_city']) ? $json['address_city'] : '',
          'address_state'             => isset($json['address_state']) ? $json['address_state'] : '',
          'address_zipCode'           => isset($json['address_zipCode']) ? $json['address_zipCode'] : '',
          'address_complement'        => isset($json['address_complement']) ? $json['address_complement'] : '',
          'address_reference'         => isset($json['address_reference']) ? $json['address_reference'] : '',
          'vaddress_street'           => isset($json['vaddress_street']) ? $json['vaddress_street'] : '',
          'vaddress_number'           => isset($json['vaddress_number']) ? $json['vaddress_number'] : '',
          'vaddress_district'         => isset($json['vaddress_district']) ? $json['vaddress_district'] : '',
          'vaddress_city'             => isset($json['vaddress_city']) ? $json['vaddress_city'] : '',
          'vaddress_zipCode'          => isset($json['vaddress_zipCode']) ? $json['vaddress_zipCode'] : '',
          'vaddress_complement'       => isset($json['vaddress_complement']) ? $json['vaddress_complement'] : '',
          'vaddress_reference'        => isset($json['vaddress_reference']) ? $json['vaddress_reference'] : '',
          'bank_account'              => isset($json['bank_account']) ? $json['bank_account'] : '',
          'bank_iban'                 => isset($json['bank_iban']) ? $json['bank_iban'] : '',
          'bank_bic'                  => isset($json['bank_bic']) ? $json['bank_bic'] : '',
          'bank_email'                => isset($json['bank_email']) ? $json['bank_email'] : '',
          'license_key'               => isset($json['license_key']) ? $json['license_key'] : '',
          'image'                     => isset($json['image']) ? $json['image'] : '',
          'bank'                      => isset($json['bank']) ? $json['bank'] : '',
          'agency'                    => isset($json['agency']) ? $json['agency'] : '',
          'account'                   => isset($json['account']) ? $json['account'] : '',
          'digit_account'             => isset($json['digit_account']) ? $json['digit_account'] : '',
          'bank_wallet_br'            => isset($json['bank_wallet_br']) ? $json['bank_wallet_br'] : '',
          'bankSpecie'                => isset($json['bankSpecie']) ? $json['bankSpecie'] : '',
          'interestDay'               => isset($json['interestDay']) ? $json['interestDay'] : '',
          'deadlineDay'               => isset($json['deadlineDay']) ? $json['deadlineDay'] : '',
          'valueMulct'                => isset($json['valueMulct']) ? $json['valueMulct'] : '',
          'code_agreement_br'         => isset($json['code_agreement_br']) ? $json['code_agreement_br'] : '',
          'wallet_variation_br'       => isset($json['wallet_variation_br']) ? $json['wallet_variation_br'] : '',
          'agency_dv_br'              => isset($json['agency_dv_br']) ? $json['agency_dv_br'] : '',
          'operation_br'              => isset($json['operation_br']) ? $json['operation_br'] : '',
          'sequence_number_file_br'   => isset($json['sequence_number_file_br']) ? $json['sequence_number_file_br'] : '',
          'wallet_code_br'            => isset($json['wallet_code_br']) ? $json['wallet_code_br'] : '',
          'registered_invoice_br'     => isset($json['registered_invoice_br']) ? $json['registered_invoice_br'] : '',
          'aceite_br'                 => isset($json['aceite_br']) ? $json['aceite_br'] : '',
          'assignor_code_br'          => isset($json['assignor_code_br']) ? $json['assignor_code_br'] : '',

          'agency_dv_br_cef'          => isset($json['agency_dv_br_cef']) ? $json['agency_dv_br_cef'] : '',
          'operation_br_cef'          => isset($json['operation_br_cef']) ? $json['operation_br_cef'] : '',
          'sequence_number_file_br_cef' => isset($json['sequence_number_file_br_cef']) ? $json['sequence_number_file_br_cef'] : ''



          ); */

        $arrayValues = array(
            'name' => isset($json['name']) ? $json['name'] : '',
            'fantasy_name' => isset($json['fantasy_name']) ? $json['fantasy_name'] : '',
            'image' => isset($json['image']) ? $json['image'] : '',
            'register_number' => isset($json['register_number2']) ? $json['register_number2'] : '',
            'phone_company' => isset($json['phone_company']) ? $json['phone_company'] : '',
            'fax_company' => isset($json['fax_company2']) ? $json['fax_company2'] : ''
        );




        try {
            // $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            $posCompany = getEm()->getRepository('PosCompany')->findOneBy(array('active' => true));


            if ($posCompany) {
                $posCompany->setName($arrayValues["name"]);
                $posCompany->setFantasyName($arrayValues["fantasy_name"]);
                $posCompany->setRegisterNumber($arrayValues["register_number"]);


            /*    if($arrayValues["pdvnumber"]){
                    $NumberPdv = new NumberPdv();
                    $mac = "00-00-00-00";
                    $NumberPdv->setMac($mac);
                    $NumberPdv->setNumber($arrayValues["pdvnumber"]);
                    $NumberPdv->setPoscompany($posCompany);
                    getEm()->persist($NumberPdv);
                     getEm()->flush();
                }*/


                $phone = getEm()->getRepository('CompanyInfo')->findOneBy(array('posCompanyposCompany' => $posCompany, 'dataType' => 'phone'));

                if (count($phone) > 0) {
                    $phone->setDataValue($arrayValues["phone_company"]);
                } else {
                    $phone = new CompanyInfo();
                    $phone->setDataType("phone");
                    $phone->setDataValue($arrayValues["phone_company"]);
                    $phone->setPosCompanyposCompany($posCompany);
                    getEm()->persist($phone);
                }

                $fax = getEm()->getRepository('CompanyInfo')->findOneBy(array('posCompanyposCompany' => $posCompany, 'dataType' => 'fax'));

                if (count($fax) > 0) {
                    $fax->setDataValue($arrayValues["fax_company"]);
                } else {
                    $fax = new CompanyInfo();
                    $fax->setDataType("fax");
                    $fax->setDataValue($arrayValues["fax_company"]);
                    $fax->setPosCompanyposCompany($posCompany);
                    getEm()->persist($fax);
                }

                getEm()->flush();
            }
            for ($i = 0; $i < count($arrayKeys); $i++) {
                $key = $arrayKeys[$i];
                $companyInfo = getEm()->getRepository('CompanyInfo')->findOneBy(array('dataType' => $key));
                if ($companyInfo) {
                    /* if($key != 'image'){
                      $companyInfo->setDataValue($arrayValues[$key]);
                      $companyInfo->setDateUpdate(new DateTime());
                      $companyInfo->setActive(1);
                      getEm()->persist($companyInfo);
                      getEm()->flush();
                      } else */
                    if ($key == 'image') {
                        $companyInfoImg = getEm()->getRepository('CompanyInfoImg')->findOneBy(array('active' => 1));
                        $companyInfoImg->setImage($arrayValues[$key]);
                        getEm()->persist($companyInfoImg);
                        getEm()->flush();
                    }
                } else {
                    if ($key != 'image') {
                        $companyInfo = new CompanyInfo();
                        $companyInfo->setDataType($key);
                        $companyInfo->setDataValue($arrayValues[$key]);
                        $companyInfo->setDateCreate(new DateTime());
                        $companyInfo->setActive(1);
                        getEm()->persist($companyInfo);
                        getEm()->flush();
                    } else if ($key == 'image') {
                        $companyInfoImg = getEm()->getRepository('CompanyInfoImg')->findOneBy(array('active' => 1));
                        if ($companyInfoImg) {
                            $companyInfoImg->setImage($arrayValues[$key]);
                            getEm()->persist($companyInfoImg);
                            getEm()->flush();
                        } else {
                            $companyInfoImg = new CompanyInfoImg();
                            $companyInfoImg->setImage($arrayValues[$key]);
                            $companyInfoImg->setActive(1);
                            getEm()->persist($companyInfoImg);
                            getEm()->flush();
                        }
                    }
                }
            }
            $commit = getEm()->getConnection()->commit();
            $result = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            echo 'catch';
            $rollback = getEm()->getConnection()->rollback();
            $result = 'error';
            $message = $e->getMessage();
        }

        $data = array(
            "result" => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getLicense() {
        try {
            $license = getEm()->getRepository('License')->findAll();
            $array_data = array();
            foreach ($license as $value) {
                $validad = $value->getValidad();
                $dat = array(
                    "idlicense" => $value->getIdlicense(),
                    "key_license" => $value->getKeyLicense(),
                    "validad" => isset($validad) ? $validad->format('d/m/Y') : '',
                    "number_of_companies" => $value->getNumberOfCompanies(),
                    "limit_users" => $value->getLimitUsers(),
                    "limit_connections" => $value->getLimitConnections(),
                    "limit_terminals" => $value->getLimitTerminals(),
                    "limit_mobiles" => $value->getLimitMobiles()
                );
                array_push($array_data, $dat);
                break;
            }
            $result = 'success';
            $message = 'query success';
            $data = $array_data;
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getFeaturesInLicense() {
        try {
            $features = getEm()->getRepository('LicenseFeatures')->findAll();

            $data = array();
            foreach ($features as $value) {
                $features_in_license = getEm()->getRepository('FeatureInLicense')->findBy(array("feature" => $value->getIdfeatures()));
                if ($features_in_license) {
                    $status = "<span class='glyphicon glyphicon-ok'></span>";
                } else {
                    $status = "<span class='glyphicon glyphicon-remove'></span>";
                }
                $dat = array(
                    "description" => $value->getDescription(),
                    "status" => $status
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function formateState($state) {
        $low_state = mb_strtolower($state);
        $low_decode_state = base64_decode(base64_encode($low_state));
        switch ($low_state) {
            case "ac":
                return "Acre";
            case "acre":
                return "Acre";

            case "al":
                return "Alagoas";
            case "alagoas":
                return "Alagoas";

            case "ap":
                return "Amapá";
            case "amapá":
                return "Amapá";

            case "am":
                return "Amazonas";
            case "amazonas":
                return "Amazonas";

            case "ba":
                return "Bahia";
            case "bahia":
                return "Bahia";

            case "ce":
                return "Ceará";
            case "ceara":
                return "Ceará";

            case "df":
                return "Distrito Federal";
            case "distrito federal":
                return "Distrito Federal";

            case "es":
                return "Espírito Santo";
            case "espirito santo":
                return "Espírito Santo";

            case "go":
                return "Goiás";
            case "goias":
                return "Goiás";

            case "ma":
                return "Maranhão";
            case "maranhao":
                return "Maranhão";

            case "mt":
                return "Mato Grosso";
            case "mato grosso":
                return "Mato Grosso";

            case "ms":
                return "Mato Grosso do Sul";
            case "mato grosso do sul":
                return "Mato Grosso do Sul";

            case "mg":
                return "Minas Gerais";
            case "minas gerais":
                return "Minas Gerais";

            case "pa":
                return "Pará";
            case "para":
                return "Pará";

            case "pb":
                return "Paraíba";
            case "paraiba":
                return "Paraíba";

            case "pr":
                return "Paraná";
            case "parana":
                return "Paraná";

            case "pb":
                return "Pernambuco";
            case "pernambuco":
                return "Pernambuco";

            case "pi":
                return "Piauí";
            case "piaui":
                return "Piauí";

            case "rj":
                return "Rio de Janeiro";
            case "rio de janeiro":
                return "Rio de Janeiro";

            case "rn":
                return "Rio Grande do Norte";
            case "rio grande do norte":
                return "Rio Grande do Norte";

            case "rs":
                return "Rio Grande do Sul";
            case "rio grande do sul":
                return "Rio Grande do Sul";

            case "ro":
                return "Rondônia";
            case "rondonia":
                return "Rondônia";

            case "rr":
                return "Roraima";
            case "roraima":
                return "Roraima";

            case "sc":
                return "Santa Catarina";
            case "santa catarina":
                return "Santa Catarina";

            case "sp":
                return "São Paulo";
            case "sao paulo":
                return "São Paulo";

            case "se":
                return "Sergipe";
            case "sergipe":
                return "Sergipe";

            case "to":
                return "Tocantins";
            case "tocantins":
                return "Tocantins";
        }

        return null;
    }

}
