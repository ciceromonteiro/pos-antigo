<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AddDeviceController {
    
    public function index(){
        $array_answer = array ("");
        GenericController::templateNewDevice("Setup", "addDevice","index", $array_answer);
    }
    
    public function checkInstall(){
        $auth = new AuthenticationController();
        $posMAC = $auth->getMac();
        $namePos = $auth->getNameMachine();
        $install = getEm()->getRepository('Install')->findAll();        
        if($install){
            foreach ($install as $value) {
                if($value->getMacaddress() == $posMAC && $value->getStatus() == 1){
                    return true;
                }
            }
        }
        return false;
    }
    
    public function install(){
        $message = "";
        $language_input = $_POST['language'];
        
        $license = getEm()->getRepository('license')->findBy(array('keyLicense' => $_POST['license']));
        $install = getEm()->getRepository('Install')->findBy(array('status' => 1));
        if($language_input == 1){
            $language = array(
                0 => 'Welcome, You will be redirected. ',
                1 => 'Your key has expired. ',
                2 => 'You have expired the installation limit for your license. ',
                3 => 'Your key is incorrect. '
            );
        } else if($language_input == 2){
            $language = array(
                0 => 'Velkommen, Du vil bli omdirigert. ',
                1 => 'Nøkkelen er utløpt. ',
                2 => 'Du har utløpt installasjonen grensen for din lisens. ',
                3 => 'Nøkkelen er feil. '
            );
        } else if($language_input == 3){
            $language = array(
                0 => 'Bem-vindo, Você será redirecionado. ',
                1 => 'Sua chave expirou. ',
                2 => 'Você expirou o limite de instalação para sua licença. ',
                3 => 'Sua chave esta incorreta. '
            );
        }
        
        if($license){
            foreach ($license as $value) {
                $date_now = new DateTime();
                $validad = $value->getValidad();
                if($validad < $date_now && count($install) < $value->getLimitTerminals()){
                    try {
                        $auth = new AuthenticationController();
                        $install = new Install();
                        $install->setSrv(rand(200, 1500));
                        $install->setMacaddress($auth->getMac());
                        $install->setNickname($auth->getNameMachine());
                        $install->setDebug($auth->getAddress());
                        $install->setDateCreate(new DateTime());
                        $install->setStatus(1);
                        getEm()->persist($install);
                        getEm()->flush();
                        
                        $result  = 'success';
                        $message = $language[0];
                        $data = "";
                    } catch (Exception $e) {
                        $result  = 'error';
                        $message = $e->getMessage();
                        $data = "";
                    }
                } else if ($validad > $date_now){
                    $result  = 'error-license';
                    $message .= $language[2];
                    $data = "";
                } else if (count($install) > $value->getLimitTerminals()){
                    $result  = 'error-license';
                    $message .= $language[3];
                    $data = "";
                }
            }
        } else {
            $result  = 'error-license';
            $message .= $language[3];
            $data = "";
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
}