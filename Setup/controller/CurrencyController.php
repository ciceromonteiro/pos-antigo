<?php

class CurrencyController {
    public function __construct(){

    }

    public static function index(){
        $navbar = "configuration|currency";
        $array_answer = array ("");
        GenericController::template("Setup", "currency","index", $navbar, $array_answer, 43);
    }

    public function getAll(){
        try{
            $em = getEm();
            $custom = $em->getRepository('Currency');
            $arrayFilter = array('active'=>1);
            $currencies = $custom->findBy($arrayFilter);
            $data = array ();
            foreach ($currencies as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdcurrency().'">',
                    "id" => '<a data-id="'.$value->getIdcurrency().'" data-name="'.$value->getName().'">'.$value->getIdcurrency().'</a>',
                    "name" => $value->getName(),
                    "iso" => $value->getIso(),
                    "symbol" => $value->getSymbol(),
                    "exchange_rate" => $value->getExchangeRate(),
                    "exchange_unit" => $value->getExchangeUnit(),
                    "smallest_denomination" => $value->getSmallestDenomination(),
                    "account" => $value->getAccount(),
                    "iban" => $value->getIban(),
                    "bic" => $value->getBic(),
                    "account_currency_code" => $value->getAccountCurrencyCode()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insert(){
        
        if(isset($_GET['nameCurrency'])){
            try {
                $currency = new Currency();
                $currency->setName($_GET['nameCurrency']);
                $currency->setIso($_GET['isoCurrency']);
                $currency->setExchangeRate($_GET['exchangeRate']);
                $currency->setExchangeUnit($_GET['exchangeUnit']);
                $currency->setSmallestDenomination($_GET['smallestDenomination']);
                $currency->setAccount($_GET['account']);
                $currency->setIban($_GET['iban']);
                $currency->setBic($_GET['bic']);
                $currency->setAccountCurrencyCode($_GET['accountCurrencyCode']);
                $currency->setSymbol($_GET['symbolCurrency']);
                $currency->setActive(1);
                $currency->setDateCreate(new \DateTime());
                getEm()->persist($currency);
                getEm()->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function update(){
        if(isset($_GET['nameCurrency']) && isset($_GET['isoCurrency']) && isset($_GET['symbolCurrency']) && isset($_GET['idCurrency'])){
            try {
                $entityManager = getEm();
                $arrayFilter = array(
                    "idcurrency" => $_GET['idCurrency']
                );

                $currencies = $entityManager->getRepository('Currency')->findBy($arrayFilter);
                $mysqlData = array ();
                foreach ($currencies as $value){
                    $dat = array (
                        "name" => $value->setName($_GET['nameCurrency']),
                        "iso" => $value->setIso($_GET['isoCurrency']),
                        "symbol" => $value->setSymbol($_GET['symbolCurrency']),
                        "exchange_rate" => $value->setExchangeRate($_GET['exchangeRate']),
                        "exchange_unit" => $value->setExchangeUnit($_GET['exchangeUnit']),
                        "smallest_denomination" => $value->setSmallestDenomination($_GET['smallestDenomination']),
                        "account" => $value->setAccount($_GET['account']),
                        "iban" => $value->setIban($_GET['iban']),
                        "bic" => $value->setBic($_GET['bic']),
                        "account_currency_code" => $value->setAccountCurrencyCode($_GET['accountCurrencyCode']),
                        "date_update" => $value->setDateUpdate(new \DateTime())
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                
                $entityManager->persist($value);
                $entityManager->flush();
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getCurrency(){
        if(isset($_GET['id'])){
            try {
                $entityManager = getEm();
                $arrayFilter = array("idcurrency" => $_GET['id']);
                $currencies = $entityManager->getRepository('Currency')->findBy($arrayFilter);
                $mysqlData = array ();
                foreach ($currencies as $value){
                    $dat = array (
                        "nameCurrency" => $value->getName(),
                        "idCurrency" => $value->getIdcurrency(),
                        "isoCurrency" => $value->getIso(),
                        "symbolCurrency" => $value->getSymbol(),
                        "exchangeRate" => $value->getExchangeRate(),
                        "exchangeUnit" => $value->getExchangeUnit(),
                        "smallestDenomination" => $value->getSmallestDenomination(),
                        "account" => $value->getAccount(),
                        "iban" => $value->getIban(),
                        "bic" => $value->getBic(),
                        "accountCurrencyCode" => $value->getAccountCurrencyCode()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function delete(){
        if(isset($_GET['id'])){
            try {
                $entityManager = getEm();
                $arrayFilter = array(
                    "idcurrency" => $_GET['id']
                );

                $currencies = $entityManager->getRepository('Currency')->findBy($arrayFilter);
                $mysqlData = array ();
                foreach ($currencies as $value){
                    $dat = array (
                        "active" => $value->setActive("3"),
                        "date_delete" => $value->setDateDelete((new \DateTime()))
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $entityManager->persist($value);
                $entityManager->flush();

                $result  = 'success';
                $message = 'query success';
                $data = '';
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = '';
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }


}