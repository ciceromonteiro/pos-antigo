<?php

/**
 * Description of GeneralController
 * 
 * @abstract file created in 02/02/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 * 
 */

class GeneralController {

    public function index(){
        $navbar = "Setup|configuration";
        $taxas = getEm()->getRepository('Tax')->findBy(array('active' => 1));
        $array_answer = array (    
            "menu-active"   =>  "general",
            "taxes"         => $taxas
        );
        GenericController::template("Setup", "general","index", $navbar, $array_answer, 4);
    }
    
    public function update(){
        $arrayValues = array (
            'inv_default_value'                     => isset($_POST['inv_default_value']) ? $_POST['inv_default_value'] : '',
            'inv_type_tax'                          => isset($_POST['inv_type_tax']) ? $_POST['inv_type_tax'] : '',
            'inv_minimum_value_for_tax_free'        => isset($_POST['inv_minimum_value_for_tax_free']) ? $_POST['inv_minimum_value_for_tax_free'] : '',
            'inv_always_collect_tax_on_inv'         => isset($_POST['inv_always_collect_tax_on_inv']) ? 1 : 0,
            'oth_impt_only_product_name_acquired'   => isset($_POST['oth_impt_only_product_name_acquired']) ? 1 : 0,
            'oth_print_list_of_all_sold'            => isset($_POST['oth_print_list_of_all_sold']) ? 1 : 0,
            'pur_when_customer_is_change'           => isset($_POST['pur_when_customer_is_change']) ? 1 : 0,
            'pur_send_message_to_customer'          => isset($_POST['pur_send_message_to_customer']) ? 1 : 0,
            'pur_apply_default_discount'            => isset($_POST['pur_apply_default_discount']) ? 1 : 0,
            'pur_automatically_put_in_all_docs'     => isset($_POST['pur_automatically_put_in_all_docs']) ? 1 : 0
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            $general_settings = getEm()->getRepository('SettingsGeneral')->findBy(array('active' => 1));
            if($general_settings != null){
                foreach ($general_settings as $general) {
                    for($i = 0; $i < count($arrayKeys); $i++){
                        $key = $arrayKeys[$i]; 
                        if($general->getDataType() == $key){
                            $general->setDataValue($arrayValues[$key]);
                            $general->setDateUpdate(new DateTime());
                            getEm()->persist($general);
                            getEm()->flush();
                        }
                    }    
                }

            }else{
                for($i = 0; $i < count($arrayKeys); $i++){
                    $key = $arrayKeys[$i]; 
                    $general_settings = new SettingsGeneral();
                    $general_settings->setDataType($key);
                    $general_settings->setDataValue($arrayValues[$key]);
                    $general_settings->setDateUpdate(new DateTime());
                    $general_settings->setActive(1);
                    getEm()->persist($general_settings);
                    getEm()->flush();
                }
            }

            
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getGeneralSettings(){
        $arrayValues = array ();
        $dat = array ();
        try {
            $pos = getEm()->getRepository('Pos')->findAll();
            $generalSettings = getEm()->getRepository('SettingsGeneral')->findAll();
            if($generalSettings){
                foreach ($generalSettings as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    
}
