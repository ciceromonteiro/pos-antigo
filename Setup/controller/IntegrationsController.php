<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IntegrationsController
 * 
 * @abstract file created in 02/02/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 */
class IntegrationsController {

    public function index() {
        
        $navbar = "Setup|configuration";
        
        //$company = getEm()->getRepository('Company');
        
        $array_answer = array(
            "menu-active" => "integrations",
            "country" => "brazil"
        );
        
        GenericController::template("Setup", "integrations", "index", $navbar, $array_answer, 18);
    }
    
    public function getData(){
        $arrayValues = array ();
        $dat = array ();
        try {
            $integration = getEm()->getRepository('Integration')->findAll();
            if($integration[0]){
                $dat['webServiceUrl'] = $integration[0]->getExchangePlatform();
                $dat['webServiceUser'] = $integration[0]->getExchangeUser();
                $dat['webServicePassword'] = $integration[0]->getExchangePassword();
                $dat['bestsellerServer'] = $integration[0]->getBestsellerServer();
                $dat['bestsellerUser'] = $integration[0]->getBestsellerUser();
                $dat['bestsellerPassword'] = $integration[0]->getBestsellerPassword();
                $dat['bestsellerFtpCatalog'] = $integration[0]->getBestsellerCatalog();
            }
            $result  = 'success';
            $message = 'query success';
            $values = $dat;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    
    public function saveData(){
        // pegar form da requisição
        $var = "teste request";
        $arrayValues = array (
            'webServiceUrl' => isset($_REQUEST['webServiceUrl']) ? $_REQUEST['webServiceUrl'] : '',
            'webServiceUser' => isset($_REQUEST['webServiceUser']) ? $_REQUEST['webServiceUser'] : '',
            'webServicePassword' => isset($_REQUEST['webServicePassword']) ? $_REQUEST['webServicePassword'] : '',
            'bestsellerServer' => isset($_REQUEST['bestsellerServer']) ? $_REQUEST['bestsellerServer'] : '',
            'bestsellerUser' => isset($_REQUEST['bestsellerUser']) ? $_REQUEST['bestsellerUser'] : '',
            'bestsellerPassword' => isset($_REQUEST['bestsellerPassword']) ? $_REQUEST['bestsellerPassword'] : '',
            'bestsellerFtpCatalog' => isset($_REQUEST['bestsellerFtpCatalog']) ? $_REQUEST['bestsellerFtpCatalog'] : '',
        );
        
        // instanciar integration
        $findIntegration = getEm()->getRepository('Integration')->findAll();
        $integration = null;
        
        if (count($findIntegration) == 1){
            $integration = $findIntegration[0];
        } else if (count($findIntegration) == 0){
            $integration = new Integration;
        }
        
        // passar form para instancia de integration
        $integration->setExchangePlatform($arrayValues['webServiceUrl']);
        $integration->setExchangeUser($arrayValues['webServiceUser']);
        $integration->setExchangePassword($arrayValues['webServicePassword']);
        $integration->setBestsellerServer($arrayValues['bestsellerServer']);
        $integration->setBestsellerUser($arrayValues['bestsellerUser']);
        $integration->setBestsellerPassword($arrayValues['bestsellerPassword']);
        $integration->setBestsellerCatalog($arrayValues['bestsellerFtpCatalog']);
        
        // persistir instancia e mandar retorno pra view
        
        getEm()->persist($integration);
        getEm()->flush();
        
        $data = array(
            "result"  => "success",
            "message" => $arrayValues
        );
        
        $json_data = json_encode($data);
        print $json_data;
        
    }
    
    public function importBarcodeBrazil(){
        
        $var = '';
        foreach($_REQUEST as $key=>$value){
            $var = $key;
        }
        
        $var2 = base64_decode(str_replace('data:;base64,', '', $var));
        $var = json_decode($var2);
        
        $product = getEm()->getRepository('Product')->findBy(array("name" => $var->name));
        $product[0]->setBarcode($var->barcode);
        getEm()->flush();
        
        $data = array(
            "result"  => "success",
            "message" => $product[0]->getBarcode()
        );
        
        $json_data = json_encode($data);
        print $json_data;
        
    }   

     /**
     * insert integration 
     * @return string json_data 
     */

    public function insertIntegration($webServiceUrl, $webServiceUser, $webServicePassword){
    	$webServiceUrl = base64_decode($webServiceUrl);
    	$webServiceUser = base64_decode($webServiceUser);
    	$webServicePassword = base64_decode($webServicePassword);

        $webServiceUrl = str_replace("<?php", "", $webServiceUrl);
        $webServiceUrl = str_replace("?>", "", $webServiceUrl);
        $webServiceUrl = str_replace("<script>", "", $webServiceUrl);
        $webServiceUrl = str_replace("</script>", "", $webServiceUrl);

        $webServiceUser = str_replace("<?php", "", $webServiceUser);
        $webServiceUser = str_replace("?>", "", $webServiceUser);
        $webServiceUser = str_replace("<script>", "", $webServiceUser);
        $webServiceUser = str_replace("</script>", "", $webServiceUser);

        $webServicePassword = str_replace("<?php", "", $webServicePassword);
        $webServicePassword = str_replace("?>", "", $webServicePassword);
        $webServicePassword = str_replace("<script>", "", $webServicePassword);
        $webServicePassword = str_replace("</script>", "", $webServicePassword);

    	if($webServiceUrl == "0"){
    		$webServiceUrl = null;
    	}
    	if($webServiceUser == "0"){
    		$webServiceUser = null;
    	}
    	if($webServicePassword == "0"){
    		$webServicePassword = null;
    	}else{
    		$webServicePassword = md5($webServicePassword);
    	}
    	
    	
    	try {
            $Integration = getEm()->getRepository('Integration')->findOneBy(array());
            if($Integration == null){
                $Integration = new Integration();
            }  		
    		$Integration->setExchangePlatform($webServiceUrl);
    		$Integration->setExchangeUser($webServiceUser);
    		$Integration->setExchangePassword($webServicePassword);
    		getEm()->persist($Integration);
            getEm()->flush();

            $result = "success";
            $message = "query success";
            $data = "";

    	} catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );
        
        $json_data = json_encode($data);
        print $json_data;

    }

    
    public function getIntegrationWebService(){
         $Integration = getEm()->getRepository('Integration')->findOneBy(array(),array('idintegration' => 'DESC'));
         $webServiceUrl = null;
         $webServiceUser = null;
         $webServicePassword = null;

         $dat = array();
         try {
             if($Integration != null){
                if($Integration->getExchangePlatform() == null){
                    $webServiceUrl = " ";
                }else{
                    $webServiceUrl = $Integration->getExchangePlatform();
                }
                if($Integration->getExchangeUser() == null){
                    $webServiceUser = " ";
                }else{
                    $webServiceUser = $Integration->getExchangeUser();
                }                
                if($Integration->getExchangePassword() == null){
                    $webServicePassword = "";
                }else{
                    $webServicePassword = "**********";
                }

                $dat = array('webServiceUrl' => $webServiceUrl, 'webServiceUser' => $webServiceUser, 'webServicePassword' => $webServicePassword);

             }

            $result = "success";
            $message = "query success";
            $data = $dat;

        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = '';
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );
        //var_dump($data);
        $json_data = json_encode($data);
        print $json_data;

    }   
    
    public function testeRequest(){
        $data = array(
            "result"  => "success",
            "message" => "testeRequest"
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function importBestseller(){
        
        $product = null;
        $findProduct = null;
        
        $var = '';
        foreach($_REQUEST as $key=>$value){
            $var = $key;
        }
        
        //var_dump(count($_REQUEST));
        
        // verificar se arquivo foi passado ou não
        if (count($_REQUEST) == 2){
            $var2 = base64_decode(str_replace('data:;base64,', '', $var));
            $json = json_decode($var2);

            $findProduct = getEm()->getRepository('Product')->findBy(array("name" => $json->product->name));

            if (count($findProduct) == 1){
                //echo 'UPDATE';
                $product = $findProduct[0];
            } else {
                //echo 'CREATE';
                $product = new Product;
            }

            // manufacturer_idmanufacturer  ok
            if ($json->product->manufacturer_idmanufacturer != null){
                $product->setManufacturermanufacturer(getEm()->getRepository('Manufacturer')->findBy(array("idmanufacturer" => intval($json->product->manufacturer_idmanufacturer)))[0]);
            }
            // currency_idcurrency   int
            if ($json->product->currency_idcurrency != null){
                $product->setCurrencycurrency(getEm()->getRepository('Currency')->findBy(array("idcurrency" => intval($json->product->currency_idcurrency)))[0]);
            }
            // currency_purchase_price  ok
            if ($json->product->currency_purchase_price != null){
                $product->setCurrencyPurchasePrice($json->product->currency_purchase_price);
            }
            // unit_idunit  int
            if ($json->product->unit_idunit != null){
                $product->setUnitunit(getEm()->getRepository('Unit')->findBy(array("idunit" => intval($json->product->unit_idunit)))[0]);
            }
            // product_collection_idcollection  OK   int
            if ($json->product->product_collection_idcollection != null){
                $product->setProductCollectioncollection(getEm()->getRepository('ProductCollection')->findBy(array("idcollection" => intval($json->product->product_collection_idcollection)))[0]);
            }
            // supplier_idsupplier  OK    int
            if ($json->product->supplier_idsupplier != null){
                $product->setSuppliersupplier(getEm()->getRepository('Supplier')->findBy(array("idsupplier" => $json->product->supplier_idsupplier))[0]);
            }
            // product_department_idproduct_department   int
            if ($json->product->product_department_idproduct_department != null){
                $product_department = getEm()->getRepository('ProductDepartment')->findBy(array("idproductDepartment" => intval($json->product->product_department_idproduct_department)))[0];
            }
            // warranty_idwarranty  OK    int        
            if ($json->product->warranty_idwarranty != null){
                $product->setWarrantywarranty(getEm()->getRepository('Warranty')->findBy(array("idwarranty" => intval($json->product->warranty_idwarranty)))[0]);
            }
            // numberPack OK
            if ($json->product->numberPack != null){
                $product->setNumberpack(intval($json->product->numberPack));
            }
            // tax_gp  OK   int
            if ($json->product->tax_gp != null){
                $tribute = getEm()->getRepository('Tribute')->findBy(array("idtribute" => intval($json->product->tax_gp)))[0];
                $product->setTaxGp($tribute);
            }
            // print_for_pick_ist  OK   int
            if ($json->product->print_for_pick_ist != null){
                $printer = getEm()->getRepository('Printer')->findBy(array("idprinter" => intval($json->product->print_for_pick_ist)))[0];
                $product->setPrintForPickIst($printer);
            }
            // value_for_sell_brute  OK   decimal
            if ($json->product->value_for_sell_brute != null){
                $product->setValueForSellBrute(intval($json->product->value_for_sell_brute));
            }
            // value_for_sell_liquid  OK   decimal
            if ($json->product->value_for_sell_liquid != null){
                $product->setValueForSellLiquid(intval($json->product->value_for_sell_liquid));
            }
            // net_weight  OK    decimal
            if ($json->product->net_weight != null){
                $product->setNetWeight($json->product->net_weight);
            }
            // additional_weight  OK  decimal
            if ($json->product->additional_weight != null){
                $product->setAdditionalWeight($json->product->additional_weight);
            }
            // volume  OK  decimal        
            if ($json->product->volume != null){
                $product->setVolume($json->product->volume);
            }
            // length  OK   decimal
            if ($json->product->length != null){
                $product->setLength($json->product->length);
            }
            // width  OK   decimal
            if ($json->product->width != null){
                $product->setWidth($json->product->width);
            }
            // height  OK   decimal    
            if ($json->product->height != null){
                $product->setHeight($json->product->height);
            }
            // minimum_purchase_of_packages  OK   int            
            if ($json->product->minimum_purchase_of_packages != null){
                $product->setMinimumPurchaseOfPackages(intval($json->product->minimum_purchase_of_packages));
            }
            // minimum_purchase  OK  int
            if ($json->product->minimum_purchase != null){
                $product->setMinimumPurchase(intval($json->product->minimum_purchase));
            }
            // name  OK  string
            if ($json->product->name != null){
                $product->setName($json->product->name);
            }
            // price  OK   decimal
            if ($json->product->price != null){
                $product->setPrice($json->product->price);
            }
            // price_purchase  OK   decimal
            if ($json->product->price_purchase != null){
                $product->setPricePurchase($json->product->price_purchase);
            }
            // contribution  OK   float
            if ($json->product->contribution != null){
                $product->setContribution($json->product->contribution);
            }
            // promotion_weekdays  OK   char
            if ($json->product->promotion_weekdays != null){
                $product->setPromotionWeekdays($json->product->promotion_weekdays);
            }
            // price_recommended  OK   decimal
            if ($json->product->price_recommended != null){
                $product->setPriceRecommended($json->product->price_recommended);
            }
            // price_takeaway  OK   decimal
            if ($json->product->price_takeaway != null){
                $product->setPriceTakeaway($json->product->price_takeaway);
            }
            // price_purchase_list  OK   decimal
            if ($json->product->price_purchase_list != null){
                $product->setPricePurchaseList($json->product->price_purchase_list);
            }
            // price_limit_bargain  OK   decimal
            if ($json->product->price_limit_bargain != null){
                $product->setPriceLimitBargain($json->product->price_limit_bargain);
            }
            // price_large_scale  OK   decimal
            if ($json->product->price_large_scale != null){
                $product->setPriceLargeScale($json->product->price_large_scale);
            }
            // profit_gross_profit_value  OK   decimal
            if ($json->product->profit_gross_profit_value != null){
                $product->setProfitGrossProfitValue($json->product->profit_gross_profit_value);
            }
            // barcode  OK   int
            if ($json->product->barcode != null){
                $product->setBarcode($json->product->barcode);
            }
            // stock_enabled  OK   int
            if ($json->product->stock_enabled != null){
                $product->setStockEnabled($json->product->stock_enabled);
            }
            // min_stock  OK   int
            if ($json->product->min_stock != null){
                $product->setMinStock($json->product->min_stock);
            }
            // max_stock  OK   int
            if ($json->product->max_stock != null){
                $product->setMaxStock($json->product->max_stock);
            }
            // description  OK  string
            if ($json->product->description != null){
                $product->setDescription($json->product->description);
            }
            // short_description  OK   string
            if ($json->product->short_description != null){
                $product->setShortDescription($json->product->short_description);
            }
            // has_serial_number  OK   int
            if ($json->product->has_serial_number != null){
                $product->setHasSerialNumber($json->product->has_serial_number);
            }
            // factor_purchase  OK   int
            if ($json->product->factor_purchase != null){
                $product->setFactorPurchase($json->product->factor_purchase);
            }
            // factor_sell  OK   int
            if ($json->product->factor_sell != null){
                $product->setFactorSell(intval($json->product->factor_sell));
            }
            // has_components  OK   int
            if ($json->product->has_components != null){
                $product->setHasComponents($json->product->has_components);
            }
            // components_show_order_subitens  OK   int
            if ($json->product->components_show_order_subitens != null){
                $product->setComponentsShowOrderSubitens($json->product->components_show_order_subitens);
            }
            // has_size_and_color  OK   int
            if ($json->product->has_size_and_color != null){
                $product->setHasSizeAndColor($json->product->has_size_and_color);
            }
            // selling_product  OK   int
            if ($json->product->selling_product != null){
                $product->setSellingProduct($json->product->selling_product);
            }
            // date_create  ou date_update   OK   datetime
            $product->setDateCreate(new DateTime);
            if ($product->getIdproduct() != null){
                $product->setDateUpdate(new DateTime);
            } else {
                $product->setDateCreate(new DateTime);
            }
            // active  OK   int
            if ($json->product->active != null){
                $product->setActive($json->product->active);
            }
            // section_idsection      int
            if ($json->product->section_idsection != null){
                $section = getEm()->getRepository('Section')->findBy(array("idsection" => intval($json->product->section_idsection)))[0];
            }
            // create_by  OK   int
            if ($json->product->create_by != null){
                $product->setCreateBy(getEm()->getRepository('Users')->findBy(array("idusers" => intval($json->product->create_by)))[0]);
            }
            // output_control  OK   int
            if ($json->product->output_control != null){
                $product->setOutputControl(intval($json->product->output_control));
            }
            // type_product   OK   int
            $product->setTypeProduct(intval($json->product->type_product));
            if ($json->product->type_product != null){
                $product->setTypeProduct(intval($json->product->type_product));
            }
            // prediction_of_purchase  OK OK  int 
            if ($json->product->prediction_of_purchase != null){
                $product->setPredictionOfPurchase(intval($json->product->prediction_of_purchase));
            }
            // importing_rate  OK   decimal
            if ($json->product->importing_rate != null){
                $product->setImportingRate($json->product->importing_rate);
            }
            // freight_tax_amount  OK   int
            if ($json->product->freight_tax_amount != null){
                $product->setFreightTaxAmount($json->product->freight_tax_amount);
            }
            // value_of_shipping  OK   decimal
            if ($json->product->value_of_shipping != null){
                $product->setValueOfShipping($json->product->value_of_shipping);
            }
            // number_of_serie_from_product  OK   int
            if ($json->product->number_of_serie_from_product != null){
                $product->setNumberOfSerieFromProduct($json->product->number_of_serie_from_product);
            }
            // base_of_calc  OK   int
            if ($json->product->base_of_calc != null){
                $product->setBaseOfCalc($json->product->base_of_calc);
            }
            // multiple_units  OK   int
            if ($json->product->multiple_units != null){
                $product->setMultipleUnits($json->product->multiple_units);
            }
            // project  OK   int
            if ($json->product->project != null){
                $project = getEm()->getRepository('Project')->findBy(array("idproject" => intval($json->product->project)))[0];
                $product->setProject($project);
            }

            getEm()->persist($product);
            getEm()->flush();

            $data = array(
                "result"  => "success"
            );

            $json_data = json_encode($data);
            print $json_data;
            
            
        } else { 
            #== dados do ftp
            //echo '=== FTP ===';
            $integration = getEm()->getRepository('Integration')->findAll()[0];
            $host = $integration->getBestsellerServer();
            $user = $integration->getBestsellerUser();
            $pass = $integration->getBestsellerPassword();
            $file = $integration->getBestsellerCatalog();
            $conn = ftp_connect($host); 
            $content = null;
            
            #== url ftp
            $urlftp = "ftp://$user@$host:21";
            if ($pass != ''){
                $urlftp = "ftp://$user:$pass@$host:21";
            }

            if (ftp_login($conn, $user, $pass)){
                $contents = ftp_nlist($conn, ".");
                //echo 'login ftp';
                foreach($contents as $aux){
                    if ($aux == "./$file"){
                        $content = file_get_contents("ftp://$user:$pass@$host/$file");
                        $json = json_decode($content, true);
                        
                        //var_dump($json['product']['name']);
                        
                        
                        $findProduct = getEm()->getRepository('Product')->findBy(array("name" => $json['product']['name']));
                        
                        if (count($findProduct) == 1){
                            //echo 'UPDATE';
                            $product = $findProduct[0];
                        } else {
                            //echo 'CREATE';
                            $product = new Product;
                        }
                        
                        
                        if ($json['product']['manufacturer_idmanufacturer'] != null){
                            $product->setManufacturermanufacturer(getEm()->getRepository('Manufacturer')->findBy(array("idmanufacturer" => intval($json['product']['manufacturer_idmanufacturer'])))[0]);
                        }
                        // currency_idcurrency   int
                        if ($json['product']['currency_idcurrency'] != null){
                            $product->setCurrencycurrency(getEm()->getRepository('Currency')->findBy(array("idcurrency" => intval($json['product']['currency_idcurrency'])))[0]);
                        }
                        // currency_purchase_price  ok
                        if ($json['product']['currency_purchase_price'] != null){
                            $product->setCurrencyPurchasePrice($json['product']['currency_purchase_price']);
                        }
                        // unit_idunit  int
                        if ($json['product']['unit_idunit'] != null){
                            $product->setUnitunit(getEm()->getRepository('Unit')->findBy(array("idunit" => intval($json['product']['unit_idunit'])))[0]);
                        }
                        // product_collection_idcollection  OK   int
                        if ($json['product']['product_collection_idcollection'] != null){
                            $product->setProductCollectioncollection(getEm()->getRepository('ProductCollection')->findBy(array("idcollection" => intval($json['product']['product_collection_idcollection'])))[0]);
                        }
                        // supplier_idsupplier  OK    int
                        if ($json['product']['supplier_idsupplier'] != null){
                            $product->setSuppliersupplier(getEm()->getRepository('Supplier')->findBy(array("idsupplier" => $json['product']['supplier_idsupplier']))[0]);
                        }
                        // product_department_idproduct_department   int
                        if ($json['product']['product_department_idproduct_department'] != null){
                            $product_department = getEm()->getRepository('ProductDepartment')->findBy(array("idproductDepartment" => intval($json['product']['product_department_idproduct_department'])))[0];
                        }
                        // warranty_idwarranty  OK    int        
                        if ($json['product']['warranty_idwarranty'] != null){
                            $product->setWarrantywarranty(getEm()->getRepository('Warranty')->findBy(array("idwarranty" => intval($json['product']['warranty_idwarranty'])))[0]);
                        }
                        // numberPack OK
                        if ($json['product']['numberPack'] != null){
                            $product->setNumberpack(intval($json['product']['numberPack']));
                        }
                        // tax_gp  OK   int
                        if ($json['product']['tax_gp'] != null){
                            $tribute = getEm()->getRepository('Tribute')->findBy(array("idtribute" => intval($json['product']['tax_gp'])))[0];
                            $product->setTaxGp($tribute);
                        }
                        // print_for_pick_ist  OK   int
                        if ($json['product']['print_for_pick_ist'] != null){
                            $printer = getEm()->getRepository('Printer')->findBy(array("idprinter" => intval($json['product']['print_for_pick_ist'])))[0];
                            $product->setPrintForPickIst($printer);
                        }
                        // value_for_sell_brute  OK   decimal
                        if ($json['product']['value_for_sell_brute'] != null){
                            $product->setValueForSellBrute(intval($json['product']['value_for_sell_brute']));
                        }
                        // value_for_sell_liquid  OK   decimal
                        if ($json['product']['value_for_sell_liquid'] != null){
                            $product->setValueForSellLiquid(intval($json['product']['value_for_sell_liquid']));
                        }
                        // net_weight  OK    decimal
                        if ($json['product']['net_weight'] != null){
                            $product->setNetWeight($json['product']['net_weight']);
                        }
                        // additional_weight  OK  decimal
                        if ($json['product']['additional_weight'] != null){
                            $product->setAdditionalWeight($json['product']['additional_weight']);
                        }
                        // volume  OK  decimal        
                        if ($json['product']['volume']!= null){
                            $product->setVolume($json['product']['volume']);
                        }
                        // length  OK   decimal
                        if ($json['product']['length']!= null){
                            $product->setLength($json['product']['length']);
                        }
                        // width  OK   decimal
                        if ($json['product']['width']!= null){
                            $product->setWidth($json['product']['width']);
                        }
                        // height  OK   decimal    
                        if ($json['product']['height'] != null){
                            $product->setHeight($json['product']['height']);
                        }
                        // minimum_purchase_of_packages  OK   int            
                        if ($json['product']['minimum_purchase_of_packages'] != null){
                            $product->setMinimumPurchaseOfPackages(intval($json['product']['minimum_purchase_of_packages']));
                        }
                        // minimum_purchase  OK  int
                        if ($json['product']['minimum_purchase'] != null){
                            $product->setMinimumPurchase(intval($json['product']['minimum_purchase']));
                        }
                        // name  OK  string
                        if ($json['product']['name'] != null){
                            $product->setName($json['product']['name']);
                        }
                        // price  OK   decimal
                        if ($json['product']['price'] != null){
                            $product->setPrice($json['product']['price']);
                        }
                        // price_purchase  OK   decimal
                        if ($json['product']['price_purchase'] != null){
                            $product->setPricePurchase($json['product']['price_purchase']);
                        }
                        // contribution  OK   float
                        if ($json['product']['contribution'] != null){
                            $product->setContribution($json['product']['contribution']);
                        }
                        // promotion_weekdays  OK   char
                        if ($json['product']['promotion_weekdays'] != null){
                            $product->setPromotionWeekdays($json['product']['promotion_weekdays']);
                        }
                        // price_recommended  OK   decimal
                        if ($json['product']['price_recommended'] != null){
                            $product->setPriceRecommended($json['product']['price_recommended']);
                        }
                        // price_takeaway  OK   decimal
                        if ($json['product']['price_takeaway'] != null){
                            $product->setPriceTakeaway($json['product']['price_takeaway']);
                        }
                        // price_purchase_list  OK   decimal
                        if ($json['product']['price_purchase_list'] != null){
                            $product->setPricePurchaseList($json['product']['price_purchase_list']);
                        }
                        // price_limit_bargain  OK   decimal
                        if ($json['product']['price_limit_bargain'] != null){
                            $product->setPriceLimitBargain($json['product']['price_limit_bargain']);
                        }
                        // price_large_scale  OK   decimal
                        if ($json['product']['price_large_scale'] != null){
                            $product->setPriceLargeScale($json['product']['price_large_scale']);
                        }
                        // profit_gross_profit_value  OK   decimal
                        if ($json['product']['profit_gross_profit_value'] != null){
                            $product->setProfitGrossProfitValue($json['product']['profit_gross_profit_value']);
                        }
                        // barcode  OK   int
                        if ($json['product']['barcode'] != null){
                            $product->setBarcode($json['product']['barcode']);
                        }
                        // stock_enabled  OK   int
                        if ($json['product']['stock_enabled'] != null){
                            $product->setStockEnabled($json['product']['stock_enabled']);
                        }
                        // min_stock  OK   int
                        if ($json['product']['min_stock'] != null){
                            $product->setMinStock($json['product']['min_stock']);
                        }
                        // max_stock  OK   int
                        if ($json['product']['max_stock'] != null){
                            $product->setMaxStock($json['product']['max_stock']);
                        }
                        // description  OK  string
                        if ($json['product']['description'] != null){
                            $product->setDescription($json['product']['description']);
                        }
                        // short_description  OK   string
                        if ($json['product']['short_description'] != null){
                            $product->setShortDescription($json['product']['short_description']);
                        }
                        // has_serial_number  OK   int
                        if ($json['product']['has_serial_number'] != null){
                            $product->setHasSerialNumber($json['product']['has_serial_number']);
                        }
                        // factor_purchase  OK   int
                        if ($json['product']['factor_purchase'] != null){
                            $product->setFactorPurchase($json['product']['factor_purchase']);
                        }
                        // factor_sell  OK   int
                        if ($json['product']['factor_sell'] != null){
                            $product->setFactorSell(intval($json['product']['factor_sell']));
                        }
                        // has_components  OK   int
                        if ($json['product']['has_components'] != null){
                            $product->setHasComponents($json['product']['has_components']);
                        }
                        // components_show_order_subitens  OK   int
                        if ($json['product']['components_show_order_subitens'] != null){
                            $product->setComponentsShowOrderSubitens($json['product']['components_show_order_subitens']);
                        }
                        // has_size_and_color  OK   int
                        if ($json['product']['has_size_and_color'] != null){
                            $product->setHasSizeAndColor($json['product']['has_size_and_color']);
                        }
                        // selling_product  OK   int
                        if ($json['product']['selling_product'] != null){
                            $product->setSellingProduct($json['product']['selling_product']);
                        }
                        // date_create  ou date_update   OK   datetime
                        $product->setDateCreate(new DateTime);
                        if ($product->getIdproduct() != null){
                            $product->setDateUpdate(new DateTime);
                        } else {
                            $product->setDateCreate(new DateTime);
                        }
                        // active  OK   int
                        if ($json['product']['active'] != null){
                            $product->setActive($json['product']['active']);
                        }
                        // section_idsection      int
                        if ($json['product']['section_idsection'] != null){
                            $section = getEm()->getRepository('Section')->findBy(array("idsection" => intval($json['product']['section_idsection'])))[0];
                        }
                        // create_by  OK   int
                        if ($json['product']['create_by'] != null){
                            $product->setCreateBy(getEm()->getRepository('Users')->findBy(array("idusers" => intval($json['product']['create_by'])))[0]);
                        }
                        // output_control  OK   int
                        if ($json['product']['output_control'] != null){
                            $product->setOutputControl(intval($json['product']['output_control']));
                        }
                        // type_product   OK   int
                        if ($json['product']['type_product'] != null){
                            $product->setTypeProduct(intval($json['product']['type_product']));
                        }
                        // prediction_of_purchase  OK OK  int 
                        if ($json['product']['prediction_of_purchase'] != null){
                            $product->setPredictionOfPurchase(intval($json['product']['prediction_of_purchase']));
                        }
                        // importing_rate  OK   decimal
                        if ($json['product']['importing_rate'] != null){
                            $product->setImportingRate($json['product']['importing_rate']);
                        }
                        // freight_tax_amount  OK   int
                        if ($json['product']['freight_tax_amount'] != null){
                            $product->setFreightTaxAmount($json['product']['freight_tax_amount']);
                        }
                        // value_of_shipping  OK   decimal
                        if ($json['product']['value_of_shipping'] != null){
                            $product->setValueOfShipping($json['product']['value_of_shipping']);
                        }
                        // number_of_serie_from_product  OK   int
                        if ($json['product']['number_of_serie_from_product'] != null){
                            $product->setNumberOfSerieFromProduct($json['product']['number_of_serie_from_product']);
                        }
                        // base_of_calc  OK   int
                        if ($json['product']['base_of_calc'] != null){
                            $product->setBaseOfCalc($json['product']['base_of_calc']);
                        }
                        // multiple_units  OK   int
                        if ($json['product']['multiple_units'] != null){
                            $product->setMultipleUnits($json['product']['multiple_units']);
                        }
                        // project  OK   int
                        if ($json['product']['project']!= null){
                            $project = getEm()->getRepository('Project')->findBy(array("idproject" => intval($json['product']['project'])))[0];
                            $product->setProject($project);
                        }
                        
                        getEm()->persist($product);
                        getEm()->flush();

                        $data = array(
                            "result"  => "success"
                        );

                        $json_data = json_encode($data);
                        print $json_data;
                        
                        //return;
                    }
                }
            }
        }
        
    }

}
