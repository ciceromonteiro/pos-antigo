<?php

/**
 * Description of PosCompanyDataController
 * 
 * @abstract file created in 25/01/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 */
class PosCompanyDataController {
    
    private $entityManager;
    
    function __construct() {
        $this->entityManager = $this->getEntityManager();
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    function getEntityManager() {
        return getEm();
    }
    
    /**
     * 
     * @param PosCompany $posCompany
     * @param string $key
     * @param string $value
     * @param string $reference_name
     */
    public function updatePosCompanyData($posCompany, $posCompanyDataArray, $reference_name="" ){
        
        foreach ($posCompanyDataArray as $key => $value) {

            $criteria = array(
                "dataType"  =>  $key,
                "active"    =>  TRUE,
                "posCompanyposCompany"  => $posCompany,
            );

            $pcd = $this->entityManager->getRepository("PosCompanyData")->findBy($criteria);

            if(count($pcd) > 0){
                
                /*@var $pcd PosCompanyData*/
                $pcd = array_pop($pcd);
                $pcd->setDataValue($value);
                $pcd->setDateUpdate(new DateTime());
                
                if($reference_name != ""){
                    $pcd->setReferenceName($reference_name);
                }
                
            }else{
                
                $pcd = new posCompanyData();
                $pcd->setPosCompanyposCompany($posCompany);
                $pcd->setDataType($key);
                $pcd->setDataValue($value);
                $pcd->setDateCreate(new DateTime());            
                $pcd->setActive(true);

                $this->entityManager->persist($pcd);    
                                
            }
            
            $this->entityManager->flush();
        }
        
        //the flush method is in the posCompanyController
    }
}
