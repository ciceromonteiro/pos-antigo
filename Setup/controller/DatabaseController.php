<?php
/**
 * @author Claudio da Cruz Silva Junior>
 * @version 1.0.0
 * @abstract file created in date september 21, 2017
 */

class DatabaseController {
    /**
     * Index call database
     * 
     * @access public
     * @return void call preview
     */
    public function index() {

        $navbar = "Setup|configuration";
 
        $array_answer = array(
            "menu-active" => "database"
        );

        GenericController::template("Setup", "database", "index", $navbar, $array_answer, 32);
    }
        
    public function resetDB(){
        $permission = AuthenticationController::checkPermission('1','test');
        $con = getEm()->getConnection();
        $database = $con->getParams()['dbname'];
        $sqlAllTables = "SHOW TABLES";
        $result = $con->prepare($sqlAllTables);
        $result->execute();
        $arrayResult = $result->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($arrayResult as $value){
            $table = $value['Tables_in_'.$database];
            try {
                $conn = clone $con;
                $result = $conn->prepare("SET FOREIGN_KEY_CHECKS = 0; TRUNCATE TABLE ".$table);
                $result->execute();
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }
        unset($con, $conn);
    }
    
    public function importClients(){
        
        $person = new Person();
        
 
        try {
            $file = $_FILES["importXML"];
            $doc = new DOMDocument();
            $doc->load($file['tmp_name']);
            $xml = simplexml_load_file($file['tmp_name']);
            $create = true;
            $x = 0;
            
            foreach ($doc->getElementsByTagName('column') as $column){
                $x++;
                //idperson nn
                if ($column->getAttribute('name') == 'idperson' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != '') {
                    $id = intval($column->nodeValue);
                    $person = getEm()->getRepository('Person')->findBy(array('idperson' => intval($column->nodeValue)))[0];
                    $create = false;
                }
                
                //Pos OK ============
                if ($column->getAttribute('name') == 'pos_idpos' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != '') {
                    $pos = getEm()->getRepository('Pos')->findBy(array('idpos' => intval($column->nodeValue)));
                    $person->setPospos($pos[0]);
                }
                
                //CustomerGp  ERROR: Call to a member function format() on string in 
                if ($column->getAttribute('name') == 'customer_gp_idcustomer_gp' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $customer_gp = getEm()->getRepository('CustomerGp')->findBy(array('idcustomerGp' => intval($column->nodeValue)));
                    $person->setCustomerGpcustomerGp($customer_gp[0]);
                } 
                
                //User
                if ($column->getAttribute('name') == 'users_idusers' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $user = getEm()->getRepository('Users')->findBy(array('idusers' => intval($column->nodeValue)));
                    $person->setUsersusers($user[0]);
                }
                
                //Zz_state
                if ($column->getAttribute('name') == 'zz_state_idzz_state' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $zz_state = getEm()->getRepository('ZzState')->findBy(array('idzz_state' => intval($column->nodeValue)));
                    $person->setZzStatezzState($zz_state[0]);
                }
                
                //PersonSettings
                if ($column->getAttribute('name') == 'person_settings_idperson_settings' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $person_settings = getEm()->getRepository('PersonSettings')->findBy(array('idperson_settings' => intval($column->nodeValue)));
                    $person->setPersonSettingspersonSettings($person_settings[0]);
                }
                
                //PaymentMtd
                if ($column->getAttribute('name') == 'payment_mtd_idpayment_mtd' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $payment_mtd = getEm()->getRepository('PaymentMtd')->findBy(array('idpayment_mtd' => intval($column->nodeValue)));
                    $person->setPaymentMtdpaymentMtd($payment_mtd[0]);
                }
                
                //Department
                if ($column->getAttribute('name') == 'department_iddepartment' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $department = getEm()->getRepository('Department')->findBy(array('iddepartment' => intval($column->nodeValue)));
                    $person->setPaymentMtdpaymentMtd($department[0]);
                }
                
                //name
                if ($column->getAttribute('name') == 'name' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $name = $column->nodeValue;
                    $person->setName($name);
                } elseif ($column->getAttribute('name') == 'name'){
                    $result = 'error';
                    $message = $e->getMessage();
                    $json_data = json_encode($data);
                    print $json_data;
                    die();
                }
                
                //document_type
                if ($column->getAttribute('name') == 'document_type' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $document_type = $column->nodeValue;
                    $person->setDocumentType($document_type);
                }
                
                //document_number
                if ($column->getAttribute('name') == 'document_number' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $document_number = $column->nodeValue;
                    $person->setDocumentNumber($document_number);
                }
                
                //birth_date
                if ($column->getAttribute('name') == 'birth_date' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $birth_date = new DateTime($column->nodeValue);
                    $person->setBirthDate($birth_date);
                }
                
                //personal_number
                if ($column->getAttribute('name') == 'personal_number' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $personal_number = $column->nodeValue;
                    $person->setPersonalNumber($personal_number);
                }
                
                //contact_person
                if ($column->getAttribute('name') == 'contact_person' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $personal_number = $column->nodeValue;
                    $person->setContactPerson($personal_number);
                }
                
                //info
                if ($column->getAttribute('name') == 'info' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $info = $column->nodeValue;
                    $person->setInfo($info);
                }
                
                //genre
                if ($column->getAttribute('name') == 'genre' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $genre = intval($column->nodeValue);
                    $person->setGenre($genre);
                }
                
                //electronic_identification
                if ($column->getAttribute('name') == 'electronic_identification' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $electronic_identification = intval($column->nodeValue);
                    $person->setElectronicIdentification($electronic_identification);
                }
                
                //date_creation 
                if ($column->getAttribute('name') == 'date_creation' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $date_creation = new DateTime($column->nodeValue);
                    $person->setDateCreation($date_creation);
                } elseif($column->getAttribute('name') == 'date_creation'){
                    $result = 'error';
                    $message = $e->getMessage();
                    $json_data = json_encode($data);
                    print $json_data;
                    die();
                }
                
                //date_update
                if ($column->getAttribute('name') == 'date_update' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $date_update = new Date($column->nodeValue);
                    $person->setDateUpdate($date_update);
                }
                
                //date_delete
                if ($column->getAttribute('name') == 'date_delete' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $date_delete = new Date($column->nodeValue);
                    $person->setDateDelete($date_delete);
                }
                
                //active
                if ($column->getAttribute('name') == 'active' && $column->nodeValue != null && $column->nodeValue != 'NULL' && $column->nodeValue != ''){
                    $active = $column->nodeValue;
                    $person->setActive($active);
                } elseif ($column->getAttribute('name') == 'active'){
                    $result = 'error';
                    $message = $e->getMessage();
                    $json_data = json_encode($data);
                    print $json_data;
                    die();
                }
            }
            
            if ($create){
                getEm()->persist($person);
            }
            getEm()->flush();
            
            $result = 'success';
            $message = 'query success';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            'result' => $result,
            'message' => $message,
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
     /**
     * Get the database and user 
     * @return string json_data 
     */
    public function getAllDatabase(){

    try{
        $con = getEm()->getConnection();
        $database = $con->getParams()['dbname'];
        $user = $con->getParams()['user'];

        if($user == null){
            $user = "";
        }

        if($database == null){
            $database = "";
        }
        $result = "success";
        $message = "query success";
        $mysqlData = array("database" => $con->getParams()['dbname'], "user" => $con->getParams()['user']);
    }catch (Exception $e){
        $result = "error";
        $message = $e->getMessage();
        $mysqlData = "";
    }
  

    $data = array("result" => $result, "message" => $message, "data" => $mysqlData);
        $json_data = json_encode($data);
        $json_data = json_decode(json_encode($json_data), True);

        $json_data = str_replace(":{", ":[{", $json_data);
        $json_data = str_replace("}}", "}]}", $json_data);

        print $json_data;
    }

}
