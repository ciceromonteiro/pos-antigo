<?php

/**
 * Description of ExternalController
 * 
 * @abstract file created in 02/02/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 */
class ExternalController {
    /**
     * Index call Setup PosCompany
     * 
     * @access public
     * @return void call preview
     */
    public function index() {
        $navbar = "Setup|configuration";
        $array_answer = array(
            "menu-active" => "external"
        );
        GenericController::template("Setup", "external", "index", $navbar, $array_answer, 354);
    }
    /**
     * Verify Persons Duplicates 
     * @return string json_data 
     */
    public function verifyDuplicate(){
    	$allClientes = getEm()->getRepository('Person')->findBy(array("active" => 1));
    	$allClientes2 = getEm()->getRepository('Person')->findBy(array("active" => 1));
    	$cont = 0;

        
    	foreach ($allClientes as $client) {
    		foreach($allClientes2 as $client2){
                if($client->getDocumentNumber() != null && $client2->getDocumentNumber() != null){
    			     if($client->getDocumentNumber() == $client2->getDocumentNumber()){
    				    if($client->getIdperson() != $client2->getIdperson()){
    				        $cont++;
    			         }
			         }


    		      }
            }
    	}
    

    	if($cont == 0){
    		$result = 'success';
            $message = 'Sem dados duplicados';
            $data = $cont;

    	}else{
    		$result = 'error';
            $message = 'Dados duplicados';
            $data = "";
    	}

    	$data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

    	$json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Verify Persons Duplicates and replace Orders and Discounts of the New Person by old person
     * @return string json_data 
     */
    public function releaseDuplicate(){
    	$allClientes = getEm()->getRepository('Person')->findBy(array("active" => 1));
    	$allClientes2 = getEm()->getRepository('Person')->findBy(array("active" => 1));
    	$id1 = 0;
    	$id2 = 0;
    	$cont = 0;
    	$idOld = 0;
    	$idNew = 0;

    	foreach ($allClientes as $client) {
    		foreach($allClientes2 as $client2){
                if($client->getDocumentNumber() != null && $client2->getDocumentNumber() != null){
    			     if($client->getDocumentNumber() == $client2->getDocumentNumber()){
                        if($client->getActive() != 0 && $client->getActive() != 0){
    				        $id1 = $client->getIdperson();
    				        $id2 = $client2->getIdperson();

    				        if($id1 < $id2){
    					       $idOld = $id1;
    					       $idNew = $id2;
				            }else if($id2 < $id1){
    					       $idOld = $id2;
    					       $idNew = $id1;
    				        }
                            try {
                                $allorders = getEm()->getRepository('Order')->findBy(array("personperson" => $idNew));
                                $allDiscounts = getEm()->getRepository('Discount')->findBy(array("personIdperson" => $idNew));
                                $ClientOld = getEm()->getRepository('Person')->findOneBy(array("idperson" => $idOld));
                                //setar os valores com as do Id novo
                                foreach($allorders as $order){
                                    $order->setPersonperson($ClientOld);
                                    getEm()->persist($order);
                                    getEm()->flush();
                                }
                                foreach ($allDiscounts as $discount) {
                                    $discount->setPersonIdperson($ClientOld);
                                    getEm()->persist($discount);
                                    getEm()->flush();
                                }
                                //setar o active do id novo para inativo
                                $clients = getEm()->getRepository('Person')->findBy(array("active" => 1));
                                foreach ($clients as $clientDesactive) {
                                    if($clientDesactive->getIdperson() == $idNew){
                                        $clientDesactive->setActive(0);
                                        getEm()->persist($clientDesactive);
                                        getEm()->flush();
                                    }
                                }
                                $result = 'success';
                                $message = 'query success';
                                $data = "";
                            } catch (Exception $e) {
                                $result = 'error';
                                $message = $e->getMessage();
                                $data = "";
                            }

                        }
			         }
                }                   

    		}
    	}
        $data = array("result" => $result, "message" => $message, "data" => "");
    	$json_data = json_encode($data);
        print $json_data;
    }

    /**
     * upload of files Xls, Xml e Json with atributes for product
     * @return string json_data 
     */
    public function uploadXlsXmlJson(){
        $file = $_FILES['file'];
        $name = $file['name'];
        $tmp = $file['tmp_name'];

        $extensao = explode('.', $name);
        $ext = end($extensao);
        
        try{
            if(empty($file)){
                $data = "Selecione um arquivo";
                die();
            }else{
                if($ext == 'json' || $ext == 'JSON'){
                    $this->uploadJson($tmp);
                }else if($ext == 'xml' || $ext == 'XML'){
                    $this->uploadXml($tmp);
                }else if($ext == 'xls' || $ext == 'XLS'){
                    $this->uploadXls($tmp);
                }else{
                   $data = "arquivo em outro formato";
                   die(); 
                }
            }
        $result = 'success';
        $message = 'query success';
        $data = '';
        }catch(Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = '';


        }    

       
        $data = array("result" => $result, "message" => $message, "data" => $data);
        $json_data = json_encode($data);
        print $json_data;

    } 

    /**
     * get the string of the function uploadXlsXmlJson, case the file be Xls, and save in the table products in database
     * @param string tmp
     */
    public function uploadXls($tmp){
     try {    
        if(move_uploaded_file($tmp, '../data/temp/uploadProducts.xls')){   
            //carregar a classe
            require_once('../libraries/print/Classes/PHPExcel.php');

            //iniciar o objeto
            $objReader = new PHPExcel_Reader_Excel5();
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load('../data/temp/uploadProducts.xls');
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
         
            foreach ($sheetData as $dataArray) {


                if(is_numeric($dataArray['A'])){
                    $product = getEm()->getRepository('Product')->find(array('idproduct' => $dataArray['A']));

                if($product == null){
                    $product = new Product();
                }
                //1
                if($dataArray['B'] != NULL){
                    $manufacturer = getEm()->getRepository('Manufacturer')->find(array('idmanufacturer' => $dataArray['B']));
                    $product->setManufacturermanufacturer($manufacturer);
                }
                //2
                if($dataArray['C']!= NULL){
                    $currency = getEm()->getRepository('Currency')->find(array('idcurrency' => $dataArray['C']));
                    $product->setCurrencycurrency($currency);
                }
                //3
                $product->setCurrencyPurchasePrice($dataArray['D']);
                //4
                if($dataArray['E'] != NULL){
                    $unit = getEm()->getRepository('Unit')->find(array('idunit' => $dataArray['4']));
                    $product->setUnitunit($unit);
                }
                //5
                if($dataArray['F'] != NULL){
                    $collection = getEm()->getRepository('ProductCollection')->find(array('idcollection' => $dataArray['F']));
                    $product->setProductCollectioncollection($collection);
                }
                //6
                if($dataArray['G'] != NULL){
                    $supplier = getEm()->getRepository('Supplier')->find(array('idsupplier' => $dataArray['G']));
                    $product->setSuppliersupplier($supplier);
                }
                //7
                if($dataArray['H'] != NULL){
                    $departament = getEm()->getRepository('ProductDepartment')->find(array('idproductDepartment' => $dataArray['H']));
                    $product->setProductDepartmentproductDepartment($departament);
                }
                //8
                if($dataArray['I'] != NULL){
                    $warranty = getEm()->getRepository('Warranty')->find(array('idwarranty' => $dataArray['I']));
                    $product->setWarrantywarranty($warranty);
                }
                //9
                $product->setNumberpack($dataArray['J']);   
                //10
                if($dataArray['K'] != NULL){
                    $tribute = getEm()->getRepository('Tribute')->find(array('idtribute' => $dataArray['K']));
                    $product->setTaxGp($tribute);
                } 
                //11
                if($dataArray['L'] != NULL){
                    $printer = getEm()->getRepository('Printer')->find(array('idprinter' => $dataArray['L']));
                    $product->setPrintForPickIst($printer);
                }
                //12
                $product->setValueForSellBrute($dataArray['M']);
                //13
                $product->setValueForSellLiquid($dataArray['N']);
                //14
                $product->setNetWeight($dataArray['O']);
                //15
                $product->setAdditionalWeight($dataArray['P']);
                //16
                $product->setVolume($dataArray['Q']);
                //17
                $product->setLength($dataArray['R']);
                //18
                $product->setWidth($dataArray['S']);
                //19
                $product->setHeight($dataArray['T']);
                //20
                $product->setMinimumPurchaseOfPackages($dataArray['U']);
                //21
                $product->setMinimumPurchase($dataArray['V']);
                //22
                
                $product->setName($dataArray['W']);
                
                //23
                
                $product->setProductNumber($dataArray['X']);
                
                //24
                
                $product->setProductNumberSupplier($dataArray['Y']);
                
                
                //25
                
                $product->setProductNumberManufacturer($dataArray['Z']);
                
                
                //26
                $product->setPrice($dataArray['AA']);
                //27
                $product->setPricePurchase($dataArray['AB']);
                //28
                $product->setContribution($dataArray['AC']);
                //29
                
                $product->setPromotionWeekdays($dataArray['AD']);
                
                
                //30
                $product->setPriceRecommended($dataArray['AE']);
                //31
                $product->setPriceTakeaway($dataArray['AF']);
                //32
                $product->setPricePurchaseList($dataArray['AG']);
                //33
                $product->setPriceLimitBargain($dataArray['AH']);
                //34
                $product->setPriceLargeScale($dataArray['AI']);
                //35
                $product->setProfitGrossProfitValue($dataArray['AJ']);
                //36
                $product->setBarcode($dataArray['AK']);
                //37
                $product->setStockEnabled($dataArray['AL']);
                //38
                $product->setMinStock($dataArray['AM']);
                //39
                $product->setMaxStock($dataArray['AN']);
                //40
                
                $product->setDescription($dataArray['AO']);
                
                
                //41
                
                $product->setShortDescription($dataArray['AP']);
                
                
                
                //42
                $product->setHasSerialNumber($dataArray['AQ']);
                //43
                $product->setFactorPurchase($dataArray['AR']);
                //44
                $product->setFactorSell($dataArray['AS']);
                //45
                $product->setHasComponents($dataArray['AT']);
                //46
                $product->setComponentsShowOrderSubitens($dataArray['AU']);
                //47
                $product->setHasSizeAndColor($dataArray['AV']);
                //48
                $product->setSellingProduct($dataArray['AW']);
                //49

                if($dataArray['AX'] != NULL){
                    $dataCreate = new DateTime($dataArray['AX']);
                }else{
                    $dataCreate = new DateTime();   
                }

                $product->setDateCreate($dataCreate);
                //50
                if($dataArray['AY'] != NULL){
                    $dataUpdate = new DateTime($dataArray['AY']);
                    $product->setDateUpdate($dataUpdate);
                }
                //51
                
                if($dataArray['AZ']!= NULL){
                    $dataDelete = new DateTime($dataArray['AZ']);
                    $product->setDateDelete($dataDelete);

                }
                //52
                $product->setActive($dataArray['BA']);
                //53
                if($dataArray['BB'] != NULL){
                    $warehouse = getEm()->getRepository('Warehouse')->find(array('idwarehouse' => $dataArray['BB']));
                    $product->setSectionsection($warehouse);
                }
                //54
                if($dataArray['BC'] != NULL){
                    $user = getEm()->getRepository('Users')->find(array('idusers' => $dataArray['BC']));
                    $product->setCreateBy($user);
                }
                //55
                $product->setOutputControl($dataArray['BD']);
                //56
                $product->setTypeProduct($dataArray['BE']);
                //57
                $product->setPredictionOfPurchase($dataArray['BF']);
                //58
                $product->setImportingRate($dataArray['BG']);
                //59
                $product->setFreightTaxAmount($dataArray['BH']);
                //60
                $product->setValueOfShipping($dataArray['BI']);
                //61
                $product->setNumberOfSerieFromProduct($dataArray['BJ']);
                //62
                $product->setBaseOfCalc($dataArray['BK']);
                //63
                $product->setMultipleUnits($dataArray['BL']);
                //64
                if($dataArray['BM'] != NULL){
                    $project = getEm()->getRepository('Project')->find(array('idproject' => $dataArray['BM']));
                    $product->setProject($project);
                }

                getEm()->persist($product);
                getEm()->flush();  



                }else{
                    $teste = "não é um numero";
                    
                    
                }


            }


         }else{
        $data = "erro";
        var_dump($data);
        die();
        }
    }catch (Exception $e) {

        echo $e->getMessage();
        die();
    } 


    }

    /**
     * get the string of the function uploadXlsXmlJson, case the file be Xml, and save in the table products in database
     * @param string tmp
     */
    public function uploadXml($tmp){
    try {    
        if(move_uploaded_file($tmp, '../data/temp/uploadProducts.xml')){
            //verificar se o xml é o phpMyAdmin
            //$doc = new DOMDocument();
           // $doc->load('../data/temp/uploadProducts.xml');
            //$phpMyAdminUrl = $doc->getElementsByTagName("pma_xml_export")->item(0);
            //$teste = $phpMyAdminUrl->baseURI;
           // var_dump($doc);
            //die();

            $xml = simplexml_load_file('../data/temp/uploadProducts.xml');
            $arrData = json_encode($xml);
            $arrData = json_decode($arrData);
            $arrData = json_decode(json_encode($arrData), True);
           
            $database = $arrData['database'];
            $table = $database['table'];
            foreach($table as $tableInternal){
                //0 
                $product = getEm()->getRepository('Product')->find(array('idproduct' => $tableInternal['column']['0']));
                if($product == null){
                    $product = new Product();
                }
                //1
                if($tableInternal['column']['1'] != "NULL"){
                    $manufacturer = getEm()->getRepository('Manufacturer')->find(array('idmanufacturer' => $tableInternal['column']['1']));
                    $product->setManufacturermanufacturer($manufacturer);
                }
                //2
                if($tableInternal['column']['2']!= "NULL"){
                    $currency = getEm()->getRepository('Currency')->find(array('idcurrency' => $tableInternal['column']['2']));
                    $product->setCurrencycurrency($currency);
                }
                //3
                $product->setCurrencyPurchasePrice($tableInternal['column']['3']);
                //4
                if($tableInternal['column']['4'] != "NULL"){
                    $unit = getEm()->getRepository('Unit')->find(array('idunit' => $tableInternal['column']['4']));
                    $product->setUnitunit($unit);
                }
                //5
                if($tableInternal['column']['5'] != "NULL"){
                    $collection = getEm()->getRepository('ProductCollection')->find(array('idcollection' => $tableInternal['column']['5']));
                    $product->setProductCollectioncollection($collection);
                }
                //6
                if($tableInternal['column']['6'] != "NULL"){
                    $supplier = getEm()->getRepository('Supplier')->find(array('idsupplier' => $tableInternal['column']['6']));
                    $product->setSuppliersupplier($supplier);
                }
                //7
                if($tableInternal['column']['7'] != "NULL"){
                    $departament = getEm()->getRepository('ProductDepartment')->find(array('idproductDepartment' => $tableInternal['column']['7']));
                    $product->setProductDepartmentproductDepartment($departament);
                }
                //8
                if($tableInternal['column']['8'] != "NULL"){
                    $warranty = getEm()->getRepository('Warranty')->find(array('idwarranty' => $tableInternal['column']['8']));
                    $product->setWarrantywarranty($warranty);
                }
                //9
                $product->setNumberpack($tableInternal['column']['9']);   
                //10
                if($tableInternal['column']['10'] != "NULL"){
                    $tribute = getEm()->getRepository('Tribute')->find(array('idtribute' => $tableInternal['column']['10']));
                    $product->setTaxGp($tribute);
                } 
                //11
                if($tableInternal['column']['11'] != "NULL"){
                    $printer = getEm()->getRepository('Printer')->find(array('idprinter' => $tableInternal['column']['11']));
                    $product->setPrintForPickIst($printer);
                }
                //12
                $product->setValueForSellBrute($tableInternal['column']['12']);
                //13
                $product->setValueForSellLiquid($tableInternal['column']['13']);
                //14
                $product->setNetWeight($tableInternal['column']['14']);
                //15
                $product->setAdditionalWeight($tableInternal['column']['15']);
                //16
                $product->setVolume($tableInternal['column']['16']);
                //17
                $product->setLength($tableInternal['column']['17']);
                //18
                $product->setWidth($tableInternal['column']['18']);
                //19
                $product->setHeight($tableInternal['column']['19']);
                //20
                $product->setMinimumPurchaseOfPackages($tableInternal['column']['20']);
                //21
                $product->setMinimumPurchase($tableInternal['column']['21']);
                //22
                if(isset($tableInternal['column']['22']['@attributes'])){
                     $product->setName(null);
                }else{
                    $product->setName($tableInternal['column']['22']);
                }
                //23
                if(isset($tableInternal['column']['23']['@attributes'])){
                    $product->setProductNumber(null);
                }else{
                    $product->setProductNumber($tableInternal['column']['23']);
                } 
                //24
                if(isset($tableInternal['column']['24']['@attributes'])){
                    $product->setProductNumberSupplier(null);
                }else{
                    $product->setProductNumberSupplier($tableInternal['column']['24']);
                } 
                
                //25
                if(isset($tableInternal['column']['25']['@attributes'])){
                    $product->setProductNumberManufacturer(null);
                }else{
                    $product->setProductNumberManufacturer($tableInternal['column']['25']);
                } 
                
                //26
                $product->setPrice($tableInternal['column']['26']);
                //27
                $product->setPricePurchase($tableInternal['column']['27']);
                //28
                $product->setContribution($tableInternal['column']['28']);
                //29
                if(isset($tableInternal['column']['29']['@attributes'])){
                    $product->setPromotionWeekdays(null);
                }else{
                    $product->setPromotionWeekdays($tableInternal['column']['29']);
                } 
                
                //30
                $product->setPriceRecommended($tableInternal['column']['30']);
                //31
                $product->setPriceTakeaway($tableInternal['column']['31']);
                //32
                $product->setPricePurchaseList($tableInternal['column']['32']);
                //33
                $product->setPriceLimitBargain($tableInternal['column']['33']);
                //34
                $product->setPriceLargeScale($tableInternal['column']['34']);
                //35
                $product->setProfitGrossProfitValue($tableInternal['column']['35']);
                //36
                $product->setBarcode($tableInternal['column']['36']);
                //37
                $product->setStockEnabled($tableInternal['column']['37']);
                //38
                $product->setMinStock($tableInternal['column']['38']);
                //39
                $product->setMaxStock($tableInternal['column']['39']);
                //40
                if(isset($tableInternal['column']['40']['@attributes'])){
                    $product->setDescription(null);
                }else{
                     $product->setDescription($tableInternal['column']['40']);
                } 
                
                //41
                if(isset($tableInternal['column']['41']['@attributes'])){
                    $product->setShortDescription(null);
                }else{
                     $product->setShortDescription($tableInternal['column']['41']);
                } 
                
                
                //42
                $product->setHasSerialNumber($tableInternal['column']['42']);
                //43
                $product->setFactorPurchase($tableInternal['column']['43']);
                //44
                $product->setFactorSell($tableInternal['column']['44']);
                //45
                $product->setHasComponents($tableInternal['column']['45']);
                //46
                $product->setComponentsShowOrderSubitens($tableInternal['column']['46']);
                //47
                $product->setHasSizeAndColor($tableInternal['column']['47']);
                //48
                $product->setSellingProduct($tableInternal['column']['48']);
                //49

                if($tableInternal['column']['49'] != "NULL"){
                    $dataCreate = new DateTime($tableInternal['column']['49']);
                }else{
                    $dataCreate = new DateTime();   
                }

                $product->setDateCreate($dataCreate);
                //50
                if($tableInternal['column']['50'] != "NULL"){
                    $dataUpdate = new DateTime($tableInternal['column']['50']);
                    $product->setDateUpdate($dataUpdate);
                }
                //51
                
                if($tableInternal['column']['51'] != "NULL"){
                    $dataDelete = new DateTime($tableInternal['column']['51']);
                    $product->setDateDelete($dataDelete);

                }
                //52
                $product->setActive($tableInternal['column']['52']);
                //53
                if($tableInternal['column']['53'] != "NULL"){
                    $warehouse = getEm()->getRepository('Warehouse')->find(array('idwarehouse' => $tableInternal['column']['53']));
                    $product->setSectionsection($warehouse);
                }
                //54
                if($tableInternal['column']['54'] != "NULL"){
                    $user = getEm()->getRepository('Users')->find(array('idusers' => $tableInternal['column']['54']));
                    $product->setCreateBy($user);
                }
                //55
                $product->setOutputControl($tableInternal['column']['55']);
                //56
                $product->setTypeProduct($tableInternal['column']['56']);
                //57
                $product->setPredictionOfPurchase($tableInternal['column']['57']);
                //58
                $product->setImportingRate($tableInternal['column']['58']);
                //59
                $product->setFreightTaxAmount($tableInternal['column']['59']);
                //60
                $product->setValueOfShipping($tableInternal['column']['60']);
                //61
                $product->setNumberOfSerieFromProduct($tableInternal['column']['61']);
                //62
                $product->setBaseOfCalc($tableInternal['column']['62']);
                //63
                $product->setMultipleUnits($tableInternal['column']['63']);
                //64
                if($tableInternal['column']['64'] != "NULL"){
                    $project = getEm()->getRepository('Project')->find(array('idproject' => $tableInternal['column']['64']));
                    $product->setProject($project);
                }

                getEm()->persist($product);
                getEm()->flush();  

            }
  
        }else{
            $data = "erro";
            var_dump($data);
            die();
        }
        }catch (Exception $e) {

            echo $e->getMessage();
            die();
        } 
    }

    /**
     * get the string of the function uploadXlsXmlJson, case the file be Json, and save in the table products in database
     * @param string tmp
     */
    public function uploadJson($tmp){
    try {
         if(move_uploaded_file($tmp, '../data/temp/uploadProducts.json')){

                    $jsonData = file_get_contents('../data/temp/uploadProducts.json');
                    $arrData = json_decode($jsonData);

                    foreach ($arrData as $dataElement) {
                        foreach ($dataElement as $dataInternal) {
                            $dataInternalArray = json_decode(json_encode($dataInternal), True);
                           

                            $id = $dataInternalArray['idProduct'];
                            $product = getEm()->getRepository('Product')->find(array('idproduct' => $id));

                             if($product == null){

                                $product = new Product();
                                
                            }

                            if($dataInternalArray['date_create'] != null){
                                $dataCreate = new DateTime($dataInternalArray['date_create']['date']);
                            }else{
                                $dataCreate = new DateTime();   
                            }

                            if($dataInternalArray['date_update'] != null || $dataInternalArray['date_update'] != ""){
                                $dataUpdate = new DateTime($dataInternalArray['date_update']['date']);
                                $product->setDateUpdate($dataUpdate);
                            }else{
                                $dataUpdate = null;
                            }
                             
                            if($dataInternalArray['date_delete'] != null || $dataInternalArray['date_delete'] != ""){
                                $dataDelete = new DateTime($dataInternalArray['date_delete']['date']);
                                $product->setDateDelete($dataDelete);

                            }else{
                                $dataDelete = null;
                            }
                            
                            $product->setCurrencyPurchasePrice($dataInternalArray['price_purchase']);
                            $product->setValueForSellBrute($dataInternalArray['ValueSellBrute']);
                            $product->setValueForSellLiquid($dataInternalArray['value_for_sell_liquid']);
                            $product->setNetWeight($dataInternalArray['netWeight']);
                            $product->setAdditionalWeight($dataInternalArray['additionalWeight']);
                            $product->setVolume($dataInternalArray['volume']);
                            $product->setLength($dataInternalArray['length']);
                            $product->setWidth($dataInternalArray['width']);
                            $product->setHeight($dataInternalArray['height']);
                            $product->setMinimumPurchaseOfPackages($dataInternalArray['minimum_purchase_of_packages']);
                            $product->setMinimumPurchase($dataInternalArray['minimum_purchase']);
                            $product->setNumberpack($dataInternalArray['numberPack']);
                            $product->setName($dataInternalArray['name']);
                            $product->setProductNumber($dataInternalArray['product_number']);
                            $product->setProductNumberSupplier($dataInternalArray['product_number_supplier']);
                            $product->setProductNumberManufacturer($dataInternalArray['product_number_manufacturer']);
                            $product->setPrice($dataInternalArray['price']);
                            $product->setPricePurchase($dataInternalArray['price_purchase']);
                            $product->setContribution($dataInternalArray['contribution']);
                            $product->setPromotionWeekdays($dataInternalArray['promotion_weekdays']);
                            $product->setPriceRecommended($dataInternalArray['price_recommended']);
                            $product->setPriceTakeaway($dataInternalArray['price_takeaway']);
                            $product->setPricePurchaseList($dataInternalArray['price_purchase_list']);
                            $product->setPriceLimitBargain($dataInternalArray['price_limit_bargain']);
                            $product->setPriceLargeScale($dataInternalArray['price_large_scale']);
                            $product->setProfitGrossProfitValue($dataInternalArray['profit_gross_profit_value']);
                            $product->setBarcode($dataInternalArray['barcode']);
                            $product->setStockEnabled($dataInternalArray['stock_enabled']);
                            $product->setMinStock($dataInternalArray['min_stock']);
                            $product->setMaxStock($dataInternalArray['max_stock']);
                            $product->setDescription($dataInternalArray['description']);
                            $product->setShortDescription($dataInternalArray['short_description']);
                            $product->setHasSerialNumber($dataInternalArray['has_serial_number']);
                            $product->setFactorPurchase($dataInternalArray['factor_purchase']);
                            $product->setFactorSell($dataInternalArray['factor_sell']);
                            $product->setHasComponents($dataInternalArray['has_components']);
                            $product->setComponentsShowOrderSubitens($dataInternalArray['components_show_order_subitens']);
                            $product->setHasSizeAndColor($dataInternalArray['has_size_and_color']);
                            $product->setSellingProduct($dataInternalArray['selling_product']);
                            $product->setDateCreate($dataCreate);
                            $product->setActive($dataInternalArray['active']);
                            $product->setOutputControl($dataInternalArray['output_control']);
                            $product->setTypeProduct($dataInternalArray['type_product']);
                            $product->setPredictionOfPurchase($dataInternalArray['prediction_of_purchase']);
                            $product->setImportingRate($dataInternalArray['Importing_rate']);
                            $product->setFreightTaxAmount($dataInternalArray['freight_tax_amount']);
                            $product->setValueOfShipping($dataInternalArray['value_of_shipping']);
                            $product->setNumberOfSerieFromProduct($dataInternalArray['number_of_serie_from_product']);
                            $product->setBaseOfCalc($dataInternalArray['base_of_calc']);
                            $product->setMultipleUnits($dataInternalArray['multiple_units']);
                            

                            

                            if($dataInternalArray['idCurrency'] != null){
                                $currency = getEm()->getRepository('Currency')->find(array('idcurrency' => $dataInternalArray['idCurrency']));
                                $product->setCurrencycurrency($currency);
                            }

                            if($dataInternalArray['idManufacture'] != null){

                                $manufacturer = getEm()->getRepository('Manufacturer')->find(array('idproduct' => $dataInternalArray['idManufacture']));
                                $product->setManufacturermanufacturer($manufacturer);
                            }

                            if($dataInternalArray['idCollection'] != null){

                                $collection = getEm()->getRepository('ProductCollection')->find(array('idcollection' => $dataInternalArray['idCollection']));
                                $product->setProductCollectioncollection($collection);
                            }

                            if($dataInternalArray['idProductDepartament'] != null){

                                    $departament = getEm()->getRepository('ProductDepartment')->find(array('idproductDepartment' => $dataInternalArray['idProductDepartament']));
                                    $product->setProductDepartmentproductDepartment($departament);
                                }
                            if($dataInternalArray['taxGp'] != null){

                                    $tribute = getEm()->getRepository('Tribute')->find(array('idtribute' => $dataInternalArray['taxGp']));
                                    $product->setTaxGp($tribute);
                                }
                            if($dataInternalArray['printForPickIst'] != null){

                                $printer = getEm()->getRepository('Printer')->find(array('idprinter' => $dataInternalArray['printForPickIst']));
                                 $product->setPrintForPickIst($printer);
                            }
                            if($dataInternalArray['create_by'] != null){

                                $user = getEm()->getRepository('Users')->find(array('idusers' => $dataInternalArray['create_by']));
                                $product->setCreateBy($user);
                            }
                            if($dataInternalArray['project'] != null){

                                $project = getEm()->getRepository('Project')->find(array('idproject' => $dataInternalArray['project']));
                                $product->setProject($project);
                            }
                             if($dataInternalArray['section_idsection']!= null){

                                $warehouse = getEm()->getRepository('Warehouse')->find(array('idwarehouse' => $dataInternalArray['section_idsection']));
                                $product->setSectionsection($warehouse);
                            }

                             if($dataInternalArray['idSupplier']!= null){

                                        $supplier = getEm()->getRepository('Supplier')->find(array('idsupplier' => $dataInternalArray['idSupplier']));
                                         $product->setSuppliersupplier($supplier);
                                    }

                            if($dataInternalArray['idUnit'] != null){

                                        $unit = getEm()->getRepository('Unit')->find(array('idunit' => $dataInternalArray['idUnit']));
                                        $product->setUnitunit($unit);
                                    }
                             if($dataInternalArray['idWarranty'] != null){

                                        $warranty = getEm()->getRepository('Warranty')->find(array('idwarranty' => $dataInternalArray['idWarranty']));
                                         $product->setWarrantywarranty($warranty);
                                    }


                            getEm()->persist($product);
                            getEm()->flush();    
                                  
                        }
                    }
                }else{
                    $data = 'erro no upload';        
                }
       }catch (Exception $e) {

            echo $e->getMessage();
            die();
        }       

    }

    /**
     * Save new fidelity in database
     * @param string typeFidelity
     * @param string product
     * @param string quantity
     * @param string value
     * @param string typeAwards
     * @return string json_data 
     */
    public function insertFidelity($typeFidelity, $product, $quantity, $value, $typeAwards){

        if($typeFidelity != "0" && $typeAwards != "0"){
            if(($product != "0" && $quantity != "0") || $value != "0"){        
                try{
                    $typeFidelity = base64_decode($typeFidelity);
                    $typeFidelity = (int) $typeFidelity; 

                    $typeAwards = base64_decode($typeAwards);
                    $typeAwards = (int) $typeAwards;
                    $fidelity = new Fidelity();
                    $fidelity->setTypeFidelity($typeFidelity);
                    $fidelity->setAwardsType($typeAwards);
                    

                    if($quantity == "" || $quantity == "0"){
                        $quantity = NULL;
                    }else{
                        $quantity = base64_decode($quantity);
                        $quantity = (int) $quantity;
                        $fidelity->setQuantity($quantity);
                    }

                    if($value == "" || $value == "0"){
                        $value = NULL;
                    }else{
                        $value = base64_decode($value);
                        $fidelity->setValue($value);
                    }

                    if($product == "" || $product == "0"){
                        $product = NULL;
                    }else{
                        $product = base64_decode($product);
                        $productOB = getEm()->getRepository('Product')->findOneBy(array("idproduct"=> $product));
                        $fidelity->setProductproduct($productOB);
                    }

                    $fidelity->setDateCreate(new DateTime());
                    $fidelity->setActive(1);
                    getEm()->persist($fidelity);
                    getEm()->flush();

                    $result = "success";
                    $message = "query success";
                    $mysqlData = "";
                }catch (Exception $e){
                        $result = "error";
                        $message = $e->getMessage();
                        $mysqlData = "";
                }

                $data = array(
                    "result"  => $result,
                    "message" => $message,
                    "data"    => $mysqlData
                );

                $json_data = json_encode($data);
                print $json_data;
            }
        }
    }

    /**
     * Get all fidelity in database with atribute active=1
     * @return string json_data 
     */
    public function getAllFidelity(){
        try{
            $data = array();
            $fidelity = getEm()->getRepository('Fidelity')->findBy(array("active"=> 1));
            if($fidelity != null){
                foreach ($fidelity as $value) {
                    if($value->getValue() != NULL){
                        $fidelityValue = $value->getValue();
                    }else{
                        $fidelityValue = "";
                    }

                    if($value->getQuantity() != NULL){
                        $fidelityQtd = $value->getQuantity();
                    }else{
                        $fidelityQtd = "";
                    }

                    if($value->getProductproduct() != NULL){
                        $fidelityProduct = $value->getProductproduct()->getName();
                    }else{
                        $fidelityProduct = "";
                    }

                    if($value->getTypeFidelity() == 1){
                        $typeFidelity = "By product";
                    }else if($value->getTypeFidelity() == 2){
                        $typeFidelity = "By purchase value";
                    }else{
                        $typeFidelity = $value->getTypeFidelity();
                    }

                    if($value->getAwardsType() == 1){
                        $awardsType = "New Product";
                    }else if($value->getAwardsType() == 2){
                        $awardsType = "Discount";
                    }else{
                        $awardsType = $value->getAwardsType();
                    }

                    $dat = array (
                        "checkbox" => '<input type="checkbox" data-id="'.$value->getIdfidelity().'">',
                        "id" => $value->getIdfidelity(),
                        "typeFidelity" => $typeFidelity,
                        "value" => $fidelityValue,
                        "fidelityQtd" => $fidelityQtd,
                        "fidelityProduct" => $fidelityProduct,
                        "awardsType" => $awardsType
                    );                
                    array_push($data, $dat);
                }


            }

            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
        
    }
    /**
     * Get one fidelity with idfidelity equal parameter
     * @param string idFidelity
     * @return string json_data 
     */
    public function getFidelity($idFidelity){
        if(isset($idFidelity) && $idFidelity != null){
            try{
                $data = array();
                $fidelity = getEm()->getRepository('Fidelity')->findOneBy(array("idfidelity"=> $idFidelity));
                if($fidelity != null){

                    if($fidelity->getValue() != NULL){
                        $fidelityValue = $fidelity->getValue();
                    }else{
                        $fidelityValue = "";
                    }

                    if($fidelity->getQuantity() != NULL){
                        $fidelityQtd = $fidelity->getQuantity();
                    }else{
                        $fidelityQtd = "";
                    }

                    if($fidelity->getProductproduct() != NULL){
                        $fidelityProduct = $fidelity->getProductproduct()->getIdproduct();
                    }else{
                        $fidelityProduct = "";
                    }

                    $dat = array (
                        "id" => $fidelity->getIdfidelity(),
                        "typeFidelity" => $fidelity->getTypeFidelity(),
                        "value" => $fidelityValue,
                        "fidelityQtd" => $fidelityQtd,
                        "fidelityProduct" => $fidelityProduct,
                        "awardsType" => $fidelity->getAwardsType()
                    );                
                    array_push($data, $dat);

                }

                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            }catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    /**
     * Update the fidelity with id equal $id in parameter
     * @param string id
     * @param string typeFidelity
     * @param string product
     * @param string quantity
     * @param string value
     * @param string typeAwards
     * @return string json_data 
     */
    public function updateFidelity($id, $typeFidelity, $product, $quantity, $value, $typeAwards){
        if(isset($id) && $id != null){
            if($typeFidelity != "0" && $typeAwards != "0"){
                if(($product != "0" && $quantity != "0") || $value != "0"){ 
                    try{
                        $typeFidelity = base64_decode($typeFidelity);
                        $typeFidelity = (int) $typeFidelity; 

                        $typeAwards = base64_decode($typeAwards);
                        $typeAwards = (int) $typeAwards;
                        $id = base64_decode($id);
                        $fidelity = getEm()->getRepository('Fidelity')->findOneBy(array("idfidelity"=> $id));

                        

                        if($quantity == "" || $quantity == "0"){
                            $quantity = NULL;
                        }else{
                            $quantity = base64_decode($quantity);
                            $quantity = (int) $quantity;
                            
                        }

                        if($value == "" || $value == "0"){
                            $value = NULL;
                        }else{
                            $value = base64_decode($value);
                            
                        }

                        if($product == "" || $product == "0"){
                            $product = NULL;
                        }else{
                            $product = base64_decode($product);
                            $productOB = getEm()->getRepository('Product')->findOneBy(array("idproduct"=> $product));
                            
                        }
                        $fidelity->setQuantity($quantity);

                        $fidelity->setValue($value);
                        
                        $fidelity->setProductproduct($productOB);
                        $fidelity->setTypeFidelity($typeFidelity);
                        $fidelity->setAwardsType($typeAwards);
                        $fidelity->setDateUpdate(new DateTime());
                        getEm()->persist($fidelity);
                        getEm()->flush();

                        $result = "success";
                        $message = "query success";
                        $mysqlData = "";
                    }catch (Exception $e){
                            $result = "error";
                            $message = $e->getMessage();
                            $mysqlData = "";
                    }

                        $data = array(
                            "result"  => $result,
                            "message" => $message,
                            "data"    => $mysqlData
                        );

                        $json_data = json_encode($data);
                        print $json_data;
                }        
            }   
        }
    }

    /**
     * Delete the fidelities of database
     * @return string json_data 
     */
    public function deleteFidelity(){
        if($_POST['idFidelity']){
            try{
                $ids = $_POST['idFidelity'];
                for($i=0; $i < count($ids); $i++){
                    $fidelity = getEm()->getRepository("Fidelity")->findOneBy(array("idfidelity" => $ids[$i]));
                    $fidelity->setActive('3');
                    $fidelity->setDateDelete(new DateTime());
                    getEm()->persist($fidelity);
                    getEm()->flush();
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    /**
     * Insert the new code zipCode or update in case of existence one data with the id of person or with id of company
     * @param string typeCodeZipCode
     * @param string client
     * @param string company
     * @param string zipCode
     * @return string json_data 
     */
    public function insertCodeZipCode($typeCodeZipCode, $client, $company, $zipCode){
        if($typeCodeZipCode != '0' && $zipCode != '0'){
            if($client != '0' || $company != '0'){
                try{
                    $typeCodeZipCode = base64_decode($typeCodeZipCode);
                    $zipCode = base64_decode($zipCode);
                    $client = base64_decode($client);
                    $company = base64_decode($company);

                    if($client != ""){
                        $CodeZipCode = getEm()->getRepository('CodeZipCode')->findOneBy(array("personperson"=> $client));

                    }else if($company != ""){
                        $CodeZipCode = getEm()->getRepository('CodeZipCode')->findOneBy(array("companycompany"=> $company));

                    }

                    if($client == ""){
                        $client = null;
                    }

                    if($company == ""){
                        $company = null;
                    }

                    if($CodeZipCode != null && $client != null){
                        $CodeZipCode->setZipCode($zipCode);
                        $CodeZipCode->setDateUpdate(new DateTime());
                        $CodeZipCode->setActive(1);

                    }else if($CodeZipCode != null && $company != null){
                        $CodeZipCode->setZipCode($zipCode);
                        $CodeZipCode->setDateUpdate(new DateTime());
                        $CodeZipCode->setActive(1);

                    }else{
                        $Person = getEm()->getRepository('Person')->findOneBy(array("idperson"=> $client));
                        $Company = getEm()->getRepository('Company')->findOneBy(array("idcompany"=> $company));
                        $CodeZipCode = new CodeZipCode();
                        if($Person != null){
                            $CodeZipCode->setPersonperson($Person);
                        }

                        if($Company != null){
                            $CodeZipCode->setCompanycompany($Company);
                        }
                        $CodeZipCode->setZipCode($zipCode);
                        $CodeZipCode->setDateCreate(new DateTime());
                        $CodeZipCode->setActive(1);

                    }

                    getEm()->persist($CodeZipCode);
                    getEm()->flush();

                    $result = "success";
                    $message = "query success";
                    $mysqlData = "";
                }catch (Exception $e){
                    $result = "error";
                    $message = $e->getMessage();
                    $mysqlData = "";
                }

                $data = array(
                    "result"  => $result,
                    "message" => $message,
                    "data"    => $mysqlData
                );

                $json_data = json_encode($data);
                print $json_data;


            }

        }

    }

    /**
     * Get all datas of the table code_zip_code with active equal 1
     * @return string json_data 
     */
    public function getAllCodeZipCode(){
        try{
            $data = array();
            $CodeZipCode = getEm()->getRepository('CodeZipCode')->findBy(array("active"=> 1));
            if($CodeZipCode != null){
                foreach ($CodeZipCode as $value) {
                    if($value->getPersonperson() != NULL){
                        $personValue = $value->getPersonperson()->getName();
                    }else{
                        $personValue = "";
                    }

                    if($value->getCompanycompany() != NULL){
                        $companyValue = $value->getCompanycompany()->getName();
                    }else{
                        $companyValue = "";
                    }

                    $dat = array (
                        "checkbox" => '<input type="checkbox" data-id="'.$value->getIdCodeZipCode().'">',
                        "id" => $value->getIdCodeZipCode(),
                        "client" => $personValue,
                        "company" => $companyValue,
                        "zipCode" => $value->getZipCode()
                    );                
                    array_push($data, $dat);
                }


            }

            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
        
    } 
    /**
     * Get data of the table code_zip_code with idcode_zip_code equal $idZipCode
     * @param string idZipCode
     * @return string json_data 
     */
    public function getCodeZipCode($idZipCode){
        if(isset($idZipCode) && $idZipCode != null){
            try{
                $data = array();
                $CodeZipCode = getEm()->getRepository('CodeZipCode')->findOneBy(array("idCodeZipCode"=> $idZipCode));
                if($CodeZipCode != null){

                    if($CodeZipCode->getPersonperson() != NULL){
                        $personValue = $CodeZipCode->getPersonperson()->getName();
                    }else{
                        $personValue = 0;
                    }

                    if($CodeZipCode->getCompanycompany() != NULL){
                        $companyValue = $CodeZipCode->getCompanycompany()->getName();
                    }else{
                        $companyValue = 0;
                    }

                    $dat = array (
                        "id" => $CodeZipCode->getIdCodeZipCode(),
                        "client" => $personValue,
                        "company" => $companyValue,
                        "zipCode" => $CodeZipCode->getZipCode()
                    );                
                    array_push($data, $dat);

                }

                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            }catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    /**
     * Update code zipCode or update in case of existence one data with the id of person or with id of company
     * @param string id
     * @param string zipCode
     * @return string json_data 
     */
    public function updateCodeZipCode($id, $zipCode){

        if(isset($id) && $id != null){
            if($zipCode != '0'){
                try{
                    $id = base64_decode($id);
                    $zipCode = base64_decode($zipCode);

                    $CodeZipCode = getEm()->getRepository('CodeZipCode')->findOneBy(array("idCodeZipCode"=> $id));
                    $CodeZipCode->setZipCode($zipCode);
                    $CodeZipCode->setDateCreate(new DateTime());
                    $CodeZipCode->setActive(1);
                    getEm()->persist($CodeZipCode);
                    getEm()->flush();

                    $result = "success";
                    $message = "query success";
                    $mysqlData = "";
                }catch (Exception $e){
                    $result = "error";
                    $message = $e->getMessage();
                    $mysqlData = "";
                }

                $data = array(
                    "result"  => $result,
                    "message" => $message,
                    "data"    => $mysqlData
                );

                $json_data = json_encode($data);
                print $json_data;

            }
        }

    }

    /**
     * Delete the Code(s) zipCode(s) of database
     * @return string json_data 
     */
    public function deleteCodeZipCode(){
        if($_POST['idZipCode']){
            try{
                $ids = $_POST['idZipCode'];
                for($i=0; $i < count($ids); $i++){
                    $CodeZipCode = getEm()->getRepository("CodeZipCode")->findOneBy(array("idCodeZipCode" => $ids[$i]));
                    $CodeZipCode->setActive('0');
                    $CodeZipCode->setDateDelete(new DateTime());
                    getEm()->persist($CodeZipCode);
                    getEm()->flush();
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    /**
     * Generete the new file of shipping, system from Brazil, for bank's Itau, BANCO DO BRASIL, CEF, SANTANDER and BRADESCO. Note, only ITAU and BANCO DO BRASIL working in the moment. 
     * @return string json_data 
     */
    public function exportShippingFile(){
        include "../libraries/Cnab/vendor/autoload.php";
        $posCompany = getEm()->getRepository('PosCompany')->findOneBy(array('active' => 1));
        if($posCompany != null && $posCompany->getBankBr() != null){
            try{
                switch ($posCompany->getBankBr()) {
                    case '1':
                        $codigo_banco = \Cnab\Banco::ITAU;
                        $arquivo = new \Cnab\Remessa\Cnab400\Arquivo($codigo_banco);
                        $arquivo->configure(array(
                            'data_geracao' => new \DateTime(),
                            'data_gravacao' => new \DateTime(),
                            'nome_fantasia' => $posCompany->getFantasyName(),
                            'razao_social' => $posCompany->getName(),
                            'cnpj' => $posCompany->getRegisterNumber(),
                            'banco' => $codigo_banco, //código do banco
                            'logradouro' => $posCompany->getAddressStreet(),
                            'numero' => $posCompany->getAddressNumber(),
                            'bairro' => $posCompany->getAddressDistrict(),
                            'cidade' => $posCompany->getAddressCity(),
                            'uf' => $posCompany->getZzStatezzState()->getName(),
                            'cep' => $posCompany->getAddressZipcode(),
                            'agencia' => $posCompany->getAgencyBr(),
                            'conta' => $posCompany->getAccountBr(),
                            'conta_dac' => $posCompany->getDigitAccountBr()
                        ));
                        break;
                    case '2':
                        $codigo_banco = \Cnab\Banco::BANCO_DO_BRASIL;
                        $arquivo = new \Cnab\Remessa\Cnab240\Arquivo($codigo_banco);
                        $arquivo->configure(array(
                            'data_geracao' => new \DateTime(),
                            'data_gravacao' => new \DateTime(),
                            'nome_fantasia' => $posCompany->getFantasyName(),
                            'razao_social' => $posCompany->getName(),
                            'cnpj' => $posCompany->getRegisterNumber(),
                            'banco' => $codigo_banco, //código do banco
                            'logradouro' => $posCompany->getAddressStreet(),
                            'numero' => $posCompany->getAddressNumber(),
                            'bairro' => $posCompany->getAddressDistrict(),
                            'cidade' => $posCompany->getAddressCity(),
                            'uf' => $posCompany->getZzStatezzState()->getName(),
                            'cep' => $posCompany->getAddressZipcode(),
                            'agencia' => $posCompany->getAgencyBr(),
                            'conta' => $posCompany->getAccountBr(),             
                            'conta_dv' => $posCompany->getDigitAccountBr(),
                            'codigo_convenio' => $posCompany->getCodeAgreementBr(),
                            'codigo_carteira' => $posCompany->getWalletCodeBr(),
                            'variacao_carteira' => $posCompany->getWalletVariationBr(),
                            'agencia_dv' => $posCompany->getAgencyDvBr(),
                            'operacao' => $posCompany->getOperationBr(),
                            'numero_sequencial_arquivo' => $posCompany->getSequenceNumberFileBr()
                        ));
                        break;
                    case '3':
                        $codigo_banco = \Cnab\Banco::CEF;
                        $arquivo = new \Cnab\Remessa\Cnab240\Arquivo($codigo_banco);
                        break;
                    case '4':
                        $codigo_banco = \Cnab\Banco::SANTANDER;
                        $arquivo = new \Cnab\Remessa\Cnab400\Arquivo($codigo_banco);
                        break;
                    case '5':
                        $codigo_banco = \Cnab\Banco::BRADESCO;
                        $arquivo = new \Cnab\Remessa\Cnab400\Arquivo($codigo_banco);
                        break;                   
                    default:
                        $codigo_banco = null;
                        break;
                }

                $specie = NULL;

                if($posCompany->getBankBr() == '1'){
                     switch ($posCompany->getSpecieBr()) {
                        case '1':
                            $specie = Cnab\Especie::ITAU_DUPLICATA_MERCANTIL;
                            break;
                        case '2':
                            $specie = Cnab\Especie::ITAU_NOTA_PROMISSORIA;
                            break;
                        case '3':
                            $specie = Cnab\Especie::ITAU_NOTA_DE_SEGURO;
                            break;
                        case '4':
                            $specie = Cnab\Especie::ITAU_MENSALIDADE_ESCOLAR;
                            break;
                        case '5':
                            $specie = Cnab\Especie::ITAU_RECIBO;
                            break;
                        case '6':
                            $specie = Cnab\Especie::ITAU_CONTRATO;
                            break;  
                        case '7':
                            $specie = Cnab\Especie::ITAU_COSSEGUROS;
                            break;  
                        case '8':
                            $specie = Cnab\Especie::ITAU_DUPLICATA_DE_SERVICO;
                            break;  
                        case '9':
                            $specie = Cnab\Especie::ITAU_LETRA_DE_CAMBIO;
                            break; 
                        case '13':
                            $specie = Cnab\Especie::ITAU_NOTA_DE_DEBITOS;
                            break; 
                        case '15':
                            $specie = Cnab\Especie::ITAU_DOCUMENTO_DE_DIVIDA;
                            break; 
                        case '16':
                            $specie = Cnab\Especie::ITAU_ENCARGOS_CONDOMINIAIS;
                            break; 
                        case '17':
                            $specie = Cnab\Especie::ITAU_CONTRATO_DE_PRESTACAO_DE_SERVICOS;
                            break; 
                        case '99':
                            $specie = Cnab\Especie::ITAU_DIVERSOS;
                            break;                                         
                        default:
                            $specie = null;
                            break;
                    }
                }else if($posCompany->getBankBr() == '2'){
                    switch ($posCompany->getSpecieBr()) {
                        case '1':
                            $specie = Cnab\Especie::BB_CHEQUE;
                            break;
                        case '2':
                            $specie = Cnab\Especie::BB_DUPLICATA_MERCANTIL;
                            break;
                        case '4':
                            $specie = Cnab\Especie::BB_DUPLICATA_DE_SERVICO;
                            break;
                        case '6':
                            $specie = Cnab\Especie::BB_DUPLICATA_RURAL;
                            break;
                        case '7':
                            $specie = Cnab\Especie::BB_LETRA_DE_CAMBIO;
                            break;
                        case '12':
                            $specie = Cnab\Especie::BB_NOTA_PROMISSORIA;
                            break;  
                        case '17':
                            $specie = Cnab\Especie::BB_RECIBO;
                            break;  
                        case '19':
                            $specie = Cnab\Especie::BB_NOTA_DE_DEBITOS;
                            break;  
                        case '26':
                            $specie = Cnab\Especie::BB_WARRANT;
                            break; 
                        case '27':
                            $specie = Cnab\Especie::BB_DIVIDA_ATIVA_DE_ESTADO;
                            break; 
                        case '28':
                            $specie = Cnab\Especie::BB_DIVIDA_ATIVA_DE_MUNICIPIO;
                            break; 
                        case '29':
                            $specie = Cnab\Especie::BB_DIVIDA_ATIVA_UNIAO;
                            break; 
                        case '16':
                            $specie = Cnab\Especie::BB_NOTA_DE_SEGURO;
                            break; 
                        case '20':
                            $specie = Cnab\Especie::BB_APOLICE_DE_SEGURO;
                            break;                                        
                        default:
                            $specie = null;
                            break;
                    }
                }else if($posCompany->getBankBr() == '3'){
                    switch ($posCompany->getSpecieBr()) {
                        case '2':
                            $specie = Cnab\Especie::CEF_DUPLICATA_MERCANTIL;
                            break;
                        case '12':
                            $specie = Cnab\Especie::CEF_NOTA_PROMISSORIA;
                            break;
                        case '4':
                            $specie = Cnab\Especie::CEF_DUPLICATA_DE_PRESTACAO_DE_SERVICOS;
                            break;
                        case '16':
                            $specie = Cnab\Especie::CEF_NOTA_DE_SEGURO;
                            break;
                        case '7':
                            $specie = Cnab\Especie::CEF_LETRA_DE_CAMBIO;
                            break;
                        case '99':
                            $specie = Cnab\Especie::CEF_OUTROS;
                            break;                           
                        default:
                            $specie = null;
                            break;
                    }
                }else{
                    $specie = Cnab\Especie::CNAB240_OUTROS;
                }
                $Invoice = getEm()->getRepository('Invoice')->findBy(array("statusBank" => 0));
                $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
                $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
                $Address = getEm()->getRepository('Address')->findBy(array("active" => 1));
                $idCustomer = 0;
                $nameCustomer = null;
                $nameEmployee = null;
                $liquidValue = 0;
                $data = array ();
                if($Invoice != null){
                    foreach ($Invoice as $invoice) {
                        if($Orders != null){
                            $idCustomer = 0;
                            $nameCustomer = null;
                            $nameEmployee = null;
                            $liquidValue = 0;
                            $discount = 0;
                            $valueGross = 0;
                            foreach ($Orders as $order) {
                                if($invoice->getOrderorder()->getIdorder() == $order->getIdorder()){
                                    //dados do cliente
                                    $idCustomer = $order->getPersonperson()->getIdperson();
                                    $nameCustomer = $order->getPersonperson()->getName();
                                    $typeDocument = $order->getPersonperson()->getDocumentType();
                                    //dados do vendedor
                                    $nameEmployee = $order->getUsersusers()->getPersonperson()->getName();
                                    
                                    foreach ($OrderLine as $orderLine) {
                                        if($order->getIdorder() == $orderLine->getOrderorder()->getIdorder()){
                                            $valueGross = $valueGross + $orderLine->getActualValue();
                                            $discount = $discount + $orderLine->getDiscount();
                                        }
                                    }
                                    $street = "";
                                    $district = "";
                                    $city = "";
                                    $zipCode = 00000000000;
                                    $state = "";
                                    foreach ($Address as $address) {
                                        if($idCustomer == $address->getPersonperson()->getIdperson()){
                                            $street = "";
                                            if($address->getAddressStreet() != NULL){
                                                $street = $address->getAddressStreet();
                                            }

                                            $district = "";
                                            if($address->getAddressDistrict() != NULL){
                                                $district = $address->getAddressDistrict();
                                            }

                                            $city = "";
                                            if($address->getAddressCity() != NULL){
                                                $city = $address->getAddressCity();
                                            }
                                            
                                            
                                            if($address->getAddressZipcode() != NULL){
                                                $zipCode = $address->getAddressZipcode();
                                            }

                                            $state = "";
                                            if($address->getZzStatezzState()->getName() != NULL){
                                                $state = $address->getZzStatezzState()->getName();
                                            }

                                        }
                                    }
                                    $discount = $discount + $order->getDiscount();
                                    $liquidValue = $valueGross - $discount;

                                    $dateDue = 0;
                                    if($invoice->getDateDue() != NULL){
                                        $dateDue = $invoice->getDateDue();
                                    }
                                    $datemulct = 0;
                                    if($invoice->getDateDue() != NULL){
                                        $day = $dateDue->format('d')+1;
                                        $month = $dateDue->format('m');
                                        $year = $dateDue->format('Y');
                                        $datemulct = new DateTime($year."-".$month."-".$day);

                                    }
                                    $dateCreate = 0;
                                    if($invoice->getDateCreate() != NULL){
                                        $dateCreate = $invoice->getDateCreate();
                                    }
                                    $interest = $posCompany->getInterestDayMaturityInvoiceBr();
                                    $ValueInterest = ($liquidValue*$interest)/100;
                                    $wallet = 0;
                                    if($posCompany->getBankWalletBr() != NULL){
                                        $wallet = $posCompany->getBankWalletBr();
                                    }
                                    $deadline = 0;
                                    if($posCompany->getDaysAfterMaturityInvoiceBr() != NULL){
                                        $deadline = $posCompany->getDaysAfterMaturityInvoiceBr();
                                    }
                                    $info = "";
                                    if($invoice->getExtraInfo() != NULL){
                                        $info = $invoice->getExtraInfo();
                                    }
                                    $mulct = 0;
                                    if($posCompany->getValueOfMulctBr() != NULL){
                                        $mulct = $posCompany->getValueOfMulctBr();
                                    }
                                    $limitDateDiscount = 0;
                                    if($invoice->getLimitDateDiscount() != NULL){
                                        $limitDateDiscount = $invoice->getLimitDateDiscount();
                                    }
                                    $valueDiscountOrder = 0;
                                    if($order->getDiscount() != NULL){
                                        $valueDiscountOrder = $order->getDiscount();
                                    }
                                    $document = 0;
                                    if($order->getPersonperson()->getDocumentNumber() != NULL){
                                        $document = $order->getPersonperson()->getDocumentNumber();
                                    }
                                    $codeWallet = 0;
                                    if($posCompany->getWalletCodeBr() != NULL){
                                        $codeWallet = $posCompany->getWalletCodeBr();
                                    }
                                    $registeredInvoice = 0;
                                    if($posCompany->getRegisteredInvoiceBr() != NULL){
                                        $registeredInvoice = $posCompany->getRegisteredInvoiceBr();
                                    }
                                    $aceite = 0;
                                    if($posCompany->getAceiteBr() != NULL){
                                        $aceite = $posCompany->getAceiteBr();
                                    }
                                    //verifica se o tipo de documento do cliente é CPF ou CNPJ
                                    $typeDocument = "cpf";
                                    if($typeDocument == "cpf" || $typeDocument == "cnpj"){

                                        $arquivo->insertDetalhe(array(
                                            'codigo_de_ocorrencia' => 1, // 1 = Entrada de título, futuramente poderemos ter uma constante
                                            'nosso_numero'      => $invoice->getIdinvoice(),//id do boleto
                                            'numero_documento'  => $invoice->getIdinvoice(),//id do boleto
                                            'carteira'          => $wallet,//de acordo com o tipo de carteira q o cliente adiquira
                                            'codigo_carteira'   => $codeWallet,
                                            'registrado'        => $registeredInvoice,
                                            'aceite'            => $aceite,
                                            'especie'           => $specie, // Você pode consultar as especies Cnab\Especie
                                            'valor'             => $liquidValue, // Valor do boleto
                                            'instrucao1'        => 2, // 1 = Protestar com (Prazo) dias, 2 = Devolver após (Prazo) dias, futuramente poderemos ter uma constante
                                            'instrucao2'        => 0, // preenchido com zeros
                                            'sacado_nome'       => $nameCustomer, // O Sacado é o cliente, preste atenção nos campos abaixo
                                            'sacado_tipo'       => $typeDocument, //campo fixo, escreva 'cpf' (sim as letras cpf) se for pessoa fisica, cnpj se for pessoa juridica
                                            'sacado_cpf'        => $document,//'111.111.111-11'
                                            'sacado_logradouro' => $street,
                                            'sacado_bairro'     => $district,
                                            'sacado_cep'        => $zipCode, // sem hífem
                                            'sacado_cidade'     => $city,
                                            'sacado_uf'         => $state,
                                            'data_vencimento'   => $dateDue,
                                            'data_cadastro'     => $dateCreate,
                                            'juros_de_um_dia'     => $ValueInterest, // Valor do juros de 1 dia' --Criar em Pós Company - de acordo com a company
                                            'data_desconto'       => $limitDateDiscount, //Data limite do desconto - criar em invoice
                                            'valor_desconto'      => $valueDiscountOrder, // Valor do desconto - order
                                            'prazo'               => $deadline, // prazo de dias para o cliente pagar após o vencimento --Criar em Poscompany
                                            'taxa_de_permanencia' => '0', //00 = Acata Comissão por Dia (recomendável), 51 Acata Condições de Cadastramento na CAIXA
                                            'mensagem'            => $info, //invoice
                                            'data_multa'          => $datemulct, // data da multa - vencimento mais um dia
                                            'valor_multa'         => $mulct, // valor da multa - Criar em PosCompany
                                        ));    
                                    }     
                                }
                            }
                        }

                        $invoice->setStatusBank(1);
                        getEm()->persist($invoice);
                        getEm()->flush();
                    }
                }
                $name = time();                
                $arquivo->save('../data/shipping_and_return_bank_file/shipping_bank_file.txt');
                $zip = new ZipArchive();
                if( $zip->open( '../data/shipping_and_return_bank_file/shipping_bank_file.zip' , ZipArchive::CREATE )  === true){
                    $zip->deleteName('shipping_bank_file.txt');
                     
                    $zip->addFile(  '../data/shipping_and_return_bank_file/shipping_bank_file.txt' , 'shipping_bank_file.txt' );
                     
                    $zip->close();
                }
                $result = "success";
                $message = "query success";
                $mysqlData = "";
            }catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;

        }
    }

    /**
     * Import the file of return, system from Brazil, for bank's Itau, BANCO DO BRASIL, CEF, SANTANDER and BRADESCO. Note, only ITAU and BANCO DO BRASIL working in the moment. 
     * @return string json_data 
     */
    public function importReturnFile(){
        include "../libraries/Cnab/vendor/autoload.php";
        try{
            $erro = "";
            if(!empty($_FILES)){
                $file = $_FILES['file'];
                $name = $file['name'];
                $tmp = $file['tmp_name'];

                $extensao = explode('.', $name);
                $ext = end($extensao);

                $Invoice = getEm()->getRepository('Invoice')->findBy(array("active" => 1));
                $Orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
                $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
                if($ext == "ret" || $ext == "RET"){
                    if(move_uploaded_file($tmp, '../data/shipping_and_return_bank_file/return_bank_file.RET')){
                        $cnabFactory = new Cnab\Factory();
                        $arquivo = $cnabFactory->createRetorno('../data/shipping_and_return_bank_file/return_bank_file.RET');
                        $detalhes = $arquivo->listDetalhes();
                        foreach($detalhes as $detalhe) {
                            if($detalhe->getValorRecebido() > 0) {
                                $nossoNumero   = $detalhe->getNossoNumero();
                                $valorRecebido = $detalhe->getValorRecebido();
                                $dataPagamento = $detalhe->getDataOcorrencia();
                                $carteira      = $detalhe->getCarteira();
                                // você já tem as informações, pode dar baixa no boleto aqui
                                $valueGross = 0;
                                $discount = 0;
                                $liquidValue = 0;
                                if($Invoice != null){
                                    foreach ($Invoice as $invoice) {
                                        //verifica o id
                                        if($invoice->getIdinvoice() == $nossoNumero){                                           
                                            if($Orders != null){
                                                $invoice->setStatusBank(2);
                                                $liquidValue = 0;
                                                $discount = 0;
                                                $valueGross = 0;
                                                $verifyValue = 0;
                                                $verifyDatePayment = 0;
                                                foreach ($Orders as $order) {
                                                    if($invoice->getOrderorder()->getIdorder() == $order->getIdorder()){
                                                        foreach ($OrderLine as $orderLine) {
                                                            if($OrderLine != null){
                                                                if($order->getIdorder() == $orderLine->getOrderorder()->getIdorder()){
                                                                    $valueGross = $valueGross + $orderLine->getActualValue();
                                                                    $discount = $discount + $orderLine->getDiscount();
                                                                }
                                                            }
                                                        }

                                                        $discount = $discount + $order->getDiscount();
                                                        $liquidValue = $valueGross - $discount;
                                                        
                                                        if($valorRecebido == $liquidValue && !empty($dataPagamento)){
                                                            $invoice->setStatusPayment(1);
                                                            $invoice->setValuePayment($valorRecebido);
                                                        }else if($valorRecebido < $liquidValue && !empty($dataPagamento)){
                                                            $invoice->setStatusPayment(2);
                                                            $invoice->setValuePayment($valorRecebido);
                                                        }else if($valorRecebido == $liquidValue && empty($dataPagamento)){
                                                            $invoice->setStatusPayment(3);
                                                            $invoice->setValuePayment($valorRecebido);
                                                        }else if($valorRecebido < $liquidValue && empty($dataPagamento)){
                                                            $invoice->setStatusPayment(4);
                                                            $invoice->setValuePayment($valorRecebido);
                                                        }

                                                    }
                                                }

                                            }

                                            getEm()->persist($invoice);
                                            getEm()->flush();
                                        }     

                                    }
                                }

                            }

                        }
                     
                    }else{
                        die();
                    }
                }else{
                    die();
                }
            }else{
                die();
            }

            $result = "success";
            $message = "query success";
            $mysqlData = "";
        }catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;

    }

}

