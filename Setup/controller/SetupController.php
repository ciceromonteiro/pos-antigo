<?php

/**
 * Description of license
 * 
 * @abstract file created in 20/10/2016
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 */

class SetupController {
    
    function __construct() {
        
    }
    
    public function index($view=""){                        
        $redirect = "Location: ".URL_BASE."Setup/posCompany/index";
        
        header($redirect);
        
        return;
    }
    
    private function getTemplate($view) {
        ob_start();
        GenericController::answer("Setup", "Setup", $view, "");
        $body = ob_get_contents();
        ob_end_clean();
        return $body;
    }
    
    public function process($view="") {
        if (!empty($_GET['id'.$view])) {
            $this->update($_GET, $view);
        } else {
            $this->insert($_GET, $view);
        }
    }
    
    public function insert($data, $view) {
        
        try {
            $entityManager = getEm();
            $entity = ucfirst($view);
            $e = new $entity();
            
            $fields = $entityManager->getClassMetadata(ucfirst($view))->getFieldNames();
            
            $data = array_merge($data, array('dateCreate' => new \DateTime()));
            
            foreach($fields as $field) {
                $setElement = 'set'.ucwords($field);
                if (array_key_exists($field, $data)) {
                    if (!empty($data[$field])) {
                        $e->$setElement($data[$field]);
                    }
                }
            }
            $entityManager->persist($e);
            $entityManager->flush();

            $result  = 'success';
            $message = 'query success';
            
            $getId = 'getId'.$view;
            $data = array('id'.$view => $e->$getId());

        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function update($data, $view) {
        if(isset($data['id'.$view])){
            try {
                $dat = array();
                
                $entityManager = getEm();
                
                $arrayFilter = array(
                    'id'.$view => $data['id'.$view]
                );
                
                unset($data['id'.$view]);
                
                $entities = $entityManager->getRepository(ucfirst($view))->findBy($arrayFilter);
            
                $fields = $entityManager->getClassMetadata(ucfirst($view))->getFieldNames();
                
                $data = array_merge($data, array('dateUpdate' => new \DateTime()));
                
                foreach($entities as $entity) {
                    foreach($fields as $field) {
                        $setElement = 'set'.ucwords($field);
                        if (array_key_exists($field, $data)) {
                            if (!empty($data[$field])) {
                                $dat[$field] = $data[$field];
                                $entity->$setElement($data[$field]);
                            } else {
                                $dat[$field] = '';
                                $entity->$setElement(null);
                            }
                        } else {
                            if ($field == "barcodeReview") {
                                $dat[$field] = '';
                                $entity->$setElement(null);
                            }
                        }
                    }
                }
                
                $entityManager->persist($entity);
                $entityManager->flush();
                $result  = 'success';
                $message = 'query success';
                $data = $dat;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getall($view="") {
        try{
            $em = getEm();
            $custom = $em->getRepository(ucfirst($view));
            
            $data = array ();
            
            if (isset($custom->findAll()[0])) {
                $register = $custom->findAll()[0];
            
                $fields = $em->getClassMetadata(ucfirst($view))->getFieldNames();

                $notAccepted = array('dateCreate','dateUpdate','bestsellerFile','barcodeFile');

                foreach($fields as $val) {
                    if (!in_array($val, $notAccepted)) {
                        $getElement = 'get'.ucwords($val);
                        $data[$val] = $register->$getElement();
                    }
                }
            }
            
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;   
    }
    
}
