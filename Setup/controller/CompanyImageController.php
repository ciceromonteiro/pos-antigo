<?php

/**
 * Description of PosCompanyLogoController
 * 
 * @abstract file created in 25/01/2017
 * 
 * @author deyvison <deyvisonestevam@gmail.com>
 */
class CompanyImageController {
    private $entityManager;
    
    function __construct() {
        $this->entityManager = $this->getEntityManager();
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    function getEntityManager() {
        return getEm();
    }
    
    /**
     * 
     * @param PosCompany $posCompany
     * @return Pos
     */
    private function getPos($posCompany) {
        $criteria = array(            
            "active"    =>  TRUE,
            "posCompanyposCompany"  => $posCompany,
        );

        $p = $this->entityManager->getRepository("Pos")->findBy($criteria);
        
        return array_pop($p);        
    }
    
    /**
     * 
     * @param $idPosCompany
     * @return PosCompany
     */
    private function getPosCompany($idPosCompany){
        
        $posCompanyRepository = $this->entityManager->getRepository("PosCompany");
        
        return $posCompanyRepository->find($idPosCompany);
    }
    
    /**
     * 
     * @param Pos $pos
     * @return Company
     */
    private function getCompany($pos) {
        $criteria = array(            
            "active"    =>  TRUE,
            "pospos"  => $pos,
        );

        $c = $this->entityManager->getRepository("Company")->findBy($criteria);
        
        return array_pop($c);   
    }
    
    /**
     * 
     * @param PosCompany $posCompany
     * @param array() companyImage
     * @param string $reference_name
     */
    public function updatePosCompanyLogo($posCompany, $companyImage, $sequence=1) {                
        
        $pos = $this->getPos($posCompany);
        $company = $this->getCompany($pos);
        
        $img = $companyImage['tmp_name'];
        $len = $companyImage['size'];
        $type = $companyImage['type'];
        $name = $companyImage['name'];
        
        if ( $img != "none" )
        {
            $fp = fopen($img, "rb");
            $content = fread($fp, $len);
            fclose($fp);
        }
        
        $criteria = array(            
            "active"    =>  TRUE,
            "companycompany"  => $company,
        );

        $companyImg = $this->entityManager->getRepository("CompanyImage")->findBy($criteria);

        if(count($companyImg) > 0){

            /*@var $companyImg CompanyImage*/
            $companyImg = array_pop($companyImg);
            $companyImg->setCompanycompany($company);
            $companyImg->setImage($content);
            $companyImg->setType($type);
            $companyImg->setSequence($sequence);
            $companyImg->setDateUpdate(new DateTime());
            
        }else{
            
            $companyImg = new CompanyImage();
            $companyImg->setCompanycompany($company);
            $companyImg->setImage($content);   
            $companyImg->setType($type);
            $companyImg->setSequence($sequence);
            $companyImg->setDateCreate(new DateTime());            
            $companyImg->setActive(TRUE);

            $this->entityManager->persist($companyImg);                            
        }
        
        $this->entityManager->flush();        
    }

    /**
     * Chack error on upload
     * 
     * @param int $error
     * @return boolean
     * @throws Exception
     */
    private function checkErrorUpload($error){
        switch ($error) {
            case 0:
                return true; // There is no error, the file uploaded with success.           
            case 1: //UPLOAD_ERR_INI_SIZE
                throw new Exception("The uploaded file exceeds the upload_max_filesize directive in php.ini.");                
            case 2: //UPLOAD_ERR_FORM_SIZE
                throw new Exception("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.");
            case 3: //UPLOAD_ERR_PARTIAL
                throw new Exception("The uploaded file was only partially uploaded.");
            case 4: //UPLOAD_ERR_NO_FILE -- No file was uploaded.
                return false;
            case 6: //UPLOAD_ERR_NO_TMP_DIR
                throw new Exception("Missing a temporary folder. Introduced in PHP 5.0.3.");
            case 7: //UPLOAD_ERR_CANT_WRITE
                throw new Exception("Failed to write file to disk. Introduced in PHP 5.1.0.");
            case 8: //UPLOAD_ERR_EXTENSION
                throw new Exception("A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help. Introduced in PHP 5.2.0.");
            default:
                return true;
        }
    }
    
    /**
     * Validate
     * @param array() $img
     * @return array()
     */
    public function validityImage($img) {
        if($this->checkErrorUpload($img["error"])){
            return $img;
        }else{
            return false;
        }
    }
    
}