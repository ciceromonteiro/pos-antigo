<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AccountingController {
    public function index(){
        $navbar = "Setup|configuration";
        $software_accounting = getEm()->getRepository('SoftwareAccounting')->findAll();
        $credit_management = getEm()->getRepository('CreditManagement')->findAll();
        $customer = getEm()->getRepository('Person')->findAll();
        $accounting = getEm()->getRepository('SettingsAccounting')->findAll();
        $array_answer = array (    
            "menu-active"   =>  "accounting",
            "software_accounting" => $software_accounting,
            "credit_management" => $credit_management,
            "customers" => $customer,
            "data" => $accounting,
        );
        GenericController::template("Setup", "accounting","index", $navbar, $array_answer, 6);
    }
    
    public function update(){
        
        $arrayValues = array (
            'info_cash_acc'                             => isset($_POST['info_cash_acc']) ? $_POST['info_cash_acc'] : '',
            'info_cash_withdrawl_acc'                   => isset($_POST['info_cash_withdrawl_acc']) ? $_POST['info_cash_withdrawl_acc'] : '',
            'info_rounding_acc'                         => isset($_POST['info_rounding_acc']) ? $_POST['info_rounding_acc'] : '',
            'info_credit_note_acc'                      => isset($_POST['info_credit_note_acc']) ? $_POST['info_credit_note_acc'] : '',
            'info_gift_voucher_acc'                     => isset($_POST['info_gift_voucher_acc']) ? $_POST['info_gift_voucher_acc'] : '',
            'info_overages'                             => isset($_POST['info_overages']) ? $_POST['info_overages'] : '',
            'info_credit_card_acc'                      => isset($_POST['info_credit_card_acc']) ? $_POST['info_credit_card_acc'] : '',
            'info_payment_automatic_account'            => isset($_POST['info_payment_automatic_account']) ? $_POST['info_payment_automatic_account'] : '',
            'info_hotel_booking_account'                => isset($_POST['info_hotel_booking_account']) ? $_POST['info_hotel_booking_account'] : '',
            'info_free_acc'                             => isset($_POST['info_free_acc']) ? $_POST['info_free_acc'] : '',
            'checkout_default_customer'                 => isset($_POST['checkout_default_customer']) ? $_POST['checkout_default_customer'] : '',
            'checkout_starting_petty_cash'              => isset($_POST['checkout_starting_petty_cash']) ? $_POST['checkout_starting_petty_cash'] : '',
            'checkout_current_cash_on_hand'             => isset($_POST['checkout_current_cash_on_hand']) ? $_POST['checkout_current_cash_on_hand'] : '',
            'checkout_divergence'                       => isset($_POST['checkout_divergence']) ? $_POST['checkout_divergence'] : '',
            'checkout_gift_voucher'                     => isset($_POST['checkout_gift_voucher']) ? $_POST['checkout_gift_voucher'] : '',
            'credit_manag_default_report_software_id'   => isset($_POST['credit_manag_default_report_software_id']) ? $_POST['credit_manag_default_report_software_id'] : 0,
            'credit_manag_email_of_contact'             => isset($_POST['credit_manag_email_of_contact']) ? $_POST['credit_manag_email_of_contact'] : '',
            'credit_manag_id_customer'                  => isset($_POST['credit_manag_id_customer']) ? $_POST['credit_manag_id_customer'] : '',
            'credit_manag_id_invoice'                   => isset($_POST['credit_manag_id_invoice']) ? $_POST['credit_manag_id_invoice'] : '',
            'credit_manag_id_credit_note'               => isset($_POST['credit_manag_id_credit_note']) ? $_POST['credit_manag_id_credit_note'] : '',
            'init_param_number_of_first_inv'            => isset($_POST['init_param_number_of_first_inv']) ? $_POST['init_param_number_of_first_inv'] : '',
            'init_param_number_of_first_log_of_cash'    => isset($_POST['init_param_number_of_first_log_of_cash']) ? $_POST['init_param_number_of_first_log_of_cash'] : '',
            'init_param_number_of_first_report_acc'     => isset($_POST['init_param_number_of_first_report_acc']) ? $_POST['init_param_number_of_first_report_acc'] : '',
            'init_param_number_of_first_report_inv'     => isset($_POST['init_param_number_of_first_report_inv']) ? $_POST['init_param_number_of_first_report_inv'] : '',
            'init_param_number_of_first_report_sales'   => isset($_POST['init_param_number_of_first_report_sales']) ? $_POST['init_param_number_of_first_report_sales'] : '',
            'credit_management_id'                      => isset($_POST['credit_management_id']) ? $_POST['credit_management_id'] : 0,
            'credit_management_email_or_idaccount'      => isset($_POST['credit_management_email_or_idaccount']) ? $_POST['credit_management_email_or_idaccount'] : '',
            'credit_management_user'                    => isset($_POST['credit_management_user']) ? $_POST['credit_management_user'] : '',
            'credit_management_password'                => isset($_POST['credit_management_password']) ? $_POST['credit_management_password'] : ''
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
                
            for($i = 0; $i < count($arrayKeys); $i++){
                $key = $arrayKeys[$i];                
                $accounting_settings = getEm()->getRepository('SettingsAccounting')->findBy(array('dataType' => $key));
                if($accounting_settings){
                    $accounting_settings[0]->setDataValue($arrayValues[$key]);
                    $accounting_settings[0]->setDateUpdate(new DateTime());
                    $accounting_settings[0]->setActive(1);
                    getEm()->persist($accounting_settings[0]);
                    getEm()->flush();
                } else {
                    $accounting_settings = new SettingsAccounting();
                    $accounting_settings->setDataType($key);
                    $accounting_settings->setDataValue($arrayValues[$key]);
                    $accounting_settings->setDateCreate(new DateTime());
                    $accounting_settings->setActive(1);
                    getEm()->persist($accounting_settings);
                    getEm()->flush();
                }
            }
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getAccounting(){
        $arrayValues = array ();
        $dat = array ();
        try {
            $accountingSettings = getEm()->getRepository('SettingsAccounting')->findAll();
            if($accountingSettings){
                foreach ($accountingSettings as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        
        $json_data = json_encode($data);
        print $json_data;        
    }
    
    public static function getAllCreditCards(){
        try{
            $entityManager = getEm();
            $arrayFilter = array(
                "active" => 1
            );
            $creditCard = $entityManager->getRepository('CreditCard')->findBy($arrayFilter);

            $data = array ();
            $showBalancing = "";
            $sendRegister = "";
            foreach ($creditCard as $value){
                if($value->getShowBalancing() == 1){
                    $showBalancing = "<span class='glyphicon glyphicon-ok'></span>";
                }
                if($value->getShowBalancing() == 0){
                    $showBalancing = "<span class='glyphicon glyphicon-remove'></span>";
                }
                if($value->getSendRegister() == 1){
                    $sendRegister = "<span class='glyphicon glyphicon-ok'></span>";
                }
                if($value->getSendRegister() == 0){
                    $sendRegister = "<span class='glyphicon glyphicon-remove'></span>";
                }
                
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdcreditcard().'" data-name="'.$value->getName().'">',
                    "id" => $value->getIdcreditcard(),
                    "bbsid" => $value->getBbsid(),
                    "name" => $value->getName(),
                    "accounting" => $value->getAccounting(),
                    "showBalancing" => $showBalancing,
                    "sendRegister" => $sendRegister
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function getCreditCard(){
        if(isset($_GET['id'])){
            try {
                $entityManager = getEm();
                $arrayFilter = array("idcreditcard" => $_GET['id']);
                $creditCard = $entityManager->getRepository('CreditCard')->findBy($arrayFilter);
                $mysqlData = array ();
                foreach ($creditCard as $value){
                    $dat = array (
                        "id" => $value->getIdcreditcard(),
                        "bbsid" => $value->getBbsid(),
                        "name" => $value->getName(),
                        "description" => $value->getDescription(),
                        "accounting" => $value->getAccounting(),
                        "showBalancing" => $value->getShowBalancing(),
                        "sendRegister" => $value->getSendRegister()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function updateCreditCard(){
        $showBalancing = "";
        $sendRegister = "";
        if(isset($_GET['id'])){
            try {
                
                $entityManager = getEm();
                $arrayFilter = array(
                    "idcreditcard" => $_GET['id']
                );

                $creditCard = $entityManager->getRepository('CreditCard')->findBy($arrayFilter);
                $date = new datetime();
                $creditCard[0]->setName($_GET['name']);
                $creditCard[0]->setDescription($_GET['description']);
                $creditCard[0]->setBbsid($_GET['BBSID']);
                $creditCard[0]->setAccounting($_GET['accounting']);
                
                if(isset($_GET['showBalancing'])){
                    if($_GET['showBalancing'] == "on"){
                        $showBalancing = 1;
                    }
                }
                if(isset($_GET['sendRegister'])){
                    if($_GET['sendRegister'] == "on"){
                        $sendRegister = 1;
                    }
                }
                $creditCard[0]->setShowBalancing($showBalancing);
                $creditCard[0]->setSendRegister($sendRegister);
                $creditCard[0]->setDateUpdate($date);
                $creditCard[0]->setActive(true);
                $entityManager->persist($creditCard[0]);
                $entityManager->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function deleteCreditCard(){
        if($_GET['idCreditCards']){
            try{
                $ids = $_GET['idCreditCards'];
                for($i=0; $i < count($ids); $i++){
                    $creditCard = getEm()->getRepository("CreditCard")->findBy(array("idcreditcard" => $ids[$i]));
                    $creditCard[0]->setActive('3');
                    $creditCard[0]->setDateDelete(new DateTime());
                    getEm()->persist($creditCard[0]);
                    getEm()->flush();
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public static function insertCreditCard(){
        $showBalancing = "";
        $sendRegister = "";
        if(isset($_GET['name'])){
            try {
                $entityManager = getEm();
                $creditcard = new CreditCard();
                $creditcard->setBbsid($_GET['BBSID']);
                $creditcard->setName($_GET['name']);
                $creditcard->setDescription($_GET['description']);
                $creditcard->setAccounting($_GET['accounting']);
                if(isset($_GET['showBalancing'])){
                    if($_GET['showBalancing'] == "on"){
                        $showBalancing = 1;
                    }
                }
                if(isset($_GET['sendRegister'])){
                    if($_GET['sendRegister'] == "on"){
                        $sendRegister = 1;
                    }
                }
                $creditcard->setShowBalancing($showBalancing);
                $creditcard->setSendRegister($sendRegister);
                $creditcard->setActive(true);
                $entityManager->persist($creditcard);
                $entityManager->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
}
