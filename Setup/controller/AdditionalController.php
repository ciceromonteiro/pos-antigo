<?php

/**
 * Description of AdditionalController
 * 
 * @abstract file created in 02/02/2017
 * 
 * @author Renato Rocha <engirocha@gmail.com>
 */

class AdditionalController {

    public function index() {
        $navbar = "Setup|configuration";
        $companyFactoring = getEm()->getRepository('CompanyFactoring')->findAll();
        $creditAdministrator = getEm()->getRepository('SettingsFactoring')->findAll();
        $array_answer = array(
            "menu-active" => "additional",
            "company_refactoring" => $companyFactoring,
            "credit_administrator" => $creditAdministrator
        );
        GenericController::template("Setup", "additional", "index", $navbar, $array_answer, 11);
    }
    
    public function getAdditionalSettings(){
        $arrayValues = array ();
        $dat = array ();
        try {
            $additional = getEm()->getRepository('SettingsAdditional')->findAll();
            if($additional){
                foreach ($additional as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateAdditionalSettings(){
        $arrayValues = array (
            'showPosSessionIr'                      => isset($_POST['showPosSessionIr']) ? $_POST['showPosSessionIr'] : '',
            'showPlaceSale'                         => isset($_POST['showPlaceSale']) ? $_POST['showPlaceSale'] : '',
            'showSessionPOSacc'                     => isset($_POST['showSessionPOSacc']) ? $_POST['showSessionPOSacc'] : '',
            'showPlaceSale'                         => isset($_POST['showPlaceSale']) ? $_POST['showPlaceSale'] : '',
            'UseStandardEmailSoftware'              => isset($_POST['UseStandardEmailSoftware']) ? $_POST['UseStandardEmailSoftware'] : '',
            'language'                              => isset($_POST['language']) ? $_POST['language'] : '',
            'initialPage'                           => isset($_POST['initialPage']) ? $_POST['initialPage'] : '',
            'sendersEmail'                          => isset($_POST['sendersEmail']) ? $_POST['sendersEmail'] : '',
            'alertEmail'                            => isset($_POST['alertEmail']) ? $_POST['alertEmail'] : '',
            'password_to_access_admin'              => isset($_POST['password_to_access_admin']) ? $_POST['password_to_access_admin'] : '',
            'password_to_access_employee'           => isset($_POST['password_to_access_employee']) ? $_POST['password_to_access_employee'] : '',
            'password_to_access_cashRegisterClos'   => isset($_POST['password_to_access_cashRegisterClos']) ? $_POST['password_to_access_cashRegisterClos'] : '',
            'password_to_access_product'            => isset($_POST['password_to_access_product']) ? $_POST['password_to_access_product'] : '',
            'password_to_access_cancelItem'         => isset($_POST['password_to_access_cancelItem']) ? $_POST['password_to_access_cancelItem'] : '',
            'password_to_access_cancelPurchase'     => isset($_POST['password_to_access_cancelPurchase']) ? $_POST['password_to_access_cancelPurchase'] : '',
            'password_to_access_showDataSale'       => isset($_POST['password_to_access_showDataSale']) ? $_POST['password_to_access_showDataSale'] : '',
            'ftp_of_provides_host'                  => isset($_POST['ftp_of_provides_host']) ? $_POST['ftp_of_provides_host'] : '',
            'ftp_of_provides_user'                  => isset($_POST['ftp_of_provides_user']) ? $_POST['ftp_of_provides_user'] : '',
            'ftp_of_provides_password'              => isset($_POST['ftp_of_provides_password']) ? $_POST['ftp_of_provides_password'] : '',
            'ftp_of_provides_layout'                => isset($_POST['ftp_of_provides_layout']) ? $_POST['ftp_of_provides_layout'] : '',
            'ftp_of_provides_includeImg'            => isset($_POST['ftp_of_provides_includeImg']) ? $_POST['ftp_of_provides_includeImg'] : ''
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            for($i = 0; $i < count($arrayKeys); $i++){
                $key = $arrayKeys[$i];
                $additional = getEm()->getRepository('SettingsAdditional')->findBy(array('dataType' => $key));
                if($additional){
                    $additional[0]->setDataValue($arrayValues[$key]);
                    $additional[0]->setDateUpdate(new DateTime());
                    $additional[0]->setActive(1);
                    getEm()->persist($additional[0]);
                    getEm()->flush();
                } else {
                    $additional = new SettingsAdditional();
                    $additional->setDataType($key);
                    $additional->setDataValue($arrayValues[$key]);
                    $additional->setDateCreate(new DateTime());
                    $additional->setActive(1);
                    getEm()->persist($additional);
                    getEm()->flush();
                }
            }
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getCompanyFactoring(){
        $companyFactoring = getEm()->getRepository('CompanyFactoring')->findBy(array ("active" => '1'));
        $mysqlData = array ();
        if(isset($companyFactoring)){
            try {
                foreach ($companyFactoring as $value){
                    $dat = array (
                        "description" => $value->getName(),
                        "id" => $value->getIdcolor()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        } else {
            
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateCreditAdministrator(){
        if($_POST['companyfactoring'] != '' || $_POST['name'] != '' || $_POST['clientId'] != '' || 
            $_POST['address'] != '' || $_POST['codPostal'] != '' || $_POST['bankAccount'] != '' || 
            $_POST['sendEmail'] != '' || $_POST['definitionKid'] != '' || $_POST['invoiceText'] != ''){
                try {
                    $begin = getEm()->getConnection()->beginTransaction();
                    $creditAdministrator = getEm()->getRepository('SettingsFactoring')->findAll();
                    if($creditAdministrator){
                        if($_POST['companyfactoring'] == 1){
                            $creditAdministrator = getEm()->getRepository('SettingsFactoring')->findAll();
                            $creditAdministrator[0]->setActive(3);
                            getEm()->persist($creditAdministrator[0]);
                            getEm()->flush();
                        } else if($_POST['companyfactoring'] != 1){
                            $companyFactoring = getEm()->getRepository('CompanyFactoring')->findBy(array('idcompanyfactoring' => $_POST['companyfactoring'], 'active' => 1));
                            $creditAdministrator[0]->setCompanyFactoring($companyFactoring[0]);
                            $creditAdministrator[0]->setName($_POST['name']);
                            $creditAdministrator[0]->setAddress($_POST['address']);
                            $creditAdministrator[0]->setCodPostal($_POST['codPostal']);
                            $creditAdministrator[0]->setInvoiceText($_POST['invoiceText']);
                            $creditAdministrator[0]->setBankAccount($_POST['bankAccount']);
                            $creditAdministrator[0]->setKidLayout($_POST['definitionKid']);
                            $creditAdministrator[0]->setClientId($_POST['clientId']);
                            $creditAdministrator[0]->setEmail($_POST['sendEmail']);
                            $creditAdministrator[0]->setAddInvoiceText(isset($_POST['includeTextDefault']) ? $_POST['includeTextDefault'] : '');
                            $creditAdministrator[0]->setDateUpdate(new DateTime());
                            $creditAdministrator[0]->setActive(1);
                            getEm()->persist($creditAdministrator[0]);
                            getEm()->flush();
                        }
                    } else {
                        $creditAdministrator = new SettingsFactoring();
                        $companyFactoring = getEm()->getRepository('CompanyFactoring')->findBy(array('idcompanyfactoring' => $_POST['companyfactoring'], 'active' => 1));
                        $creditAdministrator->setCompanyFactoring($companyFactoring[0]);
                        $creditAdministrator->setName($_POST['name']);
                        $creditAdministrator->setAddress($_POST['address']);
                        $creditAdministrator->setCodPostal($_POST['codPostal']);
                        $creditAdministrator->setInvoiceText($_POST['invoiceText']);
                        $creditAdministrator->setBankAccount($_POST['bankAccount']);
                        $creditAdministrator->setKidLayout($_POST['definitionKid']);
                        $creditAdministrator->setClientId($_POST['clientId']);
                        $creditAdministrator->setEmail($_POST['sendEmail']);
                        $creditAdministrator->setAddInvoiceText(isset($_POST['includeTextDefault']) ? $_POST['includeTextDefault'] : '');
                        $creditAdministrator->setDateCreate(new DateTime());
                        $creditAdministrator->setActive(1);
                        getEm()->persist($creditAdministrator);
                        getEm()->flush();
                    }
                    $commit = getEm()->getConnection()->commit();
                    $result  = 'success';
                    $message = 'query success';
                } catch (\Exception $e) {
                    $rollback = getEm()->getConnection()->rollback();
                    $result  = 'error';
                    $message = $e->getMessage();
                }
            } else {
                $result  = 'error';
                $message = 'query error';
            }

        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getCreditAdministrator(){
        $values = array ();
        $data = array ();
        try {
            $creditAdministrator = getEm()->getRepository('SettingsFactoring')->findAll();
            if($creditAdministrator){
                foreach ($creditAdministrator as $value) {
                    $dat = array (
                        "name" => $value->getName(),
                        "address" => $value->getAddress(),
                        "codPostal" => $value->getCodPostal(),
                        "invoiceText" => $value->getInvoiceText(),
                        "bankAccount" => $value->getBankAccount(),
                        "definitionKid" => $value->getKidLayout(),
                        "clientId" => $value->getClientId(),
                        "sendEmail" => $value->getEmail(),
                        "includeTextDefault" => $value->getAddInvoiceText(),
                        "companyfactoring" => $value->getCompanyFactoring()->getIdcompanyfactoring()
                    );
                    array_push($data, $dat);
                }
            }
            $result  = 'success';
            $message = 'query success';
            $values = $data;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $dataResult = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        
        $json_data = json_encode($dataResult);
        print $json_data;
    }
    
    
    # chama script que faz downloads dos dados do servidor ftp cadastrado
    public function update_webservices_return(){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://localhost/app/data/webservices_return/update_data.php");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
    }
    

    public function downloadProductsData(){
        $products = getEm()->getRepository('Product')->findAll();
        $i = 0;
        try { 
        foreach ($products as $product) {
           
            $obj[$i] = array(
                'element'.$i => array(
                    "idProduct"  => $product->getIdproduct(),
                    "idManufacture" => $product->getManufacturermanufacturer() != null ? $product->getManufacturermanufacturer()->getIdmanufacturer() : '',
                    "idCurrency"    => $product->getCurrencycurrency() != null ? $product->getCurrencycurrency()->getIdcurrency() : '',
                    "idUnit" => $product->getUnitunit() != null ? $product->getUnitunit()->getIdunit() : '',
                    "idCollection" => $product->getProductCollectioncollection() != null ? $product->getProductCollectioncollection()->getIdcollection() : '',
                    "idSupplier" => $product->getSuppliersupplier() != null ? $product->getSuppliersupplier()->getIdsupplier() != null : '',
                    "idProductDepartament" => $product->getProductDepartmentproductDepartment() != null ? $product->getProductDepartmentproductDepartment()->getIddepartment() : '',
                    "idWarranty" => $product->getWarrantywarranty() != null ? $product->getWarrantywarranty()->getIdwarranty() : '',
                    "numberPack" => $product->getNumberpack() != null ? $product->getNumberpack() : 0,
                    "taxGp" => $product->getTaxGp()  != null ? $product->getTaxGp()->getIdtribute() : '',
                    "printForPickIst" => $product->getPrintForPickIst() != null ? $product->getPrintForPickIst()->getIdprinter() : '',
                    "ValueSellBrute" => $product->getValueForSellBrute() != null ? $product->getValueForSellBrute() :'',
                    "value_for_sell_liquid" => $product->getValueForSellLiquid() != null ? $product->getValueForSellLiquid() :'',
                    "netWeight" => $product->getNetWeight() != null ? $product->getNetWeight() :'',
                    "additionalWeight" => $product->getAdditionalWeight() != null ? $product->getAdditionalWeight() :'',
                    "volume" => $product->getVolume() != null ? $product->getVolume() :'',
                    "length" => $product->getLength() != null ? $product->getLength() :'',
                    "width" => $product->getWidth() != null ? $product->getWidth() :'',
                    "height" => $product->getHeight() != null ? $product->getHeight() :'',
                    "minimum_purchase_of_packages" => $product->getMinimumPurchaseOfPackages() != null ? $product->getMinimumPurchaseOfPackages() :'',
                    "minimum_purchase" => $product->getMinimumPurchase() != null ? $product->getMinimumPurchase() :'',
                    "name" =>$product->getName() != null ? $product->getName() :'',
                    "product_number" => $product->getProductNumber() != null ? $product->getProductNumber():'',
                    "product_number_supplier" => $product->getProductNumberSupplier() != null ? $product->getProductNumberSupplier():'',
                    "product_number_manufacturer" => $product->getProductNumberManufacturer() != null ? $product->getProductNumberManufacturer():'',
                    "price" => $product->getPrice() != null ? $product->getPrice():'',
                    "price_purchase" => $product->getPricePurchase() != null ? $product->getPricePurchase():'',
                    "contribution" => $product->getContribution() != null ? $product->getContribution():'',
                    "promotion_weekdays" => $product->getPromotionWeekdays() != null ? $product->getPromotionWeekdays() :'',
                    "price_recommended" => $product->getPriceRecommended() != null ? $product->getPriceRecommended():'',
                    "price_takeaway" => $product->getPriceTakeaway() != null ? $product->getPriceTakeaway():'',
                    "price_purchase_list" => $product->getPricePurchaseList() != null ? $product->getPricePurchaseList():'',
                    "price_limit_bargain" => $product->getPriceLimitBargain() != null ? $product->getPriceLimitBargain():'',
                    "price_large_scale" => $product->getPriceLargeScale() != null ? $product->getPriceLargeScale():'',
                    "profit_gross_profit_value" => $product->getProfitGrossProfitValue() != null ? $product->getProfitGrossProfitValue():'',
                    "barcode" => $product->getBarcode() != null ? $product->getBarcode():'',
                    "stock_enabled" => $product->getStockEnabled() != null ? $product->getStockEnabled():'',
                    "min_stock" => $product->getMinStock() != null ? $product->getMinStock():'',
                    "max_stock" => $product->getMaxStock() != null ? $product->getMaxStock():'',
                    "description" => $product->getDescription() != null ? $product->getDescription():'',
                    "short_description" => $product->getShortDescription() != null ? $product->getShortDescription():'',
                    "has_serial_number" => $product->getHasSerialNumber() != null ? $product->getHasSerialNumber():'',
                    "factor_purchase" => $product->getFactorPurchase() != null ? $product->getFactorPurchase():'',
                    "factor_sell" => $product->getFactorSell() != null ? $product->getFactorSell() :'',
                    "has_components" => $product->getHasComponents() != null ? $product->getHasComponents() :'',
                    "components_show_order_subitens" => $product->getComponentsShowOrderSubitens() != null ? $product->getComponentsShowOrderSubitens() :'',
                    "has_size_and_color" => $product->getHasSizeAndColor() != null ? $product->getHasSizeAndColor() :'',
                    "selling_product" => $product->getSellingProduct() != null ? $product->getSellingProduct() :'',
                    "date_create" => $product->getDateCreate() != null ? $product->getDateCreate():'',
                    "date_update" => $product->getDateUpdate() != null ? $product->getDateUpdate():'',
                    "date_delete" => $product->getDateDelete() != null ? $product->getDateUpdate(): '',
                    "active" => $product->getActive()!= null ? $product->getActive() :'',
                    "section_idsection" => $product->getSectionsection() != null ? $product->getSectionsection()->getIdsection():'',

                    "create_by" => $product->getCreateBy()!= null ? $product->getCreateBy()->getIdusers():'',

                    "output_control" => $product->getOutputControl()!= null ? $product->getOutputControl():'',
                    "type_product" => $product->getTypeProduct()!= null ? $product->getTypeProduct():'',
                    "prediction_of_purchase" => $product->getPredictionOfPurchase()!= null ? $product->getPredictionOfPurchase():'',
                    "Importing_rate" => $product->getImportingRate()!= null ? $product->getImportingRate():'',
                    "freight_tax_amount" => $product->getFreightTaxAmount()!= null ? $product->getFreightTaxAmount():'',
                    "value_of_shipping" => $product->getValueOfShipping()!= null ? $product->getValueOfShipping():'',
                    "number_of_serie_from_product" => $product->getNumberOfSerieFromProduct()!= null ? $product->getNumberOfSerieFromProduct():'',
                    "base_of_calc" => $product->getBaseOfCalc()!= null ? $product->getBaseOfCalc() :'',
                    "multiple_units" => $product->getMultipleUnits()!= null ? $product->getMultipleUnits():'',
                    "project" => $product->getProject() != null ?  $product->getProject()->getIdproject() : ''

                    )
                );
                $i++;
            }

             $result = 'success';
            $message = 'query success';
            $data = '';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $content = json_encode($obj);
        $file_name = "productData.json";

        if(file_exists($file_name)) {

        
        $file = @fopen('../data/temp/'.$file_name, 'w+');
        fwrite($file, $content);
        fclose($file);
        }else{
        $file = @fopen('../data/temp/'.$file_name, 'w+');
        fwrite($file, $content);
        fclose($file);
        }



        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }

    public function downloadExec(){
        try{
        $zip = new ZipArchive();
 
        if( $zip->open( '../data/temp/product.zip' , ZipArchive::CREATE )  === true){
            $zip->deleteName('productData.json');
             
            $zip->addFile(  '../data/temp/productData.json' , 'productData.json' );
             
            $zip->close();
        }

            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;

    
    }  

    public function upload(){
        $file = $_FILES['file'];
        $name = $file['name'];
        $tmp = $file['tmp_name'];

        $extensao = explode('.', $name);
        $ext = end($extensao);
        
    try{
        if(empty($file)){
            $data = "Selecione um arquivo";
            die();
        }else{
            if($ext == 'json'){
                if(move_uploaded_file($tmp, '../data/temp/uploadProducts.json')){

                    $jsonData = file_get_contents('../data/temp/uploadProducts.json');
                    $arrData = json_decode($jsonData);

                    foreach ($arrData as $dataElement) {
                        foreach ($dataElement as $dataInternal) {
                            $dataInternalArray = json_decode(json_encode($dataInternal), True);
                           

                            $id = $dataInternalArray['idProduct'];
                            $product = getEm()->getRepository('Product')->find(array('idproduct' => $id));

                             if($product == null){

                                $product = new Product();
                                
                            }

                            if($dataInternalArray['date_create'] != null){
                                $dataCreate = new DateTime($dataInternalArray['date_create']['date']);
                            }else{
                                $dataCreate = new DateTime();   
                            }

                            if($dataInternalArray['date_update'] != null || $dataInternalArray['date_update'] != ""){
                                $dataUpdate = new DateTime($dataInternalArray['date_update']['date']);
                                $product->setDateUpdate($dataUpdate);
                            }else{
                                $dataUpdate = null;
                            }
                             
                            if($dataInternalArray['date_delete'] != null || $dataInternalArray['date_delete'] != ""){
                                $dataDelete = new DateTime($dataInternalArray['date_delete']['date']);
                                $product->setDateDelete($dataDelete);

                            }else{
                                $dataDelete = null;
                            }
                            
                            $product->setCurrencyPurchasePrice($dataInternalArray['price_purchase']);
                            $product->setValueForSellBrute($dataInternalArray['ValueSellBrute']);
                            $product->setValueForSellLiquid($dataInternalArray['value_for_sell_liquid']);
                            $product->setNetWeight($dataInternalArray['netWeight']);
                            $product->setAdditionalWeight($dataInternalArray['additionalWeight']);
                            $product->setVolume($dataInternalArray['volume']);
                            $product->setLength($dataInternalArray['length']);
                            $product->setWidth($dataInternalArray['width']);
                            $product->setHeight($dataInternalArray['height']);
                            $product->setMinimumPurchaseOfPackages($dataInternalArray['minimum_purchase_of_packages']);
                            $product->setMinimumPurchase($dataInternalArray['minimum_purchase']);
                            $product->setNumberpack($dataInternalArray['numberPack']);
                            $product->setName($dataInternalArray['name']);
                            $product->setProductNumber($dataInternalArray['product_number']);
                            $product->setProductNumberSupplier($dataInternalArray['product_number_supplier']);
                            $product->setProductNumberManufacturer($dataInternalArray['product_number_manufacturer']);
                            $product->setPrice($dataInternalArray['price']);
                            $product->setPricePurchase($dataInternalArray['price_purchase']);
                            $product->setContribution($dataInternalArray['contribution']);
                            $product->setPromotionWeekdays($dataInternalArray['promotion_weekdays']);
                            $product->setPriceRecommended($dataInternalArray['price_recommended']);
                            $product->setPriceTakeaway($dataInternalArray['price_takeaway']);
                            $product->setPricePurchaseList($dataInternalArray['price_purchase_list']);
                            $product->setPriceLimitBargain($dataInternalArray['price_limit_bargain']);
                            $product->setPriceLargeScale($dataInternalArray['price_large_scale']);
                            $product->setProfitGrossProfitValue($dataInternalArray['profit_gross_profit_value']);
                            $product->setBarcode($dataInternalArray['barcode']);
                            $product->setStockEnabled($dataInternalArray['stock_enabled']);
                            $product->setMinStock($dataInternalArray['min_stock']);
                            $product->setMaxStock($dataInternalArray['max_stock']);
                            $product->setDescription($dataInternalArray['description']);
                            $product->setShortDescription($dataInternalArray['short_description']);
                            $product->setHasSerialNumber($dataInternalArray['has_serial_number']);
                            $product->setFactorPurchase($dataInternalArray['factor_purchase']);
                            $product->setFactorSell($dataInternalArray['factor_sell']);
                            $product->setHasComponents($dataInternalArray['has_components']);
                            $product->setComponentsShowOrderSubitens($dataInternalArray['components_show_order_subitens']);
                            $product->setHasSizeAndColor($dataInternalArray['has_size_and_color']);
                            $product->setSellingProduct($dataInternalArray['selling_product']);
                            $product->setDateCreate($dataCreate);
                            $product->setActive($dataInternalArray['active']);
                            $product->setOutputControl($dataInternalArray['output_control']);
                            $product->setTypeProduct($dataInternalArray['type_product']);
                            $product->setPredictionOfPurchase($dataInternalArray['prediction_of_purchase']);
                            $product->setImportingRate($dataInternalArray['Importing_rate']);
                            $product->setFreightTaxAmount($dataInternalArray['freight_tax_amount']);
                            $product->setValueOfShipping($dataInternalArray['value_of_shipping']);
                            $product->setNumberOfSerieFromProduct($dataInternalArray['number_of_serie_from_product']);
                            $product->setBaseOfCalc($dataInternalArray['base_of_calc']);
                            $product->setMultipleUnits($dataInternalArray['multiple_units']);
                            

                            

                            if($dataInternalArray['idCurrency'] != null){
                                $currency = getEm()->getRepository('Currency')->find(array('idcurrency' => $dataInternalArray['idCurrency']));
                                $product->setCurrencycurrency($currency);
                            }

                            if($dataInternalArray['idManufacture'] != null){

                                $manufacturer = getEm()->getRepository('Manufacturer')->find(array('idproduct' => $dataInternalArray['idManufacture']));
                                $product->setManufacturermanufacturer($manufacturer);
                            }

                            if($dataInternalArray['idCollection'] != null){

                                $collection = getEm()->getRepository('ProductCollection')->find(array('idcollection' => $dataInternalArray['idCollection']));
                                $product->setProductCollectioncollection($collection);
                            }

                            if($dataInternalArray['idProductDepartament'] != null){

                                    $departament = getEm()->getRepository('ProductDepartment')->find(array('idproductDepartment' => $dataInternalArray['idProductDepartament']));
                                    $product->setProductDepartmentproductDepartment($departament);
                                }
                            if($dataInternalArray['taxGp'] != null){

                                    $tribute = getEm()->getRepository('Tribute')->find(array('idtribute' => $dataInternalArray['taxGp']));
                                    $product->setTaxGp($tribute);
                                }
                            if($dataInternalArray['printForPickIst'] != null){

                                $printer = getEm()->getRepository('Printer')->find(array('idprinter' => $dataInternalArray['printForPickIst']));
                                 $product->setPrintForPickIst($printer);
                            }
                            if($dataInternalArray['create_by'] != null){

                                $user = getEm()->getRepository('Users')->find(array('idusers' => $dataInternalArray['create_by']));
                                $product->setCreateBy($user);
                            }
                            if($dataInternalArray['project'] != null){

                                $project = getEm()->getRepository('Project')->find(array('idproject' => $dataInternalArray['project']));
                                $product->setProject($project);
                            }
                             if($dataInternalArray['section_idsection']!= null){

                                $warehouse = getEm()->getRepository('Warehouse')->find(array('idwarehouse' => $dataInternalArray['section_idsection']));
                                $product->setSectionsection($warehouse);
                            }

                             if($dataInternalArray['idSupplier']!= null){

                                        $supplier = getEm()->getRepository('Supplier')->find(array('idsupplier' => $dataInternalArray['idSupplier']));
                                         $product->setSuppliersupplier($supplier);
                                    }

                            if($dataInternalArray['idUnit'] != null){

                                        $unit = getEm()->getRepository('Unit')->find(array('idunit' => $dataInternalArray['idUnit']));
                                        $product->setUnitunit($unit);
                                    }
                             if($dataInternalArray['idWarranty'] != null){

                                        $warranty = getEm()->getRepository('Warranty')->find(array('idwarranty' => $dataInternalArray['idWarranty']));
                                         $product->setWarrantywarranty($warranty);
                                    }


                            getEm()->persist($product);
                            getEm()->flush();    
                                  
                        }
                    }
                }else{
                    $data = 'erro no upload';     
                    die();   
                }
            }else{
            $data = "Formato distinto - insira um arquivo .json";
            die();

            }
            
        }
        $result = 'success';
        $message = 'query success';
        $data = '';
        }catch(Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = '';


        }


       
        $data = array("result" => $result, "message" => $message, "data" => $data);
        $json_data = json_encode($data);
        print $json_data;

    } 
    /**
     * translate the word of all pages of the system with the language specified
     * @param string $string
     * @param int $pos
     * @return string json_data 
     */
    public function translate($string, $pos){
        $Language = getEm()->getRepository('Language')->findOneBy(array("active" => 1));
        $WordOriginal = getEm()->getRepository('WordOriginal')->findAll();
        $WordTranslated = getEm()->getRepository('WordTranslated')->findAll();
        $string = base64_decode($string);
        //susbstituir função do e comercial
        $string = str_replace("&amp;", "&", $string);
        $string = str_replace("/", " / ", $string);
        $string = str_replace(",", " , ", $string);
        $string = str_replace(":", " : ", $string);
        $string = str_replace("(", " ( ", $string);
        $string = str_replace(")", " ) ", $string);
        $string = str_replace(".", " . ", $string);
        $string = str_replace("-", " - ", $string);
        $string = str_replace("?", " ? ", $string);
        $stringTranslated = $string;
        $stringConcatened = null;
        try{
            if($Language != null && $WordTranslated != null && $WordOriginal != null){
                if($Language->getName() != 'english'){
                    $espaco = strripos($string, " ");

                    if($espaco == true){ 
                        $stringEx = explode(" ", $string);

                        foreach($stringEx as $parts){
                            $stringLowercase = strtolower($parts);
                            foreach ($WordOriginal as $original) {
                                if($stringLowercase == $original->getWord()){
                                    $newString = null;
                                   
                                    foreach ($WordTranslated as $translated) {
                                        if($translated->getWordOriginalWordOriginal()->getIdwordOriginal() == $original->getIdwordOriginal() && $translated->getLanguagelanguage()->getIdlanguage() == $Language->getIdlanguage()){
                                            $newString = $translated->getNewWord();
                                            if(preg_match('/^[A-Z]/', $parts{0})) {
                                                $newString = ucfirst($newString);
                                            }

                                            if(ctype_upper($parts)){
                                                $newString = strtoupper($newString);
                                            }
                                            $stringConcatened .= $newString." ";

                                        }
                                    }

                                }
                            }

                        }
                       $stringConcatened = substr($stringConcatened,0,-1);
                       
                    }else{ 
                        $stringLowercase = strtolower($string);
                        foreach ($WordOriginal as $original) {                     
                            if($stringLowercase == $original->getWord()){
                                foreach ($WordTranslated as $translated) {
                                    if($translated->getWordOriginalWordOriginal()->getIdwordOriginal() == $original->getIdwordOriginal() && $translated->getLanguagelanguage()->getIdlanguage() == $Language->getIdlanguage()){
                                            $stringTranslated = $translated->getNewWord();
                                            if(preg_match('/^[A-Z]/', $string{0})) {
                                                $stringTranslated = ucfirst($stringTranslated);
                                            }
                                            if(ctype_upper($string)){
                                                $stringTranslated = strtoupper($stringTranslated);
                                            }    
                                    }
                                }
                            }
                        }
                    }
                    
                } 
            }
            $result = 'success';
            $message = 'query success';
            $espaco = strripos($string, " ");
            if($espaco == true){
                if($stringConcatened == null){
                    $stringConcatened = $string;
                }
                $data = $stringConcatened;
            }else{
                $data = $stringTranslated;  
            }
             $data = str_replace(" , ", ", ", $data);
             $data = str_replace(" / ", "/", $data);
             $data = str_replace(" : ", ":", $data);
             $data = str_replace(" ( ", " (", $data);
             $data = str_replace(" ) ", ") ", $data);
             $data = str_replace(" . ", ".", $data);
             $data = str_replace(" - ", "-", $data);
             $data = str_replace(" ? ", "? ", $data);
             $data = ucfirst($data);
        }catch(Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = '';
        }
        $data = utf8_encode($data);
        $data = array("result" => $result, "message" => $message, "data" => $data, "pos" => $pos);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * verify language for translate
     * @return string json_data 
     */
    public function verifyLanguage(){
        $Language = getEm()->getRepository('Language')->findOneBy(array("active" => 1));
        if($Language != NULL){  
            try {             
                $data = "other";
                if($Language->getName() == 'english'){
                    $data = "english";
                }

                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );

            $json_data = json_encode($data);
            print $json_data;
        }else{
            var_dump("without language active");
        }
    }
}



                           
