<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#suppliers').DataTable({
            "ajax": {"url": my_url + "Reports/Supplier/getAll/"},
            "columns": [
                {"data": "id"},
                {"data": "name"},
                {"data": "address"},
                {"data": "zip"},
                {"data": "city"},
                {"data": "phone"},
                {"data": "email"},
                {"data": "contact"},
                {"data": "phoneContact"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
    });
</script>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<style type="text/css">
    .content {
        padding-top: 0px;
    }
    table {
        margin-top: 0px;
    }
    .table-bordered thead {
        box-shadow: none;
    }
    .dataTables_filter {
        padding-bottom: 0px;
        padding-top: 0px;
    }
    .dataTables_length {
        padding-bottom: 0px;
        padding-top: 0px;        
    }
</style>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <table id="suppliers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Address</th>
                        <th class="translate">code ZIP</th>
                        <th class="translate">City</th>
                        <th class="translate">Phone</th>
                        <th class="translate">Email</th>
                        <th class="translate">Contact</th>
                        <th class="translate">Phone Contact</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate"
                    onclick="printJS({printable: 'suppliers',
                                type: 'html',
                                showModal: true,
                                modalMessage: 'Printing...'})">Print Report</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>