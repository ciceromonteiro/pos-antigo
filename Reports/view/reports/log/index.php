<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#logs').DataTable( {
            "ajax": {"url": my_url+"Reports/Log/getAllLogs/"},
            "columns": [
                { "data": "id" },
                { "data": "date" },
                { "data": "action" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        }); 
    });
</script>

<ul class="navbar-three">
    <li class="active">
        <a href="#" class="translate">List</a>
    </li>
</ul>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <table id="logs" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Date</th>
                        <th class="translate">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate">Print Report</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>