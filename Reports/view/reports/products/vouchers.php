<?php 
    
    $gross_price = "";
    $liquid_price = "";
    
    if(!empty($array_answer['product_voucher'])){
        foreach ($array_answer['product_voucher'] as $value) {
            $gross_price += $gross_price + $value->getPrice();
            $liquid_price += $liquid_price + $value->getPrice();
        }
    } else {
        $gross_price = "0,00";
        $liquid_price = "0,00";
    }

    $nav = "vouchers";
    include "nav.php";
?>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#products_voucher').DataTable( {
            "ajax": {"url": my_url+"Reports/Product/getAllProductsVouchers/"},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "purchasePrice" },
                { "data": "salePrice" },
                { "data": "group" },
                { "data": "numberPack" },
                { "data": "groupTax" },
                { "data": "update" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });        
    });
</script>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="pull-left">
                <ul style="list-style-type: none; margin-right: 30px">
                    <li><t class="translate">Gross value:</t> <b><?php echo $gross_price ?></b></li>
                    <li><t class="translate">Liquid value:</t> <b><?php echo $liquid_price ?></b></li>
                    <li><p><br><input type="checkbox" name="enable_changes"> <t class="translate">Allow changes to credit notes below</t></p></li>
                </ul>
                </div>
                <div class="pull-left">
                    <div class="btn-group dropup" style="display: inline;"> 
                        <button type="button" class="btn btn-primary translate">More options</button> 
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                            <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                        </button> 
                        <ul class="dropdown-menu"> 
                            <li><a href="#" class="translate">Print</a></li>
                            <li><a href="#" class="translate">Move</a></li>
                            <li><a href="#" class="translate">Change Items</a></li>
                            <li><a href="#" class="translate">Remove Customer</a></li>
                            <li><a href="#" class="translate">Update Item</a></li>
                            <li><a href="#" class="translate">Duplicate Note</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="products_voucher" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                    <th style="width: 80px" class="translate">ID</th>
                    <th class="translate">Name of product</th>
                    <th class="translate">Purchase price</th>
                    <th class="translate">Sale price</th>
                    <th class="translate">Product group</th>
                    <th class="translate">Package number</th>
                    <th class="translate">Tax group</th>
                    <th class="translate">Update</th>
                    <th class="translate">Status</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>