<?php 
    $nav = "products";
    include "nav.php";
?>
<script type="text/javascript">
var table;
var tableProductQuantity;
    $(document).ready(function() {
        table = $('#products').DataTable( {
            "ajax": {"url": my_url+"Reports/Product/getAllProducts/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "purchasePrice" },
                { "data": "salePrice" },
                { "data": "group" },
                { "data": "numberPack" },
                { "data": "groupTax" },
                { "data": "update" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  

        tableProduct = $('#productTableIntern').DataTable( {
            "ajax": {"url": my_url+"Reports/Product/getAllProducts/"},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  

        tableProductQuantity = $('#aroundQuantityProductTable').DataTable( {
            "ajax": {"url": my_url+"Reports/Product/getAllProducts/"},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        }); 
         tablePrinters = $('#printersTable').DataTable( {
            "ajax": {"url": my_url+"Reports/Product/getAllPrinters/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "instaledIn" },
                { "data": "type" },
                { "data": "port" },
                { "data": "dateCreate" },
                { "data": "active" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  

        $("#products tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;



            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
            var itemSelected = checkMark('#products');
            if(itemSelected == 0){

                $('#printLabel').addClass('disabled');
                $('#roundQuantity').addClass('disabled');
            }
            if(itemSelected == 1){

                $('#printLabel').removeClass('disabled');
                $('#roundQuantity').removeClass('disabled');
            }
            if(itemSelected > 1){
                $('#printLabel').addClass('disabled');
                $('#roundQuantity').addClass('disabled');

            }
            
           
        });

        $("#printersTable tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
                
                
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
                
                
            }

            var itemSelected = checkMark('#printersTable');
            if(itemSelected == 0){
            $('#print').addClass('disabled');
            }
            if(itemSelected == 1){
            $('#print').removeClass('disabled');
            }
            if(itemSelected > 1){
            $('#print').addClass('disabled');

            }
           
        });       
    });
    function openModalPrintLabels(){
        tableProduct.destroy();
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');

        $('#modalPrintLabel').modal({
                show: true
            });
        tableProduct = $('#productTableIntern').DataTable( {
            "ajax": {"url": my_url+"Reports/Product/getProduct/"+idProduct},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "barCode" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  
        document.getElementById("qrcode").checked = true;
      
        qrCode();


        
    }

    function openModalAroundQuantity(){
        tableProductQuantity.destroy();
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
        $('#modalAroundQuantity').modal({
                show: true
            });
        tableProductQuantity = $('#aroundQuantityProductTable').DataTable( {
            "ajax": {"url": my_url+"Reports/Product/getProduct/"+idProduct},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "barCode" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  

        $.ajax({

                url: my_url + "Reports/Product/getProductAroundQuantity/"+idProduct,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        document.getElementById("stockQuantity").innerHTML = data.data;
                    } else {
                        notification(false);
                        console.log(data.message);
          }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        
    }

    function qrCode(){
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
        $.ajax({
                url: my_url + "Reports/Product/getProduct/"+idProduct,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        if(data.data[0].barCode != 'no data'){ 
                            document.getElementById("imageDiv").style.display = "block";
                            document.getElementById("image_preview").style.display = "block";
                            document.getElementById("gerarCode").style.display = "none";
                            $('#image_preview').attr('src', data.data[0].qrCode);
                        }else{
                            document.getElementById("imageDiv").style.display = "none";
                            document.getElementById("image_preview").style.display = "none";
                            document.getElementById("gerarCode").style.display = "block";

                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }

    function barCode(){
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
        $.ajax({
                url: my_url + "Reports/Product/getBarCodeProduct/"+idProduct,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        if(data.data != 'no data'){ 
                            document.getElementById("imageDiv").style.display = "block";
                            document.getElementById("image_preview").style.display = "block";
                            document.getElementById("gerarCode").style.display = "none";
                            $('#image_preview').attr('src', data.data);
                        }else{
                            document.getElementById("imageDiv").style.display = "none";
                            document.getElementById("image_preview").style.display = "none";
                            document.getElementById("gerarCode").style.display = "block";
                        }                         
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    
    }

    function orderProductByValue(){
        table.destroy();
        table = $('#products').DataTable( {
            "ajax": {"url": my_url+"Reports/Product/orderProductByValue/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "purchasePrice" },
                { "data": "salePrice" },
                { "data": "group" },
                { "data": "numberPack" },
                { "data": "groupTax" },
                { "data": "update" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  
    }

    function generateCodeBar(){
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
        $.ajax({
                url: my_url + "Reports/Product/generateBarCode/"+idProduct,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true); 
                        document.getElementById("qrcode").checked = true; 
                        qrCode();
                        tableProduct.destroy();
                        tableProduct = $('#productTableIntern').DataTable( {
                            "ajax": {"url": my_url+"Reports/Product/getProduct/"+idProduct},
                            "columns": [
                                { "data": "id" },
                                { "data": "name" },
                                { "data": "barCode" }
                            ],
                            "language": {
                                "url": my_url+my_language+".json"
                            }
                        });  
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }


    
</script>
<style type="text/css">
    
.pointer{
    cursor: pointer;
}
.disabled{
    pointer-events:none;
    opacity:0.4;
}

</style>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <table id="products" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('products')"></th>
                        <th style="width: 10px" class="translate">ID</th>
                        <th class="translate">Name product</th>
                        <th class="translate">Purchase price</th>
                        <th class="translate">Sale price</th>
                        <th class="translate">Group products</th>
                        <th class="translate">Number of package</th>
                        <th class="translate">Postering Group</th>
                        <th class="translate">Update</th>
                        <th class="translate">Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li ><a id="printLabel" class="pointer disabled translate" onclick="openModalPrintLabels()">Print Labels</a></li> 
                    <li><a id="o" class="pointer translate" onclick="orderProductByValue()">Balance In Stock</a></li> 
                    <li><a id="roundQuantity" class="pointer disabled translate" onclick="openModalAroundQuantity()">Round Quantity</a></li> 
                </ul> 
            </div>
        </div>

    </div>
</div>

<div class="modal fade row row-fluid" id="modalPrintLabel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Print Labels</h4>
            </div>
              
             <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4 class="translate">Product Details</h4> 
                    <div style="float: right;"">
                        <input id="qrcode" type="radio" name="opcao" value="QrCode" checked onclick="qrCode()"> <t class="translate">QrCode</t>
                        <input type="radio" name="opcao" value="BarCode" onclick="barCode()"> <t class="translate">BarCode</t>
                        <div id='imageDiv' class="inputFileImg" style="border: 1px solid #000">
                           <img  width="130" id="image_preview" src="../../../assets/images/upload.png" alt="your image"/>
                        </div>
                        <div id='gerarCode' style="text-align: center; display: none;">
                            <p style="color: red" class="translate">without barcode</p>
                            <button id="print" class="btn btn-warning translate" onclick="generateCodeBar()">Generate</button> 
                        </div>
                    </div>
                        <table id="productTableIntern" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="translate">ID</th>
                                <th class="translate">Name</th>
                                <th class="translate">barCode</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                </div>
                <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4 class="translate">Printers</h4>  
                        <table id="printersTable" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('printersTable')"></th>
                                <th class="translate">ID</th>
                                <th class="translate">Instaled In</th>
                                <th class="translate">Type</th>
                                <th class="translate">Port</th>
                                <th class="translate">Date Created</th>
                                <th class="translate">active</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    <button id="print" class="btn btn-success disabled translate">Print</button>
                </div>
                </div>



        </div>
    </div>
</div>

<div class="modal fade row row-fluid" id="modalAroundQuantity" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Around Quantity</h4>
            </div>
              
             <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4 class="translate">Product Details</h4> 
                    
                        <table id="aroundQuantityProductTable" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="translate">ID</th>
                                <th class="translate">Name</th>
                                <th class="translate">barCode</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                </div>
                <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4 class="translate" style="float: left;">Round Quantity in Stock:</h4>  
                    <h4 id="stockQuantity" style="float: left;"> 0</h4>
                    </div>
                </div>

                </div>



        </div>
    </div>
</div>