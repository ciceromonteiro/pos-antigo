<?php 
    $nav = "default_alerts";
    include "nav.php";
?>

<div class="content">
    <div class="row">
        <?php 
            include 'warnings_in_case_of_delay.php';
            include 'report_credit_manager.php';
        ?>
    </div>
    <div class="btns-footer">
        <div class="pull-right">
            <button class="btn btn-primary">Cancel</button>
            <button class="btn btn-success">Save</button>
        </div>
    </div>
</div>

