<div id="navbar-three">
    <ul class="navbar-three">
        <li <?php echo($nav == "products") ? ' class="active"' : "" ?>>
        	<a href="<?php echo URL_BASE."Reports/product/index"; ?>" class="translate">Products</a>
        </li>
        <li <?php echo($nav == "vouchers") ? ' class="active"' : "" ?>>
        	<a href="<?php echo URL_BASE."Reports/product/vouchers"; ?>" class="translate">Vouchers</a>
        </li>
        <li <?php echo($nav == "default_alerts") ? ' class="active"' : "" ?>>
        	<a href="<?php echo URL_BASE."Reports/product/default_alerts"; ?>" class="translate">Standard Warnings</a>
        </li>
    </ul>
</div>