<script type="text/javascript">
    $(document).ready(function() {
        
        var table_report = $('#report_credit_manager').DataTable( {
            "ajax": {"url": my_url+"Reports/product/getAllReport/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "sendAfter" },
                { "data": "messageTitle" },
                { "data": "message" },
                { "data": "sendCopy" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create_report_credit_manager', function(e){
            e.preventDefault();
            resetForm('#report_credit_manager_form');
            $('#report_credit_manager_modal').modal({
                show : true
            });
        });

        $(document).on('click', '#update_report_credit_manager', function(e){
            e.preventDefault();
            resetForm('#report_credit_manager_form_edit');
            $.ajax({
                url : my_url+"Reports/product/getReport/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table_report.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#report_credit_manager_modal_edit').modal({show : true});
                        $('#report_credit_manager_form_edit #idReport').val(data.data[0].id);
                        $('#report_credit_manager_form_edit #sendAfter').val(data.data[0].sendAfter);
                        $('#report_credit_manager_form_edit #messageTitle').val(data.data[0].messageTitle);
                        $('#report_credit_manager_form_edit #message').val(data.data[0].message);
                        if(data.data[0].sendCopy == 1){
                            $('#report_credit_manager_form_edit #sendCopy').prop('checked', true);
                        } else {
                            $('#report_credit_manager_form_edit #sendCopy').prop('checked', false);
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#report_credit_manager_form', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Reports/product/insertReport",
                type: "POST",
                dataType: 'JSON',
                data : $('#report_credit_manager_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#report_credit_manager_modal').modal('hide');
                    reload_table(table_report);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#report_credit_manager_form_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Reports/product/updateReport",
                type: "POST",
                dataType: 'JSON',
                data : $('#report_credit_manager_form_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#report_credit_manager_modal_edit').modal('hide');
                    reload_table(table_report);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete_report_credit_manager', function(e){
            e.preventDefault();
            var trs = table_report.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idReport : ids};
            deleteItens('Reports/product/removeReport', array_deletes, table_report);
        });
        
        $('#update_report_credit_manager').attr('disabled', true);
        $('#delete_report_credit_manager').attr('disabled', true);
        $("#report_credit_manager tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#report_credit_manager');
            if(itemSelected == 0){
                $('#update_report_credit_manager').attr('disabled', true);
                $('#delete_report_credit_manager').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_report_credit_manager').attr('disabled', false);
                $('#delete_report_credit_manager').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_report_credit_manager').attr('disabled', true);
                $('#delete_report_credit_manager').attr('disabled', false);
            }
        });
    });
</script>

<div class="col-md-12">
    <h4 style="padding-left: 30px" class="translate">Report credit manager</h4>
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create_report_credit_manager" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update_report_credit_manager" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete_report_credit_manager" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>

    <table id="report_credit_manager" class="table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                <th style="width: 80px" class="translate">ID</th>
                <th class="translate">Send after</th>
                <th class="translate">Message Title</th>
                <th class="translate">Message</th>
                <th class="translate">Send Copy</th>
            </tr>
        </thead>
    </table>
</div>

<!-- Create -->
<div class="modal fade" id="report_credit_manager_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Warnings in case of delay</h4>
            </div>
            <form id="report_credit_manager_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label for="sendAfter" class="label-style translate">Send After</label>
                            <input type="text" class="form-control number" id="sendAfter" name="sendAfter" required="true">
                        </div>
                        <div class="col-md-9">
                            <label for="messageTitle" class="label-style translate">Message Title</label>
                            <input type="text" class="form-control" id="messageTitle" name="messageTitle" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="message" class="label-style translate">Message Title</label>
                            <textarea class="form-control" name="message" id="message" rows="3" cols="4" required="true"></textarea>
                            <p style="margin-top: 15px">
                                <input type="checkbox" name="sendCopy" id="sendCopy"> <t class="translate">Send a copy of the last message to the inspection body</t>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="report_credit_manager_modal_edit" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update warnings in case of delay</h4>
            </div>
            <form id="report_credit_manager_form_edit">
                <div class="modal-body">
                    <div class="row">
                        <input type="number" id="idReport" name="idReport" required="true" style="display: none">
                        <div class="col-md-3">
                            <label for="sendAfter" class="label-style translate">Send After</label>
                            <input type="text" class="form-control number" id="sendAfter" name="sendAfter" required="true">
                        </div>
                        <div class="col-md-9">
                            <label for="messageTitle" class="label-style translate">Message Title</label>
                            <input type="text" class="form-control" id="messageTitle" name="messageTitle" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="message" class="label-style translate">Message Title</label>
                            <textarea class="form-control" name="message" id="message" rows="3" cols="4" required="true"></textarea>
                            <p style="margin-top: 15px">
                                <input type="checkbox" name="sendCopy" id="sendCopy"> <t class="translate">Send a copy of the last message to the inspection body</t>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>