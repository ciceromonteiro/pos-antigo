<script type="text/javascript">
    $(document).ready(function () {
        // var table = $('#customers').DataTable({
        //     "ajax": {"url": my_url + "Reports/Customer/getAllClient/"},
        //     "columns": [
        //         {"data": "id"},
        //         {"data": "name"}
        //     ],
        //     "language": {
        //         "url": my_url + my_language + ".json"
        //     }
        // });
        $('#customers').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": my_url+"Reports/customer/getAllClient"
        });
    });
</script>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-4">
                <p><input type="checkbox" name="show_open_invoice"> <t class="translate">Show customers with invoice in arrears</t></p>            
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="customers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px">ID</th>
                        <th>Name</th>
                        <th>CPF</th>
                        <th>Data de Nascimento</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate">Print Report</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>