<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#project').DataTable( {
            "ajax": {"url": my_url+"Reports/Project/getAll/"},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "description" },
                { "data": "start" },
                { "data": "price" },
                { "data": "end" },
                { "data": "priceAdditional" },
                { "data": "stop" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
    });
</script>

<!-- Menu -->
<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<!-- Table List -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <table id="project" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 10px" class="translate">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Description</th>
                        <th class="translate">Start</th>
                        <th class="translate">Price</th>
                        <th class="translate">End</th>
                        <th class="translate">Price additional</th>
                        <th class="translate">Stop</th>
                        <th class="translate">Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>