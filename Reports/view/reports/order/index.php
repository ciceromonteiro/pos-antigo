<?php 
    $nav = "pending";
    require_once 'nav.php';
?>

<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#pending').DataTable( {
            "ajax": {"url": my_url+"Reports/Order/getAllOrderPending/"},
            "columns": [
                { "data": "id" },
                { "data": "date" },
                { "data": "client" },
                { "data": "employee" },
                { "data": "value" },
                { "data": "info" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
    });
</script>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <table id="pending" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Date</th>
                        <th class="translate">Client</th>
                        <th class="translate">Employee</th>
                        <th class="translate">Value</th>
                        <th class="translate">Info</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate">Print Report</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>