<?php 
    $nav = "vouchers";
    include "nav.php";
?>

<div class="content">
    
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="pull-left">
                <ul style="list-style-type: none; margin-right: 30px">
                    <li><t class="translate">Gross value:</t> <b>0,00</b></li>
                    <li><t class="translate">Liquid value:</t> <b>0,00</b></li>
                    <li><p><br><input type="checkbox" name="enable_changes"> <t class="translate">Allow changes to credit notes below</t></p></li>
                </ul>
                </div>
                <div class="pull-left">
                    <div class="btn-group dropup" style="display: inline;"> 
                        <button type="button" class="btn btn-primary translate">More options</button> 
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                            <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                        </button> 
                        <ul class="dropdown-menu"> 
                            <li><a href="#" class="translate">Print</a></li>
                            <li><a href="#" class="translate">Move</a></li>
                            <li><a href="#" class="translate">Change Items</a></li>
                            <li><a href="#" class="translate">Remove Customer</a></li>
                            <li><a href="#" class="translate">Update Item</a></li>
                            <li><a href="#" class="translate">Duplicate Note</a></li>
                        </ul> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="color" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                    <th style="width: 80px" class="translate">ID</th>
                    <th class="translate">Default Message</th>
                    <th class="translate">Date</th>
                    <th class="translate">Client</th>
                    <th class="translate">Employee</th>
                    <th class="translate">Value</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>