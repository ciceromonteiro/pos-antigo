<ul class="navbar-three">
    <li <?php echo ($nav == "pending") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Order/index" ?>" class="translate">Pending</a>
    </li>
    <li <?php echo ($nav == "vouchers") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Order/vouchers" ?>" class="translate">Vouchers</a>
    </li>
    <li <?php echo ($nav == "mobiles") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Order/mobiles" ?>" class="translate">Mobiles</a>
    </li>
    <li <?php echo ($nav == "history") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Order/history" ?>" class="translate">History</a>
    </li>
</ul>