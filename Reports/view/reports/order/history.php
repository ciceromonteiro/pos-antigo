<?php 
    $nav = "history";
    require_once 'nav.php';
?>

<script type="text/javascript">
    function fillPayments(item, index){       
        $('#form_order_details #paymentsOrder tbody').append("<tr><td></td><td>"+item.name+"</td><td>"+item.value+"</td></tr>");
    }
    function fillProducts(item, index){
        if(item.size==null){
            item.size = "";
        }         
        if(item.color==null){
            item.color = "";
        }
        if(item.extraInfo==null){
            item.extraInfo = "";
        }
        $('#form_order_details #productsOrder tbody').append("<tr><td></td><td>"+item.name+"</td><td>"+item.qty+" x "+item.value+"</td><td>"+item.discount+"</td><td>"+item.size+"</td><td>"+item.color+"</td>"+item.extraInfo+"</tr>");
    }

    $(document).ready(function() {
        var table = $('#history').DataTable( {
            "ajax": {"url": my_url+"Reports/Order/getAllOrders/"},
            "columns": [
                { "data": "id" },
                { "data": "date" },
                { "data": "client" },
                { "data": "employee" },
                { "data": "value" },
                { "data": "info" },
                { "data": "link" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '.open-details', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Reports/Order/getOrder/"+$(this).attr("target_order"),
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + $(this).attr('target_order'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success') {                    
                        $('#form_order_details #identifyOrder').html(data.data[0].order_identify);
                        $('#form_order_details #valueOrder').val(data.data[0].value);
                        $('#form_order_details #valueOrderPaid').val(data.data[0].value_paid);
                        $('#form_order_details #valueOrderRemaining').val(data.data[0].value_remaining);

                        $('#form_order_details #paymentsOrder tbody').html("");
                        $('#form_order_details #productsOrder tbody').html("");
                     
                        if (data.data[0].payments.length>0) {                        
                            data.data[0].payments.forEach(fillPayments);
                        }
                        else {
                            $('#form_order_details #paymentsOrder').parent().hide();
                            //$('#form_order_details #paymentsOrder').hide();
                        }

                        if (data.data[0].products.length>0) {                        
                            data.data[0].products.forEach(fillProducts);
                        }
                        else {
                            $('#form_order_details #productsOrder').parent().hide();
                        }


                        if (data.data[0].customer) {
                            $('#form_order_details #customerOrder').val(data.data[0].customer);
                        }
                        else {
                            $('#form_order_details #customerOrder').val("-");
                        }



                        $('#orderDetailsModal').modal({show : true});
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
    });
</script>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <table id="history" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Date</th>
                        <th class="translate">Client</th>
                        <th class="translate">Employee</th>
                        <th class="translate">Value</th>
                        <th class="translate">Info</th>
                        <th class="translate"></th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate">Print Report</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>

<!-- Details -->
<div class="modal fade" id="orderDetailsModal" tabindex="-1" role="dialog" aria-labelledby="labelOrderDetails">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelOrderDetails">Order Details <span id="identifyOrder"></span></h4>
            </div>
            <form id="form_order_details">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="identifyOrder" class="label-style translate">Total</label>
                            <input type="text" class="form-control" id="valueOrder" disabled="true">
                        </div>

                        <div class="col-md-4">
                            <label for="identifyOrder" class="label-style translate">Payed</label>
                            <input type="text" class="form-control" id="valueOrderPaid" disabled="true">
                        </div>

                        <div class="col-md-4">
                            <label for="identifyOrder" class="label-style translate">Remaining</label>
                            <input type="text" class="form-control" id="valueOrderRemaining" disabled="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="paymentsOrder"><thead><tr><th></th><th class="translate">Payment</th><th class="translate">Value</th></tr></thead><tbody></tbody></table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table" id="productsOrder"><thead><tr><th></th><th class="translate">Product</th><th class="translate">Value</th><th class="translate">Discount</th><th class="translate">Cor</th><th class="translate">Tamanho</th><th class="translate">Descr.</th></tr></thead><tbody></tbody></table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="customerOrder" class="label-style translate">Customer</label>
                            <input type="text" class="form-control" id="customerOrder" disabled="true">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>