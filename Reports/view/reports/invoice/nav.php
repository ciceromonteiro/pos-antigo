<ul class="navbar-three">
    <li <?php echo ($nav == "Invoice") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/invoice/index" ?>" class="translate">Invoice</a>
    </li>
    <li <?php echo ($nav == "Printed or shipped") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/invoice/printedOrShipped" ?>" class="translate">Printed or shipped</a>
    </li>
    <li <?php echo ($nav == "Daily invoice closure") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/invoice/dailyInvoiceClosure" ?>" class="translate">Daily invoice closure</a>
    </li>
</ul>