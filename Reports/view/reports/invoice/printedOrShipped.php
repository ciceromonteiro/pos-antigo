<?php 
    $nav = "Printed or shipped";
    require_once 'nav.php';
?>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <label class="translate">Invoice Start:</label>
                <div class="input-group date">
                    <input type="text" class="form-control" name="invoice_start" required="true">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-3">
                <label class="translate">Search for ID:</label>
                <input type="text" name="invoice_start" id="invoice_start" class="form-control number">
            </div>
            <div class="col-md-3">
                <button style="margin-top: 35px" type="button" class="btn btn-primary translate">Send for email</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="suppliers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Invoice Date</th>
                        <th class="translate">Customer ID</th>
                        <th class="translate">Customer name</th>
                        <th class="translate">Value Liquid</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a href="#" class="translate">Print Report</a></li> 
                    <li><a href="#" class="translate">Send for email</a></li> 
                    <li><a href="#" class="translate">Create credit</a></li> 
                    <li><a href="#" class="translate">Duplicate invoice</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>