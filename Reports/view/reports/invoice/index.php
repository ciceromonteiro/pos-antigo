<?php 
$nav = "Invoice";
require_once 'nav.php';
?>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-2">
                <label for="data_start" class="translate">Select employee:</label>
                <input type="text" name="data_start" class="form-control number">
            </div>
            <div class="col-md-2">
                <label for="data_end" class="translate">Initial date:</label>
                <div class="input-group date">
                    <input type="text" class="form-control" name="initial_date" required="true">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-2">
                <label for="data_end" class="translate">Final date:</label>
                <div class="input-group date">
                    <input type="text" class="form-control" name="final_date" required="true">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-6">
                    <label></label>
                    <p><input type="checkbox" name="generete_pdf"> <t class="translate">Pending</t></p>
                    <p><input type="checkbox" name="view_display"> <t class="translate">Completed</t></p>
                </div>
                <div class="col-md-6">
                    <label></label>
                    <button style="margin-top: 35px" type="submit" class="col-md-12 btn btn-primary translate">Run</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="suppliers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Request Date</th>
                        <th class="translate">Terminal</th>
                        <th class="translate">Info</th>
                        <th class="translate">Employee Name</th>
                        <th class="translate">Order item</th>
                        <th class="translate">Type of purchase</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate">Print Report</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>