<?php
/* Created by PhpStorm.
 * User: rocha
 * Date: 10/24/16
 * Time: 2:39 PM
 */

$menuLeft = $navbar;
$menu = explode('|', $menuLeft);

?>

<div class="horizon-swiper">
    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/product/index' ?>">
            <div class='<?php echo ($menu[1] == "Product") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-text-align-justify"></span>
                <t class="translate">Produtos</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/accounting/index' ?>">
            <div class='<?php echo ($menu[1] == "Accounting") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-calendar-full"></span>
                <t class="translate">Financeiro</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/Customer/index' ?>">
            <div class='<?php echo ($menu[1] == "Customer") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-user"></span>
                <t class="translate">Clientes</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/supplier/index' ?>">
            <div class='<?php echo ($menu[1] == "Supplier") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-list"></span>
                <t class="translate">Fornecedores</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/stock/index' ?>">
            <div class='<?php echo ($menu[1] == "Stock") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-frame-contract"></span>
                <t class="translate">Estoque</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/order/index' ?>">
            <div class='<?php echo ($menu[1] == "Order") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-highlight"></span>
                <t class="translate">Compras</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/statistic/index' ?>">
            <div class='<?php echo ($menu[1] == "Statistic") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-earth"></span>
                <t class="translate">Estatísticas</t>
            </div>
        </a>
    </div>

    <!-- <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/project/index' ?>">
            <div class='<?php echo ($menu[1] == "Project") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-book"></span>
                <t class="translate">Projetos</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/invoice/index' ?>">
            <div class='<?php echo ($menu[1] == "Invoice") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-layers"></span>
                <t class="translate">Cobrança</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/log/index' ?>">
            <div class='<?php echo ($menu[1] == "Log") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-users"></span>
                <t class="translate">Logs</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Reports/requests/index' ?>">
            <div class='<?php echo ($menu[1] == "Requests") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-users"></span>
                <t class="translate">Requests</t>
            </div>
        </a>
    </div> -->
</div>

<script type="text/javascript">
    $('.horizon-swiper').horizonSwiper();
</script>