<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#closure').DataTable({
            "ajax": {"url": my_url + "Reports/Accounting/getAllClosingTheCarton/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "protocol"},
                {"data": "cashCloseDate"},
                {"data": "cashCloseDate"},
                {"data": "employee"},
                {"data": "cashValue"},
                {"data": "cashDifference"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
    });
</script>

<style type="text/css">
    #closure_wrapper #closure_length, #closure_filter {
        display: none
    }
</style>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">Closing the carton</a></li>
    </ul>
</div>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-3">
                
                <label class="label-style translate">Initial date:</label>
                <div class="input-group date">
                    <input type="text" class="form-control" name="dateStart" required="true">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
                
                <label class="label-style translate">Final date:</label>
                <div class="input-group date">
                    <input type="text" class="form-control" name="dateStart" required="true">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
            </div>

            <div class="col-md-4">
                <label></label>
                <p><input type="checkbox" name="generete_pdf"> <t class="translate">Generate PDF only</t></p>
                <p><input type="checkbox" name="send_reports"> <t class="translate">Submit report</t></p>
                <p><input type="checkbox" name="view_display"> <t class="translate">View on screen</t></p>
                <p><input type="checkbox" name="print"> <t class="translate">Print</t></p>
            </div>
            <div class="col-md-5">
                <label></label>
                <button style="margin-top: 35px" type="submit" class="col-md-12 btn btn-primary translate">Filter</button>
            </div>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="closure" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 10px" class="translate">ID</th>
                        <th class="translate">Protocol</th>
                        <th class="translate">Closing date and time</th>
                        <th class="translate">Cash Close Date</th>
                        <th class="translate">Employee</th>
                        <th class="translate">Cash value</th>
                        <th class="translate">Cash Difference</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate">Print Selected</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>