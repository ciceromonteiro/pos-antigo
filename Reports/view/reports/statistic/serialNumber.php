<?php 
    $nav = "serialNumber";
    require_once 'nav.php';
?>

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <label class="translate">Search Product:</label>
                <input type="text" name="invoice_start" id="invoice_start" class="form-control number">
            </div>
            <div class="col-md-3">
                <label class="translate">Search Serial Number:</label>
                <input type="text" name="invoice_start" id="invoice_start" class="form-control number">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="suppliers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Serial</th>
                        <th class="translate">Product Name</th>
                        <th class="translate">Arrival date in store</th>
                        <th class="translate">Sold in</th>
                        <th class="translate">Customer Name</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a href="#" class="translate">Print Report</a></li>
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>