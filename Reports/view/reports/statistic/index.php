<?php 
    $nav = "statistics";
    require_once 'nav.php';
?>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-2">
                <label for="data_start" class="translate">Existing report ID:</label>
                <input type="text" name="data_start" class="form-control number">
                
                
                <label for="select_template" class="translate">Select template:</label>
                <select>
                    <option value="0" class="translate">Select Option</option>
                </select>
                <p style="margin-top: 10px;"><a href="#" class="translate">Create template</a></p>
            </div>
            <div class="col-md-2">
                <label for="data_end" class="translate">Initial date:</label>
                <div class="input-group date">
                    <input type="text" class="form-control" name="initial_date" required="true">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
                
                <label for="select_template" class="translate">Statistics based on:</label>
                <select>
                    <option value="0" class="translate">Select Option</option>
                </select>
            </div>
            <div class="col-md-2">
                <label for="data_end" class="translate">Final date:</label>
                <div class="input-group date">
                    <input type="text" class="form-control" name="final_date" required="true">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
                
                <label for="data_start" class="translate">Adjust working hours:</label>
                <input type="text" name="data_start" class="form-control number">
            </div>
            <div class="col-md-4">
                <div class="col-md-6">
                    <label></label>
                    <p><input type="checkbox" name="generete_pdf"> <t class="translate">Final product without components</t></p>
                    <p><input type="checkbox" name="send_Reports"> <t class="translate">Final product with components</t></p>
                </div>
                <div class="col-md-6">
                    <label></label>
                    <p><input type="checkbox" name="view_display"> <t class="translate">Items excluded from reports</t></p>
                    <p><input type="checkbox" name="print"> <t class="translate">Products not yet sold</t></p>
                    <p><input type="checkbox" name="view_display"> <t class="translate">Vendor Product ID</t></p>
                </div>
            </div>
            <div class="col-md-2">
                <button style="margin-top: 35px" type="submit" class="col-md-12 btn btn-primary translate">Filter</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="suppliers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">ID Invoice</th>
                        <th class="translate">Create in</th>
                        <th class="translate">Expires in</th>
                        <th class="translate">Customer</th>
                        <th class="translate">Value without tax</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a href="#" class="translate">Print Report</a></li> 
                    <li><a href="#" class="translate">Send for email</a></li> 
                    <li><a href="#" class="translate">Send for SMS</a></li> 
                    <li><a href="#" class="translate">Create Graphic</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>