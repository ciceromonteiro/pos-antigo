<?php 
$nav = "products";
require_once 'nav.php';
?>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-2">
                <label for="data_start" class="translate">Search product:</label>
                <input type="text" name="data_start" class="form-control number">
            </div>
            
            <div class="col-md-2">
                <label for="group_product" class="translate">Group product:</label>
                <select>
                    <option value="0" class="translate">Select Option</option>
                </select>
            </div>
            
            <div class="col-md-2">
                <label for="select_template" class="translate">Select template:</label>
                <select>
                    <option value="0" class="translate">Select Option</option>
                </select>
                <p style="margin-top: 10px;"><a href="#" class="translate">Create template</a></p>
            </div>
            
            <div class="col-md-4">
                <div class="col-md-6">
                    <label></label>
                    <p><input type="checkbox" name="generete_pdf"> <t class="translate">Sold</t></p>
                    <p><input type="checkbox" name="view_display"> <t class="translate">Purchased</t></p>
                </div>
                <div class="col-md-6">
                    <label></label>
                    <button style="margin-top: 35px" type="submit" class="col-md-12 btn btn-primary translate">Filter</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="suppliers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Date</th>
                        <th class="translate">Customer Name</th>
                        <th class="translate">Qtd</th>
                        <th class="translate">Color/Size</th>
                        <th class="translate">Gross price</th>
                        <th class="translate">Discount</th>
                        <th class="translate">Gross price total</th>
                        <th class="translate">Serial of product</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a href="#" class="translate">Print Report</a></li> 
                    <li><a href="#" class="translate">Send for email</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>