<ul class="navbar-three">
    <li <?php echo ($nav == "statistics") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Statistic/index" ?>" class="translate">Statistics</a>
    </li>
    <li <?php echo ($nav == "orders") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Statistic/orders" ?>" class="translate">Orders</a>
    </li>
    <li <?php echo ($nav == "products") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Statistic/products" ?>" class="translate">Products</a>
    </li>
    <li <?php echo ($nav == "serialNumber") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Statistic/serialNumber" ?>" class="translate">Serial Number</a>
    </li>
    <li <?php echo ($nav == "projects") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Statistic/projects" ?>" class="translate">Projects</a>
    </li>
    <li <?php echo ($nav == "stock") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Reports/Statistic/stock" ?>" class="translate">Stock</a>
    </li>
</ul>