<div id="navbar-three">
    <ul class="navbar-three">
        <li <?php echo($nav == "Acquisitions") ? ' class="active"' : "" ?>>
        	<a href="<?php echo URL_BASE."Reports/stock/index"; ?>" class="translate">Acquisitions</a>
        </li>
        <li <?php echo($nav == "Receipt of purchase order") ? ' class="active"' : "" ?>>
        	<a href="<?php echo URL_BASE."Reports/stock/receiptOfPurchaseOrder"; ?>" class="translate">Receipt of purchase order</a>
        </li>
        <li <?php echo($nav == "Suggestions for buying") ? ' class="active"' : "" ?>>
        	<a href="<?php echo URL_BASE."Reports/stock/suggestionsForBuying"; ?>" class="translate">Suggestions for buying</a>
        </li>
        <li <?php echo($nav == "Inventory") ? ' class="active"' : "" ?>>
            <a href="<?php echo URL_BASE."Reports/stock/inventory"; ?>" class="translate">Inventory</a>
        </li>
        <li <?php echo($nav == "Stock Movement") ? ' class="active"' : "" ?>>
            <a href="<?php echo URL_BASE."Reports/stock/stockMovement"; ?>" class="translate">Stock Movement</a>
        </li>
    </ul>
</div>