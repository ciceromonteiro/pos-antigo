<?php
$nav = "Acquisitions";
include "nav.php";
?>

<!-- Table Acquisitions -->
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <table id="acqtable" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('acqtable')"></th>
                        <th style="width: 10px" class="translate">ID</th>
                        <th class="translate">Product Name</th>
                        <th class="translate">Qty</th>
                        <th class="translate">Price</th>
                        <th class="translate">Total Price</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#acqtable').DataTable({
            "ajax": {"url": my_url + "Reports/Stock/getAllAcquisitions/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "name"},
                {"data": "qty"},
                {"data": "salePrice"},
                {"data": "totalPrice"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
    });
</script>