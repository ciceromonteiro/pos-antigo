<?php
$nav = "Stock Movement";
include "nav.php";
?>


<!-- Table Acquisitions -->
<div class="content">
    <div class="row">
        <div class="col-xs-3">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <label for="nameColor" class="label-style translate">Product ID</label>
                    <input type="text" class="form-control" id="nameColor" name="nameColor" required="true">
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <label for="nameColor" class="label-style translate">Employee</label>
                    <input type="text" class="form-control" id="nameColor" name="nameColor" required="true">
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <label for="nameColor" class="label-style translate">Date count</label>
                    <div>  17-08-2016 12:24 </div>
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <label for="nameColor" class="label-style translate">Stock</label>
                    <select>
                        <option class="translate">Select option</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <input type="checkbox"> <t class="translate">Surplus items</t>

                    <input type="checkbox"> <t class="translate">Absent</t>
                </div>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-md-12">
            <table id="stockmovement" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('stockmovement')"></th>
                        <th style="width: 10px" class="translate">ID</th>
                        <th class="translate">Product Name</th>
                        <th class="translate">Qty</th>
                        <th class="translate">Price</th>
                        <th class="translate">Total Price</th>
                        <th class="translate">Location</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">Print</button>
            </div>
            <div class="btn-group dropup"> 
                <select>
                    <option class="translate">Select device</option>
                </select>
            </div>
            <div class="btn-group dropup"> 
                <t class="translate">Send:</t>
            </div> 
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">Email</button>
            </div> 
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-success translate">Save</button>
            </div>
        </div>

        <div class="pull-right">
            <button type="button" class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="acquisitions_modal_create" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Acquisitions</h4>
            </div>
            <form id="acquisitions_form_create">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="nameColor" class="label-style translate">Employee</label>
                            <select>
                                <option class="translate">Select option</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="nameColor" class="label-style translate">Supplier</label>
                            <select>
                                <option class="translate">Select option</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="nameColor" class="label-style translate">Arrival date</label>
                            <input type="text" class="form-control" id="nameColor" name="nameColor" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="nameColor" class="label-style translate">Order Observations</label>
                            <textarea class="form-control" id="nameColor" name="nameColor"></textarea>
                            <ul class="list-inline" style="margin-top: 15px">
                                <li><p><input type="checkbox" name="bekreflet"> <t class="translate">Confirm</t></p></li>
                                <li><p><input type="checkbox" name="bekreflet"> <t class="translate">Order sendt</t></p></li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="list-inline">
                                <li>
                                    <div class="btn-group" role="group">
                                        <button id="create_acquisition_item" type="button" class="btn btn-primary translate">Add</button>
                                        <button id="update_acquisition_item" type="button" class="btn btn-primary translate">Update</button>
                                        <button id="delete_acquisition_item" type="button" class="btn btn-primary translate">Delete</button>
                                    </div>
                                </li>
                            </ul>
                            <table id="shippingMethod" class="table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                                        <th style="width: 80px" class="translate">ID</th>
                                        <th class="translate">Product ID</th>
                                        <th class="translate">Product Name</th>
                                        <th class="translate">Qty</th>
                                        <th class="translate">Price</th>
                                        <th class="translate">Total Price</th>
                                        <th class="translate">Location</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Create Aquisition Item -->
<div class="modal fade" id="create_acquisition_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Acquisitions</h4>
            </div>
            <form id="acquisitions_form_create">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="nameColor" class="label-style translate">Product barcode</label>
                            <input type="text" class="form-control" id="nameColor" name="nameColor" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="nameColor" class="label-style translate">Qty</label>
                            <input type="text" class="form-control" id="nameColor" name="nameColor" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="nameColor" class="label-style translate">Discount</label>
                            <input type="text" class="form-control" id="nameColor" name="nameColor" required="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-inline" style="margin-top: 15px;">
                                <li><p><input type="checkbox" name="test"> <t class="translate">List only vendor items</t></p></li>
                                <li><p><input type="checkbox" name="test"> <t class="translate">Automatically enter value 1</t></p></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameColor" class="label-style translate">Obs</label>
                            <textarea class="form-control" id="nameColor" name="nameColor"></textarea>
                            <ul class="list-inline" style="margin-top: 15px">
                                <li><p><input type="checkbox" name="bekreflet"> <t class="translate">confirm</t></p></li>
                                <li><p><input type="checkbox" name="bekreflet"> <t class="translate">Order sendt</t></p></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p><b class="translate">Product information:</b></p>
                        </div>
                        <div class="col-md-6">
                            <p><t class="translate">Available in stock: </t><span>0</span></p>
                            <p><t class="translate">Total ordering products: </t><span>0</span></p>
                        </div>
                        <div class="col-md-6">
                            <p><t class="translate">Orders made and waiting: </t><span>0</span></p>
                            <p><t class="translate">Qty of products: </t><span>0</span></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- la vai
<script type="text/javascript">
    $(document).ready(function () {

        var table = $('#color').DataTable({
            "ajax": {"url": my_url + "Entry/Color/getAll/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "description"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });

        $(document).on('click', '#create_acquisitions', function (e) {
            e.preventDefault();
            resetForm('#acquisitions_form_create');
            $('#acquisitions_modal_create').modal({
                show: true
            });
        });

        $(document).on('click', '#create_acquisition_item', function (e) {
            e.preventDefault();
            resetForm('#acquisitions_form_create');
            $('#create_acquisition_item_modal').modal({
                show: true
            });
        });

        $(document).on('click', '#update', function (e) {
            e.preventDefault();
            resetForm('#form_color_edit');
            $.ajax({
                url: my_url + "Entry/Color/getColor/",
                type: "POST",
                dataType: 'JSON',
                data: 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#colorEditModal').modal({show: true});
                        $('#form_color_edit #idColor').val(data.data[0].id);
                        $('#form_color_edit #nameColor').val(data.data[0].description);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_color', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Entry/Color/insert",
                type: "POST",
                dataType: 'JSON',
                data: $('#form_color').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_color_edit', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Entry/Color/update",
                type: "POST",
                dataType: 'JSON',
                data: $('#form_color_edit').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#colorEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function (e) {
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });
            var i = 0, ids = [];
            for (i = 0; i < trs.length; i++) {
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }
            var array_deletes = {idColors: ids};
            deleteItens('Entry/Color/deleteColors', array_deletes, table);
        });

        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#color tbody").on('click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')) {
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#color');
            if (itemSelected == 0) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if (itemSelected == 1) {
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if (itemSelected > 1) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>
--> 
<script type="text/javascript">
    var table;
    $(document).ready(function () {
        table = $('#stockmovement').DataTable({
            "ajax": {"url": my_url + "Reports/Stock/getAllStockMovement/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "name"},
                {"data": "qty"},
                {"data": "salePrice"},
                {"data": "totalPrice"},
                {"data": "location"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });
    });
</script>