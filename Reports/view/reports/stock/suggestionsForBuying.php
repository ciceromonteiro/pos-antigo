<?php 
    $nav = "Suggestions for buying";
    include "nav.php";
?>


<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <div class="col-md-4">
                <label for="data_end" class="translate">Generate from:</label>
                <div class="input-group date">
                    <input type="text" class="form-control" name="initial_date" required="true">
                    <span class="input-group-addon">
                        <i class="glyphicon glyphicon-th"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                <label for="data_end" class="translate">Make a suggestion list:</label>
                <button type="submit" class="col-md-12 btn btn-primary translate">Create</button>
            </div>
            <div class="col-md-4">
                <label for="data_end" class="translate">Turn into purchase order:</label>
                <button type="submit" class="col-md-12 btn btn-primary translate">Create</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="suppliers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Supplier</th>
                        <th class="translate">Product ID</th>
                        <th class="translate">Product Name</th>
                        <th class="translate">On Stock</th>
                        <th class="translate">Waiting to arrive</th>
                        <th class="translate">Suggested qty</th>
                        <th class="translate">Prediction</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <button class="btn btn-primary translate">Import</button>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>