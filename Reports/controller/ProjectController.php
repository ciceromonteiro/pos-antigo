<?php

class ProjectController {

    public function index() {
        $navbar = "Reports|Project";
        $array_answer = array("");
        GenericController::template("Reports", "project", "index", $navbar, $array_answer, 168);
    }

    public function getAll() {
        try {
            $projects = getEm()->getRepository('Project')->findAll();
            $data = array();
            foreach ($projects as $value) {
                switch ($value->getActive()) {
                    case 1:
                        $status = "Active";
                        break;
                    case 2:
                        $status = "Blocked";
                        break;
                    case 3:
                        $status = "Delete";
                }
                $start = $value->getDateStart();
                $end = $value->getDateEnd();
                $stop = $value->getDateStop();
                $dat = array(
                    "id" => $value->getIdproject(),
                    "name" => $value->getName(),
                    "description" => $value->getDescription(),
                    "start" => isset($start) ? $start->format('m/d/Y') : '',
                    "price" => $value->getPrice(),
                    "end" => isset($end) ? $end->format('m/d/Y') : '',
                    "priceAdditional" => $value->getPriceAdditional(),
                    "stop" => isset($stop) ? $stop->format('m/d/Y') : '',
                    "status" => $status
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }

}

?>