<?php

class RequestsController {

    public function index() {
        $navbar = "Reports|Requests";
        $array_answer = array("");
        GenericController::template("Reports", "requests", "index", $navbar, $array_answer, 179);
    }
    
    public function getAllRequests(){
        try{
            $requests = getEm()->getRepository('Requests')->findAll();
            $data = array ();
            foreach ($requests as $value){
                $dat = array (
                    "id" => $value->getIdrequests(),
                    "date" => $value->getDate(),
                    "status" => $value->getStatus(),
                    "direction" => $value->getDirection()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function insertRequest($query){
        try {
            $request = new Requests();
            $request->getQuery($query);
            $request->setDate($dateCreate);
            $request->setStatus(0);
            $request->setDirection(0);
            getEm()->persist($request);
            getEm()->flush();
            return true;
        } catch (Exception $exc) {
            $exc->getTraceAsString();
            return false;
        }
    }

}

?>