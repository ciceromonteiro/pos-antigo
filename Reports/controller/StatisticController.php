<?php

class StatisticController {

    public function index() {
        $navbar = "Reports|Statistic";
        $array_answer = array("");
        GenericController::template("Reports", "statistic", "index", $navbar, $array_answer, 146);
    }

    public function orders() {
        $navbar = "Reports|Statistic";
        $array_answer = array("");
        GenericController::template("Reports", "statistic", "orders", $navbar, $array_answer, 152);
    }

    public function products() {
        $navbar = "Reports|Statistic";
        $array_answer = array("");
        GenericController::template("Reports", "statistic", "products", $navbar, $array_answer, 154);
    }

    public function serialNumber() {
        $navbar = "Reports|Statistic";
        $array_answer = array("");
        GenericController::template("Reports", "statistic", "serialNumber", $navbar, $array_answer, 158);
    }

    public function projects() {
        $navbar = "Reports|Statistic";
        $array_answer = array("");
        GenericController::template("Reports", "statistic", "projects", $navbar, $array_answer, 160);
    }

    public function stock() {
        $navbar = "Reports|Statistic";
        $array_answer = array("");
        GenericController::template("Reports", "statistic", "stock", $navbar, $array_answer, 162);
    }

}

?>