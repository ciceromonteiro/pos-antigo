<?php

class OrderController {

    public function index() {
        $navbar = "Reports|Order";
        $array_answer = array("");
        GenericController::template("Reports", "order", "index", $navbar, $array_answer, 137);
    }
    
    public function getAllOrderPending(){
        try{
            $orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $data = array ();
            foreach ($orders as $value){
                if($value->getParked() == true){
                    if($value->getPersonperson() != null){
                        $client = $value->getPersonperson()->getName();
                    } else {
                        $client = "";
                    }
                    
                    if($value->getUsersusers() != null){
                        $employee = $value->getUsersusers()->getLogin();
                    } else {
                        $employee = "";
                    }
                    
                    $dat = array (
                        "id" => $value->getIdorder(),
                        "date" => $value->getDateCreate(),
                        "client" => $client,
                        "employee" => $employee,
                        "value" => number_format($value->getTotalAmount(),2,",","."),
                        "info" => ""
                    );
                    array_push($data, $dat);
                }                
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllOrderMobile(){
        try{
            $orders = getEm()->getRepository('Order')->findBy(array("active" => 1));
            $data = array ();
            foreach ($orders as $value){
                if($value->getMobile() == true){
                    if($value->getPersonperson() != null){
                        $client = $value->getPersonperson()->getName();
                    } else {
                        $client = "";
                    }
                    
                    if($value->getUsersusers() != null){
                        $employee = $value->getUsersusers()->getLogin();
                    } else {
                        $employee = "";
                    }
                    
                    $date = $value->getDateCreate();
                    
                    $dat = array (
                        "id" => $value->getIdorder(),
                        "date" => isset($date) ? $date->format('m/d/Y') : '',
                        "client" => $client,
                        "employee" => $employee,
                        "value" => $value->getTotalAmount(),
                        "info" => ""
                    );
                    array_push($data, $dat);
                }                
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllOrders(){
        try{
            $params = array("active" => 1);

            $orders = getEm()->getRepository('Order')->findBy($params);
            $data = array ();
            foreach ($orders as $value){
                if($value->getPersonperson() != null){
                    $client = $value->getPersonperson()->getName();
                } else {
                    $client = "";
                }
                
                if($value->getUsersusers() != null){
                    $employee = $value->getUsersusers()->getLogin();
                } else {
                    $employee = "";
                }
                
                $dat = array (
                    "id" => $value->getIdorder(),
                    "date" => $value->getDateCreate(),
                    "client" => $client,
                    "employee" => $employee,
                    "value" => number_format($value->getTotalAmount(),2,",","."),
                    "info" => "",
                    "link" => "<span class='lnr lnr-eye open-details' target_order='".$value->getIdorder()."'></span> <a href='".URL_BASE."terminal/terminal/index/".$value->getIdorder()."'><span class='lnr lnr-arrow-right'></span></a>"
                );
                array_push($data, $dat);                             
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function vouchers() {
        $navbar = "Reports|Order";
        $array_answer = array("");
        GenericController::template("Reports", "order", "vouchers", $navbar, $array_answer, 137);
    }
    
    public function mobiles() {
        $navbar = "Reports|Order";
        $array_answer = array("");
        GenericController::template("Reports", "order", "mobiles", $navbar, $array_answer, 144);
    }

    public function history() {
        $navbar = "Reports|History";
        $array_answer = array("");
        GenericController::template("Reports", "order", "history", $navbar, $array_answer, 144);
    }

    public function getOrder() {      
        if (isset($_REQUEST['id'])) {
            try {
                $order = getEm()->getRepository('Order')->findBy(array("idorder" => $_REQUEST['id'], "active" => '1'));
                $mysqlData = array();
                foreach ($order as $value) {
                    $em = getEm();
                    $qb =  $em->getRepository('OrderPayment')
                        ->createQueryBuilder('op')
                        ->andWhere('op.orderorder = :idorder')
                        ->select('pm.name', 'op.value')
                        ->join('op.paymentMtdpaymentMtd', 'pm')
                        ->setParameter('idorder', $value->getIdorder())
                        ->getQuery()
                        ->getResult();
                    $payments = $qb;       

                    $em2 = getEm();
                    $qb2 =  $em2->getRepository('OrderLine')
                        ->createQueryBuilder('ol')
                        ->andWhere('ol.orderorder = :idorder')
                        ->select('p.name', 'ol.value, ol.qty, ol.discount, c.name as color, s.name as size, ol.extraInfo')
                        ->join('ol.productproduct', 'p')
                        ->leftJoin('ol.sizecolorsizecolor', 'sc')
                        ->leftJoin('sc.sizesize', 's')
                        ->leftJoin('sc.colorcolor', 'c')
                        ->setParameter('idorder', $value->getIdorder())
                        ->getQuery()
                        ->getResult();
                    $products = $qb2;                                  

                    $dat = array(
                        "order_identify" => $value->getIdorder(),
                        "value" => number_format($value->getTotalAmount(),2,",","."),
                        "value_paid" => number_format($value->getTotalPaid(),2,",","."),
                        "value_remaining" => number_format($value->getRemainingValue(),2,",","."),
                        "payments" => @$payments,
                        "products" => $products,
                        "customer" => @$value->getPersonperson(),
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";            
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

}

?>