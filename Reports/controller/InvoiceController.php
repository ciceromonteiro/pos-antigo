<?php

class InvoiceController {

    public function index() {
        $navbar = "Reports|Invoice";
        $array_answer = array("");
        GenericController::template("Reports", "invoice", "index", $navbar, $array_answer, 169);
    }

    public function printedOrShipped() {
        $navbar = "Reports|Invoice";
        $array_answer = array("");
        GenericController::template("Reports", "invoice", "printedOrShipped", $navbar, $array_answer, 169);
    }

    public function dailyInvoiceClosure() {
        $navbar = "Reports|Invoice";
        $array_answer = array("");
        GenericController::template("Reports", "invoice", "dailyInvoiceClosure", $navbar, $array_answer, 175);
    }

}

?>