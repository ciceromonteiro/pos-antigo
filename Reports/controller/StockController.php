<?php

class StockController {

    public function index() {
        $navbar = "Reports|Stock";
        $array_answer = array("");
        GenericController::template("Reports", "stock", "index", $navbar, $array_answer, 219);
    }

    public function receiptOfPurchaseOrder() {
        $navbar = "Reports|Stock";
        $array_answer = array("");
        GenericController::template("Reports", "stock", "receiptOfPurchaseOrder", $navbar, $array_answer, 117);
    }

    public function suggestionsForBuying() {
        $navbar = "Reports|Stock";
        $array_answer = array("");
        GenericController::template("Reports", "stock", "suggestionsForBuying", $navbar, $array_answer, 122);
    }

    public function inventory() {
        $navbar = "Reports|Stock";
        $array_answer = array("");
        GenericController::template("Reports", "stock", "inventory", $navbar, $array_answer, 126);
    }

    public function stockMovement() {
        $navbar = "Reports|Stock";
        $array_answer = array("");
        GenericController::template("Reports", "stock", "inventoryMovement", $navbar, $array_answer, 133);
    }

    public function getAllAcquisitions() {
        try {
            $products = getEm()->getRepository('Product')->findBy(array("active" => 1));
            $data = array();
            foreach ($products as $value) {
                $aux1 = getEm()->getRepository('ProductInSection')->findBy(array("productproduct" => $value->getIdproduct()));
                foreach ($aux1 as $value1) {
                    $aux2 = getEm()->getRepository('Section')->findBy(array("idsection" => $value1->getProductproduct()));
                    foreach ($aux2 as $value2) {
                        $aux3 = getEm()->getRepository('Shelf')->findBy(array("idshelf" => $value2->getShelfshelf()));
                        foreach ($aux3 as $value3) {
                            $aux4 = getEm()->getRepository('Wardrobe')->findBy(array("idwardrobe" => $value3->getWardrobewardrobe()));
                            foreach ($aux4 as $value4) {
                                $aux5 = getEm()->getRepository('Hall')->findBy(array("idhall" => $value4->getHallIdhall()));
                                foreach ($aux5 as $value5) {
                                    $aux6 = getEm()->getRepository('Warehouse')->findBy(array("idwarehouse" => $value5->getIdwarehouse()));

                                    $dat = array(
                                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproduct() . '">',
                                        "id" => $value->getIdproduct(),
                                        "name" => $value->getName(),
                                        "qty" => $value->getStockQty(),
                                        "salePrice" => $value->getPrice(),
                                        "totalPrice" => $value->getPrice()
                                    );
                                    array_push($data, $dat);
                                }
                            }
                        }
                    }
                }
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllStockMovement() {
        try {
            $products = getEm()->getRepository('Product')->findBy(array("active" => 1));
            $data = array();
            foreach ($products as $value) {
                $aux1 = getEm()->getRepository('ProductInSection')->findBy(array("productproduct" => $value->getIdproduct()));
                foreach ($aux1 as $value1) {
                    $aux2 = getEm()->getRepository('Section')->findBy(array("idsection" => $value1->getProductproduct()));
                    foreach ($aux2 as $value2) {
                        $aux3 = getEm()->getRepository('Shelf')->findBy(array("idshelf" => $value2->getShelfshelf()));
                        foreach ($aux3 as $value3) {
                            $aux4 = getEm()->getRepository('Wardrobe')->findBy(array("idwardrobe" => $value3->getWardrobewardrobe()));
                            foreach ($aux4 as $value4) {
                                $aux5 = getEm()->getRepository('Hall')->findBy(array("idhall" => $value4->getHallIdhall()));
                                foreach ($aux5 as $value5) {
                                    $aux6 = getEm()->getRepository('Warehouse')->findBy(array("idwarehouse" => $value5->getIdwarehouse()));
                                    foreach ($aux6 as $value6) {
                                        $aux7 = $value6->getName();

                                    $dat = array(
                                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproduct() . '">',
                                        "id" => $value->getIdproduct(),
                                        "name" => $value->getName(),
                                        "qty" => $value->getStockQty(),
                                        "salePrice" => $value->getPrice(),
                                        "totalPrice" => $value->getPrice(),
                                        "location" => $value6->getName()
                                    );
                                    array_push($data, $dat);
                                    }
                                    
                                    }
                            }
                        }
                    }
                }
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }    
    
}

?>