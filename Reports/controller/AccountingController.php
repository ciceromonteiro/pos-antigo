<?php

class AccountingController {

    public function index() {
        $navbar = "Reports|Accounting";
        $array_answer = array("");
        GenericController::template("Reports", "accounting", "index", $navbar, $array_answer, 351);
    }
    
    public function getAllClosingTheCarton($from = null, $to = null){
        try{
            if($from != null && $from != ''){
                $from = $from." 00:00:00";
            } else {
                $from = date('Y-m-d')." 00:00:00";
            }
            
            if($to != null && $to != ''){
                $to = $to." 23:59:59";
            } else {
                $to = date('Y-m-d')." 23:59:59";
            }
            
            $con = getEm()->getConnection();
            $database = $con->getParams()['dbname'];
            $sql = "SELECT * FROM pos_closure WHERE date BETWEEN '$from' AND '$to'";
            $result = $con->prepare($sql);
            $result->execute();
            $arrayResult = $result->fetchAll(\PDO::FETCH_ASSOC);   
            $data = array();
            foreach ($arrayResult as $value){
                $user = getEm()->getRepository('Users')->findOneBy(array('idusers' => $value['user_iduser']));
                
                $cashValue = $value['amount_received'] + $value['amount_counted'] + $value['amount_credit_card'];
                $cashDifference = $cashValue - ($value['amount_received'] + $value['amount_credit_card']);
                
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value['idpos_closure'].'>',
                    "id" => $value['idpos_closure'],
                    "protocol" => $value['protocol'],
                    "closing" => $value['date_create'],
                    "cashCloseDate" => $value['date'],
                    "employee" => $user->getLogin(),
                    "cashValue" => number_format($cashValue,2,",","."),
                    "cashDifference" => number_format($cashDifference,2,",",".")
                );
                array_push($data, $dat);            
            }
            
            unset($con, $conn);
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

}

?>