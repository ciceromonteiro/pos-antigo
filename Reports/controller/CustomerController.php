<?php
use Doctrine\ORM\Query\ResultSetMapping;

class CustomerController {

    public function index() {
        $navbar = "Reports|Customer";
        $array_answer = array("");
        GenericController::template("Reports", "customer", "index", $navbar, $array_answer, 104);
    }

    public function getAllClient() {
        // try {
        //     $CustomerGp = getEm()->getRepository('CustomerGp')->findBy(array("active" => 1));
        //     $data = array();
        //     foreach ($CustomerGp as $value) {
        //         $dat = array(
        //             "id" => '<a data-id="' . $value->getIdcustomerGp() . '" data-name="' . $value->getName() . '">' . $value->getIdcustomerGp() . '</a>',
        //             "name" => $value->getName()
        //         );
        //         array_push($data, $dat);
        //     }
        //     $result = "success";
        //     $message = "query success";
        //     $mysqlData = $data;
        // } catch (Exception $e) {
        //     $result = "error";
        //     $message = $e->getMessage();
        //     $mysqlData = "";
        // }

        // $data = array(
        //     "result" => $result,
        //     "message" => $message,
        //     "data" => $mysqlData
        // );

        // $json_data = json_encode($data);
        // print $json_data;

        header('Content-type: application/json');
        $em = getEm();
        // $output_file = __DIR__."/output.txt";
        // file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $rsm = new ResultSetMapping;

        $sSearch = $_GET['search']['value'];

        try {
            $repository = $em->getRepository('Person');
            $query = $repository->createQueryBuilder('p')
                ->select(['p.idperson', 'p.name', 'p.documentNumber', 'p.birthDate'])
                ->where('p.name LIKE :sSearch')
                ->orWhere('p.documentNumber LIKE :sSearch')
                ->setParameter('sSearch', '%'.$sSearch.'%')
                ->getQuery();
        } catch(Exception $e) {
            var_dump($e);
        }

        $result = [
            'draw' => $_GET['draw']
        ];

        if ($customers = $query->getResult(2)) {
            $customers_values = array_map(function($a) {
                $a['birthDate'] = is_null($a['birthDate']) ? '' : $a['birthDate']->format('d/m/Y');
                return $a;
            }, $customers);
            $customers_values = array_map('array_values', $customers_values);
            $result['data'] = array_values($customers_values);
        }
        else {
            $result['data'] = [];
        }

        $result['recordsTotal'] = count($customers);
        $result['recordsFiltered'] = count($customers);

        echo json_encode($result, JSON_PRETTY_PRINT);
        return;
    }

}

?>