<?php 

class SupplierController {
    public function index(){       
        $navbar = "Reports|Supplier";
        $array_answer = array ("");
        GenericController::template("Reports", "supplier","index", $navbar, $array_answer, 106);
    }
        
    public static function getAll(){
        try{
            $supplier = getEm()->getRepository('Supplier')->findBy(array ('active' => 1));
            $data = array ();
            foreach ($supplier as $value){                
                $dat = array (
                    "id" => $value->getIdsupplier(),
                    "name" => $value->getCompanycompany()->getName(),
                    "address" => "test",
                    "zip" => "test",
                    "city" => "test",
                    "phone" => "test",
                    "email" => "test",
                    "contact" => "test",
                    "phoneContact" => "test"
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
}

?>