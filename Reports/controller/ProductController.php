<?php
/**
 * @author Claudio da Cruz Silva Junior>
 * @version 1.0.0
 * @abstract file created in date september 26, 2017
 */
class ProductController {

    public function index() {
        $navbar = "Reports|Product";
        $array_answer = array("");
        GenericController::template("Reports", "products", "index", $navbar, $array_answer, 83);
    }

    public function getAllProducts() {
        try {
            $data = array();
            $product = getEm()->getRepository('Product')->findAll();
            foreach ($product as $value) {
                
                $id = is_null($value->getIdproduct()) ? '' : $value->getIdproduct();

                if ($value->getName() == null) {
                    $name = '';
                } else {
                    $name = $value->getName();
                }

                if ($value->getPricePurchase() == null) {
                    $purchasePrice = '0.00';
                } else {
                    $purchasePrice = $value->getPricePurchase();
                }

                if ($value->getPrice() == null) {
                    $salePrice = '0.00';
                } else {
                    $salePrice = $value->getPrice();
                }

                if ($value->getNumberPack() == null) {
                    $numberPack = "";
                } else {
                    $numberPack = $value->getNumberPack();
                }

                if ($value->getTaxGp() == null) {
                    $groupTax = "";
                } else {
                    $groupTax = $value->getTaxGp()->getName();
                }

                if ($value->getDateUpdate() == null) {
                    $update = "";
                } else {
                    $update = $value->getDateUpdate()->format('m/d/Y');
                }

                if ($value->getActive() == 1) {
                    $active = "Active";
                } else if ($value->getActive() == 2) {
                    $active = "Blocked";
                } else if ($value->getActive() == 3) {
                    $active = "Deleted";
                } else {
                    $active = "Temporarily saved";
                }

                $groupProductList = "";
                $groupProduct = getEm()->getRepository('ProductProductGp')->findBy(array('idproductProductGp' => $id));
                if (!empty($groupProduct)) {
                    foreach ($groupProduct as $value) {
                        $groupProductList .= $value->getProductGpproductGp()->getName() . ',';
                    }
                }

                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$id.'">',
                    "id" => $id,
                    "name" => $name,
                    "purchasePrice" => number_format($purchasePrice,2,",","."),
                    "salePrice" => number_format($salePrice,2,",","."),
                    "group" => $groupProductList,
                    "numberPack" => $numberPack,
                    "groupTax" => $groupTax,
                    "update" => $update,
                    "status" => $active
                );

                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllProductsVouchers() {
        try {
            $data = array();
            $product = getEm()->getRepository('Product')->findBy(array('typeProduct' => 2));
            foreach ($product as $value) {
                $id = is_null($value->getIdproduct()) ? '' : $value->getIdproduct();

                if ($value->getName() == null) {
                    $name = '';
                } else {
                    $name = $value->getName();
                }

                if ($value->getPricePurchase() == null) {
                    $purchasePrice = '0.00';
                } else {
                    $purchasePrice = $value->getPricePurchase();
                }

                if ($value->getCurrentPrice() == null) {
                    $salePrice = '0.00';
                } else {
                    $salePrice = $value->getCurrentPrice();
                }

                if ($value->getNumberPack() == null) {
                    $numberPack = "";
                } else {
                    $numberPack = $value->getNumberPack();
                }

                if ($value->getTaxGp() == null) {
                    $groupTax = "";
                } else {
                    $groupTax = $value->getTaxGp()->getName();
                }

                if ($value->getDateUpdate() == null) {
                    $update = "";
                } else {
                    $update = $value->getDateUpdate();
                }

                if ($value->getActive() == 1) {
                    $active = "Active";
                } else if ($value->getActive() == 2) {
                    $active = "Blocked";
                } else if ($value->getActive() == 3) {
                    $active = "Deleted";
                } else {
                    $active = "Temporarily saved";
                }

                $groupProductList = "";
                $groupProduct = getEm()->getRepository('ProductProductGp')->findBy(array('idproductProductGp' => $id));
                if (!empty($groupProduct)) {
                    foreach ($groupProduct as $value) {
                        $groupProductList .= $value->getProductGpproductGp()->getName() . ',';
                    }
                }

                $dat = array(
                    "id" => $id,
                    "name" => $name,
                    "purchasePrice" => $purchasePrice,
                    "salePrice" => $salePrice,
                    "group" => $groupProductList,
                    "numberPack" => $numberPack,
                    "groupTax" => $groupTax,
                    "update" => $update,
                    "status" => $active
                );

                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get the datas of the product clicked in reports/product/index
     * @param int $idProduct
     * @return string json_data 
     */
    public function getProduct($idProduct){
         $Product = getEm()->getRepository('Product')->findAll();
         try {
            $data = array();
            if($Product != 0){
                foreach ($Product as $product) {
                    if($product->getIdproduct() == $idProduct){
                        if ($product->getName() == null) {
                        $name = '';
                        } else {
                            $name = $product->getName();
                        }

                        if ($product->getBarcode() == null) {
                            $barCode = 'no data';
                        } else {
                            $barCode = $product->getBarcode();
                        }

                         $qrCode = '../../../libraries/qrCode/php/qr_img.php?';
                         $qrCode .= 'd='.$barCode.'&';
                         $qrCode .= 'e=H&';
                         $qrCode .= 's=4&';
                         $qrCode .= 't=P';

                        require_once('../libraries/barCode/barcode.inc.php');
                        new barCodeGenrator($barCode,1,'../data/codeBar/barcode.gif', 200, 150, true);

                        $dat = array(
                        "id" => $product->getIdproduct(),
                        "name" => $name,
                        "barCode" => $barCode,
                        "qrCode" => $qrCode,
                        "barCodeImage" => '../../../data/codeBar/barcode.gif'

                    );

                    array_push($data, $dat);
                    }
                }
             }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Get the barCode of the product respective
     * @param int $idProduct
     * @return string json_data 
     */
    public function getBarCodeProduct($idProduct){
        $Product = getEm()->getRepository('Product')->findAll();
         try {
            $data = array();
             if($Product != 0){
                $codeBar = null;
                $barcode = null;
                foreach ($Product as $product) {
                    if($product->getIdproduct() == $idProduct){
                        if ($product->getBarcode() == null) {
                            $codeBar = null;
                        } else {
                            $barCode = $product->getBarcode();
                            require_once('../libraries/barCode/barcode.inc.php');
                            new barCodeGenrator($barCode,1,'../data/codeBar/barcode.gif', 200, 150, true);  
                            $codeBar = file_get_contents('../data/codeBar/barcode.gif');
                            $codeBar = base64_encode($codeBar);
                        }
                        
                       
                    }
                }
            }
        $result = "success";
        $message = "query success";
        if($codeBar != null){
            $mysqlData = 'data:image/gif;base64,'.$codeBar;
        }else{
            $mysqlData = 'no data';
        }
        
    }catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * Order the products in the table in order crescent of price purcharse
     * @return string json_data 
     */
    public function orderProductByValue(){
        $Product = getEm()->getRepository('Product')->findBy(array(),array('pricePurchase' => 'ASC'));

         try {
            $data = array();
            foreach ($Product as $value) {
                $id = is_null($value->getIdproduct()) ? '' : $value->getIdproduct();

                if ($value->getName() == null) {
                    $name = '';
                } else {
                    $name = $value->getName();
                }

                if ($value->getPricePurchase() == null) {
                    $purchasePrice = '0.00';
                } else {
                    $purchasePrice = $value->getPricePurchase();
                }

                if ($value->getCurrentPrice() == null) {
                    $salePrice = '0.00';
                } else {
                    $salePrice = $value->getCurrentPrice();
                }

                if ($value->getNumberPack() == null) {
                    $numberPack = "";
                } else {
                    $numberPack = $value->getNumberPack();
                }

                if ($value->getTaxGp() == null) {
                    $groupTax = "";
                } else {
                    $groupTax = $value->getTaxGp()->getName();
                }

                if ($value->getDateUpdate() == null) {
                    $update = "";
                } else {
                    $update = $value->getDateUpdate()->format('m/d/Y');
                }

                if ($value->getActive() == 1) {
                    $active = "Active";
                } else if ($value->getActive() == 2) {
                    $active = "Blocked";
                } else if ($value->getActive() == 3) {
                    $active = "Deleted";
                } else {
                    $active = "Temporarily saved";
                }

                $groupProductList = "";
                $groupProduct = getEm()->getRepository('ProductProductGp')->findBy(array('idproductProductGp' => $id));
                if (!empty($groupProduct)) {
                    foreach ($groupProduct as $value) {
                        $groupProductList .= $value->getProductGpproductGp()->getName() . ',';
                    }
                }

                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$id.'">',
                    "id" => $id,
                    "name" => $name,
                    "purchasePrice" => number_format($purchasePrice,2,",","."),
                    "salePrice" => number_format($salePrice,2,",","."),
                    "group" => $groupProductList,
                    "numberPack" => $numberPack,
                    "groupTax" => $groupTax,
                    "update" => $update,
                    "status" => $active
                );

                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;

    }
    /**
     * Get quantity of product in stock
     * @param int $idProduct
     * @return string json_data 
     */
    public function getProductAroundQuantity($idProduct){
     $Product = getEm()->getRepository('Product')->findAll();
         try {
            $data = array();
             if($Product != 0){
                foreach ($Product as $product) {
                    if($product->getIdproduct() == $idProduct){
                        $qty2 = 0;
                        $qtyStock = getEm()->getRepository('ProductInSection')->findBy(array('productproduct' => $product->getIdproduct()));
                        if(!empty($qtyStock)){
                            $qty = 0;
                            foreach ($qtyStock as $value2) {
                                $qty2 += $qty + $value2->getQty();
                            }
                        }

                    }
                 }
             }
            $result = "success";
            $message = "query success";
            $mysqlData = $qty2;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        //var_dump($data);
        $json_data = json_encode($data);
        print $json_data;
    }
    
    /**
     * Generate barCode of product with barCode equal NULL
     * @param int $idProduct
     * @return string json_data 
     */
    public function generateBarCode($idProduct){
        try{
            $barCode = time();
            $Product = getEm()->getRepository('Product')->findBy(array('idproduct' => $idProduct));
            if($Product != null){
                $Product[0]->setBarcode($barCode);
                getEm()->persist($Product[0]);
                getEm()->flush();
            }
            

            $result = "success";
            $message = "query success";
            $mysqlData = "";
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data; 


    }
    public function getAllPrinters(){
        $Printers = getEm()->getRepository('Printer')->findBy(array('active' => 1));
        $PrinterType = getEm()->getRepository('PrinterType')->findBy(array('active' => 1));
        $Pos = getEm()->getRepository('Pos')->findBy(array('active' => 1));

        $namePrinterType = null;
        $namePos = null;
        try{
            $data = array();
            if($Printers != null){
                foreach ($Printers as $printer) {
                     if($PrinterType != null && $printer->getIdTypePrinter() != null){
                        $namePrinterType = null;
                        foreach ($PrinterType as $type) {
                            if($printer->getIdTypePrinter()->getIdprintertype() == $type->getIdprintertype()){
                                $namePrinterType = $type->getName();
                            }
                        }
                     }
                     if($Pos != null && $printer->getInstalledIn() != null){
                        $namePos = null;
                        foreach ($Pos as $pos) {
                            if($printer->getInstalledIn()->getIdpos() == $pos->getIdpos()){
                                $namePos = $pos->getNickname();
                            }
                        }

                     }


                if($namePrinterType == null){   
                    $namePrinterType = "";
                }
                if($namePos == null){   
                    $namePos = "";
                }
                $date = $printer->getDateCreate()->format('d/m/Y');

                 $dat = array(
                "checkbox" => '<input type="checkbox" data-id="'.$printer->getIdprinter().'">',
                "id" => $printer->getIdprinter(),
                "instaledIn" => $namePos,
                "type" => $namePrinterType,
                "port" => $printer->getPort(),
                "dateCreate" => $date,
                "active" => $printer->getActive()
                );

                array_push($data, $dat);
                }
            }

            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }



    public function vouchers() {
        $navbar = "Reports|Product";
        $product = getEm()->getRepository('Product')->findBy(array('typeProduct' => 2));
        $array_answer = array(
            "product_voucher" => $product
        );
        GenericController::template("Reports", "products", "vouchers", $navbar, $array_answer, 87);
    }

    public function default_alerts() {
        $navbar = "Reports|Product";
        $array_answer = array("");
        GenericController::template("Reports", "products", "default_alerts", $navbar, $array_answer, 96);
    }
    
    public function getAllWarnings(){
        try {
            $data = array();
            $warningsInCaseOfDelay = getEm()->getRepository('WarningsInCaseOfDelay')->findBy(array('active' => 1));
            foreach ($warningsInCaseOfDelay as $value) {
                if($value->getSendCopy() != 1){
                    $sendCopy = "";
                } else {
                    $sendCopy = "<span class='glyphicon glyphicon-ok'></span>";
                }
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdWarningsInCaseOfDelay().'">',
                    "id" => $value->getIdWarningsInCaseOfDelay(),
                    "sendAfter" => $value->getSendAfter(),
                    "messageTitle" => $value->getMessageTitle(),
                    "message" => $value->getMessage(),
                    "sendCopy" => $sendCopy
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function insertWarning(){
        if(isset($_POST['sendAfter']) && isset($_POST['messageTitle']) && isset($_POST['message'])){
            try {
                $warning = new WarningsInCaseOfDelay();
                if(isset($_POST['sendCopy'])){
                    $warning->setSendCopy(1);
                }
                $warning->setSendAfter($_POST['sendAfter']);
                $warning->setMessageTitle($_POST['messageTitle']);
                $warning->setMessage($_POST['message']);
                $warning->setDateCreate(new Datetime());
                $warning->setActive(1);
                getEm()->persist($warning);
                getEm()->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data
            );

            $json_data = json_encode($data);
            print $json_data;            
        }
        
    }
    
    public function getWarning(){
        if(isset($_POST['id'])){
            try {
                $warning = getEm()->getRepository('WarningsInCaseOfDelay')->findBy(array("idWarningsInCaseOfDelay" => $_POST['id']));
                $array_data = array ();
                foreach ($warning as $value){
                    $dat = array (
                        "id" => $value->getIdWarningsInCaseOfDelay(),
                        "sendAfter" => $value->getSendAfter(),
                        "messageTitle" => $value->getMessageTitle(),
                        "message" => $value->getMessage(),
                        "sendCopy" => $value->getSendCopy()
                    );
                    array_push($array_data, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;        
    }
    
    public function updateWarning(){
        if(isset($_POST['sendAfter']) && isset($_POST['messageTitle']) && isset($_POST['message']) && isset($_POST['idWarning'])){
            try {
                $warning = getEm()->getRepository('WarningsInCaseOfDelay')->findBy(array('idWarningsInCaseOfDelay' => $_POST['idWarning']));
                if(isset($_POST['sendCopy'])){
                    $warning[0]->setSendCopy(1);
                } else {
                    $warning[0]->setSendCopy(null);
                }
                $warning[0]->setSendAfter($_POST['sendAfter']);
                $warning[0]->setMessageTitle($_POST['messageTitle']);
                $warning[0]->setMessage($_POST['message']);
                $warning[0]->setDateUpdate(new Datetime());
                $warning[0]->setActive(1);
                getEm()->persist($warning[0]);
                getEm()->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data
            );

            $json_data = json_encode($data);
            print $json_data;            
        }
    }
    
    public function removeWarning(){
        if($_POST['idWarning']){
            try{
                $ids = $_POST['idWarning'];
                for($i=0; $i < count($ids); $i++){
                    $warning = getEm()->getRepository("WarningsInCaseOfDelay")->findBy(array("idWarningsInCaseOfDelay" => $ids[$i]));
                    $warning[0]->setActive('3');
                    $warning[0]->setDateDelete(new DateTime());
                    getEm()->persist($warning[0]);
                    getEm()->flush();
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function getAllReport(){
        try {
            $data = array();
            $report = getEm()->getRepository('ReportCreditManager')->findBy(array('active' => 1));
            foreach ($report as $value) {
                if($value->getSendCopy() != 1){
                    $sendCopy = "";
                } else {
                    $sendCopy = "<span class='glyphicon glyphicon-ok'></span>";
                }
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdReportCreditManager().'">',
                    "id" => $value->getIdReportCreditManager(),
                    "sendAfter" => $value->getSendAfter(),
                    "messageTitle" => $value->getMessageTitle(),
                    "message" => $value->getMessage(),
                    "sendCopy" => $sendCopy
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function insertReport(){
        if(isset($_POST['sendAfter']) && isset($_POST['messageTitle']) && isset($_POST['message'])){
            try {
                $report = new ReportCreditManager();
                if(isset($_POST['sendCopy'])){
                    $report->setSendCopy(1);
                }
                $report->setSendAfter($_POST['sendAfter']);
                $report->setMessageTitle($_POST['messageTitle']);
                $report->setMessage($_POST['message']);
                $report->setDateCreate(new Datetime());
                $report->setActive(1);
                getEm()->persist($report);
                getEm()->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data
            );

            $json_data = json_encode($data);
            print $json_data;            
        }
        
    }
    
    public function getReport(){
        if(isset($_POST['id'])){
            try {
                $report = getEm()->getRepository('ReportCreditManager')->findBy(array("idReportCreditManager" => $_POST['id']));
                $array_data = array ();
                foreach ($report as $value){
                    $dat = array (
                        "id" => $value->getIdReportCreditManager(),
                        "sendAfter" => $value->getSendAfter(),
                        "messageTitle" => $value->getMessageTitle(),
                        "message" => $value->getMessage(),
                        "sendCopy" => $value->getSendCopy()
                    );
                    array_push($array_data, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;        
    }
    
    public function updateReport(){
        if(isset($_POST['sendAfter']) && isset($_POST['messageTitle']) && isset($_POST['message']) && isset($_POST['idReport'])){
            try {
                $report = getEm()->getRepository('ReportCreditManager')->findBy(array('idReportCreditManager' => $_POST['idReport']));
                if(isset($_POST['sendCopy'])){
                    $report[0]->setSendCopy(1);
                } else {
                    $report[0]->setSendCopy(null);
                }
                $report[0]->setSendAfter($_POST['sendAfter']);
                $report[0]->setMessageTitle($_POST['messageTitle']);
                $report[0]->setMessage($_POST['message']);
                $report[0]->setDateUpdate(new Datetime());
                $report[0]->setActive(1);
                getEm()->persist($report[0]);
                getEm()->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data
            );

            $json_data = json_encode($data);
            print $json_data;            
        }
    }
    
    public function removeReport(){
        if($_POST['idReport']){
            try{
                $ids = $_POST['idReport'];
                for($i=0; $i < count($ids); $i++){
                    $report = getEm()->getRepository("ReportCreditManager")->findBy(array("idReportCreditManager" => $ids[$i]));
                    $report[0]->setActive('3');
                    $report[0]->setDateDelete(new DateTime());
                    getEm()->persist($report[0]);
                    getEm()->flush();
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    
}

?>