<?php

class LogController {

    public function index() {
        $navbar = "Reports|Log";
        $array_answer = array("");
        GenericController::template("Reports", "log", "index", $navbar, $array_answer, 177);
    }
    
    public function getAllLogs(){
        try{
            $logs = getEm()->getRepository('Log')->findAll();
            $data = array ();
            foreach ($logs as $value){
                $dat = array (
                    "id" => $value->getIdlog(),
                    "date" => $value->getDateCreate(),
                    "action" => $value->getInfo()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

}

?>