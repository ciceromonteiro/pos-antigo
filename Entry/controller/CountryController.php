<?php
/**
 * Created by PhpStorm.
 * User: rocha
 * Date: 11/1/16
 * Time: 11:10 AM
 */

class CountryController {

    public function __contruct(){

    }

    public static function index(){
        $navbar = "Entry|country";
        $array_answer = "";
        GenericController::template("Entry", "country","index", $navbar, $array_answer, 239);
    }

    public function getAll(){
        try{
            $entityManager = getEm();
            $arrayFilter = array(
                "active" => 1
            );
            $country = $entityManager->getRepository('ZzCountry')->findBy($arrayFilter);
            $data = array ();
            foreach ($country as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdzzCountry().'" data-name="'.$value->getName().'">',
                    "id" => $value->getIdzzCountry(),
                    "name" => $value->getName(),
                    "code" => $value->getCode()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;

    }

    public function insert(){

        if(isset($_POST['nameCountry'])){
            try {
                $country = new ZzCountry();
                $country->setName($_POST['nameCountry']);
                $country->setCode($_POST['codeCountry']);
                $country->setDateCreate(new DateTime());
                $country->setActive('1');
                getEm()->persist($country);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'country', $_POST['nameCountry']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getCountry(){
        if(isset($_POST['id'])){
            try {
                $country = getEm()->getRepository('ZzCountry')->findBy(array("idzzCountry" => $_POST['id']));
                $mysqlData = array ();
                foreach ($country as $value){
                    $dat = array (
                        "id" => $value->getIdzzCountry(),
                        "name" => $value->getName(),
                        "code" => $value->getCode(),
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function update(){
        if(isset($_POST['nameCountry']) && isset($_POST['idCountry'])){
            try {
                $country = getEm()->getRepository('ZzCountry')->findBy(array("idzzCountry" => $_POST['idCountry']));
                $country[0]->setName($_POST['nameCountry']);
                $country[0]->setCode($_POST['codeCountry']);
                $country[0]->setDateUpdate(new datetime());
                $country[0]->setActive(true);
                getEm()->persist($country[0]);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'country', $_POST['nameCountry']);

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function delete(){
        if($_POST['idCountrys']){
            try{
                $ids = $_POST['idCountrys'];
                for($i=0; $i < count($ids); $i++){
                    $country = getEm()->getRepository("ZzCountry")->findOneBy(array("idzzCountry" => $ids[$i]));
                    $country->setActive('3');
                    $country->setDateDelete(new DateTime());
                    getEm()->persist($country);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'country', $country->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
}