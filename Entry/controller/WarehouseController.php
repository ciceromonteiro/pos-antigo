<?php

/**
 * @author Paulo Egito <pvegito@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Oct 24, 2016
 */

require_once '../init_autoload.php';

class WarehouseController {

    public function __construct() {
        
    }

    public function createRollBackList(){
        $warehouse_id = $_POST['warehouse_id'];

        $halls = $this->getHall($warehouse_id);
        $wardrobes = $this->getWardrobe($halls);
        $shelfs = $this->getShelf($wardrobes);
        $sections = $this->getSection($shelfs);

        $fp = fopen("warehouse_rollback_list.txt","a+");
        $phrase = "datetime_".date("U")."\n";

        if($halls){
            foreach ($halls as $hall){
                $phrase .= "hall_".$hall['id']."_".$hall['warehouse_id']."_".$hall['position_top']."_".$hall['position_left']."_".$hall['height']."_".$hall['width']."\n";
            }
        }
        if($wardrobes){
            foreach ($wardrobes as $wardrobe){
                $phrase .= "wardrobe_".$wardrobe['id']."_".$wardrobe['hall_id']."_".$wardrobe['position_top']."_".$wardrobe['position_left']."_".$wardrobe['height']."_".$wardrobe['width']."\n";
            }
        }
        if($shelfs){
            foreach ($shelfs as $shelf){
                $phrase .= "shelf_".$shelf['id']."_".$shelf['wardrobe_id']."_".$shelf['position_top']."\n";
            }
        }
        if($sections){
            foreach ($sections as $section){
                $phrase .= "section_".$section['id']."_".$section['shelf_id']."_".$section['position_left']."\n";
            }
        }

        $writes = fwrite($fp, $phrase);
        fclose($fp);
    }
    public function doRollBack(){
        //first we must delete all the elements created after the rollback list was created.
        $fp = file("warehouse_roolback_list.txt");
        $datetime = explode("_",$fp[0]);
        $em = getEm();
        //$hall_del = getEm()->getRepository("Hall")->findBy(array('active' => 1, 'created_at' => $datetime))->where('created_at > "$datetime" ');
        

        //second for HALLS
        $fp = file("warehouse_roolback_list.txt");
        foreach($fp as $line){
            $element = explode("_",$line);

            if($element[0]=="hall"){
                $em = getEm();
                $hall = getEm()->getRepository("Hall")->findBy(array('active' => 1, 'idhall' => $element[1]));

                if($hall){
                    $this->editHall("",$element[3],$element[4],$element[5],$element[6],1,$element[1]);
                }
            }
        }

        //third for WARDROBES
        $fp = file("warehouse_roolback_list.txt");
        foreach($fp as $line){
            $element = explode("_",$line);

            if($element[0]=="wardrobe"){
                $em = getEm();
                $wardrobe = getEm()->getRepository("Wardrobe")->findBy(array('active' => 1, 'idwardrobe' => $element[1]));

                if($wardrobe){
                    $this->editWardrobe("",$element[2],$element[3],$element[4],$element[5],$element[6],1,$element[1]);
                }
            }
        }

        //fourth for SHELVES
        $fp = file("warehouse_roolback_list.txt");
        foreach($fp as $line){
            $element = explode("_",$line);

            if($element[0]=="shelf"){
                $em = getEm();
                $shelf = getEm()->getRepository("Shelf")->findBy(array('active' => 1, 'idshelf' => $element[1]));

                if($shelf){
                    $this->editShelf("",$element[2],$element[3],1,$element[1]);
                }
            }
        }

        //last for SECTIONS
        $fp = file("warehouse_roolback_list.txt");
        foreach($fp as $line){
            $element = explode("_",$line);

            if($element[0]=="section"){
                $em = getEm();
                $section = getEm()->getRepository("Section")->findBy(array('active' => 1, 'idsection' => $element[1]));

                if($section){
                    $this->editSection("",$element[2],$element[3],1,$element[1]);
                }
            }
        }

        $fp = fopen("warehouse_roolback_list.txt","w+");
        $writes = fwrite($fp, "");
        fclose($fp);
    }

    public function getWarehouse() {
        $em = getEm();
        
        $warehouses = getEm()->getRepository("Warehouse")->findBy(array('active' => 1));

        $data = array ();

        foreach ($warehouses as $value){
            $dat = array (
                "id" => $value->getIdwarehouse(),
                "name" => $value->getName()
            );
            array_push($data, $dat);
        }

        return $data;

    }
    public function getHall($warehouse_id) {
        $em = getEm();
        
        $halls = getEm()->getRepository("Hall")->findBy(array('active' => 1, 'warehousewarehouse' => $warehouse_id));

        $data = array ();

        if($halls){
            foreach ($halls as $value){
                $dat = array (
                    "id" => $value->getIdhall(),
                    "warehouse_id" => $value->getIdwarehouse(),
                    "position_top" => $value->getPosition_top(),
                    "position_left" => $value->getPosition_left(),
                    "height" => $value->getHeight(),
                    "width" => $value->getWidth()
                );
                array_push($data, $dat);
            }
        }else{
            $halls = array();
        }

        return $data;

    }
    public function addHall($name, $position_top, $position_left, $height, $width, $active, $warehousewarehouse) {
        $hall = new Hall;

        $hall->setName($name);

        $warehouse = getEm()->getRepository("Warehouse")->findBy(array('idwarehouse' => $warehousewarehouse));
        $hall->setIdwarehouse($warehouse[0]);

        $hall->setPosition_top($position_top);

        $hall->setPosition_left($position_left);

        $hall->setHeight($height);

        $hall->setWidth($width);

        $hall->setActive($active);


        getEm()->persist($hall);

        getEm()->flush();

        return $hall->getIdhall();
    }
    public function editHall($name, $position_top, $position_left, $height, $width, $active, $id) {
        if($hall = getEm()->find('Hall', $id)){

            $hall->setName($name);

            $hall->setPosition_top($position_top);

            $hall->setPosition_left($position_left);

            $hall->setHeight($height);

            $hall->setWidth($width);

            $hall->setActive($active);


            getEm()->persist($hall);

            getEm()->flush();
        }
    }
    public function deleteHall($id) {
        if($hall = getEm()->find('Hall', $id)){
            $hall->setActive(0);

            getEm()->persist($hall);

            getEm()->flush();
        }
    }

    public function getWardrobe($halls) {
        $em = getEm();

        $data = array ();


        if($halls){
            foreach ($halls as $hall){
                $wardrobes = getEm()->getRepository("Wardrobe")->findBy(array('active' => 1, 'hallIdhall' => $hall['id']));

                if($wardrobes){
                    foreach ($wardrobes as $value){
                        $dat = array (
                            "id" => $value->getIdwardrobe(),
                            "hall_id" => $hall['id'],
                            "position_top" => $value->getPositionTop(),
                            "position_left" => $value->getPositionLeft(),
                            "height" => $value->getHeight(),
                            "width" => $value->getWidth()
                        );
                        array_push($data, $dat);
                    }
                }
            }
        }else{
            $halls = array();
        }

        return $data;

    }

    public function addWardrobe($name, $hall_id, $position_top, $position_left, $height, $width, $active) {
        $wardrobe = new Wardrobe;

        $wardrobe->setName($name);
        
        $hall = getEm()->getRepository("Hall")->findBy(array('idhall' => $hall_id));

        $wardrobe->setHall($hall[0]);

        $wardrobe->setPositionTop($position_top);

        $wardrobe->setPositionLeft($position_left);

        $wardrobe->setHeight($height);

        $wardrobe->setWidth($width);

        $wardrobe->setActive($active);

        getEm()->persist($wardrobe);

        getEm()->flush();


        //always a wardrobe is created, it is automatically created a shelf and a section
        $idshelf_and_idsection = $this->addShelf("",$wardrobe->getIdwardrobe(),372,1,1); //390 is the wardrobe height

        $return_array[] = $wardrobe->getIdwardrobe();
        $return_array[] = $idshelf_and_idsection[0];
        $return_array[] = $idshelf_and_idsection[1];

        return $return_array;
    }

    public function editWardrobe($name, $hall_id, $position_top, $position_left, $height, $width, $active, $id) {
        if($wardrobe = getEm()->find('Wardrobe', $id)){

            if($active){
                $wardrobe->setName($name);

                if($hall_id!=0){
                    $hall = getEm()->getRepository("Hall")->findBy(array('idhall' => $hall_id));
                    $wardrobe->setHall($hall[0]);
                }

                $wardrobe->setPositionTop($position_top);

                $wardrobe->setPositionLeft($position_left);

                $wardrobe->setHeight($height);

                $wardrobe->setWidth($width);
            }
            $wardrobe->setActive($active);

            getEm()->persist($wardrobe);

            getEm()->flush();
        }
    }
    public function deleteWardrobe($id) {
        if($wardrobe = getEm()->find('Wardrobe', $id)){
            $wardrobe->setActive(0);

            getEm()->persist($wardrobe);

            getEm()->flush();
        }
    }

    public function getShelf($wardrobes){
        $em = getEm();

        $data = array ();

        if($wardrobes){
            foreach ($wardrobes as $wardrobe){
                $shelfs = getEm()->getRepository("Shelf")->findBy(array('active' => 1, 'wardrobewardrobe' => $wardrobe['id']));

                if($shelfs){
                    foreach ($shelfs as $value){
                        $dat = array (
                            "id" => $value->getIdshelf(),
                            "wardrobe_id" => $wardrobe['id'],
                            "position_top" => $value->getPositionTop()
                        );
                        array_push($data, $dat);
                    }
                }
            }
        }else{
            $shelfs = array();
        }

        return $data;
    }
    public function addShelf($name, $wardrobe_id, $position_top, $active, $fixed) {
        $shelf = new Shelf;

        $shelf->setName($name);
        
        $wardrobe = getEm()->getRepository("Wardrobe")->findBy(array('idwardrobe' => $wardrobe_id));

        $shelf->setWardrobe($wardrobe[0]);

        $shelf->setPositionTop($position_top);

        $shelf->setActive($active);

        $shelf->setFixed($fixed);

        getEm()->persist($shelf);

        getEm()->flush();

        //whenever a Shelf is created, it is created a Section for it also
        $idsection = $this->addSection("",$shelf->getIdshelf(),0,1,1);

        $return_array[] = $shelf->getIdshelf();
        $return_array[] = $idsection;

        return $return_array;
    }
    public function editShelf($name, $position_top, $active, $id) {
        if($shelf = getEm()->find('Shelf', $id)){

            if($active){
                $shelf->setName($name);

                $shelf->setPositionTop($position_top);
            }

            $shelf->setActive($active);


            getEm()->persist($shelf);

            getEm()->flush();
        }
    }
    public function deleteShelf($id) {
        if($shelf = getEm()->find('Shelf', $id)){
            $shelf->setActive(0);

            getEm()->persist($shelf);

            getEm()->flush();
        }
    }

    public function getSection($shelves){
        $em = getEm();

        $data = array ();

        if($shelves){
            foreach ($shelves as $shelf){
                $sections = getEm()->getRepository("Section")->findBy(array('active' => 1, 'shelfshelf' => $shelf['id']));

                if($sections){
                    foreach ($sections as $value){
                        $dat = array (
                            "id" => $value->getIdsection(),
                            "shelf_id" => $shelf['id'],
                            "position_left" => $value->getPositionLeft()
                        );
                        array_push($data, $dat);
                    }
                }
            }
        }else{
            $shelfs = array();
        }

        return $data;
    }
    public function addSection($name, $shelf_id, $position_left, $active, $fixed) {
        $section = new Section;

        $section->setName($name);
        
        $shelf = getEm()->getRepository("Shelf")->findBy(array('idshelf' => $shelf_id));
        $section->setShelf($shelf[0]);

        $section->setPositionLeft($position_left);

        $section->setActive($active);

        $section->setFixed($fixed);

        getEm()->persist($section);

        getEm()->flush();
    }
    public function editSection($name, $shelf_id, $position_left, $active, $id) {
        if($section = getEm()->find('Section', $id)){

            if($active){
                $section->setName($name);

                $shelf = getEm()->getRepository("Shelf")->findBy(array('idshelf' => $shelf_id));
                $section->setShelf($shelf[0]);

                $section->setPositionLeft($position_left);
            }
            $section->setActive($active);


            getEm()->persist($section);

            getEm()->flush();
        }
    }
    public function deleteSection($id) {
        if($section = getEm()->find('Section', $id)){
            $section->setActive(0);

            getEm()->persist($section);

            getEm()->flush();
        }
    }

    /**
     * Return page with content avoiding print in controller
     * @param unknown $data
     */
    public function returnData($data){      
            GenericController::answer( "Pagebuilder", "index", "echo", $data);          
    }

    public function loadWardrobe() {
        $wardrobe_id = $_REQUEST['wardrobe_id'];

        $em = getEm();
    
        $wardrobe = getEm()->getRepository("Wardrobe")->findBy(array('active' => 1, 'idwardrobe' => $wardrobe_id));

        $data = array ();

        if($wardrobe){
            foreach ($wardrobe as $value){

                $shelves = getEm()->getRepository("Shelf")->findBy(array('active' => 1, 'wardrobewardrobe' => $wardrobe_id));

                //$data = array ();

                if($shelves){
                    foreach ($shelves as $value2){

                        $sections = getEm()->getRepository("Section")->findBy(array('active' => 1, 'shelfshelf' => $value2->getIdshelf()));

                        $data2 = array ();

                        if($sections){
                            foreach ($sections as $value3){
                                $dat = array (
                                    "section" => $value3->getIdsection()."_".$value3->getPositionLeft()."_".$value3->getFixed()
                                );
                                array_push($data2, $dat);
                            }
                        }

                        $dat = array (
                            "shelf" => $value2->getIdshelf()."_".$value2->getPositionTop()."_".$value2->getFixed()
                        );
                        array_push($data, $dat);
                        array_push($data, $data2);

                    }
                }
            }
        }else{
            $wardrobe = array();
        }

        return $this->returnData(json_encode($data));
    }

    public function saveHall() {
        $em = getEm();
        $hall = getEm()->getRepository("Hall")->findBy(array('active' => 1, 'idhall' => $_POST['hall_id']));
        $idhall = $_POST['hall_id'];

        if($hall){
            $this->editHall("",$_POST['position_top'],$_POST['position_left'],$_POST['height'],$_POST['width'],$_POST['active'],$_POST['hall_id']);
        }else{
            $idhall = $this->addHall("",$_POST['position_top'],$_POST['position_left'],$_POST['height'],$_POST['width'],1,$_POST['warehouse_id']);
        }

        return $this->returnData($idhall);
    }
    public function saveWardrobe() {
        $em = getEm();
        $wardrobe = getEm()->getRepository("Wardrobe")->findBy(array('active' => 1, 'idwardrobe' => $_POST['wardrobe_id']));
        $return_array[] = $_POST['wardrobe_id'];
        $return_array[] = 0;
        $return_array[] = 0;

        if($wardrobe){
            $this->editWardrobe("",$_POST['hall_id'],$_POST['position_top'],$_POST['position_left'],$_POST['height'],$_POST['width'],$_POST['active'],$_POST['wardrobe_id']);
        }else if($_POST['hall_id']){
            $return_array = $this->addWardrobe("",$_POST['hall_id'],$_POST['position_top'],$_POST['position_left'],$_POST['height'],$_POST['width'],1);
        }
        $return_array = json_encode($return_array);

        return $this->returnData($return_array);
    }
    public function saveShelf() {
        $em = getEm();
        $shelf = getEm()->getRepository("Shelf")->findBy(array('active' => 1, 'idshelf' => $_POST['shelf_id']));
        $return_array[] = $_POST['shelf_id'];
        $return_array[] = 0;

        if($shelf){
            $this->editShelf("",$_POST['position_top'],$_POST['active'],$_POST['shelf_id']);
        }else{
            $return_array = $this->addShelf("",$_POST['wardrobe_id'],$_POST['position_top'],1,0);
        }
        $return_array = json_encode($return_array);

        return $this->returnData($return_array);
    }
    public function saveSection() {
        $em = getEm();
        $section = getEm()->getRepository("Section")->findBy(array('active' => 1, 'idsection' => $_POST['section_id']));
        $idsection = $_POST['section_id'];

        if($section){
            $this->editSection("",$_POST['shelf_id'],$_POST['position_left'],$_POST['active'],$_POST['section_id']);
        }else{
            $this->addSection("",$_POST['shelf_id'],$_POST['position_left'],1,0);
        }

        return $this->returnData($idsection);
    }
    
    public function index(){
        $navbar = "Entry|warehouse";

        $warehouse_id = 1; //por enquanto

        $warehouses = $this->getWarehouse();
        $halls = $this->getHall($warehouse_id);
        $wardrobes = $this->getWardrobe($halls);
    	$hall_id = 0;
    	$wardrobe_id = 0;
        $shelf_id = 0;
        $section_id = 0;

        $fp = fopen("update_settings.json","a+");

        $array_answer = array (
        	'0'=>'',
        	'halls'=>json_encode($halls),
        	'wardrobes'=>json_encode($wardrobes),
            'warehouses'=>json_encode($warehouses),
        	'hall_id'=>$hall_id,
        	'wardrobe_id'=>$wardrobe_id,
            'shelf_id'=>$shelf_id,
            'section_id'=>$section_id
        );      

        GenericController::template("Entry", "warehouse","index", $navbar, $array_answer, 227);
    }

    public function stocks(){
        $navbar = "Entry|warehouse";
        $array_answer = array ("");
        GenericController::template("Entry", "warehouse","stocks", $navbar, $array_answer, 219);
    }

    public function getAllStocks(){
        try{
            $entityManager = getEm();
            $arrayFilter = array(
                "active" => 1
            );
            $warehouse = $entityManager->getRepository('Warehouse')->findBy($arrayFilter);
            $data = array ();
            foreach ($warehouse as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdwarehouse().'" data-name="'.$value->getName().'">',
                    "id" => $value->getIdwarehouse(),
                    "description" => $value->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function insert_warehouse(){
        if(isset($_POST['nameWarehouse'])){
            try {
                $warehouse = new Warehouse();
                $warehouse->setName($_POST['nameWarehouse']);
                $warehouse->setDateCreate(new DateTime());
                $warehouse->setActive(true);
                getEm()->persist($warehouse);
                getEm()->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function get_warehouse(){
        if(isset($_POST['id'])){
            try {
                $warehouse = getEm()->getRepository('Warehouse')->findBy(array("idwarehouse" => $_POST['id']));
                $mysqlData = array ();
                foreach ($warehouse as $value){
                    $dat = array (
                        "description" => $value->getName(),
                        "id" => $value->getIdwarehouse()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function update_warehouse(){
        if(isset($_POST['nameWarehouse']) && isset($_POST['idWarehouse'])){
            try {
                $warehouse = getEm()->getRepository('Warehouse')->findBy(array("idwarehouse" => $_POST['idWarehouse']));
                $warehouse[0]->setName($_POST['nameWarehouse']);
                $warehouse[0]->setDateUpdate(new datetime());
                $warehouse[0]->setActive(true);
                getEm()->persist($warehouse[0]);
                getEm()->flush();

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function delete_warehouse(){
        if($_POST['idWarehouses']){
            try{
                $ids = $_POST['idWarehouses'];
                for($i=0; $i < count($ids); $i++){
                    $warehouse = getEm()->getRepository("Warehouse")->findBy(array("idwarehouse" => $ids[$i]));
                    $warehouse[0]->setActive('3');
                    $warehouse[0]->setDateDelete(new DateTime());
                    getEm()->persist($warehouse[0]);
                    getEm()->flush();
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
}
