<?php 

class TributeController {

    public function index(){
        $navbar = "Entry|tributes";
        $tax = getEm()->getRepository("Tax")->findBy(array("active" => 1));
        $array_answer = array (
            "taxs" => $tax
        );
        GenericController::template("Entry", "tributes", "index", $navbar, $array_answer, 340);
    }

    public function getAll($returnArray = false){
        try{
            $tribute = getEm()->getRepository('Tribute')->findBy(array("active" => 1));
            $data = array ();
            foreach ($tribute as $value){
                $tax = $value->getTax();
                $vat_alternative = $value->getVatAlternative();
                if($value->getPayVat() == false){
                    $pay_vat = "";
                } else {
                    $pay_vat = "<span class='glyphicon glyphicon-ok'></span>";
                }
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdTribute().'" data-name="'.$value->getName().'">',
                    "id" => $value->getIdTribute(),
                    "name" => $value->getName(),
                    "pay_vat" => $pay_vat,
                    "tax" => $tax->getName(),
                    "treasury" => $value->getTreasury(),
                    "treasury_without_vat" => $value->getTreasuryWithoutVat(),
                    "cod_additional" => $value->getCodAdditional(),
                    "vat_alternative" => $vat_alternative->getName(),
                    "treasury_alternative" => $value->getTreasuryAlternative(),
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );

        if ($returnArray == true) {
            return $tribute;
        }

        $json_data = json_encode($data);
        print $json_data;
    }

    public function insert(){
        if(isset($_POST['nameTribute'])){
            try {
                $tribute = new Tribute();
                $tribute->setName($_POST['nameTribute']);
                $tribute->setPayVat(isset($_POST['pay_vat']));
                if(isset($_POST['vat'])){
                    $tax = getEm()->getRepository('Tax')->findOneBy(array ('idtax' => $_POST['vat']));
                    $tribute->setTax($tax);
                }
                $tribute->setTreasury($_POST['treasury']);
                $tribute->setTreasuryWithoutVat($_POST['treasuryWithoutVat']);
                $tribute->setCodAdditional($_POST['codAdditional']);
                if(isset($_POST['alternativeVat']) && $_POST['alternativeVat'] != 0){
                    $tax = getEm()->getRepository('Tax')->findOneBy(array ('idtax' => $_POST['alternativeVat']));
                    $tribute->setVatAlternative($tax);
                }
                $tribute->setTreasuryAlternative($_POST['treasuryAlternative']);
                $tribute->setActive(1);
                getEm()->persist($tribute);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'tribute', $_POST['nameTribute']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
}


?>