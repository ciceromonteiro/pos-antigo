<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Feb 3, 2017
 */
class ZzStateController {

    public function __contruct() {
        
    }

    /**
     * get id ZzState
     * 
     * @param int $zzstate idzz_state
     * @param int $countryselect idzz_country
     * @return int id of ZzState
     */
    public function insertState($zzstate, $countryselect) {

        if ($zzstate == 0) {
            $StateOB = getEm()->getRepository('ZzCountry')->findBy(array("idzzCountry" => $countryselect));
            //var_dump($countryselect);
            //die();

            $StateOBTemp = getEm()->getRepository('ZzState')->findBy(array("zzCountryzzCountry" => $StateOB[0]->getIdzzCountry()));

            if (!empty($StateOBTemp[0])) {
                $idState = $StateOBTemp[0]->getIdzzState();
                return $idState;
            } else {
                try {
                    $StateOB2 = getEm()->getRepository('ZzCountry')->findBy(array("code" => $countryselect));
                    $State = new ZzState();
                    $State->setName($StateOB2[0]->getName());
                    $State->setZzCountryzzCountry($StateOB2[0]);
                    $State->setActive(1);
                    getEm()->persist($State);
                    getEm()->flush();
                    $idState = $State->getIdzzState();
                } catch (Exception $e) {
                    print $e;
                    exit(1);
                }
                
                return $idState;
            }
        } else {
            $StateOB = getEm()->getRepository('ZzState')->findBy(array("idzzState" => $zzstate));
            $idState = $StateOB[0]->getIdzzState();
            return $idState;
        }
    }

}
