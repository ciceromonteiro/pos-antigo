<?php

/**
 * @author Claudio da Cruz Silva Junior>
 * @version 1.0.0
 * @abstract file created in date september 19, 2017
 */

/**
 * @var $entityManager EntityManager *
 */
class ProductController {

    public function index() {
        $navbar = "Entry|product";

        $products = getEm()->getRepository('Product')->findAll();
        $manufacturers = ProductController::getAllManufacturers(true);
        $supplier = getEm()->getRepository('Supplier')->findBy(array ('active' => 1));
        $productGp = getEm()->getRepository('ProductGp')->findBy(array('active' => 1));
        $array_answer = array(
            "products" => $products,
            "manufacturers" => $manufacturers,
            "suppliers" => $suppliers,
            "productGp" => $productGp
        );
        GenericController::template("Entry", "product", "list_products", $navbar, $array_answer, 288);
    }

    public function getAllStock(){
        $sql = "SELECT (CASE 
                            WHEN SC.idsize_color = NULL 
                                THEN SC.current_amount
                            ELSE P.total_amount
                        END) as total_amount, 
                                SC.active AS active_size, 
                                SC.idsize_color,
                                P.active, P.idproduct, P.sku, P.name, 
                                S.name AS size, 
                                C.name as color
                FROM product AS P
                LEFT JOIN size_color AS SC ON P.idproduct = SC.product_idproduct
                LEFT JOIN size AS S ON SC.size_idsize = S.idsize
                LEFT JOIN color AS C ON SC.color_idcolor = C.idcolor
                WHERE P.active = 1";
        $stmt = getEm()->getConnection()->prepare($sql);
        $stmt->execute();
        $rows = [];
        if($result = $stmt->fetchAll()){
            foreach ($result as $value) {
                if($value['active_size'] == NULL && $value['active'] != 1){
                    return;
                }
                if($value['active_size'] != NULL && $value['active'] != 1 && $value['active_size'] != 1){
                    return;
                }
                if($value['idsize_color'] != NULL){
                    $value['type'] = 2; // size_color
                } else {
                    $value['type'] = 1; // product
                }
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $value['idproduct'] . '">',
                    "id" => $value['idproduct'],
                    "sku" => $value['sku'],
                    "cod" => '',
                    "product" => $value['name'],
                    "size" => $value['size'],
                    "qtd" => $value['total_amount'],
                    "type_row" => $value['type']
                );
                array_push($rows, $dat);
            }
        }
        $data = array(
            "result" => "success",
            "message" => "query success",
            "data" => $rows
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getStockChange($id, $type){
        $sql = "SELECT (CASE 
                            WHEN SC.idsize_color = NULL 
                                THEN SC.current_amount
                            ELSE P.total_amount
                        END) as total_amount, 
                                SC.active AS active_size, 
                                SC.idsize_color,
                                P.active, P.idproduct, P.sku, P.name, 
                                S.name AS size, 
                                C.name as color
                FROM product AS P
                LEFT JOIN size_color AS SC ON P.idproduct = SC.product_idproduct
                LEFT JOIN size AS S ON SC.size_idsize = S.idsize
                LEFT JOIN color AS C ON SC.color_idcolor = C.idcolor
                WHERE P.active = 1";
        if($type == 1){
            $sql .= ' AND P.idproduct = '.$id;    
        } else {
            $sql .= ' AND SC.idsize_color = '.$id;
        }
        $stmt = getEm()->getConnection()->prepare($sql);
        $stmt->execute();
        $rows = [];
        if($result = $stmt->fetchAll()){
            foreach ($result as $value) {
                $dat = array(
                    "name" => $value['name'],
                    "qtd" => $value['total_amount']
                );
                if($value['idsize_color'] != NULL){
                    $dat['type_row'] = 2; // size_color
                    $dat['id'] = $value['idsize_color']; // size_color
                } else {
                    $dat['type_row'] = 1; // product
                    $dat['id'] = $value['idproduct']; // product
                }
                array_push($rows, $dat);
            }
        }
        $data = array(
            "status" => 200,
            "item" => $rows
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function product($id = 0){
        require "ColorController.php";
        require "ProductSizeController.php";
        require "SupplierController.php";
        require "TributeController.php";
        require "DepartmentController.php";
        require "ProjectController.php";
        
        $colors = new ColorController();
        $sizes = new ProductSizeController();
        $manufacturers = ProductController::getAllManufacturers(true);
        $suppliers =  new SupplierController();
        $tributes = new TributeController();
        $units = ProductController::getAllUnit(true);
        $department = new DepartmentController();
        $project = new ProjectController();
        $productGp = getEm()->getRepository('ProductGp')->findBy(array('active' => 1));
        $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $id));
        $product_in_productGp = getEm()->getRepository('ProductProductGp')->findBy(array('productproduct' => $id));
        $currency = getEm()->getRepository('Currency')->findBy(array('active' => 1));
        $stocks = getEm()->getRepository('Warehouse')->findBy(array('active' => 1));
        
        $array_answer = array (
            "colors" => $colors->getAll(true),
            "sizes" => $sizes->getAll(true),
            "manufacturers" => $manufacturers,
            "suppliers" => $suppliers->getAll(true),
            "tributes" => $tributes->getAll(true), 
            "units" => $units,
            "departments" => $department->getAll(true),
            "projects" => $project->getAll(true),
            "productGp" => $productGp,
            "product_in_productGp" => $product_in_productGp,
            "product" => $product,
            "currency" => $currency,
            "stocks" => $stocks
        );
        
        if($id != null && $this->checkProduct($id) == true){
            $page = "product";
            $array_answer['idproduct'] = $id;
        } else if($id != null && $this->checkProduct($id) == false){
            $page = "not_found";
        } else if ($id == null){
            $array_answer['idproduct'] = $this->createProductTemp();
            $page = "product";
        }

        $navbar = "Entry|product";
        GenericController::template("Entry", "product", $page, $navbar, $array_answer, 286);
    }

    public function markup($id) {
        ini_set('display_errors', 1); // 0 for production
        require "ColorController.php";
        require "ProductSizeController.php";
        require "SupplierController.php";
        require "TributeController.php";
        require "DepartmentController.php";
        require "ProjectController.php";

        $colors = new ColorController();
        $sizes = new ProductSizeController();
        $manufacturers = ProductController::getAllManufacturers(true);
        $suppliers = new SupplierController();
        $tributes = new TributeController();
        $units = ProductController::getAllUnit(true);
        $department = new DepartmentController();
        $project = new ProjectController();
        $printers = getEm()->getRepository('Printer')->findBy(array('active' => 1));
        $productGp = getEm()->getRepository('ProductGp')->findBy(array('active' => 1));
        $products = getEm()->getRepository('Product')->findAll();
        $currency = getEm()->getRepository('Currency')->findBy(array('active' => 1));
        $stocks = getEm()->getRepository('Warehouse')->findBy(array('active' => 1));

        $array_answer = array(
            "colors" => $colors->getAll(true),
            "sizes" => $sizes->getAll(true),
            "manufacturers" => $manufacturers,
            "suppliers" => $suppliers->getAll(true),
            "tributes" => $tributes->getAll(true),
            "units" => $units,
            "departments" => $department->getAll(true),
            "projects" => $project->getAll(true),
            "printers" => $printers,
            "productGp" => $productGp,
            "products" => $products,
            "currency" => $currency,
            "stocks" => $stocks
        );
        $product = getEm()->getRepository('Product')->findOneBy(array('idproduct' => $id));
        // echo '<pre>';
        //     var_dump($product->pricePurchase);
        // echo '</pre>';
        // die();
        if ($id != null && $this->checkProduct($id) == true) {
            $page = "markup";
            $array_answer['idproduct'] = $id;
            $array_answer['product'] = $product;
        } else if ($id != null && $this->checkProduct($id) == false) {
            $page = "not_found";
        } else if ($id == null) {
            $array_answer['idproduct'] = $this->createProductTemp();
            $page = "markup";
        }
        $navbar = "Entry|markup";
        GenericController::template("Entry", "product", $page, $navbar, $array_answer, 286);
    }

    public function checkProduct($id) {
        $product = getEm()->getRepository('Product')->findOneBy(array('idproduct' => $id));
        if (!empty($product)) {
            return true;
        } else {
            return false;
        }
    }

    public function create() {
        try {
            $product = new Product();
            if(isset($_POST['name'])){
                $product->setName($_POST['name']);
            }
            if(isset($_POST['sku'])){
                $product->setSku($_POST['sku']);
            }
            if(isset($_POST['description'])){
                $product->setDescription($_POST['description']);
            }
            if(isset($_POST['supplier']) && $_POST['supplier'] != ''){
                $supplier = getEm()->getRepository('Supplier')->findBy(array('idsupplier' => $_POST['supplier']));
                $product->setSuppliersupplier($supplier[0]);
            }
            if(isset($_POST['manufacturer']) && $_POST['manufacturer'] != ''){
                $manufacturer = getEm()->getRepository('Manufacturer')->findBy(array('idmanufacturer' => $_POST['manufacturer']));
                $product->setManufacturermanufacturer($manufacturer[0]);
            }
            if(isset($_POST['price'])){
                $price1 = str_replace('.', '', $_POST['price']);
                $price = (float)str_replace(',', '.', $price1);
                $product->setPrice($price);
            }
            if($_SESSION['user']){
                $user = getEm()->getRepository('Users')->findBy(array('idusers' => $_SESSION['user']));
                $product->setCreateBy($user[0]);
            }
            if(isset($_POST['total_amount']) && $_POST['total_amount'] != ''){
                $product->setTotalAmount($_POST['total_amount']);
            }
            $product->setDateCreate(new DateTime());
            $product->setTypeProduct(1);
            $product->setActive(1);
            getEm()->persist($product);
            getEm()->flush();

            if(isset($_POST['productGp']) && $_POST['productGp'] != ''){
                $productGp = getEm()->getRepository('ProductGp')->findBy(array('idproductGp' => $_POST['productGp']));
                $product_product_gp = new ProductProductGp();
                $product_product_gp->setProductproduct($product);
                $product_product_gp->setProductGpproductGp($productGp[0]);
                getEm()->persist($product_product_gp);
                getEm()->flush();
            }
            die(json_encode(['status' => 200, 'id' => $product->getIdproduct()]));
        } catch (Exception $e) {
            //_dump($e->getMessage());
            die(json_encode(['status' => 500]));
        }
    }

    public function get($id = false){
        if($id){
            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $id));
            if($product){
                $group = getEm()->getRepository('ProductProductGp')->findBy(array('productproduct' => $product[0]->getIdproduct()));
                
                $item = [
                    'id' => $product[0]->getIdproduct(),
                    'name' => $product[0]->getName(),
                    'sku' => $product[0]->getSku(),
                    'description' => $product[0]->getDescription(),
                    'price' => number_format($product[0]->getPrice(), 2, ',', '.')
                ];
                if($product[0]->getSuppliersupplier()){
                    $item['supplier'] = $product[0]->getSuppliersupplier()->getIdsupplier();
                }
                if($product[0]->getManufacturermanufacturer()){
                    $item['manufacturer'] = $product[0]->getManufacturermanufacturer()->getIdmanufacturer();
                }
                if($group){
                    $item['group'] = $group[0]->getProductGpproductGp()->getIdproductGp();
                }
                die(json_encode(['status' => 200, 'item' => $item]));
            }
            die(json_encode(['status' => 404]));
        } else {
            die(json_encode(['status' => 404]));
        }
    }

    public function update() {
        if(!isset($_POST['id'])){
            die(json_encode(['status' => 404]));
        }
        try {
            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['id']));
            if(isset($_POST['name'])){
                $product[0]->setName($_POST['name']);
            }
            if(isset($_POST['sku'])){
                $product[0]->setSku($_POST['sku']);
            }
            if(isset($_POST['description'])){
                $product[0]->setDescription($_POST['description']);
            }
            if(isset($_POST['supplier']) && $_POST['supplier'] != ''){
                $supplier = getEm()->getRepository('Supplier')->findBy(array('idsupplier' => $_POST['supplier']));
                $product[0]->setSuppliersupplier($supplier[0]);
            } else { 
                $product[0]->setSuppliersupplier(NULL);
            }
            if(isset($_POST['manufacturer']) && $_POST['manufacturer'] != ''){
                $manufacturer = getEm()->getRepository('Manufacturer')->findBy(array('idmanufacturer' => $_POST['manufacturer']));
                $product[0]->setManufacturermanufacturer($manufacturer[0]);
            } else {
                $product[0]->setManufacturermanufacturer(NULL);
            }
            if(isset($_POST['price'])){
                $price1 = str_replace('.', '', $_POST['price']);
                $price = (float)str_replace(',', '.', $price1);
                $product[0]->setPrice($price);
            }
            /* Unit */
            if(isset($_POST['unit'])){
                $unit = getEm()->getRepository('Unit')->findBy(array('idunit' => $_POST['unit']));
                if(count($unit) > 0){
                    $product[0]->setUnitunit($unit[0]);
                }
            }
            if(!isset($_POST['unit']) && isset($_POST['full_edit'])) {
                $product[0]->setManufacturermanufacturer(NULL);
            }
            /* Department */
            if(isset($_POST['department'])){
                $department = getEm()->getRepository('Department')->findBy(array('iddepartment' => $_POST['department']));
                if(count($department) > 0){
                    $product[0]->setProductDepartmentproductDepartment($department[0]);
                }
            }
            if(!isset($_POST['department']) && isset($_POST['full_edit'])) {
                $product[0]->setProductDepartmentproductDepartment(NULL);
            }
            /* Project */
            if(isset($_POST['project'])){
                $project = getEm()->getRepository('Project')->findBy(array('idproject' => $_POST['project']));
                if(count($project) > 0){
                    $product[0]->setProject($project[0]);
                }
            }
            if(!isset($_POST['project']) && isset($_POST['full_edit'])) {
                $product[0]->setProject(NULL);
            }
            /* Status */
            if(isset($_POST['status'])){
                $product[0]->setActive($_POST['status']);
            }
            if(!isset($_POST['status']) && isset($_POST['full_edit'])) {
                $product[0]->setActive(NULL);
            }
            $product[0]->setDateUpdate(new DateTime());
            getEm()->persist($product[0]);
            getEm()->flush();

            if(isset($_POST['productGp']) && $_POST['productGp'] != '' && $_POST['productGp'] != '0'){
                $this->delete_product_group_by_product($_POST['id']);
                $productGp = getEm()->getRepository('ProductGp')->findBy(array('idproductGp' => $_POST['productGp']));
                $product_product_gp = getEm()->getRepository('ProductProductGp')->findBy(array('productproduct' => $_POST['id']));
                if($product_product_gp && count($product_product_gp) > 0){
                    $product_product_gp[0]->setProductproduct($product[0]);
                    $product_product_gp[0]->setProductGpproductGp($productGp[0]);
                    getEm()->persist($product_product_gp[0]);
                    getEm()->flush();
                } else {
                    $product_product_gp = new ProductProductGp();
                    $product_product_gp->setProductproduct($product[0]);
                    $product_product_gp->setProductGpproductGp($productGp[0]);
                    getEm()->persist($product_product_gp);
                    getEm()->flush();
                }
            } else {
                $this->delete_product_group_by_product($_POST['id']);
            }
            die(json_encode(['status' => 200, 'id' => $product[0]->getIdproduct()]));
        } catch (Exception $e) {
            die(json_encode(['status' => 500, 'extra' => $e->getMessage()]));
        }
    }

    public function saveStockChange() {
        if( !isset($_POST['id']) || !isset($_POST['type']) || !isset($_POST['qtd']) ){
            die(json_encode(['status' => 404]));
        }
        try {
            $user = getEm()->getRepository('Users')->findBy(array('idusers' => $_SESSION['user']));
            if($_POST['type'] == 1){ // product
                $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['id']));
                $product[0]->setTotalAmount($_POST['qtd']);
                $product[0]->setDateUpdate(new DateTime());
                getEm()->persist($product[0]);
                getEm()->flush();
                $this->insert_stock_changes(@$_POST['type'], $product, null, $user, $product[0]->getTotalAmount(), $_POST['qtd'], @$_POST['comment']);
                die(json_encode(['status' => 200, 'id' => $product[0]->getIdproduct()]));
            }
            else { //size_color
                die('Test 3');
                $size_color = getEm()->getRepository('SizeColor')->findBy(array('idsizeColor' => $_POST['id']));
                $size_color[0]->setTotalamount($_POST['qtd']);

                $size_color[0]->setDateUpdate(new DateTime());
                getEm()->persist($size_color[0]);
                getEm()->flush();
                $this->insert_stock_changes(@$_POST['type'], null, $size_color, $user, $size_color[0]->getTotalamount(), $_POST['qtd'], @$_POST['comment']);
                die(json_encode(['status' => 200, 'id' => $size_color[0]->getIdsizeColor()]));
            }
        } catch (Exception $e) {
            die(json_encode(['status' => 500, 'extra' => $e->getMessage()]));
        }
    }

    public function productRemoveAllTemps() {
        $product = getEm()->getRepository('Product')->findBy(array('active' => 6));
        foreach ($product as $value) {
            try {
                $value->setActive(5);
                getEm()->persist($value);
                getEm()->flush();
                AuthenticationController::insertLog('delete', 'ProductTemp', $value->getIdproduct());
                $result = "success";
                $message = "query success";
                $mysqlData = '';
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllProducts() {
        try {
            $data = array();
            $product = getEm()->getRepository('Product')->findAll();
            foreach ($product as $value) {
                $id = is_null($value->getIdproduct()) ? '' : $value->getIdproduct();

                if ($value->getName() == null) {
                    $name = '';
                } else {
                    $name = $value->getName();
                }
                if ($value->getSku() == null) {
                    $sku = '';
                } else {
                    $sku = $value->getSku();
                }

                if ($value->getPricePurchase() == null) {
                    $purchasePrice = '0,00';
                } else {
                    $purchasePrice = $value->getPricePurchase();
                }

                if ($value->getPrice() == null) {
                    $salePrice = '0,00';
                } else {
                    $salePrice = $value->getPrice();
                }

                if ($value->getDateUpdate() == null) {
                    $update = "";
                } else {
                    $update = $value->getDateUpdate()->format('d/m/Y H:i:s');
                }

                if ($value->getActive() == 1) {
                    $active = "Active";
                } else if ($value->getActive() == 2) {
                    $active = "Passive";
                } else if ($value->getActive() == 3) {
                    $active = "Expired";
                } else if ($value->getActive() == 4) {
                    $active = "Blocked";
                } else if ($value->getActive() == 5) {
                    $active = "Deleted";
                } else if ($value->getActive() == 6) {
                    $active = "Except temporarily";
                } else {
                    $active = "None";
                }

                $groupProductList = "";
                $groupProduct = getEm()->getRepository('ProductProductGp')->findBy(array('idproductProductGp' => $id));
                if (!empty($groupProduct)) {
                    foreach ($groupProduct as $value) {
                        $groupProductList .= $value->getProductGpproductGp()->getName() . ',';
                    }
                }
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $id . '">',
                    "id" => $id,
                    "sku" => $sku,
                    "name" => $name,
                    "salePrice" => 'R$ '.number_format($salePrice, 2, ',', '.'),
                    "group" => $groupProductList,
                    "amount" => $value->getTotalAmount(),
                    "status" => $active
                );
 
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getProduct($idProduct) {
        if (isset($idProduct)) {
            try {
                $mysqlData = array();
                $product = getEm()->getRepository('Product')->findOneBy(array('idproduct' => $idProduct));
                if (!empty($product) || $product != null) {
                    $tax = $product->getTaxGp() != null ? $product->getTaxGp()->getIdtribute() : '';
                    $printer = $product->getPrintForPickIst() != null ? $product->getPrintForPickIst()->getIdprinter() : '';
                    $manufacturer = $product->getManufacturermanufacturer() != null ? $product->getManufacturermanufacturer()->getIdmanufacturer() : '';
                    $supplier = $product->getSuppliersupplier() != null ? $product->getSuppliersupplier()->getIdsupplier() : '';
                    $unit = $product->getUnitunit() != null ? $product->getUnitunit()->getIdunit() : '';
                    $department = $product->getProductDepartmentproductDepartment() != null ? $product->getProductDepartmentproductDepartment()->getIddepartment() : '';
                    $project = $product->getProject() != null ? $product->getProject()->getIdproject() : '';
                    $stock = $product->getSectionsection() != null ? $product->getSectionsection()->getIdWarehouse() : '';
                    $curreny = $product->getCurrencycurrency() != null ? $product->getCurrencycurrency()->getIdcurrency() : '';

                    $dat = array(
                        "id_product" => $product->getIdproduct(),
                        "name_product" => $product->getName(),
                        "type_product" => $product->getTypeProduct(),
                        "output_control" => $product->getOutputControl(),
                        "pricing_of_sell_value_without_tax" => $product->getPricingOfSellValueWithoutTax(),
                        "pricing_of_buy" => $product->getPricePurchase(),
                        "pricing_of_shipping" => $product->getValueOfShipping(),
                        "profit_gross_profit_value" => $product->getProfitGrossProfitValue(),
                        "groups_from_tributes" => $tax,
                        "print_for_pick_list" => $printer,
                        "price_of_table" => $product->getPrice(),
                        "recom_value_for_sell_liquid" => $product->getRecomValueForSellLiquid(),
                        "recom_value_for_sell_brute" => $product->getRecomValueForSellBrute(),
                        "trade_value_for_sell_liquid" => $product->getTradeValueForSellLiquid(),
                        "trade_value_for_sell_brute" => $product->getTradeValueForSellBrute(),
                        "value_for_travel_liquid" => $product->getValueForTravelLiquid(),
                        "value_for_travel_brute" => $product->getValueForTravelBrute(),
                        "number_of_serie_from_product" => $product->getNumberOfSerieFromProduct(),
                        "select_manufacturer" => $manufacturer,
                        "select_supplier" => $supplier,
                        "select_unit_from_product" => $unit,
                        "select_base_of_calc" => $product->getBaseOfCalc(),
                        "number_of_manufacture" => $product->getProductNumberManufacturer(),
                        "number_of_supplier" => $product->getProductNumberSupplier(),
                        "multiple_units" => $product->getMultipleUnits(),
                        "status_of_product" => $product->getActive(),
                        "select_department" => $department,
                        "select_projects" => $project,
                        "place_where_the_product" => $stock,
                        "components_compound_product" => $product->getHasComponents(),
                        "components_show_order_subitens" => $product->getComponentsShowOrderSubitens(),
                        "prediction_of_purchase" => $product->getPredictionOfPurchase(),
                        "net_weight" => $product->getNetWeight(),
                        "additional_weight" => $product->getAdditionalWeight(),
                        "volume" => $product->getVolume(),
                        "lenght" => $product->getLength(),
                        "width" => $product->getWidth(),
                        "height" => $product->getHeight(),
                        "minimun_purchase_of_packages" => $product->getMinimumPurchaseOfPackages(),
                        "minimum_purchase" => $product->getMinimumPurchase(),
                        "min_stock" => $product->getMinStock(),
                        "max_stock" => $product->getMaxStock(),
                        "select_currency" => $curreny,
                        "purchase_price_exchange" => $product->getCurrencyPurchasePrice(),
                        "import_rate" => $product->getImportingRate(),
                        "freight_tax_amount" => $product->getFreightTaxAmount()
                    );
                    array_push($mysqlData, $dat);

                    $result = 'success';
                    $message = 'query success';
                    $data = $mysqlData;
                } else {
                    $result = 'not-exist';
                    $message = 'query success';
                    $data = '';
                }
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Get the datas with barCode and qrCode of the product clicked in reports/product/index
     * @param int $idProduct
     * @return string json_data 
     */
    public function getProductWithBarcodeAndrCode($idProduct) {
        $Product = getEm()->getRepository('Product')->findAll();
        try {
            $data = array();
            if ($Product != 0) {
                foreach ($Product as $product) {
                    if ($product->getIdproduct() == $idProduct) {
                        if ($product->getName() == null) {
                            $name = '';
                        } else {
                            $name = $product->getName();
                        }

                        if ($product->getBarcode() == null) {
                            $barCode = 'no data';
                        } else {
                            $barCode = $product->getBarcode();
                        }

                        $qrCode = '../../../libraries/qrCode/php/qr_img.php?';
                        $qrCode .= 'd=' . $barCode . '&';
                        $qrCode .= 'e=H&';
                        $qrCode .= 's=4&';
                        $qrCode .= 't=P';

                        require_once('../libraries/barCode/barcode.inc.php');
                        new barCodeGenrator($barCode, 1, '../data/codeBar/barcode.gif', 200, 150, true);

                        $dat = array(
                            "id" => $product->getIdproduct(),
                            "name" => $name,
                            "barCode" => $barCode,
                            "qrCode" => $qrCode,
                            "barCodeImage" => '../../../data/codeBar/barcode.gif'
                        );

                        array_push($data, $dat);
                    }
                }
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Get the barCode of the product respective
     * @param int $idProduct
     * @return string json_data 
     */
    public function getBarCodeProduct($idProduct) {
        $Product = getEm()->getRepository('Product')->findAll();
        try {
            $data = array();
            if ($Product != 0) {
                $codeBar = null;
                $barcode = null;
                foreach ($Product as $product) {
                    if ($product->getIdproduct() == $idProduct) {
                        if ($product->getBarcode() == null) {
                            $codeBar = null;
                        } else {
                            $barCode = $product->getBarcode();
                            require_once('../libraries/barCode/barcode.inc.php');
                            new barCodeGenrator($barCode, 1, '../data/codeBar/barcode.gif', 200, 150, true);
                            $codeBar = file_get_contents('../data/codeBar/barcode.gif');
                            $codeBar = base64_encode($codeBar);
                        }
                    }
                }
            }
            $result = "success";
            $message = "query success";
            if ($codeBar != null) {
                $mysqlData = 'data:image/gif;base64,' . $codeBar;
            } else {
                $mysqlData = 'no data';
            }
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Order the products in the table in order crescent of price purcharse
     * @return string json_data 
     */
    public function orderProductByValue() {
        $Product = getEm()->getRepository('Product')->findBy(array(), array('pricePurchase' => 'ASC'));

        try {
            $data = array();
            foreach ($Product as $value) {
                $id = is_null($value->getIdproduct()) ? '' : $value->getIdproduct();

                if ($value->getName() == null) {
                    $name = '';
                } else {
                    $name = $value->getName();
                }

                if ($value->getPricePurchase() == null) {
                    $purchasePrice = '0.00';
                } else {
                    $purchasePrice = $value->getPricePurchase();
                }

                if ($value->getPrice() == null) {
                    $salePrice = '0.00';
                } else {
                    $salePrice = $value->getPrice();
                }

                if ($value->getDateUpdate() == null) {
                    $update = "";
                } else {
                    $update = $value->getDateUpdate()->format('m/d/Y');
                }

                if ($value->getActive() == 1) {
                    $active = "Active";
                } else if ($value->getActive() == 2) {
                    $active = "Blocked";
                } else if ($value->getActive() == 3) {
                    $active = "Deleted";
                } else {
                    $active = "Temporarily saved";
                }

                $groupProductList = "";
                $groupProduct = getEm()->getRepository('ProductProductGp')->findBy(array('idproductProductGp' => $id));
                if (!empty($groupProduct)) {
                    foreach ($groupProduct as $value) {
                        $groupProductList .= $value->getProductGpproductGp()->getName() . ',';
                    }
                }

                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $id . '">',
                    "id" => $id,
                    "name" => $name,
                    "purchasePrice" => number_format($purchasePrice, 2, ",", "."),
                    "salePrice" => number_format($salePrice, 2, ",", "."),
                    "group" => $groupProductList,
                    "update" => $update,
                    "status" => $active
                );

                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Generate barCode of product with barCode equal NULL
     * @param int $idProduct
     * @return string json_data 
     */
    public function generateBarCode($idProduct) {
        try {
            $barCode = time();
            $Product = getEm()->getRepository('Product')->findBy(array('idproduct' => $idProduct));
            if ($Product != null) {
                $Product[0]->setBarcode($barCode);
                getEm()->persist($Product[0]);
                getEm()->flush();
            }

            $result = "success";
            $message = "query success";
            $mysqlData = "";
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllPrinters() {
        $Printers = getEm()->getRepository('Printer')->findBy(array('active' => 1));
        $PrinterType = getEm()->getRepository('PrinterType')->findBy(array('active' => 1));
        $Pos = getEm()->getRepository('Pos')->findBy(array('active' => 1));

        $namePrinterType = null;
        $namePos = null;
        try {
            $data = array();
            if ($Printers != null) {
                foreach ($Printers as $printer) {
                    if ($PrinterType != null && $printer->getIdTypePrinter() != null) {
                        $namePrinterType = null;
                        foreach ($PrinterType as $type) {
                            if ($printer->getIdTypePrinter()->getIdprintertype() == $type->getIdprintertype()) {
                                $namePrinterType = $type->getName();
                            }
                        }
                    }
                    if ($Pos != null && $printer->getInstalledIn() != null) {
                        $namePos = null;
                        foreach ($Pos as $pos) {
                            if ($printer->getInstalledIn()->getIdpos() == $pos->getIdpos()) {
                                $namePos = $pos->getNickname();
                            }
                        }
                    }


                    if ($namePrinterType == null) {
                        $namePrinterType = "";
                    }
                    if ($namePos == null) {
                        $namePos = "";
                    }
                    $date = $printer->getDateCreate()->format('d/m/Y');

                    $dat = array(
                        "checkbox" => '<input type="checkbox" data-id="' . $printer->getIdprinter() . '">',
                        "id" => $printer->getIdprinter(),
                        "instaledIn" => $namePos,
                        "type" => $namePrinterType,
                        "port" => $printer->getPort(),
                        "dateCreate" => $date,
                        "active" => $printer->getActive()
                    );

                    array_push($data, $dat);
                }
            }

            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Get quantity of product in stock
     * @param int $idProduct
     * @return string json_data 
     */
    public function getProductAroundQuantity($idProduct) {
        $Product = getEm()->getRepository('Product')->findAll();
        try {
            $data = array();
            if ($Product != 0) {
                foreach ($Product as $product) {
                    if ($product->getIdproduct() == $idProduct) {
                        $qty2 = 0;
                        $qtyStock = getEm()->getRepository('ProductInSection')->findBy(array('productproduct' => $product->getIdproduct()));
                        if (!empty($qtyStock)) {
                            $qty = 0;
                            foreach ($qtyStock as $value2) {
                                $qty2 += $qty + $value2->getQty();
                            }
                        }
                    }
                }
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $qty2;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        //var_dump($data);
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllProductGpByProduct($idProduct) {
        try {
            $data = array();
            $productGpByProduct = getEm()->getRepository('ProductProductGp')->findBy(array("productproduct" => $idProduct));
            foreach ($productGpByProduct as $value) {
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproductProductGp() . '">',
                    "id" => $value->getIdproductProductGp(),
                    "description" => $value->getProductGpproductGp()->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insertProductGpByProduct() {
        try {
            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['idProduct']));
            $group = getEm()->getRepository('ProductGp')->findBy(array('idproductGp' => $_POST['group']));
            $productGpByProduct = new ProductProductGp();
            $productGpByProduct->setProductproduct($product[0]);
            $productGpByProduct->setProductGpproductGp($group[0]);

            getEm()->persist($productGpByProduct);
            getEm()->flush();
            AuthenticationController::insertLog('create', 'ProductGpByProduct', $product[0]->getIdproduct());
            $result = "success";
            $message = "query success";
            $mysqlData = '';
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteProductGpByProduct() {
        if ($_POST['idProductsGp']) {
            try {
                $ids = $_POST['idProductsGp'];
                for ($i = 0; $i < count($ids); $i++) {
                    $product = getEm()->getRepository("ProductProductGp")->findBy(array("idproductProductGp" => $ids[$i]));
                    getEm()->remove($product[0]);
                    getEm()->flush();
                }
                $result = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function deleteProducts() {
        if ($_POST['idProducts']) {
            try {
                $ids = $_POST['idProducts'];
                for ($i = 0; $i < count($ids); $i++) {
                    $product = getEm()->getRepository("Product")->findBy(array("idproduct" => $ids[$i]));
                    $product[0]->setActive('5');
                    $product[0]->setDateDelete(new DateTime());
                    getEm()->persist($product[0]);
                    getEm()->flush();
                    AuthenticationController::insertLog('create', 'deleteProduct', $product[0]->getIdproduct());
                }
                $result = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public static function group_products() {
        $navbar = "Entry|product";
        $groups = getEm()->getRepository('ProductGp')->findBy(array('active' => 1));
        $departments = getEm()->getRepository('Department')->findBy(array('active' => 1));
        $rules = getEm()->getRepository('OrderRule')->findBy(array('active' => 1));
        $array_answer = array(
            "groups" => $groups,
            "departments" => $departments,
            "rules" => $rules
        );
        GenericController::template("Entry", "product", "group_products", $navbar, $array_answer, 292);
    }

    public function getAllProductGp() {
        try {
            $data = array();
            $productGp = getEm()->getRepository('ProductGp')->findBy(array("active" => "1"));
            foreach ($productGp as $value) {
                if ($value->getProductGpproductGp() != null) {
                    $coreGroup = $value->getProductGpproductGp()->getName();
                } else {
                    $coreGroup = "";
                }

                if ($value->getPrinter() != null) {
                    $printer = $value->getPrinter()->getDescription();
                } else {
                    $printer = "";
                }

                if ($value->getDepartment() != null) {
                    $department = $value->getDepartment()->getName();
                } else {
                    $department = "";
                }

                if ($value->getRules() != null) {
                    $rules = $value->getRules()->getName();
                } else {
                    $rules = "";
                }

                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproductGp() . '">',
                    "id" => $value->getIdproductGp(),
                    "description" => $value->getName(),
                    "coreGroup" => $coreGroup,
                    "alternatives" => '<button class="btn btn-primary" onclick="openModalAlternatives(this.value)" value="' . $value->getIdproductgp() . '">Alternatives</button>',
                    "printer" => $printer,
                    "department" => $department,
                    "rules" => $rules
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public static function insertProductGroup() {
        try {
            $productGp = new ProductGp();
            $productGp->setName($_POST['description']);

            if ($_POST['department'] && $_POST['department'] != '0') {
                $department = getEm()->getRepository('Department')->findBy(array('iddepartment' => $_POST['department']));
                $productGp->setDepartment($department[0]);
            }

            // if($_POST['coreGroup'] && $_POST['coreGroup'] != '0'){
            //     $group = getEm()->getRepository('ProductGp')->findBy(array('idproductGp' => $_POST['coreGroup']));
            //     $productGp->setProductGpproductGp($group[0]);
            // }
            // if($_POST['printer'] && $_POST['printer'] != '0'){
            //     $printer = getEm()->getRepository('Printer')->findBy(array('idprinter' => $_POST['printer']));
            //     $productGp->setPrinter($printer[0]);
            // }
            // if($_POST['rules'] && $_POST['rules'] != '0'){
            //     $rules = getEm()->getRepository('OrderRule')->findBy(array('idorderrule' => $_POST['rules']));
            //     $productGp->setRules($rules[0]);
            // }

            $productGp->setDateCreate(new DateTime());
            $productGp->setActive(1);

            getEm()->persist($productGp);
            getEm()->flush();
            AuthenticationController::insertLog('create', 'Productgroup', $_POST['description']);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getProductGp() {
        if (isset($_POST['id'])) {
            try {
                $productGp = getEm()->getRepository('ProductGp')->findBy(array("idproductGp" => $_POST['id']));
                $mysqlData = array();
                foreach ($productGp as $value) {
                    if ($value->getProductGpproductGp() != null) {
                        $coreGroup = $value->getProductGpproductGp()->getIdproductGp();
                    } else {
                        $coreGroup = "0";
                    }
                    if ($value->getDepartment() != null) {
                        $department = $value->getDepartment()->getIddepartment();
                    } else {
                        $department = "0";
                    }
                    if ($value->getPrinter() != null) {
                        $printer = $value->getPrinter()->getIdprinter();
                    } else {
                        $printer = "0";
                    }
                    if ($value->getRules() != null) {
                        $rules = $value->getRules()->getIdorderrule();
                    } else {
                        $rules = "0";
                    }
                    $dat = array(
                        "id" => $value->getIdproductGp(),
                        "description" => $value->getName(),
                        "coreGroup" => $coreGroup,
                        "printer" => $printer,
                        "department" => $department,
                        "rules" => $rules,
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateProductGp() {
        if (isset($_POST['idproductGp'])) {
            try {
                $productGp = getEm()->getRepository('ProductGp')->findBy(array('idproductGp' => $_POST['idproductGp']));
                $productGp[0]->setName($_POST['description']);

                if ($_POST['department'] && $_POST['department'] != '0') {
                    $department = getEm()->getRepository('Department')->findBy(array('iddepartment' => $_POST['department']));
                    $productGp[0]->setDepartment($department[0]);
                } else {
                    $productGp[0]->setDepartment(null);
                }

                // if($_POST['coreGroup'] && $_POST['coreGroup'] != '0'){
                //     $group = getEm()->getRepository('ProductGp')->findBy(array('idproductGp' => $_POST['coreGroup']));
                //     $productGp[0]->setProductGpproductGp($group[0]);
                // } else {
                $productGp[0]->setProductGpproductGp(null);
                // }
                // if($_POST['printer'] && $_POST['printer'] != '0'){
                //     $printer = getEm()->getRepository('Printer')->findBy(array('idprinter' => $_POST['printer']));
                //     $productGp[0]->setPrinter($printer[0]);
                // } else {
                $productGp[0]->setPrinter(null);
                // }
                // if($_POST['rules'] && $_POST['rules'] != '0'){
                //     $rules = getEm()->getRepository('OrderRule')->findBy(array('idorderrule' => $_POST['rules']));
                //     $productGp[0]->setRules($rules[0]);
                // } else {
                $productGp[0]->setPrinter(null);
                // }

                $productGp[0]->setDateUpdate(new DateTime());
                $productGp[0]->setActive(1);

                getEm()->persist($productGp[0]);
                getEm()->flush();
                AuthenticationController::insertLog('update', 'Productgroup', $_POST['idproductGp']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteProductGp() {
        if ($_POST['idProductGroups']) {
            try {
                $ids = $_POST['idProductGroups'];
                for ($i = 0; $i < count($ids); $i++) {
                    $productGp = getEm()->getRepository("ProductGp")->findBy(array("idproductGp" => $ids[$i]));
                    $productGp[0]->setActive('3');
                    $productGp[0]->setDateDelete(new DateTime());
                    getEm()->persist($productGp[0]);
                    getEm()->flush();
                }
                $result = 'success';
                $message = 'Deleted elements successful';
                //AuthenticationController::insertLog('delete', 'Productgroup', $ids);
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function getAllProductGpAlternatives() {
        try {
            $data = array();
            $productGpAlt = getEm()->getRepository('ProductGpAlternatives')->findBy(array("active" => "1"));
            foreach ($productGpAlt as $value) {
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproductgpalternatives() . '">',
                    "id" => $value->getIdproductgpalternatives(),
                    "description" => $value->getDescription(),
                    "priceChange" => $value->getPricechange(),
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public static function insertProductGpAlternatives() {
        if ($_POST['idproductGp']) {
            try {
                if ($_POST['changePrice']) {
                    $changePrice = str_replace(',', '.', $_POST['changePrice']);
                } else {
                    $changePrice = null;
                }

                $productGpAlt = new ProductGpAlternatives();
                $productGp = getEm()->getRepository('ProductGp')->findBy(array('active' => 1, 'idproductGp' => $_POST['idproductGp']));
                $productGpAlt->setIdproductgp($productGp[0]);
                $productGpAlt->setDescription($_POST['description']);
                $productGpAlt->setPricechange($changePrice);
                $productGpAlt->setDateCreate(new DateTime());
                $productGpAlt->setActive(1);

                getEm()->persist($productGpAlt);
                getEm()->flush();
                AuthenticationController::insertLog('create', 'ProductGpAlternatives', $_POST['idproductGp']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function getProductGpAlternatives($id) {

        try {
            $data = array();
            $productGpAlt = getEm()->getRepository('ProductGpAlternatives')->findOneBy(array("idproductgpalternatives" => $id, "active" => "1"));
            $dat = array(
                "id" => $productGpAlt->getIdproductgpalternatives(),
                "description" => $productGpAlt->getDescription(),
                "priceChange" => $productGpAlt->getPricechange(),
            );
            array_push($data, $dat);
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public static function updateProductGpAlternatives($id) {
        if ($id != null) {
            try {
                if ($_POST['changePrice']) {
                    $changePrice = str_replace(',', '.', $_POST['changePrice']);
                } else {
                    $changePrice = null;
                }

                $productGpAlt = getEm()->getRepository('ProductGpAlternatives')->findOneBy(array("idproductgpalternatives" => $id, "active" => "1"));
                $productGpAlt->setDescription($_POST['description']);
                $productGpAlt->setPricechange($changePrice);
                $productGpAlt->setDateUpdate(new DateTime());
                $productGpAlt->setActive(1);

                getEm()->persist($productGpAlt);
                getEm()->flush();
                //AuthenticationController::insertLog('create', 'ProductGpAlternatives', $_POST['idproductGp']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function deleteProductGpAlternative() {
        if ($_POST['idProductGroupAlternative']) {
            try {
                $ids = $_POST['idProductGroupAlternative'];
                for ($i = 0; $i < count($ids); $i++) {
                    $productGpAlt = getEm()->getRepository("ProductGpAlternatives")->findOneBy(array("idproductgpalternatives" => $ids[$i]));
                    $productGpAlt->setActive('3');
                    $productGpAlt->setDateDelete(new DateTime());
                    getEm()->persist($productGpAlt);
                    getEm()->flush();
                }
                $result = 'success';
                $message = 'Deleted elements successful';
                //AuthenticationController::insertLog('delete', 'Productgroup', $ids);
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public static function unit() {
        $navbar = "Entry|product";
        $units = ProductController::getAllUnit(true);
        $array_answer = array(
            "units" => $units
        );
        GenericController::template("Entry", "product", "unit", $navbar, $array_answer, 296);
    }

    public static function getAllUnit($returnArray = false) {
        try {
            $entityManager = getEm();
            $unit = $entityManager->getRepository('Unit')->findBy(array("active" => "1"));
            $data = array();

            foreach ($unit as $value) {
                if ($value->getUnitunit() != null) {
                    $derivative = $value->getUnitunit()->getName();
                } else {
                    $derivative = "";
                }
                if ($value->getDivisible() != 0) {
                    $divisible = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $divisible = '';
                }
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $value->getIdunit() . '">',
                    "id" => $value->getIdunit(),
                    "name" => $value->getName(),
                    "derivative" => $derivative,
                    "ratio" => $value->getRatio(),
                    "divisible" => $divisible
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        if ($returnArray == true) {
            return $unit;
        }
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getUnit() {
        if (isset($_POST['id'])) {
            try {
                $entityManager = getEm();
                $unit = $entityManager->getRepository('Unit')->findBy(array("idunit" => $_POST['id']));
                $mysqlData = array();
                foreach ($unit as $value) {
                    if ($value->getUnitunit() != null && $value->getUnitunit() != "") {
                        $unitUnit = $value->getUnitunit()->getIdunit();
                    } else {
                        $unitUnit = 0;
                    }


                    $dat = array(
                        "id" => $value->getIdUnit(),
                        "name" => $value->getName(),
                        "ratio" => $value->getRatio(),
                        "unit" => $unitUnit,
                        "divisible" => $value->getDivisible()
                    );
                    array_push($mysqlData, $dat);
                    //break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insertUnit() {
        if (isset($_POST['nameUnit'])) {
            try {
                $unit = new Unit();
                $unit->setName($_POST['nameUnit']);
                if (isset($_POST['derivative']) && $_POST['derivative'] != 0) {
                    $unitunit = getEm()->getRepository('Unit')->findOneBy(array('idunit' => $_POST['derivative']));
                    $unit->setUnitunit($unitunit);
                }
                if (isset($_POST['derivative'])) {
                    $unit->setRatio($_POST['ratio']);
                }
                if (isset($_POST['divisible'])) {
                    $unit->setDivisible(1);
                } else {
                    $unit->setDivisible(0);
                }
                $unit->setActive(true);
                getEm()->persist($unit);
                getEm()->flush();
                AuthenticationController::insertLog('create', 'ProductUnit', $_POST['nameUnit']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateUnit() {
        if (isset($_POST['nameUnit']) && isset($_POST['idUnit'])) {
            try {
                $unit = getEm()->getRepository('Unit')->findBy(array("idunit" => $_POST['idUnit']));
                $unit[0]->setName($_POST['nameUnit']);
                if (isset($_POST['derivative']) && $_POST['derivative'] != 0) {
                    $unitunit = getEm()->getRepository('Unit')->findBy(array('idunit' => $_POST['derivative']));
                    $unit[0]->setUnitunit($unitunit[0]);
                } else {
                    $unit[0]->setUnitunit(null);
                }
                if (isset($_POST['ratio'])) {
                    $unit[0]->setRatio($_POST['ratio']);
                }
                if (isset($_POST['divisible'])) {
                    $unit[0]->setDivisible(1);
                } else {
                    $unit[0]->setDivisible(0);
                }
                $unit[0]->setDateUpdate(new DateTime());
                $unit[0]->setActive(true);
                getEm()->persist($unit[0]);
                getEm()->flush();
                AuthenticationController::insertLog('update', 'ProductUnit', $_POST['nameUnit']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteUnits() {
        if ($_POST['idUnit']) {
            try {
                $ids = $_POST['idUnit'];
                for ($i = 0; $i < count($ids); $i++) {
                    $unit = getEm()->getRepository("Unit")->findBy(array("idunit" => $ids[$i]));
                    $unit[0]->setActive('3');
                    $unit[0]->setDateDelete(new DateTime());
                    getEm()->persist($unit[0]);
                    getEm()->flush();
                    //AuthenticationController::insertLog('delete', 'ProductUnit', $_POST['idUnit']);
                }
                $result = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public static function stock() {
        $navbar = "Entry|product";
        $array_answer = array(
            "section" => getEm()->getRepository('Section')->findBy(array('active' => 1))
        );
        GenericController::template("Entry", "product", "stock_changes", $navbar, $array_answer, 298);
    }

    public static function rules() {
        $navbar = "Entry|product";
        $array_answer = array("");
        GenericController::template("Entry", "product", "rules", $navbar, $array_answer, 303);
    }

    public function getAllRules() {
        $data = array();
        $alternativePrice = "";
        $showPopup = "";
        $alternativeVat = "";
        try {
            $rules = getEm()->getRepository('OrderRule')->findBy(array("active" => "1"));
            foreach ($rules as $value) {
                $alternativePrice = $value->getAlternativePrice() == 0 ? '' : "<span class='glyphicon glyphicon-ok'></span>";
                $showPopup = $value->getShowPopup() == 0 ? '' : "<span class='glyphicon glyphicon-ok'></span>";
                $alternativeVat = $value->getAlternativeVat() == 0 ? '' : "<span class='glyphicon glyphicon-ok'></span>";

                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $value->getIdorderrule() . '" data-name="' . $value->getName() . '">',
                    "id" => $value->getIdorderrule(),
                    "name" => $value->getName(),
                    "alternativePrice" => $alternativePrice,
                    "showPopup" => $showPopup,
                    "alternativeVat" => $alternativeVat,
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insertRule() {
        if (isset($_POST['nameRules'])) {
            try {
                $alternativePrice = isset($_POST['alternativePrice']) ? 1 : 0;
                $showPopup = isset($_POST['alternativeShowPopup']) ? 1 : 0;
                $alternativeVat = isset($_POST['alternativeVat']) ? 1 : 0;

                $rules = new OrderRule();
                $rules->setName($_POST['nameRules']);
                $rules->setAlternativePrice($alternativePrice);
                $rules->setShowPopup($showPopup);
                $rules->setAlternativeVat($alternativeVat);
                $rules->setDateCreate(new DateTime());
                $rules->setActive(1);
                getEm()->persist($rules);
                getEm()->flush();
                AuthenticationController::insertLog('create', 'ProductRule', $_POST['nameRules']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getRule() {
        if (isset($_POST['id'])) {
            try {
                $rule = getEm()->getRepository('OrderRule')->findBy(array("idorderrule" => $_POST['id']));
                $mysqlData = array();
                foreach ($rule as $value) {
                    $dat = array(
                        "id" => $value->getIdorderrule(),
                        "name" => $value->getName(),
                        "alternativePrice" => $value->getAlternativePrice(),
                        "showPopup" => $value->getShowPopup(),
                        "alternativeVat" => $value->getAlternativeVat(),
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function updateRule() {
        $alternativePrice = 0;
        $showPopup = 0;
        $alternativeVat = 0;
        if (isset($_POST['nameRules']) && isset($_POST['idRules'])) {
            try {
                if (isset($_POST['alternativePrice'])) {
                    $alternativePrice = 1;
                }
                if (isset($_POST['showPopup'])) {
                    $showPopup = 1;
                }
                if (isset($_POST['alternativeVat'])) {
                    $alternativeVat = 1;
                }
                $rule = getEm()->getRepository('OrderRule')->findBy(array("idorderrule" => $_POST['idRules']));
                $rule[0]->setName($_POST['nameRules']);
                $rule[0]->setAlternativePrice($alternativePrice);
                $rule[0]->setShowPopup($showPopup);
                $rule[0]->setAlternativeVat($alternativeVat);
                $rule[0]->setDateUpdate(new DateTime());
                $rule[0]->setActive(true);
                getEm()->persist($rule[0]);
                getEm()->flush();
                AuthenticationController::insertLog('update', 'ProductRule', $_POST['nameRules']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteRules() {
        if ($_POST['idRules']) {
            try {
                $ids = $_POST['idRules'];
                for ($i = 0; $i < count($ids); $i++) {
                    $rule = getEm()->getRepository("OrderRule")->findBy(array("idorderrule" => $ids[$i]));
                    $rule[0]->setActive('3');
                    $rule[0]->setDateDelete(new DateTime());
                    getEm()->persist($rule[0]);
                    getEm()->flush();
                }
                $result = 'success';
                $message = 'Deleted elements successful';
                //AuthenticationController::insertLog('delete', 'ProductRule', $_POST['idRules']);
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public static function manufacturers() {
        $navbar = "Entry|product";
        $array_answer = array("");
        GenericController::template("Entry", "product", "manufacturers", $navbar, $array_answer, 307);
    }

    public static function getmanufacturers() {
        if (isset($_POST['id'])) {
            try {
                $manufacturer = getEm()->getRepository('Manufacturer')->findBy(array("idmanufacturer" => $_POST['id']));
                $mysqlData = array();
                foreach ($manufacturer as $value) {
                    $dat = array(
                        "name" => $value->getName(),
                        "discount" => $value->getNegociatedDiscount(),
                        "id" => $value->getIdManufacturer()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function insertManufacturer() {
        if (isset($_POST['name'])) {
            try {
                // if(isset($_POST['discount'])){
                //     $discount = str_replace('%', '', $_POST['discount']);
                // } else {
                //     $discount = null;
                // }
                $manufacturer = new Manufacturer();
                $manufacturer->setName($_POST['name']);
                $manufacturer->setNegociatedDiscount(0);
                $manufacturer->setDateCreate(new DateTime());
                $manufacturer->setActive(1);
                getEm()->persist($manufacturer);
                getEm()->flush();
                AuthenticationController::insertLog('create', 'ProductManufacturer', $_POST['name']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function updateManufacturer() {
        if (isset($_POST['name']) && isset($_POST['idmanufacturer'])) {
            try {
                $manufacturer = getEm()->getRepository('Manufacturer')->findBy(array("idmanufacturer" => $_POST['idmanufacturer']));
                // if(isset($_POST['discount'])){
                //     $discount = floatval(str_replace(',', '.', $_POST['discount']));
                // } else {
                //     $discount = 0;
                // }
                // $manufacturer[0]->setNegociatedDiscount($discount);
                $manufacturer[0]->setName($_POST['name']);
                $manufacturer[0]->setDateUpdate(new DateTime());
                $manufacturer[0]->setActive(true);
                getEm()->persist($manufacturer[0]);
                getEm()->flush();
                AuthenticationController::insertLog('update', 'ProductManufacturer', $_POST['name']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function getAllManufacturers($returnArray = false) {
        try {
            $manufacturer = getEm()->getRepository('Manufacturer')->findBy(array("active" => 1));
            $data = array();
            foreach ($manufacturer as $value) {
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $value->getIdManufacturer() . '">',
                    "id" => $value->getIdManufacturer(),
                    "name" => $value->getName(),
                        // "negociated_discount" => $value->getNegociatedDiscount().'%'
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );

        if ($returnArray == true) {
            return $manufacturer;
        }

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteManufacturer() {
        if ($_POST['idManufactures']) {
            try {
                $ids = $_POST['idManufactures'];
                for ($i = 0; $i < count($ids); $i++) {
                    $manufacturer = getEm()->getRepository("Manufacturer")->findBy(array("idmanufacturer" => $ids[$i]));
                    $manufacturer[0]->setActive('3');
                    $manufacturer[0]->setDateDelete(new DateTime());
                    getEm()->persist($manufacturer[0]);
                    getEm()->flush();
                }
                $result = 'success';
                $message = 'Deleted elements successful';
                //AuthenticationController::insertLog('delete', 'ProductManufacturer', $_POST['idManufactures']);
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public static function importation_settings() {
        $navbar = "Entry|product";
        $array_answer = array("");
        GenericController::template("Entry", "product", "importation_settings", $navbar, $array_answer, 309);
    }

    /**
     * Get all importations settings
     * @return string json_data 
     */
    public function getAllImportationSettings() {
        try {
            $data = array();
            $ImportationSettings = getEm()->getRepository('ImportationSettings')->findAll();
            foreach ($ImportationSettings as $importation) {
                if ($importation->getName() != null) {
                    $nameImport = $importation->getName();
                } else {
                    $nameImport = "";
                }

                if ($importation->getFile() != null) {
                    $fileImport = $importation->getFile();
                } else {
                    $fileImport = "";
                }

                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $importation->getIdimportationSettings() . '">',
                    "id" => $importation->getIdimportationSettings(),
                    "name" => $nameImport,
                    "file" => $fileImport,
                    "download" => '<button class="btn btn-success" onclick="downloadFile(' . $fileImport . ')"><span class="lnr lnr-download"></span></button>'
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Download of the importation settings by name of file
     * @return string json_data 
     */
    public function downloadSettings($file) {
        try {
            $zip = new ZipArchive();

            if ($zip->open('../data/information_settings/settings.zip', ZipArchive::CREATE) === true) {
                $zip->deleteName('information_setting.json');

                $zip->addFile('../data/information_settings/' . $file . '.json', 'information_setting.json');

                $zip->close();
            }

            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }
        $data = array("result" => $result, "message" => $message, "data" => "");
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * create importation setting by file
     * @return string json_data 
     */
    public function insertImportation_settings() {
        $file = $_FILES['file'];
        $name = $file['name'];
        $tmp = $file['tmp_name'];
        $newName = time();


        $extensao = explode('.', $name);
        $ext = end($extensao);

        try {
            if (empty($file)) {
                $data = "Selecione um arquivo";
                die();
            } else {
                if ($ext == 'json') {
                    if (move_uploaded_file($tmp, '../data/information_settings/' . $newName . '.json')) {
                        $jsonData = file_get_contents('../data/information_settings/' . $newName . '.json');
                        $jsonData = str_replace("[", "<--->[", $jsonData);
                        $jsonData = explode("<--->", $jsonData);

                        $this->import_person_setting($jsonData);
                        $this->import_pos_setting($jsonData);
                        $this->import_product_setting($jsonData);
                        $this->import_settings($jsonData);
                        $this->import_settings_settings($jsonData);

                        $newImportationSettings = new ImportationSettings();
                        $newImportationSettings->setName($name);
                        $newImportationSettings->setFile($newName);
                        getEm()->persist($newImportationSettings);
                        getEm()->flush();
                    } else {
                        $data = 'erro no upload';
                        die();
                    }
                } else {
                    $data = "Formato distinto - insira um arquivo .json";
                    die();
                }
            }
            $result = 'success';
            $message = 'query success';
            $data = '';
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = '';
        }

        $data = array("result" => $result, "message" => $message, "data" => $data);
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * insert the new setting by file, function called in insertImportation_settings
     * @return string json_data 
     */
    public function import_settings_settings($jsonData) {
        $Settings = getEm()->getRepository('Settings')->findBy(array("active" => 1));

        try {
            foreach ($jsonData as $data) {
                if ($data != null || $data != "") {
                    $arrData = json_decode($data);
                    $arrData = json_decode(json_encode($arrData), True);

                    foreach ($arrData as $settingFile) {
                        if (key($settingFile) == 'idsettings') {
                            foreach ($Settings as $setting) {
                                $setting->setActive(0);
                                getEm()->persist($setting);
                                getEm()->flush();
                            }

                            $newSettings = new Settings();
                            $newSettings->setImportOnlyProdName($settingFile['import_only_prod_name']);
                            $newSettings->setPrintSoldProdAtClosure($settingFile['print_sold_prod_at_closure']);
                            $newSettings->setProductNumerationMin($settingFile['product_numeration_min']);
                            $newSettings->setProductNumerationMax($settingFile['product_numeration_max']);
                            $newSettings->setApplyDiscountInvoice($settingFile['apply_discount_invoice']);

                            if ($settingFile['date_create'] != null && $settingFile['date_create'] != "") {
                                $dataCreate = new DateTime($settingFile['date_create']);
                            } else {
                                $dataCreate = new DateTime();
                            }

                            if ($settingFile['date_update'] != null && $settingFile['date_update'] != "") {
                                $dataUpdate = new DateTime($settingFile['date_update']);
                                $newSettings->setDateUpdate($dataUpdate);
                            }

                            if ($settingFile['date_delete'] != null && $settingFile['date_delete'] != "") {
                                $dataDelete = new DateTime($settingFile['date_delete']);
                                $newSettings->setDateDelete($dataDelete);
                            }

                            $newSettings->setDateCreate($dataCreate);
                            $newSettings->setActive($settingFile['active']);
                            $newSettings->setInvoiceSn($settingFile['invoice_sn']);
                            $newSettings->setPosClosureLogSn($settingFile['pos_closure_log_sn']);
                            $newSettings->setAccountingReportSn($settingFile['accounting_report_sn']);
                            $newSettings->setInvoiceReportSn($settingFile['invoice_report_sn']);
                            $newSettings->setWebshopSaleReportSn($settingFile['webshop_sale_report_sn']);
                            $newSettings->setShowPosSessionIr($settingFile['show_pos_session_ir']);
                            $newSettings->setShowPosSessionAr($settingFile['show_pos_session_ar']);
                            $newSettings->setShowProjectOfSale($settingFile['show_project_of_sale']);
                            $newSettings->setPasswordAdmin($settingFile['password_admin']);
                            $newSettings->setPasswordSetup($settingFile['password_setup']);
                            $newSettings->setPasswordClosure($settingFile['password_closure']);
                            $newSettings->setPasswordProducts($settingFile['password_products']);
                            $newSettings->setPasswordCancelO($settingFile['password_cancel_o']);
                            $newSettings->setPasswordCancelP($settingFile['password_cancel_p']);
                            $newSettings->setSenderMail($settingFile['sender_mail']);
                            $newSettings->setAlertsMail($settingFile['alerts_mail']);
                            $newSettings->setUseDefaultEmailSoft($settingFile['use_default_email_soft']);
                            $newSettings->setSmsUsers($settingFile['sms_user']);
                            $newSettings->setSmsPassword($settingFile['sms_password']);
                            $newSettings->setSendSmsAtClosure($settingFile['send_sms_at_closure']);
                            $newSettings->setMobileSmsReceiver($settingFile['mobile_sms_receiver']);
                            $newSettings->setExchangePassword($settingFile['exchange_password']);
                            $newSettings->setExchangeUsers($settingFile['exchange_user']);
                            $newSettings->setClosureInfo($settingFile['closure_info']);
                            $newSettings->setPrintQtyProdSoldAtClosure($settingFile['print_qty_prod_sold_at_closure']);
                            $newSettings->setPrintMoneyCoinInDraw($settingFile['print_money_coin_in_draw']);
                            $newSettings->setIncludeSaleByTax($settingFile['include_sale_by_tax']);
                            $newSettings->setUseCommonPrinter($settingFile['use_common_printer']);
                            $newSettings->setPrintDiscountAtClosure($settingFile['print_discount_at_closure']);
                            $newSettings->setQtyCreditNoteToPrint($settingFile['qty_credit_note_to_print']);
                            $newSettings->setCreditNoteExtraInfo($settingFile['credit_note_extra_info']);
                            $newSettings->setPrintCreditNoteUseStatus($settingFile['print_credit_note_use_status']);
                            $newSettings->setPrintGiftCardUseStatus($settingFile['print_gift_card_use_status']);
                            $newSettings->setQtyInvoiceToPrint($settingFile['qty_invoice_to_print']);
                            $newSettings->setInvQtyDecimals($settingFile['inv_qty_decimals']);
                            $newSettings->setInvUseCommonPrinter($settingFile['inv_use_common_printer']);
                            $newSettings->setInvPrintValueIncTax($settingFile['inv_print_value_inc_tax']);
                            $newSettings->setInvPrintProjectInfo($settingFile['inv_print_project_info']);
                            $newSettings->setInvFolder($settingFile['inv_folder']);
                            $newSettings->setClosureCanceledOrder($settingFile['closure_canceled_order']);
                            $newSettings->setClosureCanceledProd($settingFile['closure_canceled_prod']);
                            $newSettings->setClosureDrawOpening($settingFile['closure_draw_opening']);
                            $newSettings->setRepClosureGenPdf($settingFile['rep_closure_gen_pdf']);
                            $newSettings->setRepClosureSendReport($settingFile['rep_closure_send_report']);
                            $newSettings->setRepClosureShowScreen($settingFile['rep_closure_show_screen']);
                            $newSettings->setRepClosurePrint($settingFile['rep_closure_print']);
                            $newSettings->setRepInvClosureGenPdf($settingFile['rep_inv_closure_gen_pdf']);
                            $newSettings->setRepInvClosureSendReport($settingFile['rep_inv_closure_send_report']);
                            $newSettings->setRepInvClosureShowScreen($settingFile['rep_inv_closure_show_screen']);
                            $newSettings->setRepInvClosurePrint($settingFile['rep_inv_closure_print']);
                            $newSettings->setRepInvDetailed($settingFile['rep_inv_detailed']);
                            $newSettings->setRepInvSendCredAdm($settingFile['rep_inv_send_cred_adm']);
                            $newSettings->setRepWebPayGenPdf($settingFile['rep_web_pay_gen_pdf']);
                            $newSettings->setRepWebPaySendReport($settingFile['rep_web_pay_send_report']);
                            $newSettings->setRepWebPayShowScreen($settingFile['rep_web_pay_show_screen']);
                            $newSettings->setRepWebPayPrint($settingFile['rep_web_pay_print']);
                            $newSettings->setRepWebPayDetailed($settingFile['rep_web_pay_detailed']);
                            $testeExchange = 0;
                            $testeLanguage = 0;
                            $testeScreen = 0;
                            $testeSmsPlataform = 0;
                            foreach ($jsonData as $data1) {
                                if ($data1 != null || $data1 != "") {
                                    $arrData1 = json_decode($data1);
                                    $arrData1 = json_decode(json_encode($arrData1), True);

                                    if ($settingFile['exchange_platform_idexchange_platform'] != null && $settingFile['exchange_platform_idexchange_platform'] != "") {
                                        foreach ($arrData1 as $exchangePlataformFile) {
                                            if (key($exchangePlataformFile) == 'idexchange_platform') {

                                                if (isset($exchangePlataformFile['name'])) {

                                                    if ($settingFile['exchange_platform_idexchange_platform'] == $exchangePlataformFile['idexchange_platform']) {

                                                        $exchangeName = $exchangePlataformFile['name'];

                                                        $ExchangePlatform = getEm()->getRepository('ExchangePlatform')->findOneBy(array("active" => 1, "name" => $exchangeName));

                                                        if ($ExchangePlatform != null) {
                                                            $testeExchange++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($settingFile['language_idlanguage'] != null && $settingFile['language_idlanguage'] != "") {
                                        foreach ($arrData1 as $languageFile) {
                                            if (key($languageFile) == 'idlanguage') {

                                                if (isset($languageFile['name'])) {

                                                    if ($settingFile['language_idlanguage'] == $languageFile['idlanguage']) {

                                                        $languageName = $languageFile['name'];

                                                        $Language = getEm()->getRepository('Language')->findOneBy(array("active" => 1, "name" => $languageName));

                                                        if ($Language != null) {
                                                            $testeLanguage++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($settingFile['screen_idscreen'] != null && $settingFile['screen_idscreen'] != "") {
                                        foreach ($arrData1 as $screenFile) {
                                            if (key($screenFile) == 'idscreen') {

                                                if (isset($screenFile['name'])) {

                                                    if ($settingFile['screen_idscreen'] == $screenFile['idscreen']) {

                                                        $screenName = $screenFile['name'];

                                                        $Screen = getEm()->getRepository('Screen')->findOneBy(array("active" => 1, "name" => $screenName));

                                                        if ($Screen != null) {
                                                            $testeScreen++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($settingFile['sms_platform_idsms_platform'] != null && $settingFile['sms_platform_idsms_platform'] != "") {
                                        foreach ($arrData1 as $plataformSmsFile) {
                                            if (key($plataformSmsFile) == 'idsms_platform') {

                                                if (isset($plataformSmsFile['name'])) {

                                                    if ($settingFile['sms_platform_idsms_platform'] == $plataformSmsFile['idsms_platform']) {

                                                        $smsPlataformName = $plataformSmsFile['name'];

                                                        $SmsPlatform = getEm()->getRepository('SmsPlatform')->findOneBy(array("active" => 1, "name" => $smsPlataformName));

                                                        if ($SmsPlatform != null) {
                                                            $testeSmsPlataform++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if ($testeExchange != 0 && $testeLanguage != 0 && $testeScreen != 0 && $testeSmsPlataform != 0) {
                                $newSettings->setExchangePlatformexchangePlatform($ExchangePlatform);
                                $newSettings->setLanguagelanguage($Language);
                                $newSettings->setScreenscreen($Screen);
                                $newSettings->setSmsPlatformsmsPlatform($SmsPlatform);

                                getEm()->persist($newSettings);
                                getEm()->flush();
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            die();
        }
    }

    /**
     * insert the news settings by file, function called in insertImportation_settings
     * @return string json_data 
     */
    public function import_settings($jsonData) {
        $SettingsAccounting = getEm()->getRepository('SettingsAccounting')->findBy(array("active" => 1));
        $SettingsAdditional = getEm()->getRepository('SettingsAdditional')->findBy(array("active" => 1));
        $SettingsFactoring = getEm()->getRepository('SettingsFactoring')->findBy(array("active" => 1));
        $SettingsGeneral = getEm()->getRepository('SettingsGeneral')->findBy(array("active" => 1));
        try {

            foreach ($jsonData as $data) {
                if ($data != null || $data != "") {
                    $arrData = json_decode($data);
                    $arrData = json_decode(json_encode($arrData), True);
                    foreach ($arrData as $settingAccountFile) {
                        if (key($settingAccountFile) == 'idsettingsaccounting') {
                            foreach ($SettingsAccounting as $account) {
                                $account->setActive(0);
                                getEm()->persist($account);
                                getEm()->flush();
                            }
                            $newSettingsAccounting = new SettingsAccounting();
                            $newSettingsAccounting->setDataType($settingAccountFile['data_type']);
                            $newSettingsAccounting->setDataValue($settingAccountFile['data_value']);
                            if ($settingAccountFile['date_create'] != null && $settingAccountFile['date_create'] != "") {
                                $dataCreate = new DateTime($settingAccountFile['date_create']);
                            } else {
                                $dataCreate = new DateTime();
                            }

                            if ($settingAccountFile['date_update'] != null && $settingAccountFile['date_update'] != "") {
                                $dataUpdate = new DateTime($settingAccountFile['date_update']);
                                $newSettingsAccounting->setDateUpdate($dataUpdate);
                            }

                            if ($settingAccountFile['date_delete'] != null && $settingAccountFile['date_delete'] != "") {
                                $dataDelete = new DateTime($settingAccountFile['date_delete']);
                                $newSettingsAccounting->setDateDelete($dataDelete);
                            }

                            $newSettingsAccounting->setDateCreate($dataCreate);
                            $newSettingsAccounting->setActive($settingAccountFile['active']);
                            getEm()->persist($newSettingsAccounting);
                            getEm()->flush();
                        }
                    }

                    foreach ($arrData as $settingAdditionalFile) {
                        if (key($settingAdditionalFile) == 'idsettingsadditional') {
                            foreach ($SettingsAdditional as $additional) {
                                $additional->setActive(0);
                                getEm()->persist($additional);
                                getEm()->flush();
                            }
                            $newSettingsAdditional = new SettingsAdditional();
                            $newSettingsAdditional->setDataType($settingAdditionalFile['data_type']);
                            $newSettingsAdditional->setDataValue($settingAdditionalFile['data_value']);
                            if ($settingAdditionalFile['date_create'] != null && $settingAdditionalFile['date_create'] != "") {
                                $dataCreate = new DateTime($settingAdditionalFile['date_create']);
                            } else {
                                $dataCreate = new DateTime();
                            }

                            if ($settingAdditionalFile['date_update'] != null && $settingAdditionalFile['date_update'] != "") {
                                $dataUpdate = new DateTime($settingAdditionalFile['date_update']);
                                $newSettingsAdditional->setDateUpdate($dataUpdate);
                            }

                            if ($settingAdditionalFile['date_delete'] != null && $settingAdditionalFile['date_delete'] != "") {
                                $dataDelete = new DateTime($settingAdditionalFile['date_delete']);
                                $newSettingsAdditional->setDateDelete($dataDelete);
                            }

                            $newSettingsAdditional->setDateCreate($dataCreate);
                            $newSettingsAdditional->setActive($settingAdditionalFile['active']);
                            getEm()->persist($newSettingsAdditional);
                            getEm()->flush();
                        }
                    }

                    foreach ($arrData as $settingfactoringFile) {
                        if (key($settingfactoringFile) == 'idsettingsfactoring') {


                            foreach ($SettingsFactoring as $factoring) {
                                $factoring->setActive(0);
                                getEm()->persist($factoring);
                                getEm()->flush();
                            }


                            $newSettingfactoring = new SettingsFactoring();
                            $newSettingfactoring->setName($settingfactoringFile['name']);
                            $newSettingfactoring->setAddress($settingfactoringFile['address']);
                            $newSettingfactoring->setCodPostal($settingfactoringFile['cod_postal']);
                            $newSettingfactoring->setInvoiceText($settingfactoringFile['invoice_text']);
                            $newSettingfactoring->setBankAccount($settingfactoringFile['bank_account']);
                            $newSettingfactoring->setKidLayout($settingfactoringFile['kid_layout']);
                            $newSettingfactoring->setClientId($settingfactoringFile['client_id']);
                            $newSettingfactoring->setEmail($settingfactoringFile['email']);
                            $newSettingfactoring->setAddInvoiceText($settingfactoringFile['add_invoice_text']);
                            if ($settingfactoringFile['date_create'] != null && $settingfactoringFile['date_create'] != "") {
                                $dataCreate = new DateTime($settingfactoringFile['date_create']);
                            } else {
                                $dataCreate = new DateTime();
                            }

                            if ($settingfactoringFile['date_update'] != null && $settingfactoringFile['date_update'] != "") {
                                $dataUpdate = new DateTime($settingfactoringFile['date_update']);
                                $newSettingfactoring->setDateUpdate($dataUpdate);
                            }

                            if ($settingfactoringFile['date_delete'] != null && $settingfactoringFile['date_delete'] != "") {
                                $dataDelete = new DateTime($settingfactoringFile['date_delete']);
                                $newSettingfactoring->setDateDelete($dataDelete);
                            }

                            $newSettingfactoring->setDateCreate($dataCreate);
                            $newSettingfactoring->setActive($settingfactoringFile['active']);

                            $testeFatctoring = 0;
                            foreach ($jsonData as $data1) {
                                if ($data1 != null || $data1 != "") {
                                    $arrData1 = json_decode($data1);
                                    $arrData1 = json_decode(json_encode($arrData1), True);
                                    if ($settingfactoringFile['company_factoring'] != null && $settingfactoringFile['company_factoring'] != "") {
                                        foreach ($arrData1 as $companyFactoringFile) {
                                            if (key($companyFactoringFile) == 'idcompanyfactoring') {

                                                if (isset($companyFactoringFile['name'])) {

                                                    if ($companyFactoringFile['idcompanyfactoring'] == $settingfactoringFile['company_factoring']) {

                                                        $factoringName = $companyFactoringFile['name'];

                                                        $CompanyFactoring = getEm()->getRepository('CompanyFactoring')->findOneBy(array("active" => 1, "name" => $factoringName));

                                                        if ($CompanyFactoring != null) {
                                                            $testeFatctoring++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if ($testeFatctoring != null) {
                                $newSettingfactoring->setCompanyFactoring($CompanyFactoring);
                                getEm()->persist($newSettingfactoring);
                                getEm()->flush();
                            }
                        }
                    }


                    foreach ($arrData as $settingGeneralFile) {
                        if (key($settingGeneralFile) == 'idsettingsgeneral') {
                            foreach ($SettingsGeneral as $general) {
                                $general->setActive(0);
                                getEm()->persist($general);
                                getEm()->flush();
                            }
                            $newSettingsGeneral = new SettingsGeneral();
                            $newSettingsGeneral->setDataType($settingGeneralFile['data_type']);
                            $newSettingsGeneral->setDataValue($settingGeneralFile['data_value']);
                            if ($settingGeneralFile['date_create'] != null && $settingGeneralFile['date_create'] != "") {
                                $dataCreate = new DateTime($settingGeneralFile['date_create']);
                            } else {
                                $dataCreate = new DateTime();
                            }

                            if ($settingGeneralFile['date_update'] != null && $settingGeneralFile['date_update'] != "") {
                                $dataUpdate = new DateTime($settingGeneralFile['date_update']);
                                $newSettingsGeneral->setDateUpdate($dataUpdate);
                            }

                            if ($settingGeneralFile['date_delete'] != null && $settingGeneralFile['date_delete'] != "") {
                                $dataDelete = new DateTime($settingGeneralFile['date_delete']);
                                $newSettingsGeneral->setDateDelete($dataDelete);
                            }

                            $newSettingsGeneral->setDateCreate($dataCreate);
                            $newSettingsGeneral->setActive($settingGeneralFile['active']);
                            getEm()->persist($newSettingsGeneral);
                            getEm()->flush();
                        }
                    }
                }
            }
        } catch (Exception $e) {

            echo $e->getMessage();
            die();
        }
    }

    /**
     * insert the new setting of products by file, function called in insertImportation_settings
     * @return string json_data 
     */
    public function import_product_setting($jsonData) {
        $Product = getEm()->getRepository('Product')->findBy(array("active" => 1));
        $Category = getEm()->getRepository('Category')->findBy(array("active" => 1));
        $ProductGp = getEm()->getRepository('ProductGp')->findBy(array("active" => 1));
        try {

            foreach ($jsonData as $data) {
                if ($data != null || $data != "") {
                    $arrData = json_decode($data);
                    $arrData = json_decode(json_encode($arrData), True);

                    foreach ($arrData as $dataInternal) {
                        if (key($dataInternal) == 'idproduct') {
                            if (isset($dataInternal['name'])) {
                                foreach ($Product as $product) {
                                    if ($product->getName() == $dataInternal['name']) {
                                        foreach ($jsonData as $data1) {
                                            if ($data1 != null || $data1 != "") {
                                                $arrData1 = json_decode($data1);
                                                $arrData1 = json_decode(json_encode($arrData1), True);

                                                foreach ($arrData1 as $productWtyFile) {

                                                    if (key($productWtyFile) == 'idproduct_wty') {
                                                        if ($productWtyFile['product_idproduct'] == $dataInternal['idproduct']) {

                                                            $ProductWty = getEm()->getRepository('ProductWty')->findBy(array("active" => 1));

                                                            foreach ($ProductWty as $wty) {
                                                                if ($wty->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                    $wty->setActive(0);
                                                                    getEm()->persist($wty);
                                                                    getEm()->flush();
                                                                }
                                                            }

                                                            $newProductWty = new ProductWty();
                                                            $newProductWty->setValue($productWtyFile['value']);
                                                            if ($productWtyFile['date_create'] != null && $productWtyFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productWtyFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productWtyFile['date_update'] != null && $productWtyFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productWtyFile['date_update']);
                                                                $newProductWty->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productWtyFile['date_delete'] != null && $productWtyFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productWtyFile['date_delete']);
                                                                $newProductWty->setDateDelete($dataDelete);
                                                            }

                                                            $newProductWty->setDateCreate($dataCreate);
                                                            $newProductWty->setActive($productWtyFile['active']);
                                                            $newProductWty->setProductproduct($product);
                                                            $testeWarranty = 0;
                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);
                                                                    if ($productWtyFile['warranty_idwarranty'] != null && $productWtyFile['warranty_idwarranty'] != "") {
                                                                        foreach ($arrData2 as $warrantyFile) {
                                                                            if (key($warrantyFile) == "idwarranty") {

                                                                                if (isset($warrantyFile['name'])) {

                                                                                    if ($productWtyFile['warranty_idwarranty'] == $warrantyFile['idwarranty']) {

                                                                                        $warrantyName = $warrantyFile['name'];

                                                                                        $Warranty = getEm()->getRepository('Warranty')->findOneBy(array("active" => 1, "name" => $warrantyName));

                                                                                        if ($Warranty != null) {
                                                                                            $testeWarranty++;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if ($testeWarranty != 0) {
                                                                $newProductWty->setWarrantywarranty($Warranty);
                                                                getEm()->persist($newProductWty);
                                                                getEm()->flush();
                                                            }
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productWholesalePriceFile) {
                                                    if (key($productWholesalePriceFile) == 'id_product_wholesale_price') {
                                                        if ($productWholesalePriceFile['product_idproduct'] == $dataInternal['idproduct']) {

                                                            $ProductWholesalePrice = getEm()->getRepository('ProductWholesalePrice')->findBy(array("active" => 1));

                                                            foreach ($ProductWholesalePrice as $wholePrice) {
                                                                if ($wholePrice->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                    $wholePrice->setActive(0);
                                                                    getEm()->persist($wholePrice);
                                                                    getEm()->flush();
                                                                }
                                                            }
                                                            $newProductWholesalePrice = new ProductWholesalePrice();
                                                            $newProductWholesalePrice->setUnitPrice($productWholesalePriceFile['unit_price']);
                                                            $newProductWholesalePrice->setQtd($productWholesalePriceFile['qtd']);
                                                            $newProductWholesalePrice->setPriceForQuantity($productWholesalePriceFile['price_for_quantity']);
                                                            if ($productWholesalePriceFile['date_create'] != null && $productWholesalePriceFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productWholesalePriceFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productWholesalePriceFile['date_update'] != null && $productWholesalePriceFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productWholesalePriceFile['date_update']);
                                                                $newProductWholesalePrice->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productWholesalePriceFile['date_delete'] != null && $productWholesalePriceFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productWholesalePriceFile['date_delete']);
                                                                $newProductWholesalePrice->setDateDelete($dataDelete);
                                                            }

                                                            $newProductWholesalePrice->setDateCreate($dataCreate);
                                                            $newProductWholesalePrice->setActive($productWholesalePriceFile['active']);
                                                            $newProductWholesalePrice->setProductproduct($product);
                                                            getEm()->persist($newProductWholesalePrice);
                                                            getEm()->flush();
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productSerialNumberFile) {
                                                    if (key($productSerialNumberFile) == 'idproduct_serial_number') {
                                                        if ($productSerialNumberFile['product_idproduct'] == $dataInternal['idproduct']) {

                                                            $ProductSerialNumber = getEm()->getRepository('ProductSerialNumber')->findBy(array("active" => 1));

                                                            foreach ($ProductSerialNumber as $serial) {
                                                                if ($serial->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                    $serial->setActive(0);
                                                                    getEm()->persist($serial);
                                                                    getEm()->flush();
                                                                }
                                                            }

                                                            $newProductSerialNumber = new ProductSerialNumber();
                                                            $newProductSerialNumber->setSerial($productSerialNumberFile['serial']);
                                                            if ($productSerialNumberFile['date_create'] != null && $productSerialNumberFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productSerialNumberFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productSerialNumberFile['date_update'] != null && $productSerialNumberFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productSerialNumberFile['date_update']);
                                                                $newProductSerialNumber->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productSerialNumberFile['date_delete'] != null && $productSerialNumberFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productSerialNumberFile['date_delete']);
                                                                $newProductSerialNumber->setDateDelete($dataDelete);
                                                            }

                                                            $newProductSerialNumber->setDateCreate($dataCreate);
                                                            $newProductSerialNumber->setActive($productSerialNumberFile['active']);

                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);

                                                                    if ($productSerialNumberFile['order_line_idorder_line'] != null && $productSerialNumberFile['order_line_idorder_line'] != "") {
                                                                        foreach ($arrData2 as $orderLine) {
                                                                            if (key($orderLine) == "idorder_line") {
                                                                                if ($productSerialNumberFile['order_line_idorder_line'] == $orderLine['idorder_line']) {
                                                                                    $OrderLine = getEm()->getRepository('OrderLine')->findBy(array("active" => 1));
                                                                                    foreach ($OrderLine as $order) {
                                                                                        if ($order->getProductproduct()->getIdproduct() == $product->getIdproduct()) {
                                                                                            $newProductSerialNumber->setOrderLineorderLine($order);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    if ($productSerialNumberFile['section_idsection'] != null && $productSerialNumberFile['section_idsection'] != "") {
                                                                        foreach ($arrData2 as $sectionFile) {
                                                                            if (key($sectionFile) == "idsection") {

                                                                                if (isset($sectionFile['name'])) {

                                                                                    if ($productSerialNumberFile['section_idsection'] == $sectionFile['idsection']) {

                                                                                        $sectionName = $sectionFile['name'];

                                                                                        $Section = getEm()->getRepository('Section')->findOneBy(array("active" => 1, "name" => $sectionName));

                                                                                        if ($Section != null) {
                                                                                            $newProductSerialNumber->setSectionsection($Section);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            $newProductSerialNumber->setProductproduct($product);
                                                            getEm()->persist($newProductSerialNumber);
                                                            getEm()->flush();
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productPromotionalFile) {
                                                    if (key($productPromotionalFile) == 'idproductpromotional') {
                                                        if ($productPromotionalFile['idproduct'] == $dataInternal['idproduct']) {
                                                            $ProductPromotional = getEm()->getRepository('ProductPromotional')->findBy(array("active" => 1));
                                                            foreach ($ProductPromotional as $promotional) {
                                                                if ($promotional->getIdproduct()->getIdproduct() == $product->getIdproduct()) {
                                                                    if ($productPromotionalFile['initial_date'] != null && $productPromotionalFile['initial_date'] != "") {
                                                                        $initialDate = new DateTime($productPromotionalFile['initial_date']);
                                                                        $promotional->setInitialDate($initialDate);
                                                                    }
                                                                    $promotional->setActive(0);
                                                                    getEm()->persist($promotional);
                                                                    getEm()->flush();
                                                                }
                                                            }

                                                            $newProductPromotional = new ProductPromotional();
                                                            $newProductPromotional->setGrossPrice($productPromotionalFile['gross_price']);
                                                            if ($productPromotionalFile['initial_date'] != null && $productPromotionalFile['initial_date'] != "") {
                                                                $initialDate = new DateTime($productPromotionalFile['initial_date']);
                                                                $newProductPromotional->setInitialDate($initialDate);
                                                            }
                                                            $newProductPromotional->setPriceWithoutTax($productPromotionalFile['price_without_tax']);
                                                            if ($productPromotionalFile['final_date'] != null && $productPromotionalFile['final_date'] != "") {
                                                                $finalDate = new DateTime($productPromotionalFile['final_date']);
                                                                $newProductPromotional->setFinalDate($finalDate);
                                                            }
                                                            $newProductPromotional->setWeek($productPromotionalFile['week']);
                                                            if ($productPromotionalFile['date_create'] != null && $productPromotionalFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productPromotionalFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productPromotionalFile['date_update'] != null && $productPromotionalFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productPromotionalFile['date_update']);
                                                                $newProductPromotional->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productPromotionalFile['date_delete'] != null && $productPromotionalFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productPromotionalFile['date_delete']);
                                                                $newProductPromotional->setDateDelete($dataDelete);
                                                            }

                                                            $newProductPromotional->setDateCreate($dataCreate);
                                                            $newProductPromotional->setActive($productPromotionalFile['active']);
                                                            $newProductPromotional->setIdproduct($product);
                                                            getEm()->persist($newProductPromotional);
                                                            getEm()->flush();
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productProductGpFile) {

                                                    if (key($productProductGpFile) == 'idproduct_product_gp') {
                                                        if ($dataInternal['idproduct'] == $productProductGpFile['product_idproduct']) {

                                                            $ProductProductGp = getEm()->getRepository('ProductProductGp')->findAll();
                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);
                                                                    foreach ($arrData2 as $productGpFile) {
                                                                        //var_dump($productGpFile);
                                                                        if (key($productGpFile) == 'idproduct_gp') {
                                                                            if (isset($productGpFile['name'])) {

                                                                                foreach ($ProductGp as $productGp) {
                                                                                    if ($productGp->getName() == $productGpFile['name']) {
                                                                                        if ($productProductGpFile['product_gp_idproduct_gp'] == $productGpFile['idproduct_gp']) {


                                                                                            $ProductProductGp = getEm()->getRepository('ProductProductGp')->findBy(array("productGpproductGp" => $productGp->getIdproductGp(), "productproduct" => $product->getIdproduct()));

                                                                                            if ($ProductProductGp != null) {
                                                                                                foreach ($ProductProductGp as $productProductGp) {
                                                                                                    $productProductGp->setProductGpproductGp($productGp);
                                                                                                    $productProductGp->setProductproduct($product);
                                                                                                    getEm()->persist($productProductGp);
                                                                                                    getEm()->flush();
                                                                                                }
                                                                                            } else {
                                                                                                $newProductProductGp = new ProductProductGp();
                                                                                                $newProductProductGp->setProductGpproductGp($productGp);
                                                                                                $newProductProductGp->setProductproduct($product);
                                                                                                getEm()->persist($newProductProductGp);
                                                                                                getEm()->flush();
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }


                                                foreach ($arrData1 as $productOrderFile) {
                                                    if (key($productOrderFile) == 'idproduct') {
                                                        if (!isset($productOrderFile['name'])) {
                                                            if ($productOrderFile['product_idproduct'] == $dataInternal['idproduct']) {

                                                                $ProductOrder = getEm()->getRepository('ProductOrder')->findBy(array("active" => 1));
                                                                foreach ($ProductOrder as $order) {
                                                                    if ($order->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                        $order->setActive(0);
                                                                        getEm()->persist($order);
                                                                        getEm()->flush();
                                                                    }
                                                                }

                                                                $newProductOrder = new ProductOrder();
                                                                $newProductOrder->setPercent($productOrderFile['percent']);
                                                                $newProductOrder->setTheSalePriceWill($productOrderFile['the_sale_price_will']);
                                                                $newProductOrder->setDoNotPutASellingPrice($productOrderFile['do_not_put_a_selling_price']);
                                                                $newProductOrder->setDoNotAllowDiscount($productOrderFile['do_not_allow_discount']);
                                                                $newProductOrder->setProductOnlyToBuy($productOrderFile['product_only_to_buy']);
                                                                $newProductOrder->setPrintProductInformationInTaxCoupon($productOrderFile['print_product_information_in_tax_coupon']);
                                                                $newProductOrder->setFillQuantityBasedOnTheInformation($productOrderFile['fill_quantity_based_on_the_information']);
                                                                $newProductOrder->setHideFromStatistics($productOrderFile['hide_from_statistics']);
                                                                $newProductOrder->setDisplayAlternate($productOrderFile['display_alternate']);
                                                                $newProductOrder->setThePurchasePrice($productOrderFile['the_purchase_price']);
                                                                $newProductOrder->setViewProductDescription($productOrderFile['view_product_description']);
                                                                $newProductOrder->setDoNotShowProduct($productOrderFile['do_not_show_product']);


                                                                if ($productOrderFile['date_create'] != null && $productOrderFile['date_create'] != "") {
                                                                    $dataCreate = new DateTime($productOrderFile['date_create']);
                                                                } else {
                                                                    $dataCreate = new DateTime();
                                                                }

                                                                if ($productOrderFile['date_update'] != null && $productOrderFile['date_update'] != "") {
                                                                    $dataUpdate = new DateTime($productOrderFile['date_update']);
                                                                    $newProductOrder->setDateUpdate($dataUpdate);
                                                                }

                                                                if ($productOrderFile['date_delete'] != null && $productOrderFile['date_delete'] != "") {
                                                                    $dataDelete = new DateTime($productOrderFile['date_delete']);
                                                                    $newProductOrder->setDateDelete($dataDelete);
                                                                }

                                                                $newProductOrder->setDateCreate($dataCreate);
                                                                $newProductOrder->setActive($productOrderFile['active']);
                                                                $newProductOrder->setProductproduct($product);
                                                                getEm()->persist($newProductOrder);
                                                                getEm()->flush();
                                                            }
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productSectionFile) {
                                                    if (key($productSectionFile) == 'idproduct_in_section') {
                                                        if ($productSectionFile['product_idproduct'] == $dataInternal['idproduct']) {

                                                            $ProductInSection = getEm()->getRepository('ProductInSection')->findBy(array("active" => 1));
                                                            foreach ($ProductInSection as $section) {
                                                                if ($section->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                    $section->setActive(0);
                                                                    getEm()->persist($section);
                                                                    getEm()->flush();
                                                                }
                                                            }
                                                            $newProductInSection = new ProductInSection();
                                                            $newProductInSection->setQty($productSectionFile['qty']);
                                                            $testeSection = 0;
                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);
                                                                    if ($productSectionFile['section_idsection'] != null) {
                                                                        foreach ($arrData2 as $section) {
                                                                            if (key($section) == "idsection") {
                                                                                if (isset($section['name'])) {

                                                                                    if ($productSectionFile['section_idsection'] == $section['idsection']) {

                                                                                        $sectionName = $section['name'];


                                                                                        $Section = getEm()->getRepository('Section')->findOneBy(array("active" => 1, "name" => $sectionName));

                                                                                        if ($Section != null) {

                                                                                            $testeSection++;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            $newProductInSection->setProductproduct($product);
                                                            if ($productSectionFile['date'] != null && $productSectionFile['date'] != "") {
                                                                $dataDate = new DateTime($productSectionFile['date']);
                                                                $newProductInSection->setDate($dataDate);
                                                            }

                                                            if ($productSectionFile['expiration_date'] != null && $productSectionFile['expiration_date'] != "") {
                                                                $dataExpiration = new DateTime($productSectionFile['expiration_date']);
                                                                $newProductInSection->setExpirationdate($dataExpiration);
                                                            }

                                                            if ($productSectionFile['date_create'] != null && $productSectionFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productSectionFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productSectionFile['date_update'] != null && $productSectionFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productSectionFile['date_update']);
                                                                $newProductInSection->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productSectionFile['date_delete'] != null && $productSectionFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productSectionFile['date_delete']);
                                                                $newProductInSection->setDateDelete($dataDelete);
                                                            }

                                                            $newProductInSection->setDateCreate($dataCreate);

                                                            $newProductInSection->setActive($productSectionFile['active']);
                                                            if ($testeSection != 0) {
                                                                $newProductInSection->setSectionsection($Section);
                                                                getEm()->persist($newProductInSection);
                                                                getEm()->flush();
                                                            }
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productIncludeFile) {
                                                    if (key($productIncludeFile) == 'id_product_ included_and_substitute') {

                                                        if ($dataInternal['idproduct'] == $productIncludeFile['product_idproduct']) {

                                                            $ProductIncludedAndSubstitute = getEm()->getRepository('ProductIncludedAndSubstitute')->findBy(array("active" => 1));

                                                            foreach ($ProductIncludedAndSubstitute as $include) {
                                                                if ($include->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                    $include->setActive(0);
                                                                    getEm()->persist($include);
                                                                    getEm()->flush();
                                                                }
                                                            }
                                                            $newProductInclude = new ProductIncludedAndSubstitute();
                                                            $newProductInclude->setType($productIncludeFile['type']);
                                                            $newProductInclude->setPrice($productIncludeFile['price']);
                                                            if ($productIncludeFile['date_create'] != null && $productIncludeFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productIncludeFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productIncludeFile['date_update'] != null && $productIncludeFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productIncludeFile['date_update']);
                                                                $newProductInclude->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productIncludeFile['date_delete'] != null && $productIncludeFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productIncludeFile['date_delete']);
                                                                $newProductInclude->setDateDelete($dataDelete);
                                                            }

                                                            $newProductInclude->setDateCreate($dataCreate);

                                                            $newProductInclude->setProductproduct($product);
                                                            $testeProductSubs = 0;
                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);
                                                                    if ($productIncludeFile['product_incl_subs'] != null) {
                                                                        foreach ($arrData2 as $productfile1) {
                                                                            if (key($productfile1) == "idproduct") {

                                                                                if (isset($productfile1['name'])) {

                                                                                    if ($productIncludeFile['product_incl_subs'] == $productfile1['idproduct']) {

                                                                                        $product1Name = $productfile1['name'];

                                                                                        $Product1 = getEm()->getRepository('Product')->findOneBy(array("active" => 1, "name" => $product1Name));

                                                                                        if ($Product1 != null) {
                                                                                            $testeProductSubs++;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if ($testeProductSubs != 0) {
                                                                $newProductInclude->setProductInclSubs($Product1);
                                                                getEm()->persist($newProductInclude);
                                                                getEm()->flush();
                                                            }
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productComponentFile) {
                                                    if (key($productComponentFile) == 'idproduct_component') {
                                                        if ($dataInternal['idproduct'] == $productComponentFile['product_idproduct']) {

                                                            $ProductComponent = getEm()->getRepository('ProductComponent')->findBy(array("active" => 1));

                                                            foreach ($ProductComponent as $component) {
                                                                if ($component->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                    $component->setActive(0);
                                                                    getEm()->persist($component);
                                                                    getEm()->flush();
                                                                }
                                                            }

                                                            $newProductComponent = new ProductComponent();
                                                            $newProductComponent->setQty($productComponentFile['qty']);
                                                            $newProductComponent->setCostPrice($productComponentFile['cost_price']);
                                                            $newProductComponent->setRetailPrice($productComponentFile['retail_price']);

                                                            if ($productComponentFile['date_create'] != null && $productComponentFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productComponentFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productComponentFile['date_update'] != null && $productComponentFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productComponentFile['date_update']);
                                                                $newProductComponent->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productComponentFile['date_delete'] != null && $productComponentFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productComponentFile['date_delete']);
                                                                $newProductComponent->setDateDelete($dataDelete);
                                                            }

                                                            $newProductComponent->setDateCreate($dataCreate);
                                                            $newProductComponent->setActive($productComponentFile['active']);
                                                            $newProductComponent->setProductproduct($product);
                                                            $testeProduct = 0;
                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);
                                                                    if ($productComponentFile['product_idproduct1'] != null) {
                                                                        foreach ($arrData2 as $productfile1) {
                                                                            if (key($productfile1) == "idproduct") {

                                                                                if (isset($productfile1['name'])) {

                                                                                    if ($productComponentFile['product_idproduct1'] == $productfile1['idproduct']) {

                                                                                        $product1Name = $productfile1['name'];

                                                                                        $Product1 = getEm()->getRepository('Product')->findOneBy(array("active" => 1, "name" => $product1Name));

                                                                                        if ($Product1 != null) {
                                                                                            $testeProduct++;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            if ($testeProduct != 0) {
                                                                $newProductComponent->setProductproduct1($Product1);
                                                                getEm()->persist($newProductComponent);
                                                                getEm()->flush();
                                                            }
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productCategoryFile) {
                                                    if (key($productCategoryFile) == 'idproduct_category') {
                                                        if ($dataInternal['idproduct'] == $productCategoryFile['product_idproduct']) {

                                                            $ProductCategory = getEm()->getRepository('ProductCategory')->findAll();
                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);
                                                                    foreach ($arrData2 as $categoryFile) {
                                                                        if (key($categoryFile) == 'idcategory') {
                                                                            if (isset($categoryFile['name'])) {

                                                                                foreach ($Category as $category) {
                                                                                    if ($category->getName() == $categoryFile['name']) {
                                                                                        if ($productCategoryFile['category_idcategory'] == $categoryFile['idcategory']) {
                                                                                            $ProductCategory = getEm()->getRepository('ProductCategory')->findBy(array("categorycategory" => $category->getIdcategory(), "productproduct" => $product->getIdproduct()));

                                                                                            if ($ProductCategory != null) {
                                                                                                foreach ($ProductCategory as $productCategory) {
                                                                                                    $productCategory->setCategorycategory($category);
                                                                                                    $productCategory->setProductproduct($product);
                                                                                                    getEm()->persist($productCategory);
                                                                                                    getEm()->flush();
                                                                                                }
                                                                                            } else {
                                                                                                $newProductCategory = new ProductCategory();
                                                                                                $newProductCategory->setCategorycategory($category);
                                                                                                $newProductCategory->setProductproduct($product);
                                                                                                getEm()->persist($newProductCategory);
                                                                                                getEm()->flush();
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }


                                                foreach ($arrData1 as $productAdditionalItemFile) {
                                                    if (key($productAdditionalItemFile) == 'idproductadditionalitem') {


                                                        if ($dataInternal['idproduct'] == $productAdditionalItemFile['product_idproduct']) {

                                                            $ProductAdditionalItem = getEm()->getRepository('ProductAdditionalItem')->findBy(array("active" => 1));

                                                            foreach ($ProductAdditionalItem as $additionalItem) {
                                                                if ($additionalItem->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                    $additionalItem->setActive(0);
                                                                    getEm()->persist($additionalItem);
                                                                    getEm()->flush();
                                                                }
                                                            }

                                                            $newProductAdditionalItem = new ProductAdditionalItem();
                                                            $newProductAdditionalItem->setPriceType($productAdditionalItemFile['price_type']);
                                                            $newProductAdditionalItem->setPrice($productAdditionalItemFile['price']);
                                                            $newProductAdditionalItem->setQty($productAdditionalItemFile['qty']);
                                                            $newProductAdditionalItem->setBrukFactor($productAdditionalItemFile['bruk_factor']);

                                                            if ($productAdditionalItemFile['date_create'] != null && $productAdditionalItemFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productAdditionalItemFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productAdditionalItemFile['date_update'] != null && $productAdditionalItemFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productAdditionalItemFile['date_update']);
                                                                $newProductAdditionalItem->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productAdditionalItemFile['date_delete'] != null && $productAdditionalItemFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productAdditionalItemFile['date_delete']);
                                                                $newProductAdditionalItem->setDateDelete($dataDelete);
                                                            }

                                                            $newProductAdditionalItem->setDateCreate($dataCreate);
                                                            $newProductAdditionalItem->setActive($productAdditionalItemFile['active']);


                                                            $testeProductGp = 0;
                                                            $testeAditionalIntem = 0;
                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);

                                                                    if ($productAdditionalItemFile['group_idgroup'] != null) {
                                                                        foreach ($arrData2 as $productGroup) {

                                                                            if (key($productGroup) == "idproduct_gp") {

                                                                                if ($productGroup['idproduct_gp'] == $productAdditionalItemFile['group_idgroup']) {

                                                                                    $productGpName = $productGroup['name'];

                                                                                    $ProductGp = getEm()->getRepository('ProductGp')->findOneBy(array("active" => 1, "name" => $productGpName));
                                                                                    if ($ProductGp != null) {
                                                                                        $testeProductGp++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    if ($productAdditionalItemFile['id_product_additional'] != null) {
                                                                        foreach ($arrData2 as $productAdditional1) {

                                                                            if (key($productAdditional1) == "idproductadditional") {

                                                                                if ($productAdditional1['idproductadditional'] == $productAdditionalItemFile['id_product_additional']) {

                                                                                    $productId = $product->getIdproduct();
                                                                                    $ProductAdditional = getEm()->getRepository('ProductAdditional')->findOneBy(array("active" => 1, "productproduct" => $productId));
                                                                                    if ($ProductAdditional != null) {
                                                                                        $testeAditionalIntem++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            $newProductAdditionalItem->setProductproduct($product);
                                                            if ($testeProductGp != 0 && $testeAditionalIntem != 0) {
                                                                $newProductAdditionalItem->setIdProductAdditional($ProductAdditional);
                                                                $newProductAdditionalItem->setGroupgroup($ProductGp);
                                                                getEm()->persist($newProductAdditionalItem);
                                                                getEm()->flush();
                                                            }
                                                        }
                                                    }
                                                }

                                                foreach ($arrData1 as $productAdditionalFile) {
                                                    if (key($productAdditionalFile) == 'idproductadditional') {

                                                        if ($dataInternal['idproduct'] == $productAdditionalFile['product_idproduct']) {

                                                            $ProductAdditional = getEm()->getRepository('ProductAdditional')->findBy(array("active" => 1));
                                                            foreach ($ProductAdditional as $additional) {
                                                                if ($additional->getProductproduct()->getIdproduct() == $product->getIdproduct()) {

                                                                    $additional->setActive(0);
                                                                    getEm()->persist($additional);
                                                                    getEm()->flush();
                                                                }
                                                            }

                                                            $newProductAdditional = new ProductAdditional();
                                                            $newProductAdditional->setOrder($productAdditionalFile['order']);
                                                            $newProductAdditional->setAdditionalType($productAdditionalFile['additional_type']);
                                                            $newProductAdditional->setInfo($productAdditionalFile['info']);

                                                            if ($productAdditionalFile['date_create'] != null && $productAdditionalFile['date_create'] != "") {
                                                                $dataCreate = new DateTime($productAdditionalFile['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($productAdditionalFile['date_update'] != null && $productAdditionalFile['date_update'] != "") {
                                                                $dataUpdate = new DateTime($productAdditionalFile['date_update']);
                                                                $newProductAdditional->setDateUpdate($dataUpdate);
                                                            }

                                                            if ($productAdditionalFile['date_delete'] != null && $productAdditionalFile['date_delete'] != "") {
                                                                $dataDelete = new DateTime($productAdditionalFile['date_delete']);
                                                                $newProductAdditional->setDateDelete($dataDelete);
                                                            }

                                                            $newProductAdditional->setDateCreate($dataCreate);
                                                            $newProductAdditional->setActive($productAdditionalFile['active']);
                                                            $newProductAdditional->setProductproduct($product);
                                                            getEm()->persist($newProductAdditional);
                                                            getEm()->flush();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {

            echo $e->getMessage();
            die();
        }
    }

    /**
     * insert the new setting of POS by file, function called in insertImportation_settings
     * @return string json_data 
     */
    public function import_pos_setting($jsonData) {
        $Pos = getEm()->getRepository('Pos')->findBy(array("active" => 1));
        $PosSettingsClosingParameters = getEm()->getRepository('PosSettingsClosingParameters')->findBy(array("active" => 1));
        $PosSettingsCreditNotes = getEm()->getRepository('PosSettingsCreditNotes')->findBy(array("active" => 1));
        $PosSettingsReceipts = getEm()->getRepository('PosSettingsReceipts')->findBy(array("active" => 1));
        $PosSettingsFeatures = getEm()->getRepository('PosSettingsFeatures')->findBy(array("active" => 1));
        $PosSettingsDeliveryList = getEm()->getRepository('PosSettingsDeliveryList')->findBy(array("active" => 1));
        try {
            $testReceipts = 0;
            $testfeatures = 0;
            $testDeliveryList = 0;
            $testCreditNotes = 0;
            $testClosingParameters = 0;
            foreach ($jsonData as $data) {
                if ($data != null || $data != "") {
                    $arrData = json_decode($data);
                    $arrData = json_decode(json_encode($arrData), True);
                    foreach ($arrData as $dataInternal) {


                        if (key($dataInternal) == "idpossettingsreceipts") {
                            $testReceipts++;
                        } else if (key($dataInternal) == "idpossettingsfeatures") {
                            $testfeatures++;
                        } else if (key($dataInternal) == "idpossettingsdeliverylist") {
                            $testDeliveryList++;
                        } else if (key($dataInternal) == "idpossettingscreditnotes") {
                            $testCreditNotes++;
                        } else if (key($dataInternal) == "idpossettingsclosingparameters") {
                            $testClosingParameters++;
                        }
                    }
                    if ($testReceipts != 0) {
                        foreach ($PosSettingsReceipts as $receipts) {
                            $receipts->setActive(0);
                            getEm()->persist($receipts);
                            getEm()->flush();
                        }
                    }
                    if ($testfeatures != 0) {
                        foreach ($PosSettingsFeatures as $features) {
                            $features->setActive(0);
                            getEm()->persist($features);
                            getEm()->flush();
                        }
                    }
                    if ($testDeliveryList != 0) {
                        foreach ($PosSettingsDeliveryList as $deliveryList) {
                            $deliveryList->setActive(0);
                            getEm()->persist($deliveryList);
                            getEm()->flush();
                        }
                    }
                    if ($testCreditNotes != 0) {
                        foreach ($PosSettingsCreditNotes as $creditNotes) {
                            $creditNotes->setActive(0);
                            getEm()->persist($creditNotes);
                            getEm()->flush();
                        }
                    }
                    if ($testClosingParameters != 0) {
                        foreach ($PosSettingsClosingParameters as $closingParameters) {
                            $closingParameters->setActive(0);
                            getEm()->persist($closingParameters);
                            getEm()->flush();
                        }
                    }
                }
            }
            foreach ($jsonData as $data) {
                if ($data != null || $data != "") {
                    $arrData = json_decode($data);
                    $arrData = json_decode(json_encode($arrData), True);

                    foreach ($Pos as $pos) {
                        foreach ($arrData as $dataInternal) {
                            if (key($dataInternal) == "idpossettingsreceipts") {
                                $PosSettingsReceipts = new PosSettingsReceipts();
                                $PosSettingsReceipts->setDataType($dataInternal['data_type']);
                                $PosSettingsReceipts->setDataValue($dataInternal['data_value']);
                                if ($dataInternal['date_create'] != null) {
                                    $dataCreate = new DateTime($dataInternal['date_create']);
                                } else {
                                    $dataCreate = new DateTime();
                                }
                                if ($dataInternal['date_update'] != null || $dataInternal['date_update'] != "") {
                                    $dataUpdate = new DateTime($dataInternal['date_update']);
                                    $PosSettingsReceipts->setDateUpdate($dataUpdate);
                                }
                                if ($dataInternal['date_delete'] != null || $dataInternal['date_delete'] != "") {
                                    $dataDelete = new DateTime($dataInternal['date_delete']);
                                    $PosSettingsReceipts->setDateDelete($dataDelete);
                                }
                                $PosSettingsReceipts->setDateCreate($dataCreate);
                                $PosSettingsReceipts->setActive($dataInternal['active']);
                                $PosSettingsReceipts->setIdpos($pos);
                                getEm()->persist($PosSettingsReceipts);
                                getEm()->flush();
                            } else if (key($dataInternal) == "idpossettingsfeatures") {
                                $PosSettingsFeatures = new PosSettingsFeatures();
                                $PosSettingsFeatures->setDataType($dataInternal['data_type']);
                                $PosSettingsFeatures->setDataValue($dataInternal['data_value']);
                                if ($dataInternal['date_create'] != null) {
                                    $dataCreate = new DateTime($dataInternal['date_create']);
                                } else {
                                    $dataCreate = new DateTime();
                                }
                                if ($dataInternal['date_update'] != null || $dataInternal['date_update'] != "") {
                                    $dataUpdate = new DateTime($dataInternal['date_update']);
                                    $PosSettingsFeatures->setDateUpdate($dataUpdate);
                                }
                                if ($dataInternal['date_delete'] != null || $dataInternal['date_delete'] != "") {
                                    $dataDelete = new DateTime($dataInternal['date_delete']);
                                    $PosSettingsFeatures->setDateDelete($dataDelete);
                                }
                                $PosSettingsFeatures->setDateCreate($dataCreate);
                                $PosSettingsFeatures->setActive($dataInternal['active']);
                                $PosSettingsFeatures->setIdpos($pos);
                                getEm()->persist($PosSettingsFeatures);
                                getEm()->flush();
                            } else if (key($dataInternal) == "idpossettingsdeliverylist") {
                                $PosSettingsDeliveryList = new PosSettingsDeliveryList();
                                $PosSettingsDeliveryList->setDataType($dataInternal['data_type']);
                                $PosSettingsDeliveryList->setDataValue($dataInternal['data_value']);
                                if ($dataInternal['date_create'] != null) {
                                    $dataCreate = new DateTime($dataInternal['date_create']);
                                } else {
                                    $dataCreate = new DateTime();
                                }
                                if ($dataInternal['date_update'] != null || $dataInternal['date_update'] != "") {
                                    $dataUpdate = new DateTime($dataInternal['date_update']);
                                    $PosSettingsDeliveryList->setDateUpdate($dataUpdate);
                                }
                                if ($dataInternal['date_delete'] != null || $dataInternal['date_delete'] != "") {
                                    $dataDelete = new DateTime($dataInternal['date_delete']);
                                    $PosSettingsDeliveryList->setDateDelete($dataDelete);
                                }
                                $PosSettingsDeliveryList->setDateCreate($dataCreate);
                                $PosSettingsDeliveryList->setActive($dataInternal['active']);
                                $PosSettingsDeliveryList->setIdpos($pos);
                                getEm()->persist($PosSettingsDeliveryList);
                                getEm()->flush();
                            } else if (key($dataInternal) == "idpossettingscreditnotes") {
                                $PosSettingsCreditNotes = new PosSettingsCreditNotes();
                                $PosSettingsCreditNotes->setDataType($dataInternal['data_type']);
                                $PosSettingsCreditNotes->setDataValue($dataInternal['data_value']);
                                if ($dataInternal['date_create'] != null) {
                                    $dataCreate = new DateTime($dataInternal['date_create']);
                                } else {
                                    $dataCreate = new DateTime();
                                }
                                if ($dataInternal['date_update'] != null || $dataInternal['date_update'] != "") {
                                    $dataUpdate = new DateTime($dataInternal['date_update']);
                                    $PosSettingsCreditNotes->setDateUpdate($dataUpdate);
                                }
                                if ($dataInternal['date_delete'] != null || $dataInternal['date_delete'] != "") {
                                    $dataDelete = new DateTime($dataInternal['date_delete']);
                                    $PosSettingsCreditNotes->setDateDelete($dataDelete);
                                }
                                $PosSettingsCreditNotes->setDateCreate($dataCreate);
                                $PosSettingsCreditNotes->setActive($dataInternal['active']);
                                $PosSettingsCreditNotes->setIdpos($pos);
                                getEm()->persist($PosSettingsCreditNotes);
                                getEm()->flush();
                            } else if (key($dataInternal) == "idpossettingsclosingparameters") {
                                $PosSettingsClosingParameters = new PosSettingsClosingParameters();
                                $PosSettingsClosingParameters->setDataType($dataInternal['data_type']);
                                $PosSettingsClosingParameters->setDataValue($dataInternal['data_value']);
                                if ($dataInternal['date_create'] != null) {
                                    $dataCreate = new DateTime($dataInternal['date_create']);
                                } else {
                                    $dataCreate = new DateTime();
                                }
                                if ($dataInternal['date_update'] != null || $dataInternal['date_update'] != "") {
                                    $dataUpdate = new DateTime($dataInternal['date_update']);
                                    $PosSettingsClosingParameters->setDateUpdate($dataUpdate);
                                }
                                if ($dataInternal['date_delete'] != null || $dataInternal['date_delete'] != "") {
                                    $dataDelete = new DateTime($dataInternal['date_delete']);
                                    $PosSettingsClosingParameters->setDateDelete($dataDelete);
                                }
                                $PosSettingsClosingParameters->setDateCreate($dataCreate);
                                $PosSettingsClosingParameters->setActive($dataInternal['active']);
                                $PosSettingsClosingParameters->setIdpos($pos);
                                getEm()->persist($PosSettingsClosingParameters);
                                getEm()->flush();
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {

            echo $e->getMessage();
            die();
        }
    }

    /**
     * insert the new setting of person by file, function called in insertImportation_settings
     * @return string json_data 
     */
    public function import_person_setting($jsonData) {
        $Person = getEm()->getRepository('Person')->findBy(array("active" => 1));
        $InvoiceTmpt = getEm()->getRepository('InvoiceTmpt')->findBy(array("active" => 1));
        $Language = getEm()->getRepository('Language')->findBy(array("active" => 1));
        $Project = getEm()->getRepository('Project')->findBy(array("active" => 1));
        $TicketTmpt = getEm()->getRepository('TicketTmpt')->findBy(array("active" => 1));
        try {

            foreach ($jsonData as $data) {
                if ($data != null || $data != "") {
                    $arrData = json_decode($data);
                    $arrData = json_decode(json_encode($arrData), True);


                    foreach ($arrData as $dataInternal) {
                        if (key($dataInternal) == "idperson") {
                            $documentVerify = $dataInternal['document_number'];
                            if ($documentVerify != null && $documentVerify != "") {
                                foreach ($Person as $personVerify) {
                                    if ($personVerify->getDocumentNumber() == $documentVerify) {
                                        foreach ($jsonData as $data1) {
                                            if ($data1 != null || $data1 != "") {
                                                $arrData1 = json_decode($data1);
                                                $arrData1 = json_decode(json_encode($arrData1), True);

                                                foreach ($arrData1 as $dataInternal1) {
                                                    if (key($dataInternal1) == "idperson_settings") {
                                                        if ($dataInternal1['person_idperson'] == $dataInternal['idperson']) {
                                                            $PersonSettings = getEm()->getRepository('PersonSettings')->findBy(array("active" => 1));

                                                            foreach ($PersonSettings as $setting) {

                                                                if ($setting->getPersonperson()->getIdperson() == $dataInternal1['person_idperson']) {

                                                                    $setting->setActive(0);
                                                                    getEm()->persist($setting);
                                                                    getEm()->flush();
                                                                }
                                                            }


                                                            $newPersonSettings = new PersonSettings();
                                                            $newPersonSettings->setTaxFree($dataInternal1['tax_free']);
                                                            $newPersonSettings->setInvShowCustomerName($dataInternal1['inv_show_customer_name']);
                                                            $newPersonSettings->setInvGroupInvoices($dataInternal1['inv_group_invoices']);
                                                            $newPersonSettings->setInvTaxFree($dataInternal1['inv_tax_free']);
                                                            $newPersonSettings->setInvInvoicePrintExclusive($dataInternal1['inv_invoice_print_exclusive']);
                                                            $newPersonSettings->setInvHideProducts($dataInternal1['inv_hide_products']);
                                                            $newPersonSettings->setInvElectronicInvoice($dataInternal1['inv_electronic_invoice']);
                                                            $newPersonSettings->setInvInfo($dataInternal1['inv_info']);
                                                            $newPersonSettings->setInvEmail($dataInternal1['inv_email']);
                                                            $newPersonSettings->setInvTrackEmail($dataInternal1['inv_track_email']);
                                                            $newPersonSettings->setOrdShowInfo($dataInternal1['ord_show_info']);
                                                            $newPersonSettings->setOrdHideFromStatistics($dataInternal1['ord_hide_from_statistics']);
                                                            $newPersonSettings->setOrdUseComAddress($dataInternal1['ord_use_com_address']);
                                                            $newPersonSettings->setOrdCreditLimit($dataInternal1['ord_credit_limit']);
                                                            $newPersonSettings->setChargeTimes($dataInternal1['charge_times']);
                                                            $newPersonSettings->setAuthReceiveSms($dataInternal1['auth_receive_sms']);
                                                            $newPersonSettings->setAuthReceiveEmail($dataInternal1['auth_receive_email']);


                                                            if ($dataInternal1['date_create'] != null) {
                                                                $dataCreate = new DateTime($dataInternal1['date_create']);
                                                            } else {
                                                                $dataCreate = new DateTime();
                                                            }

                                                            if ($dataInternal1['date_update'] != null || $dataInternal1['date_update'] != "") {
                                                                $dataUpdate = new DateTime($dataInternal1['date_update']);
                                                                $newPersonSettings->setDateUpdate($dataUpdate);
                                                            } else {
                                                                $dataUpdate = null;
                                                            }

                                                            if ($dataInternal1['date_delete'] != null || $dataInternal1['date_delete'] != "") {
                                                                $dataDelete = new DateTime($dataInternal1['date_delete']);
                                                                $newPersonSettings->setDateDelete($dataDelete);
                                                            } else {
                                                                $dataDelete = null;
                                                            }

                                                            $newPersonSettings->setDateCreate($dataCreate);
                                                            $newPersonSettings->setActive(1);

                                                            $testeInvoice = 0;
                                                            $testeLanguage = 0;
                                                            foreach ($jsonData as $data2) {
                                                                if ($data2 != null || $data2 != "") {
                                                                    $arrData2 = json_decode($data2);
                                                                    $arrData2 = json_decode(json_encode($arrData2), True);

                                                                    foreach ($arrData2 as $dataInternal2) {

                                                                        if ($dataInternal1['currency_idcurrency'] != null) {

                                                                            if (key($dataInternal2) == "idcurrency") {

                                                                                if ($dataInternal2['idcurrency'] == $dataInternal1['currency_idcurrency']) {

                                                                                    $currencyName = $dataInternal2['name'];

                                                                                    $Currency = getEm()->getRepository('Currency')->findOneBy(array("active" => 1, "name" => $currencyName));
                                                                                    if ($Currency != null) {
                                                                                        $newPersonSettings->setCurrencycurrency($Currency);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    foreach ($arrData2 as $dataInternal2) {

                                                                        if ($dataInternal1['invoice_tmpt_idinvoice_tmpt'] != null) {

                                                                            if (key($dataInternal2) == "idinvoice_tmpt") {

                                                                                if ($dataInternal2['idinvoice_tmpt'] == $dataInternal1['invoice_tmpt_idinvoice_tmpt']) {

                                                                                    $invoiceName = $dataInternal2['name'];

                                                                                    $InvoiceTmpt = getEm()->getRepository('InvoiceTmpt')->findOneBy(array("active" => 1, "name" => $invoiceName));
                                                                                    if ($InvoiceTmpt != null) {
                                                                                        //
                                                                                        $testeInvoice++;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    foreach ($arrData2 as $dataInternal2) {

                                                                        if ($dataInternal1['language_idlanguage'] != null) {

                                                                            if (key($dataInternal2) == "idlanguage") {

                                                                                if ($dataInternal2['idlanguage'] == $dataInternal1['language_idlanguage']) {

                                                                                    $languageName = $dataInternal2['name'];

                                                                                    $Language = getEm()->getRepository('Language')->findOneBy(array("active" => 1, "name" => $languageName));
                                                                                    if ($Language != null) {
                                                                                        $testeLanguage++;
                                                                                        //
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    foreach ($arrData2 as $dataInternal2) {

                                                                        if ($dataInternal1['project_idproject'] != null) {

                                                                            if (key($dataInternal2) == "idproject") {

                                                                                if ($dataInternal2['idproject'] == $dataInternal1['project_idproject']) {

                                                                                    $ProjectName = $dataInternal2['name'];

                                                                                    $Project = getEm()->getRepository('Project')->findOneBy(array("active" => 1, "name" => $ProjectName));
                                                                                    if ($Project != null) {
                                                                                        $newPersonSettings->setProjectproject($Project);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }

                                                                    foreach ($arrData2 as $dataInternal2) {

                                                                        if ($dataInternal1['ticket_tmpt_idticket_tmpt'] != null) {

                                                                            if (key($dataInternal2) == "idticket_tmpt") {

                                                                                if ($dataInternal2['idticket_tmpt'] == $dataInternal1['ticket_tmpt_idticket_tmpt']) {

                                                                                    $TicketName = $dataInternal2['title'];

                                                                                    $TicketTmpt = getEm()->getRepository('TicketTmpt')->findOneBy(array("active" => 1, "title" => $TicketName));
                                                                                    if ($TicketTmpt != null) {
                                                                                        $newPersonSettings->setTicketTmptticketTmpt($TicketTmpt);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            $newPersonSettings->setPersonperson($personVerify);
                                                            if ($testeInvoice != 0 && $testeLanguage != 0) {
                                                                $newPersonSettings->setInvoiceTmptinvoiceTmpt($InvoiceTmpt);
                                                                $newPersonSettings->setLanguagelanguage($Language);
                                                                getEm()->persist($newPersonSettings);
                                                                getEm()->flush();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {

            echo $e->getMessage();
            die();
        }
    }

    public function series() {
        $navbar = "Entry|product";
        $products = getEm()->getRepository('Product')->findBy(array('active' => 1));
        $array_answer = array(
            "products" => $products
        );
        GenericController::template("Entry", "product", "series", $navbar, $array_answer, 312);
    }

    public function createJSON() {
        $idUser = "101";
        $filename = $idUser . "_product_temp.json";
        if (is_writable($filename)) {
            $result = "exist_file";
        } else {
            $fileJSON = fopen($filename, "a+");
            fclose($fileJSON);
            $result = 'not_exist';
        }
        $data = array(
            "result" => $result
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function saveJSON() {
        $idUser = "101";
        $filename = $idUser . "_product_temp.json";
        $fp = fopen($filename, "w");
        $line = $_GET['idUser'] . "_" . $_GET['date_saved'];
        fwrite($fp, $line);
        fclose($fp);
    }

    public function getAllPromotional($id) {

        if ($id) {
            try {
                $promotional = getEm()->getRepository('ProductPromotional')->findBy(array("active" => "1", "idproduct" => $id));
                $data = array();
                foreach ($promotional as $value) {
                    $initialDate = $value->getInitialDate();
                    $finalDate = $value->getFinalDate();
                    $week = $value->getWeek();
                    $days = "";
                    if (isset($week[0])) {
                        if ($week[0] == "1") {
                            $days .= "Monday,";
                        }
                    }
                    if (isset($week[1])) {
                        if ($week[1] == "1") {
                            $days .= "Tuesday,";
                        }
                    }
                    if (isset($week[2])) {
                        if ($week[2] == "1") {
                            $days .= "Wednesday,";
                        }
                    }
                    if (isset($week[3])) {
                        if ($week[3] == "1") {
                            $days .= "Thursday,";
                        }
                    }
                    if (isset($week[4])) {
                        if ($week[4] == "1") {
                            $days .= "Friday,";
                        }
                    }
                    if (isset($week[5])) {
                        if ($week[5] == "1") {
                            $days .= "Saturday,";
                        }
                    }
                    if (isset($week[6])) {
                        if ($week[6] == "1") {
                            $days .= "Sunday,";
                        }
                    }


                    $dat = array(
                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproductpromotional() . '">',
                        "id" => $value->getIdproductpromotional(),
                        "grossPrice" => $value->getGrossPrice(),
                        "initialDate" => isset($initialDate) ? $initialDate->format('m/d/Y') : '',
                        "priceWithoutTaxes" => $value->getPriceWithoutTax(),
                        "finalDate" => isset($finalDate) ? $finalDate->format('m/d/Y') : '',
                        "week" => $days
                    );
                    array_push($data, $dat);
                }
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public static function insertPromotional() {
        try {
            $initialDate = null;
            $finalDate = null;

            if (isset($_POST['initialDate'])) {
                $date = explode('/', $_POST['initialDate']);
                $initialDate = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }
            if (isset($_POST["finalDate"])) {
                $date = explode('/', $_POST['finalDate']);
                $finalDate = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }

            $day = "";
            $day .= isset($_POST['monday']) ? $_POST['monday'] : '0';
            $day .= isset($_POST['tuesday']) ? '-' . $_POST['tuesday'] : '-0';
            $day .= isset($_POST['wednesday']) ? '-' . $_POST['wednesday'] : '-0';
            $day .= isset($_POST['thursday']) ? '-' . $_POST['thursday'] : '-0';
            $day .= isset($_POST['friday']) ? '-' . $_POST['friday'] : '-0';
            $day .= isset($_POST['saturday']) ? '-' . $_POST['saturday'] : '-0';
            $day .= isset($_POST['sunday']) ? '-' . $_POST['sunday'] : '-0';
            $days = str_replace('on', '1', $day);

            $productPromotional = new ProductPromotional();
            $productPromotional->setGrossPrice(str_replace(',', '.', $_POST['grossPrice']));
            $productPromotional->setInitialDate($initialDate);
            $productPromotional->setPriceWithoutTax(str_replace(',', '.', $_POST['priceWithoutTaxes']));
            $productPromotional->setFinalDate($finalDate);
            $productPromotional->setWeek($days);
            $productPromotional->setDateCreate(new DateTime());
            $productPromotional->setActive(1);
            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['idproduct']));
            $productPromotional->setIdproduct($product[0]);

            getEm()->persist($productPromotional);
            getEm()->flush();
            $result = 'success';
            $message = 'query success';
            $data = "";
            //AuthenticationController::insertLog('create', 'ProductPromotional', $_POST['idManufactures']);
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getPromotional($id) {
        if ($id) {
            try {
                $promotional = getEm()->getRepository('ProductPromotional')->findBy(array("active" => "1", "idproductpromotional" => $id));
                $data = array();
                if ($promotional) {
                    $initialDate = $promotional[0]->getInitialDate();
                    $finalDate = $promotional[0]->getFinalDate();
                    $week = $promotional[0]->getWeekString();

                    $dat = array(
                        "id" => $promotional[0]->getIdproductpromotional(),
                        "grossPrice" => $promotional[0]->getGrossPrice(),
                        "initialDate" => isset($initialDate) ? $initialDate->format('m/d/Y') : '',
                        "priceWithoutTaxes" => $promotional[0]->getPriceWithoutTax(),
                        "finalDate" => isset($finalDate) ? $finalDate->format('m/d/Y') : '',
                        "week" => $week,
                    );
                }
                array_push($data, $dat);

                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public static function updatePromotional() {
        try {
            $initialDate = null;
            $finalDate = null;

            if (isset($_POST['initialDate'])) {
                $date = explode('/', $_POST['initialDate']);
                $initialDate = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }
            if (isset($_POST["finalDate"])) {
                $date = explode('/', $_POST['finalDate']);
                $finalDate = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }

            $day = "";
            $day .= isset($_POST['monday']) ? $_POST['monday'] : '0';
            $day .= isset($_POST['tuesday']) ? '-' . $_POST['tuesday'] : '-0';
            $day .= isset($_POST['wednesday']) ? '-' . $_POST['wednesday'] : '-0';
            $day .= isset($_POST['thursday']) ? '-' . $_POST['thursday'] : '-0';
            $day .= isset($_POST['friday']) ? '-' . $_POST['friday'] : '-0';
            $day .= isset($_POST['saturday']) ? '-' . $_POST['saturday'] : '-0';
            $day .= isset($_POST['sunday']) ? '-' . $_POST['sunday'] : '-0';
            $days = str_replace('on', '1', $day);

            $grossPrice_1 = str_replace('.', '', $_POST['grossPrice']);
            $grossPrice = str_replace(',', '.', $grossPrice_1);

            $priceWithoutTaxes_1 = str_replace('.', '', $_POST['priceWithoutTaxes']);
            $priceWithoutTaxes = str_replace(',', '.', $priceWithoutTaxes_1);

            $productPromotional = getEm()->getRepository('ProductPromotional')->findOneBy(array('idproductpromotional' => $_POST['idProductPromotional']));
            $productPromotional->setGrossPrice($grossPrice);
            $productPromotional->setInitialDate($initialDate);
            $productPromotional->setPriceWithoutTax($priceWithoutTaxes);
            $productPromotional->setFinalDate($finalDate);
            $productPromotional->setWeek($days);
            $productPromotional->setDateUpdate(new DateTime());
            getEm()->persist($productPromotional);
            getEm()->flush();
            AuthenticationController::insertLog('update', 'ProductPromotional', $_POST['idProductPromotional']);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function insertColorsSize() {
        try {
            $product = getEm()->getRepository('Product')->findOneBy(array('idproduct' => $_POST['idProduct']));
            $colorSize = new SizeColor();

            if ($_POST['color'] != "0") {
                $color = getEm()->getRepository('Color')->findOneBy(array('idcolor' => $_POST['color']));
                $colorSize->setColorcolor($color);
            }

            if ($_POST['size'] != "0") {
                $size = getEm()->getRepository('Size')->findOneBy(array('idsize' => $_POST['size']));
                $colorSize->setSizesize($size);
            }

            $colorSize->setProductproduct($product);
            $colorSize->setCodean($_POST['cod_ean']);
            $colorSize->setCodsupplier($_POST['cod_intern']);
            $colorSize->setDateCreate(new DateTime());
            $colorSize->setActive(1);
            getEm()->persist($colorSize);
            getEm()->flush();


            $result = "success";
            $message = "query success";
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllColorsSize($id) {
        if ($id) {
            try {
                $colorSize = getEm()->getRepository('SizeColor')->findBy(array("active" => "1", "productproduct" => $id));
                $data = array();
                foreach ($colorSize as $value) {
                    $dat = array(
                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdsizeColor() . '">',
                        "id" => $value->getIdsizeColor(),
                        "color" => $value->getColorcolor()->getName(),
                        "size" => $value->getSizesize()->getName(),
                        "codEan" => $value->getCodean(),
                        "qtdStock" => '0',
                        "cod" => $value->getCodsupplier(),
                    );
                    array_push($data, $dat);
                }
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function getColorSize($id) {
        if ($id) {
            try {
                $colorSize = getEm()->getRepository('SizeColor')->findOneBy(array("active" => "1", "idsizeColor" => $id));
                $data = array();
                if ($colorSize != null) {
                    $dat = array(
                        "id" => $colorSize->getIdsizeColor(),
                        "color" => $colorSize->getColorcolor()->getIdcolor(),
                        "size" => $colorSize->getSizesize()->getIdsize(),
                        "codEan" => $colorSize->getCodean(),
                        "cod" => $colorSize->getCodsupplier(),
                    );
                    array_push($data, $dat);
                }

                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function updateColorsSize() {
        try {

            $colorSize = getEm()->getRepository('SizeColor')->findOneBy(array("active" => "1", "idsizeColor" => $_POST['idColorSize']));

            if ($_POST['color'] != "0") {
                $color = getEm()->getRepository('Color')->findOneBy(array('idcolor' => $_POST['color']));
                $colorSize->setColorcolor($color);
            }

            if ($_POST['size'] != "0") {
                $size = getEm()->getRepository('Size')->findOneBy(array('idsize' => $_POST['size']));
                $colorSize->setSizesize($size);
            }
            $colorSize->setCodean($_POST['cod_ean']);
            $colorSize->setCodsupplier($_POST['cod_intern']);
            $colorSize->setDateUpdate(new DateTime());
            $colorSize->setActive(1);
            getEm()->persist($colorSize);
            getEm()->flush();


            $result = "success";
            $message = "query success";
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteColorsSize() {
        try {
            $ids = $_POST['idColors'];
            for ($i = 0; $i < count($ids); $i++) {
                $colorSize = getEm()->getRepository("SizeColor")->findOneBy(array("idsizeColor" => $ids[$i]));
                $colorSize->setDateDelete(new DateTime());
                $colorSize->setActive('3');
                getEm()->persist($colorSize);
                getEm()->flush();
            }

            $result = "success";
            $message = "query success";
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function genereteColorsSize() {
        if (isset($_POST['color']) && isset($_POST['size']) && isset($_POST['idProduct'])) {
            try {
                if (count($_POST['color']) >= count($_POST['size'])) {
                    foreach ($_POST['color'] as $value1) {
                        foreach ($_POST['size'] as $value2) {
                            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['idProduct']));
                            $color = getEm()->getRepository('Color')->findBy(array('idcolor' => $value1));
                            $size = getEm()->getRepository('Size')->findBy(array('idsize' => $value2));



                            $size_color = new SizeColor();
                            $size_color->setProductproduct($product[0]);
                            $size_color->setColorcolor($color[0]);
                            $size_color->setSizesize($size[0]);
                            $codEan = time() . mt_rand();
                            $size_color->setCodean($codEan);
                            $codSupplier = mt_rand();
                            $size_color->setCodsupplier($codSupplier);
                            $size_color->setDateCreate(new DateTime());
                            getEm()->persist($size_color);
                            getEm()->flush();
                        }
                    }
                } else {
                    foreach ($_POST['size'] as $value1) {
                        foreach ($_POST['color'] as $value2) {
                            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['idProduct']));
                            $color = getEm()->getRepository('Color')->findBy(array('idcolor' => $value2));
                            $size = getEm()->getRepository('Size')->findBy(array('idsize' => $value1));

                            $size_color = new SizeColor();
                            $size_color->setProductproduct($product[0]);
                            $size_color->setColorcolor($color[0]);
                            $size_color->setSizesize($size[0]);
                            $codEan = time() . mt_rand();
                            $size_color->setCodean($codEan);
                            $codSupplier = time() . mt_rand();
                            $size_color->setCodsupplier($codSupplier);
                            $size_color->setDateCreate(new DateTime());
                            getEm()->persist($size_color);
                            getEm()->flush();
                        }
                    }
                }

                $result = "success";
                $message = "query success";
                $mysqlData = '';
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    /**
     * Get all product in section associated to stock of the product 
     * @param string $id
     * @return string json_data 
     */
    public function adjustQtdStock($id) {
        try {
            $data = array();
            $Product = getEm()->getRepository('Product')->findoneBy(array('idproduct' => $id));

            $ProductsInSection = getEm()->getRepository('ProductInSection')->findBy(array('active' => 1, 'productproduct' => $Product->getIdproduct()));
            $qty2 = 0;
            foreach ($ProductsInSection as $productInSection) {
                $active = "";
                if ($productInSection->getActive() == 1) {
                    $active = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $active = '<span class="glyphicon glyphicon-remove"></span>';
                }
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $productInSection->getIdproductInSection() . '">',
                    "id" => $productInSection->getIdproductInSection(),
                    "qty" => $productInSection->getQty() != NULL ? $productInSection->getQty() : "",
                    "dateCreate" => $productInSection->getDateCreate()->format('Y-m-d H:i'),
                    "active" => $active,
                    "section" => $productInSection->getSectionsection() != NULL ? $productInSection->getSectionsection()->getName() : ""
                );

                array_push($data, $dat);
                $qty2 = $qty2 + $productInSection->getQty();
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
            $qty = $qty2;
            $productId = $Product->getIdproduct();
            $productName = $Product->getName();
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
            $qty = "";
            $productId = "";
            $productName = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData,
            "qty" => $qty,
            "idProduct" => $productId,
            "nameProduct" => $productName
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    /**
     * Insert new product in section for ajust stock 
     * @param string $idProduct
     * @param string $section
     * @param string $quantity
     * @param string $dateValue
     * @param string $dateExpirationValue
     * @return string json_data 
     */
    public function insertProductInSection($idProduct, $section, $quantity, $dateValue, $dateExpirationValue) {
        $idProduct = base64_decode($idProduct);
        $section = base64_decode($section);
        $quantity = base64_decode($quantity);
        $dateValue = base64_decode($dateValue);
        $dateExpirationValue = base64_decode($dateExpirationValue);
        if ($idProduct != "0" && $section != "0" && $quantity != "0") {
            try {
                if ($dateExpirationValue == "0") {
                    $dateExpirationValue = "";
                } else {
                    $dateExpirationValue = new DateTime($dateExpirationValue);
                }

                if ($dateValue != "0") {
                    $dateValue = new DateTime($dateValue);
                } else {
                    $dateValue = new DateTime();
                }

                if ($dateExpirationValue == "" || $dateExpirationValue == "0") {
                    $dateExpirationValue = null;
                }

                $Product = getEm()->getRepository('Product')->findoneBy(array('idproduct' => $idProduct));
                $Section = getEm()->getRepository('Section')->findoneBy(array('idsection' => $section));

                $productInSection = new ProductInSection();
                $productInSection->setQty($quantity);
                $productInSection->setProductproduct($Product);
                $productInSection->setSectionsection($Section);
                $productInSection->setDate($dateValue);

                $productInSection->setExpirationdate($dateExpirationValue);
                $productInSection->setDateCreate(new DateTime());
                $productInSection->setActive(1);
                getEm()->persist($productInSection);
                getEm()->flush();

                $result = "success";
                $message = "query success";
                $mysqlData = "";
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        } else {
            var_dump("$idProduct, $section, $quantity or $dateValue is zero");
        }
    }

    /**
     * Get product in section with respective $id for ajust stock 
     * @param string $id
     * @return string json_data 
     */
    public function getProductInSection($id) {
        if (isset($id)) {
            try {
                $productInSection = getEm()->getRepository('ProductInSection')->findOneBy(array("idproductInSection" => $id));
                $mysqlData = array();
                $dateExpiration = "";
                if ($productInSection->getExpirationdate() != NULL) {
                    $dateExpiration = $productInSection->getExpirationdate()->format('m/d/Y');
                }
                $dat = array(
                    "id" => $productInSection->getIdproductInSection(),
                    "qty" => $productInSection->getQty(),
                    "section" => $productInSection->getSectionsection()->getIdsection(),
                    "date" => $productInSection->getDate()->format('m/d/Y'),
                    "dateExpiration" => $dateExpiration
                );
                array_push($mysqlData, $dat);

                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        } else {
            var_dump("error: $id not exists!");
        }
    }

    /**
     * Update product in section for ajust stock 
     * @param string $idProduct
     * @param string $section
     * @param string $quantity
     * @param string $dateValue
     * @param string $dateExpirationValue
     * @return string json_data 
     */
    public function updateProductInSection($idProductInSection, $idProduct, $section, $quantity, $dateValue, $dateExpirationValue) {
        $idProductInSection = base64_decode($idProductInSection);
        $idProduct = base64_decode($idProduct);
        $section = base64_decode($section);
        $quantity = base64_decode($quantity);
        $dateValue = base64_decode($dateValue);
        $dateExpirationValue = base64_decode($dateExpirationValue);

        if ($idProductInSection != "0" && $idProduct != "0" && $section != "0" && $quantity != "0" && $dateValue != "0") {
            try {
                if ($dateExpirationValue == "0") {
                    $dateExpirationValue = "";
                } else {
                    $dateExpirationValue = new DateTime($dateExpirationValue);
                }

                $dateValue = new DateTime($dateValue);
                $Product = getEm()->getRepository('Product')->findoneBy(array('idproduct' => $idProduct));
                $Section = getEm()->getRepository('Section')->findoneBy(array('idsection' => $section));

                $productInSection = getEm()->getRepository('ProductInSection')->findoneBy(array('idproductInSection' => $idProductInSection));
                $productInSection->setQty($quantity);
                $productInSection->setProductproduct($Product);
                $productInSection->setSectionsection($Section);
                $productInSection->setDate($dateValue);
                $productInSection->setExpirationdate($dateExpirationValue);
                $productInSection->setDateUpdate(new DateTime());
                $productInSection->setActive(1);
                getEm()->persist($productInSection);
                getEm()->flush();

                $result = "success";
                $message = "query success";
                $mysqlData = "";
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        } else {
            var_dump("$idProduct, $section, $quantity or $dateValue is zero");
        }
    }

    /**
     * Delete the product in section for ajust stock 
     * @return string json_data 
     */
    public function deleteProductInSection() {
        if ($_POST['idProductInSection']) {
            try {
                $ids = $_POST['idProductInSection'];
                for ($i = 0; $i < count($ids); $i++) {
                    $ProductInSection = getEm()->getRepository("ProductInSection")->findOneBy(array("idproductInSection" => $ids[$i]));
                    $ProductInSection->setActive('0');
                    $ProductInSection->setDateDelete(new DateTime());
                    getEm()->persist($ProductInSection);
                    getEm()->flush();
                    //AuthenticationController::insertLog('delete', 'ProductUnit', $_POST['idUnit']);
                }
                $result = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function insertImageFromProduct() {
        try {
            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['idProduct']));
            $productImage = new ProductImage();
            if (isset($_POST['main'])) {
                $productImage->setMain(1);
            }
            if (isset($_POST['order'])) {
                $productImage->setOrder($_POST['order']);
            }
            $productImage->setPath($_POST['image_from_product']);
            $productImage->setProductproduct($product[0]);
            $productImage->setDateCreate(new DateTime());
            $productImage->setActive(1);
            getEm()->persist($productImage);
            getEm()->flush();
            AuthenticationController::insertLog('create', 'ProductImageFromProduct', $_POST['image_from_product']);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateImageFromProduct($id) {
        try {
            $productImage = getEm()->getRepository('ProductImage')->findOneBy(array('idproductImage' => $id));
            if (isset($_POST['main'])) {
                $productImage->setMain(1);
            }

            if (isset($_POST['order'])) {
                $productImage->setOrder($_POST['order']);
            }
            $productImage->setPath($_POST['image_from_product1']);
            $productImage->setDateUpdate(new DateTime());
            $productImage->setActive(1);
            getEm()->persist($productImage);
            getEm()->flush();
            //AuthenticationController::insertLog('create', 'ProductImageFromProduct', $_POST['image_from_product']);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllImageFromProduct($id) {
        if ($id) {
            try {
                $imageProduct = getEm()->getRepository('ProductImage')->findBy(array("active" => "1", "productproduct" => $id));
                $data = array();
                foreach ($imageProduct as $value) {
                    $dat = array(
                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproductImage() . '">',
                        "id" => $value->getIdproductImage(),
                        "image" => '<img src="' . $value->getPath() . '" width="50px">',
                        "main" => $value->getMain(),
                        "order" => $value->getOrder()
                    );
                    array_push($data, $dat);
                }
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function deleteImageFromProduct() {
        if ($_POST['idImageFromProducts']) {
            try {
                $ids = $_POST['idImageFromProducts'];
                for ($i = 0; $i < count($ids); $i++) {
                    $product = getEm()->getRepository("ProductImage")->findBy(array("idproductImage" => $ids[$i]));
                    $product[0]->setActive('3');
                    $product[0]->setDateDelete(new DateTime());
                    getEm()->persist($product[0]);
                    getEm()->flush();
                    //AuthenticationController::insertLog('delete', 'ProductImageFromProduct', $ids);
                }
                $result = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function getImageFromProduct() {
        if (isset($_POST['id'])) {
            try {
                $productImage = getEm()->getRepository('ProductImage')->findBy(array("idproductImage" => $_POST['id']));
                $mysqlData = array();
                foreach ($productImage as $value) {
                    $dat = array(
                        "id" => $value->getIdproductImage(),
                        "image" => $value->getPath(),
                        "order" => $value->getOrder(),
                        "main" => $value->getMain()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllWholesalePrice($id) {
        if ($id) {
            try {
                $wholesalePrice = getEm()->getRepository('ProductWholesalePrice')->findBy(array("active" => "1", "productproduct" => $id));
                $data = array();
                foreach ($wholesalePrice as $value) {
                    $dat = array(
                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdProductWholesalePrice() . '">',
                        "id" => $value->getIdProductWholesalePrice(),
                        "unitPrice" => $value->getUnitPrice(),
                        "qtd" => $value->getQtd(),
                        "priceForQuantity" => $value->getPriceForQuantity()
                    );
                    array_push($data, $dat);
                }
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function insertWholesalePrice() {
        try {
            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['idProduct']));
            $productWholesalePrice = new ProductWholesalePrice();
            $unitPrice = str_replace('.', '', $_POST['unitPrice']);
            $unitPrice = str_replace(',', '.', $unitPrice);

            $priceForQuantity = str_replace('.', '', $_POST['priceForQuantity']);
            $priceForQuantity = str_replace(',', '.', $priceForQuantity);

            $productWholesalePrice->setUnitPrice($unitPrice);
            $productWholesalePrice->setQtd($_POST['qtd']);
            $productWholesalePrice->setPriceForQuantity($priceForQuantity);
            $productWholesalePrice->setProductproduct($product[0]);
            $productWholesalePrice->setDateCreate(new DateTime());
            $productWholesalePrice->setActive(1);
            getEm()->persist($productWholesalePrice);
            getEm()->flush();
            //AuthenticationController::insertLog('create', 'ProductWholesalePrice', $ids);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getWholesalePrice() {
        if (isset($_POST['id'])) {
            try {
                $productWholesalePrice = getEm()->getRepository('ProductWholesalePrice')->findBy(array("idProductWholesalePrice" => $_POST['id']));
                $mysqlData = array();
                foreach ($productWholesalePrice as $value) {
                    $dat = array(
                        "id" => $value->getIdProductWholesalePrice(),
                        "unitPrice" => $value->getUnitPrice(),
                        "qtd" => $value->getQtd(),
                        "priceForQuantity" => $value->getPriceForQuantity()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateWholesalePrice() {
        try {
            $productWholesalePrice = getEm()->getRepository('ProductWholesalePrice')->findBy(array('idProductWholesalePrice' => $_POST['idWholesalePrice']));
            $unitPrice = str_replace('.', '', $_POST['unitPrice']);
            $unitPrice = str_replace(',', '.', $unitPrice);

            $priceForQuantity = str_replace('.', '', $_POST['priceForQuantity']);
            $priceForQuantity = str_replace(',', '.', $priceForQuantity);

            $productWholesalePrice[0]->setUnitPrice($unitPrice);
            $productWholesalePrice[0]->setQtd($_POST['qtd']);
            $productWholesalePrice[0]->setPriceForQuantity($priceForQuantity);
            $productWholesalePrice[0]->setDateUpdate(new DateTime());
            $productWholesalePrice[0]->setActive(1);
            getEm()->persist($productWholesalePrice[0]);
            getEm()->flush();
            AuthenticationController::insertLog('update', 'ProductWholesalePrice', $_POST['idWholesalePrice']);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteWholesalePrice() {
        if ($_POST['idWholesalePrice']) {
            try {
                $ids = $_POST['idWholesalePrice'];
                for ($i = 0; $i < count($ids); $i++) {
                    $productWholesalePrice = getEm()->getRepository("ProductWholesalePrice")->findBy(array("idProductWholesalePrice" => $ids[$i]));
                    $productWholesalePrice[0]->setActive('3');
                    $productWholesalePrice[0]->setDateDelete(new DateTime());
                    getEm()->persist($productWholesalePrice[0]);
                    getEm()->flush();
                }
                // AuthenticationController::insertLog('delete', 'ProductWholesalePrice', $_POST['idWholesalePrice']);
                $result = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function getAllSubstituteIncludedProducts($id) {
        if ($id) {
            try {
                $IncludedAndSubstitute = getEm()->getRepository('ProductIncludedAndSubstitute')->findBy(array("active" => "1", "productproduct" => $id));
                $data = array();
                foreach ($IncludedAndSubstitute as $value) {
                    $dat = array(
                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdProductIncludedAndSubstitute() . '">',
                        "id" => $value->getIdProductIncludedAndSubstitute(),
                        "product" => $value->getProductInclSubs()->getName(),
                        "type" => $value->getType() == 1 ? 'Substitute' : 'Included',
                        "price" => $value->getPrice()
                    );
                    array_push($data, $dat);
                }
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $mysqlData
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function insertSubstituteIncludedProducts() {
        try {
            $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['idproduct']));
            $productInclSubs = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['product']));
            $IncludedAndSubstitute = new ProductIncludedAndSubstitute();
            $price = str_replace('.', '', $_POST['value_product']);
            $price = str_replace(',', '.', $price);

            $IncludedAndSubstitute->setProductproduct($product[0]);
            $IncludedAndSubstitute->setProductInclSubs($productInclSubs[0]);
            $IncludedAndSubstitute->setType($_POST['type']);
            $IncludedAndSubstitute->setPrice($price);
            $IncludedAndSubstitute->setDateCreate(new DateTime());
            $IncludedAndSubstitute->setActive(1);
            getEm()->persist($IncludedAndSubstitute);
            getEm()->flush();
            AuthenticationController::insertLog('create', 'ProductSubstituteIncludedProducts', $_POST['idproduct']);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getSubstituteIncludedProducts($id) {
        if ($id) {
            try {
                $IncludedAndSubstitute = getEm()->getRepository('ProductIncludedAndSubstitute')->findBy(array("idProductIncludedAndSubstitute" => $id));
                $mysqlData = array();
                foreach ($IncludedAndSubstitute as $value) {
                    $dat = array(
                        "id" => $value->getIdProductIncludedAndSubstitute(),
                        "product" => $value->getProductInclSubs()->getIdproduct(),
                        "price" => $value->getPrice(),
                        "type" => $value->getType()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateSubstituteIncludedProducts() {
        try {
            $IncludedAndSubstitute = getEm()->getRepository('ProductIncludedAndSubstitute')->findBy(array('idProductIncludedAndSubstitute' => $_POST['id_substitute_included_products']));
            $productInclSubs = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['product']));
            $price = str_replace('.', '', $_POST['value_product']);
            $price = str_replace(',', '.', $price);

            $IncludedAndSubstitute[0]->setProductInclSubs($productInclSubs[0]);
            $IncludedAndSubstitute[0]->setType($_POST['type']);
            $IncludedAndSubstitute[0]->setPrice($price);
            $IncludedAndSubstitute[0]->setDateUpdate(new DateTime());
            getEm()->persist($IncludedAndSubstitute[0]);
            getEm()->flush();
            AuthenticationController::insertLog('update', 'ProductSubstituteIncludedProducts', $_POST['id_substitute_included_products']);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteSubstituteIncludedProducts() {
        if ($_POST['idSubsIncl']) {
            try {
                $ids = $_POST['idSubsIncl'];
                for ($i = 0; $i < count($ids); $i++) {
                    $IncludedAndSubstitute = getEm()->getRepository("ProductIncludedAndSubstitute")->findBy(array("idProductIncludedAndSubstitute" => $ids[$i]));
                    $IncludedAndSubstitute[0]->setActive('3');
                    $IncludedAndSubstitute[0]->setDateDelete(new DateTime());
                    getEm()->persist($IncludedAndSubstitute[0]);
                    getEm()->flush();
                }
                //AuthenticationController::insertLog('delete', 'ProductSubstituteIncludedProducts', $_POST['idSubsIncl']);
                $result = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function getAllComponent() {
        try {
            $productComponent = getEm()->getRepository('ProductComponent')->findBy(array('active' => 1));
            $data = array();
            foreach ($productComponent as $value) {
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproductComponent() . '">',
                    "id" => $value->getIdproductComponent(),
                    "name" => $value->getProductproduct1()->getName(),
                    "qtd" => $value->getQty(),
                    "cost" => $value->getCostPrice(),
                    "retailPrice" => $value->getRetailPrice()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getComponent($id) {
        if ($id) {
            try {
                $component = getEm()->getRepository('ProductComponent')->findBy(array("idproductComponent" => $id));
                $mysqlData = array();
                foreach ($component as $value) {
                    $dat = array(
                        "idComponent" => $value->getIdproductComponent(),
                        "productComponent" => $value->getProductproduct1()->getIdproduct(),
                        "qtd" => $value->getQty(),
                        "cost" => $value->getCostPrice(),
                        "retailPrice" => $value->getRetailPrice()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function insertComponent() {
        if (isset($_POST['idproduct']) && isset($_POST['productComponent']) &&
                isset($_POST['cost']) && isset($_POST['retailPrice']) && isset($_POST['qtd'])) {
            try {
                $productPrimary = getEm()->getRepository('Product')->findOneBy(array('idproduct' => $_POST['idproduct']));
                $product = getEm()->getRepository('Product')->findOneBy(array('idproduct' => $_POST['productComponent']));

                $productComponent = new ProductComponent();
                $cost_1 = str_replace('.', '', $_POST['cost']);
                $cost = str_replace(',', '.', $cost_1);
                $retailPrice_1 = str_replace(',', '.', $_POST['retailPrice']);
                $retailPrice = str_replace(',', '.', $retailPrice_1);
                $productComponent->setProductproduct($productPrimary);
                $productComponent->setProductproduct1($product);
                $productComponent->setQty($_POST['qtd']);
                $productComponent->setCostPrice($cost);
                $productComponent->setRetailPrice($retailPrice);
                $productComponent->setDateCreate(new DateTime());
                $productComponent->setActive(1);
                getEm()->persist($productComponent);
                getEm()->flush();
                AuthenticationController::insertLog('create', 'ProductComponent', $_POST['idproduct']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function updateComponent() {
        try {
            $component = getEm()->getRepository('ProductComponent')->findOneBy(array('idproductComponent' => $_POST['idComponent']));
            $product = getEm()->getRepository('Product')->findOneBy(array('idproduct' => $_POST['productComponent']));

            $cost_1 = str_replace('.', '', $_POST['cost']);
            $cost = str_replace(',', '.', $cost_1);
            $retailPrice_1 = str_replace(',', '.', $_POST['retailPrice']);
            $retailPrice = str_replace(',', '.', $retailPrice_1);
            $component->setProductproduct1($product);
            $component->setQty($_POST['qtd']);
            $component->setCostPrice($cost);
            $component->setRetailPrice($retailPrice);
            $component->setDateUpdate(new DateTime());
            $component->setActive(1);
            getEm()->persist($component);
            getEm()->flush();
            AuthenticationController::insertLog('update', 'ProductComponent', $_POST['idComponent']);
            $result = 'success';
            $message = 'query success';
            $data = "";

            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function deleteComponent() {
        if ($_POST['idComponents']) {
            try {
                $ids = $_POST['idComponents'];
                for ($i = 0; $i < count($ids); $i++) {
                    $component = getEm()->getRepository("ProductComponent")->findBy(array("idproductComponent" => $ids[$i]));
                    $component[0]->setActive('3');
                    $component[0]->setDateDelete(new DateTime());
                    getEm()->persist($component[0]);
                    getEm()->flush();
                }
                AuthenticationController::insertLog('delete', 'ProductComponent', $_POST['idComponents']);
                $result = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => ""
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function getAllAdditional($id) {
        try {
            $productAdditional = getEm()->getRepository('ProductAdditional')->findBy(array('productproduct' => $id, 'active' => 1));
            $data = array();
            if ($productAdditional != null) {
                foreach ($productAdditional as $value) {
                    $dat = array(
                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproductadditional() . '">',
                        "id" => $value->getIdproductadditional(),
                        "order" => $value->getOrder(),
                        "additionalType" => $value->getAdditionalType(),
                        "info" => $value->getInfo()
                    );
                    array_push($data, $dat);
                }
            }

            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAllAdditionalItens($id) {
        try {
            $productAdditional = getEm()->getRepository('ProductAdditional')->findOneBy(array('idproductadditional' => $id, 'active' => 1));
            $data = array();
            if ($productAdditional != null) {
                $idAdditional = $productAdditional->getIdproductadditional();
                $productAdditionalItem = getEm()->getRepository('ProductAdditionalItem')->findBy(array('idProductAdditional' => $idAdditional, 'active' => 1));

                foreach ($productAdditionalItem as $value) {
                    if ($value->getBrukFactor() != 0 || $value->getBrukFactor() != null) {
                        $bruk = "<span class='glyphicon glyphicon-ok'></span>";
                    } else {
                        $bruk = "";
                    }
                    $dat = array(
                        "checkbox" => '<input type="checkbox" data-id="' . $value->getIdproductadditionalitem() . '">',
                        "id" => $value->getIdproductadditionalitem(),
                        "product" => $value->getProductproduct()->getName(),
                        "group" => $value->getGroupgroup()->getName(),
                        "price" => $value->getPrice(),
                        "priceType" => $value->getPriceType(),
                        "qty" => $value->getQty(),
                        "brukFactor" => $bruk
                    );
                    array_push($data, $dat);
                }
            }

            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }
        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insertAdditional($id) {
        if ($id) {
            $AddProduct = getEm()->getRepository('ProductAdditional')->findOneBy(array('productproduct' => $id, 'active' => 1));
            if ($AddProduct == null) {
                try {
                    $productAdditional = new ProductAdditional();
                    $productproduct = getEm()->getRepository("Product")->findOneBy(array('idproduct' => $id));
                    $productAdditional->setProductproduct($productproduct);
                    $productAdditional->setDateCreate(new DateTime());
                    $productAdditional->setActive(1);
                    getEm()->persist($productAdditional);
                    getEm()->flush();
                    //AuthenticationController::insertLog('delete', 'ProductAdditional', $id);
                    $result = 'success';
                    $message = 'query success';
                    $AddProduct = getEm()->getRepository('ProductAdditional')->findOneBy(array('productproduct' => $id, 'active' => 1));
                    $data = $AddProduct->getIdproductadditional();
                } catch (Exception $e) {
                    $result = 'error';
                    $message = $e->getMessage();
                    $data = "0";
                }
            } else {
                $result = 'success';
                $message = 'query success';
                $data = $AddProduct->getIdproductadditional();
            }
            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function insertAdditionalItem($id) {
        if (isset($_POST['product']) && isset($_POST['product_group']) &&
                isset($_POST['price_type']) && isset($_POST['price']) && isset($_POST['qtd'])) {
            try {
                $productAdditional = getEm()->getRepository('ProductAdditional')->findOneBy(array('idproductadditional' => $id));
                $groupgroup = getEm()->getRepository("ProductGp")->findOneBy(array('idproductGp' => $_POST['product_group']));
                $productproduct = getEm()->getRepository("Product")->findOneBy(array('idproduct' => $_POST['product']));

                $productAdditionalItem = new ProductAdditionalItem();
                $productAdditionalItem->setProductproduct($productproduct);
                $productAdditionalItem->setGroupgroup($groupgroup);
                $productAdditionalItem->setIdProductAdditional($productAdditional);
                $price_1 = str_replace('.', '', $_POST['price']);
                $price = str_replace(',', '.', $price_1);
                $productAdditionalItem->setPriceType($_POST['price_type']);
                $productAdditionalItem->setPrice($price);
                $productAdditionalItem->setQty($_POST['qtd']);
                if (isset($_POST['bruk_factor'])) {
                    $productAdditionalItem->setBrukFactor(1);
                }
                $productAdditionalItem->setDateCreate(new DateTime());
                $productAdditionalItem->setActive(1);
                getEm()->persist($productAdditionalItem);
                getEm()->flush();
                AuthenticationController::insertLog('create', 'ProductAdditional', $_POST['product']);
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function getAdditionalItem($id) {
        $data = array();
        if ($id != null) {
            try {
                $productAdditionalItem = getEm()->getRepository('ProductAdditionalItem')->findOneBy(array('idproductadditionalitem' => $id));
                $dat = array(
                    "id" => $productAdditionalItem->getIdproductadditionalitem(),
                    "idProductAdditional" => $productAdditionalItem->getIdProductAdditional()->getIdproductadditional(),
                    "product" => $productAdditionalItem->getProductproduct()->getIdproduct(),
                    "group" => $productAdditionalItem->getGroupgroup()->getIdproductGp(),
                    "price" => $productAdditionalItem->getPrice(),
                    "priceType" => $productAdditionalItem->getPriceType(),
                    "qty" => $productAdditionalItem->getQty(),
                    "brukFactor" => $productAdditionalItem->getBrukFactor()
                );
                array_push($data, $dat);
                $result = 'success';
                $message = 'query success';
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function updateAdditionalItem($id) {
        if ($id != null) {
            try {
                $productAdditionalItem = getEm()->getRepository('ProductAdditionalItem')->findOneBy(array('idproductadditionalitem' => $id));
                $groupgroup = getEm()->getRepository("ProductGp")->findOneBy(array('idproductGp' => $_POST['product_group']));
                $productproduct = getEm()->getRepository("Product")->findOneBy(array('idproduct' => $_POST['product']));

                $productAdditionalItem->setProductproduct($productproduct);
                $productAdditionalItem->setGroupgroup($groupgroup);
                $price_1 = str_replace('.', '', $_POST['price']);
                $price = str_replace(',', '.', $price_1);
                $productAdditionalItem->setPriceType($_POST['price_type']);
                $productAdditionalItem->setPrice($price);
                $productAdditionalItem->setQty($_POST['qtd']);
                if (isset($_POST['bruk_factor'])) {
                    $productAdditionalItem->setBrukFactor(1);
                }
                $productAdditionalItem->setDateUpdate(new DateTime());
                $productAdditionalItem->setActive(1);
                getEm()->persist($productAdditionalItem);
                getEm()->flush();
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function deleteAdditionalitems() {
        try {
            $ids = $_POST['idAdditionalItems'];
            for ($i = 0; $i < count($ids); $i++) {
                $productAdditionalItem = getEm()->getRepository("ProductAdditionalItem")->findOneBy(array("idproductadditionalitem" => $ids[$i]));
                $productAdditionalItem->setDateDelete(new DateTime());
                $productAdditionalItem->setActive('3');
                getEm()->persist($productAdditionalItem);
                getEm()->flush();
            }

            $result = "success";
            $message = "query success";
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateAdditional($id) {
        try {
            $additional = getEm()->getRepository('ProductAdditional')->findOneBy(array('idproductadditional' => $id));
            $additional->setOrder($_POST['order']);
            $additional->setAdditionalType($_POST['additionalType']);
            $additional->setInfo($_POST['info']);
            $additional->setDateUpdate(new DateTime());
            getEm()->persist($additional);
            getEm()->flush();
            //$result  = 'success';
            //$message = 'query success';
            //$data = "";
            //AuthenticationController::insertLog('update', 'ProductAdditional', $_POST['idAdditional']);
            $result = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getAdditional($id) {
        $data = array();
        if ($id != null) {
            try {
                $additional = getEm()->getRepository('ProductAdditional')->findOneBy(array('idproductadditional' => $id));
                $dat = array(
                    "id" => $additional->getIdproductadditional(),
                    "order" => $additional->getOrder(),
                    "additionalType" => $additional->getAdditionalType(),
                    "info" => $additional->getInfo()
                );
                array_push($data, $dat);
                $result = 'success';
                $message = 'query success';
                $mysqlData = $data;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result" => $result,
                "message" => $message,
                "data" => $data
            );
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function deleteAdditional() {
        try {
            $ids = $_POST['idAdditional'];
            for ($i = 0; $i < count($ids); $i++) {
                $additional = getEm()->getRepository("ProductAdditional")->findOneBy(array("idproductadditional" => $ids[$i]));
                $additional->setDateDelete(new DateTime());
                $additional->setActive('3');
                getEm()->persist($additional);
                getEm()->flush();

                $productAdditionalItem = getEm()->getRepository("ProductAdditionalItem")->findBy(array("idProductAdditional" => $ids[$i], "active" => 1));
                foreach ($productAdditionalItem as $item) {
                    $item->setDateDelete(new DateTime());
                    $item->setActive('3');
                    getEm()->persist($item);
                    getEm()->flush();
                }
            }

            $result = "success";
            $message = "query success";
            $data = "";
        } catch (Exception $e) {
            $result = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    private function delete_product_group_by_product($product_id){
        try{
            $productGps = getEm()->getRepository("ProductProductGp")->findBy(array("productproduct" => $product_id));
            if($productGps){
                foreach ($productGps as $productProductGp) {
                    getEm()->remove($productProductGp);
                    getEm()->flush();
                }
            }
            return true;
        } catch (Exception $e) {
            die(json_encode(['status' => 500, 'extra' => $e->getMessage()]));
        }
    }

    private function insert_stock_changes($type = false, $size_color, $product, $user, $old, $new, $comment = false){
        try {
            $stock_changes = new StockChanges();
            die('Test 10');
            if($type){
                $stock_changes->setType($type);    
            }
            if($product){
                $stock_changes->setProductproduct($product[0]);
            } else {
                $stock_changes->setSizecolorid($size_color[0]);
            }
            $stock_changes->setUsersusers($user[0]);
            $stock_changes->setOldvalue($old);
            $stock_changes->setNewvalue($new);
            if($comment){
                $stock_changes->setComment($comment);    
            }
            $stock_changes->setDateCreate(new DateTime());
            getEm()->persist($stock_changes);
            getEm()->flush();
            return true;
        } catch (Exception $e) {
            die(json_encode(['status' => 500, 'extra' => $e->getMessage()]));
        }
    }
}
