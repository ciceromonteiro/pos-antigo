<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com> 
 * @version 1.0.0
 * @abstract file created in date Oct 22, 2016
 */
class UserController {

    public function index() {
        $departments = getEm()->getRepository('Department')->findBy(array('active' => 1));
        $jobs = getEm()->getRepository('Job')->findBy(array('active' => 1));
        $pos = getEm()->getRepository('Pos')->findBy(array('active' => 1));
        $types = getEm()->getRepository('UsersType')->findBy(array('active' => 1));
        $customerGroups = getEm()->getRepository('CustomerGp')->findBy(array('active' => 1));
        $array_answer = array(
            'types' => $types,
            'departments' => $departments,
            'pos' => $pos,
            'jobs' => $jobs,
            'customer_group' => $customerGroups
        );
        $navbar = "Entry|user";
        GenericController::template("Entry", "users", "user", $navbar, $array_answer, 201);
    }
    
    public function getAll() {
        try{
            $users = getEm()->getRepository('Users')->findAll();
            $data = array ();
            foreach ($users as $value){
                if($value->getActive() != 3){
                    $status = "";
                    switch ($value->getActive()) {
                        case 1:
                            $status = "Active";
                            break;
                        case 2:
                            $status = "Blocked";
                            break;
                    }
                    $dat = array (
                        "checkbox" => '<input type="checkbox" data-id="'.$value->getIdusers().'">',
                        "id" => $value->getIdusers(),
                        "login" => $value->getLogin(),
                        "fullName" => $value->getPersonperson()->getName(),
                        "status" => $status,
                        "department" => $value->getJobjob()->getDepartmentdepartment()->getName(),
                        "typeUser" => $value->getUsersTypeusersType()->getName()
                    );
                    array_push($data, $dat);
                }
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function insertUser(){
        if(isset($_POST['fullName']) && isset($_POST['job']) && isset($_POST['login']) 
            && isset($_POST['password']) && isset($_POST['type'])
             && isset($_POST['status'])){
                try {
                    $customerGp = getEm()->getRepository('CustomerGp')->findBy(array ('idcustomerGp' => $_POST['customerGp']));
                    $person = new Person();
                    $person->setName($_POST['fullName']);
                    $person->setCustomerGpcustomerGp($customerGp[0]);
                    $person->setDateCreation(new DateTime());
                    $person->setActive($_POST['status']);
                    getEm()->persist($person);
                    getEm()->flush();
                    
                    $job = getEm()->getRepository('Job')->findBy(array ('idjob' => $_POST['job']));
                    $type = getEm()->getRepository('UsersType')->findBy(array ('idusersType' => $_POST['type']));
                    $user = new Users();
                    $user->setUsersTypeusersType($type[0]);
                    $user->setJobjob($job[0]);
                    $user->setPersonperson($person);
                    $user->setLogin($_POST['login']);
                    $user->setPassword($_POST['password']);
                    //$user->setSystemVersion(AuthenticationController::getVersionSystem());
                    if($_POST['status'] != "3"){
                        $user->setDateCreation(new DateTime());
                    } else {
                        $user->setDateDelete(new DateTime());
                    }
                    $user->setActive($_POST['status']);
                    getEm()->persist($user);
                    getEm()->flush();
                    AuthenticationController::insertLog('create', 'user', $_POST['login']);

                    $result  = 'success';
                    $message = 'query success';
                    $data = "";
                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getUser(){
        if(isset($_POST['id'])){
            try {
                $user = getEm()->getRepository('Users')->findBy(array("idusers" => $_POST['id']));
                $array_data = array ();
                foreach ($user as $value){
                    $dat = array (
                        "id" => $value->getIdusers(),
                        "fullName" => $value->getPersonperson()->getName(),
                        "login" => $value->getLogin(),
                        "customerGp" => $value->getPersonperson()->getCustomerGpcustomerGp()->getIdcustomerGp(),
                        "job" => $value->getJobjob()->getIdjob(),
                        "type" => $value->getUsersTypeusersType()->getIdusersType(),
                        "status" => $value->getActive()
                    );
                    array_push($array_data, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function updateUser(){
        if(isset($_POST['fullName']) && isset($_POST['job']) && isset($_POST['login']) 
            && isset($_POST['type']) && isset($_POST['status']) && isset($_POST['idUser'])){
                try {
                    $customerGp = getEm()->getRepository('CustomerGp')->findBy(array ('idcustomerGp' => $_POST['customerGp']));
                    $job = getEm()->getRepository('Job')->findBy(array ('idjob' => $_POST['job']));
                    $type = getEm()->getRepository('UsersType')->findBy(array ('idusersType' => $_POST['type']));
                    
                    $user = getEm()->getRepository('Users')->findBy(array( "idusers" => $_POST['idUser']));
                    $user[0]->setUsersTypeusersType($type[0]);
                    $user[0]->setJobjob($job[0]);
                    $user[0]->setLogin($_POST['login']);
                    if(isset($_POST['password'])){
                        $user[0]->setPassword($_POST['password']);
                    }
                    $user[0]->setSystemVersion(AuthenticationController::getVersionSystem());
                    if($_POST['status'] != "3"){
                        $user[0]->setDateUpdate(new DateTime());
                    } else {
                        $user[0]->setDateDelete(new DateTime());
                    }
                    $user[0]->setActive($_POST['status']);
                    getEm()->persist($user[0]);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('update', 'user', $_POST['login']);
                    
                    $person = getEm()->getRepository('Person')->findBy(array( "idperson" => $user[0]->getPersonperson()));
                    $person[0]->setName($_POST['fullName']);
                    $person[0]->setCustomerGpcustomerGp($customerGp[0]);
                    if($_POST['status'] != "3"){
                        $person[0]->setDateUpdate(new DateTime());
                    } else {
                        $person[0]->setDateDelete(new DateTime());
                    }
                    $person[0]->setActive($_POST['status']);
                    getEm()->persist($person[0]);
                    getEm()->flush();

                    $result  = 'success';
                    $message = 'query success';
                    $data = "";
                } catch (Exception $e) {
                    $result  = 'error';
                    $message = $e->getMessage();
                    $data = "";
                }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function deleteUsers(){
        if($_POST['idUsers']){
            try{
                $ids = $_POST['idUsers'];
                for($i=0; $i < count($ids); $i++){
                    $user = getEm()->getRepository("Users")->findOneBy(array("idusers" => $ids[$i]));
                    $user->setActive('3');
                    $user->setDateDelete(new DateTime());
                    getEm()->persist($user);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'user', $user->getLogin());
                    
                    $person = getEm()->getRepository("Person")->findOneBy(array("idperson" => $user->getPersonperson()->getIdperson()));
                    $person->setActive('3');
                    $person->setDateDelete(new DateTime());
                    getEm()->persist($person);
                    getEm()->flush();
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function insertPermissionUser() {
//        echo "olha eu aki";
//        die();
//        try {
//            $entityManager = getEm();
//            $color = new Color();
//            $color->setName($_GET['nameColor']);
//            $entityManager->persist($color);
//            $entityManager->flush();
//
//            $result = 'success';
//            $message = 'query success';
//            $data = "";
//        } catch (Exception $e) {
//            $result = 'error';
//            $message = $e->getMessage();
//            $data = "";
//        }
//
//
//        $data = array(
//            "result" => $result,
//            "message" => $message,
//            "data" => $data
//        );
//
//        $json_data = json_encode($data);
//        print $json_data;
    }

    public function type() {
        $navbar = "Entry|user";
        $array_answer = array();
        GenericController::template("Entry", "users", "type", $navbar, $array_answer, 211);
    }
    
    public function getAllTypes(){
        try{
            $types = getEm()->getRepository('UsersType')->findBy(array("active" => 1));
            $data = array ();
            foreach ($types as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdusersType().'" data-name="'.$value->getName().'">',
                    "id" => $value->getIdusersType(),
                    "description" => $value->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insertType(){
        if(isset($_POST['nameType'])){
            try {
                $type = new UsersType();
                $type->setName($_POST['nameType']);
                $type->setDateCreate(new DateTime());
                $type->setActive(true);
                getEm()->persist($type);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'type user', $_POST['nameType']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function getType(){
        if(isset($_POST['id'])){
            try {
                $type = getEm()->getRepository('UsersType')->findBy(array("idusersType" => $_POST['id']));
                $array_data = array ();
                foreach ($type as $value){
                    $dat = array (
                        "description" => $value->getName(),
                        "id" => $value->getIdusersType()
                    );
                    array_push($array_data, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateType(){
        if(isset($_POST['nameType']) && isset($_POST['idType'])){
            try {
                $type = getEm()->getRepository('UsersType')->findBy(array( "idusersType" => $_POST['idType']));
                $type[0]->setName($_POST['nameType']);
                $type[0]->setDateUpdate(new DateTime());
                $type[0]->setActive(true);
                getEm()->persist($type[0]);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'type user', $_POST['nameType']);

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function deleteTypes(){
        if($_POST['idTypes']){
            try{
                $ids = $_POST['idTypes'];
                for($i=0; $i < count($ids); $i++){
                    $type = getEm()->getRepository("UsersType")->findOneBy(array("idusersType" => $ids[$i]));
                    $type->setActive('3');
                    $type->setDateDelete(new DateTime());
                    getEm()->persist($type);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'type user', $type->getName());
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function jobs() {
        $navbar = "Entry|user";
        $departments = getEm()->getRepository('Department')->findBy(array('active' => 1));
        $array_answer = array(
            'departments' => $departments
        );
        GenericController::template("Entry", "users", "jobs", $navbar, $array_answer, 215);
    }
    
    public function getAllJobs(){
        try{
            $jobs = getEm()->getRepository('Job')->findBy(array("active" => 1));
            $data = array ();
            foreach ($jobs as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdjob().'">',
                    "id" => $value->getIdjob(),
                    "department" => $value->getDepartmentdepartment()->getName(),
                    "name" => $value->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function insertJob(){
        if(isset($_POST['name']) && isset($_POST['department'])){
            try {
                $department = getEm()->getRepository('Department')->findBy(array('iddepartment' => $_POST['department']));
                $job = new Job();
                $job->setName($_POST['name']);
                $job->setDepartmentdepartment($department[0]);
                $job->setDateCreate(new DateTime());
                $job->setActive(true);
                getEm()->persist($job);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'job', $_POST['name']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getJob(){
        if(isset($_POST['id'])){
            try {
                $job = getEm()->getRepository('Job')->findBy(array("idjob" => $_POST['id']));
                $array_data = array ();
                foreach ($job as $value){
                    $dat = array (
                        "id" => $value->getIdjob(),
                        "name" => $value->getName(),
                        "department" => $value->getDepartmentdepartment()->getIddepartment(),
                    );
                    array_push($array_data, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateJob(){
        if(isset($_POST['name']) && isset($_POST['idJob']) && isset($_POST['department'])){
            try {
                $job = getEm()->getRepository('Job')->findBy(array( "idjob" => $_POST['idJob']));
                $deparment = getEm()->getRepository('Department')->findBy(array( "iddepartment" => $_POST['department']));
                $job[0]->setName($_POST['name']);
                $job[0]->setDepartmentdepartment($deparment[0]);
                $job[0]->setDateUpdate(new DateTime());
                $job[0]->setActive(true);
                getEm()->persist($job[0]);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'job', $_POST['name']);

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function deleteJobs(){
        if($_POST['idJobs']){
            try{
                $ids = $_POST['idJobs'];
                for($i=0; $i < count($ids); $i++){
                    $jobs = getEm()->getRepository("Job")->findOneBy(array("idjob" => $ids[$i]));
                    $jobs->setActive('3');
                    $jobs->setDateDelete(new DateTime());
                    getEm()->persist($jobs);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'job', $jobs->getName());
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function permissions(){
        $navbar = "Entry|user";
        $permissions = getEm()->getRepository('Permission')->findAll();
        $array_answer = array(
            'permissions' => $permissions
        );
        GenericController::template("Entry", "users", "permissions", $navbar, $array_answer, 204);
    }
    
    public function getAllPermissions() {
        try{
            $permissions = getEm()->getRepository('Permission')->findAll();
            $data = array ();
            foreach ($permissions as $value){
                $dat = array (
                    "id" => $value->getIdpermission(),
                    "name" => $value->getName(),
                    "description" => $value->getDescription()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getPermissionsByUserType(){
        if(isset($_POST['id'])){
            try {
                $permissionByUserType = getEm()->getRepository('UsersType')->findBy(array("idusersType" => $_POST['id']));
                $defaultPermissions = getEm()->getRepository('DefaultPermission')->findBy(array("usersTypeusersType" => $permissionByUserType[0]));
                $ids = array ();
                foreach ($defaultPermissions as $value){
                    if($value->getActive() == 1){
                        $ids[] = $value->getPermissionpermission()->getIdpermission();
                    }
                }
                $result  = 'success';
                $message = 'query success';
                $data = array ('id' => $permissionByUserType[0]->getIdusersType(), 'permissions' => $ids);
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
        
    public function updatePermissionsByUserType(){
        if(isset($_POST['idTypeUsers']) && isset($_POST['checkbox_permissions'])){
            try {
                $values = $_POST['checkbox_permissions'];
                $typeUsers = getEm()->getRepository('UsersType')->findOneBy(array( "idusersType" => $_POST['idTypeUsers']));
                for($i=0; $i <= 1; $i++){
                    $default_permissions = getEm()->getRepository('DefaultPermission')->findBy(array( "usersTypeusersType" => $typeUsers));
                    if($default_permissions == null){
                        $this->checkIfThereIsAnyUsersType($_POST['idTypeUsers']);
                    }
                    if($default_permissions != null){
                        $this->checkIfThereIsAnyUsersType($_POST['idTypeUsers']);
                        foreach ($default_permissions as $default_permission) {
                            foreach ($values as $value) {
                                if($default_permission->getPermissionpermission()->getIdpermission() == $value){
                                    $permission = getEm()->getRepository('Permission')->findOneBy(array('idpermission' => $value));
                                    $default_permission->setPermissionpermission($permission);
                                    $default_permission->setUsersTypeusersType($typeUsers);
                                    $default_permission->setActive(1);
                                    $default_permission->setDateUpdate(new DateTime());
                                    getEm()->persist($default_permission);
                                    getEm()->flush();
                                }
                            }
                        }
                    
                    }
                }
                
                AuthenticationController::insertLog('update', 'permissions', $typeUsers->getName());
                
                $result  = 'success';
                $message = 'query success';
                $data = '';
            } catch (Exception $e) {
                $result  = 'success';
                $message = $e->getMessage();
                $data = '';
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function removePermissionsByUserType(){
        if($_POST['idTypes']){
            try{
                $ids = $_POST['idTypes'];
                for($i=0; $i < count($ids); $i++){
                    $typeUsers = getEm()->getRepository('UsersType')->findOneBy(array( "idusersType" => $ids[$i]));
                    $default_permissions = getEm()->getRepository("DefaultPermission")->findBy(array("usersTypeusersType" => $typeUsers));
                    foreach ($default_permissions as $default_permission) {
                        $default_permission->setActive('3');
                        $default_permission->setDateDelete(new DateTime());
                        getEm()->persist($default_permission);
                        getEm()->flush();
                    }
                }
                
                AuthenticationController::insertLog('delete', 'permissions', $typeUsers->getName());
                
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function getPermissionsByUser(){
        if(isset($_POST['id'])){
            try {
                $this->clonePermissionsFromUsersTypeForUser($_POST['id']);
                $user = getEm()->getRepository('Users')->findBy(array("idusers" => $_POST['id']));
                $permissionByUser = getEm()->getRepository('UsersPermission')->findBy(array("usersusers" => $user[0]));
                $ids = array ();
                foreach ($permissionByUser as $value){
                    if($value->getActive() == 1){
                        $ids[] = $value->getPermissionpermission()->getIdpermission();
                    }
                }
                $result  = 'success';
                $message = 'query success';
                $data = array ('id' => $user[0]->getIdusers(), 'permissions' => $ids);
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
        
    public function updatePermissionsByUser(){
        if(isset($_POST['idUser']) && isset($_POST['checkbox_permissions'])){
            try {
                $values = $_POST['checkbox_permissions'];
                $users = getEm()->getRepository('Users')->findOneBy(array( "idusers" => $_POST['idUser']));
                for($i=0; $i <= 1; $i++){
                    $user_permissions = getEm()->getRepository('UsersPermission')->findBy(array( "usersusers" => $users));
                    if($user_permissions == null){
                        $permissions = getEm()->getRepository('Permission')->findAll();
                        for($j=0; $j < count($permissions); $j++){
                            $permission = $permissions[$j];
                            $insert_user_permissions = new UsersPermission();
                            $insert_user_permissions->setPermissionpermission($permission);
                            $insert_user_permissions->setUsersusers($users);
                            $insert_user_permissions->setActive(2);
                            getEm()->persist($insert_user_permissions);
                            getEm()->flush();
                        }
                        $this->clonePermissionsFromUsersTypeForUser($_POST['idUser']);
                    }
                    if($user_permissions != null){
                        foreach ($user_permissions as $user_permission) {
                            foreach ($values as $value) {
                                if($user_permission->getPermissionpermission()->getIdpermission() == $value){
                                    $permission = getEm()->getRepository('Permission')->findBy(array('idpermission' => $value));
                                    $user_permission->setPermissionpermission($permission[0]);
                                    $user_permission->setUsersusers($users);
                                    $user_permission->setActive(1);
                                    getEm()->persist($user_permission);
                                    getEm()->flush();
                                }
                            }
                        }
                        $this->clonePermissionsFromUsersTypeForUser($_POST['idUser']);
                    }
                }
                
                AuthenticationController::insertLog('update', 'permissions', $users->getLogin());
                
                $result  = 'success';
                $message = 'query success';
                $data = '';
            } catch (Exception $e) {
                $result  = 'success';
                $message = $e->getMessage();
                $data = '';
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function removePermissionsByUser(){
        if($_POST['idUsers']){
            try{
                $ids = $_POST['idUsers'];
                for($i=0; $i < count($ids); $i++){
                    $users = getEm()->getRepository('Users')->findBy(array("idusers" => $ids[$i]));
                    $usersPermissions = getEm()->getRepository('UsersPermission')->findBy(array("usersusers" => $users[0]));
                    foreach ($usersPermissions as $usersPermission) {
                        $usersPermission->setActive('3');
                        getEm()->persist($usersPermission);
                        getEm()->flush();
                        
                        //AuthenticationController::insertLog('delete', 'permissions', $users->getLogin());
                    }
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function checkIfThereIsAnyUsersType($typeUser){
        $typeUsers = getEm()->getRepository('UsersType')->findOneBy(array( "idusersType" => $typeUser));
        $permissions = getEm()->getRepository('Permission')->findAll();
        foreach ($permissions as $permission) {
            $default_permissions = getEm()->getRepository('DefaultPermission')->findOneBy( array(
                "usersTypeusersType" => $typeUsers,
                "permissionpermission" => $permission,
            ));
            if($default_permissions == null){
                $insert_default_permissions = new DefaultPermission();
                $insert_default_permissions->setPermissionpermission($permission);
                $insert_default_permissions->setUsersTypeusersType($typeUsers);
                $insert_default_permissions->setActive(2);
                $insert_default_permissions->setDateCreate(new DateTime());
                getEm()->persist($insert_default_permissions);
                getEm()->flush();
            }
        }
    }
    
    public function clonePermissionsFromUsersTypeForUser($user){
        $permissions = getEm()->getRepository('Permission')->findAll();
        $users = getEm()->getRepository('Users')->findBy( array("idusers" => $user));
        
        foreach ($permissions as $permission) {
            $default_permissions = getEm()->getRepository('DefaultPermission')->findBy( array(
                "usersTypeusersType" => $users[0]->getUsersTypeusersType(),
                "permissionpermission" => $permission,
            ));

            foreach ($default_permissions as $default_permission) {
                $users_permissions = getEm()->getRepository('UsersPermission')->findOneBy( array(
                    "usersusers" => $users[0],
                    "permissionpermission" => $default_permission->getPermissionpermission(),
                ));
                if($users_permissions == null){
                    $insert_users_permissions = new UsersPermission();
                    $insert_users_permissions->setPermissionpermission($default_permission->getPermissionpermission());
                    $insert_users_permissions->setUsersusers($users[0]);
                    $insert_users_permissions->setActive($default_permission->getActive());
                    getEm()->persist($insert_users_permissions);
                    getEm()->flush();
                }
            }
        }
    }
}
