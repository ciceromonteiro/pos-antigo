<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com> 
 * @version 1.0.0
 * @abstract file created in date Oct 22, 2016
 */
class DepartmentController {

    public function __construct() {
        
    }

    /**
     * 
     */
    public function index() {
        $array_answer = array ("");
        $navbar = "Entry|department";
        GenericController::template("Entry", "department", "index", $navbar, $array_answer, 193);
    }

    /**
     * Insert Department
     * 
     * @param string $name
     * @param datetime $date_create
     * @param datetime $date_update
     * @param datetime $date_delete
     * @param int $active
     */
    public function addDepartment($name, $date_update, $date_delete, $active) {
        $department = new Department;
        $department->setName($name);
        $department->setDate_update($date_update);
        $department->setDate_delete($date_delete);
        $department->setActive($active);

        getEm()->persist($department);
        getEm()->flush();
        
        AuthenticationController::insertLog('create', 'department', $name);
    }

    /**
     * alter Department
     * 
     * @param string $name
     * @param datetime $date_create
     * @param datetime $date_update
     * @param datetime $date_delete
     * @param int $active
     * @param int $id
     */
    public function editDepartment($name, $date_create, $date_update, $date_delete, $active, $id) {
        $department = getEm()->find('Department', $id);
        $department->setName($name);
        $department->setDate_create($date_create);
        $department->setDate_update($date_update);
        $department->setDate_delete($date_delete);
        $department->setActive($active);
        AuthenticationController::insertLog('edit', 'department', $name);
        getEm()->persist($department);
        getEm()->flush();
        
    }

    public function getAll($returnArray = false) {
        try {
            $department = getEm()->getRepository('Department')->findBy(array("active" => 1));
            $data = array();
            foreach ($department as $value) {
                $dat = array(
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIddepartment().'">',
                    "id" => $value->getIddepartment(),
                    "description" => $value->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e) {
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $mysqlData
        );
        if($returnArray == true){
            return $department;
        }
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insert() {
        if (isset($_POST['nameDepartment'])) {
            try {
                $department = new Department();
                $department->setName($_POST['nameDepartment']);
                $department->setDateCreate(new DateTime());
                $department->setActive(1);
                getEm()->persist($department);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'department', $_POST['nameDepartment']);

                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getDepartment() {
        if (isset($_POST['id'])) {
            try {
                $department = getEm()->getRepository('Department')->findBy(array("iddepartment" => $_POST['id']));
                $mysqlData = array();
                foreach ($department as $value) {
                    $dat = array(
                        "description" => $value->getName(),
                        "id" => $value->getIddepartment()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function update() {
        if (isset($_POST['nameDepartment']) && isset($_POST['idDepartment'])) {
            try {
                $department = getEm()->getRepository('Department')->findBy(array( "iddepartment" => $_POST['idDepartment']));
                $department[0]->setName($_POST['nameDepartment']);
                $department[0]->setDateUpdate(new DateTime());
                $department[0]->setActive(true);
                getEm()->persist($department[0]);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'department', $_POST['nameDepartment']);
                
                $result = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result" => $result,
            "message" => $message,
            "data" => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function deleteDepartments(){
        if($_POST['idDepartments']){
            try{
                $ids = $_POST['idDepartments'];
                for($i=0; $i < count($ids); $i++){
                    $department = getEm()->getRepository("Department")->findOneBy(array("iddepartment" => $ids[$i]));
                    $department->setActive('3');
                    $department->setDateDelete(new DateTime());
                    getEm()->persist($department);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'department', $department->getName());
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

}
