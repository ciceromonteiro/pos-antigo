<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com> 
 * @version 1.0.0
 * @abstract file created in date Oct 31, 2016
 */
class ProjectController {

    public function __construct() {
        
    }
    
    public function index() {
        $array_answer = array("a", "b", "c");
        $navbar = "Entry|project";
        GenericController::template("Entry", "project", "index", $navbar, $array_answer, 197);
    }

    public function addProject($name, $id, $extra_info, $date_update, $date_delete, $active, $date_start, $date_end, $price, $projectcol) {
        $project = new Project;
        $project->setName($name);
        $project->setId($id);
        $project->setExtra_info($extra_info);
        $project->setDate_update($date_update);
        $project->setDate_delete($date_delete);
        $project->setActive($active);
        $project->setDate_start($date_start);
        $project->setDate_end($date_end);
        $project->setPrice($price);
        $project->setProjectcol($projectcol);
        getEm()->persist($project);
        getEm()->flush();
    }

    public function editProject($name, $id, $extra_info, $date_update, $date_delete, $active, $date_start, $date_end, $price, $projectcol, $idpro) {
        $project = getEm()->find('Project', $idpro);
        $project->setName($name);
        $project->setId($id);
        $project->setExtra_info($extra_info);
        $project->setDate_update($date_update);
        $project->setDate_delete($date_delete);
        $project->setActive($active);
        $project->setDate_start($date_start);
        $project->setDate_end($date_end);
        $project->setPrice($price);
        $project->setProjectcol($projectcol);
        getEm()->persist($project);
        getEm()->flush();
    }

    public function getAll($returnArray = false) {
        try{
            $projects = getEm()->getRepository('Project')->findAll();
            $data = array ();
            foreach ($projects as $value){
                switch ($value->getActive()) {
                    case 1:
                        $status = "Active";
                        break;
                    case 2:
                        $status = "Blocked";
                        break;
                    case 3:
                        $status = "Delete";
                }
                $start = $value->getDateStart();
                $end = $value->getDateEnd();
                $stop = $value->getDateStop();
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdproject().'">',
                    "id" => $value->getIdproject(),
                    "name" => $value->getName(),
                    "description" => $value->getDescription(),
                    "start" => isset($start) ? $start->format('m/d/Y') : '',
                    "price" => number_format($value->getPrice(),2,",","."),
                    "end" => isset($end) ? $end->format('m/d/Y') : '',
                    "priceAdditional" => number_format($value->getPriceAdditional(),2,",","."),
                    "stop" => isset($stop) ? $stop->format('m/d/Y') : '',
                    "status" => $status
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
            if($returnArray == true){
                return $projects;
            }
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insert() {
        try {
            $dateStart = null;
            $dateStop = null;
            $dateEnd = null;
            if(isset($_POST['dateStart'])){
                $date = explode('/', $_POST['dateStart']);
                $dateStart = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }
            if(isset($_POST["dateStop"])){
                $date = explode('/', $_POST['dateStop']);
                $dateStop = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }
            if(isset($_POST['dateEnd'])){
                $date = explode('/', $_POST['dateEnd']);
                $dateEnd = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }
            $price_1 = str_replace('.', '', $_POST['price']);
            $price = str_replace(',', '.', $price_1);
            
            $priceAdditional_1 = str_replace('.', '', $_POST['priceAdditional']);
            $priceAdditional = str_replace(',', '.', $priceAdditional_1);
            
            $project = new Project();
            $project->setName($_POST['nameProject']);
            $project->setDescription($_POST['description']);
            $project->setDateStart($dateStart);
            $project->setDateStop($dateStop);
            $project->setDateEnd($dateEnd);
            $project->setPrice($price);
            $project->setPriceadditional($priceAdditional);
            $project->setDateCreate(new DateTime());
            $project->setActive($_POST['status']);
            getEm()->persist($project);
            getEm()->flush();
            
            AuthenticationController::insertLog('create', 'project', $_POST['nameProject']);
            
            $result  = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function update(){
        try {
            $project = getEm()->getRepository('Project')->findBy(array( "idproject" => $_POST['idProject']));
            
            $dateStart = null;
            $dateStop = null;
            $dateEnd = null;
            if(isset($_POST['dateStart'])){
                $date = explode('/', $_POST['dateStart']);
                $dateStart = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }
            if(isset($_POST["dateStop"])){
                $date = explode('/', $_POST['dateStop']);
                $dateStop = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }
            if(isset($_POST['dateEnd'])){
                $date = explode('/', $_POST['dateEnd']);
                $dateEnd = new DateTime("{$date[2]}-{$date[0]}-{$date[1]}");
            }
            
            $price_1 = str_replace('.', '', $_POST['price']);
            $price = str_replace(',', '.', $price_1);
            
            $priceAdditional_1 = str_replace('.', '', $_POST['priceAdditional']);
            $priceAdditional = str_replace(',', '.', $priceAdditional_1);
            
            $project[0]->setName($_POST['nameProject']);
            $project[0]->setDescription($_POST['description']);
            $project[0]->setDateStart($dateStart);
            $project[0]->setDateStop($dateStop);
            $project[0]->setDateEnd($dateEnd);
            $project[0]->setPrice($price);
            $project[0]->setPriceadditional($priceAdditional);
            $project[0]->setDateUpdate(new DateTime());
            $project[0]->setActive($_POST['status']);
            getEm()->persist($project[0]);
            getEm()->flush();
            
            AuthenticationController::insertLog('update', 'project', $_POST['nameProject']);

            $result  = 'success';
            $message = 'query success';
            $data = "";
        } catch (Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getProject(){
        if(isset($_POST['id'])){
            try {
                $project = getEm()->getRepository('Project')->findBy(array("idproject" => $_POST['id']));
                $array_data = array ();
                foreach ($project as $value){
                    $dat = array (
                        "id" => $value->getIdproject(),
                        "name" => $value->getName(),
                        "description" => $value->getDescription(),
                        "start" => $value->getDateStart()->format('m/d/Y'),
                        "price" => $value->getPrice(),
                        "end" => $value->getDateEnd()->format('m/d/Y'),
                        "priceAdditional" => $value->getPriceAdditional(),
                        "stop" => $value->getDateStop()->format('m/d/Y'),
                        "status" => $value->getActive()
                    );
                    array_push($array_data, $dat);
                    break;
                }                    
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }
    
    public static function deleteProjects(){
        if($_POST['idProjects']){
            try{
                $ids = $_POST['idProjects'];
                for($i=0; $i < count($ids); $i++){
                    $project = getEm()->getRepository("Project")->findOneBy(array("idproject" => $ids[$i]));
                    $project->setActive(3);
                    $project->setDateDelete(new DateTime());
                    getEm()->persist($project);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'project', $project->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
}
