<?php

class ProductSerialController {
    
    public function getAllSerialNumbers(){
        try{
            $productsSerial = getEm()->getRepository('ProductSerialNumber')->findBy(array("active" => 1));
            $data = array ();
            foreach ($productsSerial as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdproductSerialNumber().'">',
                    "id" => $value->getIdproductSerialNumber(),
                    "product" => $value->getProductproduct()->getName(),
                    "serial" => $value->getSerial()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insertSerialNumber(){        
        if(isset($_POST['product']) && $_POST['serialNumber'] != ""){
            try {
                $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['product']));
                $serial = new ProductSerialNumber();
                $serial->setProductproduct($product[0]);
                $serial->setSerial($_POST['serialNumber']);
                $serial->setActive(1);
                getEm()->persist($serial);
                getEm()->flush();          
                
                AuthenticationController::insertLog('create', 'serial number', $_POST['serialNumber']);
                
                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );
        
        $json_data = json_encode($data);
        print $json_data;
        }
    }

    public static function updateSerialNumber(){
        if(isset($_POST['product']) && isset($_POST['productSerialNumber']) && isset($_POST['serialNumber'])){
            try {
                $product = getEm()->getRepository('Product')->findBy(array('idproduct' => $_POST['product']));
                $serial = getEm()->getRepository('ProductSerialNumber')->findBy(array( "idproductSerialNumber" => $_POST['productSerialNumber']));                
                $serial[0]->setProductproduct($product[0]);
                $serial[0]->setSerial($_POST['serialNumber']);
                $serial[0]->setDateUpdate(new DateTime());
                $serial[0]->setActive(1);
                
                getEm()->persist($serial[0]);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'serial number', $_POST['serialNumber']);

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function getSerialNumber(){
        if(isset($_POST['id'])){
            try {
                $serial = getEm()->getRepository('ProductSerialNumber')->findBy(array("idproductSerialNumber" => $_POST['id']));
                $array_data = array ();
                foreach ($serial as $value){
                    $dat = array (
                        "product" => $value->getProductproduct()->getIdproduct(),
                        "serial" => $value->getSerial(),
                        "id" => $value->getIdproductSerialNumber()
                    );
                    array_push($array_data, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $array_data;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function deleteSerialNumber(){
        if($_POST['idSerialNumbers']){
            try{
                $ids = $_POST['idSerialNumbers'];
                for($i=0; $i < count($ids); $i++){
                    $serial = getEm()->getRepository("ProductSerialNumber")->findOneBy(array("idproductSerialNumber" => $ids[$i]));
                    $serial->setActive('3');
                    $serial->setDateDelete(new DateTime());
                    getEm()->persist($serial);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'serial number', $serial->getSerial());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
}
