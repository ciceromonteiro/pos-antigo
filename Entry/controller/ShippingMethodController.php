<?php
/**
 * Created by PhpStorm.
 * User: rocha
 * Date: 10/20/16
 * Time: 2:49 PM
 */

class ShippingMethodController {
    public function __contruct(){

    }

    public static function index(){
        $shipping_company = getEm()->getRepository('Company')->findBy(array('active' => 1));
        $array_answer = array (
            'company' => $shipping_company
        );
        $navbar = "Entry|ShippingMethod";
        GenericController::template("Entry", "shippingMethod","index", $navbar, $array_answer, 316);
    }

    public function getAll(){
        try{
            $shippingMethods = getEm()->getRepository('ShippingMtd')->findBy(array("active" => 1));
            $data = array ();
            foreach ($shippingMethods as $value){
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$value->getIdshippingMtd().'">',
                    "id" => $value->getIdshippingMtd(),
                    "description" => $value->getName(),
                    "company" => $value->getShippingCmpshippingCmp()->getName()
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }

    public function insert(){

        if(isset($_POST['name']) && isset($_POST['company'])){
            try {
                $company = getEm()->getRepository('Company')->findBy(array("idcompany" => $_POST['company']));
                $shipping = new ShippingMtd();
                $shipping->setName($_POST['name']);
                $shipping->setShippingCmpshippingCmp($company[0]);
                $shipping->setActive(1);
                getEm()->persist($shipping);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'shipping method', $_POST['name']);

                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function getShippingMtd(){
        if(isset($_POST['id'])){
            try {
                $shipping = getEm()->getRepository('ShippingMtd')->findBy(array("idshippingMtd" => $_POST['id']));
                $mysqlData = array ();
                foreach ($shipping as $value){
                    $dat = array (
                        "id" => $value->getIdshippingMtd(),
                        "description" => $value->getName(),
                        "company" => $value->getShippingCmpshippingCmp()->getIdcompany()
                    );
                    array_push($mysqlData, $dat);
                    break;
                }
                $result  = 'success';
                $message = 'query success';
                $data = $mysqlData;
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function update(){
        if(isset($_POST['name']) && isset($_POST['idMethod']) && isset($_POST['company'])){
            try {
                $company = getEm()->getRepository('Company')->findBy(array("idcompany" => $_POST['company']));
                $shipping = getEm()->getRepository('ShippingMtd')->findBy(array( "idshippingMtd" => $_POST['idMethod']));
                $shipping[0]->setName($_POST['name']);
                $shipping[0]->setShippingCmpshippingCmp($company[0]);
                $shipping[0]->setDateUpdate(new DateTime());
                $shipping[0]->setActive(1);
                getEm()->persist($shipping[0]);
                getEm()->flush();
                
                AuthenticationController::insertLog('update', 'shipping method', $_POST['name']);

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public function delete(){
        if($_POST['idMethods']){
            try{
                $ids = $_POST['idMethods'];
                for($i=0; $i < count($ids); $i++){
                    $methods = getEm()->getRepository("ShippingMtd")->findOneBy(array("idshippingMtd" => $ids[$i]));
                    $methods->setActive('3');
                    $methods->setDateDelete(new DateTime());
                    getEm()->persist($methods);
                    getEm()->flush();
                    
                    AuthenticationController::insertLog('delete', 'shipping method', $methods->getName());
                    
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }


}