<?php
/**
 * @author Claudio da Cruz Silva Junior>
 * @version 1.0.0
 * @abstract file created in date september 20, 2017
 */
require_once '../init_autoload.php';

class TemplateController {

    public function __construct() {
        
    }

    /**
     * Return page with content avoiding print in controller
     * @param unknown $data
     */
    public function returnData($data){      
            GenericController::answer( "Pagebuilder", "index", "echo", $data);          
    }
    
    public function index(){
        $navbar = "Entry|template";
        $array_answer = array ();      
        GenericController::template("Entry", "template","index", $navbar, $array_answer, 251);
    }
    
    public function getAllTemplate(){
        try{
            $templates = getEm()->getRepository('OrderTmpt')->findBy(array("active" => 1));
            $data = array ();
            foreach ($templates as $value){
                $id = $value->getidordertmpt();
                $name = $value->getName();
                $withdrawalFromStock = $value->getWithdrawalFromStock();
                if($withdrawalFromStock == 1){
                    $withdrawalFromStock = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $withdrawalFromStock = "";
                }
                $sellByInvoice = $value->getSellByInvoice();
                if($sellByInvoice == 1){
                    $sellByInvoice = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $sellByInvoice = "";
                }
                $includeInStockStatistics = $value->getIncludeInStockStatistics();
                if($includeInStockStatistics == 1){
                    $includeInStockStatistics = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $includeInStockStatistics = "";
                }
                $changePrintSendMethod = $value->getChangePrintSendMethod();
                if($changePrintSendMethod == 1){
                    $changePrintSendMethod = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $changePrintSendMethod = "";
                }
                $printDeliveryList = $value->getPrintDeliveryList();
                if($printDeliveryList == 1){
                    $printDeliveryList = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $printDeliveryList = "";
                }
                $printInvoice = $value->getPrintInvoice();
                if($printInvoice == 1){
                    $printInvoice = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $printInvoice = "";
                }
                $templateOrder = $value->getTemplateOrder();
                
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$id.'" data-name="'.$id.'">',
                    "id" => $id,
                    "name" => $name,
                    "withdrawalFromStock" => $withdrawalFromStock,
                    "sellByInvoice" => $sellByInvoice,
                    "includeInStockStatistics" => $includeInStockStatistics,
                    "changePrintSendMethod" => $changePrintSendMethod,
                    "printDeliveryList" => $printDeliveryList,
                    "printInvoice" => $printInvoice,
                    "templateOrder" => $templateOrder
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getTemplate(){
        if($_POST['id']){
            try{
                $template = getEm()->getRepository('OrderTmpt')->findBy(array("idordertmpt" => $_POST['id'], "active" => 1));
                $id = $template[0]->getidordertmpt();
                $name = $template[0]->getName();
                $withdrawalFromStock = $template[0]->getWithdrawalFromStock();
                $sellByInvoice = $template[0]->getSellByInvoice();
                $includeInStockStatistics = $template[0]->getIncludeInStockStatistics();
                $changePrintSendMethod = $template[0]->getChangePrintSendMethod();
                $printDeliveryList = $template[0]->getPrintDeliveryList();
                $printInvoice = $template[0]->getPrintInvoice();
                $templateOrder = $template[0]->getTemplateOrder();
                
                $templateFooter = getEm()->getRepository('OrderTmptFooter')->findBy(array("idordertmpt" => $template[0]->getidordertmpt(), "active" => 1));
                $footer = $templateFooter[0]->getFooter1();
                $footerA5 = $templateFooter[0]->getFooter2();
                
                $templateImg = getEm()->getRepository('OrderTmptImg')->findBy(array("idordertmpt" => $template[0]->getidordertmpt(), "active" => 1));

                if($templateImg != null){
                    $image = $templateImg[0]->getImage();
                } else {
                    $imageType = "";
                    $image = "";
                }
                
                $data = array (
                    "id" => $id,
                    "name" => $name,
                    "withdrawalFromStock" => $withdrawalFromStock,
                    "sellByInvoice" => $sellByInvoice,
                    "includeInStockStatistics" => $includeInStockStatistics,
                    "changePrintSendMethod" => $changePrintSendMethod,
                    "printDeliveryList" => $printDeliveryList,
                    "printInvoice" => $printInvoice,
                    "templateOrder" => $templateOrder,
                    "footer" => $footer,
                    "footerA5" => $footerA5,
                    "image" => $image
                );
                
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function insertTemplate(){
        $nameTemplate = isset($_POST['nameTemplate']) ? $_POST['nameTemplate'] : null;
        $withdrawalFromStock = isset($_POST['withdrawalFromStock']) ? 1 : null;
        $sellByInvoice = isset($_POST['sellByInvoice']) ? 1 : null;
        $includeInStockStatistics = isset($_POST['includeInStockStatistics']) ? 1 : null;
        $changePrintSendMethod = isset($_POST['changePrintSendMethod']) ? 1 : null;
        $printDeliveryList = isset($_POST['printDeliveryList']) ? 1 : null;
        $printInvoice = isset($_POST['printInvoice']) ? 1 : null;
        $templateOrder = isset($_POST['templateOrder']) ? $_POST['templateOrder'] : null;
        $textFooter = isset($_POST['textFooter']) ? $_POST['textFooter'] : null;
        $textFooterA5 = isset($_POST['textFooterA5']) ? $_POST['textFooterA5'] : null;
        $image = isset($_POST['image']) ? $_POST['image'] : null;
                
        if($nameTemplate != null && $templateOrder != null){
            try {
                $template = new OrderTmpt();
                $template->setName($nameTemplate);
                $template->setWithdrawalFromStock($withdrawalFromStock);
                $template->setSellByInvoice($sellByInvoice);
                $template->setIncludeInStockStatistics($includeInStockStatistics);
                $template->setChangePrintSendMethod($changePrintSendMethod);
                $template->setPrintDeliveryList($printDeliveryList);
                $template->setPrintInvoice($printInvoice);
                $template->setTemplateOrder($templateOrder);
                $template->setDateCreate(new DateTime());
                $template->setActive(1);
                getEm()->persist($template);
                getEm()->flush();
                
                $templateFooter = new OrderTmptFooter();
                $templateFooter->setIdordertmpt($template);  
                $templateFooter->setFooter1($textFooter);
                $templateFooter->setFooter2($textFooterA5);
                $templateFooter->setActive(1);
                getEm()->persist($templateFooter);
                getEm()->flush();      
                
                if($image != null){
                    $templateImg = new OrderTmptImg();
                    $templateImg->setIdordertmpt($template);  
                    $templateImg->setImage($image);
                    $templateImg->setImageType('image');
                    $templateImg->setActive(1);
                    getEm()->persist($templateImg);
                    getEm()->flush(); 
                }
                
                $templatePrinterSettings = new OrderTmptPrinterSettings();
                $order = getEm()->getRepository('OrderTmpt')->findBy(array('idordertmpt' => $template->getIdordertmpt()));
                $templatePrinterSettings->setIdOrderTmpt($order[0]);
                
                $templatePrinterSettings->setDateCreate(new DateTime());
                $templatePrinterSettings->setActive(1);
                getEm()->persist($templatePrinterSettings);
                getEm()->flush();
                
                AuthenticationController::insertLog('create', 'template', $_POST['nameTemplate']);
                
                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function updateTemplate(){
        if(isset($_POST['idTemplate'])){
            try {
                $nameTemplate = isset($_POST['nameTemplate']) ? $_POST['nameTemplate'] : null;
                $withdrawalFromStock = isset($_POST['withdrawalFromStock']) ? 1 : null;
                $sellByInvoice = isset($_POST['sellByInvoice']) ? 1 : null;
                $includeInStockStatistics = isset($_POST['includeInStockStatistics']) ? 1 : null;
                $changePrintSendMethod = isset($_POST['changePrintSendMethod']) ? 1 : null;
                $printDeliveryList = isset($_POST['printDeliveryList']) ? 1 : null;
                $printInvoice = isset($_POST['printInvoice']) ? 1 : null;
                $templateOrder = isset($_POST['templateOrder']) ? $_POST['templateOrder'] : null;
                $textFooter = isset($_POST['textFooter']) ? $_POST['textFooter'] : null;
                $textFooterA5 = isset($_POST['textFooterA5']) ? $_POST['textFooterA5'] : null;
                $image = isset($_POST['image']) ? $_POST['image'] : null;

                $template = getEm()->getRepository('OrderTmpt')->findBy(array( "idordertmpt" => $_POST['idTemplate']));
                $template[0]->setName($nameTemplate);
                $template[0]->setWithdrawalFromStock($withdrawalFromStock);
                $template[0]->setSellByInvoice($sellByInvoice);
                $template[0]->setIncludeInStockStatistics($includeInStockStatistics);
                $template[0]->setChangePrintSendMethod($changePrintSendMethod);
                $template[0]->setPrintDeliveryList($printDeliveryList);
                $template[0]->setPrintInvoice($printInvoice);
                $template[0]->setTemplateOrder($templateOrder);
                $template[0]->setDateUpdate(new DateTime());
                $template[0]->setActive(1);
                getEm()->persist($template[0]);
                getEm()->flush();
                
                $templateFooter = getEm()->getRepository('OrderTmptFooter')->findBy(array( "idordertmpt" => $_POST['idTemplate']));
                $templateFooter[0]->setIdordertmpt($template[0]);  
                $templateFooter[0]->setFooter1($textFooter);
                $templateFooter[0]->setFooter2($textFooterA5);
                $templateFooter[0]->setActive(1);
                getEm()->persist($templateFooter[0]);
                getEm()->flush();      
                
                if($image != null){
                    $templateImg = getEm()->getRepository('OrderTmptImg')->findBy(array( "idordertmpt" => $_POST['idTemplate']));
                    $templateImg[0]->setIdordertmpt($template[0]);  
                    $templateImg[0]->setImage($image);
                    $templateImg[0]->setImageType('image');
                    $templateImg[0]->setActive(1);
                    getEm()->persist($templateImg[0]);
                    getEm()->flush(); 
                }
                
                AuthenticationController::insertLog('update', 'template', $nameTemplate);

                $result  = 'success';
                $message = 'query success';
                $data = "";
            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $data
        );

        $json_data = json_encode($data);
        print $json_data;
    }

    public static function deleteTemplates(){
        if($_POST['idTemplates']){
            try{
                $ids = $_POST['idTemplates'];
                for($i=0; $i < count($ids); $i++){
                    $template = getEm()->getRepository("OrderTmpt")->findBy(array("idordertmpt" => $ids[$i]));
                    $template[0]->setActive('3');
                    $template[0]->setDateDelete(new DateTime());
                    getEm()->persist($template[0]);
                    getEm()->flush();
                    
                    $templateFooter = getEm()->getRepository("OrderTmptFooter")->findBy(array("idordertmpt" => $template[0]->getidordertmpt()));
                    $templateFooter[0]->setActive('3');
                    getEm()->persist($templateFooter[0]);
                    getEm()->flush();
                    
                    //AuthenticationController::insertLog('delete', 'template', $template->getName());
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function printerSettings(){
        $navbar = "Entry|template";
        $printer = getEm()->getRepository('Printer')->findBy(array('active' => 1));
        $array_answer = array(
            'printer' => $printer
        );
        GenericController::template("Entry", "template", "printer_settings", $navbar, $array_answer, 258);
    }

    public function getAllPrinterSettings(){
        try{
            $templatesPrintSett = getEm()->getRepository('OrderTmptPrinterSettings')->findBy(array("active" => 1));
            $data = array ();
            foreach ($templatesPrintSett as $value){
                $id = $value->getIdOrderTmptPrinterSettings();
                $name = $value->getIdOrderTmpt()->getName();
                
                $numbeofwaystoprint = $value->getNumbeofwaystoprint();
                if($numbeofwaystoprint == 1){
                    $numbeofwaystoprint = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $numbeofwaystoprint = "";
                }
                
                $printvalues = $value->getPrintvalues();
                if($printvalues == 1){
                    $printvalues = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $printvalues = "";
                }
                
                $printpriceswithtaxes = $value->getPrintpriceswithtaxes();
                if($printpriceswithtaxes == 1){
                    $printpriceswithtaxes = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $printpriceswithtaxes = "";
                }
                
                $hidetotalprice = $value->getHidetotalprice();
                if($hidetotalprice == 1){
                    $hidetotalprice = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $hidetotalprice = "";
                }
                
                $printObs = $value->getPrintobs();
                if($printObs == 1){
                    $printObs = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $printObs = "";
                }
                
                $printtheobsinaseparateline = $value->getPrinttheobsinaseparateline();
                if($printtheobsinaseparateline == 1){
                    $printtheobsinaseparateline = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $printtheobsinaseparateline = "";
                }
                
                $lineswithdifferentbackgrounds = $value->getLineswithdifferentbackgrounds();
                if($lineswithdifferentbackgrounds == 1){
                    $lineswithdifferentbackgrounds = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $lineswithdifferentbackgrounds = "";
                }
                
                $idPrinter = $value->getIdPrinter();
                if($idPrinter != null){
                    $idPrinter = $value->getIdPrinter()->getDescription();
                } else {
                    $idPrinter = "";
                }
                
                $sendByEmail = $value->getSendbyemail();
                if($sendByEmail == 1){
                    $sendByEmail = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $sendByEmail = "";
                }
                
                $sendBySMS = $value->getSendbysms();
                if($sendBySMS == 1){
                    $sendBySMS = '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    $sendBySMS = "";
                }
                
                $dat = array (
                    "checkbox" => '<input type="checkbox" data-id="'.$id.'" data-name="'.$id.'">',
                    "id" => $id,
                    "name" => $name,
                    "numbeofwaystoprint" => $numbeofwaystoprint,
                    "printvalues" => $printvalues,
                    "printpriceswithtaxes" => $printpriceswithtaxes,
                    "hidetotalprice" => $hidetotalprice,
                    "printObs" => $printObs,
                    "printtheobsinaseparateline" => $printtheobsinaseparateline,
                    "lineswithdifferentbackgrounds" => $lineswithdifferentbackgrounds,
                    "idPrinter" => $idPrinter,
                    "sendByEmail" => $sendByEmail,
                    "sendBySMS" => $sendBySMS
                );
                array_push($data, $dat);
            }
            $result = "success";
            $message = "query success";
            $mysqlData = $data;
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function getTemplatePrinterSettings(){
        if($_POST['id']){
            try{
                $template = getEm()->getRepository('OrderTmptPrinterSettings')->findBy(array("idOrderTmptPrinterSettings" => $_POST['id'], "active" => 1));
                if($template[0]->getIdPrinter()){
                    $printer = $template[0]->getIdPrinter()->getIdprinter();
                } else {
                    $printer = '';
                }
                
                $data = array (
                    "idOrder" => $template[0]->getIdOrderTmptPrinterSettings(),
                    "idTemplate" => $template[0]->getIdOrderTmpt()->getIdordertmpt(),
                    "nameTemplate" => $template[0]->getIdOrderTmpt()->getName(),
                    "numbeofwaystoprint" => $template[0]->getNumbeofwaystoprint(),
                    "printvalues" => $template[0]->getPrintvalues(),
                    "printpriceswithtaxes" => $template[0]->getPrintpriceswithtaxes(),
                    "hidetotalprice" => $template[0]->getHidetotalprice(),
                    "printObs" => $template[0]->getPrintobs(),
                    "printtheobsinaseparateline" => $template[0]->getPrinttheobsinaseparateline(),
                    "lineswithdifferentbackgrounds" => $template[0]->getLineswithdifferentbackgrounds(),
                    "idPrinter" => $printer,
                    "sendByEmail" => $template[0]->getSendbyemail(),
                    "sendBySMS" => $template[0]->getSendbysms()
                );
                
                $result = "success";
                $message = "query success";
                $mysqlData = $data;
            } catch (Exception $e){
                $result = "error";
                $message = $e->getMessage();
                $mysqlData = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $mysqlData
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }
    
    public function updatePrinterSettings(){
        if(isset($_POST['idOrderPrintSettings']) && isset($_POST['idTemplate'])){
            $printer = isset($_POST['printer']) ? $_POST['printer'] : null;
            $numbeofwaystoprint = isset($_POST['numbeofwaystoprint']) ? 1 : null;
            $printvalues = isset($_POST['printvalues']) ? 1 : null;
            $printpriceswithtaxes = isset($_POST['printpriceswithtaxes']) ? 1 : null;
            $hidetotalprice = isset($_POST['hidetotalprice']) ? 1 : null;
            $printObs = isset($_POST['printObs']) ? 1 : null;
            $printtheobsinaseparateline = isset($_POST['printtheobsinaseparateline']) ? 1 : null;
            $lineswithdifferentbackgrounds = isset($_POST['lineswithdifferentbackgrounds']) ? 1 : null;
            $sendByEmail = isset($_POST['sendByEmail']) ? 1 : null;
            $sendBySMS = isset($_POST['sendBySMS']) ? 1 : null;
            
            try {
                $template = getEm()->getRepository('OrderTmptPrinterSettings')->findBy(array('idOrderTmptPrinterSettings' => $_POST['idOrderPrintSettings'], 'active' => 1));
                $template[0]->setSendbysms($sendBySMS);
                $template[0]->setSendbyemail($sendByEmail);
                $template[0]->setPrintvalues($printvalues);
                $template[0]->setPrinttheobsinaseparateline($printtheobsinaseparateline);
                $template[0]->setPrintpriceswithtaxes($printpriceswithtaxes);
                $template[0]->setPrintobs($printObs);
                $template[0]->setNumbeofwaystoprint($numbeofwaystoprint);
                $template[0]->setLineswithdifferentbackgrounds($lineswithdifferentbackgrounds);
                $template[0]->setHidetotalprice($hidetotalprice);         
                if($printer == null || $printer == 0){
                    $template[0]->setIdPrinter(null);
                } else {
                    $printer = getEm()->getRepository('Printer')->findBy(array('idprinter' => $printer));
                    $template[0]->setIdPrinter($printer[0]);
                }
                
                $template[0]->setDateUpdate(new DateTime());
                getEm()->persist($template[0]);
                getEm()->flush();
                
                //AuthenticationController::insertLog('update', 'template', $template[0]->getName());
                
                $result  = 'success';
                $message = 'query success';
                $data = "";

            } catch (Exception $e) {
                $result  = 'error';
                $message = $e->getMessage();
                $data = "";
            }

            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data
            );

            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public static function deletePrinterSettings(){
        if($_POST['idTemplatesPrintersSett']){
            try{
                $ids = $_POST['idTemplatesPrintersSett'];
                for($i=0; $i < count($ids); $i++){
                    $template = getEm()->getRepository("OrderTmptPrinterSettings")->findOneBy(array("idOrderTmptPrinterSettings" => $ids[$i]));
                    $template->setActive('3');
                    $template->setDateDelete(new DateTime());
                    getEm()->persist($template);
                    getEm()->flush();
                    
                    //AuthenticationController::insertLog('delete', 'template', $template[0]->getName());
                            
                }
                $result  = 'success';
                $message = 'Deleted elements successful';
            } catch (Exception $ex) {
                $result  = 'error';
                $message = $ex->getMessage();
                $data = "";
            }
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => ""
            );        
            $json_data = json_encode($data);
            print $json_data;
        }
    }

    public function epost(){
        $navbar = "Entry|template";
        $array_answer = null;
        GenericController::template("Entry", "template", "epost", $navbar, $array_answer, 261);
    }
    
    public function getEpost(){
        $arrayValues = array ();
        $dat = array ();
        try {
            $epost = getEm()->getRepository('OrderTmptEpost')->findAll();
            if($epost){
                foreach ($epost as $value) {
                    $dataType = $value->getDataType();
                    $dataValue = $value->getDataValue();
                    $dat[$dataType] = $dataValue;
                }
                array_push($arrayValues, $dat);
            }
            $result  = 'success';
            $message = 'query success';
            $values = $arrayValues;
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    
    public function updateEpost(){
        $arrayValues = array (
            'invoice_text_sent_email_title'     => isset($_POST['invoice_text_sent_email_title']) ? $_POST['invoice_text_sent_email_title'] : '',
            'invoice_text_first_email'          => isset($_POST['invoice_text_first_email']) ? $_POST['invoice_text_first_email'] : '',
            'invoice_text_second_email'         => isset($_POST['invoice_text_second_email']) ? $_POST['invoice_text_second_email'] : '',
            'proposals_text_sent_email_title'   => isset($_POST['proposals_text_sent_email_title']) ? $_POST['proposals_text_sent_email_title'] : '',
            'proposals_text'                    => isset($_POST['proposals_text']) ? $_POST['proposals_text'] : ''
        );
        
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $arrayKeys = array_keys($arrayValues);
            for($i = 0; $i < count($arrayKeys); $i++){
                $key = $arrayKeys[$i];
                $epost = getEm()->getRepository('OrderTmptEpost')->findBy(array('dataType' => $key));
                if($epost){
                    $epost[0]->setDataValue($arrayValues[$key]);
                    $epost[0]->setDateUpdate(new DateTime());
                    $epost[0]->setActive(1);
                    getEm()->persist($epost[0]);
                    getEm()->flush();
                } else {
                    $epost = new OrderTmptEpost();
                    $epost->setDataType($key);
                    $epost->setDataValue($arrayValues[$key]);
                    $epost->setDateCreate(new DateTime());
                    $epost->setActive(1);
                    getEm()->persist($epost);
                    getEm()->flush();
                }
                AuthenticationController::insertLog('update', 'e-post', '');
            }
            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = 'query success';
        } catch (\Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message
        );

        $json_data = json_encode($data);
        print $json_data;
    }

     /**
     * update logo of template
     * @return string json_data 
     */
    public function insertLogo(){
        $image = $_POST['image'];
        $image = base64_decode($image);
         
        $LogoTmpt = getEm()->getRepository('LogoTmpt')->findAll();
        try {
         foreach ($LogoTmpt as $logo) {
             $logo->setActive(0);
             getEm()->persist($logo);
             getEm()->flush();

         }

            $newLogoTmpt = new LogoTmpt();
            $newLogoTmpt->setImgTmpt($image);
            $newLogoTmpt->setActive(1);
            getEm()->persist($newLogoTmpt);
            getEm()->flush();

            $result  = 'success';
            $message = 'query success';
            $values = '';
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * get logo of template
     * @return string json_data 
     */
    public function getLogo(){

        try{
            $LogoTmpt = getEm()->getRepository('LogoTmpt')->findOneBy(array("active" => 1));
         
            $result = "success";
            $message = "query success";
            if($LogoTmpt != null){
            $mysqlData = array("img" => $LogoTmpt->getImgTmpt());
            }else{
            $mysqlData = "../../../assets/images/upload.png";    
            }
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * delete logo of template
     * @return string json_data 
     */
    public function logoDelete(){

    try{
            $LogoTmpt = getEm()->getRepository('LogoTmpt')->findBy(array("active" => 1));
            foreach ($LogoTmpt as $logo) {
                 $logo->setActive(0);
                 getEm()->persist($logo);
                 getEm()->flush();

             }
            $result = "success";
            $message = "query success";
            $mysqlData = "../../../assets/images/upload.png";
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * define image of employee cards
     * @return string json_data 
     */
    public function newEmployeeCardsImg(){

        $image = $_POST['image'];
        $image = base64_decode($image);
         
        $EmployeeCardsImgTmpt = getEm()->getRepository('EmployeeCardsImgTmpt')->findAll();
        try {
         foreach ($EmployeeCardsImgTmpt as $employeeCards) {
             $employeeCards->setActive(0);
             getEm()->persist($employeeCards);
             getEm()->flush();

         }

            $newEmployeeCardsImgTmpt = new EmployeeCardsImgTmpt();
            $newEmployeeCardsImgTmpt->setImgTmpt($image);
            $newEmployeeCardsImgTmpt->setActive(1);
            getEm()->persist($newEmployeeCardsImgTmpt);
            getEm()->flush();

            $result  = 'success';
            $message = 'query success';
            $values = '';
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * get image of employee cards
     * @return string json_data 
     */
    public function getEmployeeCardsImg(){

        try{
            $EmployeeCardsImgTmpt = getEm()->getRepository('EmployeeCardsImgTmpt')->findOneBy(array("active" => 1));
         
            $result = "success";
            $message = "query success";
            if($EmployeeCardsImgTmpt != null){
            $mysqlData = array("img" => $EmployeeCardsImgTmpt->getImg());
            }else{
            $mysqlData = "../../../assets/images/upload.png";    
            }
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * delete image of employee cards
     * @return string json_data 
     */
    public function deleteEmployeeCardsImg(){

    try{
            $EmployeeCardsImgTmpt = getEm()->getRepository('EmployeeCardsImgTmpt')->findBy(array("active" => 1));
            foreach ($EmployeeCardsImgTmpt as $employeeCards) {
                 $employeeCards->setActive(0);
                 getEm()->persist($employeeCards);
                 getEm()->flush();

             }
            $result = "success";
            $message = "query success";
            $mysqlData = "../../../assets/images/upload.png";
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * define image of access card
     * @return string json_data 
     */
    public function newAccessCardImg(){

        $image = $_POST['image'];
        $image = base64_decode($image);
         
        $AccessCardImgTmpt = getEm()->getRepository('AccessCardImgTmpt')->findAll();
        try {
         foreach ($AccessCardImgTmpt as $accessCard) {
             $accessCard->setActive(0);
             getEm()->persist($accessCard);
             getEm()->flush();

         }

            $newAccessCardImgTmpt = new AccessCardImgTmpt();
            $newAccessCardImgTmpt->setImgTmpt($image);
            $newAccessCardImgTmpt->setActive(1);
            getEm()->persist($newAccessCardImgTmpt);
            getEm()->flush();

            $result  = 'success';
            $message = 'query success';
            $values = '';
        } catch (\Exception $e) {
            $result  = 'error';
            $message = $e->getMessage();
            $values = '';
        }
        
        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $values
        );
        
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * get image of access card
     * @return string json_data 
     */
    public function getAccessCardImg(){

        try{
            $AccessCardImgTmpt = getEm()->getRepository('AccessCardImgTmpt')->findOneBy(array("active" => 1));
         
            $result = "success";
            $message = "query success";
            if($AccessCardImgTmpt != null){
            $mysqlData = array("img" => $AccessCardImgTmpt->getImg());
            }else{
            $mysqlData = "../../../assets/images/upload.png";    
            }
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }
    /**
     * delete image of access card
     * @return string json_data 
     */
    public function deleteAccessCardImg(){

        try{
             $AccessCardImgTmpt = getEm()->getRepository('AccessCardImgTmpt')->findBy(array("active" => 1));
            foreach ($AccessCardImgTmpt as $accessCard) {
                 $accessCard->setActive(0);
                 getEm()->persist($accessCard);
                 getEm()->flush();

             }
            $result = "success";
            $message = "query success";
            $mysqlData = "../../../assets/images/upload.png";
        } catch (Exception $e){
            $result = "error";
            $message = $e->getMessage();
            $mysqlData = "";
        }

        $data = array(
            "result"  => $result,
            "message" => $message,
            "data"    => $mysqlData
        );
        $json_data = json_encode($data);
        print $json_data;
    }

    public function labelLayout(){
        $navbar = "Entry|template";
        $array_answer = array ();      
        GenericController::template("Entry", "template","labelLayout", $navbar, $array_answer, 251);
    }


    
}
