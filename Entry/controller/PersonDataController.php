<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Feb 7, 2017
 */
class PersonDataController {

    public function __contruct() {
        
    }

    /**
     * File of the person
     * 
     * @param string $dataType
     * @param string $dataValue
     * @param Object $personperson
     * @return void lass return
     */
    public function insertPersonData($dataType, $dataValue, $personperson) {
        try {
            $PersonData = new PersonData();
            $PersonData->setDataType($dataType);
            $PersonData->setDataValue($dataValue);
            $PersonData->setPersonperson($personperson);
            $PersonData->setActive(1);
            //AuthenticationController::insertLog('create', 'persondata', $personperson->getName());
            getEm()->persist($PersonData);
            getEm()->flush();
        } catch (Exception $e) {
            print $e;
            exit(1);
        }
    }
    
    /**
     * Update of the values Persondata
     *  
     * @param int $id
     * @param string $dataType
     * @param string $dataValue
     * @param int $personperson
     * @return void 
     */
    public function editPersonData( $dataType, $dataValue, $personperson) {
        try {
            $PersonData = getEm()->getRepository('PersonData')->findoneby(array("personperson" => $personperson, "dataType" => $dataType));            
            $PersonData->setDataType($dataType);
            $PersonData->setDataValue($dataValue);
            $PersonData->setPersonperson($personperson);
            $PersonData->setActive(1);
            getEm()->persist($PersonData);
            getEm()->flush();
            //AuthenticationController::insertLog('edit', 'persondata', $personperson->getName());
        } catch (Exception $e) {
            print $e;
            exit(1);
        }
    }

}
