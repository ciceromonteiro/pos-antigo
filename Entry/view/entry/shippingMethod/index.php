<?php 
    $company_select = "";
    foreach ($array_answer['company'] as $value) {
        $company_select .= "<option value='".$value->getIdcompany()."'>".$value->getName()."</option>";
    }
    $company_select = "<option value=''>Select Option</option>".$company_select;
?>

<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#methods').DataTable( {
            "ajax": {"url": my_url+"Entry/ShippingMethod/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" },
                { "data": "company" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_methods');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_methods_edit');
            $.ajax({
                url : my_url+"Entry/ShippingMethod/getShippingMtd/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#methodsEditModal').modal({show : true});
                        $('#form_methods_edit #idMethod').val(data.data[0].id);
                        $('#form_methods_edit #name').val(data.data[0].description);
                        $("#form_methods_edit #company").val(data.data[0].company).prop('selected', true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_methods', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/ShippingMethod/insert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_methods').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_methods_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/ShippingMethod/update",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_methods_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#methodsEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idMethods : ids};
            deleteItens('Entry/ShippingMethod/delete', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#methods tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#methods');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<!-- Menu -->
<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="methods" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('methods')"></th>
                        <th style="width: 10px" class="translate">ID</th>
                        <th class="translate">Description</th>
                        <th class="translate">Company</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Methods</h4>
            </div>
            <form id="form_methods">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name" class="label-style translate">Method</label>
                            <input type="text" class="form-control" id="nameMethods" name="name" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="company" class="label-style translate">Company</label>
                            <select name="company" required="true">
                                <?php echo $company_select ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="methodsEditModal" tabindex="-1" role="dialog" aria-labelledby="labelMethodsUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelMethodsUpdate">Update Method</h4>
            </div>
            <form id="form_methods_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name" class="label-style translate">Method</label>
                            <input type="text" id="idMethod" name="idMethod" required="true" style="display: none">
                            <input type="text" class="form-control" id="name" name="name" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="company" class="label-style translate">Company</label>
                            <select name="company" id="company" required="true">
                                <?php echo $company_select ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>