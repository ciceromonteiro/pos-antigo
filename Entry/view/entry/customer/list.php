<?php



$customerGpOB = getEm()->getRepository('CustomerGp')->findBy(array("active" => 1));
$zzCountryOB = getEm()->getRepository('ZzCountry')->findBy(array("active" => 1));
$usersOB = getEm()->getRepository('Users')->findBy(array("active" => 1));
$CompanyOB = getEm()->getRepository('Company')->findBy(array("active" => 1)); 
$paymentMtdOB = getEm()->getRepository('PaymentMtd')->findBy(array("active" => 1));
//$invoiceThemeOB = getEm()->getRepository('InvoiceTmpt')->findBy(array("active" => 1));
//$languageOB = getEm()->getRepository('Language')->findBy(array("active" => 1));
$zzStateOB = getEm()->getRepository('ZzState')->findBy(array("active" => 1));
//$ticketTemplateOB = getEm()->getRepository('TicketTmpt')->findBy(array("active" => 1));

?>
<script type="text/javascript">  
  function b64EncodeUnicode(str) {
            // first we use encodeURIComponent to get percent-encoded UTF-8,
            // then we convert the percent encodings into raw bytes which
            // can be fed into btoa.
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                function toSolidBytes(match, p1) {
                    return String.fromCharCode('0x' + p1);
            }));
        }


  function loadImageFileAsURL(){
        var filesSelected = document.getElementById("uploadImage").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image').val(fileLoadedEvent.target.result);
                $('#image_upload_preview').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

    function loadImageUploadFileAsURL(){
        var filesSelected = document.getElementById("uploadImageUpdate").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#imageUpdate').val(fileLoadedEvent.target.result);
                $('#image_upload_preview').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    } 
     function loadImageFileAsURL1(){
        var filesSelected = document.getElementById("uploadImage1").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image1').val(fileLoadedEvent.target.result);
                $('#image_upload_preview1').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

    function loadImageUploadFileAsURL1(){
        var filesSelected = document.getElementById("uploadImageUpdate1").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#imageUpdate1').val(fileLoadedEvent.target.result);
                $('#image_upload_preview1').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    } 
             


    $(document).ready(function() {

         var table_customer = $('#customers').DataTable( {
            "ajax": {"url": my_url+"Entry/Customer/getAllListCustomers/"},
            "columns": [
                {"data": "checkbox" },
                {"data": "id"},
                {"data": "name"},
                {"data": "street"},
                {"data": "zip"},
                {"data": "city"},
                {"data": "payment"}
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        function reload_table(){
            table_customer.ajax.reload(null,false);
        }

        //create
         $(document).on('click', '#create_modal_customer', function(e){
                e.preventDefault();
                $('#modal_create_customer').modal({
                    show : true
                });
            });

         //INSERT

          $(document).on('submit', '#form_create_modal_customer', function(e){
               //view - create customer

               //var image_upload_preview = $('#image_upload_preview').siblings('img').attr('src');
               
               //alert(image_upload_preview);
               //BLOCO 1
               var name = $('#name_customer').val();

                if (name === "") {
                    name = "0";
                } else {
                    name = b64EncodeUnicode(name);
                }

               var street = $('#address_customer').val();
               if (street === "") {
                    street = "0";
                } else {
                    street = b64EncodeUnicode(street);
                }

               var zip = $('#zip_code_customer').val();
                if (zip === "") {
                    zip = "0";
                } 

               var countryselect = $('#country').val();
                if (countryselect === "") {
                    countryselect = "0";
                } else {
                    countryselect = b64EncodeUnicode(countryselect);
                }

               var employsselect = $('#employee_default').val();
                if (employsselect === "") {
                    employsselect = "0";
                } 

               var selectgroup = $('#customer_group').val();
                if (selectgroup === "") {
                    selectgroup = "0";
                } 

               //novos
               var zzstate = $('#state_customer').val();
                if (zzstate === "") {
                    zzstate = "0";
                } 

               var numbercustome = $('#number_customer').val();
                if (numbercustome === "") {
                    numbercustome = "0";
                } 

               var citycustome = $('#city_customer').val();
                if (citycustome === "") {
                    citycustome = "0";
                } else {
                    citycustome = b64EncodeUnicode(citycustome);
                }

               var Complementcustome = $('#complement_customer').val();
                if (Complementcustome === "") {
                    Complementcustome = "0";
                } 

               var districtcustome = $('#district_customer').val();
                if (districtcustome === "") {
                    districtcustome = "0";
                } else {
                    districtcustome = b64EncodeUnicode(districtcustome);
                }
               //alert(districtcustome);
               
               //BLOCO2
               var idcompany = $('#business_company_id').val();
                if (idcompany === "") {
                    idcompany = "0";
                } 

               var phonecompany = $('#business_phone').val();
                if (phonecompany === "") {
                    phonecompany = "0";
                } 

               var emailcompany = $('#business_email').val();
                if (emailcompany === "") {
                    emailcompany = "0";
                } else {
                    emailcompany = b64EncodeUnicode(emailcompany);
                }
               //BLOCO3
               var namePerson = $('#contact_person_name').val();
                if (namePerson === "") {
                    namePerson = "0";
                } else {
                    namePerson = b64EncodeUnicode(namePerson);
                }

               var phoneclint = $('#contact_person_phone').val();
                if (phoneclint === "") {
                    phoneclint = "0";
                } 

               var emailclient = $('#contact_person_email').val();
                if (emailclient === "") {
                    emailclient = "0";
                } else {
                    emailclient = b64EncodeUnicode(emailclient);
                }

               var describe = $('#contact_person_obs').val();
                if (describe === "") {
                    describe = "0";
                } 

               var taxlass = $('#contact_person_without_tax').val();
                if (taxlass === "") {
                    taxlass = "0";
                } 

               //view - invoices
               var viewname = $('#view_name_in_invoices').val();
                if (viewname === "") {
                    viewname = "0";
                } 

               var invoiceg = $('#group_invoice').val();
                if (invoiceg === "") {
                    invoiceg = "0";
                } 

               var withoutc = $('#without_customer').val();
                if (withoutc === "") {
                    withoutc = "0";
                } 

               var printcompany = $('#invoice_define_for_in_print_company').val();
                if (printcompany === "") {
                    printcompany = "0";
                } 

               var hideinvoice = $('#hide_items_in_paymeny_invoiced').val();
                if (hideinvoice === "") {
                    hideinvoice = "0";
                } 

               var invoiceelectro = $('#electron_invoice').val();
                if (invoiceelectro === "") {
                    invoiceelectro = "0";
                } 

               var themeinvoice = $('#invoice_template').val();
                if (themeinvoice === "") {
                    themeinvoice = "0";
                } 

               var payment = $('#payment_method_allowed').val();
                if (payment === "") {
                    payment = "0";
                } 

               var sendinvoice = $('#send_invoice_in_email').val();
                if (sendinvoice === "") {
                    sendinvoice = "0";
                } 

               var trackemail = $('#track_open_email_customers').val();
                if (trackemail === "") {
                    trackemail = "0";
                } 

               var coincustomer = $('#default_customer_currency').val();
                if (coincustomer === "") {
                    coincustomer = "0";
                } 

               var redirectinvoice = $('#redirect_invoices_for_customer').val();
                if (redirectinvoice === "") {
                    redirectinvoice = "0";
                } 

               var describeinvoice = $('#description_invoice').val();
                if (describeinvoice === "") {
                    describeinvoice = "0";
                } 

   
               //view - purchase_requests
               var boxcustomer = $('#when_the_box_selects_the_custumer').val();
                if (boxcustomer === "") {
                    boxcustomer = "0";
                } 

               var nshowcustomer = $('#dnot_show_data_of_the_customers').val();
                if (nshowcustomer === "") {
                    nshowcustomer = "0";
                } 

               var exchagersend = $('#exchange_address_company').val();
                if (exchagersend === "") {
                    exchagersend = "0";
                } 

               var ticket = $('#ticket_template').val();
                if (ticket === "") {
                    ticket = "0";
                } 

               var department = $('#department').val();
                if (department === "") {
                    department = "0";
                } 

               var project = $('#project').val();
                if (project === "") {
                    project = "0";
                }

               var limit = $('#limit_of_credit_in_store').val();
                if (limit === "") {
                    limit = "0";
                } 


               //view - collection_letter
               var emailpostcolle = $('#number_of_times_they_should').val();
                if (emailpostcolle === "") {
                    emailpostcolle = "0";
                } 

               //view - extra_data
               var fax = $('#fax').val();
                if (fax === "") {
                    fax = "0";
                } 

               var birth = $('#date_birthday').val();
                if (birth === "") {
                    birth = "0";
                } else {
                    birth = b64EncodeUnicode(birth);
                }
               var sms = $('#send_sms').val();
                if (sms === "") {
                    sms = "0";
                } 

               var sendemailclient = $('#send_email').val();
                if (sendemailclient === "") {
                    sendemailclient = "0";
                } 

               var identification = $('#electronic_identification').val();
                if (identification === "") {
                    identification = "0";
                } 

               var genre = $('#genre').val();
                if (genre === "") {
                    genre = "0";
                } 
               var language = $('#language').val();
                if (language === "") {
                    language = "0";
                } 

               var image = $('#image_upload_preview').attr('src');
                if (image === "") {
                    image = "0";
                }  else {
                    image = b64EncodeUnicode(image);
                }
               //alert(image);
               
               
               //$('#image').val(fileLoadedEvent.target.result);
               //$('#image_upload_preview').attr('src', fileLoadedEvent.target.result);
               var userlog = <?php echo $_SESSION['user']?>;

               
        e.preventDefault();
                $.ajax({
                    url : my_url+"Entry/Customer/insertCustomer/" + name + '/' + street + '/' +  zip + '/' +  countryselect + '/' +  employsselect + '/' +  namePerson+ '/' +  phoneclint + '/' +  emailclient + '/' +  selectgroup + '/' +  idcompany + '/' +  phonecompany + '/' +  emailcompany + '/' +  describe + '/' +  taxlass + '/' +  viewname + '/' +  invoiceg + '/' +  withoutc + '/' +  printcompany + '/' +  hideinvoice + '/' +  invoiceelectro + '/' +  themeinvoice + '/' +  payment  + '/' + sendinvoice + '/' + trackemail + '/' + coincustomer + '/' + redirectinvoice + '/' + describeinvoice + '/' + boxcustomer + '/' + nshowcustomer + '/' + exchagersend + '/' + ticket + '/' + department + '/' + project + '/' + limit + '/' + emailpostcolle + '/' + fax + '/' + birth + '/' + sms + '/' + sendemailclient + '/' + identification + '/' + genre + '/' + language + '/' + userlog + '/' + zzstate + '/' + numbercustome + '/' + citycustome + '/' + Complementcustome + '/' + districtcustome + '/' + image,
                    type: "POST",
                    dataType: 'JSON',
                    data : $('#form_create_modal_customer').serialize(),
                     success: function(data, textStatus, jqXHR){
                      //console.log(data.result);
                    if (data.result == 'success'){
                       
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_create_customer').modal('hide');
                    reload_table(table_customer);
                    //location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
                });
            });

        // Select tr
          /* $(document).on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                $(this).removeClass('selected');
            }
            else {
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
                table_customer.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        } );*/

        //UPDATE
            $(document).on('click', '#update', function(e){
            e.preventDefault();

             var trs = table_customer.$('tr.hover-select').find('input:checkbox').data('id');
             //trs é o id - variavel modificada por causa de bugs
            //}  
            //var array_deletes = {idCustomers : ids};
            //alert(trs);
            var request = $.ajax({
                url:          '<?php echo URL_BASE ?>Entry/customer/getCustomer/'+trs,
                cache:        false,
                data:         'id=' + trs,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            
            request.done(function(output){
                if (output.result == 'success'){
                     $('#form_update_modal_customer #id_customer').val(output.data.id);
                     $('#form_update_modal_customer #name_customer').val(output.data.name);
                     $('#form_update_modal_customer #address_customer').val(output.data.street);
                     $('#form_update_modal_customer #zip_code_customer').val(output.data.zip);
                     $('#form_update_modal_customer #country').val(output.data.country);


                    $('#form_update_modal_customer #business_phone').val(output.data.phone);
                    $('#form_update_modal_customer #business_email').val(output.data.email);
                    $('#form_update_modal_customer #number_customer').val(output.data.number);
                    $('#form_update_modal_customer #city_customer').val(output.data.city);
                    $('#form_update_modal_customer #business_company_id').val(output.data.company);
                    $('#form_update_modal_customer #district_customer').val(output.data.district);
                    $('#form_update_modal_customer #complement_customer').val(output.data.complement);

                    $('#form_update_modal_customer #contact_person_name').val(output.data.contactperson);
                    $('#form_update_modal_customer #contact_person_phone').val(output.data.phonecontact);
                    $('#form_update_modal_customer #contact_person_email').val(output.data.emailcontact);
                    $('#form_update_modal_customer #customer_group').val(output.data.customergp);
                    $('#form_update_modal_customer #employee_default').val(output.data.employs);
                    $('#form_update_modal_customer #contact_person_obs').val(output.data.describe1);
                    $('#form_update_modal_customer #contact_person_without_tax').prop("checked",output.data.withouttax);
                    $('#form_update_modal_customer #view_name_in_invoices').prop("checked",output.data.viewname);
                    $('#form_update_modal_customer #group_invoice').prop("checked",output.data.gpinvoice);
                    $('#form_update_modal_customer #without_customer').prop("checked",output.data.withoutcustomer);
                    $('#form_update_modal_customer #invoice_define_for_in_print_company').prop("checked",output.data.invoiceprint);
                    $('#form_update_modal_customer #hide_items_in_paymeny_invoiced').prop("checked",output.data.hidepayment);
                    $('#form_update_modal_customer #electron_invoice').prop("checked",output.data.electroninvoice);
                    $('#form_update_modal_customer #invoice_template').val(output.data.tmptinvoice);
                    $('#form_update_modal_customer #payment_method_allowed').val(output.data.payment);
                    $('#form_update_modal_customer #send_invoice_in_email').val(output.data.sendemail);
                    $('#form_update_modal_customer #track_open_email_customers').prop("checked",output.data.trackemail);
                    $('#form_update_modal_customer #default_customer_currency').val(output.data.currency);
                    $('#form_update_modal_customer #redirect_invoices_for_customer').val(output.data.redirectperson);
                    $('#form_update_modal_customer #description_invoice').val(output.data.describe2);
                    $('#form_update_modal_customer #when_the_box_selects_the_custumer').prop("checked",output.data.showinfo);
                    $('#form_update_modal_customer #dnot_show_data_of_the_customers').prop("checked",output.data.notshowdata);
                    $('#form_update_modal_customer #exchange_address_company').prop("checked",output.data.andresscompany);

                    $('#form_update_modal_customer #ticket_template').val(output.data.tmptticket);
                    $('#form_update_modal_customer #department').val(output.data.department);
                    $('#form_update_modal_customer #project').val(output.data.project);
                    $('#form_update_modal_customer #limit_of_credit_in_store').val(output.data.limit);
                    $('#form_update_modal_customer #number_of_times_they_should').val(output.data.chargetime);
                    $('#form_update_modal_customer #fax').val(output.data.fax);
                    $('#form_update_modal_customer #date_birthday').val(output.data.birt);
                    $('#form_update_modal_customer #send_sms').prop("checked",output.data.sms);
                    $('#form_update_modal_customer #send_email').prop("checked",output.data.emaily);
                    $('#form_update_modal_customer #electronic_identification').val(output.data.identification);
                    $('#form_update_modal_customer #genre').val(output.data.genre);
                    $('#form_update_modal_customer #language').val(output.data.language);
                    $('#form_update_modal_customer #state_customer').val(output.data.state);
                    //$('#form_update_modal_customer #image_upload_preview1').val(output.data.dataimg);
                    $('#form_update_modal_customer #image_upload_preview1').attr('src', output.data.dataimg);
                     $('#modal_update_customer').modal({
                    show : true
                        });

                   
               
               //var userlog = <?php echo $_SESSION['user']?>;
                   
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(true);
                }
            });
            request.fail(function(jqXHR, textStatus){
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(false);
                console.log(jqXHR.responseText);
            });
        });

            //form_update_modal_customer
            $(document).on('submit', '#form_update_modal_customer', function(e){
           
               var id = $("#form_update_modal_customer #id_customer").val();

               var name = $('#form_update_modal_customer #name_customer').val();
               if (name === "") {
                    name = "0";
                } else {
                    name = b64EncodeUnicode(name);
                }
               var street = $('#form_update_modal_customer #address_customer').val();
               if (street === "") {
                    street = "0";
                } else {
                    street = b64EncodeUnicode(street);
                }

               var zip = $('#form_update_modal_customer #zip_code_customer').val();
               if (zip === "") {
                    zip = "0";
                } 

               var countryselect = $('#form_update_modal_customer #country').val();
               if (countryselect === "") {
                    countryselect = "0";
                } else {
                    countryselect = b64EncodeUnicode(countryselect);
                }

               var employsselect = $('#form_update_modal_customer #employee_default').val();
               if (employsselect === "") {
                    employsselect = "0";
                } 

               var selectgroup = $('#form_update_modal_customer #customer_group').val();
               if (selectgroup === "") {
                    selectgroup = "0";
                } 

               //novos
              var zzstate = $('#form_update_modal_customer #state_customer').val();
              if (zzstate === "") {
                    zzstate = "0";
                } 

              var numbercustome = $('#form_update_modal_customer #number_customer').val();
              if (numbercustome === "") {
                    numbercustome = "0";
                } 

              var citycustome = $('#form_update_modal_customer #city_customer').val();
              if (citycustome === "") {
                    citycustome = "0";
                } else {
                    citycustome = b64EncodeUnicode(citycustome);
                }

              var Complementcustome = $('#form_update_modal_customer #complement_customer').val();
              if (Complementcustome === "") {
                    Complementcustome = "0";
                } 

              var districtcustome = $('#form_update_modal_customer #district_customer').val();
              if (districtcustome === "") {
                    districtcustome = "0";
                } else {
                    districtcustome = b64EncodeUnicode(districtcustome);
                }
              
               
               //BLOCO2
               var idcompany = $('#form_update_modal_customer #business_company_id').val();
               if (idcompany === "") {
                    idcompany = "0";
                } 

              var phonecompany = $('#form_update_modal_customer #business_phone').val();
              if (phonecompany === "") {
                    phonecompany = "0";
                } 

               var emailcompany = $('#form_update_modal_customer #business_email').val();
               //BLOCO3
               if (emailcompany === "") {
                    emailcompany = "0";
                } else {
                    emailcompany = b64EncodeUnicode(emailcompany);
                }

               var namePerson = $('#form_update_modal_customer #contact_person_name').val();
               if (namePerson === "") {
                    namePerson = "0";
                } else {
                    namePerson = b64EncodeUnicode(namePerson);
                }
               var phoneclint = $('#form_update_modal_customer #contact_person_phone').val();
               if (phoneclint === "") {
                    phoneclint = "0";
                } 

               var emailclient = $('#form_update_modal_customer #contact_person_email').val();
              if (emailclient === "") {
                    emailclient = "0";
                } else {
                    emailclient = b64EncodeUnicode(emailclient);
                }

              var describe = $('#form_update_modal_customer #contact_person_obs').val();
              if (describe === "") {
                    describe = "0";
                } 

               var taxlass = $('#form_update_modal_customer #contact_person_without_tax').val();
               if (taxlass === "") {
                    taxlass = "0";
                } 

               //view - invoices
               var viewname = $('#form_update_modal_customer #view_name_in_invoices').val();
               if (viewname === "") {
                    viewname = "0";
                } 

               var invoiceg = $('#form_update_modal_customer #group_invoice').val();
               if (invoiceg === "") {
                    invoiceg = "0";
                } 

               var withoutc = $('#form_update_modal_customer #without_customer').val();
               if (withoutc === "") {
                    withoutc = "0";
                } 

               var printcompany = $('#form_update_modal_customer #invoice_define_for_in_print_company').val();
               if (printcompany === "") {
                    printcompany = "0";
                } 

               var hideinvoice = $('#form_update_modal_customer #hide_items_in_paymeny_invoiced').val();
               if (hideinvoice === "") {
                    hideinvoice = "0";
                } 

               var invoiceelectro = $('#form_update_modal_customer #electron_invoice').val();
               if (invoiceelectro === "") {
                    invoiceelectro = "0";
                } 

               var themeinvoice = $('#form_update_modal_customer #invoice_template').val();
               if (themeinvoice === "") {
                    themeinvoice = "0";
                } 

               var payment = $('#form_update_modal_customer #payment_method_allowed').val();
               if (payment === "") {
                    payment = "0";
                } 

               var sendinvoice = $('#form_update_modal_customer #send_invoice_in_email').val();
               if (sendinvoice === "") {
                    sendinvoice = "0";
                } 

               var trackemail = $('#form_update_modal_customer #track_open_email_customers').val();
               if (trackemail === "") {
                    trackemail = "0";
                } 

               var coincustomer = $('#form_update_modal_customer #default_customer_currency').val();
               if (coincustomer === "") {
                    coincustomer = "0";
                } 
              var redirectinvoice = $('#form_update_modal_customer #redirect_invoices_for_customer').val();
              if (redirectinvoice === "") {
                    redirectinvoice = "0";
                } 

              var describeinvoice = $('#form_update_modal_customer #description_invoice').val();
              if (describeinvoice === "") {
                    describeinvoice = "0";
                } 
              
               //view - purchase_requests
              var boxcustomer = $('#form_update_modal_customer #when_the_box_selects_the_custumer').val();
              if (boxcustomer === "") {
                    boxcustomer = "0";
                } 

               var nshowcustomer = $('#form_update_modal_customer #dnot_show_data_of_the_customers').val();
               if (nshowcustomer === "") {
                    nshowcustomer = "0";
                } 

               var exchagersend = $('#form_update_modal_customer #exchange_address_company').val();
               if (exchagersend === "") {
                    exchagersend = "0";
                } 

              var ticket = $('#form_update_modal_customer #ticket_template').val();
              if (ticket === "") {
                    ticket = "0";
                } 

               var department = $('#form_update_modal_customer #department').val();
               if (department === "") {
                    department = "0";
                } 

               var project = $('#form_update_modal_customer #project').val();
               if (project === "") {
                    project = "0";
                } 

               var limit = $('#form_update_modal_customer #limit_of_credit_in_store').val();
               if (limit === "") {
                    limit = "0";
                } 


               //view - collection_letter
               var emailpostcolle = $('#form_update_modal_customer #number_of_times_they_should').val();
               if (emailpostcolle === "") {
                    emailpostcolle = "0";
                } 

               //view - extra_data
              var fax = $('#form_update_modal_customer #fax').val();
              if (fax === "") {
                    fax = "0";
                } 

              var birth = $('#form_update_modal_customer #date_birthday').val();
              if (birth === "") {
                    birth = "0";
                } else {
                    birth = b64EncodeUnicode(birth);
                }
              var sms = $('#form_update_modal_customer #send_sms').val();
              if (sms === "") {
                    sms = "0";
                } 

               var sendemailclient = $('#form_update_modal_customer #send_email').val();
               if (sendemailclient === "") {
                    sendemailclient = "0";
                } 

              var identification = $('#form_update_modal_customer #electronic_identification').val();
              if (identification === "") {
                    naidentificationme = "0";
                } 

               var genre = $('#form_update_modal_customer #genre').val();
               if (genre === "") {
                    genre = "0";
                } 

               var language = $('#form_update_modal_customer #language').val();
               if (language === "") {
                    language = "0";
                } 
               var image = $('#image_upload_preview1').attr('src');
                if (image === "") {
                    image = "0";
                }  else {
                    image = b64EncodeUnicode(image);
                }

               var userlog = <?php echo $_SESSION['user']?>;
            /*var removerimg = $('#form_customer_edit_edit #removerimg').val();
            if (removerimg === "") {
                removerimg = "0";
            }*/
             e.preventDefault();


             $.ajax({
                    url : my_url+"Entry/customer/CustomerEdit/" + id + '/' + name + '/' + street + '/' + zip + '/' + countryselect + '/'+ employsselect + '/' + namePerson + '/' + phoneclint + '/'+ emailclient + '/' + selectgroup + '/'+ idcompany + '/' + phonecompany + '/' + emailcompany + '/'+ describe + '/' + taxlass + '/' + viewname + '/'+ invoiceg + '/' + withoutc + '/' + printcompany + '/'+ hideinvoice + '/' + invoiceelectro + '/' + themeinvoice + '/'+ payment + '/' + sendinvoice + '/' + trackemail + '/'+ coincustomer + '/' + redirectinvoice + '/' + describeinvoice + '/'+ boxcustomer + '/' + nshowcustomer + '/' + exchagersend + '/'+ ticket + '/' + department + '/' + project + '/' + limit + '/'+ emailpostcolle + '/' + fax + '/' + birth + '/' + sms + '/'+ sendemailclient + '/' + identification + '/' + genre + '/'+ language + '/' + userlog + '/' + zzstate + '/' + numbercustome + '/'+ citycustome + '/' + Complementcustome + '/' + districtcustome + '/' + image,
                    type: "POST",
                    dataType: 'JSON',
                    data : $('#form_update_modal_customer').serialize(),
                     success: function(data, textStatus, jqXHR){
                      //console.log(data.result);
                    if (data.result == 'success'){
                       
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_update_customer').modal('hide');
                    reload_table(table_customer);
                    //location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
                });
             });

  
    
          
    //DELETE
       /* $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table_customer.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }         

            var array_deletes = {idCustomers : ids};
            alert(ids);
            deleteItens('Entry/customer/deleteCustomer/', array_deletes, table_customer);
        });*/

        // Delete
        $(document).on('click', '#delete', function (e) {
            e.preventDefault();
            var id = table_customer.$('tr.hover-select').find('input:checkbox').data('id');
            //var name = table_customer.$('tr.hover-select').find('input:text').data('name');
            //alert(t);
            //var name = table.$('tr.selected').find('a').data('name');
            //var id = table.$('tr.selected').find('a').data('id');
            
            var title = "Confirmation Needed";
            var text = "Are you sure you want to delete?";
            var icon = "glyphicon glyphicon-minus";
            confirmDelete(title, text, icon, id);
        });




     //confirme delete
      function confirmDelete(title, text, icon, id){

            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function(){
                $.ajax({
                    url:          '<?php echo URL_BASE ?>Entry/customer/deleteCustomer/' + id,
                    cache:        false,
                    dataType:     'json',
                    contentType:  'application/json; charset=utf-8',
                    type:         'get',
                     success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_update_customer').modal('hide');
                    reload_table(table_customer);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }

                });
                $('.ui-pnotify-modal-overlay').remove();
                    })
                    .on('pnotify.cancel', function(){
                        $('.ui-pnotify-modal-overlay').remove();
                    });
                                
                           
        }  
 
       $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#customers tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#customers');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<?php
    $customerGps = "";
    foreach ($array_answer['customerGps'] as $value) {
        $customerGps .= "<option value'{$value->getIdcustomerGp()}'>{$value->getName()}</option>";
    }
    
    $countrys = "";
    foreach ($array_answer['countrys'] as $value) {
        $countrys .= "<option value='{$value->getIdzzCountry()}'>{$value->getName()}</option>";
    }
    
    $employees = "";
    foreach ($array_answer['employees'] as $value) {
        $employees .= "<option value='{$value->getIdusers()}'>{$value->getLogin()}</option>";
    }
    
    // $templates_order = "";
    // foreach ($array_answer['templates'] as $value) {
    //     $templates_order .= "<option value='{$value->getIdordertmpt()}'>{$value->getName()}</option>";
    // }
    
    $currencies = "";
    foreach ($array_answer['currencies'] as $value) {
        $currencies .= "<option value='{$value->getIdcurrency()}'>{$value->getName()}</option>";
    }
    
    $paymentMethods = "";
    foreach ($array_answer['paymentMethods'] as $value) {
        $paymentMethods .= "<option value='{$value->getIdpaymentMtd()}'>{$value->getName()}</option>";
    }
    
    $customers = "";
    foreach ($array_answer['customers'] as $value) {
        if($value->getUsersusers() == null){
            $customers .= "<option value='{$value->getIdperson()}'>{$value->getName()}</option>";
        }
    }
    
    $departments = "";
    foreach ($array_answer['departments'] as $value) {
        $departments .= "<option value='{$value->getIddepartment()}'>{$value->getName()}</option>";
    }
    
    $projects = "";
    foreach ($array_answer['projects'] as $value) {
        $projects .= "<option value='{$value->getIdproject()}'>{$value->getName()}</option>";
    }
    
    $nav = "list";
    include 'nav.php';
?>

<div class="content">  
    <div class="row">
        <div class="col-md-12">

            <div id="messageAlert" role="alert"></div>
        </div>

        <div class="col-md-12">
            <div class="button-group">
                <button id="create_modal_customer" class="btn btn-primary"><span class="lnr lnr-menu-circle"></span> <t class="translate">New</t></button>
                <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
            </div>

            <table id="customers" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>                      
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('customers')"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Street</th>
                        <th class="translate">code Zip</th>
                        <th class="translate">City</th>
                        <th class="translate">Payment model</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<?php 
    //require_once 'modal/createCustomer.php';
    //modalCreateCustomer($customerGps, $countrys, $employees, $currencies, $paymentMethods, $customers, $departments, $projects, $templates_order);
?>

<div class="modal fade" id="modal_create_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 870px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title translate" id="myModalLabel">Registrar Cliente</h4>
                </div>
                <form id="form_create_modal_customer">
                    <div class="modal-body">
                        <div class="row row-fluid">
                            <div class="col-md-7">
                                <label for="name_customer" class="translate">Nome do Cliente:</label>
                                <input type="text" value="" required="required" class="form-control" name="name_customer" id="name_customer">
                            </div>
                           <!-- <div class="col-md-3">
                                <label for="status_customer">Status:</label>
                                <select name="status_customer" id="status_customer" required="">
                                    <option value="0">Select option</option>
                                    <option value="1">Active</option>
                                    <option value="2">Blocked</option>
                                    <option value="3">Deleted</option>
                                </select>
                            </div>-->
                            <div class="col-md-3">
                                <label for="customer_group" class="translate">Grupo de Clientes:</label>
                                <select name="customer_group" id="customer_group">
                                    <option value="0" class="translate">Select option</option>
                                    <?php //echo $customerGps ?>
                                    <?php
                                foreach ($customerGpOB as $valuescustomearray):
                                    ?><option value="<?php echo $valuescustomearray->getIdcustomerGp(); ?>"><?php echo $valuescustomearray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="zip_code_customer" class="translate">CEP:</label>
                                <input type="text" value="" class="form-control" name="zip_code_customer" id="zip_code_customer">
                            </div>
                        </div>


                        <div class="divider"></div>

                        <div class="row row-fluid">
                            <div class="col-md-4">
                                <label for="address_customer" class="translate">Endereço:</label>
                                <input type="text" value="" class="form-control" name="address_customer" id="address_customer">
                            </div>
                            <div class="col-md-2">
                                <label for="number_customer" class="translate">Numero:</label>
                                <input type="number" value="" class="form-control" name="number_customer" id="number_customer">
                            </div>
                            <div class="col-md-3">
                                <label for="city_customer" class="translate">Cidade:</label>
                                <input type="text" value="" class="form-control" name="city_customer" id="city_customer">
                            </div>
                            <div class="col-md-3">
                                <label for="district_customer" class="translate">Bairro:</label>
                                <input type="text" value="" class="form-control" name="district_customer" id="district_customer">
                            </div>
                            
                            
                        </div>

                        <div class="divider"></div>

                          <div class="row row-fluid">
                          <div class="col-md-3">
                                <label for="state_customer" class="translate">Estado:</label>
                                <select name="state_customer" id="state_customer">
                                    <!--<option value="0">Select option</option>-->
                                    <?php //echo $countrys ?>
                                     <option value="0" class="translate">Select option</option>
                                    <?php //echo $customerGps ?>
                                    <?php
                                foreach ($zzStateOB as $valuesstatearray):
                                    ?><option value="<?php echo $valuesstatearray->getIdzzState(); ?>"><?php echo $valuesstatearray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                                </select>
                                <!--<input type="text" value="" required="required" class="form-control" name="state_customer" id="state_customer">-->
                            </div>
                          <div class="col-md-4">
                                <label for="complement_customer" class="translate">Complemento:</label>
                                <input type="text" value="" class="form-control" name="complement_customer" id="complement_customer">
                            </div>
                            <div class="col-md-3">
                                <label for="country" class="translate">Country:</label>
                                <select name="country" id="country">
                                    <!--<option value="0">Select option</option>-->
                                    <?php //echo $countrys ?>
                                     <option value="0" class="translate">Select option</option>
                                    <?php //echo $customerGps ?>
                                    <?php
                                foreach ($zzCountryOB as $valuescountryarray):
                                    ?><option value="<?php echo $valuescountryarray->getIdzzCountry(); ?>"><?php echo $valuescountryarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label for="employee_default" class="translate">Funcionario Padrão:</label>
                                <select name="employee_default" id="employee_default">
                                    <option value="0" class="translate">Select option</option>
                                    <?php echo $employees ?>
                                </select>
                            </div>
                        </div>

                        <div class="divider"></div>

                        <div class="row row-fluid">
                            <div class="col-md-12">
                                <h4 class="no-padding" class="translate">Endereço Comercial</h4>
                            </div>
                            <!--<div class="col-md-4">
                                <label for="business_address">Address:</label>
                                <input type="text" value="" required="required" class="form-control" name="business_address" id="business_address">
                            </div>-->
                            <div class="col-md-4">
                                <label for="business_email" class="translate">E-mail:</label>
                                <input type="email" value="" class="form-control" name="business_email" id="business_email">
                            </div>
                            <div class="col-md-4">
                                <label for="business_company_id" class="translate">Nome da Companhia</label>
                                <select name="business_company_id" id="business_company_id">
                                    <!--<option value="0">Select option</option>-->
                                    <?php //echo $countrys ?>
                                     <option value="0" class="translate">Select option</option>
                                    <?php //echo $customerGps ?>
                                    <?php
                                foreach ($CompanyOB as $valuescompanyarray):
                                    ?><option value="<?php echo $valuescompanyarray->getIdcompany(); ?>"><?php echo $valuescompanyarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="business_phone" class="translate">Telefone:</label>
                                <input type="text" value="" class="form-control" name="business_phone" id="business_phone">
                            </div>
                            <!--<div class="col-md-4">
                                <label for="business_contact">Contact:</label>
                                <input type="text" value="" required="required" class="form-control" name="business_contact" id="business_contact">
                            </div>-->
                        </div>
                        <div class="row row-fluid">
                           <!-- <div class="col-md-4">
                                <label for="business_zip_code">ZIP code:</label>
                                <input type="text" value="" required="required" class="form-control number" name="business_zip_code" id="business_zip_code">
                            </div>
                                COMVERSAR SOBRE A COMPANY COM SERVULO
                              <div class="col-md-4"
                                <label for="business_company_id">Company ID:</label>
                                <input type="text" value="" required="required" class="form-control" name="business_company_id" id="business_company_id">
                            </div>-->
                            
                        </div>

                        <div class="divider"></div>

                        <div class="row row-fluid">
                            <div class="col-md-12">
                                <h4 class="no-padding translate">Contato Pessoal</h4>
                            </div>
                            <div class="col-md-4">
                                <label for="contact_person_name" class="translate">Nome:</label>
                                <input type="text" value="" class="form-control" name="contact_person_name" id="contact_person_name">
                            </div>
                            <div class="col-md-2">
                                <label for="contact_person_phone" class="translate">Telefone:</label>
                                <input type="text" value="" class="form-control" name="contact_person_phone" id="contact_person_phone">
                            </div>
                            <div class="col-md-2">
                                <label for="contact_person_email" class="translate">E-mail:</label>
                                <input type="email" value="" class="form-control" name="contact_person_email" id="contact_person_email">
                            </div>
                            <div class="col-md-4">
                                <label for="contact_person_obs" class="translate">Obs:</label>
                                <textarea id="contact_person_obs" name="contact_person_obs" class="form-control" rows="2"></textarea>
                                <br><p><input id="contact_person_without_tax" type="checkbox" name="contact_person_without_tax" > <t class="translate">Without tax</t></p>
                            </div>
                        </div>

                        <div class="divider"></div>

                        <div class="row">
                            <ul class="nav nav-tabs navbar-three" style="margin: 0px; padding-left: 30px;">
                                <!-- <li class="active"><a data-toggle="tab" href="#invoices" class="translate">Faturas</a></li> -->
                                <!-- <li><a data-toggle="tab" href="#purchase_requests" class="translate">Requesição de Compras</a></li> -->
                                <!-- <li><a data-toggle="tab" href="#collection_letter" class="translate">Collection Letter</a></li> -->
                                <li><a data-toggle="tab" href="#extra_data" class="translate">Dados Extras</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <?php
                                //include __DIR__.'/../tabs_customers/invoices.php';
                                //include __DIR__.'/../tabs_customers/purchase_requests.php';
                               // include __DIR__.'/../tabs_customers/collection_letter.php';
                               // include __DIR__.'/../tabs_customers/extra_data.php';
                            ?>

                            <div id="invoices" class="tab-pane active" style="margin-bottom: 30px">
                                <div class="row row-fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <p><input id="view_name_in_invoices" name="view_name_in_invoices" type="checkbox"> <t class="translate">Mostra nome na fatura</t></p>
                                            <p><input id="group_invoice" name="group_invoice" type="checkbox"> <t class="translate">Grupo de faturas</t></p>
                                            <p><input id="without_customer" name="without_customer" type="checkbox"> <t class="translate">Without Customer</t></p>
                                            <p><input id="invoice_define_for_in_print_company" name="invoice_define_for_in_print_company" type="checkbox"> <t class="translate">Invoice define for in print company</t></p>
                                            <p><input id="hide_items_in_paymeny_invoiced" name="hide_items_in_paymeny_invoiced" type="checkbox"> <t class="translate">Hide item's in payment invoiced</t></p>
                                            <p><input id="electron_invoice" name="electron_invoice" type="checkbox"> <t class="translate">Electronic invoice</t></p>
                                        </div>
                                        <div class="col-md-8">
                                            <!-- <div class="col-md-6">
                                                <label for="invoice_template" style="color: red" class="translate">Invoice template:</label>
                                                 <select name="invoice_template" id="invoice_template">
                                                   <option value="0" class="translate">Select Option</option>
                                                            <?php
                                                            foreach ($invoiceThemeOB as $valuesinvoiceThemearray):
                                                                ?><option value="<?php echo $valuesinvoiceThemearray->getIdinvoiceTmpt(); ?>"><?php echo $valuesinvoiceThemearray->getName(); ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                </select>
                                            </div> -->
                                            <div class="col-md-6">
                                                <label for="payment_method_allowed" class="translate">Payment method allowed:</label>
                                                <select name="payment_method_allowed" id="payment_method_allowed">
                                                    <!--<option value="0">Select option</option>-->
                                                    <?php //echo $paymentMethods ?>
                                                   <option value="0" class="translate">Select Option</option>
                                                            <?php
                                                            foreach ($paymentMtdOB as $valuespaymentarray):
                                                                ?><option value="<?php echo $valuespaymentarray->getIdpaymentMtd(); ?>"><?php echo $valuespaymentarray->getName(); ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="default_customer_currency" class="translate">Default customer currency:</label>
                                                <select name="default_customer_currency" id="default_customer_currency">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $currencies ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="redirect_invoices_for_customer" class="translate">Redirect Invoices for Customer:</label>
                                                <select name="redirect_invoices_for_customer" id="redirect_invoices_for_customer">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $customers ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="send_invoice_in_email" class="translate">Send invoice in email:</label>
                                                <input id="send_invoice_in_email" name="send_invoice_in_email" type="email" class="form-control">
                                                <p><input type="checkbox" name="track_open_email_customers" id="track_open_email_customers"> <t class="translate">Track open email customers</t></p>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="description_invoice" class="translate">Description</label>
                                            <textarea class="form-control" rows="4" name="description_invoice" id="description_invoice"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div id="purchase_requests" class="tab-pane" style="margin-bottom: 30px">
                                <div class="row row-fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <p><input id="when_the_box_selects_the_custumer" name="when_the_box_selects_the_custumer" type="checkbox"> <t class="translate">When the box selects the customer Automatically opens a Customer describe</t></p>
                                            <p><input id="dnot_show_data_of_the_customers" name="dnot_show_data_of_the_customers" type="checkbox"> <t class="translate">D'not show data of the customers in statical</t></p>
                                            <p><input id="exchange_address_company" name="exchange_address_company" type="checkbox"> <t class="translate">Exchange address company for address register in send</t></p>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="col-md-6">
                                                <label for="ticket_template" class="translate">Ticket template:</label>
                                                <!--<select name="ticket_template" id="ticket_template">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $templates_order ?>
                                                </select>-->
                                                <!-- <select name="ticket_template" id="ticket_template">
                                                   <option value="0" class="translate">Select Option</option>
                                                            <?php
                                                            foreach ($ticketTemplateOB as $valuesTicketTemplatearray):
                                                                ?><option value="<?php echo $valuesTicketTemplatearray->getIdticketTmpt(); ?>"><?php echo $valuesTicketTemplatearray->getTitle(); ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                </select> -->
                                            </div>
                                            <div class="col-md-6">
                                                <label for="project" class="translate">Project:</label>
                                                <select name="project" id="project">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $projects ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="department" class="translate">Department:</label>
                                                <select name="department" id="department">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $departments ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="limit_of_credit_in_store" class="translate">Limit of credit in store:</label>
                                                <input type="text" name="limit_of_credit_in_store" id="limit_of_credit_in_store" class="form-control money">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div id="collection_letter" class="tab-pane" style="margin-bottom: 30px">
                                <div class="row row-fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="translate">Number of times they should be sent letters of collection, before Of sending the ticket to the exchequer Of collection</label>
                                            <input type="text" name="number_of_times_they_should" id="number_of_times_they_should" class="form-control money">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="extra_data" class="tab-pane" style="margin-bottom: 30px">
                                <div class="row row-fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                              <img class="text-center" id="image_upload_preview" src="../../../assets/images/upload.png" alt="your image" onclick="$('#uploadImage').click()" width="200" value="1">
                                              <input type="file" id="uploadImage" name="uploadImage" style="display: none" onchange="loadImageFileAsURL()">
                                              <input type="text" id="image" name="image" style="display: none">
                                              <br>
                                              <label class="translate">Client Authorization</label>
                                              <p><input type="checkbox" id="send_sms"> <t class="translate">Send sms</t></p>
                                              <p><input type="checkbox" id="send_email"> <t class="translate">Send Email</t></p>
                                          </div>
                                        <div class="col-md-8">
                                            <div class="col-md-6">
                                                <label class="translate">Genre:</label>
                                                <select name="genre" id="genre">
                                                    <option value="0" class="translate">Not Specified</option>
                                                    <option value="1" class="translate">Male</option>
                                                    <option value="2" class="translate">Female</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="translate">Language:</label>
                                              <!--  <select name="language" id="language" required="true">
                                                    <option value="0">Select option</option>
                                                    <option value="1">English</option>
                                                    <option value="2">Portuguese</option>
                                                    <option value="3">Spanish</option>
                                                    <option value="4">Norwegian</option>
                                                </select>-->
                                                 <select name="language" id="language">
                                                                <option value="0" class="translate">Select option</option>
                                                                <?php //echo $customerGps ?>
                                                                <?php
                                                            foreach ($languageOB as $valueslanguagearray):
                                                                ?><option value="<?php echo $valueslanguagearray->getIdlanguage(); ?>"><?php echo $valueslanguagearray->getName(); ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="translate">Fax:</label>
                                                <input type="text" name="fax" id="fax" class="form-control number">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="translate">Electronic Identification:</label>
                                                <input type="text" name="electronic_identification" id="electronic_identification" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="translate">Date Birthday:</label>
                                                <input type="text" name="date_birthday" id="date_birthday" class="form-control date">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary translate">Insert</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- Update -->
<?php 
    //require_once 'modal/updateCustomer.php'; 
    //modalUpdateCustomer($customerGps, $countrys, $employees, $currencies, $paymentMethods, $customers, $departments, $projects, $templates_order);
?>


<div class="modal fade" id="modal_update_customer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 870px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title translate" id="myModalLabel">Atualizar Cliente</h4>
                </div>
                <form id="form_update_modal_customer">
                    <div class="modal-body">
                        <div class="row row-fluid">
                            <div class="col-md-7">
                                <label for="name_customer" class="translate">Nome:</label>
                                <input type="text" value="" required="required" class="form-control" name="id_customer" id="id_customer" style="display: none;">
                                <input type="text" value="" required="required" class="form-control" name="name_customer" id="name_customer">
                            </div>
                           <!-- <div class="col-md-3">
                                <label for="status_customer">Status:</label>
                                <select name="status_customer" id="status_customer" required="">
                                    <option value="0">Select option</option>
                                    <option value="1">Active</option>
                                    <option value="2">Blocked</option>
                                    <option value="3">Deleted</option>
                                </select>
                            </div>-->
                            <div class="col-md-3">
                                <label for="customer_group" class="translate">Group:</label>
                                <select name="customer_group" id="customer_group">
                                    <option value="0" class="translate">Select option</option>
                                    <?php //echo $customerGps ?>
                                    <?php
                                foreach ($customerGpOB as $valuescustomearray):
                                    ?><option value="<?php echo $valuescustomearray->getIdcustomerGp(); ?>"><?php echo $valuescustomearray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="zip_code_customer" class="translate">CEP:</label>
                                <input type="text" value=""  class="form-control" name="zip_code_customer" id="zip_code_customer">
                            </div>
                        </div>


                        <div class="divider"></div>

                        <div class="row row-fluid">
                            <div class="col-md-4">
                                <label for="address_customer" class="translate">Endereço:</label>
                                <input type="text" value=""  class="form-control" name="address_customer" id="address_customer">
                            </div>
                            <div class="col-md-2">
                                <label for="number_customer" class="translate">Número:</label>
                                <input type="number" value=""  class="form-control" name="number_customer" id="number_customer">
                            </div>
                            <div class="col-md-3">
                                <label for="city_customer" class="translate">Cidade:</label>
                                <input type="text" value=""  class="form-control" name="city_customer" id="city_customer">
                            </div>
                            <div class="col-md-3">
                                <label for="district_customer" class="translate">Bairro:</label>
                                <input type="text" value=""  class="form-control" name="district_customer" id="district_customer">
                            </div>
                            
                            
                        </div>

                        <div class="divider"></div>

                          <div class="row row-fluid">
                          <div class="col-md-3">
                                <label for="state_customer" class="translate">Estado:</label>
                                <!--<input type="text" value="" required="required" class="form-control" name="state_customer" id="state_customer">-->
                                <select name="state_customer" id="state_customer">
                                    <!--<option value="0">Select option</option>-->
                                    <?php //echo $countrys ?>
                                     <option value="0" class="translate">Select option</option>
                                    <?php //echo $customerGps ?>
                                    <?php
                                foreach ($zzStateOB as $valuesstatearray):
                                    ?><option value="<?php echo $valuesstatearray->getIdzzState(); ?>"><?php echo $valuesstatearray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                                </select>
                            </div>
                          <div class="col-md-4">
                                <label for="complement_customer" class="translate">Complemento:</label>
                                <input type="text" value="" class="form-control" name="complement_customer" id="complement_customer">
                            </div>
                            <div class="col-md-3">
                                <label for="country" class="translate">País:</label>
                                <select name="country" id="country">
                                    <!--<option value="0">Select option</option>-->
                                    <?php //echo $countrys ?>
                                     <option value="0" class="translate">Select option</option>
                                    <?php //echo $customerGps ?>
                                    <?php
                                foreach ($zzCountryOB as $valuescountryarray):
                                    ?><option value="<?php echo $valuescountryarray->getIdzzCountry(); ?>"><?php echo $valuescountryarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                                </select>
                            </div>
                        <!--     <div class="col-md-3">
                                <label for="employee_default" class="translate">Employee default:</label>
                                <select name="employee_default" id="employee_default">
                                    <option value="0" class="translate">Select option</option>
                                    <?php //echo $employees ?>
                                </select>
                            </div> -->
                        </div>

                        <div class="divider"></div>

                      <!--   <div class="row row-fluid hide">
                            <div class="col-md-12">
                                <h4 class="no-padding translate">Business address</h4>
                            </div>
                            <div class="col-md-4">
                                <label for="business_address">Address:</label>
                                <input type="text" value="" required="required" class="form-control" name="business_address" id="business_address">
                            </div>
                            <div class="col-md-4">
                                <label for="business_email" class="translate">Email:</label>
                                <input type="email" value="" required="required" class="form-control" name="business_email" id="business_email">
                            </div>
                            <div class="col-md-4">
                                <label for="business_company_id" class="translate">Company Name</label>
                                <select required="required" name="business_company_id" id="business_company_id">
                                    <option value="0">Select option</option>
                                    <?php //echo $countrys ?>
                                     <option value="0" class="translate">Select option</option>
                                    <?php //echo $customerGps ?>
                                    <?php
                                foreach ($CompanyOB as $valuescompanyarray):
                                    ?><option value="<?php echo $valuescompanyarray->getIdcompany(); ?>"><?php echo $valuescompanyarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="business_phone" class="translate">Phone:</label>
                                <input type="text" value="" required="required" class="form-control" name="business_phone" id="business_phone">
                            </div>
                            <div class="col-md-4">
                                <label for="business_contact">Contact:</label>
                                <input type="text" value="" required="required" class="form-control" name="business_contact" id="business_contact">
                            </div>
                        </div> -->
                        <!-- <div class="row row-fluid"> -->
                           <!-- <div class="col-md-4">
                                <label for="business_zip_code">ZIP code:</label>
                                <input type="text" value="" required="required" class="form-control number" name="business_zip_code" id="business_zip_code">
                            </div>
                                COMVERSAR SOBRE A COMPANY COM SERVULO
                              <div class="col-md-4"
                                <label for="business_company_id">Company ID:</label>
                                <input type="text" value="" required="required" class="form-control" name="business_company_id" id="business_company_id">
                            </div>-->
                            
                        </div>

                        <!-- <div class="divider"></div> -->

                        <!-- <div class="row row-fluid hide">
                            <div class="col-md-12">
                                <h4 class="no-padding translate">Contact person</h4>
                            </div>
                            <div class="col-md-4">
                                <label for="contact_person_name" class="translate">Name:</label>
                                <input type="text" value="" required="required" class="form-control" name="contact_person_name" id="contact_person_name">
                            </div>
                            <div class="col-md-2">
                                <label for="contact_person_phone" class="translate">Phone:</label>
                                <input type="text" value="" required="required" class="form-control" name="contact_person_phone" id="contact_person_phone">
                            </div>
                            <div class="col-md-2">
                                <label for="contact_person_email" class="translate">Email:</label>
                                <input type="email" value="" required="required" class="form-control" name="contact_person_email" id="contact_person_email">
                            </div>
                            <div class="col-md-4">
                                <label for="contact_person_obs" class="translate">Obs:</label>
                                <textarea id="contact_person_obs" name="contact_person_obs" class="form-control" rows="2"></textarea>
                                <br><p><input id="contact_person_without_tax" type="checkbox" name="contact_person_without_tax"> <t class="translate">Without tax</t></p>
                            </div>
                        </div> -->

                        <!-- <div class="divider"></div>

                        <div class="row hide">
                            <ul class="nav nav-tabs navbar-three" style="margin: 0px; padding-left: 30px;">
                                <li class="active"><a data-toggle="tab" href="#invoices1" class="translate">Invoices</a></li>
                                <li><a data-toggle="tab" href="#purchase_requests1" class="translate">Purchase Requests</a></li>
                                <li><a data-toggle="tab" href="#collection_letter1" class="translate">Collection Letter</a></li>
                                <li><a data-toggle="tab" href="#extra_data1" class="translate">Extra Data</a></li>
                            </ul>
                        </div> -->
                        <!-- <div class="tab-content">
                            <?php
                                //include __DIR__.'/../tabs_customers/invoices.php';
                                //include __DIR__.'/../tabs_customers/purchase_requests.php';
                               // include __DIR__.'/../tabs_customers/collection_letter.php';
                               // include __DIR__.'/../tabs_customers/extra_data.php';
                            ?>

                            <div id="invoices1 hide" class="tab-pane active" style="margin-bottom: 30px">
                                <div class="row row-fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <p><input id="view_name_in_invoices" name="view_name_in_invoices" type="checkbox"> <t class="translate">View name in invoices</t></p>
                                            <p><input id="group_invoice" name="group_invoice" type="checkbox"> <t class="translate">Group invoice</t></p>
                                            <p><input id="without_customer" name="without_customer" type="checkbox"> <t class="translate">Without Customer</t></p>
                                            <p><input id="invoice_define_for_in_print_company" name="invoice_define_for_in_print_company" type="checkbox"> <t class="translate">Invoice define for in print company</t></p>
                                            <p><input id="hide_items_in_paymeny_invoiced" name="hide_items_in_paymeny_invoiced" type="checkbox"> <t class="translate">Hide item's in payment invoiced</t></p>
                                            <p><input id="electron_invoice" name="electron_invoice" type="checkbox"> <t class="translate">Electronic invoice</t></p>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="col-md-6">
                                                <label for="invoice_template" style="color: red" class="translate">Invoice template:</label>
                                                <select style="color: red" name="invoice_template" id="invoice_template">
                                                    <option value="1">No giro</option>
                                                    <option value="2">No giro 2</option>
                                                    <option value="3">With giro</option>
                                                </select>
                                                 <select name="invoice_template" id="invoice_template">
                                                   <option value="0" class="translate">Select Option</option>
                                                            <?php
                                                            foreach ($invoiceThemeOB as $valuesinvoiceThemearray):
                                                                ?><option value="<?php echo $valuesinvoiceThemearray->getIdinvoiceTmpt(); ?>"><?php echo $valuesinvoiceThemearray->getName(); ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="payment_method_allowed" class="translate">Payment method allowed:</label>
                                                <select name="payment_method_allowed" id="payment_method_allowed">
                                                    <option value="0">Select option</option>
                                                    <?php //echo $paymentMethods ?>
                                                   <option value="0" class="translate">Select Option</option>
                                                            <?php
                                                            foreach ($paymentMtdOB as $valuespaymentarray):
                                                                ?><option value="<?php echo $valuespaymentarray->getIdpaymentMtd(); ?>"><?php echo $valuespaymentarray->getName(); ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="default_customer_currency" class="translate">Default customer currency:</label>
                                                <select name="default_customer_currency" id="default_customer_currency">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $currencies ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="redirect_invoices_for_customer" class="translate">Redirect Invoices for Customer:</label>
                                                <select name="redirect_invoices_for_customer" id="redirect_invoices_for_customer">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $customers ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="send_invoice_in_email" class="translate">Send invoice in email:</label>
                                                <input id="send_invoice_in_email" name="send_invoice_in_email" type="email" class="form-control">
                                                <p><input type="checkbox" name="track_open_email_customers" id="track_open_email_customers"> <t class="translate">Track open email customers</t></p>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="description_invoice" class="translate">Description</label>
                                            <textarea class="form-control" rows="4" name="description_invoice" id="description_invoice"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div id="purchase_requests1" class="tab-pane" style="margin-bottom: 30px">
                                <div class="row row-fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <p><input id="when_the_box_selects_the_custumer" name="when_the_box_selects_the_custumer" type="checkbox"> <t class="translate">When the box selects the customer Automatically opens a Customer describe</t></p>
                                            <p><input id="dnot_show_data_of_the_customers" name="dnot_show_data_of_the_customers" type="checkbox"> <t class="translate">D'not show data of the customers in statical</t></p>
                                            <p><input id="exchange_address_company" name="exchange_address_company" type="checkbox"> <t class="translate">Exchange address company for address register in send</t></p>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="col-md-6">
                                                <label for="ticket_template" class="translate">Ticket template:</label>
                                                <select name="ticket_template" id="ticket_template">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $templates_order ?>
                                                </select>
                                                <select name="ticket_template" id="ticket_template">
                                                    <option value="0">Select option</option>
                                                    <?php //echo $paymentMethods ?>
                                                   <option value="0" class="translate">Select Option</option>
                                                            <?php
                                                            foreach ($ticketTemplateOB as $valuesTicketTemplatearray):
                                                                ?><option value="<?php echo $valuesTicketTemplatearray->getIdticketTmpt(); ?>"><?php echo $valuesTicketTemplatearray->getTitle(); ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="project" class="translate">Project:</label>
                                                <select name="project" id="project">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $projects ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="department" class="translate">Department:</label>
                                                <select name="department" id="department">
                                                    <option value="0" class="translate">Select option</option>
                                                    <?php echo $departments ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="limit_of_credit_in_store" class="translate">Limit of credit in store:</label>
                                                <input type="text" name="limit_of_credit_in_store" id="limit_of_credit_in_store" class="form-control money">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div id="collection_letter1" class="tab-pane" style="margin-bottom: 30px">
                                <div class="row row-fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <label class="translate">Number of times they should be sent letters of collection, before Of sending the ticket to the exchequer Of collection</label>
                                            <input type="text" name="number_of_times_they_should" id="number_of_times_they_should" class="form-control money">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="extra_data1" class="tab-pane" style="margin-bottom: 30px">
                                <div class="row row-fluid">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                           <img class="text-center" id="image_upload_preview1" src="../../../assets/images/upload.png" alt="your image" onclick="$('#uploadImage1').click()" width="200" value="1">
                                            <input type="file" id="uploadImage1" name="uploadImage" style="display: none" onchange="loadImageFileAsURL1()">
                                            <input type="text" id="image1" name="image" style="display: none">
                                            <br>
                                            <label class="translate">Client Authorization</label>
                                            <p><input type="checkbox" id="send_sms"> <t class="translate">Send sms</t></p>
                                            <p><input type="checkbox" id="send_email"> <t class="translate">Send E-mail</t></p>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="col-md-6">
                                                <label class="translate">Genre:</label>
                                                <select name="genre" id="genre">
                                                    <option value="0" class="translate">Not Specified</option>
                                                    <option value="1" class="translate">Male</option>
                                                    <option value="2" class="translate">Female</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="translate">Language:</label>
                                                <select name="language" id="language" required="true">
                                                    <option value="0">Select option</option>
                                                    <option value="1">English</option>
                                                    <option value="2">Portuguese</option>
                                                    <option value="3">Spanish</option>
                                                    <option value="4">Norwegian</option>
                                                </select>
                                                 <select name="language" id="language">
                                                                <option value="0" class="translate">Select option</option>
                                                                <?php //echo $customerGps ?>
                                                                <?php
                                                            foreach ($languageOB as $valueslanguagearray):
                                                                ?><option value="<?php echo $valueslanguagearray->getIdlanguage(); ?>"><?php echo $valueslanguagearray->getName(); ?></option>
                                                                <?php
                                                            endforeach;
                                                            ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="translate">Fax:</label>
                                                <input type="text" name="fax" id="fax" class="form-control number">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="translate">Electronic Identification:</label>
                                                <input type="text" name="electronic_identification" id="electronic_identification" class="form-control">
                                            </div>
                                            <div class="col-md-6">
                                                <label class="translate">Date Birthday:</label>
                                                <input type="text" name="date_birthday" id="date_birthday" class="form-control date">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->

                    <div class="modal-footer">
                        <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary translate">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
