<?php
/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Dec 1, 2016
 */
$customerGpOB = getEm()->getRepository('CustomerGp')->findBy(array("active" => 1));
$PersonOB = getEm()->getRepository('Person')->findBy(array("active" => 1));
$ProductOB = getEm()->getRepository('Product')->findBy(array("active" => 1));
$productgOB = getEm()->getRepository('ProductGp')->findBy(array("active" => 1));
?>
<script type="text/javascript">

 function b64EncodeUnicode(str) {
            // first we use encodeURIComponent to get percent-encoded UTF-8,
            // then we convert the percent encodings into raw bytes which
            // can be fed into btoa.
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                function toSolidBytes(match, p1) {
                    return String.fromCharCode('0x' + p1);
            }));
        }

    /*module-folderview-pagename*/
    var pageName = "Entry-discount-Index";

    $(document).ready(function () {

        //GetAll
        var table = $('#discount').DataTable({
            "ajax": {"url": "<?php echo URL_BASE ?>Entry/customer/getAllDiscount/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "cgroup"},
                {"data": "idc"},
                {"data": "cname"},
                {"data": "pgroup"},
                {"data": "product"},
                {"data": "percent"}
            ],
            "aoColumnDefs": [
                {"bSortable": true, "aTargets": [-1]}
            ],
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "oLanguage": {
                "sLengthMenu": "Records per page: _MENU_",
                "sInfo": "Total of _TOTAL_ records (showing _START_ to _END_)",
                "sInfoFiltered": "(filtered from _MAX_ total records)"
            }
        });


        function reload_table() {
            console.log(table);
            table.ajax.reload(null, false);
        }

        // Add button
        $(document).on('click', '#create', function (e) {
            e.preventDefault();
            $('#myModal').modal({
                show: true
            });
        });

        // Select tr
        /*$(document).on('click', 'tr', function () {
            if ($(this).hasClass('selected')) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                $(this).removeClass('selected');
            } else {
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
        });*/

        // Add submit form
        /*$(document).on('submit', '#form_discount', function (e) {

            e.preventDefault();
            var form_data = $('#form_discount').serialize();
            var nameDiscount = $('#nameDiscount').val();
            if (nameDiscount === "") {
                nameDiscount = "0";
            }
            var clientname = $('#clientname').val();
            if (clientname === "") {
                clientname = "0";
            }
            var product = $('#product').val();
            if (product === "") {
                product = "0";
            }
            var productg = $('#productg').val();
            if (productg === "") {
                productg = "0";
            }
            var percent = $('#percent').val();
            alert(nameDiscount);
            alert(clientname);
            alert(product);
            alert(percent);
            var request = $.ajax({
                url: '<?php echo URL_BASE ?>Entry/customer/insertDiscount/' + nameDiscount + '/' + clientname + '/' + product + '/' + productg + '/' + percent,
               // cache: false,
                type: 'POST',
                dataType: 'json',
                data: $('#form_discount').serialize(),
                contentType: 'application/json; charset=utf-8'
                
            });

            request.done(function (output) {
                console.log(output);
                if (output.result === 'success') {
                    var title = "Added successfully";
                    var text = "Discount added successfully";
                    var icon = "glyphicon glyphicon-plus";
                    var type = "success";
                    $('#myModal').modal('hide');
                    reload_table();
                    notification(true);
                } else {
                    var title = "Add request failed";
                    var text = "Add request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(false);
                }
            });
            request.fail(function (jqXHR, textStatus) {
                var title = "Add request failed";
                var text = textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(false);
                console.log(jqXHR.responseText);
            });
        });*/
              $(document).on('submit', '#form_discount', function(e){
            var nameDiscount = $('#nameDiscount').val();
            var clientname = $('#clientname').val();
            var product = $('#product').val();
            var productg = $('#productg').val();
            var percent = b64EncodeUnicode($('#percent').val());
            
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/customer/insertDiscount/" + nameDiscount + '/' + clientname + '/' + product + '/' + productg + '/' + percent,
                type: "POST",
                dataType: 'JSON',
                data : $('#form_discount').serialize(),
                   success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $("#nameDiscount").val("");
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
       


            

        // Get for update
        $(document).on('click', '#update', function (e) {

            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            //var id = table.$('tr.selected').find('a').data('id');
            var request = $.ajax({
                url: '<?php echo URL_BASE ?>Entry/customer/getDiscount/' + id,
                cache: false,
                data: 'id=' + id,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                type: 'get'
            });
            request.done(function (output) {
                console.log(output.data);
                if (output.result == 'success') {
                    $('#form_discount_edit #idedit').val(output.data.idedit);
                    $('#form_discount_edit #nameDiscountedit').val(output.data.nameDiscountedit);
                    $('#form_discount_edit #clientnameedit').val(output.data.clientnameedit);
                    $('#form_discount_edit #productedit').val(output.data.productedit);
                    $('#form_discount_edit #productgedit').val(output.data.productgedit);
                    $('#form_discount_edit #percentedit').val(output.data.percentedit);
                    $('#discountEditModal').modal({show: true});
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(false);
                }
            });
            request.fail(function (jqXHR, textStatus) {
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(false);
                console.log(jqXHR.responseText);
            });
        });

        // Edit submit form
        $(document).on('submit', '#form_discount_edit', function (e) {
            var nameDiscount = $('#form_discount_edit #nameDiscountedit').val();
            if (nameDiscount == "") {
                nameDiscount = "0";
            }
            var clientname = $('#form_discount_edit #clientnameedit').val();
            if (clientname == "") {
                clientname = "0";
            }
            var product = $('#form_discount_edit #productedit').val();
            if (product == "") {
                product = "0";
            }
            var productg = $('#form_discount_edit #productgedit').val();
            if (productg == "") {
                productg = "0";
            }
            if (nameDiscount == null) {
                nameDiscount = "0";
            }
            if (clientname == null) {
                clientname = "0";
            }
            if (product == null) {
                product = "0";
            }
            if (productg == null) {
                productg = "0";
            }
            var percent = b64EncodeUnicode($('#form_discount_edit #percentedit').val());
            //var percent = $('#form_discount_edit #percentedit').val();
            e.preventDefault();
            var id = $('#form_discount_edit #idedit').val();
            var form_data = $('#form_discount_edit').serialize();
            var request = $.ajax({
                url: '<?php echo URL_BASE ?>Entry/customer/updateDiscount/' + id + '/' + nameDiscount + '/' + clientname + '/' + product + '/' + productg + '/' + percent,
                cache: false,
                data: form_data,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                type: 'get'
            });
            request.done(function (output) {
                if (output.result === 'success') {
                    var nameDiscount = $('#nameDiscount').val();
                    var title = "Added successfully";
                    var text = "Discount '" + nameDiscount + "' edited successfully";
                    var icon = "glyphicon glyphicon-plus";
                    var type = "success";
                    $('#discountEditModal').modal('hide');
                    reload_table();
                    notification(true);
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(false);
                }
            });
            request.fail(function (jqXHR, textStatus) {
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(false);
                console.log(jqXHR.responseText);
            });
        });

        // Delete
        $(document).on('click', '#delete', function (e) {
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            //var name = table.$('tr.selected').find('a').data('name');
            //var id = table.$('tr.selected').find('a').data('id');
            var title = "Confirmation Needed";
            var text = "Are you sure you want to delete?";
            var icon = "glyphicon glyphicon-minus";
            confirmDelete(title, text, icon, id);
        });

        // Delete action
        function confirmDelete(title, text, icon, id, name) {
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function () {
                $.ajax({
                    url: '<?php echo URL_BASE ?>Entry/customer/deleteDiscount/' + id,
                    cache: false,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    type: 'get',
                         success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#GroupEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
                 });
                $('.ui-pnotify-modal-overlay').remove();
                })
             .on('pnotify.cancel', function(){
                        $('.ui-pnotify-modal-overlay').remove();
            });
        }



        $("#discount tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;



            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
            var itemSelected = checkMark('#discount');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
               
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);

            }
            
           
        });
    });


</script>

<?php
$nav = "discounts";
include 'nav.php';
?>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div id="messageAlert" role="alert"></div>
        </div>
        <div class="col-md-12">
            <div class="button-group">
                <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
            </div>
            <table id="discount" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('discount')"></th>
                        <th>ID</th>
                        <th class="translate">Customer group</th>
                        <th class="translate">Id Customer</th>
                        <th class="translate">Client name</th>
                        <th class="translate">Product group</th>
                        <th class="translate">Single product</th>
                        <th class="translate">Discount percentage</th>
                    </tr>
                </thead>
            </table>
        </div>

    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Discount</h4>
            </div>
            <form id="form_discount" name="form_discount" class="form_discount">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameDiscount" class="label-style translate">Group Customer</label>
                            <input type="text" class="form-control" placeholder="Name" id="idDiscount" name="idDiscount" value="" style="display: none">
                            <select name="nameDiscount" id="nameDiscount">   
                                <option value="0" class="translate">Select Option</option>
                                <?php
                                foreach ($customerGpOB as $valuescustomearray):
                                    ?><option value="<?php echo $valuescustomearray->getIdcustomerGp(); ?>"><?php echo $valuescustomearray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="nameDiscount" class="label-style translate">Client name</label>
                            <select name="clientname" id="clientname"> 
                                <option value="0" class="translate">Select Option</option>
                                <?php
                                foreach ($PersonOB as $valuespersonarray):
                                    ?><option value="<?php echo $valuespersonarray->getIdperson(); ?>"><?php echo $valuespersonarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="nameDiscount" class="label-style translate">Product</label>
                            <select name="product" id="product"> 
                                <option value="0" class="translate">Select Option</option>
                                <?php
                                foreach ($ProductOB as $valuesproductarray):
                                    ?><option value="<?php echo $valuesproductarray->getIdproduct(); ?>"><?php echo $valuesproductarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label for="nameDiscount" class="label-style translate">Product Group</label>
                            <select name="productg" id="productg">
                                <option value="0" class="translate">Select Option</option>
                                <?php
                                foreach ($productgOB as $valuesproductgarray):
                                    ?><option value="<?php echo $valuesproductgarray->getIdproductGp(); ?>"><?php echo $valuesproductgarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="nameDiscount" class="label-style translate">Percent</label>
                            <input type="text" class="form-control percent" placeholder="0" id="percent" name="percent" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="discountEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Discount</h4>
            </div>

            <form id="form_discount_edit" name="form_discount_edit" class="form_discount_edit">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameDiscount" class="label-style translate">Group Customer</label>
                            <input type="text" class="form-control" placeholder="Name" id="idedit" name="idedit" value="" style="display: none">
                            <select name="nameDiscountedit" id="nameDiscountedit">  
                                <option value="0" class="translate">Select Option</option>
                                <?php
                                foreach ($customerGpOB as $valuescustomearray):
                                    ?><option value="<?php echo $valuescustomearray->getIdcustomerGp(); ?>"><?php echo $valuescustomearray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="nameDiscount" class="label-style translate">Client name</label>
                            <select name="clientnameedit" id="clientnameedit"> 
                                <option value="0" class="translate">Select Option</option>idedit
                                <?php
                                foreach ($PersonOB as $valuespersonarray):
                                    ?><option value="<?php echo $valuespersonarray->getIdperson(); ?>"><?php echo $valuespersonarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="nameDiscount" class="label-style translate">Product</label>
                            <select name="productedit" id="productedit"> 
                                <option value="0" class="translate">Select Option</option>
                                <?php
                                foreach ($ProductOB as $valuesproductarray):
                                    ?><option value="<?php echo $valuesproductarray->getIdproduct(); ?>"><?php echo $valuesproductarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label for="nameDiscount" class="label-style translate">Product Group</label>
                            <select name="productgedit" id="productgedit">
                                <option value="0" class="translate">Select Option</option>
                                <?php
                                foreach ($productgOB as $valuesproductgarray):
                                    ?><option value="<?php echo $valuesproductgarray->getIdproductGp(); ?>"><?php echo $valuesproductgarray->getName(); ?></option>
                                    <?php
                                endforeach;
                                ?>s
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="nameDiscount" class="label-style translate">Percent</label>
                            <input type="text" class="form-control percent" placeholder="Name" id="percentedit" name="percentedit" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>

            </form>
        </div>
    </div>
</div>