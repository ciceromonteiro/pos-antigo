<?php
$nav = "customer";
include 'nav.php';
?>

<style type="text/css">
    .row-fluid {
        padding-left: 25px;
        padding-right: 25px;
    }
    .no-padding {
        padding: 0px;
    }
    .no-padding-left {
        padding-left: 0px;
    }
    .divider {
        margin-bottom: 15px;
        margin-top: 15px;
        margin-right: auto;
        margin-left: auto;
        width: 95%;
        height: 2px;
        background-color: #EDECEC;
    }
    .navbar-three li {
        padding-bottom: 0px; 
        margin-bottom: 0px
    }
</style>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-6">
            <label>Name of customer:</label>
            <input type="text" value="" required="required" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-3">
            <label>Status:</label>
            <select name="type_product" id="type_product">
                <option value="0">Select option</option>
            </select>
        </div>
        <div class="col-md-3">
            <label>Customer group:</label>
            <select name="output_control" id="output_control">
                <option value="0">Select option</option>
            </select>
        </div>
    </div>
    
    <div class="divider"></div>
    
    <div class="row row-fluid">
        <div class="col-md-4">
            <label>Address:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-2">
            <label>ZIP code:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-3">
            <label>Country:</label>
            <select name="output_control" id="output_control">
                <option value="0">Select option</option>
            </select>
        </div>
        <div class="col-md-3">
            <label>Employee default:</label>
            <select name="output_control" id="output_control">
                <option value="0">Select option</option>
            </select>
        </div>
    </div>
    
    <div class="divider"></div>
    
    <div class="row row-fluid">
        <div class="col-md-12">
            <h4 class="no-padding">Business address</h4>
        </div>
        <div class="col-md-2">
            <label>Address:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-2">
            <label>ZIP code:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-2">
            <label>Company ID:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-2">
            <label>Phone:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-2">
            <label>Email:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-2">
            <label>Contact:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
    </div>
    
    <div class="divider"></div>
    
    <div class="row row-fluid">
        <div class="col-md-12">
            <h4 class="no-padding">Contact person</h4>
        </div>
        <div class="col-md-4">
            <label>Name:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-2">
            <label>Phone:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-2">
            <label>Email:</label>
            <input type="text" value="" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-4">
            <label>Obs:</label>
            <textarea class="form-control" rows="2"></textarea>
            <br><p><input type="checkbox" name="test"> Without tax</p>
        </div>
    </div>
    
    <div class="divider"></div>

    <div class="row">
        <ul class="nav nav-tabs navbar-three" style="margin: 0px; padding-left: 30px;">
            <li class="active"><a data-toggle="tab" href="#invoices">Invoices</a></li>
            <li><a data-toggle="tab" href="#purchase_requests">Purchase Requests</a></li>
            <li><a data-toggle="tab" href="#collection_letter">Collection Letter</a></li>
            <li><a data-toggle="tab" href="#extra_data">Extra Data</a></li>
        </ul>
    </div>
    <div class="tab-content">
        <?php 
            include 'tabs_customers/invoices.php';
            include 'tabs_customers/purchase_requests.php';
            include 'tabs_customers/collection_letter.php';
            include 'tabs_customers/extra_data.php';
        ?>
    </div>
    
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a href="#">POS favorite</a></li> 
                    <li><a href="#">Invoices of the customer</a></li> 
                    <li><a href="#">Statistics</a></li> 
                    <li><a href="#">Order</a></li> 
                    <li><a href="#">Delete</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button type="reset" class="btn btn-primary">Cancel</button>
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </div>
</div>