<?php
/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Dec 2, 2016
 */
$CustomerOB = new CustomerController();
?>


<script type="text/javascript">
var table;
    /*module-folderview-pagename*/
    var pageName = "Entry-department-Index";

    $(document).ready(function () {

        //GetAll
        table = $('#department').DataTable({
            "ajax": {"url": "<?php echo URL_BASE ?>Entry/customer/getAllCard"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "card"},
                {"data": "create"},
                {"data": "valueinit"},
                {"data": "balance"},
                {"data": "updatev"}
            ],
            "aoColumnDefs": [
                {"bSortable": true, "aTargets": [-1]}
            ],
            "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
            "oLanguage": {
                "sLengthMenu": "Records per page: _MENU_",
                "sInfo": "Total of _TOTAL_ records (showing _START_ to _END_)",
                "sInfoFiltered": "(filtered from _MAX_ total records)"
            }
        });


        function reload_table() {
            table.ajax.reload(null, false);
        }

        // Add button
        $(document).on('click', '#create', function (e) {
            e.preventDefault();
            $('#myModal').modal({
                show: true
            });
        });

 // Add submit form
        $(document).on('submit', '#form_giftcard', function(e){
            var nemaTemplate = $('#form_giftcard #nematemplate').val();
            var valueStart = $('#form_giftcard #valuestart').val();
            var person = $('#form_giftcard #selectperson').val();
            var info = $('#form_giftcard #info').val();
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/customer/insertGiftCard/"+ nemaTemplate + '/' + valueStart + '/' + person + '/' + info,
                type: "POST",
                dataType: 'JSON',
                data : $('#form_giftcard').serialize(),
                   success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $("#nameTemplate").val("");
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        // Get for update
        $(document).on('click', '#update', function (e) {
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            //var id = table.$('tr.selected').find('a').data('id');
            var request = $.ajax({
                url: '<?php echo URL_BASE ?>Entry/customer/getCard/'+id,
                cache: false,
                data: 'id=' + id,
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                type: 'get'
            });
            request.done(function (output) {
                if (output.result == 'success') {
                    $('#form_giftcard_edit #idCardedit').val(output.data[0].idCardedit);
                    $('#form_giftcard_edit #templateedit').val(output.data[0].templateedit);
                    $('#form_giftcard_edit #selectperson').val(output.data[0].selectperson);
                    $('#form_giftcard_edit #valuestart').val(output.data[0].valuestart);
                    $('#form_giftcard_edit #info').val(output.data[0].info);
                    $('#departmentEditModal').modal({show: true});
                } else {
                    var title = "failed";
                    var text = "Information request failed";
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                }
            });
            request.fail(function (jqXHR, textStatus) {
                var title = "failed";
                var text = "Information request failed: " + textStatus;
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
                console.log(jqXHR.responseText);
            });
        });

        // Edit submit form
         $(document).on('submit', '#form_giftcard_edit', function(e){
            e.preventDefault();
            var id        = $('#form_giftcard_edit #idCardedit').val();
            var nameTemplate = $('#form_giftcard_edit #templateedit').val();
            var valueStart = $('#form_giftcard_edit #valuestart').val();
            var person = $('#form_giftcard_edit #selectperson').val();
            var info = $('#form_giftcard_edit #info').val();
            var form_data = $('#form_cliente_edit').serialize();
            var request   = $.ajax({
                url:          '<?php echo URL_BASE ?>Entry/customer/updateCard/' + id +'/'+ nameTemplate +'/'+ valueStart+'/'+ person +'/'+ info,
                cache:        false,
                data:         form_data,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#departmentEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
            
        });
        // Delete
        $(document).on('click', '#delete', function (e) {
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
           // var name = table.$('tr.selected').find('a').data('name');
           // var id = table.$('tr.selected').find('a').data('id');
            var title = "Confirmation Needed";
            var text = "Are you sure you want to delete '" + name + "'?";
            var icon = "glyphicon glyphicon-minus";
            confirmDelete(title, text, icon, id, name);
        });

        // Delete action
        function confirmDelete(title, text, icon, id, name) {
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function () {
                $.ajax({
                    url: '<?php echo URL_BASE ?>Entry/customer/deleteCard/' + id,
                    cache: false,
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    type: 'get',
                     success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#GroupEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }

                });
             $('.ui-pnotify-modal-overlay').remove();
                    })
                    .on('pnotify.cancel', function(){
                        $('.ui-pnotify-modal-overlay').remove();
                    });
        }

        $("#department tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');

            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;



            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
            var itemSelected = checkMark('#department');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                $('#showDetails').addClass('disabled');

                
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
                $('#showDetails').removeClass('disabled');
               
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
                $('#showDetails').addClass('disabled');;


            }
            
           
        });


    });


/*

    function _signalFirst(v) {
        return /^(,|\.)/.test(v);
    }
    function _signalOne(v) {
        return /,|\.+\d$/.test(v);
    }
    function _onlyNumber(v) {
        return /^\d+$/.test(v);
    }
    function _rightPosition(v) {
        return /(,|\.)\d{2}$/.test(v);
    }
    function _format(v) {
        var length = v.length;

        if (_signalFirst(v)) {
            console.log('_signalFirst');

            v = v.replace(/[,.]/, '');
            if (length === 2)
                v = '0,' + v + '0';
            else
                v = '0,' + v;
        } else if (_onlyNumber(v)) {
            console.log('_onlyNumber');

            v = v + ',00';
        } else if (_rightPosition(v)) {
            console.log('_rightPosition');

            v = v.replace(/[,.]/, '');
            v = v.replace(/(\d)(\d{2})$/, '$1,$2');
        } else if (_signalOne(v)) {
            console.log('_signalOne');

            v = v.replace(/[,.]/, '');
            v = v + '0';
            v = v.replace(/(\d)(\d{2})$/, "$1,$2");
        } else {
            v = v.replace(/[,.]/, '');
            v = v.replace(/(\d{1,3})$/g, "$1,00");
            v = v.replace(/(\d{1,3})(\d{3},00)$/, "$1.$2");
        }
        return v;
    }

    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = _format;
        }
        exports._format = _format;
    }




    function id(el) {
        return document.getElementById(el);
    }
    window.onload = function () {
        id('valuestart').onkeyup = function () {
            var v = this.value;
            v = v.replace(/[^\d,.]/, '');
            this.value = v;
        };
        id('valuestart').onblur = function () {
            var v = this.value;
            this.value = _format(v);
        }
    };

*/


function showDetailsFunction(){
    var id = table.$('tr.hover-select').find('input:checkbox').data('id');
        $.ajax({
            url : my_url+"Entry/customer/getCardDetails/"+id,
            type: "POST",
            dataType: 'JSON',
            data : "",
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    $('#idValue').html(data.data[0].idCardedit);
                    $('#clientValue').html(data.data[0].person);
                    $('#companyValue').html(data.data[0].company);
                    $('#cardIdValue').html(data.data[0].card_identity);
                    $('#cardTemplateValue').html(data.data[0].template);
                    $('#ValueValue').html(data.data[0].value);
                    $('#valueUsedValue').html(data.data[0].value_used);
                    $('#extraInfoValue').html(data.data[0].extra_info);
                    $('#dateCreatedValue').html(data.data[0].date_created);
                    $('#dateUpdatedValue').html(data.data[0].date_updated);
                    $('#dateUpdatedValue').html(data.data[0].date_deleted);
                    $('#ActiveValue').html(data.data[0].active);
                    $('#showDetailsModal').modal({show: true});
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });

    
}
</script>

<?php
$nav = "card";
include 'nav.php';
?>

<style type="text/css">
    
.pointer{
    cursor: pointer;
}
.disabled{
    pointer-events:none;
    opacity:0.4;
}

</style>

<div class="content">
    <div class="row">
        <div class="row row-fluid">
            <div class="col-md-12">
                <div class="col-md-4">
                    <label class="label-style translate">Initial date:</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" id="dataStar" name="dataStar" required="true">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-th"></i>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="label-style translate">Final date:</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" id="dataEnd" name="dataEnd" required="true">
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-th"></i>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <label></label>
                    <p><input type="checkbox" name="showcard" id="showcard"> <t class="translate">Show gift card's</t></p>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12">
            <div class="button-group">
                <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
            </div>
            <table id="department" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('gift_cards')"></th>
                        <th class="translate">ID</th>
                        <th class="translate">ID Gift card</th>
                        <th class="translate">Date Create</th>
                        <th class="translate">Initial price</th>
                        <th class="translate">Balance </th>
                        <th class="translate">Update</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a class="translate" onclick="printJS({printable: 'department',
                                type: 'html',
                                showModal: true,
                                modalMessage: 'Printing...'})">Print</a></li> 
                    <li><a id="showDetails" class="translate disabled pointer" onclick="showDetailsFunction()">Show Details</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>
<?php
$giftTemplateArray = $CustomerOB->getTemplateGiftCard();
?>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Gift Card</h4>
            </div>
            <form id="form_giftcard" name="form_giftcard" class="form_giftcard">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameDepartment" class="label-style translate">Template</label>
                            <input type="text" class="form-control" placeholder="Name" id="idCard" name="idCard" value="" style="display: none">
                            <select class="form-control" id="nematemplate" name="nematemplate">                          
                                <?php
                                foreach ($giftTemplateArray as $valuegift):
                                    ?>
                                    <option value="<?php echo $valuegift->getIdgiftCardTmpt(); ?>"> <?php
                                        echo $valuegift->getName();
                                        ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="nameDepartment" class="label-style translate">Value</label>
                            <input type="text" class="form-control" placeholder="Value" id="valuestart" name="valuestart" value=""> 
                        </div>

                        <div class="col-md-12">
                            <label for="nameDepartment" class="label-style translate">Client</label>
                            <select class="form-control" id="selectperson" name="selectperson">
                                <?php
                                foreach ($view as $valor) {
                                    ?>
                                    <option value="<?php echo $valor->getIdperson(); ?>"> <?php
                                        echo $valor->getName();
                                        ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col-md-12">
                            <label for="nameDepartment" class="label-style translate">Info</label>
                            <textarea class="form-control" id="info" name="info" ></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="departmentEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Gift</h4>
            </div>

            <form id="form_giftcard_edit" name="form_giftcard_edit" class="form_giftcard_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="form_giftcard_edit" class="label-style translate">Template</label>
                            <input type="text" class="form-control" placeholder="Name" id="idCardedit" name="idCardedit" value="" style="display: none">              
                            <select class="form-control" id="templateedit" name="templateedit">
                                <option value=""></option>
                                <?php
                               foreach ($giftTemplateArray as $valuegift):
                                    ?>
                                    <option value="<?php echo $valuegift->getIdgiftCardTmpt(); ?>"> <?php
                                        echo $valuegift->getName();
                                        ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="form_giftcard_edit" class="label-style translate">Value</label>
                            <input type="text" class="form-control" placeholder="Value" id="valuestart" name="valuestart" value="" > 
                        </div>

                        <div class="col-md-12">
                            <label for="form_giftcard_edit" class="label-style translate">Client</label>
                            <select class="form-control" id="selectperson" name="selectperson">
                                <?php
                                foreach ($view as $valor) {
                                    ?>
                                    <option value="<?php echo $valor->getIdperson(); ?>"> <?php
                                        echo $valor->getName();
                                        ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="col-md-12">
                            <label for="form_giftcard_edit" class="label-style translate">Info</label>
                            <textarea class="form-control" id="info" name="info" ></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>;;
            </form>
        </div>
    </div>
</div>

<!-- modal show details -->
 <div class="modal fade" id="showDetailsModal" tabindex="-1" role="dialog"><!--  -->
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <h5><b>ID:</b> <t id="idValue"></t></h5> 
                    </div>
                    <div class="col-md-9">
                        <h5><b>Client:</b> <t id="clientValue"></t></h5> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <h5><b>Company:</b> <t id="companyValue"></t></h5> 
                    </div>
                    <div class="col-md-5">
                        <h5><b>Card Identity:</b> <t id="cardIdValue"></t></h5> 
                    </div>
                 </div>   
                 <div class="row">
                    <div class="col-md-4">
                        <h5><b>Template:</b> <t id="cardTemplateValue"></t></h5> 
                    </div>
                    <div class="col-md-4">
                        <h5><b>Value:</b> <t id="ValueValue"></t></h5> 
                    </div>
                    <div class="col-md-4">
                        <h5><b>Value used:</b> <t id="valueUsedValue"></t></h5> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h5><b>Extra Info:</b> <t id="extraInfoValue"></t></h5> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h5><b>Date Created:</b> <t id="dateCreatedValue"></t></h5> 
                    </div>
                    <div class="col-md-6">
                        <h5><b>Date updated:</b> <t id="dateUpdatedValue"></t></h5> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h5><b>Date Deleted:</b> <t id="dateDeletedValue"></t></h5> 
                    </div>
                    <div class="col-md-6">
                        <h5><b>Active:</b> <t id="ActiveValue"></t></h5> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>