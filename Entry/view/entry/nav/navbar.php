<?php
/* Created by PhpStorm.
 * User: rocha
 * Date: 10/24/16
 * Time: 2:39 PM
 */

$menuLeft = $navbar;
$menu = explode('|', $menuLeft);

?>

<div class="horizon-swiper">
    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/department/index' ?>">
            <div class='<?php echo ($menu[1] == "department") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-text-align-justify"></span>
                <t class="translate">Departamentos</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/project/index' ?>">
            <div class='<?php echo ($menu[1] == "project") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-calendar-full"></span>
                <t class="translate">Projects</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/user/index' ?>">
            <div class='<?php echo ($menu[1] == "user") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-user"></span>
                <t class="translate">Usuário</t>
            </div>
        </a>
    </div>

    <!-- <div class="horizon-item">
        <a href="<?php //echo URL_BASE.'Entry/warehouse/stocks' ?>">
            <div class='<?php// echo ($menu[1] == "warehouse") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-list"></span>
                <t class="translate">Warehouse</t>
            </div>
        </a>
    </div> -->

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/productSize/index' ?>">
            <div class='<?php echo ($menu[1] == "ProductSize") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-frame-contract"></span>
                <t class="translate">Tamanhos</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/color/index' ?>">
            <div class='<?php echo ($menu[1] == "color") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-highlight"></span>
                <t class="translate">Cores</t>
            </div>
        </a>
    </div>

    <!-- <div class="horizon-item">
        <a href="<?php// echo URL_BASE.'Entry/country/index' ?>">
            <div class='<?php// echo ($menu[1] == "country") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-earth"></span>
                <t class="translate">Country</t>
            </div>
        </a>
    </div> -->

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/supplier/index' ?>">
            <div class='<?php echo ($menu[1] == "supplier") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-book"></span>
                <t class="translate">Fornecedores</t>
            </div>
        </a>
    </div>

    <!-- <div class="horizon-item">
        <a href="<?php// echo URL_BASE.'Entry/template/index' ?>">
            <div class='<?php// echo ($menu[1] == "template") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-layers"></span>
                <t class="translate">Templates</t>
            </div>
        </a>
    </div> -->

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/customer/listed' ?>">
            <div class='<?php echo ($menu[1] == "customer") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-users"></span>
                <t class="translate">Clientes</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/product/index' ?>">
            <div class='<?php echo ($menu[1] == "product") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-shirt"></span>
                <t class="translate">Produtos</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php echo URL_BASE.'Entry/Company/index' ?>">
            <div class='<?php echo ($menu[1] == "Company") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-inbox"></span>
                <t class="translate">Companhia</t>
            </div>
        </a>
    </div>
    
   <!--  <div class="horizon-item">
        <a href="<?php //echo URL_BASE.'Entry/ShippingMethod/index' ?>">
            <div class='<?php //echo ($menu[1] == "ShippingMethod") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-inbox"></span>
                <t class="translate">Shipping Method</t>
            </div>
        </a>
    </div> -->

    <!-- <div class="horizon-item">
        <a href="<?php// echo URL_BASE.'Entry/invoice/index' ?>">
            <div class='<?php// echo ($menu[1] == "invoice") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-file-empty"></span>
                <t class="translate">Invoice</t>
            </div>
        </a>
    </div> -->

    <!-- <div class="horizon-item">
        <a href="<?php// echo URL_BASE.'Entry/tax/index' ?>">
            <div class='<?php// echo ($menu[1] == "tax") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-bookmark"></span>
                <t class="translate">Tax</t>
            </div>
        </a>
    </div>

    <div class="horizon-item">
        <a href="<?php// echo URL_BASE.'Entry/tribute/index' ?>">
            <div class='<?php// echo ($menu[1] == "tributes") ? "nav-btn active" : 'nav-btn' ?>'>
                <span class="lnr lnr-tag"></span>
                <t class="translate">Tributes</t>
            </div>
        </a>
    </div> -->
</div>

<script type="text/javascript">
    // $('.horizon-swiper').horizonSwiper();
</script>