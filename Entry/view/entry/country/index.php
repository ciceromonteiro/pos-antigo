<script type="text/javascript">
    $(document).ready(function() {

        var table = $('#country').DataTable( {
            "ajax": {"url": my_url+"Entry/Country/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "code" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_country');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_country_edit');
            $.ajax({
                url : my_url+"Entry/Country/getCountry/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#countryEditModal').modal({show : true});
                        $('#form_country_edit #idCountry').val(data.data[0].id);
                        $('#form_country_edit #nameCountry').val(data.data[0].name);
                        $('#form_country_edit #codeCountry').val(data.data[0].code);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_country', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Country/insert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_country').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_country_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Country/update",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_country_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#countryEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idCountrys : ids};
            deleteItens('Entry/Country/delete', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#country tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#country');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<!-- Table List Colors -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="country" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('country')"></th>
                    <th style="width: 10px" class="translate">ID</th>
                    <th class="translate">Name</th>
                    <th class="translate">Code</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Country</h4>
            </div>
            <form id="form_country" name="form_country" class="form_country">
                <div class="modal-body">
                    <div class="row">
                        <input type="text" id="idCountry" name="idCountry" style="display: none">
                        <div class="col-md-8">
                            <label for="nameCountry" class="label-style translate">Country</label>
                            <input type="text" class="form-control" id="nameCountry" name="nameCountry" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="nameCountry" class="label-style translate">Code</label>
                            <input type="text" class="form-control" id="codeCountry" name="codeCountry" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="countryEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Country</h4>
            </div>
            <form id="form_country_edit">
                <div class="modal-body">
                    <div class="row">
                        <input type="text" id="idCountry" name="idCountry" style="display: none">
                        <div class="col-md-8">
                            <label for="nameCountry" class="label-style translate">Country</label>
                            <input type="text" class="form-control" id="nameCountry" name="nameCountry" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="nameCountry" class="label-style translate">Code</label>
                            <input type="text" class="form-control" id="codeCountry" name="codeCountry" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>