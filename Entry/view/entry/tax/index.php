<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#tax').DataTable( {
            "ajax": {"url": my_url+"Entry/Tax/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "percent" },
                { "data": "mvaAccount" },
                { "data": "accountRelapse" },
                { "data": "externalTaxCode" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
        
        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_tax');
            $('#myModal').modal({show : true});
        });

        // Add submit form
        $(document).on('submit', '#form_tax', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Tax/insert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_tax').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        // Get for update
        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_tax_edit');
            $.ajax({
                url : my_url+"Entry/Tax/getTax/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#form_tax_edit #idTax').val(data.data[0].id);
                        $('#form_tax_edit #nameTax').val(data.data[0].name);
                        $('#form_tax_edit #mva_account').val(data.data[0].mvaAccount);
                        $('#form_tax_edit #percent').val(data.data[0].percent);
                        $('#form_tax_edit #account_relapse').val(data.data[0].accountRelapse);
                        $('#form_tax_edit #external_tax_code').val(data.data[0].externalTaxCode);
                        $('#taxEditModal').modal({show : true});
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        // Edit submit form
        $(document).on('submit', '#form_tax_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Tax/update",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_tax_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#taxEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        // Delete
        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idTaxs : ids};
            deleteItens('Entry/Tax/delete', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#tax tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#tax');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<!-- Menu -->
<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<!-- Table List Taxs -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="tax" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                    <th style="width: 80px" class="translate">ID</th>
                    <th class="translate">Name</th>
                    <th class="translate">Percent</th>
                    <th class="translate">MVA Account</th>
                    <th class="translate">Account relapse</th>
                    <th class="translate">external tax code</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Tax</h4>
            </div>
            <form id="form_tax" name="form_tax" class="form_tax">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameTax" class="label-style translate">Tax</label>
                            <input type="text" id="idTax" name="idTax" style="display: none">
                            <input type="text" class="form-control" id="nameTax" name="nameTax" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="percent" class="label-style translate">Percent</label>
                            <input type="text" class="form-control percent" id="percent" name="percent" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="mva_account" class="label-style translate">MVA Account</label>
                            <input type="text" class="form-control number" id="mva_account" name="mva_account" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="account_relapse" class="label-style translate">Account relapse</label>
                            <input type="text" class="form-control number" id="account_relapse" name="account_relapse" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="external_tax_code" class="label-style translate">External tax code</label>
                            <input type="text" class="form-control number" id="external_tax_code" name="external_tax_code" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="taxEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Tax</h4>
            </div>
            <form id="form_tax_edit" name="form_tax" class="form_tax">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameTax" class="label-style translate">Tax</label>
                            <input type="text" id="idTax" name="idTax" style="display: none">
                            <input type="text" class="form-control" id="nameTax" name="nameTax" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="percent" class="label-style translate">Percent</label>
                            <input type="text" class="form-control percent" id="percent" name="percent" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="mva_account" class="label-style translate">MVA Account</label>
                            <input type="text" class="form-control number" id="mva_account" name="mva_account" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="account_relapse" class="label-style translate">Account relapse</label>
                            <input type="text" class="form-control number" id="account_relapse" name="account_relapse" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="external_tax_code" class="label-style translate">External tax code</label>
                            <input type="text" class="form-control number" id="external_tax_code" name="external_tax_code" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>