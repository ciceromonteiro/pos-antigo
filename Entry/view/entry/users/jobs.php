<?php
    $init_select = "<option value=''>Select Option</option>";
    $department_select = "";
    foreach ($array_answer['departments'] as $value) {
        $department_select .= "<option value='".$value->getIddepartment()."'>".$value->getName()."</option>";
    }
    $department_select = $init_select.$department_select;
?>

<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#jobs').DataTable( {
            "ajax": {"url": my_url+"Entry/User/getAllJobs/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "department" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_job');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_job_edit');
            $.ajax({
                url : my_url+"Entry/User/getJob/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#jobEditModal').modal({show : true});
                        $('#form_job_edit #idJob').val(data.data[0].id);
                        $('#form_job_edit #name').val(data.data[0].name);
                        $("#form_job_edit #department").val(data.data[0].department).prop('selected', true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_job', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/User/insertJob",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_job').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_job_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/User/updateJob",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_job_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#jobEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idJobs : ids};
            deleteItens('Entry/User/deleteJobs', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#jobs tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#jobs');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<?php 
    $nav = "jobs";
    include "nav.php";
?>

<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="jobs" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('jobs')"></th>
                    <th style="width: 10px">ID</th>
                    <th class="translate">Name</th>
                    <th class="translate">Department</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Jobs</h4>
            </div>
            <form id="form_job">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name" class="label-style translate">Job</label>
                            <input type="text" class="form-control" id="name" name="name" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="department" class="label-style translate">Department</label>
                            <select name="department" required="true">
                                <?php echo $department_select ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="jobEditModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Job</h4>
            </div>
            <form id="form_job_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name" class="label-style translate">Job</label>
                            <input type="text" id="idJob" name="idJob" required="true" style="display: none">
                            <input type="text" class="form-control" id="name" name="name" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="department" class="label-style translate">Department</label>
                            <select name="department" id="department" required="true">
                                <?php echo $department_select ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>