<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#listAllPermissions').DataTable( {
            "ajax": {"url": my_url+"Entry/User/getAllPermissions/"},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "description" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
    });
</script>

<div id="allPermissions" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <table id="listAllPermissions" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width: 10px">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Description</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>