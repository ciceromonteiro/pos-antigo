<ul class="navbar-three">
    <li <?php echo ($nav == "Invoice") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/invoice/index" ?>" class="translate">Invoice</a>
    </li>
    <li <?php echo ($nav == "Default Alerts") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/invoice/default_alerts" ?>" class="translate">Default Alerts</a>
    </li>
    <li <?php echo ($nav == "Printer") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/invoice/printer" ?>" class="translate">Printer</a>
    </li>
</ul>