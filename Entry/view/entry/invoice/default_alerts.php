<?php
    $nav = "Default Alerts";
    include 'nav.php';
?>
<script type="text/javascript">
    $(document).ready(function() {

         var table = $('#default_alert_table').DataTable( {
            "ajax": {"url": my_url+"Entry/invoice/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "sendAfter" },
                { "data": "sendCopy" },
                { "data": "messageTitle" },
                { "data": "message" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_Alert_create');
            $('#myModal').modal({
                show : true
            });
        });


        $(document).on('submit', '#form_Alert_create', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/invoice/insertDefaultAlert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_Alert_create').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#default_alert_table tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#default_alert_table');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });    

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_Alert_edit');
            var id= + table.$('tr.hover-select').find('input:checkbox').data('id')
            $.ajax({
                url : my_url+"Entry/invoice/getDefaultAlert/"+id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#editModal').modal({show : true});
                        $('#form_Alert_edit #idDefaultAlert').val(data.data[0].id);
                        $('#form_Alert_edit #send_after').val(data.data[0].sendAfter);
                        $('#form_Alert_edit #send_copy').prop("checked",data.data[0].sendCopy);
                        $('#form_Alert_edit #message_title').val(data.data[0].messageTitle);
                        $('#form_Alert_edit #message').val(data.data[0].message);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });


        $(document).on('submit', '#form_Alert_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/invoice/updateDefaultAlert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_Alert_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#editModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

         $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idDefaultAlert : ids};
            deleteItens('Entry/invoice/deleteDefaultAlert', array_deletes, table);
        });

    });
</script>
<div class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="top-bar-buttons">
                <div class="button-group">
                    <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
                    <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
                    <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
                </div>
            </div>

            <table id="default_alert_table" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Send after</th>
                        <th class="translate">Send Copy</th>
                        <th class="translate">Title message</th>
                        <th class="translate">Message</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Default Alert</h4>
            </div>
            <form id="form_Alert_create">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Send After</label>
                            <input type="text" class="form-control number" id="send_after" name="send_after" required="true">
                        </div>
                        <div class="col-md-12">
                            <p><br><input type="checkbox" id="send_copy" name="send_copy"> <t class="translate">Send copy of the last message also to the inspection body</t></p>
                        </div>
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Message Title</label>
                            <input type="text" class="form-control" id="message_title" name="message_title" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Message</label>
                            <input type="text" class="form-control" id="message" name="message" required="true">
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Update -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Default Alert</h4>
            </div>
            <form id="form_Alert_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="number" id="idDefaultAlert" name="idDefaultAlert" required="true" style="display: none;">
                            <label for="description" class="label-style translate">Send After</label>
                            <input type="text" class="form-control number" id="send_after" name="send_after" required="true">
                        </div>
                        <div class="col-md-12">
                            <p><br><input type="checkbox" id="send_copy" name="send_copy"> <t class="translate">Send copy of the last message also to the inspection body</t></p>
                        </div>
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Message Title</label>
                            <input type="text" class="form-control" id="message_title" name="message_title" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="description" class="label-style translate">Message</label>
                            <input type="text" class="form-control" id="message" name="message" required="true">
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>