<?php
    $nav = "Printer";
    include 'nav.php';
?>
<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({
            url : my_url+"Entry/invoice/getPrinter",
            type: "POST",
            dataType: 'JSON',
               success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                         $('#text_closing').val(data.data[0].text_closing);
                         $('#size_font').val(data.data[0].size_font);
                         $('#description_printed').val(data.data[0].description_printed);
                         $('#number_copies').val(data.data[0].number_copies);
                         $('#number_places').val(data.data[0].number_places);
                         $('#print_drawing').prop("checked",data.data[0].print_drawing);
                         $('#enter_customer').prop("checked",data.data[0].enter_customer);
                         $('#change_location').prop("checked",data.data[0].change_location);
                         $('#print_taxes').prop("checked",data.data[0].print_taxes);
                         $('#not_print_project').prop("checked",data.data[0].not_print_project);
                         $('#use_default_printer').prop("checked",data.data[0].use_default_printer);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });

    });

    function generatePdf(){
         $('#loading').modal({
                show: true
            });
         document.getElementById("divImage").style.display = "block";
         document.getElementById("TextSemDados").style.display = "none";
        $.ajax({
                url : my_url+"Entry/invoice/generatePdf",
                type: "POST",
                dataType: 'JSON',
                   success: function(data, textStatus, jqXHR){  
                             
                    if (data.result == 'success'){
                        
                        if(data.data == 'Com dados'){
                            window.location.href = '../../../data/pdfTicket/TicketsPdfs.zip';
                            $('#loading').modal('hide');                           
                        }else{
                            document.getElementById("divImage").style.display = "none";
                            document.getElementById("TextSemDados").style.display = "block";
                            setTimeout(function() {
                                  $('#loading').modal('hide');
                                }, 2000);
                        }
                    } else {
                        notification(false);

                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }

    function saveData(){
        

        var text_closing = $('#text_closing').val();
            if (text_closing === "") {
                text_closing = "0";
                
            }
            text_closing = b64EncodeUnicode(text_closing);
        var size_font = $('#size_font').val();
            if (size_font === "") {
                size_font = "0";  
            }
            size_font = b64EncodeUnicode(size_font);
        var description_printed = $('#description_printed').val();
            if (description_printed === "") {
                description_printed = "0";
            } 
            description_printed = b64EncodeUnicode(description_printed);
        var number_copies = $('#number_copies').val();
            if (number_copies === "") {
                number_copies = "0";
            } 
            number_copies = b64EncodeUnicode(number_copies);   
        var number_places = $('#number_places').val();
            if (number_places === "") {
                number_places = "0";              
            }
            number_places = b64EncodeUnicode(number_places);
        var print_drawing = document.getElementById("print_drawing").checked;
            if (print_drawing === "") {
                print_drawing = "0";   
            } 
            print_drawing = b64EncodeUnicode(print_drawing);
        var enter_customer = document.getElementById("enter_customer").checked;
            if (enter_customer === "") {
                enter_customer = "0"; 
            } 
            enter_customer = b64EncodeUnicode(enter_customer);
        var change_location = document.getElementById("change_location").checked;
            if (change_location === "") {
                change_location = "0";    
            } 
            change_location = b64EncodeUnicode(change_location); 
        var print_taxes = document.getElementById("print_taxes").checked;
            if (print_taxes === "") {
                print_taxes = "0";              
            } 
            print_taxes = b64EncodeUnicode(print_taxes);
        var not_print_project = document.getElementById("not_print_project").checked;
            if (not_print_project === "") {
                not_print_project = "0";
            } 
            not_print_project = b64EncodeUnicode(not_print_project);
        var use_default_printer = document.getElementById("use_default_printer").checked;
            if (use_default_printer === "") {
                use_default_printer = "0";  
            } 
            use_default_printer = b64EncodeUnicode(use_default_printer);                     

             $.ajax({
                url : my_url+"Entry/invoice/insertPrinter/"+ text_closing +"/"+size_font+"/"+description_printed+"/"+number_copies+"/"+number_places+"/"+print_drawing+"/"+enter_customer+"/"+change_location+"/"+print_taxes+"/"+not_print_project+"/"+use_default_printer,
                type: "POST",
                dataType: 'JSON',
                data : "",
                   success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                       // $("#nameGroup").val("");
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });

    }
</script>

<style type="text/css">
    .divider {
        margin-bottom: 15px;
        margin-top: 15px;
        margin-right: auto;
        margin-left: auto;
        width: 95%;
        height: 2px;
        background-color: #EDECEC;
    }
</style>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12" style="margin-bottom: 60px;">
            <div class="col-md-3">
                <h4 class="translate">Text to include in the cashier closing document</h4>
                <textarea class="form-control" name="text_closing" id="text_closing"></textarea>
                
                <label class="translate">Size font</label>
                <input type="text" name="size_font" id="size_font" class="form-control number">
                
                <label class="translate">Description to be printed</label>
                <input type="text" name="description_printed" id="description_printed" class="form-control">
                
                <label class="translate">Number of copies</label>
                <input type="text" name="number_copies" id="number_copies" class="form-control number">
                
                <label class="translate">Number of decimal places</label>
                <input type="text" name="number_places" id="number_places" class="form-control number">
            </div>
            
            <div class="col-md-3">
                <h4 class="translate">All tickets will be saved in PDF</h4>
                <button onclick="generatePdf()" id="create" class="btn btn-primary col-md-12" style="margin-top: 20px" ><span class="lnr lnr-checkmark-circle"></span> <t class="translate">Generate PDF</t></button>
            </div>
            
            <div class="col-md-3">
                <h4 style="color: red" class="translate">Automatic debit solution</h4>
                <label style="color: red" class="translate">Select sending protocol</label>
                <select style="color: red">
                    <option value="1" class="translate">Save locally</option>
                    <option value="2" class="translate">FTP</option>
                    <option value="3" class="translate">SFTP</option>
                    <option value="4" class="translate">SCP</option>
                </select>

                <label style="color: red" class="translate">Keep on:</label>
                <input type="text" name="" class="form-control">

                <label style="color: red" class="translate">File name:</label>
                <input type="text" name="" class="form-control">
                <button style="color: red" id="create" class="btn btn-primary col-md-12" style="margin-top: 20px"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">Test</t></button>
            </div>
            
            <div class="col-md-3">
                <h4 class="translate">Other options</h4>
                <p><input type="checkbox" name="print_drawing" id="print_drawing"> <t class="translate">Print drawing of colors in the lines of each item</t></p>
                <p><input type="checkbox" name="enter_customer" id="enter_customer"> <t class="translate">Enter the customers phone number on the ticket</t></p>
                <p><input type="checkbox" name="change_location" id="change_location"> <t class="translate">Change the location where the billet number is printed</t></p>
                <p><input type="checkbox" name="print_taxes" id="print_taxes"> <t class="translate">Print Taxes Included</t></p>
                <p><input type="checkbox" name="not_print_project" id="not_print_project"> <t class="translate">Do not print project information on the ticket</t></p>
                <p><input type="checkbox" name="use_default_printer" id="use_default_printer"> <t class="translate">Use the default printer instead of the fiscal printer</t></p>
            </div>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-right">
            
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate" onclick="saveData()">Save</button>
        </div>
    </div>
</div>
<div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-center" role="document">
        <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                     <div class="col-md-12 text-center">
                     <div id="divImage" style="display: block;">
                     <img class="text-center" id="image_loading" src="../../../assets/images/default.gif" alt="your image"  width="50" value="1">
                     </div>
                     <h3 id="TextSemDados" style="display: none;" class="translate">no data</h3>
                     
                </div>
                </div>
        </div>
    </div>
</div>
