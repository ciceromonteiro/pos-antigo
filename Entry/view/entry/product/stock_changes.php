<?php
    $nav = "stock_changes";
    include 'nav.php';
?>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#stock_changes').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllStock/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "sku" },
                { "data": "type_row" },
                { "data": "cod" },
                { "data": "product" },
                { "data": "size" },
                { "data": "qtd" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            },
            "columnDefs": [
                {
                    "targets": [ 3 ],
                    "visible": false,
                    "searchable": false
                }
            ]
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            var type = table.rows( { selected: true } ).data()[0];
            if(id == null || id == undefined || id == '' || id == 'NaN'){
                return;
            }
            if(type == null || type == undefined || type == '' || type == 'NaN'){
                return;
            }
            get_product_stock(id, type.type_row);
        });
        
        $('#update').attr('disabled', true);
        $("#stock_changes tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;

            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
            
            var itemSelected = checkMark('#stock_changes');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
            }
        });
    });
</script>
<style type="text/css">
    .pointer{
        cursor: pointer;
    }
    .disabled{
        pointer-events:none;
        opacity:0.4;
    } 
</style>
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="update" class="btn btn-primary"><span class="lnr lnr-menu-circle"></span> <t>Editar qtd</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="stock_changes" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('stock_changes')"></th>
                        <th style="width: 80px">ID</th>
                        <th>SKU</th>
                        <th>Type</th>
                        <th>Código</th>
                        <th>Produto</th>
                        <th>Tamanho</th>
                        <th>Qtd</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Update Stock Changes -->
<div class="modal fade" id="modal_stock_changes" tabindex="-1" role="dialog" aria-labelledby="modal_stock_changes_Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_stock_changes_Label">Ajustar estoque</h4>
            </div>
            <form id="form_edit_stock_changes">
                <input type="hidden" name="id" id="edit_stock_changes_id" required="true">
                <input type="hidden" name="type" id="edit_stock_changes_type" required="true">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p><b>Nome do produto:</b></p>
                            <p id="edit_stock_changes_name">{nome_do_produto}</p>
                        </div>
                        <div class="col-md-6">
                            <label for="edit_stock_changes_qtd" class="label-style">Qtd:</label>
                            <input type="number" class="form-control" name="qtd" id="edit_stock_changes_qtd" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="edit_stock_changes_occurence" class="label-style">Ocorrência:</label>
                            <select name="occurence" id="edit_stock_changes_occurence">
                                <option value="" selected="selected">Selecione uma opção</option>
                                <option value="1">Perda</option>
                                <option value="2">Furto</option>
                                <option value="3">Recontagem</option>
                                <option value="4">Outros</option>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="edit_stock_changes_comment" class="label-style">Observação:</label>
                            <textarea class="form-control" name="comment" id="edit_stock_changes_comment"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Alterar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function get_product_stock(id, type){
        var jqxhr = $.get( my_url+"Entry/Product/getStockChange/"+id+"/"+type )
        .done(function(data) {
            var data = JSON.parse(data);
            if(data.status == 200 && data.item){
                $('#form_edit_stock_changes')[0].reset();
                $('#modal_stock_changes #edit_stock_changes_id').val(data.item[0].id);
                $('#modal_stock_changes #edit_stock_changes_type').val(data.item[0].type_row);
                $('#modal_stock_changes #edit_stock_changes_name').html(data.item[0].name);
                $('#modal_stock_changes #edit_stock_changes_qtd').val(data.item[0].qtd);
                $('#modal_stock_changes').modal({show: true});
            } else {
                var notice = new PNotify({
                    title: 'Ops..',
                    text: 'Ocorreu um erro ao tentar editar a quantidade em estoque do item',
                    icon: 'glyphicon glyphicon-minus',
                    type: 'error',
                    confirm: {
                        confirm: false
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                PNotify.prototype.options.styling = "bootstrap3";
                notice.get().click(function() {
                    notice.remove();
                });
            }
        })
        .fail(function() {
            var notice = new PNotify({
                title: 'Ops..',
                text: 'Ocorreu um erro ao tentar editar o produto',
                icon: 'glyphicon glyphicon-minus',
                type: 'error',
                confirm: {
                    confirm: false
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            PNotify.prototype.options.styling = "bootstrap3";
            notice.get().click(function() {
                notice.remove();
            });
        });
    }

    $('#form_edit_stock_changes').submit(function(e){
        e.preventDefault();
        $.post( my_url+"Entry/Product/saveStockChange", $( "#form_edit_stock_changes" ).serialize())
        .done(function( data ) {
            var data = JSON.parse(data);
            if(data.status == 200){
                var notice = new PNotify({
                    title: 'Ajuste de estoque concluido',
                    text: 'Item editado com sucesso',
                    icon: 'glyphicon glyphicon-plus',
                    type: 'success',
                    confirm: {
                        confirm: false
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                PNotify.prototype.options.styling = "bootstrap3";
                notice.get().click(function() {
                    notice.remove();
                });
                $('#modal_stock_changes').modal('hide');
                reload_table(table);
            } else {
                var notice = new PNotify({
                    title: 'Ops..',
                    text: 'Ocorreu um erro ao tentar editar o produto',
                    icon: 'glyphicon glyphicon-minus',
                    type: 'error',
                    confirm: {
                        confirm: false
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                PNotify.prototype.options.styling = "bootstrap3";
                notice.get().click(function() {
                    notice.remove();
                });
            }
            return;
        })
        .fail(function() {
            var notice = new PNotify({
                title: 'Ops..',
                text: 'Ocorreu um erro ao tentar editar o produto',
                icon: 'glyphicon glyphicon-minus',
                type: 'error',
                confirm: {
                    confirm: false
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            PNotify.prototype.options.styling = "bootstrap3";
            notice.get().click(function() {
                notice.remove();
            });
        });
    })
</script>