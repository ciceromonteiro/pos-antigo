<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#rules_table').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllRules/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "alternativePrice" },
                { "data": "showPopup" },
                { "data": "alternativeVat" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
        
        $(document).on('click', '#rules_create', function(e){
            e.preventDefault();
            resetForm('#form_rules_insert');
            $('#modal_rules_insert').modal({
                show : true
            });
        });
        
        $(document).on('submit', '#form_rules_insert', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertRule",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_rules_insert').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_rules_insert').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#rules_update', function(e){
            e.preventDefault();
            resetForm('#form_rules_edit');
            $.ajax({
                url : my_url+"Entry/Product/getRule/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#form_rules_edit #idRules').val(data.data[0].id);
                        $('#form_rules_edit #nameRules').val(data.data[0].name);
                        if(data.data[0].alternativePrice == 1){
                            $("#form_rules_edit #alternativePrice").attr("checked", true);
                        }
                        if(data.data[0].showPopup == 1){
                            $("#form_rules_edit #showPopup").attr("checked", true);
                        }
                        if(data.data[0].alternativeVat == 1){
                            $("#form_rules_edit #alternativeVat").attr("checked", true);
                        }
                        $('#modal_rules_edit').modal({show : true});
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_rules_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateRule",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_rules_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#modal_rules_edit').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#rules_delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idRules : ids};
            deleteItens('Entry/Product/deleteRules', array_deletes, table);
        });
        
        $('#rules_update').attr('disabled', true);
        $('#rules_delete').attr('disabled', true);
        $("#rules_table tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#rules_table');
            if(itemSelected == 0){
                $('#rules_update').attr('disabled', true);
                $('#rules_delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#rules_update').attr('disabled', false);
                $('#rules_delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#rules_update').attr('disabled', true);
                $('#rules_delete').attr('disabled', false);
            }
        });
        
    });
</script>

<?php
    $nav = "rules";
    include 'nav.php';
?>

<!-- Table List Ruless -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="rules_create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="rules_update" class="btn btn-primary"><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="rules_delete" class="btn btn-primary"><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="rules_table" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" onclick="checkAll('rules_table')"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Alternative price</th>
                        <th class="translate">Show popup</th>
                        <th class="translate">Alternative VAT</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_rules_insert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Rules</h4>
            </div>
            <form id="form_rules_insert" name="form_rules" class="form_rules">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameRules" class="label-style translate">Rules</label>
                            <input type="text" class="form-control" name="nameRules" id="nameRules" required="true">
                            <br><p><input type="checkbox" name="alternativePrice" id="alternativePrice"> <t class="translate">Alternative price</t></p>
                            <p><input type="checkbox" name="alternativeShowPopup" id="alternativeShowPopup"> <t class="translate">Show popup</t></p>
                            <p><input type="checkbox" name="alternativeVat" id="alternativeVat"> <t class="translate">Alternative Vat</t></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="modal_rules_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Rules</h4>
            </div>
            <form id="form_rules_edit" name="form_rules_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameRules" class="label-style translate">Rules</label>
                            <input type="text" id="idRules" name="idRules" value="0" style="display: none">
                            <input type="text" class="form-control" name="nameRules" id="nameRules" required="true">
                            <br><p><input type="checkbox" name="alternativePrice" id="alternativePrice"> <t class="translate">Alternative price</t></p>
                            <p><input type="checkbox" name="showPopup" id="showPopup"> <t class="translate">Show popup</t></p>
                            <p><input type="checkbox" name="alternativeVat" id="alternativeVat"> <t class="translate">Alternative Vat</t></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>