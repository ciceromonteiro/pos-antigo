<?php 

  

    $select = "<option value='0'>Select Option</option>";
    $select_group_from_tributes = "";
    $select_printers = "";
    $select_products = "";
    $productIncomplete = "";
    $name = '';

    foreach ($array_answer['tributes'] as $value){
        $select_group_from_tributes .= "<option value='".$value->getIdTribute()."'>".$value->getName()."</option>";
    } 
    
    foreach ($array_answer['printers'] as $value){
        $select_printers .= "<option value='".$value->getIdprinter()."'>".$value->getDescription()."</option>";
    }
    if(isset($array_answer['product'][0])){
        if($array_answer['product'][0]->getName() == '' || $array_answer['product'][0]->getName() == null){
            $name = "Unnamed";
        } else {
            $name = $array_answer['product'][0]->getName();
        }
        
        if($array_answer['product'][0]->getActive() == 1){
            $select_products .= "<option value='".$array_answer['product'][0]->getIdproduct()."'>".$name."</option>";
        }
    }
    
    $idproduct = $array_answer['idproduct'];
    
    $nav = "products";
    include 'nav.php';
?>

<style type="text/css">
    .row-fluid {
        padding-left: 25px;
        padding-right: 25px;
    }
    .no-padding {
        padding: 0px;
    }
    .no-padding-left {
        padding-left: 0px;
    }
    .divider {
        margin-bottom: 15px;
        margin-top: 15px;
        margin-right: auto;
        margin-left: auto;
        width: 95%;
        height: 2px;
        background-color: #EDECEC;
    }
    .navbar-three li {
        padding-bottom: 0px; 
        margin-bottom: 0px
    }
</style>

<div class="content">
    <div class="row row-fluid">
        <div class="col-md-2">
            <label class="translate">ID:</label>
            <div class="inner-addon right-addon">
                <input value="<?php echo $idproduct ?>" class="form-control" type="text" name="id_product" id="id_product" disabled="true">
            </div>
        </div>
        <div class="col-md-4">
            <label class="translate">Name of product:</label>
            <input type="text" value="<?= @$name ?>" required="required" class="form-control" name="name_product" id="name_product">
        </div>
        <div class="col-md-3">
            <label class="translate">Type of product:</label>
            <select name="type_product" id="type_product">
                <option value="0" class="translate">Select option</option>
                <option value="1" class="translate">Product</option>
                <option value="2" class="translate">Voucher</option>
                <option value="3" class="translate">Gift Card</option>
            </select>
        </div> 
        <div class="col-md-3">
            <label class="translate">Output control:</label>
            <select name="output_control" id="output_control">
                <option value="0" class="translate">Select option</option>
                <option value="1" class="translate">First in first on</option>
                <option value="2" class="translate">Last in first out</option>
                <option value="3" class="translate">First expiry first out</option>
            </select>
        </div>
    </div>
    
    <div class="divider"></div>

    <div class="row row-fluid">
        <div class="col-md-4">
            <h4 class="no-padding translate">Pricing of sell</h4>
            <div class="col-md-6 no-padding-left">
                <label class="translate">Value with tax:</label>
                <input type="text" required="required" class="form-control money" name="pricing_of_sell_value_with_tax" id="pricing_of_sell_value_with_tax" >
            </div>
            <div class="col-md-6 no-padding">
                <label class="translate">Value without tax:</label>
                <input type="text" required="required" class="form-control money" name="pricing_of_sell_value_without_tax" id="pricing_of_sell_value_without_tax" >
            </div>
        </div>

        <div class="col-md-4">
            <h4 class="no-padding translate">Pricing of buy</h4>
            <div class="col-md-6 no-padding-left">
                <label class="translate">Value of buy:</label>
                <input type="text" value="" required="required" onchange="setMargin(this.value)" class="form-control money" name="pricing_of_buy" id="pricing_of_buy">
            </div>
            <div class="col-md-6 no-padding">
                <label class="translate">Value of shipping:</label>
                <input type="text" value="" required="required" onchange="setMargin(this.value)" class="form-control money" name="pricing_of_shipping" id="pricing_of_shipping">
            </div>
        </div>

        <!-- <div class="col-md-4">
            <h4 class="no-padding translate">Profit</h4>
            <div class="col-md-6 no-padding-left">
                <label class="translate">Gross profit value:</label>
                <input type="text" onchange="setMargin(this.value)" required="required" class="form-control money" name="profit_gross_profit_value" id="profit_gross_profit_value">
            </div>
            <div class="col-md-6 no-padding">
                <label class="translate">Profit margin:</label>
                <input type="text" onchange="setMargin(this.value)" required="required" class="form-control percent" name="profit_margin" id="profit_margin">
            </div>
        </div> -->

        <!-- <div class="col-md-12">
            <div class="col-md-4 no-padding-left">
                <label class="translate">Groups from tributes</label>
                <select name="groups_from_tributes" id="groups_from_tributes">
                    <?php //echo $select.$select_group_from_tributes ?>
                </select>
            </div>
            <div class="col-md-3 no-padding">
                <label class="translate">Print for pick list</label>
                <select name="print_for_pick_list" id="print_for_pick_list">
                    <?php // echo $select.$select_printers; ?>
                    <option value="">Test</option>
                </select>
            </div> -->
        <!-- </div> -->
    </div>
    
    <div class="divider"></div>

    <div class="row">
        <ul class="nav nav-tabs navbar-three" style="margin: 0px; padding-left: 30px;">
            <!-- <li class="active"><a data-toggle="tab" href="#promotional" class="translate">Promotional</a></li> ignore for now... -->
            <li class="active"><a data-toggle="tab" href="#groups_from_products" class="translate">Groups from products</a></li>
            <li><a data-toggle="tab" href="#image_from_product" class="translate">Image from product</a></li>
        </ul>
    </div>
    <div class="tab-content">
        <?php 
            // include 'tabs_products/promotional.php'; ignore for now...
            include 'tabs_products/groups_from_products.php';
            include 'tabs_products/image_from_product.php';
        ?>
    </div>
        
    <div class="row">
        <ul class="nav nav-tabs navbar-three-min" style="margin: 0px; padding-left: 30px;">
            <!-- <li class="active"><a data-toggle="tab" href="#price" class="translate">Price</a></li> not use in brazil -->
            <li class="active"><a data-toggle="tab" href="#extra" class="translate">Extra</a></li>
            <li><a data-toggle="tab" href="#components" class="translate">Components</a></li>
            <li><a data-toggle="tab" href="#alternatives" class="translate">Alternatives</a></li>
            <li><a data-toggle="tab" href="#color_sizes" class="translate">Color/Sizes</a></li>
            <li><a data-toggle="tab" href="#additional" class="translate">Additional</a></li>
            <li><a data-toggle="tab" href="#orders" class="translate">Orders</a></li>
            <li><a data-toggle="tab" href="#acquisition" class="translate">Acquisition</a></li>
            <li><a data-toggle="tab" href="#webshop" class="translate">WebShop</a></li>
        </ul>
    </div>
    <div class="tab-content">
        <?php
            // include 'tabs_products/price.php'; not use in brazil
            include 'tabs_products/extra.php';
            include 'tabs_products/components.php';
            include 'tabs_products/alternatives.php';
            include 'tabs_products/color_sizes.php';
            include 'tabs_products/additional.php';
            include 'tabs_products/orders.php';
            include 'tabs_products/acquisition.php';
            include 'tabs_products/webshop.php';
        ?>
    </div>
    
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a href="#" class="translate">Statistics</a></li> 
                    <li><a href="#" class="translate">Labels</a></li> 
                    <li><a href="#" class="translate">POS</a></li> 
                    <li><a href="#" class="translate">Cod EAN</a></li> 
                    <li><a href="#" class="translate">Delete</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button type="reset" class="btn btn-primary translate">Cancel</button>
            <button type="submit" class="btn btn-primary translate" onclick="saveProductTemp('temporarily')">Save temporarily</button>
            <button type="submit" class="btn btn-success translate" onclick="saveProductTemp('complete')">Save</button>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    var idProduct = "<?php echo $idproduct ?>";
    
    //window.location.hash = idProduct;
    
    load(idProduct);
        
    function load(id){
        $('#continue_registration').modal('hide');
        $('#id_product').val(id);
        loadFieldsAndTables(id);
    }
    
    function loadFieldsAndTables(id){
        loadFields(id);
        loadPromocional(id);
        loadComponent(id);
//        loadAdditional(id);
        loadGroupsFromProducts(id);
        loadImageFromProducts(id);
        loadWholesalePrice(id);
        //loadColorSizes(id);
        //loadOrdersProduct(id);
    }
    
    function loadFields(id){
        $.ajax({
            url : my_url+"Entry/Product/getProduct/"+id,
            type: "POST",
            dataType: 'JSON',
            data : '',
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    var values = data.data[0];
                    for (var prop in values) {
                        if (prop == "components_compound_product" || prop == "components_show_order_subitens" || prop == "prediction_of_purchase"){
                            //checkboxes
                            if(values[prop] == "on" || values[prop] == "1"){
                                document.getElementById(prop).checked = true;
                            } else {
                                document.getElementById(prop).checked = false;
                            }
                        } else if (prop == "type_product" || prop == "output_control" ||
                        prop == "groups_from_tributes" || prop == "print_for_pick_list" ||
                        prop == "number_of_serie_from_product" || prop == "select_manufacturer" ||
                        prop == "select_supplier" || prop == "select_unit_from_product" ||
                        prop == "select_base_of_calc" || prop == "status_of_product" ||
                        prop == "select_department" || prop == "select_projects" ||
                        prop == "place_where_the_product" || prop == "select_currency"){
                            //selects
                            if(values[prop] == "" || values[prop] == null || values[prop] == 'null'){
                                document.getElementById(prop).value = 0;
                            } else{
                                document.getElementById(prop).value = values[prop];
                            }
 
                       } else {
                            //inputs == ERRO ESTÁ AQUI
                            document.getElementById(prop).value = values[prop];
                        }
                    }
                } else if(data.result == 'not-exist'){
                    alert('Product does not exist, try again other id');
                } else {
                    notification(false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    }

    function setPriceProduct(price){
        price = parseFloat(price);
        $('#pricing_of_sell_value_with_tax').val(addMask(price));
        $('#pricing_of_sell_value_without_tax').val(addMask(price));
        $('#profit_gross_profit_value').val(addMask(price));
        if($('#profit_margin').val() == ''){
            $('#profit_margin').val('100,00%');
        }    
    }

    function setMargin(value){/*
        if($('#pricing_of_sell_value_with_tax').val() == ''){
            $('#profit_gross_profit_value').val('');
            $('#profit_margin').val('');
            return;
        }
        if($('#pricing_of_buy').val() == ''){
            var pricingOfBuy = 0;
        } else {
            var pricingOfBuy = parseFloat($('#pricing_of_buy').val());
        }  

        if($('#pricing_of_shipping').val() == ''){
            var pricingOfShipping = 0;
        } else {
            var pricingOfShipping = parseFloat($('#pricing_of_shipping').val());
        }

        var pricingOfSellWithTax = parseFloat($('#pricing_of_sell_value_with_tax').val());
        var pricingOfSellWithoutTax = parseFloat($('#pricing_of_sell_value_without_tax').val());

        var grossPrice = (pricingOfSellWithTax - (pricingOfBuy + pricingOfShipping));
        var margin = (grossPrice/pricingOfSellWithTax) * 100;

        $('#profit_gross_profit_value').val(addMask(grossPrice));
        $('#profit_margin').val(margin+"%");
    */}

    function addMask(value){/*
        var check = Number.isInteger(value);
        if(check == true){
            return value+",00";
        } else {
            return value;
        }
    */}
    
    function saveProductTemp(type){
      
        var array_values = {};
        //Input and Select
        array_values['id_product'] = $('#id_product').val();
        array_values['name_product'] = $('#name_product').val();
        array_values['type_product'] = $('#type_product option:selected').val();
        array_values['output_control'] = $('#output_control option:selected').val();
        array_values['pricing_of_sell_value_without_tax'] = $('#pricing_of_sell_value_without_tax').val();
        array_values['pricing_of_buy'] = $('#pricing_of_buy').val();
        array_values['pricing_of_shipping'] = $('#pricing_of_shipping').val();
        array_values['profit_gross_profit_value'] = $('#profit_gross_profit_value').val();
        array_values['groups_from_tributes'] = $('#groups_from_tributes option:selected').val();
        array_values['print_for_pick_list'] = $('#print_for_pick_list option:selected').val();
        array_values['price_of_table'] = $('#price_of_table').val();
        array_values['recom_value_for_sell_liquid'] = $('#recom_value_for_sell_liquid').val();
        array_values['recom_value_for_sell_brute'] = $('#recom_value_for_sell_brute').val();
        array_values['recom_value_for_sell_liquid'] = $('#recom_value_for_sell_liquid').val();
        array_values['trade_value_for_sell_brute'] = $('#trade_value_for_sell_brute').val();
        array_values['trade_value_for_sell_liquid'] = $('#trade_value_for_sell_liquid').val();
        array_values['value_for_travel_brute'] = $('#value_for_travel_brute').val(); 
        array_values['value_for_travel_liquid'] = $('#value_for_travel_liquid').val();
        array_values['number_of_serie_from_product'] = $('#number_of_serie_from_product').val();
        array_values['select_manufacturer'] = $('#select_manufacturer option:selected').val();
        array_values['select_supplier'] = $('#select_supplier option:selected').val();
        array_values['select_unit_from_product'] = $('#select_unit_from_product option:selected').val();
        array_values['select_base_of_calc'] = $('#select_base_of_calc option:selected').val();
        array_values['number_of_manufacture'] = $('#number_of_manufacture').val();
        array_values['number_of_supplier'] = $('#number_of_supplier').val();
        array_values['multiple_units'] = $('#multiple_units').val();
        if(type == 'complete'){
            array_values['status_of_product'] = 1;
        } else { 
           array_values['status_of_product'] = $('#status_of_product option:selected').val();
        }
        
        
        array_values['select_department'] = $('#select_department option:selected').val();
        array_values['select_projects'] = $('#select_projects option:selected').val();
        array_values['place_where_the_product'] = $('#place_where_the_product option:selected').val();
        array_values['value_for_sell_liquid'] = $('#pricing_of_sell_value_without_tax').val();
        
        //Checkbox        
        if($("#components_compound_product").is(':checked')){
            array_values['components_compound_product'] = 1;
        } else {
            array_values['components_compound_product'] = 0;
        }
        
        if($("#components_show_order_subitens").is(':checked')){
            array_values['components_show_order_subitens'] = 1;
        } else {
            array_values['components_show_order_subitens'] = 0;
        }
        
        if($("#prediction_of_purchase").is(':checked')){
            array_values['prediction_of_purchase'] = 1;
        } else {
            array_values['prediction_of_purchase'] = "";
        }
        
        array_values['net_weight'] = $('#net_weight').val();
        array_values['additional_weight'] = $('#additional_weight').val();
        array_values['volume'] = $('#volume').val();
        array_values['lenght'] = $('#lenght').val();
        array_values['width'] = $('#width').val();
        array_values['height'] = $('#height').val();
        array_values['minimun_purchase_of_packages'] = $('#minimun_purchase_of_packages').val();
        array_values['minimum_purchase'] = $('#minimum_purchase').val();
        array_values['min_stock'] = $('#min_stock').val();
        array_values['max_stock'] = $('#max_stock').val();
        array_values['select_currency'] = $('#select_currency option:selected').val();
        array_values['purchase_price_exchange'] = $('#purchase_price_exchange').val();
        array_values['import_rate'] = $('#import_rate').val();
        array_values['freight_tax_amount'] = $('#freight_tax_amount').val();
        
        /* Orders */
        array_values['percent_order'] = $('#percent_order').val();
        
        if($("#the_sale_price_will").is(':checked')){
            array_values['the_sale_price_will'] = 1;
        } else {
            array_values['the_sale_price_will'] = "";
        }
        
        if($("#do_not_put_a_selling_price").is(':checked')){
            array_values['do_not_put_a_selling_price'] = 1;
        } else {
            array_values['do_not_put_a_selling_price'] = "";
        }
        
        if($("#do_not_allow_discount").is(':checked')){
            array_values['do_not_allow_discount'] = 1;
        } else {
            array_values['do_not_allow_discount'] = "";
        }
        
        if($("#product_only_to_buy").is(':checked')){
            array_values['product_only_to_buy'] = 1;
        } else {
            array_values['product_only_to_buy'] = "";
        }
        
        if($("#print_product_information_in_tax_coupon").is(':checked')){
            array_values['print_product_information_in_tax_coupon'] = 1;
        } else {
            array_values['print_product_information_in_tax_coupon'] = "";
        }
        
        if($("#fill_quantity_based_on_the_information").is(':checked')){
            array_values['fill_quantity_based_on_the_information'] = 1;
        } else {
            array_values['fill_quantity_based_on_the_information'] = "";
        }
        
        if($("#hide_from_statistics").is(':checked')){
            array_values['hide_from_statistics'] = 1;
        } else {
            array_values['hide_from_statistics'] = "";
        }
        
        if($("#display_alternate").is(':checked')){
            array_values['display_alternate'] = 1;
        } else {
            array_values['display_alternate'] = "";
        }
        
        if($("#the_purchase_price").is(':checked')){
            array_values['the_purchase_price'] = 1;
        } else {
            array_values['the_purchase_price'] = "";
        }
        
        if($("#view_product_description").is(':checked')){
            array_values['view_product_description'] = 1;
        } else {
            array_values['view_product_description'] = "";
        }
        
        if($("#do_not_show_product").is(':checked')){
            array_values['do_not_show_product'] = 1;
        } else {
            array_values['do_not_show_product'] = "";
        }
        
        if($("#hide_from_statistics").is(':checked')){
            array_values['hide_from_statistics'] = 1;
        } else {
            array_values['hide_from_statistics'] = "";
        }
        
        var jsonString = JSON.stringify(array_values);
        
        $.ajax({
            url : my_url+"Entry/Product/editProduct/",
            type: "POST",
            dataType: 'JSON',
            data : {data: jsonString},
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                    console.log(data);
               } else {
                    notification(false);
                }
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    };
</script>