<?php
    $productIncomplete = "";
    
    foreach ($array_answer['products'] as $value){
        if($value->getName() == '' || $value->getName() == null){
            $name = "Unnamed";
        } else {
            $name = $value->getName();
        }
        
        if($value->getActive() == 6){
            $productIncomplete .= "<p><input onclick='load(this.value)' type='radio' name='continueProd' value='".$value->getIdproduct()."'> ".$name." - ".$value->getDateCreate()->format('d/m/Y')."</p>";
        } else {
            $productIncomplete = "";
        }
    }
    
    $nav = "list_products";
    include 'nav.php';
?>

<script type="text/javascript">
    var table;
    var tableProductQuantity;
    $(document).ready(function() {
        table = $('#products').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllProducts/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "sku" },
                { "data": "name" },
                { "data": "salePrice" },
                { "data": "group" },
                { "data": "amount" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        tableProduct = $('#productTableIntern').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllProducts/"},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        })

        // tablePrinters = $('#printersTable').DataTable( {
        //     "ajax": {"url": my_url+"Entry/Product/getAllPrinters/"},
        //     "columns": [
        //         { "data": "checkbox" },
        //         { "data": "id" },
        //         { "data": "instaledIn" },
        //         { "data": "type" },
        //         { "data": "port" },
        //         { "data": "dateCreate" },
        //         { "data": "active" }
        //     ],
        //     "language": {
        //         "url": my_url+my_language+".json"
        //     }
        // });  

        tableProductQuantity = $('#aroundQuantityProductTable').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllProducts/"},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        }); 

        $(document).on('click', '#create', function(e){
            $('#form_create_product')[0].reset();
            $('#modal_create_product').modal({show: true});
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            if(id != null || id != undefined || id != '' || id != 'NaN'){
                get_product(id);
            }
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idProducts : ids};
            deleteItens('Entry/Product/deleteProducts', array_deletes, table);
        });

        $(document).on('click', '#markup', function(e){
            e.preventDefault();
            var id = table.$('tr.hover-select').find('input:checkbox').data('id');
            console.log(id);
            if(id != null || id != undefined || id != '' || id != 'NaN'){
                window.location = my_url+'Entry/Product/markup/'+id;
            }
        });

        $("#printersTable tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            var itemSelected = checkMark('#printersTable');
            if(itemSelected == 0){
                $('#print').addClass('disabled');
            }
            if(itemSelected == 1){
                $('#print').removeClass('disabled');
            }
            if(itemSelected > 1){
                $('#print').addClass('disabled');
            }
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $('#markup').attr('disabled', true);
        $("#products tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;

            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;

            }
            
            var itemSelected = checkMark('#products');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                $('#markup').attr('disabled', true);
                $('#printLabel').addClass('disabled');
                $('#roundQuantity').addClass('disabled');
            }
            if(itemSelected == 1){
                $('#markup').attr('disabled', false);
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
                $('#printLabel').removeClass('disabled');
                $('#roundQuantity').removeClass('disabled');
            }
            if(itemSelected > 1){
                $('#printLabel').addClass('disabled');
                $('#roundQuantity').addClass('disabled');
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
                $('#markup').attr('disabled', true);
            }
        });
    });
        
    function load(id){
        window.location = my_url+'Entry/Product/product/?idproduct='+id;
    }

    function openModalAroundQuantity(){
        tableProductQuantity.destroy();
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
        $('#modalAroundQuantity').modal({show: true});
        tableProductQuantity = $('#aroundQuantityProductTable').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getProductWithBarcodeAndrCode/"+idProduct},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "barCode" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  

        $.ajax({
            url: my_url + "Entry/Product/getProductAroundQuantity/"+idProduct,
            type: "POST",
            dataType: 'JSON',
            success: function (data, textStatus, jqXHR) {
                if (data.result == 'success') {
                    document.getElementById("stockQuantity").innerHTML = data.data;
                } else {
                    notification(false);
                    console.log(data.message);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
    }

    function openModalPrintLabels(){
        tableProduct.destroy();
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');

        $('#modalPrintLabel').modal({show: true});
        tableProduct = $('#productTableIntern').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getProductWithBarcodeAndrCode/"+idProduct},
            "columns": [
                { "data": "id" },
                { "data": "name" },
                { "data": "barCode" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  
        document.getElementById("qrcode").checked = true;
        qrCode();
    }

    function qrCode(){
        $.ajax({
                url: my_url + "Entry/Product/getProductWithBarcodeAndrCode/"+idProduct,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        if(data.data[0].barCode != 'no data'){ 
                            document.getElementById("imageDiv").style.display = "block";
                            document.getElementById("image_preview").style.display = "block";
                            document.getElementById("gerarCode").style.display = "none";
                            $('#image_preview').attr('src', data.data[0].qrCode);
                        }else{
                            document.getElementById("imageDiv").style.display = "none";
                            document.getElementById("image_preview").style.display = "none";
                            document.getElementById("gerarCode").style.display = "block";

                        }
                    } else {
                        notification(false);
                        console.log(data.message);
          }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }

    function barCode(){
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
        $.ajax({
                url: my_url + "Entry/Product/getBarCodeProduct/"+idProduct,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        if(data.data != 'no data'){ 
                            document.getElementById("imageDiv").style.display = "block";
                            document.getElementById("image_preview").style.display = "block";
                            document.getElementById("gerarCode").style.display = "none";
                            $('#image_preview').attr('src', data.data);
                        }else{
                            document.getElementById("imageDiv").style.display = "none";
                            document.getElementById("image_preview").style.display = "none";
                            document.getElementById("gerarCode").style.display = "block";
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
          }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }

    function generateCodeBar(){
        idProduct = table.$('tr.hover-select').find('input:checkbox').data('id');
        $.ajax({
                url: my_url + "Entry/Product/generateBarCode/"+idProduct,
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true); 
                        document.getElementById("qrcode").checked = true; 
                        qrCode();
                        tableProduct.destroy();
                        tableProduct = $('#productTableIntern').DataTable( {
                            "ajax": {"url": my_url+"Reports/Product/getProduct/"+idProduct},
                            "columns": [
                                { "data": "id" },
                                { "data": "name" },
                                { "data": "barCode" }
                            ],
                            "language": {
                                "url": my_url+my_language+".json"
                            }
                        }); 

                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }

    function orderProductByValue(){
        table.destroy();
        table = $('#products').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/orderProductByValue/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "purchasePrice" },
                { "data": "salePrice" },
                { "data": "group" },
                { "data": "numberPack" },
                { "data": "groupTax" },
                { "data": "update" },
                { "data": "status" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });  
    }
</script>
<style type="text/css">
    .pointer{
        cursor: pointer;
    }
    .disabled{
        pointer-events:none;
        opacity:0.4;
    } 
</style>
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t>Novo produto</t></button>
            <button id="update" class="btn btn-primary"><span class="lnr lnr-menu-circle"></span> <t>Editar produto</t></button>
            <button id="delete" class="btn btn-primary"><span class="lnr lnr-circle-minus"></span> <t>Deletar produto</t></button>
            <!-- <button id="markup" class="btn btn-primary"><span class="lnr lnr-circle-minus"></span> <t>Add Markup</t></button> -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="products" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('products')"></th>
                        <th style="width: 80px">ID</th>
                        <th>SKU</th>
                        <th>Nome</th>
                        <th>Preço</th>
                        <th>Categoria</th>
                        <th>Em estoque</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li ><a id="printLabel" class="pointer disabled translate" onclick="openModalPrintLabels()">Print Labels</a></li> 
                    <li><a id="o" class="pointer translate" onclick="orderProductByValue()">Balance In Stock</a></li> 
                    <li><a id="roundQuantity" class="pointer disabled translate" onclick="openModalAroundQuantity()">Round Quantity</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
            <button class="btn btn-success translate">Save</button>
        </div>
    </div>
</div>

<!-- Modal Groups From Products -->
<div class="modal fade" id="continue_registration" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Incomplete product registration</h4>
            </div>
            <div class="modal-body">
                <form id="form_continue_registration">
                    <div class="row">
                        <div class="col-md-12">
                            <p>The following products are incomplete, to complete the incomplete product registration, just click on the item.</p>
                            <p><?php echo $productIncomplete ?></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button onclick="$('#continue_registration').modal('hide')" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                <button onclick="window.location = my_url+'Entry/Product/product'" id="btn_continue_registration" class="btn btn-primary translate" data-dismiss="modal">Create new product</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade row row-fluid" id="modalPrintLabel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title  translate" id="myModalLabel">Print Labels</h4>
            </div>
              
             <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4>Product Details</h4> 
                    <div style="float: right;"">
                        <input id="qrcode" type="radio" name="opcao" value="QrCode" checked onclick="qrCode()"> <t>QrCode</t>
                        <input type="radio" name="opcao" value="BarCode" onclick="barCode()"> <t>BarCode</t>
                        <div id='imageDiv' class="inputFileImg" style="border: 1px solid #000">
                           <img  width="130" id="image_preview" src="../../../assets/images/upload.png" alt="your image"/>
                        </div>
                        <div id='gerarCode' style="text-align: center; display: none;">
                            <p style="color: red">without barcode</p>
                            <button id="print" class="btn btn-warning" onclick="generateCodeBar()"><t>Generate</t></button> 
                        </div>
                    </div>
                        <table id="productTableIntern" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>barCode</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                </div>
                <div class="modal-header">
                <div class="row">
                    <div class="col-md-12"> 
                    <h4>Printers</h4>  
                        <table id="printersTable" class="table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('printersTable')"></th>
                                <th>ID</th>
                                <th>Instaled In</th>
                                <th>Type</th>
                                <th>Port</th>
                                <th>Date Created</th>
                                <th>active</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="pull-right">
                    <button id="print" class="btn btn-success disabled translate">Print</button>
                </div>
                </div>



        </div>
    </div>
</div>

<!-- Create Product -->
<div class="modal fade" id="modal_create_product" tabindex="-1" role="dialog" aria-labelledby="modal_create_product_Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_create_product_Label">Novo produto</h4>
            </div>
            <form id="form_create_product">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="sku" class="label-style">SKU</label>
                            <input type="text" class="form-control" name="sku">
                        </div>
                        <div class="col-md-12">
                            <label for="name" class="label-style">Nome do produto</label>
                            <input type="text" class="form-control" name="name" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="description" class="label-style">Descrição</label>
                            <input type="text" class="form-control" name="description" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="productGp" class="label-style">Categoria</label>
                            <select name="productGp">
                                <option value="">Selecione uma opção</option>
                                <?php
                                    if($view['productGp']){
                                        $options = "";
                                        foreach ($view['productGp'] as $value){
                                            $options .=  '<option value="'.$value->getIdproductGp().'">'.$value->getName().'</option>';
                                        }
                                        echo $options;
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="supplier" class="label-style">Fornecedor</label>
                            <select name="supplier">
                                <option value="">Selecione uma opção</option>
                                <?php
                                    if($view['suppliers']){
                                        $options = "";
                                        foreach ($view['suppliers'] as $value){
                                            $options .=  '<option value="'.$value->getIdsupplier().'">'.$value->getCompanycompany()->getName().'</option>';
                                        }
                                        echo $options;
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="manufacturer" class="label-style">Fabricante</label>
                            <select name="manufacturer">
                                <option value="">Selecione uma opção</option>
                                <?php
                                    if($view['manufacturers']){
                                        $options = "";
                                        foreach ($view['manufacturers'] as $value){
                                            $options .=  '<option value="'.$value->getIdmanufacturer().'">'.$value->getName().'</option>';
                                        }
                                        echo $options;
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="price" class="label-style">Preço de venda</label>
                            <input type="text" class="form-control money" name="price" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="total_amount" class="label-style">Total em estoque</label>
                            <input type="number" class="form-control" name="total_amount">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#form_create_product').submit(function(e){
        e.preventDefault();
        $.post( my_url+"Entry/Product/create", $( "#form_create_product" ).serialize())
        .done(function( data ) {
            var data = JSON.parse(data);
            if(data.status == 200){
                var notice = new PNotify({
                    title: 'Produto cadastrado',
                    text: 'Item cadastrado com sucesso',
                    icon: 'glyphicon glyphicon-plus',
                    type: 'success',
                    confirm: {
                        confirm: false
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                PNotify.prototype.options.styling = "bootstrap3";
                notice.get().click(function() {
                    notice.remove();
                });
                $('#modal_create_product').modal('hide');
                reload_table(table);
            } else {
                var notice = new PNotify({
                    title: 'Ops..',
                    text: 'Ocorreu um erro ao tentar adicionar o produto',
                    icon: 'glyphicon glyphicon-minus',
                    type: 'error',
                    confirm: {
                        confirm: false
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                PNotify.prototype.options.styling = "bootstrap3";
                notice.get().click(function() {
                    notice.remove();
                });
            }
            return;
        })
        .fail(function() {
            var notice = new PNotify({
                title: 'Ops..',
                text: 'Ocorreu um erro ao tentar adicionar o produto',
                icon: 'glyphicon glyphicon-minus',
                type: 'error',
                confirm: {
                    confirm: false
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            PNotify.prototype.options.styling = "bootstrap3";
            notice.get().click(function() {
                notice.remove();
            });
        });
    })
</script>

<!-- Update Product -->
<div class="modal fade" id="modal_update_product" tabindex="-1" role="dialog" aria-labelledby="modal_update_product_Label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_update_product_Label">Editar produto</h4>
            </div>
            <form id="form_update_product">
                <input type="hidden" name="id" id="edit_product_id" required="true">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="edit_product_sku" class="label-style">SKU</label>
                            <input type="text" class="form-control" name="sku" id="edit_product_sku">
                        </div>
                        <div class="col-md-12">
                            <label for="edit_product_name" class="label-style">Nome do produto</label>
                            <input type="text" class="form-control" name="name" id="edit_product_name" required="true">
                        </div>
                        <div class="col-md-12">
                            <label for="edit_product_description" class="label-style">Descrição</label>
                            <input type="text" class="form-control" name="description" id="edit_product_description" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="edit_product_productGp" class="label-style">Categoria</label>
                            <select name="productGp" id="edit_product_productGp">
                                <option value="">Selecione uma opção</option>
                                <?php
                                    if($view['productGp']){
                                        $options = "";
                                        foreach ($view['productGp'] as $value){
                                            $options .=  '<option value="'.$value->getIdproductGp().'">'.$value->getName().'</option>';
                                        }
                                        echo $options;
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="edit_product_supplier" class="label-style">Fornecedor</label>
                            <select name="supplier" id="edit_product_supplier">
                                <option value="">Selecione uma opção</option>
                                <?php
                                    if($view['suppliers']){
                                        $options = "";
                                        foreach ($view['suppliers'] as $value){
                                            $options .=  '<option value="'.$value->getIdsupplier().'">'.$value->getCompanycompany()->getName().'</option>';
                                        }
                                        echo $options;
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="edit_product_manufacturer" class="label-style">Fabricante</label>
                            <select name="manufacturer" id="edit_product_manufacturer">
                                <option value="">Selecione uma opção</option>
                                <?php
                                    if($view['manufacturers']){
                                        $options = "";
                                        foreach ($view['manufacturers'] as $value){
                                            $options .=  '<option value="'.$value->getIdmanufacturer().'">'.$value->getName().'</option>';
                                        }
                                        echo $options;
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="edit_product_price" class="label-style">Preço de venda</label>
                            <input type="text" class="form-control money" name="price" id="edit_product_price" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <button type="reset" onclick="window.location = my_url+'Entry/Product/product/'+$('#modal_update_product #edit_product_id').val();" class="btn btn-primary" data-dismiss="modal">Editar mais detalhes</button>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    function get_product(id){
        var jqxhr = $.get( my_url+"Entry/Product/get/"+id )
        .done(function(data) {
            var data = JSON.parse(data);
            if(data.status == 200 && data.item){
                $('#form_update_product')[0].reset();
                $('#modal_update_product #edit_product_id').val(data.item.id);
                $('#modal_update_product #edit_product_name').val(data.item.name);
                $('#modal_update_product #edit_product_sku').val(data.item.sku);
                $('#modal_update_product #edit_product_description').val(data.item.description);
                if(data.item.group){
                    $('#modal_update_product #edit_product_productGp').val(data.item.group);    
                }
                if(data.item.supplier){
                    $('#modal_update_product #edit_product_supplier').val(data.item.supplier);
                }
                if(data.item.manufacturer){
                    $('#modal_update_product #edit_product_manufacturer').val(data.item.manufacturer);
                }
                $('#modal_update_product #edit_product_price').val(data.item.price);
                $('#modal_update_product').modal({show: true});
            } else {
                var notice = new PNotify({
                    title: 'Ops..',
                    text: 'Ocorreu um erro ao tentar editar o produto',
                    icon: 'glyphicon glyphicon-minus',
                    type: 'error',
                    confirm: {
                        confirm: false
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                PNotify.prototype.options.styling = "bootstrap3";
                notice.get().click(function() {
                    notice.remove();
                });
            }
        })
        .fail(function() {
            var notice = new PNotify({
                title: 'Ops..',
                text: 'Ocorreu um erro ao tentar editar o produto',
                icon: 'glyphicon glyphicon-minus',
                type: 'error',
                confirm: {
                    confirm: false
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            PNotify.prototype.options.styling = "bootstrap3";
            notice.get().click(function() {
                notice.remove();
            });
        });
    }

    $('#form_update_product').submit(function(e){
        e.preventDefault();
        $.post( my_url+"Entry/Product/update", $( "#form_update_product" ).serialize())
        .done(function( data ) {
            var data = JSON.parse(data);
            if(data.status == 200){
                var notice = new PNotify({
                    title: 'Produto editado',
                    text: 'Item editado com sucesso',
                    icon: 'glyphicon glyphicon-plus',
                    type: 'success',
                    confirm: {
                        confirm: false
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                PNotify.prototype.options.styling = "bootstrap3";
                notice.get().click(function() {
                    notice.remove();
                });
                $('#modal_update_product').modal('hide');
                reload_table(table);
            } else {
                var notice = new PNotify({
                    title: 'Ops..',
                    text: 'Ocorreu um erro ao tentar editar o produto',
                    icon: 'glyphicon glyphicon-minus',
                    type: 'error',
                    confirm: {
                        confirm: false
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                PNotify.prototype.options.styling = "bootstrap3";
                notice.get().click(function() {
                    notice.remove();
                });
            }
            return;
        })
        .fail(function() {
            var notice = new PNotify({
                title: 'Ops..',
                text: 'Ocorreu um erro ao tentar adicionar o produto',
                icon: 'glyphicon glyphicon-minus',
                type: 'error',
                confirm: {
                    confirm: false
                },
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            PNotify.prototype.options.styling = "bootstrap3";
            notice.get().click(function() {
                notice.remove();
            });
        });
    })
</script>