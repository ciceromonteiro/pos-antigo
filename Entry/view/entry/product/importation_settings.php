<?php
    $nav = "importation_settings";
    include 'nav.php';
?>
<script type="text/javascript">
     $(document).ready(function () {


     var table = $('#importation').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllImportationSettings/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "file" },
                { "data": "download" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });        

      document.getElementById('formUpload').onsubmit = function() {
            return false;
        }

        $("#formUpload").submit(function(){
            var formData = new FormData($("form[name='formUpload']")[0]);
            document.getElementById("uploadDiv").style.display = "none";
             $("#loading").html("<img class='text-center' id='image_loading' src='../../../assets/images/default.gif' alt='your image'  width='50' value='1'>");
             $.ajax({
                url : my_url+"Entry/product/insertImportation_settings",
                type: "POST",
                dataType: 'JSON',
                data: formData,
                processData: false,
                contentType: false,
                   success: function(data, textStatus, jqXHR){             
                    if (data.result == 'success'){
                        notification(true);

                    } else {
                        notification(false);

                    }
                    $('#import_json').modal('hide');
                     document.getElementById("uploadDiv").style.display = "block";
                     $("#loading").html("");
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                    document.getElementById("uploadDiv").style.display = "block";
                     $("#loading").html("");
                }

            });
           
        });

    });

     function downloadFile(file){
        $.ajax({
                url : my_url+"Entry/product/downloadSettings/"+ file,
                type: "POST",
                dataType: 'JSON',
                   success: function(data, textStatus, jqXHR){             
                    if (data.result == 'success'){
                        window.location.href = '../../../data/information_settings/settings.zip'
                        notification(true);

                    } else {
                        notification(false);

                    }
                     $('#import_json').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        
    }


</script>


<div class="content">
    <div class="row row-fluid">
        <div class="col-md-12">
            <table id="importation" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">File</th>
                        <th class="translate">Download</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-right">
            <button class="btn btn-primary" onclick="$('#import_json').modal({show : true});"><t class="translate">Import</t></button>
        </div>
        
    </div>
</div>


<div class="modal fade" id="import_json" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-center" role="document">
        <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                         <div id="loading" class="col-md-12 text-center">
                             
                        </div>
                        <div id="uploadDiv">
                        <form method="Post" enctype="multipart/form-data" id="formUpload" name="formUpload">
                        <div class="col-md-12">
                            <div style="float: left;">
                                <input type="file" name="file" id="file">
                            </div>
                            <div style="float: right;">
                                <button id="upload" type = "submit" class = "btn btn-success translate">Insert</button>

                            </div>
                        </div>
                        </form>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

