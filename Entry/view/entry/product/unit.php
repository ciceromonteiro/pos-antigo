<?php 
    $select = "<option value='0'>Select Option</option>";
    $select_units = "";
    
    foreach ($array_answer['units'] as $value) {
        $select_units .= "<option value='".$value->getIdunit()."'>".$value->getName()."</option>";
    }
    
    $select_units = $select . $select_units;
?>
<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#unit').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllUnit/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "derivative" },
                { "data": "ratio" },
                { "data": "divisible" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });
        
        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_unit');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('submit', '#form_unit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertUnit",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_unit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_unit_edit');
            $.ajax({
                url : my_url+"Entry/Product/getUnit/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#unitEditModal').modal({show : true});
                        $('#form_unit_edit #idUnit').val(data.data[0].id);
                        $('#form_unit_edit #nameUnit').val(data.data[0].name);
                        $('#form_unit_edit #ratio').val(data.data[0].ratio);
                        $('#form_unit_edit #derivative').val(data.data[0].unit);
                        $('#form_unit_edit #divisible').prop("checked",data.data[0].divisible);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_unit_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateUnit",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_unit_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#unitEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        // Get for update
        /*$(document).on('click', '#update', function(e){
            e.preventDefault();
            var id      = table.$('tr.hover-select').find('input:checkbox').data('id');
            var request = $.ajax({
                url:          '<?php echo URL_BASE ?>Entry/Product/getUnit',
                cache:        false,
                data:         'id=' + id,
                dataType:     'json',
                contentType:  'application/json; charset=utf-8',
                type:         'get'
            });
            request.done(function(output){
                if (output.result == 'success'){
                    $('#form_unit_edit #idUnit').val(output.data[0].id);
                    $('#form_unit_edit #nameUnit').val(output.data[0].name);
                    $('#form_unit_edit #numberOfMedia').val(output.data[0].numberOfMedia);
                    $('#unitEditModal').modal({show : true});
                } else {
                    notification(false);
                }
            });
            request.fail(function(jqXHR, textStatus){
                notification(false);
                console.log(jqXHR.responseText);
            });
        });*/

        // Delete
       /* $(document).on('click', '#delete', function(e){
            e.preventDefault();
            confirmDelete(this);
        });*/

        //selecionar 0
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#unit tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#unit');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idUnit : ids};
            deleteItens('Entry/Product/deleteUnits', array_deletes, table);
        });
    });

        /* Delete Modal */
        /*function confirmDelete(btnDelete){        
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        

            var i=0, ids = [];

            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var arrayDeletes = {idUnits : ids};
            var title = "Confirmation Needed";
            var text  = "Are you sure you want to delete?";
            var icon = "glyphicon glyphicon-minus";

            deleteItems(title, text, icon, arrayDeletes);
        }*/
        
        // Delete action
        /*function deleteItems(title, text, icon, arrayDeletes){
            PNotify.prototype.options.styling = "bootstrap3";
            (new PNotify({
                title: title,
                text: text,
                icon: icon,
                hide: false,
                confirm: {
                    confirm: true
                },
                buttons: {
                    closer: false,
                    sticker: false
                },
                history: {
                    history: false
                },
                addclass: 'stack-modal',
                stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
            })).get().on('pnotify.confirm', function(){
                var request = $.ajax({
                    url:          '<?php echo URL_BASE ?>Entry/Product/deleteUnits',
                    cache:        false,
                    data:         arrayDeletes,
                    dataType:     'json',
                    contentType:  'application/json; charset=utf-8',
                    type:         'get'
                });
                request.done(function(output){
                    if (output.result == 'success'){
                        var title = "Deleted successfully";
                        var text = "Deleted successfully";
                        var icon = "glyphicon glyphicon-plus";
                        var type = "success";
                        $('#unitEditModal').modal('hide');
                        reload_table();
                        notification(title, text, icon, type);
                    } else {
                        var title = "Delete request failed";
                        var text = "Delete request failed";
                        var icon = "glyphicon glyphicon-minus";
                        var type = "error";
                        $('#unitEditModal').modal('hide');
                        reload_table();
                        notification(title, text, icon, type);
                    }
                    $('.ui-pnotify-modal-overlay').remove();
                });
                request.fail(function(jqXHR, textStatus){
                    var title = "Delete request failed";
                    var text = textStatus;
                    var icon = "glyphicon glyphicon-minus";
                    var type = "error";
                    notification(title, text, icon, type);
                    console.log(jqXHR.responseText);
                });
            }).on('pnotify.cancel', function(){
                $('.ui-pnotify-modal-overlay').remove();
            });
        }*/
</script>

<?php
    $nav = "unit";
    include 'nav.php';
?>

<!-- Table List Units -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="unit" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                        <th style="width: 80px" class="translate">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Derivative</th>
                        <th class="translate">Ratio</th>
                        <th class="translate">Divisible</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Unit</h4>
            </div>
            <form id="form_unit" name="form_unit" class="form_unit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="nameUnit" class="label-style translate">Name</label>
                            <input type="text" class="form-control" id="nameUnit" name="nameUnit" value="" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="derivative" class="label-style translate">Derivative</label>
                            <select name="derivative">
                                <?php echo $select_units ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="ratio" class="label-style translate">Ratio</label>
                            <input type="text" class="form-control number" id="ratio" name="ratio" required="true">
                            <p><input type="checkbox" id="divisible" name="divisible"> <t class="translate">Divisible</t></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="unitEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Unit</h4>
            </div>
            <form id="form_unit_edit" name="form_unit_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="nameUnit" class="label-style translate">Name</label>
                            <input type="text" class="form-control" id="nameUnit" name="nameUnit" value="" required="true">
                            <input type="number" class="form-control number" style="display: none;" id="idUnit" name="idUnit">
                        </div>
                        <div class="col-md-6">
                            <label for="derivative" class="label-style translate">Derivative</label>
                            <select name="derivative" id="derivative">
                                <?php echo $select_units ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="ratio" class="label-style translate">Ratio</label>
                            <input type="text" class="form-control number" id="ratio" name="ratio" required="true">
                            <p><input type="checkbox" id="divisible" name="divisible"> <t class="translate">Divisible</t></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>