<ul class="navbar-three">
    <li <?php echo ($nav == "list_products" || $nav == 'products') ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/index" ?>" class="translate">Produtos</a>
    </li>
    <li <?php echo ($nav == "group_products") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/group_products" ?>" class="translate">Categorias</a>
    </li>
    <li <?php echo ($nav == "unit") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/unit" ?>" class="translate">Unidades</a>
    </li>
    <li <?php echo ($nav == "stock_changes") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/stock" ?>" class="translate">Ajuste de estoque</a>
    </li>
    <!-- <li <?php// echo ($nav == "rules") ? 'class="active"' : '' ?>>
        <a href="<?php// echo URL_BASE."Entry/Product/rules" ?>" class="translate">Rules</a>
    </li> -->
    <li <?php echo ($nav == "manufacturers") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/manufacturers" ?>" class="translate">Fabricantes</a>
    </li>
    <!-- <li <?php// echo ($nav == "importation_settings") ? 'class="active"' : '' ?>>
        <a href="<?php// echo URL_BASE."Entry/Product/importation_settings" ?>" class="translate">Importation Settings</a>
    </li> -->
    <li <?php echo ($nav == "series") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/Product/series" ?>" class="translate">Series</a>
    </li>
</ul>