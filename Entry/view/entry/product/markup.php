<?php 
    $select = "<option value='0'>Select Option</option>";
    $select_group_from_tributes = "";
    $select_printers = "";
    $select_products = "";
    $productIncomplete = "";
    
    foreach ($array_answer['tributes'] as $value){
        $select_group_from_tributes .= "<option value='".$value->getIdTribute()."'>".$value->getName()."</option>";
    } 
    
    foreach ($array_answer['printers'] as $value){
        $select_printers .= "<option value='".$value->getIdprinter()."'>".$value->getDescription()."</option>";
    }
    
    foreach ($array_answer['products'] as $value){
        if($value->getName() == '' || $value->getName() == null){
            $name = "Unnamed";
        } else {
            $name = $value->getName();
        }
        
        if($value->getActive() == 1){
            $select_products .= "<option value='".$value->getIdproduct()."'>".$name."</option>";
        }
    }
    
    $product = $array_answer['product'];
    $idproduct = $array_answer['idproduct'];
    
    $nav = "products";
    include 'nav.php';
?>

<style type="text/css">
    .row-fluid {
        padding-left: 25px;
        padding-right: 25px;
    }
    .no-padding {
        padding: 0px;
    }
    .no-padding-left {
        padding-left: 0px;
    }
    .divider {
        margin-bottom: 15px;
        margin-top: 15px;
        margin-right: auto;
        margin-left: auto;
        width: 95%;
        height: 2px;
        background-color: #EDECEC;
    }
    .navbar-three li {
        padding-bottom: 0px; 
        margin-bottom: 0px
    }
</style>
<div class="content">
    <div class="row row-fluid">
        <h2>Produto</h2>
        <div class="col-md-2">
            <label class="translate">Preço de Compra:</label>
            <div class="inner-addon right-addon">
                <input value="<?php echo $product->pricePurchase ?>" class="form-control" type="text" name="preco_de_compra" id="preco_de_compra">
            </div>
        </div>
    </div>
    <div class="divider"></div>
    <div class="row row-fluid">
        <h2>Despesas Variáveis</h2>      
        <div class="col-md-4">
            <label class="translate">Frete:</label>
            <input type="text" value="" required="required" class="form-control" name="frete" id="frete">
        </div>
        <div class="col-md-4">
            <label class="translate">Substituição (icms) %:</label>
            <input type="text" value="" required="required" class="form-control" name="icms" id="icms">
        </div>
        <div class="col-md-4">
            <label class="translate">Fecop %:</label>
            <input type="text" value="" required="required" class="form-control" name="fecop" id="fecop">
        </div>
        <div class="col-md-4">
            <label class="translate">Icms simples %:</label>
            <input type="text" value="" required="required" class="form-control" name="icms_simples" id="icms_simples">
        </div>
         <div class="col-md-4">
            <label class="translate">Custo operacional %:</label>
            <input type="text" value="" required="required" class="form-control" name="custo_operacional" id="custo_operacional">
        </div>
         <div class="col-md-4">
            <label class="translate">Comissão %:</label>
            <input type="text" value="" required="required" class="form-control" name="comissao" id="comissao">
        </div>
    </div>
     <div class="divider"></div>
    <div class="row row-fluid">
        <h2>Lucro</h2>
        <div class="col-md-4">
            <label class="translate">Lucro Pretendido %:</label>
            <input type="text" value="" required="required" class="form-control" name="LP" id="LP">
        </div>
        <div class="col-md-4" id="markup_show">
            <label class="translate">Preço Sugerido:</label>
            <input disabled="true" class="form-control" id="preco_sugerido"></input>
        </div>
    </div>
    
    <div class="row row-fluid pull-right">
        <div class="col-md-12">
            <br>
            <button type="submit" class="btn btn-success translate" onclick="calcMarkup()">Calcular</button>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("#markup_show").hide();
    function calcMarkup() {
        // lucro pretendido
        var LP = parseFloat($("#LP").val());
        // desepeasas fixas 
        var DF = 0;

        // calculando despesasa variaveis
        var preco_de_compra = parseFloat($("#preco_de_compra").val());
        var frete = parseFloat($("#frete").val());
        var frete = 100*(frete/preco_de_compra);
        // console.log('frete ', frete);
        // calculando icms
        var icms = parseFloat($("#icms").val());
        // console.log('icms ', icms);
        // calculando fecop
        var fecop = parseFloat($("#fecop").val());
         // console.log('fecop ', fecop);
        // calculando icms simples
        var icms_simples = parseFloat($("#icms_simples").val());
        // console.log('icms_simples ', icms_simples);
        // calculando custo operacional
        var custo_operacional = parseFloat($("#custo_operacional").val());
        // console.log('custo_operacional', custo_operacional);
        // calculando comissao
        var comissao = parseFloat($("#comissao").val());
        // console.log('comissao', comissao);
        // despesas variaveis  
        var DV = icms + fecop + icms_simples + custo_operacional + comissao + frete;

        // operador do markup
        // console.log('DV ',DV);
        // console.log('DF ',DF);
        // console.log('LP ',LP);
        var op = (DV+DF+LP);
        // console.log('op ',op);

        var markup = 100/(100-op);
        //preço sugerido
        var PS = preco_de_compra * markup;
        PS = PS.toFixed(2);
        // console.log('ps', PS);
        // console.log(isNaN(PS));

        if (!preco_de_compra) {
            swal('Opes...','Por favor, registre um preço de compara para seu produto!','warning');
            return false;
        }

        if(!isNaN(PS)){
            $("#markup_show").slideDown('slow');
            $("#preco_sugerido").val(PS);
            $("#preco_sugerido").focus();
        }
        else {
            swal('Opes...','Por favor confira se os campos foram devidamente preenchidos!','warning');
        }
    }
    
//     var idProduct = "<?php //echo $idproduct ?>";
    
//     //window.location.hash = idProduct;
    
//     load(idProduct);
        
//     function load(id){
//         $('#continue_registration').modal('hide');
//         $('#id_product').val(id);
//         loadFieldsAndTables(id);
//     }
    
//     function loadFieldsAndTables(id){
//         loadFields(id);
//         loadPromocional(id);
//         loadComponent(id);
// //        loadAdditional(id);
//         loadGroupsFromProducts(id);
//         loadImageFromProducts(id);
//         loadWholesalePrice(id);
//         //loadColorSizes(id);
//         //loadOrdersProduct(id);
//     }
    
//     function loadFields(id){
//         $.ajax({
//             url : my_url+"Entry/Product/getProduct/"+id,
//             type: "POST",
//             dataType: 'JSON',
//             data : '',
//             success: function(data, textStatus, jqXHR){
//                 if (data.result == 'success'){
//                     var values = data.data[0];
//                     for (var prop in values) {
//                         if (prop == "components_compound_product" || prop == "components_show_order_subitens" || prop == "prediction_of_purchase"){
//                             //checkboxes
//                             if(values[prop] == "on" || values[prop] == "1"){
//                                 document.getElementById(prop).checked = true;
//                             } else {
//                                 document.getElementById(prop).checked = false;
//                             }
//                         } else if (prop == "type_product" || prop == "output_control" ||
//                         prop == "groups_from_tributes" || prop == "print_for_pick_list" ||
//                         prop == "number_of_serie_from_product" || prop == "select_manufacturer" ||
//                         prop == "select_supplier" || prop == "select_unit_from_product" ||
//                         prop == "select_base_of_calc" || prop == "status_of_product" ||
//                         prop == "select_department" || prop == "select_projects" ||
//                         prop == "place_where_the_product" || prop == "select_currency"){
//                             //selects
//                             if(values[prop] == "" || values[prop] == null || values[prop] == 'null'){
//                                 document.getElementById(prop).value = 0;
//                             } else{
//                                 document.getElementById(prop).value = values[prop];
//                             }
 
//                        } else {
//                             //inputs == ERRO ESTÁ AQUI
//                             document.getElementById(prop).value = values[prop];
//                         }
//                     }
//                 } else if(data.result == 'not-exist'){
//                     alert('Product does not exist, try again other id');
//                 } else {
//                     notification(false);
//                 }
//             },
//             error: function (jqXHR, textStatus, errorThrown){
//                 notification(false);
//                 console.log(jqXHR.responseText);
//             }
//         });
//     }

//     function setPriceProduct(price){
//         price = parseFloat(price);
//         $('#pricing_of_sell_value_with_tax').val(addMask(price));
//         $('#pricing_of_sell_value_without_tax').val(addMask(price));
//         $('#profit_gross_profit_value').val(addMask(price));
//         if($('#profit_margin').val() == ''){
//             $('#profit_margin').val('100,00%');
//         }    
//     }

//     function setMargin(value){/*
//         if($('#pricing_of_sell_value_with_tax').val() == ''){
//             $('#profit_gross_profit_value').val('');
//             $('#profit_margin').val('');
//             return;
//         }
//         if($('#pricing_of_buy').val() == ''){
//             var pricingOfBuy = 0;
//         } else {
//             var pricingOfBuy = parseFloat($('#pricing_of_buy').val());
//         }  

//         if($('#pricing_of_shipping').val() == ''){
//             var pricingOfShipping = 0;
//         } else {
//             var pricingOfShipping = parseFloat($('#pricing_of_shipping').val());
//         }

//         var pricingOfSellWithTax = parseFloat($('#pricing_of_sell_value_with_tax').val());
//         var pricingOfSellWithoutTax = parseFloat($('#pricing_of_sell_value_without_tax').val());

//         var grossPrice = (pricingOfSellWithTax - (pricingOfBuy + pricingOfShipping));
//         var margin = (grossPrice/pricingOfSellWithTax) * 100;

//         $('#profit_gross_profit_value').val(addMask(grossPrice));
//         $('#profit_margin').val(margin+"%");
//     */}

//     function addMask(value){/*
//         var check = Number.isInteger(value);
//         if(check == true){
//             return value+",00";
//         } else {
//             return value;
//         }
//     */}
    
//     function saveProductTemp(type){
//         var array_values = {};
//         //Input and Select
//         array_values['id_product'] = $('#id_product').val();
//         array_values['name_product'] = $('#name_product').val();
//         array_values['type_product'] = $('#type_product option:selected').val();
//         array_values['output_control'] = $('#output_control option:selected').val();
//         array_values['pricing_of_sell_value_without_tax'] = $('#pricing_of_sell_value_without_tax').val();
//         array_values['pricing_of_buy'] = $('#pricing_of_buy').val();
//         array_values['pricing_of_shipping'] = $('#pricing_of_shipping').val();
//         array_values['profit_gross_profit_value'] = $('#profit_gross_profit_value').val();
//         array_values['groups_from_tributes'] = $('#groups_from_tributes option:selected').val();
//         array_values['print_for_pick_list'] = $('#print_for_pick_list option:selected').val();
//         array_values['price_of_table'] = $('#price_of_table').val();
//         array_values['recom_value_for_sell_liquid'] = $('#recom_value_for_sell_liquid').val();
//         array_values['recom_value_for_sell_brute'] = $('#recom_value_for_sell_brute').val();
//         array_values['recom_value_for_sell_liquid'] = $('#recom_value_for_sell_liquid').val();
//         array_values['trade_value_for_sell_brute'] = $('#trade_value_for_sell_brute').val();
//         array_values['trade_value_for_sell_liquid'] = $('#trade_value_for_sell_liquid').val();
//         array_values['value_for_travel_brute'] = $('#value_for_travel_brute').val(); 
//         array_values['value_for_travel_liquid'] = $('#value_for_travel_liquid').val();
//         array_values['number_of_serie_from_product'] = $('#number_of_serie_from_product').val();
//         array_values['select_manufacturer'] = $('#select_manufacturer option:selected').val();
//         array_values['select_supplier'] = $('#select_supplier option:selected').val();
//         array_values['select_unit_from_product'] = $('#select_unit_from_product option:selected').val();
//         array_values['select_base_of_calc'] = $('#select_base_of_calc option:selected').val();
//         array_values['number_of_manufacture'] = $('#number_of_manufacture').val();
//         array_values['number_of_supplier'] = $('#number_of_supplier').val();
//         array_values['multiple_units'] = $('#multiple_units').val();
//         if(type == 'complete'){
//             array_values['status_of_product'] = 1;
//         } else { 
//            array_values['status_of_product'] = $('#status_of_product option:selected').val();
//         }
        
        
//         array_values['select_department'] = $('#select_department option:selected').val();
//         array_values['select_projects'] = $('#select_projects option:selected').val();
//         array_values['place_where_the_product'] = $('#place_where_the_product option:selected').val();
//         array_values['value_for_sell_liquid'] = $('#pricing_of_sell_value_without_tax').val();
        
//         //Checkbox        
//         if($("#components_compound_product").is(':checked')){
//             array_values['components_compound_product'] = 1;
//         } else {
//             array_values['components_compound_product'] = 0;
//         }
        
//         if($("#components_show_order_subitens").is(':checked')){
//             array_values['components_show_order_subitens'] = 1;
//         } else {
//             array_values['components_show_order_subitens'] = 0;
//         }
        
//         if($("#prediction_of_purchase").is(':checked')){
//             array_values['prediction_of_purchase'] = 1;
//         } else {
//             array_values['prediction_of_purchase'] = "";
//         }
        
//         array_values['net_weight'] = $('#net_weight').val();
//         array_values['additional_weight'] = $('#additional_weight').val();
//         array_values['volume'] = $('#volume').val();
//         array_values['lenght'] = $('#lenght').val();
//         array_values['width'] = $('#width').val();
//         array_values['height'] = $('#height').val();
//         array_values['minimun_purchase_of_packages'] = $('#minimun_purchase_of_packages').val();
//         array_values['minimum_purchase'] = $('#minimum_purchase').val();
//         array_values['min_stock'] = $('#min_stock').val();
//         array_values['max_stock'] = $('#max_stock').val();
//         array_values['select_currency'] = $('#select_currency option:selected').val();
//         array_values['purchase_price_exchange'] = $('#purchase_price_exchange').val();
//         array_values['import_rate'] = $('#import_rate').val();
//         array_values['freight_tax_amount'] = $('#freight_tax_amount').val();
        
//         /* Orders */
//         array_values['percent_order'] = $('#percent_order').val();
        
//         if($("#the_sale_price_will").is(':checked')){
//             array_values['the_sale_price_will'] = 1;
//         } else {
//             array_values['the_sale_price_will'] = "";
//         }
        
//         if($("#do_not_put_a_selling_price").is(':checked')){
//             array_values['do_not_put_a_selling_price'] = 1;
//         } else {
//             array_values['do_not_put_a_selling_price'] = "";
//         }
        
//         if($("#do_not_allow_discount").is(':checked')){
//             array_values['do_not_allow_discount'] = 1;
//         } else {
//             array_values['do_not_allow_discount'] = "";
//         }
        
//         if($("#product_only_to_buy").is(':checked')){
//             array_values['product_only_to_buy'] = 1;
//         } else {
//             array_values['product_only_to_buy'] = "";
//         }
        
//         if($("#print_product_information_in_tax_coupon").is(':checked')){
//             array_values['print_product_information_in_tax_coupon'] = 1;
//         } else {
//             array_values['print_product_information_in_tax_coupon'] = "";
//         }
        
//         if($("#fill_quantity_based_on_the_information").is(':checked')){
//             array_values['fill_quantity_based_on_the_information'] = 1;
//         } else {
//             array_values['fill_quantity_based_on_the_information'] = "";
//         }
        
//         if($("#hide_from_statistics").is(':checked')){
//             array_values['hide_from_statistics'] = 1;
//         } else {
//             array_values['hide_from_statistics'] = "";
//         }
        
//         if($("#display_alternate").is(':checked')){
//             array_values['display_alternate'] = 1;
//         } else {
//             array_values['display_alternate'] = "";
//         }
        
//         if($("#the_purchase_price").is(':checked')){
//             array_values['the_purchase_price'] = 1;
//         } else {
//             array_values['the_purchase_price'] = "";
//         }
        
//         if($("#view_product_description").is(':checked')){
//             array_values['view_product_description'] = 1;
//         } else {
//             array_values['view_product_description'] = "";
//         }
        
//         if($("#do_not_show_product").is(':checked')){
//             array_values['do_not_show_product'] = 1;
//         } else {
//             array_values['do_not_show_product'] = "";
//         }
        
//         if($("#hide_from_statistics").is(':checked')){
//             array_values['hide_from_statistics'] = 1;
//         } else {
//             array_values['hide_from_statistics'] = "";
//         }
        
//         var jsonString = JSON.stringify(array_values);
        
//         $.ajax({
//             url : my_url+"Entry/Product/saveProductTemp/",
//             type: "POST",
//             dataType: 'JSON',
//             data : {data: jsonString},
//             success: function(data, textStatus, jqXHR){
//                 if (data.result == 'success'){
//                     notification(true);
//                } else {
//                     notification(false);
//                 }
//             },
//             error: function (jqXHR, textStatus, errorThrown){
//                 notification(false);
//                 console.log(jqXHR.responseText);
//             }
//         });
//     };
</script>