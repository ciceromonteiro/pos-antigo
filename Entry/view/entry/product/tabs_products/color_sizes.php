<?php
$colors_checkbox = "";
$colors_select = "";

$sizes_checkbox = "";
$sizes_select = "";

foreach ($array_answer['colors'] as $colors) {
    $colors_checkbox .= "<p><input type='checkbox' name='color[]' value='" . $colors->getIdColor() . "'> " . $colors->getName() . "</p>";
    $colors_select .= "<option value='" . $colors->getIdColor() . "'>" . $colors->getName() . "</option>";
}

foreach ($array_answer['sizes'] as $sizes) {
    $sizes_checkbox .= "<p><input type='checkbox' name='size[]' value='" . $sizes->getIdSize() . "'> " . $sizes->getName() . "</p>";
    $sizes_select .= "<option value='" . $sizes->getIdSize() . "'>" . $sizes->getName() . "</option>";
}
?>

<!-- New color/sizes -->
<div class="modal fade" id="newSizeAndColor_modal" tabindex="-1" role="dialog" aria-labelledby="modalNewSizeAndColor">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title translate" id="modalNewSizeAndColor">Adicionar</h4>
            </div>
            <form id="form_create_size_color" name="form_size_color">
                <div class="modal-body">
                    <div class="row">
                        <form name="generate_color_and_sizes">
                            <div class="col-md-6">
                                <div id="colors">
                                    <label class="translate">Cor</label>
                                    <select name="color" id="color">
                                        <option value="0" class="translate">Escolha uma opção</option>
                                        <?php echo $colors_select; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="sizes">
                                    <label class="translate">Tamanho</label>
                                    <select name="size" id="size">
                                        <option value="0" class="translate">Escolha uma opção</option>
                                            <?php echo $sizes_select; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="sizes">
                                    <label class="translate">EAN</label>
                                    <input type="text" name="cod_ean" id="cod_ean" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="sizes">
                                    <label class="translate">Cód</label>
                                    <input type="text" name="cod_intern" id="cod_intern" class="form-control">
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary translate">Adicionar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update color/sizes -->
<div class="modal fade" id="SizeAndColor_modal_update" tabindex="-1" role="dialog" aria-labelledby="modalNewSizeAndColor">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title translate" id="modalEditSizeAndColor">Editar</h4>
            </div>
            <form id="form_size_color_edit" name="form_size_color">
                <div class="modal-body">
                    <div class="row">
                        <form name="generate_color_and_sizes">
                            <div class="col-md-6">
                            <input type="number" style="display: none" id="idColorSize" name="idColorSize" require="true">
                                <div id="colors">
                                    <label class="translate">Cor</label>
                                    <select name="color" id="color">
                                        <option value="0" class="translate">Escolha uma opção</option>
                                        <?php echo $colors_select; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="sizes">
                                    <label class="translate">Tamanho</label>
                                    <select name="size" id="size">
                                        <option value="0" class="translate">Escolha uma opção</option>
                                            <?php echo $sizes_select; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="sizes">
                                    <label class="translate">EAN</label>
                                    <input type="text" name="cod_ean" id="cod_ean" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div id="sizes">
                                    <label class="translate">Cód</label>
                                    <input type="text" name="cod_intern" id="cod_intern" class="form-control">
                                </div>
                            </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Editar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Generate color/sizes -->
<div class="modal fade" id="generate_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Gerar</h4>
            </div>
            <form id="form_generete_color_size" name="form_generete_color_size">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="colors">
                                <h4 class="translate">Cores</h4>
                                <?php echo $colors_checkbox; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="sizes">
                                <h4 class="translate">Tamanho</h4>
                                <?php echo $sizes_checkbox; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary translate">Gerar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    //function loadColorSizes(id){
    $(document).ready(function() {
        $(document).on('click', '#generate_color_and_sizes', function (e) {
            e.preventDefault();
            $('#generate_modal').modal({show: true});
        });

        $(document).on('click', '#create_sizecolor', function (e) {
            e.preventDefault();
            $('#newSizeAndColor_modal').modal({show: true});
        });

        var tableColorSizes = $('#color_sizes_product').DataTable({
            "ajax": {"url": my_url + "Entry/Product/getAllColorsSize/" + idProduct},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "color"},
                {"data": "size"},
                {"data": "codEan"},
                {"data": "qtdStock"},
                {"data": "cod"}
            ],
            "retrieve": true,
            "paging": false,
            "language": {
                "url": my_url + my_language + ".json"
            }
        });

        $(document).on('submit', '#form_create_size_color', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Entry/Product/insertColorsSize",
                type: "POST",
                dataType: 'JSON',
                data: 'idProduct='+idProduct+'&'+$('#form_create_size_color').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#newSizeAndColor_modal').modal('hide');
                    reload_table(tableColorSizes);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#update_sizecolor', function(e){
            e.preventDefault();
            resetForm('#form_size_color_edit');
            var id = tableColorSizes.$('tr.hover-select').find('input:checkbox').data('id');
            //alert(id);
            $.ajax({
                url : my_url+"Entry/Product/getColorSize/"+id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#SizeAndColor_modal_update').modal({show : true});
                        $('#form_size_color_edit #idColorSize').val(data.data[0].id);
                        $('#form_size_color_edit #color').val(data.data[0].color);
                        $('#form_size_color_edit #size').val(data.data[0].size);
                        $('#form_size_color_edit #cod_ean').val(data.data[0].codEan);
                        $('#form_size_color_edit #cod_intern').val(data.data[0].cod);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_size_color_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateColorsSize",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_size_color_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#SizeAndColor_modal_update').modal('hide');
                    reload_table(tableColorSizes);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

         $(document).on('click', '#delete_sizecolor', function(e){
            e.preventDefault();
            var trs = tableColorSizes.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idColors : ids};
            deleteItens('Entry/Product/deleteColorsSize', array_deletes, tableColorSizes);
        });



        $(document).on('submit', '#form_generete_color_size', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Entry/Product/genereteColorsSize",
                type: "POST",
                dataType: 'JSON',
                data: 'idProduct=' + idProduct + '&' + $('#form_generete_color_size').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#generate_modal').modal('hide');
                    reload_table(tableColorSizes);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $('#update_sizecolor').attr('disabled', true);
        $('#delete_sizecolor').attr('disabled', true);
        $("#color_sizes_product tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#color_sizes_product');
            if(itemSelected == 0){
                $('#update_sizecolor').attr('disabled', true);
                $('#delete_sizecolor').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_sizecolor').attr('disabled', false);
                $('#delete_sizecolor').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_sizecolor').attr('disabled', true);
                $('#delete_sizecolor').attr('disabled', false);
            }
        });
    });
</script>