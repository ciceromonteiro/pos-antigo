<?php
    $select = "<option value='0'>Escolha uma opção</option>";
    $select_option_group = "";
    $select_option_manufacturer = "";
    $select_option_supplier = "";
    $select_option_unit = "";
    $select_option_departments = "";
    $select_option_projects = "";
    $select_stocks = "";
    $select_status_of_product = "";
    $status_of_product = [
        1 => 'Ativo',
        2 => 'Passivo',
        3 => 'Expirado',
        4 => 'Bloqueado',
        5 => 'Deleted',
        6 => 'Temporário'
    ];
    
    foreach ($array_answer['productGp'] as $value) {
        if(count($array_answer['product_in_productGp']) > 0 &&
            $array_answer['product_in_productGp'][0]->getProductGpproductGp()->getIdproductGp() == $value->getIdproductGp()){
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $select_option_group .= "<option ".$selected." value='" . $value->getIdproductGp() . "'>" . $value->getName() . "</option>";
    }
    
    foreach ($array_answer['manufacturers'] as $value) {
        if($array_answer['product'][0]->getManufacturermanufacturer() != NULL && 
            $array_answer['product'][0]->getManufacturermanufacturer()->getIdmanufacturer() == $value->getIdmanufacturer()){
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $select_option_manufacturer .= "<option ".$selected." value='" . $value->getIdmanufacturer() . "'>" . $value->getName() . "</option>";
    }

    foreach ($array_answer['suppliers'] as $value) {
        if($array_answer['product'][0]->getSuppliersupplier() != NULL && 
            $array_answer['product'][0]->getSuppliersupplier()->getIdsupplier() == $value->getIdsupplier()){
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $select_option_supplier .= "<option ".$selected." value='" . $value->getIdsupplier() . "'>" . $value->getCompanycompany()->getName() . "</option>";
    }

    foreach ($array_answer['units'] as $value) {
        if($array_answer['product'][0]->getUnitunit() != NULL && 
            $array_answer['product'][0]->getUnitunit()->getIdunit() == $value->getIdunit()){
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $select_option_unit .= "<option ".$selected." value='" . $value->getIdunit() . "'>" . $value->getName() . "</option>";
    }

    foreach ($array_answer['departments'] as $value) {
        if($array_answer['product'][0]->getProductDepartmentproductDepartment() != NULL && 
            $array_answer['product'][0]->getProductDepartmentproductDepartment()->getIddepartment() == $value->getIddepartment()){
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $select_option_departments .= "<option ".$selected." value='" . $value->getIddepartment() . "'>" . $value->getName() . "</option>";
    }
    
    foreach ($array_answer['projects'] as $value) {
        if($array_answer['product'][0]->getProject() != NULL && 
            $array_answer['product'][0]->getProject()->getIdproject() == $value->getIdproject()){
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $select_option_projects .= "<option ".$selected." value='" . $value->getIdproject() . "'>" . $value->getName() . "</option>";
    }
    
    foreach ($array_answer['stocks'] as $value){
        if($array_answer['product'][0]->getWarrantywarranty() != NULL && 
            $array_answer['product'][0]->getWarrantywarranty()->getIdwarehouse() == $value->getIdwarehouse()){
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $select_stocks .= "<option ".$selected." value='".$value->getIdwarehouse()."'>".$value->getName()."</option>";
    }
    foreach ($status_of_product as $key => $value) {
        if($array_answer['product'][0]->getActive() != NULL && 
            $array_answer['product'][0]->getActive() == $key){
            $selected = 'selected';
        } else {
            $selected = '';
        }
        $select_status_of_product .= "<option ".$selected." value='".$key."'>".$value."</option>";
    }
    
    $select_option_manufacturer = $select . $select_option_manufacturer;
    $select_option_group = $select . $select_option_group;
    $select_option_supplier = $select . $select_option_supplier;
    $select_option_unit = $select . $select_option_unit;
    $select_option_departments = $select . $select_option_departments;
    $select_option_projects = $select . $select_option_projects;
    $select_status_of_product = $select . $select_status_of_product;
?>

<div id="extra" class="tab-pane active" style="margin-bottom: 30px">
    <div class="row row-fluid">
        <!-- <div class="col-md-3">
            <label>Number of serie from product</label> 
            <select name="number_of_serie_from_product" id="number_of_serie_from_product">
                <option value="0">Not in use</option>
                <option value="">When sold</option>
                <option value="">When sold and bought</option>
                <option value="">Match when sold</option>
                <option value="">Match when returned</option>
            </select>
            <select class="form-control form-control-select" disabled="true" style="margin-top: 10px;">
                <option value="">Select option</option>
            </select>
        </div> -->

        <div class="col-md-3">
            <label>Categoria</label> 
            <select name="productGp" id="select_productGp">
                <?php echo $select_option_group ?>
            </select>
        </div>

        <div class="col-md-3">
            <label>Fabricante</label> 
            <select name="manufacturer" id="select_manufacturer">
                <?php echo $select_option_manufacturer ?>
            </select>
        </div>

        <div class="col-md-3">
            <label>Fornecedor</label> 
            <select name="supplier" id="select_supplier">
                <?php echo $select_option_supplier ?>
            </select>
        </div>

        <div class="col-md-3">
            <label>Unidade do produto</label> 
            <select name="unit" id="select_unit_from_product">
                <?php echo $select_option_unit ?>
            </select>
        </div>

        <div class="col-md-3">
            <label>Departamento</label> 
            <select name="department" id="select_department">
                <?php echo $select_option_departments ?>
            </select>
        </div>

        <div class="col-md-3">
            <label>Projeto</label> 
            <select name="project" id="select_projects">
                <?php echo $select_option_projects ?>
            </select>
        </div>

        <!-- <div class="col-md-3">
            <label>Estoque</label> 
            <select name="warrant" id="place_where_the_product">
                <option value="0">None</option>
                <?php// echo $select_stocks ?>
            </select>
        </div> -->

        <div class="col-md-3">
            <label>Status</label> 
            <select name="status" id="status_of_product">
                <?php echo $select_status_of_product ?>
            </select>
        </div>
    </div>

    <!-- <div class="row row-fluid">
        <div class="col-md-3">
            <label>Base of calc</label> 
            <select name="select_base_of_calc" id="select_base_of_calc">
                <option value="0">Select option</option>
                <option value="1">Sales Amount</option>
                <option value="2">Contribution</option>
                <option value="3">Cost</option>
            </select>
        </div>

        <div class="col-md-3">
            <label>Number of manufacture</label> 
            <input type="text" class="form-control" name="number_of_manufacture" id="number_of_manufacture" value="">
        </div>

        <div class="col-md-3">
            <label>Number of supplier</label> 
            <input type="text" class="form-control" name="number_of_supplier" id="number_of_supplier" value="">
        </div>

        <div class="col-md-3">
            <label>Multiple units</label> 
            <input type="text" class="form-control" name="multiple_units" id="multiple_units" value="">
        </div>
    </div> -->
</div>
