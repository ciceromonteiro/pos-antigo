<script type="text/javascript">
    function setRecommendedPrice(value) {
        if (value != '') {
            $('#recom_value_for_sell_brute').val(value);
            $('#recom_value_for_sell_liquid').val(value);
        }
    }

    function setTradedValue(value) {
        if (value != '') {
            $('#trade_value_for_sell_brute').val(value);
            $('#trade_value_for_sell_liquid').val(value);
            setMarginTraded(value);
        }
    }

    function setTakeawayPrice(value) {
        if (value != '') {
            $('#value_for_travel_brute').val(value);
            $('#value_for_travel_liquid').val(value);
        }
    }
    
    function setMarginTraded(value){
        if($('#trade_value_for_sell_brute').val() == ''){
            $('#profit_gross_profit_value').val('');
            $('#profit_margin').val('');
            return;
        }
        if($('#pricing_of_buy').val() == ''){
            var pricingOfBuy = 0;
        } else {
            var pricingOfBuy = parseFloat($('#pricing_of_buy').val());
        }  
        
        if($('#pricing_of_shipping').val() == ''){
            var pricingOfShipping = 0;
        } else {
            var pricingOfShipping = parseFloat($('#pricing_of_shipping').val());
        }
        
        var tradedOfSellWithTax = parseFloat($('#trade_value_for_sell_brute').val());
        var tradedOfSellWithoutTax = parseFloat($('#trade_value_for_sell_liquid').val());
        
        var grossPrice = (tradedOfSellWithTax - (pricingOfBuy + pricingOfShipping));
        var margin = (grossPrice/tradedOfSellWithTax) * 100;
        
        $('#div_total_final_traded').val(margin+"%");
    }
</script>

<style>
    #price .dataTables_filter {
        display: none;
    }
</style>

<div id="price" class="tab-pane active">
    <div class="row row-fluid">
        <div class="col-md-2">
            <h4 class="no-padding translate">Price</h4>
            <label class="translate">Price of table</label>
            <input type="text" class="form-control money" name="price_of_table" id="price_of_table">
        </div>
        <div class="col-md-5">
            <h4 class="no-padding translate">Recommended Price</h4>
            <div class="col-md-6 no-padding-left"> 
               <label class="translate">Value for sell brute</label> 
                <input type="text" name="recom_value_for_sell_brute" id="recom_value_for_sell_brute" class="form-control money" > 
            </div>

            <div class="col-md-6 no-padding">
                <label class="translate">Value for sell liquid</label> 
                <input type="text" name="recom_value_for_sell_liquid" id="recom_value_for_sell_liquid" class="form-control money" >
            </div>
        </div>

        <div class="col-md-5">
            <h4 class="no-padding translate">Traded value</h4>
            <div class="col-md-6 no-padding-left">
                <label class="translate">Value for sell brute</label> 
                <div class="input-group">
                    <input type="text" name="trade_value_for_sell_brute" id="trade_value_for_sell_brute" class="form-control money" onchange="setTradedValue(this.value)">
                    <span class="input-group-addon">DG</span>
                </div>
            </div>

            <div class="col-md-6 no-padding">
                <label class="translate">Value for sell liquid</label> 
                <div class="input-group">
                    <input type="text" name="trade_value_for_sell_liquid" id="trade_value_for_sell_liquid" class="form-control money" onchange="setTradedValue(this.value)">
                    <span id="div_total_final_traded" class="input-group-addon">0.00</span>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <h4 class="no-padding translate">Takeaway</h4>
            <div class="col-md-6 no-padding-left">
                <label class="translate">Price for travel brute</label> 
                <input type="text" name="value_for_travel_brute" id="value_for_travel_brute" class="form-control money" onchange="setTakeawayPrice(this.value)"> 
            </div>

            <div class="col-md-6 no-padding">
                <label class="translate">Price for travel liquid</label> 
                <input type="text" name="value_for_travel_liquid" id="value_for_travel_liquid" class="form-control money" onchange="setTakeawayPrice(this.value)"> 
            </div>
        </div>
    </div>

    <div class="divider"></div>

    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
                <li><h4 class="no-padding"><t class="translate">Wholesale price:</t></h4></li>
                <li>
                    <div class="btn-group" role="group" aria-label="...">
                        <button id="wholesale_price_table_create" type="button" class="btn btn-primary translate">Create</button>
                        <button id="wholesale_price_table_update" type="button" class="btn btn-primary translate">Update</button>
                        <button id="wholesale_price_table_delete" type="button" class="btn btn-primary translate">Delete</button>
                    </div>
                </li>
            </ul>
            <table id="wholesale_price_table" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 60px">
                            <input type="checkbox" name="all" id="all-checkbox" value="0" onclick="markAll('wholesale_price_table')">
                        </th>
                        <th class="text-center translate" style="width: 60px">Id</th>
                        <th class="translate">Unit Price</th>
                        <th class="translate">Qtd</th>
                        <th class="translate">Price for quantity</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>	

    <!-- Create -->
    <div class="modal fade" id="wholesale_price_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title translate" id="myModalLabel">Wholesale Price</h4>
                </div>
                <form id="wholesale_price_form">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="unitPrice" class="translate">Unit Price</label>
                                <input type="text" name="unitPrice" id="unitPrice" class="form-control money" required="true">
                            </div>
                            <div class="col-md-6">
                                <label for="qtd" class="translate">Qtd</label>
                                <input type="number" name="qtd" id="qtd" class="form-control number" required="true">
                            </div>
                            <div class="col-md-6">
                                <label for="priceForQuantity" class="translate">Price for Quantity</label>
                                <input type="text" name="priceForQuantity" id="priceForQuantity" class="form-control money" required="true">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary translate">Insert</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Update -->
    <div class="modal fade" id="wholesale_price_modal_edit" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title translate">Update Wholesale Price</h4>
                </div>
                <form id="wholesale_price_form_edit">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="unitPrice" class="translate">Unit Price</label>
                                <input type="number" name="idWholesalePrice" id="idWholesalePrice" style="display: none" required="true">
                                <input type="text" name="unitPrice" id="unitPrice" class="form-control money" required="true">
                            </div>
                            <div class="col-md-6">
                                <label for="qtd" class="translate">Qtd</label>
                                <input type="number" name="qtd" id="qtd" class="form-control number" required="true">
                            </div>
                            <div class="col-md-6">
                                <label for="priceForQuantity" class="translate">Price for Quantity</label>
                                <input type="text" name="priceForQuantity" id="priceForQuantity" class="form-control money" required="true">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary translate">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
<script type="text/javascript">
    function loadWholesalePrice(){
        var wholesale_price_table = $('#wholesale_price_table').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllWholesalePrice/"+idProduct},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "unitPrice" },
                { "data": "qtd" },
                { "data": "priceForQuantity" }
            ],
            "retrieve": true,
            "paging": false,
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#wholesale_price_table_create', function(e){
            e.preventDefault();
            resetForm('#wholesale_price_form');
            $('#wholesale_price_modal').modal({
                show : true
            });
        });

        $(document).on('submit', '#wholesale_price_form', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertWholesalePrice",
                type: "POST",
                dataType: 'JSON',
                data : 'idProduct='+idProduct+'&'+$('#wholesale_price_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#wholesale_price_modal').modal('hide');
                    reload_table(wholesale_price_table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#wholesale_price_table_update', function(e){
            e.preventDefault();
            resetForm('#wholesale_price_form_edit');
            $.ajax({
                url : my_url+"Entry/Product/getWholesalePrice/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + wholesale_price_table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#wholesale_price_modal_edit').modal({show : true});
                        $('#wholesale_price_form_edit #idWholesalePrice').val(data.data[0].id);
                        $('#wholesale_price_form_edit #unitPrice').val(data.data[0].unitPrice);
                        $('#wholesale_price_form_edit #qtd').val(data.data[0].qtd);
                        $('#wholesale_price_form_edit #priceForQuantity').val(data.data[0].priceForQuantity);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#wholesale_price_form_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateWholesalePrice",
                type: "POST",
                dataType: 'JSON',
                data : $('#wholesale_price_form_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#wholesale_price_modal_edit').modal('hide');
                    reload_table(wholesale_price_table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#wholesale_price_table_delete', function(e){
            e.preventDefault();
            var trs = wholesale_price_table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idWholesalePrice : ids};
            deleteItens('Entry/Product/deleteWholesalePrice', array_deletes, wholesale_price_table);
        });

        $('#wholesale_price_table_update').attr('disabled', true);
        $('#wholesale_price_table_delete').attr('disabled', true);
        $("#wholesale_price_table tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#wholesale_price_table');
            if(itemSelected == 0){
                $('#wholesale_price_table_update').attr('disabled', true);
                $('#wholesale_price_table_delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#wholesale_price_table_update').attr('disabled', false);
                $('#wholesale_price_table_delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#wholesale_price_table_update').attr('disabled', true);
                $('#wholesale_price_table_delete').attr('disabled', false);
            }
        });

    }
</script>