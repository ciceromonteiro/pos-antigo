<style>
    #additional_table .dataTables_length {
        padding-top: 0px;
        padding-bottom: 0px;
    }
    #additional_table .dataTables_filter {
        padding-top: 0px;
        padding-bottom: 0px;
    }
    td.details-control {
        background: url('../../../assets/images/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('../../../assets/images/details_close.png') no-repeat center center;
    }    
    #additional_itens_table_wrapper .dataTables_filter {
        display: none;
    }
</style>

<div id="additional" class="tab-pane">
    <div class="row">
        <div class="col-md-12">
            <ul class="list-inline" style="padding-left: 30px; padding-top: 15px;">
                <li><h4 class="no-padding translate">Additional:</h4></li>
                <li>
                    <div class="btn-group" role="group">
                <button id="create_additional" type="button" class="btn btn-primary translate">Add</button>
                <button id="update_additional" type="button" class="btn btn-primary translate">Update</button>
                <button id="delete_additional" type="button" class="btn btn-primary translate">Delete</button>
                    </div>
                </li>
            </ul>
            <table id="additional_table" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('additional_table')"></th>
                        <th class="translate">Id</th>
                        <th class="translate">Order</th>
                        <th class="translate">Additional Type</th>
                        <th class="translate">Info</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="additional_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Additional</h4>
            </div>
            <form id="create_additional_itens_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="number" style="display: none" name="idAdditional" id="idAdditional">
                            <label for="order" class="label-style translate">Order</label>
                            <input type="text" class="form-control" id="order" name="order" required="true">
                        </div>
                        <div class="col-md-2">
                            <label for="additionalType" class="label-style translate">Additional Type</label>
                            <select name="additionalType" id="additionalType">
                                <option value="1" class="translate">Normal</option>
                                <option value="2" class="translate">Required</option>
                                <option value="3" class="translate">Multiselect</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label for="info" class="label-style translate">Info</label>
                            <input type="text" class="form-control" id="info" name="info" required="true">
                        </div>
                    </div>
                    
                    <div id="additional_itens" class="tab-pane">
                        <ul class="list-inline" style="margin-top: 30px;">
                            <li>
                                <div class="btn-group" role="group">
                                    <button id="create_additional_itens" type="submit" class="btn btn-primary translate">Add</button>
                                    <button id="update_additional_itens" type="button" class="btn btn-primary translate">Update</button>
                                    <button id="delete_additional_itens" type="button" class="btn btn-primary translate">Delete</button>
                                </div>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="additional_itens_table" class="table-bordered" cellspacing="0" width="100%" style="margin-top: 10px">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" name="all"></th>
                                            <th class="translate">ID</th>
                                            <th class="translate">Product</th>
                                            <th class="translate">Group</th>
                                            <th class="translate">Price</th>
                                            <th class="translate">Price Type</th>
                                            <th class="translate">Qtd</th>
                                            <th class="translate">Bruk Factor</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" style="display: none;" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" style="display: none;" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Update -->
<div class="modal fade" id="additional_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Additional</h4>
            </div>
            <form id="update_additional_itens_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="number" style="display: none" name="idAdditional" id="idAdditional">
                            <label for="order" class="label-style translate">Order</label>
                            <input type="text" class="form-control" id="order" name="order" required="true">
                        </div>
                        <div class="col-md-2">
                            <label for="additionalType" class="label-style translate">Additional Type</label>
                            <select name="additionalType" id="additionalType">
                                <option value="1" class="translate">Normal</option>
                                <option value="2" class="translate">Required</option>
                                <option value="3" class="translate">Multiselect</option>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <label for="info" class="label-style translate">Info</label>
                            <input type="text" class="form-control" id="info" name="info" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Create Items -->
<div class="modal fade" id="additional_itens_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Additional Item</h4>
            </div>
            <form id="additional_itens_form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="product" class="label-style translate">Product</label>
                            <select id="product" name="product" required="true">
                                <?php echo '<option value="">Select Option</option>'.$select_products ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="product_group" class="label-style translate">Group</label>
                            <select name="product_group" id="product_group" required="true">
                                <?php echo '<option value="">Not in use</option>'.$select_groups ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="price_type" class="label-style translate">Price Type</label>
                            <select name="price_type" id="price_type">
                                <option value="0" class="translate">Not in use</option>
                                <option value="1" class="translate">Set as price</option>
                                <option value="2" class="translate">Add to main article`s price</option>
                                <option value="3" class="translate">Set as main article`s price</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="price" class="label-style translate">Price</label>
                            <input type="text" class="form-control money" id="price" name="price" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="nameColor" class="label-style translate">Qtd</label>
                            <input type="text" class="form-control" id="qtd" name="qtd" required="true">
                        </div>
                        <div class="col-md-12">
                            <p style="margin-top: 15px;"><input type="checkbox" name="bruk_factor" id="bruk_factor"> <t class="translate">Bruk Factor</t></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update Items -->
<div class="modal fade" id="additional_itens_modal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Additional Item Update</h4>
            </div>
            <form id="additional_itens_form_edit">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                           <input type="number" style="display: none" id="idAdditionalItem" name="idAdditionalItem" require="true">
                           <input type="number" style="display: none" id="idAdditional" name="idAdditional" require="true">
                           
                            <label for="product" class="label-style translate">Product</label>
                            <select id="product" name="product" required="true">
                                <?php echo '<option value="">Select Option</option>'.$select_products ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="product_group" class="label-style translate">Group</label>
                            <select name="product_group" id="product_group" required="true">
                                <?php echo '<option value="">Not in use</option>'.$select_groups ?>
                            </select>
                        </div>
                        <div class="col-md-12">
                            <label for="price_type" class="label-style translate">Price Type</label>
                            <select name="price_type" id="price_type">
                                <option value="0" class="translate">Not in use</option>
                                <option value="1" class="translate">Set as price</option>
                                <option value="2" class="translate">Add to main article`s price</option>
                                <option value="3" class="translate">Set as main article`s price</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="price" class="label-style translate">Price</label>
                            <input type="text" class="form-control money" id="price" name="price" required="true">
                        </div>
                        <div class="col-md-6">
                            <label for="nameColor" class="label-style translate">Qtd</label>
                            <input type="text" class="form-control" id="qtd" name="qtd" required="true">
                        </div>
                        <div class="col-md-12">
                            <p style="margin-top: 15px;"><input type="checkbox" name="bruk_factor" id="bruk_factor"> <t class="translate">Bruk Factor</t></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
 var additional_table;
    /* Formatting function for row details - modify as you need */
    function format(d) {
        // `d` is the original data object for the row
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding:0px;margin:0px;">' +
                '<tr>' +
                '<td>Full name:</td>' +
                '<td>' + d.name + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Extension number:</td>' +
                '<td>' + d.extn + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Extra info:</td>' +
                '<td>And any further details here (images etc)...</td>' +
                '</tr>' +
                '</table>';
    }
    
    function validateFormAdditional(){
        var order = $('#create_additional_itens_form #order').val();
        var additionalType = $('#create_additional_itens_form #additionalType').val();
        var additionalInfo = $('#create_additional_itens_form #info').val();

        if(order == '' || order == undefined || order == 'NaN' || order == 0){
            return false;
        } 
        if(additionalType == '' || additionalType == undefined || additionalType == 'NaN' || additionalType == 0){
            return false;
        } 
        if(additionalInfo == '' || additionalInfo == undefined || additionalInfo == 'NaN' || additionalInfo == 0){
            return false;
        }
        return true;
    }

    function updateAdditional(form){
        id = $('#create_additional_itens_form #idAdditional').val();
            $.ajax({
                url : my_url+"Entry/Product/updateAdditional/"+id,
                type: "POST",
                dataType: 'JSON',
                data : $(form).serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        additional_table.destroy();
                        additional_table = $('#additional_table').DataTable( {
                            "ajax": {"url": my_url+"Entry/Product/getAllAdditional/"+idProduct},
                            "columns": [
                                { "data": "checkbox" },
                                { "data": "id" },
                                { "data": "order" },
                                { "data": "additionalType" },
                                { "data": "info" }
                            ],
                            "retrieve": true,
                            "paging": false,
                            "language": {
                                "url": my_url+my_language+".json"
                            }
                        });
                        return true;
                    } else {
                        notification(false);
                        console.log(data.message);
                        return false;
                    }


                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                    return false;
                }
            });
        }
    
    //function loadAdditional(){
$(document).ready(function() {
        /*$(document).on('click', '#create_additional', function (e) {
            e.preventDefault();
            $('#additional_modal').modal({show: true});
        });*/

        var idAdditionalTableModal = "";
        var additional_table_modal = "";

        var additional_table_modal = $('#additional_itens_table').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllAdditionalItens/"+idAdditionalTableModal},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "product" },
                { "data": "group" },
                { "data": "price" },
                { "data": "priceType" },
                { "data": "qty" },
                { "data": "brukFactor" }
            ],
            "retrieve": true,
            "paging": false,
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        additional_table = $('#additional_table').DataTable( {
            "ajax": {"url": my_url+"Entry/Product/getAllAdditional/"+idProduct},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "order" },
                { "data": "additionalType" },
                { "data": "info" }
            ],
            "retrieve": true,
            "paging": false,
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $('#update_additional').attr('disabled', true);
        $('#delete_additional').attr('disabled', true);
        $("#additional_table tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#additional_table');
            if(itemSelected == 0){
                $('#update_additional').attr('disabled', true);
                $('#delete_additional').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_additional').attr('disabled', false);
                $('#delete_additional').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_additional').attr('disabled', true);
                $('#delete_additional').attr('disabled', false);
            }
        });
        
        /*var additional_table = $('#additional_table').DataTable({
            "ajax": '../../../object.txt',
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "name"},
                {"data": "order"},
                {"data": "additionalType"},
                {"data": "info"}
            ],
            "retrieve": true,
            "paging": false,
            "order": [[1, 'asc']]
        });*/
        
        // Add event listener for opening and closing details
        $('#additional_table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = additional_table.row(tr);
            if (row.child.isShown()) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                // Open this row
                row.child(format(row.data())).show();
                tr.addClass('shown');
            }
        });

        $(document).on('click', '#create_additional', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/insertAdditional/"+idProduct,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        resetForm('#create_additional_itens_form');
                        $('#create_additional_itens_form #idAdditional').val(data.data);
                        $('#additional_modal').modal({show : true});
                        idAdditionalTableModal = data.data;
                         //reload_table(additional_table_modal);
                         additional_table_modal.destroy();
                         additional_table_modal = $('#additional_itens_table').DataTable( {
                            "ajax": {"url": my_url+"Entry/Product/getAllAdditionalItens/"+idAdditionalTableModal},
                            "columns": [
                                { "data": "checkbox" },
                                { "data": "id" },
                                { "data": "product" },
                                { "data": "group" },
                                { "data": "price" },
                                { "data": "priceType" },
                                { "data": "qty" },
                                { "data": "brukFactor" }
                            ],
                            "retrieve": true,
                            "paging": false,
                            "language": {
                                "url": my_url+my_language+".json"
                            }
                        });

                         reload_table(additional_table);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });



        $(document).on('click', '#update_additional', function(e){
            var id = additional_table.$('tr.hover-select').find('input:checkbox').data('id');
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/getAdditional/"+id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#additional_modal_edit').modal({show : true});
                        $('#update_additional_itens_form #idAdditional').val(data.data[0].id);
                        $('#update_additional_itens_form #order').val(data.data[0].order);
                        $('#update_additional_itens_form #additionalType').val(data.data[0].additionalType);
                        $('#update_additional_itens_form #info').val(data.data[0].info);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });


        $(document).on('submit', '#update_additional_itens_form', function(e){
            var idAdditional = $('#update_additional_itens_form #idAdditional').val();
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Product/updateAdditional/"+idAdditional,
                type: "POST",
                dataType: 'JSON',
                data : $('#update_additional_itens_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                    $('#additional_modal_edit').modal('hide');
                    reload_table(additional_table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

         $(document).on('click', '#delete_additional', function(e){
            e.preventDefault();
            var trs = additional_table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idAdditional : ids};
            deleteItens('Entry/Product/deleteAdditional', array_deletes, additional_table);
        });



        
        $(document).on('submit', '#create_additional_itens_form', function(e){
            e.preventDefault();
            var validate = validateFormAdditional();
            if(validate != false){
                var validateInsert = updateAdditional(this);
                if(validateInsert != false){
                    resetForm('#additional_itens_form');
                    $('#additional_itens_modal').modal({
                        show : true
                    });
                    $('#create_components_product_modal_edit').modal('hide');
                }
            } else {
                alert('Please fill out the fields correctly before adding an item.');
            }
        });
        
        

        $(document).on('submit', '#additional_itens_form', function(e){
            e.preventDefault();
            var idAdditional = $('#create_additional_itens_form #idAdditional').val();
            $.ajax({
                url : my_url+"Entry/Product/insertAdditionalItem/"+idAdditional,
                type: "POST",
                dataType: 'JSON',
                data : $('#additional_itens_form').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#additional_itens_modal').modal('hide');
                     reload_table(additional_table_modal);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $('#update_additional_itens').attr('disabled', true);
        $('#delete_additional_itens').attr('disabled', true);
        $("#additional_itens_table tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#additional_itens_table');
            if(itemSelected == 0){
                $('#update_additional_itens').attr('disabled', true);
                $('#delete_additional_itens').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update_additional_itens').attr('disabled', false);
                $('#delete_additional_itens').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update_additional_itens').attr('disabled', true);
                $('#delete_additional_itens').attr('disabled', false);
            }
        });

        $(document).on('click', '#update_additional_itens', function(e){
            e.preventDefault();
            resetForm('#additional_itens_form_edit');
            var id = additional_table_modal.$('tr.hover-select').find('input:checkbox').data('id');
            //alert(id);
            $.ajax({
                url : my_url+"Entry/Product/getAdditionalItem/"+id,
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#additional_itens_modal_edit').modal({show : true});
                        $('#additional_itens_form_edit #idAdditionalItem').val(data.data[0].id);
                        $('#additional_itens_form_edit #idAdditional').val(data.data[0].idProductAdditional);
                        $('#additional_itens_form_edit #product').val(data.data[0].product);
                        $('#additional_itens_form_edit #product_group').val(data.data[0].group);
                        $('#additional_itens_form_edit #price_type').val(data.data[0].priceType);
                        $('#additional_itens_form_edit #price').val(data.data[0].price);
                        $('#additional_itens_form_edit #qtd').val(data.data[0].qty);
                        $('#additional_itens_form_edit #bruk_factor').prop("checked",data.data[0].brukFactor);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#additional_itens_form_edit', function(e){
            e.preventDefault();
            var idAdditional = $('#additional_itens_form_edit #idAdditional').val();
            var idAdditionalItem = $('#additional_itens_form_edit #idAdditionalItem').val();
            $.ajax({
                url : my_url+"Entry/Product/updateAdditionalItem/"+idAdditionalItem,
                type: "POST",
                dataType: 'JSON',
                data : $('#additional_itens_form_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#additional_itens_modal_edit').modal('hide');
                     reload_table(additional_table_modal);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete_additional_itens', function(e){
            e.preventDefault();
            var trs = additional_table_modal.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idAdditionalItems : ids};
            deleteItens('Entry/Product/deleteAdditionalitems', array_deletes, additional_table_modal);
        });


        
    });
</script>