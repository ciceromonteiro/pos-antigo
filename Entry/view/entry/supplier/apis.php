<?php 
    $option_supplier = "<option value='0'>Select option</option>";
    if(isset($array_answer['supplier'])){
        foreach ($array_answer['supplier'] as $value){
            $option_supplier .= "<option value='".$value->getIdsupplier()."'>".$value->getCompanycompany()->getName()."</option>";
        }
    }
    
    $option_type_api = "<option value='0'>Select option</option>";
    if(isset($array_answer['type_api'])){
        foreach ($array_answer['type_api'] as $value){
            $option_type_api .= "<option value='".$value->getIdsuppliertypeapi()."'>".$value->getName()."</option>";
        }
    }
    
    $option_format_api = "<option value='0'>Select option</option>";
    if(isset($array_answer['format_api'])){
        foreach ($array_answer['format_api'] as $value){
            $option_format_api .= "<option value='".$value->getIdsupplierformatapi()."'>".$value->getName()."</option>";
        }
    }
?>

<script type="text/javascript">
    $(document).ready(function() {
        
        var table = $('#supplierApi').DataTable( {
            "ajax": {"url": my_url+"Entry/Supplier/getAllApis/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "supplier" },
                { "data": "typeapi" },
                { "data": "formatapi" },
                { "data": "address" },
                { "data": "username" },
                { "data": "password" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#create', function(e){
            e.preventDefault();
            resetForm('#form_supplier_api');
            $('#myModal').modal({
                show : true
            });
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            resetForm('#form_supplier_api_edit');
            $.ajax({
                url : my_url+"Entry/Supplier/getSupplierApi/",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        $('#supplierApiModal').modal({show : true});
                        $('#form_supplier_api_edit #idSupplierApi').val(data.data[0].id);
                        $('#form_supplier_api_edit #supplier').val(data.data[0].supplier);
                        $('#form_supplier_api_edit #typeApi').val(data.data[0].typeApi);
                        $('#form_supplier_api_edit #typeFormat').val(data.data[0].typeFormat);
                        $('#form_supplier_api_edit #address').val(data.data[0].address);
                        $('#form_supplier_api_edit #username').val(data.data[0].username);
                        $('#form_supplier_api_edit #password').val(data.data[0].password);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_supplier_api', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Supplier/insertApi",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_supplier_api').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        $(document).on('submit', '#form_supplier_api_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Supplier/updateApi",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_supplier_api_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#supplierApiModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idSuppliersApis : ids};
            deleteItens('Entry/Supplier/deleteApis', array_deletes, table);
        });
        
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#supplierApi tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#supplierApi');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    });
</script>

<?php    
    $nav = "apis";
    include "nav.php";
?>


<!-- Table List -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="supplierApi" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('supplierApi')"></th>
                    <th style="width: 10px" class="translate">ID</th>
                    <th class="translate">Supplier</th>
                    <th class="translate">Type Api</th>
                    <th class="translate">Format Api</th>
                    <th class="translate">Address</th>
                    <th class="translate">Username</th>
                    <th class="translate">Password</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Supplier Api</h4>
            </div>
            <form id="form_supplier_api">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="supplier" class="label-style translate">Supplier</label>
                            <select name="supplier" require="true"><?php echo $option_supplier ?></select>
                        </div>
                        <div class="col-md-12">
                            <label for="typeApi" class="label-style translate">Type Api</label>
                            <select name="typeApi" require="true"><?php echo $option_type_api ?></select>
                        </div>
                        <div class="col-md-12">
                            <label for="typeFormat" class="label-style translate">Type Format Api</label>
                            <select name="typeFormat" require="true"><?php echo $option_format_api ?></select>
                        </div>
                        <div class="col-md-12">
                            <label for="address" class="label-style translate">Address</label>
                            <input name="address" type="text" class="form-control" require="true">
                        </div>
                        <div class="col-md-6">
                            <label for="username" class="label-style translate">Username</label>
                            <input name="username" type="text" class="form-control"  require="true">
                        </div>
                        <div class="col-md-6">
                            <label for="password" class="label-style translate">Password</label>
                            <input name="password" type="password" class="form-control" require="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="supplierApiModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Supplier Api</h4>
            </div>
            <form id="form_supplier_api_edit">
                <div class="modal-body">
                    <div class="row">
                        <input type="text" id="idSupplierApi" name="idSupplierApi" style="display: none">
                        <div class="col-md-12">
                            <label for="supplier" class="label-style translate">Supplier</label>
                            <select name="supplier" id="supplier" require="true"><?php echo $option_supplier ?></select>
                        </div>
                        <div class="col-md-12">
                            <label for="typeApi" class="label-style translate">Type Api</label>
                            <select name="typeApi" id="typeApi" require="true"><?php echo $option_type_api ?></select>
                        </div>
                        <div class="col-md-12">
                            <label for="typeFormat" class="label-style translate">Type Format Api</label>
                            <select name="typeFormat" id="typeFormat" require="true"><?php echo $option_format_api ?></select>
                        </div>
                        <div class="col-md-12">
                            <label for="address" class="label-style translate">Address</label>
                            <input name="address" id="address" type="text" class="form-control" require="true">
                        </div>
                        <div class="col-md-6">
                            <label for="username" class="label-style translate">Username</label>
                            <input name="username" id="username" type="text" class="form-control"  require="true">
                        </div>
                        <div class="col-md-6">
                            <label for="password" class="label-style translate">Password</label>
                            <input name="password" id="password" type="password" class="form-control" require="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>