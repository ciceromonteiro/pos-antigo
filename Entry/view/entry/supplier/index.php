<?php
    /**
     * Created by PhpStorm.
     * User: rocha
     * Date: 11/23/16
     * Time: 1:08 PM
     */
    $nav = "list";
    $country = "";
    include "nav.php";
    foreach ($array_answer['country'] as $value) {
        $country .= "<option value='".$value->getidzzCountry()."'>".$value->getName()."</option>";
    }
?>

<script type="text/javascript">
    $(document).ready(function() {

        var table_supplier = $('#supplier').DataTable( {
            "ajax": {"url": my_url+"Entry/supplier/getAll/"},
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "description" }
            ],
            "language": {
                "url": my_url+my_language+".json"
            }
        });

        $(document).on('click', '#update', function(e){
            e.preventDefault();
            var id      = table.$('tr.hover-select').find('input:checkbox').data('id');
            window.location.href = my_url+"Entry/supplier/update?id="+id;
        });
        
        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idSuppliers : ids};
            deleteItens('Entry/Supplier/delete', array_deletes, table);
        });

        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#supplier tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')){
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
            
            var itemSelected = checkMark('#supplier');
            if(itemSelected == 0){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if(itemSelected == 1){
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if(itemSelected > 1){
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });
    } );
</script>

<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create_modal_supplier" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update_modal_supplier" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete_modal_supplier" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="supplier" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('supplier')"></th>
                        <th style="width: 10px" class="translate">ID</th>
                        <th class="translate">Description</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<?php
    require_once __dir__.'/modal/create_supplier.php';
    modalCreateSupplier($country);
?>