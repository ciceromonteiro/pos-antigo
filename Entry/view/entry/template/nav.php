<ul class="navbar-three">
    <li <?php echo ($nav == "Templates") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/template/index" ?>" class="translate">Templates</a>
    </li>
    <li <?php echo ($nav == "Printer settings") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/template/printerSettings" ?>" class="translate">Printer settings</a>
    </li>
    <li <?php echo ($nav == "E-post") ? 'class="active"' : '' ?>>
        <a href="<?php echo URL_BASE."Entry/template/epost" ?>">E-post</a>
    </li>
</ul>