<?php 
    $nav = "E-post";
    include "nav.php";
?>

<script type="text/javascript">
    $(document).ready(function() {
        function getData(){
            $.ajax({
                url : my_url+"Entry/Template/getEpost",
                type: "POST",
                dataType: 'JSON',
                data : '',
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        var values = data.data[0];
                        for (var prop in values) {
                            document.getElementById(prop).value = values[prop];
                        }
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        }
        
        $(document).on('submit', '#form_epost', function(e){
            e.preventDefault();
            $.ajax({
                url : my_url+"Entry/Template/updateEpost",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_epost').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        document.getElementById("form_epost").reset();
        getData();
    });
</script>

<div class="content">
    <form name="form_epost" id="form_epost">
        <div class="row row-fluid" style="margin-bottom: 60px;">
            <div class="col-md-12">
                <h4 class="translate">Invoice</h4>
                <div class="col-md-4">
                    <label for="invoice_text_sent_email_title" class="translate">Text to be sent in the email title:</label>
                    <textarea id="invoice_text_sent_email_title" name="invoice_text_sent_email_title" class="form-control" cols="5" rows="6"></textarea>
                </div>
                <div class="col-md-4">
                    <label for="invoice_text_first_email" class="translate">Text of the first email:</label>
                    <textarea id="invoice_text_first_email" name="invoice_text_first_email" class="form-control" cols="5" rows="6"></textarea>
                </div>
                <div class="col-md-4">
                    <label for="invoice_text_second_email" class="translate">Text of the second email:</label>
                    <textarea id="invoice_text_second_email" name="invoice_text_second_email" class="form-control" cols="5" rows="6"></textarea>
                </div>
            </div>

            <div class="col-md-12">
                <h4 class="translate">Proposals</h4>
                <div class="col-md-4">
                    <label for="proposals_text_sent_email_title" class="translate">Text to be sent in the email title:</label>
                    <textarea id="proposals_text_sent_email_title" name="proposals_text_sent_email_title" class="form-control" cols="5" rows="6"></textarea>
                </div>
                <div class="col-md-4">
                    <label for="proposals_text" class="translate">Proposal text:</label>
                    <textarea id="proposals_text" name="proposals_text" class="form-control" cols="5" rows="6"></textarea>
                </div>
            </div>
        </div>

        <div class="btns-footer">
            <div class="pull-right">
                <button class="btn btn-primary translate">Cancel</button>
                <button type="submit" class="btn btn-success translate">Save</button>
            </div>
        </div>
    </form>
</div>
