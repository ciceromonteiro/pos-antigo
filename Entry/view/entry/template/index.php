<script type="text/javascript">
    $(document).ready(function () {

        $.ajax({
                url: my_url + "Entry/Template/getAccessCardImg",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#image_upload_preview3').attr('src', data.data.img);
                        //notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    
                    //$('#templateEditModal').modal('hide');
                    //reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });

        $.ajax({
                url: my_url + "Entry/Template/getEmployeeCardsImg",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#image_upload_preview2').attr('src', data.data.img);
                        //notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    
                    //$('#templateEditModal').modal('hide');
                    //reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });

            $.ajax({
                url: my_url + "Entry/Template/getLogo",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#image_upload_preview1').attr('src', data.data.img);
                        //notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    
                    //$('#templateEditModal').modal('hide');
                    //reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });

        var table = $('#template').DataTable({
            "ajax": {"url": my_url + "Entry/Template/getAllTemplate/"},
            "columns": [
                {"data": "checkbox"},
                {"data": "id"},
                {"data": "name"},
                {"data": "withdrawalFromStock"},
                {"data": "sellByInvoice"},
                {"data": "includeInStockStatistics"},
                {"data": "changePrintSendMethod"},
                {"data": "printDeliveryList"},
                {"data": "printInvoice"},
                {"data": "templateOrder"}
            ],
            "language": {
                "url": my_url + my_language + ".json"
            }
        });

        $(document).on('click', '#create', function (e) {
            e.preventDefault();
            resetForm('#form_template');
            $('#myModal').modal({
                show: true
            });
        });

        $(document).on('click', '#update', function (e) {
            e.preventDefault();
            resetForm('#form_template_edit');
            $.ajax({
                url: my_url + "Entry/Template/getTemplate/",
                type: "POST",
                dataType: 'JSON',
                data: 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#templateEditModal').modal({show: true});
                        $('#form_template_edit #preview').html(data.data.image);
                        $('#form_template_edit #idTemplate').val(data.data.id);
                        $('#form_template_edit #nameTemplate').val(data.data.name);
                        if (data.data.withdrawalFromStock == 1) {
                            $('#form_template_edit #withdrawalFromStock').prop("checked", true);
                        }
                        if (data.data.sellByInvoice == 1) {
                            $('#form_template_edit #sellByInvoice').prop("checked", true);
                        }
                        if (data.data.includeInStockStatistics == 1) {
                            $('#form_template_edit #includeInStockStatistics').prop("checked", true);
                        }
                        if (data.data.changePrintSendMethod == 1) {
                            $('#form_template_edit #changePrintSendMethod').prop("checked", true);
                        }
                        if (data.data.printDeliveryList == 1) {
                            $('#form_template_edit #printDeliveryList').prop("checked", true);
                        }
                        if (data.data.printInvoice == 1) {
                            $('#form_template_edit #printInvoice').prop("checked", true);
                        }
                        if (data.data.image != '') {
                            $('#form_template_edit #image_upload_preview').attr('src', data.data.image);
                            $('#form_template_edit #imageUpdate').val(data.data.image);
                        }
                        $('#form_template_edit #templateOrder').val(data.data.templateOrder);
                        $('#form_template_edit #textFooter').val(data.data.footer);
                        $('#form_template_edit #textFooterA5').val(data.data.footerA5);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_template', function (e) {
            e.preventDefault();
            var form_values = $('#form_template').serialize();
            $.ajax({
                url: my_url + "Entry/Template/insertTemplate",
                type: "POST",
                dataType: 'JSON',
                data: form_values,
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('submit', '#form_template_edit', function (e) {
            e.preventDefault();
            $.ajax({
                url: my_url + "Entry/Template/updateTemplate",
                type: "POST",
                dataType: 'JSON',
                data: $('#form_template_edit').serialize(),
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#templateEditModal').modal('hide');
                    reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        $(document).on('click', '#delete', function (e) {
            e.preventDefault();
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });
            var i = 0, ids = [];
            for (i = 0; i < trs.length; i++) {
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }
            var array_deletes = {idTemplates: ids};
            deleteItens('Entry/Template/deleteTemplates', array_deletes, table);
        });

        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $("#template tbody").on('click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ($(this).hasClass('hover-select')) {
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            } else {
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }

            var itemSelected = checkMark('#template');
            if (itemSelected == 0) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
            }
            if (itemSelected == 1) {
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
            }
            if (itemSelected > 1) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', false);
            }
        });

    });
    
    function b64EncodeUnicode(str) {
            // first we use encodeURIComponent to get percent-encoded UTF-8,
            // then we convert the percent encodings into raw bytes which
            // can be fed into btoa.
            return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
                function toSolidBytes(match, p1) {
                    return String.fromCharCode('0x' + p1);
            }));
        }

    function newLogo(){

        var image = $('#image_upload_preview1').attr('src');
        if (image === "") {
            image = "0";
        }  else {
            image = b64EncodeUnicode(image);
        }

        $.ajax({
                url: my_url + "Entry/Template/insertLogo",
                type: "POST",
                dataType: 'JSON',
                data: {image: image},
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    //$('#templateEditModal').modal('hide');
                    //reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        
    }

    function deleteLogo(){
        decision = confirm("Are you sure you want to delete?");
        if (decision){
            $.ajax({
                url: my_url + "Entry/Template/logoDelete",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#image_upload_preview1').attr('src', data.data);
                        document.getElementById("uploadImage1").value = "";
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    //$('#templateEditModal').modal('hide');
                    //reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        } 
       
        
        
    }


    function modal1(){
        $('#modalEmployeeCards').modal({
                show: true
            });
    }

    function newImageEmployeeCard(){
        var image = $('#image_upload_preview2').attr('src');
        if (image === "") {
            image = "0";
        }  else {
            image = b64EncodeUnicode(image);
        }
        $.ajax({
                url: my_url + "Entry/Template/newEmployeeCardsImg",
                type: "POST",
                dataType: 'JSON',
                data: {image: image},
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
       

    }

    function deleteImageEmployeeCard(){

       decision = confirm("Are you sure you want to delete?");
        if (decision){
        $.ajax({
                url: my_url + "Entry/Template/deleteEmployeeCardsImg",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#image_upload_preview2').attr('src', data.data);
                        document.getElementById("uploadImage2").value = "";
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    //$('#templateEditModal').modal('hide');
                    //reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }
        
    }

    function modal2(){
        $('#accessCardImage').modal({
                show: true
            });
    }

    function newAccessCardImg(){
        var image = $('#image_upload_preview3').attr('src');
        if (image === "") {
            image = "0";
        }  else {
            image = b64EncodeUnicode(image);
        }
        $.ajax({
                url: my_url + "Entry/Template/newAccessCardImg",
                type: "POST",
                dataType: 'JSON',
                data: {image: image},
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }

    function deleteAccessCardImg(){
        decision = confirm("Are you sure you want to delete?");
        if (decision){
        $.ajax({
                url: my_url + "Entry/Template/deleteAccessCardImg",
                type: "POST",
                dataType: 'JSON',
                success: function (data, textStatus, jqXHR) {
                    if (data.result == 'success') {
                        $('#image_upload_preview3').attr('src', data.data);
                        document.getElementById("uploadImage3").value = "";
                        notification(true);

                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    //$('#templateEditModal').modal('hide');
                    //reload_table(table);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
    }
    }



function loadImageFileAsURL1(){
        var filesSelected = document.getElementById("uploadImage1").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image1').val(fileLoadedEvent.target.result);
                $('#image_upload_preview1').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

    function loadImageFileAsURL2(){
        var filesSelected = document.getElementById("uploadImage2").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image2').val(fileLoadedEvent.target.result);
                $('#image_upload_preview2').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

    function loadImageUploadFileAsURL2(){
        var filesSelected = document.getElementById("uploadImageUpdate2").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#imageUpdate2').val(fileLoadedEvent.target.result);
                $('#image_upload_preview2').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

</script>

<!-- Menu -->
<?php
$nav = "Templates";
include "nav.php";
?>

<!-- Table List -->
<div class="content">
<div class="top-bar-buttons" >
    <div class="button-group">
        <div class="row">
            <div class="col-md-2 pull-right">
                <div class="col-md-12">
                <h4 class="translate">Define Logo</h4>
                    <div class="inputFileImg">
                        <img width="130" id="image_upload_preview1" src="../../../assets/images/upload.png" alt="your image" onclick="$('#uploadImage1').click()" />
                        <input type="file" id="uploadImage1" name="uploadImage1" style="display: none" onchange="loadImageFileAsURL1()">
                        <input type="text" id="image1" name="image1" style="display: none">
                    </div>
                </div>
            </div>

        </div>
    </div>    
    </div>
    <div class="top-bar-buttons" style="float: right;">
        <div class="button-group">
            <button id="newLogo" class="btn btn-success" onclick="newLogo()"><span class="lnr lnr-menu-circle"></span> <t class="translate">update</t></button>
            <button id="deleteLogo" class="btn btn-primary" onclick="deleteLogo()"><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="top-bar-buttons" style="float: left;">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="template" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 10px"><input type="checkbox" name="all" onclick="markAll('template')"></th>
                        <th style="width: 10px" class="translate">ID</th>
                        <th class="translate">Name</th>
                        <th class="translate">Withdrawal from stock</th>
                        <th class="translate">Sell by invoice</th>
                        <th class="translate">Include in stock statistics</th>
                        <th class="translate">Change print / send method</th>
                        <th class="translate">Print delivery list</th>
                        <th class="translate">Print invoice</th>
                        <th class="translate">Template order</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="btns-footer">
        <div class="pull-left">
            <div class="btn-group dropup"> 
                <button type="button" class="btn btn-primary translate">More options</button> 
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> 
                    <span class="caret"></span> <span class="sr-only">Toggle Dropdown</span> 
                </button> 
                <ul class="dropdown-menu"> 
                    <li><a class="pointer translate" onclick="modal1()">Define images of employee cards</a></li> 
                    <li><a class="pointer translate" onclick="modal2()">Define access card image</a></li> 
                    <li><a href="labelLayout" class="translate">Define Label Layout</a></li> 
                </ul> 
            </div>
        </div>
        <div class="pull-right">
            <button class="btn btn-primary translate">Cancel</button>
        </div>
    </div>
</div>

<style>
    .previewImg {
        width: 120px; 
        height: 120px; 
        background-color: #ccc; 
        margin-top: 23px
    }
    .previewImg > p {
        padding-top: 28%;
        color: black;
        text-align: center;
        font-size: 12px;
    }
    .previewImg > p > span {
        font-size: 28px;
    }
    .pointer {
    cursor:pointer;
    }
</style>

<!-- Create -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Template</h4>
            </div>
            <form id="form_template">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameTemplate" class="label-style translate">Name</label>
                            <input type="text" class="form-control" id="nameTemplate" name="nameTemplate" required="true">
                            <label for="templateOrder" class="label-style translate">Template order</label>
                            <input type="text" class="form-control" id="templateOrder" name="templateOrder" required="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="margin-top: 10px">
                            <p><input type="checkbox" name="withdrawalFromStock" id="withdrawalFromStock"> <t class="translate">Withdrawal from stock</t></p>
                            <p><input type="checkbox" name="sellByInvoice" id="sellByInvoice"> <t class="translate">Sell by invoice</t></p>
                            <p><input type="checkbox" name="includeInStockStatistics" id="includeInStockStatistics"> <t class="translate">Include in stock statistics</t></p>
                        </div>
                        <div class="col-md-6" style="margin-top: 10px">
                            <p><input type="checkbox" name="changePrintSendMethod" id="changePrintSendMethod"> <t class="translate">Change print / send method</t></p>
                            <p><input type="checkbox" name="printDeliveryList" id="printDeliveryList"> <t class="translate">Print delivery list</t></p>
                            <p><input type="checkbox" name="printInvoice" id="printInvoice"> <t class="translate">Print invoice</t></p>
                        </div>
                        <div class="col-md-12">
                            <label for="textFooter" class="translate">Footer</label>
                            <textarea class="form-control" name="textFooter" id="textFooter" cols="6" rows="4"></textarea>
                        </div>
                        <div class="col-md-12">
                            <label for="textFooter" class="translate">Footer A5</label>
                            <textarea class="form-control" name="textFooterA5" id="textFooterA5" cols="6" rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="templateEditModal" tabindex="-1" role="dialog" aria-labelledby="labelColorUpdate">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="labelColorUpdate">Update Template</h4>
            </div>
            <form id="form_template_edit">
                <div class="modal-body">
                    <div class="row">
                        <input type="number" class="form-control" id="idTemplate" name="idTemplate" required="true" style="display: none">
                        <div class="col-md-12">
                            <label for="nameTemplate" class="label-style translate">Name</label>
                            <input type="text" class="form-control" id="nameTemplate" name="nameTemplate" required="true">
                            <label for="templateOrder" class="label-style translate">Template order</label>
                            <input type="text" class="form-control" id="templateOrder" name="templateOrder" required="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="margin-top: 10px">
                            <p><input type="checkbox" name="withdrawalFromStock" id="withdrawalFromStock"> <t class="translate">Withdrawal from stock</t></p>
                            <p><input type="checkbox" name="sellByInvoice" id="sellByInvoice"> <t class="translate">Sell by invoice</t></p>
                            <p><input type="checkbox" name="includeInStockStatistics" id="includeInStockStatistics"> <t class="translate">Include in stock statistics</t></p>
                        </div>
                        <div class="col-md-6" style="margin-top: 10px">
                            <p><input type="checkbox" name="changePrintSendMethod" id="changePrintSendMethod"> <t class="translate">Change print / send method</t></p>
                            <p><input type="checkbox" name="printDeliveryList" id="printDeliveryList"> <t class="translate">Print delivery list</t></p>
                            <p><input type="checkbox" name="printInvoice" id="printInvoice"> <t class="translate">Print invoice</t></p>
                        </div>
                        <div class="col-md-12">
                            <label for="textFooter" class="translate">Footer</label>
                            <textarea class="form-control" name="textFooter" id="textFooter" cols="6" rows="4"></textarea>
                        </div>
                        <div class="col-md-12">
                            <label for="textFooter" class="translate">Footer A5</label>
                            <textarea class="form-control" name="textFooterA5" id="textFooterA5" cols="6" rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEmployeeCards" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="text-align: center;">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Define Images of employee Cards</h4>
            </div>
                <div class="modal-body">
                    <div class="row" >
                        <div class="col-md-12" >
                            <div class="inputFileImg" >
                                <img width="130" id="image_upload_preview2" src="../../../assets/images/upload.png" alt="your image" onclick="$('#uploadImage2').click()" />
                                <input type="file" id="uploadImage2" name="uploadImage2" style="display: none" onchange="loadImageFileAsURL2()">
                                <input type="text" id="image2" name="image2" style="display: none">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <button type="updateEmployeeCards" class="btn btn-success translate" onclick="newImageEmployeeCard()" data-dismiss="modal">Update</button>
                    <button type="deleteEmployeeCards" class="btn btn-primary translate" onclick="deleteImageEmployeeCard()">Delete</button>
                </div>
        </div>
    </div>
</div>

<div class="modal fade" id="accessCardImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="text-align: center;">
    <div class="modal-dialog" role="document"> 
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Define Access Card Image</h4>
            </div>
                <div class="modal-body">
                    <div class="row" >
                        <div class="col-md-12" >
                            <div class="inputFileImg" >
                                <img width="130" id="image_upload_preview3" src="../../../assets/images/upload.png" alt="your image" onclick="$('#uploadImage3').click()" />
                                <input type="file" id="uploadImage3" name="uploadImage3" style="display: none" onchange="loadImageFileAsURL3()">
                                <input type="text" id="image3" name="image3" style="display: none">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: center;">
                    <button type="updateAccessCardImg" class="btn btn-success translate" onclick="newAccessCardImg()" data-dismiss="modal">Update</button>
                    <button type="deleteAccessCardImg" class="btn btn-primary translate" onclick="deleteAccessCardImg()">Delete</button>
                </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    function loadImageFileAsURL(){
        var filesSelected = document.getElementById("uploadImage").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image').val(fileLoadedEvent.target.result);
                $('#image_upload_preview').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

    function loadImageUploadFileAsURL(){
        var filesSelected = document.getElementById("uploadImageUpdate").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#imageUpdate').val(fileLoadedEvent.target.result);
                $('#image_upload_preview').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

     
    

    function loadImageFileAsURL3(){
        var filesSelected = document.getElementById("uploadImage3").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#image3').val(fileLoadedEvent.target.result);
                $('#image_upload_preview3').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }

    function loadImageUploadFileAsURL3(){
        var filesSelected = document.getElementById("uploadImageUpdate3").files;
        if (filesSelected.length > 0){
            var fileToLoad = filesSelected[0];
            var fileReader = new FileReader();
            fileReader.onload = function(fileLoadedEvent){
                $('#imageUpdate3').val(fileLoadedEvent.target.result);
                $('#image_upload_preview3').attr('src', fileLoadedEvent.target.result);
            };
            fileReader.readAsDataURL(fileToLoad);
        }
    }
</script>

<!--
<script type="text/javascript">
    window.URL = window.URL || window.webkitURL;
    var elBrowse = document.getElementById("inputUploadLogo"),
            elPreview = document.getElementById("preview"),
            useBlob = false && window.URL;
    
    function getBlobImage(){
        var image = $(".inputFileImg").find("img:first").attr("src");
        return image;
    }
    
    function constructView(extension,imageBlob){
        var image = '<img src="data:image/'+extension+';base64,'+imageBlob+'" width="120">';
        return image;
    }

    function readImage(file) {
        document.getElementById("preview").innerHTML = "";
        var reader = new FileReader();
        reader.addEventListener("load", function () {
            var image = new Image();
            image.addEventListener("load", function () {
                this.width = '120';
                elPreview.append(this);
                if (useBlob) {
                    window.URL.revokeObjectURL(image.src);
                }
            });
            image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;
        });
        reader.readAsDataURL(file);
    }

    elBrowse.addEventListener("change", function () {
        var files = this.files;
        var errors = "";
        if (!files) {
            errors += "File upload not supported by your browser.";
        }
        if (files && files[0]) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if ((/\.(png|jpeg|jpg|gif)$/i).test(file.name)) {
                    readImage(file);
                } else {
                    errors += file.name + " Unsupported Image extension\n";
                }
            }
        }
        if (errors) {
            alert(errors);
        }
    });
</script> -->