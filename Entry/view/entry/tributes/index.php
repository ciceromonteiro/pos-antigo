<?php 
$options_vat = "<option value=''>Select Option</option>";
foreach ($array_answer['taxs'] as $value){
    $options_vat .= "<option value='".$value->getIdTax()."'>".$value->getName()."</option>";
}
?>
<script type="text/javascript">
    $(document).ready(function() {

        var table = $('#tribute').DataTable( {
            "ajax": {"url": "<?php echo URL_BASE ?>Entry/Tribute/getAll"},
            /*"dom": 'frtip',*/
            "columns": [
                { "data": "checkbox" },
                { "data": "id" },
                { "data": "name" },
                { "data": "pay_vat" },
                { "data": "tax" },
                { "data": "treasury" },
                { "data": "treasury_without_vat" },
                { "data": "cod_additional" },
                { "data": "vat_alternative" },
                { "data": "treasury_alternative" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "ordering": false,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });

        function reload_table(){
            table.ajax.reload(null,false);
        }
        
        $(document).on('click', '#create', function(e){
            e.preventDefault();
            $('#myModal').modal({
                show : true
            });
        });

        // Select tr
        $("#tribute tbody").on( 'click', 'tr', function () {
            var checkboxInput = $(this).find('input:checkbox');
            if ( $(this).hasClass('hover-select') ) {
                $('#update').attr('disabled', true);
                $('#delete').attr('disabled', true);
                $(this).removeClass('hover-select');
                checkboxInput[0].checked = false;
            }
            else {
                $('#update').attr('disabled', false);
                $('#delete').attr('disabled', false);
                $(this).addClass('hover-select');
                checkboxInput[0].checked = true;
            }
        } );

        // Add submit form
        $(document).on('submit', '#form_tribute', function(e){
            e.preventDefault();
            $.ajax({
                url : "<?php echo URL_BASE ?>Entry/Tribute/insert",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_tribute').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#myModal').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        // Get for update
        $(document).on('submit', '#update', function(e){
            e.preventDefault();
            $.ajax({
                url : "<?php echo URL_BASE ?>Entry/Tribute/getTribute",
                type: "POST",
                dataType: 'JSON',
                data : 'id=' + table.$('tr.hover-select').find('input:checkbox').data('id'),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });
        
        // Edit submit form
        $(document).on('submit', '#form_tribute_edit', function(e){
            e.preventDefault();
            $.ajax({
                url : "<?php echo URL_BASE ?>Entry/Tribute/update",
                type: "POST",
                dataType: 'JSON',
                data : $('#form_tribute_edit').serialize(),
                success: function(data, textStatus, jqXHR){
                    if (data.result == 'success'){
                        notification(true);
                    } else {
                        notification(false);
                        console.log(data.message);
                    }
                    $('#tributeEditModal').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown){
                    notification(false);
                    console.log(jqXHR.responseText);
                }
            });
        });

        // Delete
        $(document).on('click', '#delete', function(e){
            e.preventDefault();
            confirmDelete(this);
        });
        
        /* Delete Modal */
        function confirmDelete(btnDelete){        
            var trs = table.$('tr.hover-select').each(function () {
                var sThisVal = (this.checked ? $(this).val() : "");
            });        
            var i=0, ids = [];
            for(i=0; i<trs.length; i++){
                ids[i] = $(trs[i]).find('input:checkbox').data('id');
            }                    
            var array_deletes = {idTributes : ids};
            deleteItems('#tributeEditModal','Entry/Tribute/deleteTributes', array_deletes);
        }
        
    });
</script>

<!-- Menu -->
<div id="navbar-three">
    <ul class="navbar-three">
        <li class="active"><a href="#" class="translate">List</a></li>
    </ul>
</div>

<!-- Table List Tributes -->
<div class="content">
    <div class="top-bar-buttons">
        <div class="button-group">
            <button id="create" class="btn btn-primary"><span class="lnr lnr-checkmark-circle"></span> <t class="translate">New</t></button>
            <button id="update" class="btn btn-primary" disabled><span class="lnr lnr-menu-circle"></span> <t class="translate">Edit</t></button>
            <button id="delete" class="btn btn-primary" disabled><span class="lnr lnr-circle-minus"></span> <t class="translate">Remove</t></button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table id="tribute" class="table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 60px"><input type="checkbox" name="all" id="all-checkbox" value="0"></th>
                    <th style="width: 80px" class="translate">ID</th>
                    <th class="translate">Name</th>
                    <th class="translate">Pay VAT</th>
                    <th class="translate">Tax</th>
                    <th class="translate">Treasury</th>
                    <th class="translate">Treasury without VAT</th>
                    <th class="translate">Cod Additional</th>
                    <th class="translate">VAT Alternative</th>
                    <th class="translate">Treasury Alternative</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Tribute</h4>
            </div>
            <form id="form_tribute" name="form_tribute" class="form_tribute">
                <input type="text" id="idTribute" name="idTribute" style="display: none">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="nameTribute" class="label-style translate">Tribute</label>
                            <input type="text" class="form-control" id="nameTribute" name="nameTribute" required="true">
                            <br><p><input type="checkbox" name="pay_vat" id="pay_vat"> <t class="translate">Pay Vat</t></p>
                        </div>
                        <div class="col-md-6">
                            <label for="vat" class="translate">Vat</label>
                            <select name="vat" id="vat" required="true">
                                <?php echo $options_vat ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="treasury" class="translate">Treasury</label>
                            <input type="text" name="treasury" class="form-control number" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="treasuryWithoutVat" class="translate">Treasury Without VAT</label>
                            <input type="text" name="treasuryWithoutVat" class="form-control number" required="true">
                        </div>
                        <div class="col-md-4">
                            <label for="codAdditional" class="translate">Cod Additional</label>
                            <input type="text" name="codAdditional" class="form-control number" required="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="alternativeVat" class="translate">Alternative Vat</label>
                            <select name="alternativeVat" id="alternativeVat" required="true">
                                <?php echo $options_vat ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="treasuryAlternative" class="translate">Treasury Alternative</label>
                            <input type="text" name="treasuryAlternative" class="form-control number" required="true">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Insert</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<div class="modal fade" id="tributeEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title translate" id="myModalLabel">Update Tribute</h4>
            </div>
            <form id="form_tribute_edit" name="form_tribute" class="form_tribute">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="nameTribute" class="label-style translate">Tribute</label>
                            <input type="text" class="form-control" placeholder="Name" id="idTribute" name="idTribute" value="" style="display: none">
                            <input type="text" class="form-control" placeholder="Name" id="nameTribute" name="nameTribute" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-primary translate" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary translate">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var checkboxAll = $("#tribute thead tr").find('input:checkbox');
    checkboxAll[0].checked = false;
    $('#update').attr('disabled', true);
    $('#delete').attr('disabled', true);

    $('#tribute thead').on('click', '#all-checkbox', function () {        
        var idTable = "tribute";
        var i;
        var checkboxArray = $("#"+idTable+" tbody tr").find('input:checkbox');
        var isChecked = $("#"+idTable+" thead tr").find('input:checkbox');

        if(isChecked[0].checked == false){
            for(i = 0; i < checkboxArray.length; i++){
                $("#"+idTable+" tbody").find('tr').removeClass('hover-select');
                checkboxArray[i].checked = false;
            }

            $('#update').attr('disabled', true);
            $('#delete').attr('disabled', true);            
        } else {
            for(i = 0; i < checkboxArray.length; i++){
                $("#"+idTable+" tbody").find('tr').addClass('hover-select');
                checkboxArray[i].checked = true;
            }


            $('#delete').attr('disabled', false);
        }
    });
</script>