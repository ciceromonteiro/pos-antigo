-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: Standard
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_card_img_tmpt`
--

DROP TABLE IF EXISTS `access_card_img_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_card_img_tmpt` (
  `idaccess_card_img_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `img` longblob,
  `active` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`idaccess_card_img_tmpt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_card_img_tmpt`
--

LOCK TABLES `access_card_img_tmpt` WRITE;
/*!40000 ALTER TABLE `access_card_img_tmpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `access_card_img_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `idaddress` int(11) NOT NULL AUTO_INCREMENT,
  `zz_state_idzz_state` int(11) DEFAULT NULL,
  `person_idperson` int(11) DEFAULT NULL COMMENT 'This can be null if company_idcompany is not.',
  `company_idcompany` int(11) DEFAULT NULL COMMENT 'This can be null if person_idperson is not.',
  `name` varchar(45) DEFAULT NULL,
  `visiting_address` tinyint(1) DEFAULT NULL COMMENT 'Whether this is a visiting address or not.',
  `address_street` varchar(100) NOT NULL,
  `address_number` varchar(14) DEFAULT NULL,
  `address_district` varchar(45) DEFAULT NULL,
  `address_city` varchar(100) DEFAULT NULL,
  `address_zipcode` varchar(45) NOT NULL,
  `address_complement` varchar(200) DEFAULT NULL,
  `address_reference` varchar(200) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idaddress`),
  KEY `fk_address_zz_state1_idx` (`zz_state_idzz_state`),
  KEY `fk_address_person1_idx` (`person_idperson`),
  KEY `fk_address_company1_idx` (`company_idcompany`),
  CONSTRAINT `fk_address_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_address_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_address_zz_state1` FOREIGN KEY (`zz_state_idzz_state`) REFERENCES `zz_state` (`idzz_state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applied_tx`
--

DROP TABLE IF EXISTS `applied_tx`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applied_tx` (
  `idapplied_tx` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `tax_idtax` int(11) NOT NULL,
  `customer_gp_idcustomer_gp` int(11) DEFAULT NULL,
  `zz_state_idzz_state` int(11) DEFAULT NULL,
  `zz_country_idzz_country` int(11) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idapplied_tx`),
  KEY `fk_applied_tx_tax1_idx` (`tax_idtax`),
  KEY `fk_applied_tx_customer_gp1_idx` (`customer_gp_idcustomer_gp`),
  KEY `fk_applied_tx_zz_state1_idx` (`zz_state_idzz_state`),
  KEY `fk_applied_tx_zz_country1_idx` (`zz_country_idzz_country`),
  KEY `fk_applied_tx_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_applied_tx_customer_gp1` FOREIGN KEY (`customer_gp_idcustomer_gp`) REFERENCES `customer_gp` (`idcustomer_gp`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_applied_tx_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_applied_tx_tax1` FOREIGN KEY (`tax_idtax`) REFERENCES `tax` (`idtax`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_applied_tx_zz_country1` FOREIGN KEY (`zz_country_idzz_country`) REFERENCES `zz_country` (`idzz_country`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_applied_tx_zz_state1` FOREIGN KEY (`zz_state_idzz_state`) REFERENCES `zz_state` (`idzz_state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applied_tx`
--

LOCK TABLES `applied_tx` WRITE;
/*!40000 ALTER TABLE `applied_tx` DISABLE KEYS */;
/*!40000 ALTER TABLE `applied_tx` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `idcategory` int(11) NOT NULL AUTO_INCREMENT,
  `category_idcategory` int(11) DEFAULT NULL COMMENT 'A category may be a subcategory of other one.',
  `name` varchar(60) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcategory`),
  KEY `fk_category_category1_idx` (`category_idcategory`),
  CONSTRAINT `fk_category_category1` FOREIGN KEY (`category_idcategory`) REFERENCES `category` (`idcategory`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `idcolor` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcolor`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'Test2','2017-05-17 15:47:41','2017-05-17 15:47:52','2017-05-17 15:48:03',3),(2,'Test','2017-05-18 18:46:00','2017-05-18 18:46:04','2017-05-18 18:46:07',3),(3,'Red','2017-06-01 14:12:49',NULL,NULL,1),(4,'Green','2017-06-01 14:12:54',NULL,NULL,1),(5,'Blue','2017-06-01 14:12:59',NULL,NULL,1),(6,'Pink','2018-04-19 14:38:26','2018-04-19 14:38:38','2018-04-19 14:38:52',3),(7,'pp','2018-08-06 13:50:57',NULL,'2018-08-06 13:51:05',3),(8,'PP21','2018-08-06 13:51:12','2018-08-06 13:52:31',NULL,1);
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `combined_sale`
--

DROP TABLE IF EXISTS `combined_sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `combined_sale` (
  `idcombined_sale` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcombined_sale`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `combined_sale`
--

LOCK TABLES `combined_sale` WRITE;
/*!40000 ALTER TABLE `combined_sale` DISABLE KEYS */;
/*!40000 ALTER TABLE `combined_sale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `combined_sale_product`
--

DROP TABLE IF EXISTS `combined_sale_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `combined_sale_product` (
  `idcombined_sale_product` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `combined_sale_idcombined_sale` int(11) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcombined_sale_product`),
  KEY `fk_combined_sale_product_product1_idx` (`product_idproduct`),
  KEY `fk_combined_sale_product_combined_sale1_idx` (`combined_sale_idcombined_sale`),
  CONSTRAINT `fk_combined_sale_product_combined_sale1` FOREIGN KEY (`combined_sale_idcombined_sale`) REFERENCES `combined_sale` (`idcombined_sale`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_combined_sale_product_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `combined_sale_product`
--

LOCK TABLES `combined_sale_product` WRITE;
/*!40000 ALTER TABLE `combined_sale_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `combined_sale_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `idcompany` int(11) NOT NULL AUTO_INCREMENT,
  `pos_idpos` int(11) NOT NULL,
  `payment_mtd_idpayment_mtd` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL COMMENT 'The official name of the company.',
  `fantasy_name` varchar(100) DEFAULT NULL COMMENT 'A name that the company is known.',
  `register_number` varchar(45) DEFAULT NULL COMMENT 'The National Register Number.',
  `info` longtext,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indicates the company status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcompany`),
  KEY `fk_company_pos1_idx` (`pos_idpos`),
  KEY `fk_company_payment_mtd1_idx` (`payment_mtd_idpayment_mtd`),
  CONSTRAINT `fk_company_payment_mtd1` FOREIGN KEY (`payment_mtd_idpayment_mtd`) REFERENCES `payment_mtd` (`idpayment_mtd`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,1,NULL,'Default','Default-Fantasy','12345789',NULL,'2017-03-17 22:13:43',NULL,NULL,1),(2,1,NULL,'Teste','',NULL,'','2018-08-10 16:29:06',NULL,'2018-08-13 13:47:18',3),(3,1,NULL,'Teste','',NULL,'','2018-08-13 12:44:06',NULL,'2018-08-13 13:47:18',3),(4,1,NULL,'Teste','',NULL,'','2018-08-13 12:44:16',NULL,'2018-08-13 13:47:18',3),(5,1,NULL,'Teste','RENATO ALMEIDA',NULL,'','2018-08-13 12:44:26',NULL,'2018-08-13 13:47:18',3),(6,1,NULL,'Teste','RENATO ALMEIDA',NULL,'','2018-08-13 12:44:38',NULL,'2018-08-13 13:47:18',3),(7,1,2,'Teste','RENATO ALMEIDA',NULL,'','2018-08-13 12:44:41',NULL,'2018-08-13 13:47:18',3),(8,1,2,'Teste','RENATO ALMEIDA',NULL,'123','2018-08-13 12:44:45',NULL,'2018-08-13 13:47:19',3),(9,1,2,'Teste','RENATO ALMEIDA',NULL,'123','2018-08-13 12:44:49',NULL,'2018-08-13 13:47:19',3),(10,1,2,'Teste','RENATO ALMEIDA',NULL,'123','2018-08-13 12:45:03',NULL,'2018-08-13 13:47:19',3),(11,1,2,'Teste','RENATO ALMEIDA',NULL,'123','2018-08-13 12:45:08',NULL,'2018-08-13 13:47:24',3),(12,1,2,'Teste','RENATO ALMEIDA',NULL,'123','2018-08-13 12:46:11',NULL,'2018-08-13 13:47:24',3),(13,1,NULL,'Teste','',NULL,'','2018-08-13 12:47:28',NULL,NULL,1),(14,1,2,'Teste 2','',NULL,'','2018-08-13 12:47:43',NULL,NULL,1);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_data`
--

DROP TABLE IF EXISTS `company_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_data` (
  `idcompany_data` int(11) NOT NULL AUTO_INCREMENT,
  `company_idcompany` int(11) NOT NULL,
  `data_type` varchar(250) NOT NULL COMMENT 'If this is a phone number, email, etc.',
  `data_value` varchar(250) NOT NULL COMMENT 'The Value. Ex: The number of the phone.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcompany_data`),
  KEY `fk_user_data_copy1_company1_idx` (`company_idcompany`),
  CONSTRAINT `fk_user_data_copy1_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_data`
--

LOCK TABLES `company_data` WRITE;
/*!40000 ALTER TABLE `company_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_factoring`
--

DROP TABLE IF EXISTS `company_factoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_factoring` (
  `idcompanyfactoring` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idcompanyfactoring`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_factoring`
--

LOCK TABLES `company_factoring` WRITE;
/*!40000 ALTER TABLE `company_factoring` DISABLE KEYS */;
INSERT INTO `company_factoring` VALUES (1,'None','2017-05-19 13:44:59',NULL,NULL,1),(2,'SG Finans','2017-05-19 13:44:59',NULL,NULL,1),(3,'Svea Finans','2017-05-19 13:44:59',NULL,NULL,1),(4,'Gothia Finans','2017-05-19 13:44:59',NULL,NULL,1),(5,'DnB','2017-05-19 13:44:59',NULL,NULL,1),(6,'Nordea Finans','2017-05-19 13:44:59',NULL,NULL,1);
/*!40000 ALTER TABLE `company_factoring` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_has_permission`
--

DROP TABLE IF EXISTS `company_has_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_has_permission` (
  `idcompany_has_permission` int(11) NOT NULL AUTO_INCREMENT,
  `permission_idpermission` int(11) NOT NULL,
  `company_idcompany` int(11) NOT NULL,
  PRIMARY KEY (`idcompany_has_permission`),
  KEY `fk_company_has_permission_permission1_idx` (`permission_idpermission`),
  KEY `fk_company_has_permission_company1_idx` (`company_idcompany`),
  CONSTRAINT `fk_company_has_permission_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_company_has_permission_permission1` FOREIGN KEY (`permission_idpermission`) REFERENCES `permission` (`idpermission`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_has_permission`
--

LOCK TABLES `company_has_permission` WRITE;
/*!40000 ALTER TABLE `company_has_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_has_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_image`
--

DROP TABLE IF EXISTS `company_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_image` (
  `idcompany_image` int(11) NOT NULL AUTO_INCREMENT,
  `company_idcompany` int(11) NOT NULL,
  `type` varchar(300) NOT NULL,
  `sequence` int(11) DEFAULT '0' COMMENT 'The images has a priority order: the lowest first.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `image` text NOT NULL,
  PRIMARY KEY (`idcompany_image`),
  KEY `fk_company_image_company1_idx` (`company_idcompany`),
  CONSTRAINT `fk_company_image_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_image`
--

LOCK TABLES `company_image` WRITE;
/*!40000 ALTER TABLE `company_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_info`
--

DROP TABLE IF EXISTS `company_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_info` (
  `id_company_info` int(11) NOT NULL AUTO_INCREMENT,
  `data_type` varchar(150) NOT NULL,
  `data_value` text NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `pos_company_idpos_company` int(11) NOT NULL,
  PRIMARY KEY (`id_company_info`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_info`
--

LOCK TABLES `company_info` WRITE;
/*!40000 ALTER TABLE `company_info` DISABLE KEYS */;
INSERT INTO `company_info` VALUES (1,'name','A Coxinharia','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(2,'fantasy_name','A Coxinharia','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(3,'register_number','19.139.057/0001-40','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(4,'phone_company','(84) 3615-1700','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(5,'fax_company','123124','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(6,'default_message_invoice','Test2','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(7,'address_street','Rua Anibal Brandao','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(8,'address_number','526','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(9,'address_district','Nova Parnamirim','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(10,'address_city','Parnamirim','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(11,'address_zip','59082090','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(12,'address_state','RN','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(13,'address_reference','Test','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(14,'vaddress_street','Test','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(15,'vaddress_number','123123','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(16,'vaddress_district','Test','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(17,'vaddress_city','Test','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(18,'vaddress_zipCode','123123','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(19,'vaddress_complement','Test','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(20,'vaddress_reference','Test','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(21,'bank_account','12312','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(22,'bank_iban','123123','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(23,'bank_bic','123123','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(24,'bank_email','test@test','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(25,'license_key','','2017-05-30 14:44:32','2018-04-19 13:57:47',NULL,1,1),(26,'inscricao_estadual','0962716790','2018-09-12 14:33:44',NULL,NULL,1,1),(27,'nota_versao','4.0','2018-09-17 17:01:55',NULL,NULL,1,1),(28,'nota_cUF','24','2018-09-17 17:03:12',NULL,NULL,1,1),(29,'nota_tpAmb','2','2018-09-17 17:05:06',NULL,NULL,1,1),(30,'nota_natOp','VENDA','2018-09-17 17:06:18',NULL,NULL,1,1),(31,'nota_mod','65','2018-09-17 17:06:47',NULL,NULL,1,1),(32,'nota_cMunFG','2408102','2018-09-17 17:41:29',NULL,NULL,1,1),(33,'nota_xLgr','Av. Engenheiro Roberto Freire','2018-09-17 18:02:41',NULL,NULL,1,1),(34,'nota_nro','5200','2018-09-17 18:02:54',NULL,NULL,1,1),(35,'nota_xCpl','','2018-09-17 18:03:04',NULL,NULL,1,1),(36,'nota_xBairro','Capim Macio','2018-09-17 18:03:19',NULL,NULL,1,1),(37,'nota_xMun','Natal','2018-09-17 18:05:49',NULL,NULL,1,1),(38,'nota_UF','RN','2018-09-17 18:27:29',NULL,NULL,1,1),(39,'nota_cPais','1058','2018-09-17 18:28:24',NULL,NULL,1,1),(40,'nota_xPais','BRASIL','2018-09-17 18:28:45',NULL,NULL,1,1),(41,'nota_fone','08432210977','2018-09-17 18:29:12',NULL,NULL,1,1),(44,'TESTE','Teste','2018-09-19 14:18:58',NULL,NULL,1,1),(45,'eeeeeeeeeeeeeeeeee','rrrrrrrrrrrrrrrrrrr',NULL,NULL,NULL,NULL,1),(46,'aaaaaaaaaaaaa','asssssssssssss',NULL,NULL,NULL,NULL,1),(47,'aaaaaaaaaaaaa','asssssssssssss',NULL,NULL,NULL,NULL,1),(48,'aaaaaaaaaaaaaaaaaaaa','rrrrrrrrrrrrr',NULL,NULL,NULL,NULL,1),(49,'aaaaaaaaaaaaaaaaaaaa','rrrrrrrrrrrrr',NULL,NULL,NULL,NULL,1),(50,'qqqqqqqqqqqqqq','wwwwwwww','2018-09-24 23:52:02',NULL,NULL,1,1),(51,'aaaaaaaaa','bbbbbbbbbbb','2018-09-24 23:52:13',NULL,NULL,1,1);
/*!40000 ALTER TABLE `company_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_info_img`
--

DROP TABLE IF EXISTS `company_info_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_info_img` (
  `idcompanyinfoimg` int(11) NOT NULL AUTO_INCREMENT,
  `image` longblob NOT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idcompanyinfoimg`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_info_img`
--

LOCK TABLES `company_info_img` WRITE;
/*!40000 ALTER TABLE `company_info_img` DISABLE KEYS */;
INSERT INTO `company_info_img` VALUES (1,'',1);
/*!40000 ALTER TABLE `company_info_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_card`
--

DROP TABLE IF EXISTS `credit_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_card` (
  `idcreditcard` int(11) NOT NULL AUTO_INCREMENT,
  `bbsid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `accounting` varchar(100) DEFAULT NULL,
  `show_balancing` int(11) NOT NULL,
  `send_register` int(11) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idcreditcard`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_card`
--

LOCK TABLES `credit_card` WRITE;
/*!40000 ALTER TABLE `credit_card` DISABLE KEYS */;
INSERT INTO `credit_card` VALUES (1,1,'Bankkort','Bankkort','1920',1,1,'2017-03-17 22:13:43',NULL,NULL,1),(2,2,'Visa','Visa','1940',1,1,'2017-03-17 22:13:43',NULL,NULL,1),(3,3,'EuroCard','EuroCard','1940',1,1,'2017-03-17 22:13:43',NULL,NULL,1),(4,4,'AmEx','AmEx','1940',1,1,'2017-03-17 22:13:43',NULL,NULL,1),(5,5,'Diners','Diners','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(6,6,'JCB','JCB','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(7,7,'Trumf','Trumf','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(8,8,'Maestro','Maestro','1940',1,1,'2017-03-17 22:13:43',NULL,NULL,1),(9,9,'Lindex','Lindex','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(10,10,'Ikano','Ikano','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(11,11,'NBBL','NBBL','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(12,12,'Gavekort Senter','Gavekort Senter','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(13,13,'Gavekort Kjede','Gavekort Kjede','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(14,14,'Esso Mastercard','Esso Mastercard','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(15,15,'Sentrumskortet','Sentrumskortet','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(16,16,'Statoil','Statoil','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(17,17,'XPonCard','XPonCard','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(18,18,'Multicard','Multicard','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(19,19,'Universal','Universal','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(20,20,'Bankaxept(EMV)','Bankaxept(EMV)','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(21,21,'Resurs Bank','Resurs Bank','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(22,22,'NG Bedriftskort','NG Bedriftskort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(23,23,'BBS Senter Gavekort','BBS Senter Gavekort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(24,24,'BBS Kjede Gavekort','BBS Kjede Gavekort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(25,25,'EMV ePurse','EMV ePurse','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(26,26,'Nokaskort','Nokaskort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(27,27,'S&S Medlemskort','S&S Medlemskort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(28,28,'Bring Bedriftskort','Bring Bedriftskort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(29,29,'Nordea','Nordea','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(30,30,'Handelsbanken','Handelsbanken','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(31,31,'Swedbank','Swedbank','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(32,32,'SEB','SEB','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(33,33,'Ressurs','Ressurs','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(34,34,'Dankort','Dankort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(35,35,'Coop Visa','Coop Visa','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(36,36,'Payex Gavekort','Payex Gavekort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(37,37,'BBS - Vot','BBS - Vot','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(38,38,'Trumf Visa','Trumf Visa','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(39,39,'Gavekort 1','Gavekort 1','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(40,40,'Cashcoms presentkort','Cashcoms presentkort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(41,41,'Storcash Bedrift','Storcash Bedrift','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(42,42,'PBS Gavekort','PBS Gavekort','0',0,0,'2017-03-17 22:13:43',NULL,NULL,1),(43,43,'Forbrungsforening','Forbrungsforening','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(44,44,'Sparxpres','Sparxpres','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(45,45,'China Unionpay','China Unionpay','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(46,46,'Rikslunchen','Rikslunchen','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(47,47,'Kjedekort 1','Kjedekort 1','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(48,48,'Collector Credit','Collector Credit','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(49,49,'FDM, DK','FDM, DK','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(50,50,'BBS Senter Gavekort','BBS Senter Gavekort','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(51,51,'PBS Centerkort','PBS Centerkort','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(52,52,'LIC Card','LIC Card','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(53,53,'Accept Card','Accept Card','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(54,54,'Coop Mastercard','Coop Mastercard','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(55,55,'Oberthur Gavekort','Oberthur Gavekort','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(56,56,'Bunnpris Bedrift','Bunnpris Bedrift','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(57,57,'Rikskortet','Rikskortet','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(58,58,'Coop Bedrift','Coop Bedrift','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(59,59,'Swedbank (Debet/Credit)','Swedbank (Debet/Credit)','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(60,60,'Visa Bankkort','Visa Bankkort','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(61,61,'Mastercard Bankkort','Mastercard Bankkort','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(62,62,'Swedbank','Swedbank','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(63,63,'Visa DK','Visa DK','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(64,64,'Mastercard DK','Mastercard DK','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(65,65,'Maestro DK','Maestro DK','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(66,66,'Diners DK','Diners DK','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(67,67,'Amex DK','Amex DK','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(68,68,'Ikano Finans','Ikano Finans','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(69,69,'KappAhl Club','KappAhl Club','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(70,70,'MediaMarkt','MediaMarkt','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(71,71,'Statoil MC','Statoil MC','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1),(72,72,'Visa Prepaid','Visa Prepaid','0',0,0,'2017-03-17 22:13:44',NULL,NULL,1);
/*!40000 ALTER TABLE `credit_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_management`
--

DROP TABLE IF EXISTS `credit_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_management` (
  `idcreditmanegement` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcreditmanegement`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_management`
--

LOCK TABLES `credit_management` WRITE;
/*!40000 ALTER TABLE `credit_management` DISABLE KEYS */;
INSERT INTO `credit_management` VALUES (1,'None','2017-05-19 13:43:55',NULL,NULL,1),(2,'Hokas','2017-05-19 13:43:55',NULL,NULL,1),(3,'Ikas','2017-05-19 13:43:55',NULL,NULL,1),(4,'IFS','2017-05-19 13:43:55',NULL,NULL,1),(5,'Predator','2017-05-19 13:43:55',NULL,NULL,1);
/*!40000 ALTER TABLE `credit_management` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_note`
--

DROP TABLE IF EXISTS `credit_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_note` (
  `idcredit_note` int(11) NOT NULL AUTO_INCREMENT,
  `credit_tmpt_idcredit_tmpt` int(11) DEFAULT NULL,
  `order_idorder` int(11) NOT NULL,
  `value` decimal(11,2) NOT NULL DEFAULT '0.00',
  `extra_info` longtext COMMENT 'OBS in the credit note.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcredit_note`),
  KEY `fk_credit_note_credit_tmpt1_idx` (`credit_tmpt_idcredit_tmpt`),
  KEY `fk_order1` (`order_idorder`),
  CONSTRAINT `fk_credit_note_credit_tmpt1` FOREIGN KEY (`credit_tmpt_idcredit_tmpt`) REFERENCES `credit_tmpt` (`idcredit_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order1` FOREIGN KEY (`order_idorder`) REFERENCES `order` (`idorder`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_note`
--

LOCK TABLES `credit_note` WRITE;
/*!40000 ALTER TABLE `credit_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_tmpt`
--

DROP TABLE IF EXISTS `credit_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_tmpt` (
  `idcredit_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `tmpt_header_idtmpt_header` int(11) NOT NULL,
  `tmpt_footer_idtmpt_footer` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `info` longtext COMMENT 'A text to be printed in the credit note.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcredit_tmpt`),
  KEY `fk_credit_tmpt_tmpt_header1_idx` (`tmpt_header_idtmpt_header`),
  KEY `fk_credit_tmpt_tmpt_footer1_idx` (`tmpt_footer_idtmpt_footer`),
  CONSTRAINT `fk_credit_tmpt_tmpt_footer1` FOREIGN KEY (`tmpt_footer_idtmpt_footer`) REFERENCES `tmpt_footer` (`idtmpt_footer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_credit_tmpt_tmpt_header1` FOREIGN KEY (`tmpt_header_idtmpt_header`) REFERENCES `tmpt_header` (`idtmpt_header`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_tmpt`
--

LOCK TABLES `credit_tmpt` WRITE;
/*!40000 ALTER TABLE `credit_tmpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currency`
--

DROP TABLE IF EXISTS `currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currency` (
  `idcurrency` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `iso` varchar(50) DEFAULT NULL,
  `symbol` varchar(7) NOT NULL COMMENT 'A symbol like: $, U$, R$, etc.',
  `exchange_rate` double DEFAULT NULL,
  `exchange_unit` varchar(50) DEFAULT NULL,
  `smallest_denomination` double DEFAULT NULL,
  `account` int(11) DEFAULT NULL,
  `iban` varchar(50) DEFAULT NULL,
  `bic` varchar(50) DEFAULT NULL,
  `account_currency_code` varchar(50) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcurrency`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currency`
--

LOCK TABLES `currency` WRITE;
/*!40000 ALTER TABLE `currency` DISABLE KEYS */;
INSERT INTO `currency` VALUES (1,'Defaut','trs','D$',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-03-17 22:13:43',NULL,NULL,1);
/*!40000 ALTER TABLE `currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_page`
--

DROP TABLE IF EXISTS `custom_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_page` (
  `idcustom_page` int(11) NOT NULL AUTO_INCREMENT,
  `user_iduser` int(11) DEFAULT NULL COMMENT 'The user_id prevails over pos_id. If a user_id is null, the customization is applied to all users in that specific POS.',
  `pos_idpos` int(11) DEFAULT NULL COMMENT 'The user_id prevails over pos_id. If a pos_id is null, the customization is applied to all POS for that specific user.',
  `page_name` varchar(60) NOT NULL,
  `json_file` varchar(254) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcustom_page`),
  KEY `fk_custom_page_pos1_idx` (`pos_idpos`),
  KEY `fk_custom_page_user1_idx` (`user_iduser`),
  CONSTRAINT `fk_custom_page_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_custom_page_user1` FOREIGN KEY (`user_iduser`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_page`
--

LOCK TABLES `custom_page` WRITE;
/*!40000 ALTER TABLE `custom_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_gp`
--

DROP TABLE IF EXISTS `customer_gp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_gp` (
  `idcustomer_gp` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idcustomer_gp`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_gp`
--

LOCK TABLES `customer_gp` WRITE;
/*!40000 ALTER TABLE `customer_gp` DISABLE KEYS */;
INSERT INTO `customer_gp` VALUES (1,'Admin','2017-05-17 11:30:14',NULL,NULL,1);
/*!40000 ALTER TABLE `customer_gp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_type`
--

DROP TABLE IF EXISTS `customer_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_type` (
  `idcustumer_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `company_idcompany` int(11) NOT NULL,
  PRIMARY KEY (`idcustumer_type`),
  KEY `fk_costumer_type_company1_idx` (`company_idcompany`),
  CONSTRAINT `fk_costumer_type_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_type`
--

LOCK TABLES `customer_type` WRITE;
/*!40000 ALTER TABLE `customer_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `default_permission`
--

DROP TABLE IF EXISTS `default_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `default_permission` (
  `iddefault_permission` int(11) NOT NULL AUTO_INCREMENT,
  `permission_idpermission` int(11) NOT NULL,
  `users_type_idusers_type` int(11) NOT NULL,
  `active` int(11) DEFAULT '1' COMMENT 'Indicates the "Default Permission" status.:\n1-Active\n2-Blocked\n3-Deleted',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`iddefault_permission`),
  KEY `fk_permission_type_permission1_idx` (`permission_idpermission`),
  KEY `fk_permission_type_user_type1_idx` (`users_type_idusers_type`),
  CONSTRAINT `fk_permission_type_permission1` FOREIGN KEY (`permission_idpermission`) REFERENCES `permission` (`idpermission`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_permission_type_user_type1` FOREIGN KEY (`users_type_idusers_type`) REFERENCES `users_type` (`idusers_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=356 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `default_permission`
--

LOCK TABLES `default_permission` WRITE;
/*!40000 ALTER TABLE `default_permission` DISABLE KEYS */;
INSERT INTO `default_permission` VALUES (1,1,1,2,'2018-04-19 14:21:15',NULL,NULL),(2,2,1,2,'2018-04-19 14:21:15',NULL,NULL),(3,3,1,2,'2018-04-19 14:21:15',NULL,NULL),(4,4,1,2,'2018-04-19 14:21:15',NULL,NULL),(5,5,1,2,'2018-04-19 14:21:15',NULL,NULL),(6,6,1,2,'2018-04-19 14:21:15',NULL,NULL),(7,7,1,2,'2018-04-19 14:21:15',NULL,NULL),(8,8,1,2,'2018-04-19 14:21:15',NULL,NULL),(9,9,1,2,'2018-04-19 14:21:15',NULL,NULL),(10,10,1,2,'2018-04-19 14:21:16',NULL,NULL),(11,11,1,2,'2018-04-19 14:21:16',NULL,NULL),(12,12,1,2,'2018-04-19 14:21:16',NULL,NULL),(13,13,1,2,'2018-04-19 14:21:16',NULL,NULL),(14,14,1,2,'2018-04-19 14:21:16',NULL,NULL),(15,15,1,2,'2018-04-19 14:21:16',NULL,NULL),(16,16,1,2,'2018-04-19 14:21:16',NULL,NULL),(17,17,1,2,'2018-04-19 14:21:16',NULL,NULL),(18,18,1,2,'2018-04-19 14:21:16',NULL,NULL),(19,19,1,2,'2018-04-19 14:21:16',NULL,NULL),(20,20,1,2,'2018-04-19 14:21:16',NULL,NULL),(21,21,1,2,'2018-04-19 14:21:16',NULL,NULL),(22,22,1,2,'2018-04-19 14:21:16',NULL,NULL),(23,23,1,2,'2018-04-19 14:21:16',NULL,NULL),(24,24,1,2,'2018-04-19 14:21:16',NULL,NULL),(25,25,1,2,'2018-04-19 14:21:16',NULL,NULL),(26,26,1,2,'2018-04-19 14:21:16',NULL,NULL),(27,27,1,2,'2018-04-19 14:21:16',NULL,NULL),(28,28,1,2,'2018-04-19 14:21:16',NULL,NULL),(29,29,1,2,'2018-04-19 14:21:16',NULL,NULL),(30,30,1,2,'2018-04-19 14:21:16',NULL,NULL),(31,31,1,2,'2018-04-19 14:21:16',NULL,NULL),(32,32,1,2,'2018-04-19 14:21:16',NULL,NULL),(33,33,1,2,'2018-04-19 14:21:16',NULL,NULL),(34,34,1,2,'2018-04-19 14:21:16',NULL,NULL),(35,35,1,2,'2018-04-19 14:21:16',NULL,NULL),(36,36,1,2,'2018-04-19 14:21:16',NULL,NULL),(37,37,1,2,'2018-04-19 14:21:16',NULL,NULL),(38,38,1,2,'2018-04-19 14:21:16',NULL,NULL),(39,39,1,2,'2018-04-19 14:21:16',NULL,NULL),(40,40,1,2,'2018-04-19 14:21:16',NULL,NULL),(41,41,1,2,'2018-04-19 14:21:16',NULL,NULL),(42,42,1,2,'2018-04-19 14:21:16',NULL,NULL),(43,43,1,2,'2018-04-19 14:21:16',NULL,NULL),(44,44,1,2,'2018-04-19 14:21:16',NULL,NULL),(45,45,1,2,'2018-04-19 14:21:16',NULL,NULL),(46,46,1,2,'2018-04-19 14:21:16',NULL,NULL),(47,47,1,2,'2018-04-19 14:21:16',NULL,NULL),(48,48,1,2,'2018-04-19 14:21:16',NULL,NULL),(49,49,1,2,'2018-04-19 14:21:16',NULL,NULL),(50,50,1,2,'2018-04-19 14:21:16',NULL,NULL),(51,51,1,2,'2018-04-19 14:21:16',NULL,NULL),(52,52,1,2,'2018-04-19 14:21:16',NULL,NULL),(53,53,1,2,'2018-04-19 14:21:16',NULL,NULL),(54,54,1,2,'2018-04-19 14:21:16',NULL,NULL),(55,55,1,2,'2018-04-19 14:21:17',NULL,NULL),(56,56,1,2,'2018-04-19 14:21:17',NULL,NULL),(57,57,1,2,'2018-04-19 14:21:17',NULL,NULL),(58,58,1,2,'2018-04-19 14:21:17',NULL,NULL),(59,59,1,2,'2018-04-19 14:21:17',NULL,NULL),(60,60,1,2,'2018-04-19 14:21:17',NULL,NULL),(61,61,1,2,'2018-04-19 14:21:17',NULL,NULL),(62,62,1,2,'2018-04-19 14:21:17',NULL,NULL),(63,63,1,2,'2018-04-19 14:21:17',NULL,NULL),(64,64,1,2,'2018-04-19 14:21:17',NULL,NULL),(65,65,1,2,'2018-04-19 14:21:17',NULL,NULL),(66,66,1,2,'2018-04-19 14:21:17',NULL,NULL),(67,67,1,2,'2018-04-19 14:21:17',NULL,NULL),(68,68,1,2,'2018-04-19 14:21:17',NULL,NULL),(69,69,1,2,'2018-04-19 14:21:17',NULL,NULL),(70,70,1,2,'2018-04-19 14:21:17',NULL,NULL),(71,71,1,2,'2018-04-19 14:21:17',NULL,NULL),(72,72,1,2,'2018-04-19 14:21:17',NULL,NULL),(73,73,1,2,'2018-04-19 14:21:17',NULL,NULL),(74,74,1,2,'2018-04-19 14:21:17',NULL,NULL),(75,75,1,2,'2018-04-19 14:21:17',NULL,NULL),(76,76,1,2,'2018-04-19 14:21:17',NULL,NULL),(77,77,1,2,'2018-04-19 14:21:17',NULL,NULL),(78,78,1,2,'2018-04-19 14:21:17',NULL,NULL),(79,79,1,2,'2018-04-19 14:21:17',NULL,NULL),(80,80,1,2,'2018-04-19 14:21:17',NULL,NULL),(81,81,1,2,'2018-04-19 14:21:17',NULL,NULL),(82,82,1,2,'2018-04-19 14:21:17',NULL,NULL),(83,83,1,2,'2018-04-19 14:21:17',NULL,NULL),(84,84,1,2,'2018-04-19 14:21:17',NULL,NULL),(85,85,1,2,'2018-04-19 14:21:17',NULL,NULL),(86,86,1,2,'2018-04-19 14:21:17',NULL,NULL),(87,87,1,2,'2018-04-19 14:21:17',NULL,NULL),(88,88,1,2,'2018-04-19 14:21:17',NULL,NULL),(89,89,1,2,'2018-04-19 14:21:17',NULL,NULL),(90,90,1,2,'2018-04-19 14:21:17',NULL,NULL),(91,91,1,2,'2018-04-19 14:21:17',NULL,NULL),(92,92,1,2,'2018-04-19 14:21:17',NULL,NULL),(93,93,1,2,'2018-04-19 14:21:17',NULL,NULL),(94,94,1,2,'2018-04-19 14:21:17',NULL,NULL),(95,95,1,2,'2018-04-19 14:21:17',NULL,NULL),(96,96,1,2,'2018-04-19 14:21:17',NULL,NULL),(97,97,1,2,'2018-04-19 14:21:17',NULL,NULL),(98,98,1,2,'2018-04-19 14:21:17',NULL,NULL),(99,99,1,2,'2018-04-19 14:21:17',NULL,NULL),(100,100,1,2,'2018-04-19 14:21:17',NULL,NULL),(101,101,1,2,'2018-04-19 14:21:17',NULL,NULL),(102,102,1,2,'2018-04-19 14:21:17',NULL,NULL),(103,103,1,2,'2018-04-19 14:21:17',NULL,NULL),(104,104,1,2,'2018-04-19 14:21:17',NULL,NULL),(105,105,1,2,'2018-04-19 14:21:18',NULL,NULL),(106,106,1,2,'2018-04-19 14:21:18',NULL,NULL),(107,107,1,2,'2018-04-19 14:21:18',NULL,NULL),(108,108,1,2,'2018-04-19 14:21:18',NULL,NULL),(109,109,1,2,'2018-04-19 14:21:18',NULL,NULL),(110,110,1,2,'2018-04-19 14:21:18',NULL,NULL),(111,111,1,2,'2018-04-19 14:21:18',NULL,NULL),(112,112,1,2,'2018-04-19 14:21:18',NULL,NULL),(113,113,1,2,'2018-04-19 14:21:18',NULL,NULL),(114,114,1,2,'2018-04-19 14:21:18',NULL,NULL),(115,115,1,2,'2018-04-19 14:21:18',NULL,NULL),(116,116,1,2,'2018-04-19 14:21:18',NULL,NULL),(117,117,1,2,'2018-04-19 14:21:18',NULL,NULL),(118,118,1,2,'2018-04-19 14:21:18',NULL,NULL),(119,119,1,2,'2018-04-19 14:21:18',NULL,NULL),(120,120,1,2,'2018-04-19 14:21:18',NULL,NULL),(121,121,1,2,'2018-04-19 14:21:18',NULL,NULL),(122,122,1,2,'2018-04-19 14:21:18',NULL,NULL),(123,123,1,2,'2018-04-19 14:21:18',NULL,NULL),(124,124,1,2,'2018-04-19 14:21:18',NULL,NULL),(125,125,1,2,'2018-04-19 14:21:18',NULL,NULL),(126,126,1,2,'2018-04-19 14:21:18',NULL,NULL),(127,127,1,2,'2018-04-19 14:21:18',NULL,NULL),(128,128,1,2,'2018-04-19 14:21:18',NULL,NULL),(129,129,1,2,'2018-04-19 14:21:18',NULL,NULL),(130,130,1,2,'2018-04-19 14:21:18',NULL,NULL),(131,131,1,2,'2018-04-19 14:21:18',NULL,NULL),(132,132,1,2,'2018-04-19 14:21:18',NULL,NULL),(133,133,1,2,'2018-04-19 14:21:18',NULL,NULL),(134,134,1,2,'2018-04-19 14:21:18',NULL,NULL),(135,135,1,2,'2018-04-19 14:21:18',NULL,NULL),(136,136,1,2,'2018-04-19 14:21:18',NULL,NULL),(137,137,1,2,'2018-04-19 14:21:18',NULL,NULL),(138,138,1,2,'2018-04-19 14:21:18',NULL,NULL),(139,139,1,2,'2018-04-19 14:21:18',NULL,NULL),(140,140,1,2,'2018-04-19 14:21:18',NULL,NULL),(141,141,1,2,'2018-04-19 14:21:19',NULL,NULL),(142,142,1,2,'2018-04-19 14:21:19',NULL,NULL),(143,143,1,2,'2018-04-19 14:21:19',NULL,NULL),(144,144,1,2,'2018-04-19 14:21:19',NULL,NULL),(145,145,1,2,'2018-04-19 14:21:19',NULL,NULL),(146,146,1,2,'2018-04-19 14:21:19',NULL,NULL),(147,147,1,2,'2018-04-19 14:21:19',NULL,NULL),(148,148,1,2,'2018-04-19 14:21:19',NULL,NULL),(149,149,1,2,'2018-04-19 14:21:19',NULL,NULL),(150,150,1,2,'2018-04-19 14:21:19',NULL,NULL),(151,151,1,2,'2018-04-19 14:21:19',NULL,NULL),(152,152,1,2,'2018-04-19 14:21:19',NULL,NULL),(153,153,1,2,'2018-04-19 14:21:19',NULL,NULL),(154,154,1,2,'2018-04-19 14:21:19',NULL,NULL),(155,155,1,2,'2018-04-19 14:21:19',NULL,NULL),(156,156,1,2,'2018-04-19 14:21:19',NULL,NULL),(157,157,1,2,'2018-04-19 14:21:19',NULL,NULL),(158,158,1,2,'2018-04-19 14:21:19',NULL,NULL),(159,159,1,2,'2018-04-19 14:21:19',NULL,NULL),(160,160,1,2,'2018-04-19 14:21:19',NULL,NULL),(161,161,1,2,'2018-04-19 14:21:19',NULL,NULL),(162,162,1,2,'2018-04-19 14:21:19',NULL,NULL),(163,163,1,2,'2018-04-19 14:21:19',NULL,NULL),(164,164,1,2,'2018-04-19 14:21:19',NULL,NULL),(165,165,1,2,'2018-04-19 14:21:19',NULL,NULL),(166,166,1,2,'2018-04-19 14:21:19',NULL,NULL),(167,167,1,2,'2018-04-19 14:21:19',NULL,NULL),(168,168,1,2,'2018-04-19 14:21:19',NULL,NULL),(169,169,1,2,'2018-04-19 14:21:19',NULL,NULL),(170,170,1,2,'2018-04-19 14:21:19',NULL,NULL),(171,171,1,2,'2018-04-19 14:21:19',NULL,NULL),(172,172,1,2,'2018-04-19 14:21:19',NULL,NULL),(173,173,1,2,'2018-04-19 14:21:19',NULL,NULL),(174,174,1,2,'2018-04-19 14:21:19',NULL,NULL),(175,175,1,2,'2018-04-19 14:21:20',NULL,NULL),(176,176,1,2,'2018-04-19 14:21:20',NULL,NULL),(177,177,1,2,'2018-04-19 14:21:20',NULL,NULL),(178,178,1,2,'2018-04-19 14:21:20',NULL,NULL),(179,179,1,1,'2018-04-19 13:21:29','2018-04-19 14:21:29',NULL),(180,180,1,2,'2018-04-19 14:21:20',NULL,NULL),(181,181,1,1,'2018-04-19 13:21:29','2018-04-19 14:21:29',NULL),(182,182,1,1,'2018-04-19 13:21:29','2018-04-19 14:21:29',NULL),(183,183,1,2,'2018-04-19 14:21:20',NULL,NULL),(184,184,1,2,'2018-04-19 14:21:20',NULL,NULL),(185,185,1,2,'2018-04-19 14:21:20',NULL,NULL),(186,186,1,2,'2018-04-19 14:21:20',NULL,NULL),(187,187,1,2,'2018-04-19 14:21:20',NULL,NULL),(188,188,1,2,'2018-04-19 14:21:20',NULL,NULL),(189,189,1,2,'2018-04-19 14:21:20',NULL,NULL),(190,190,1,2,'2018-04-19 14:21:20',NULL,NULL),(191,191,1,2,'2018-04-19 14:21:20',NULL,NULL),(192,192,1,2,'2018-04-19 14:21:20',NULL,NULL),(193,193,1,2,'2018-04-19 14:21:20',NULL,NULL),(194,194,1,2,'2018-04-19 14:21:20',NULL,NULL),(195,195,1,2,'2018-04-19 14:21:20',NULL,NULL),(196,196,1,2,'2018-04-19 14:21:20',NULL,NULL),(197,197,1,2,'2018-04-19 14:21:20',NULL,NULL),(198,198,1,2,'2018-04-19 14:21:20',NULL,NULL),(199,199,1,2,'2018-04-19 14:21:20',NULL,NULL),(200,200,1,2,'2018-04-19 14:21:20',NULL,NULL),(201,201,1,2,'2018-04-19 14:21:20',NULL,NULL),(202,202,1,2,'2018-04-19 14:21:20',NULL,NULL),(203,203,1,2,'2018-04-19 14:21:20',NULL,NULL),(204,204,1,2,'2018-04-19 14:21:20',NULL,NULL),(205,205,1,2,'2018-04-19 14:21:20',NULL,NULL),(206,206,1,2,'2018-04-19 14:21:20',NULL,NULL),(207,207,1,2,'2018-04-19 14:21:20',NULL,NULL),(208,208,1,2,'2018-04-19 14:21:20',NULL,NULL),(209,209,1,2,'2018-04-19 14:21:20',NULL,NULL),(210,210,1,2,'2018-04-19 14:21:20',NULL,NULL),(211,211,1,2,'2018-04-19 14:21:20',NULL,NULL),(212,212,1,2,'2018-04-19 14:21:21',NULL,NULL),(213,213,1,2,'2018-04-19 14:21:21',NULL,NULL),(214,214,1,2,'2018-04-19 14:21:21',NULL,NULL),(215,215,1,2,'2018-04-19 14:21:21',NULL,NULL),(216,216,1,2,'2018-04-19 14:21:21',NULL,NULL),(217,217,1,2,'2018-04-19 14:21:21',NULL,NULL),(218,218,1,2,'2018-04-19 14:21:21',NULL,NULL),(219,219,1,2,'2018-04-19 14:21:21',NULL,NULL),(220,220,1,2,'2018-04-19 14:21:21',NULL,NULL),(221,221,1,2,'2018-04-19 14:21:21',NULL,NULL),(222,222,1,2,'2018-04-19 14:21:21',NULL,NULL),(223,223,1,2,'2018-04-19 14:21:21',NULL,NULL),(224,224,1,2,'2018-04-19 14:21:21',NULL,NULL),(225,225,1,2,'2018-04-19 14:21:21',NULL,NULL),(226,226,1,2,'2018-04-19 14:21:21',NULL,NULL),(227,227,1,2,'2018-04-19 14:21:21',NULL,NULL),(228,228,1,2,'2018-04-19 14:21:21',NULL,NULL),(229,229,1,2,'2018-04-19 14:21:21',NULL,NULL),(230,230,1,2,'2018-04-19 14:21:21',NULL,NULL),(231,231,1,2,'2018-04-19 14:21:21',NULL,NULL),(232,232,1,2,'2018-04-19 14:21:21',NULL,NULL),(233,233,1,2,'2018-04-19 14:21:21',NULL,NULL),(234,234,1,2,'2018-04-19 14:21:21',NULL,NULL),(235,235,1,2,'2018-04-19 14:21:21',NULL,NULL),(236,236,1,2,'2018-04-19 14:21:21',NULL,NULL),(237,237,1,2,'2018-04-19 14:21:21',NULL,NULL),(238,238,1,2,'2018-04-19 14:21:21',NULL,NULL),(239,239,1,2,'2018-04-19 14:21:21',NULL,NULL),(240,240,1,2,'2018-04-19 14:21:21',NULL,NULL),(241,241,1,2,'2018-04-19 14:21:21',NULL,NULL),(242,242,1,2,'2018-04-19 14:21:21',NULL,NULL),(243,243,1,2,'2018-04-19 14:21:22',NULL,NULL),(244,244,1,2,'2018-04-19 14:21:22',NULL,NULL),(245,245,1,2,'2018-04-19 14:21:22',NULL,NULL),(246,246,1,2,'2018-04-19 14:21:22',NULL,NULL),(247,247,1,2,'2018-04-19 14:21:22',NULL,NULL),(248,248,1,2,'2018-04-19 14:21:22',NULL,NULL),(249,249,1,2,'2018-04-19 14:21:22',NULL,NULL),(250,250,1,2,'2018-04-19 14:21:22',NULL,NULL),(251,251,1,2,'2018-04-19 14:21:22',NULL,NULL),(252,252,1,2,'2018-04-19 14:21:22',NULL,NULL),(253,253,1,2,'2018-04-19 14:21:22',NULL,NULL),(254,254,1,2,'2018-04-19 14:21:22',NULL,NULL),(255,255,1,2,'2018-04-19 14:21:22',NULL,NULL),(256,256,1,2,'2018-04-19 14:21:22',NULL,NULL),(257,257,1,2,'2018-04-19 14:21:22',NULL,NULL),(258,258,1,2,'2018-04-19 14:21:23',NULL,NULL),(259,259,1,2,'2018-04-19 14:21:23',NULL,NULL),(260,260,1,2,'2018-04-19 14:21:23',NULL,NULL),(261,261,1,2,'2018-04-19 14:21:23',NULL,NULL),(262,262,1,2,'2018-04-19 14:21:23',NULL,NULL),(263,263,1,2,'2018-04-19 14:21:23',NULL,NULL),(264,264,1,2,'2018-04-19 14:21:23',NULL,NULL),(265,265,1,2,'2018-04-19 14:21:23',NULL,NULL),(266,266,1,2,'2018-04-19 14:21:23',NULL,NULL),(267,267,1,2,'2018-04-19 14:21:23',NULL,NULL),(268,268,1,2,'2018-04-19 14:21:23',NULL,NULL),(269,269,1,2,'2018-04-19 14:21:23',NULL,NULL),(270,270,1,2,'2018-04-19 14:21:23',NULL,NULL),(271,271,1,2,'2018-04-19 14:21:23',NULL,NULL),(272,272,1,2,'2018-04-19 14:21:23',NULL,NULL),(273,273,1,2,'2018-04-19 14:21:23',NULL,NULL),(274,274,1,2,'2018-04-19 14:21:23',NULL,NULL),(275,275,1,2,'2018-04-19 14:21:23',NULL,NULL),(276,276,1,2,'2018-04-19 14:21:23',NULL,NULL),(277,277,1,2,'2018-04-19 14:21:23',NULL,NULL),(278,278,1,2,'2018-04-19 14:21:23',NULL,NULL),(279,279,1,2,'2018-04-19 14:21:23',NULL,NULL),(280,280,1,2,'2018-04-19 14:21:23',NULL,NULL),(281,281,1,2,'2018-04-19 14:21:24',NULL,NULL),(282,282,1,2,'2018-04-19 14:21:24',NULL,NULL),(283,283,1,2,'2018-04-19 14:21:24',NULL,NULL),(284,284,1,2,'2018-04-19 14:21:24',NULL,NULL),(285,285,1,2,'2018-04-19 14:21:24',NULL,NULL),(286,286,1,2,'2018-04-19 14:21:24',NULL,NULL),(287,287,1,2,'2018-04-19 14:21:24',NULL,NULL),(288,288,1,2,'2018-04-19 14:21:24',NULL,NULL),(289,289,1,2,'2018-04-19 14:21:24',NULL,NULL),(290,290,1,2,'2018-04-19 14:21:24',NULL,NULL),(291,291,1,2,'2018-04-19 14:21:24',NULL,NULL),(292,292,1,2,'2018-04-19 14:21:24',NULL,NULL),(293,293,1,2,'2018-04-19 14:21:24',NULL,NULL),(294,294,1,2,'2018-04-19 14:21:24',NULL,NULL),(295,295,1,2,'2018-04-19 14:21:25',NULL,NULL),(296,296,1,2,'2018-04-19 14:21:25',NULL,NULL),(297,297,1,2,'2018-04-19 14:21:25',NULL,NULL),(298,298,1,2,'2018-04-19 14:21:25',NULL,NULL),(299,299,1,2,'2018-04-19 14:21:25',NULL,NULL),(300,300,1,2,'2018-04-19 14:21:25',NULL,NULL),(301,301,1,2,'2018-04-19 14:21:25',NULL,NULL),(302,302,1,2,'2018-04-19 14:21:25',NULL,NULL),(303,303,1,2,'2018-04-19 14:21:25',NULL,NULL),(304,304,1,2,'2018-04-19 14:21:25',NULL,NULL),(305,305,1,2,'2018-04-19 14:21:25',NULL,NULL),(306,306,1,2,'2018-04-19 14:21:25',NULL,NULL),(307,307,1,2,'2018-04-19 14:21:25',NULL,NULL),(308,308,1,2,'2018-04-19 14:21:25',NULL,NULL),(309,309,1,2,'2018-04-19 14:21:25',NULL,NULL),(310,310,1,2,'2018-04-19 14:21:25',NULL,NULL),(311,311,1,2,'2018-04-19 14:21:26',NULL,NULL),(312,312,1,2,'2018-04-19 14:21:26',NULL,NULL),(313,313,1,2,'2018-04-19 14:21:26',NULL,NULL),(314,314,1,2,'2018-04-19 14:21:26',NULL,NULL),(315,315,1,2,'2018-04-19 14:21:26',NULL,NULL),(316,316,1,2,'2018-04-19 14:21:26',NULL,NULL),(317,317,1,2,'2018-04-19 14:21:26',NULL,NULL),(318,318,1,2,'2018-04-19 14:21:26',NULL,NULL),(319,319,1,2,'2018-04-19 14:21:26',NULL,NULL),(320,320,1,2,'2018-04-19 14:21:26',NULL,NULL),(321,321,1,2,'2018-04-19 14:21:26',NULL,NULL),(322,322,1,2,'2018-04-19 14:21:26',NULL,NULL),(323,323,1,2,'2018-04-19 14:21:26',NULL,NULL),(324,324,1,2,'2018-04-19 14:21:26',NULL,NULL),(325,325,1,2,'2018-04-19 14:21:26',NULL,NULL),(326,326,1,2,'2018-04-19 14:21:26',NULL,NULL),(327,327,1,2,'2018-04-19 14:21:26',NULL,NULL),(328,328,1,2,'2018-04-19 14:21:26',NULL,NULL),(329,329,1,2,'2018-04-19 14:21:26',NULL,NULL),(330,330,1,2,'2018-04-19 14:21:26',NULL,NULL),(331,331,1,2,'2018-04-19 14:21:26',NULL,NULL),(332,332,1,2,'2018-04-19 14:21:26',NULL,NULL),(333,333,1,2,'2018-04-19 14:21:27',NULL,NULL),(334,334,1,2,'2018-04-19 14:21:27',NULL,NULL),(335,335,1,2,'2018-04-19 14:21:27',NULL,NULL),(336,336,1,2,'2018-04-19 14:21:27',NULL,NULL),(337,337,1,2,'2018-04-19 14:21:27',NULL,NULL),(338,338,1,2,'2018-04-19 14:21:27',NULL,NULL),(339,339,1,2,'2018-04-19 14:21:27',NULL,NULL),(340,340,1,2,'2018-04-19 14:21:27',NULL,NULL),(341,341,1,2,'2018-04-19 14:21:27',NULL,NULL),(342,342,1,2,'2018-04-19 14:21:27',NULL,NULL),(343,343,1,2,'2018-04-19 14:21:27',NULL,NULL),(344,344,1,2,'2018-04-19 14:21:27',NULL,NULL),(345,345,1,2,'2018-04-19 14:21:27',NULL,NULL),(346,346,1,2,'2018-04-19 14:21:27',NULL,NULL),(347,347,1,2,'2018-04-19 14:21:27',NULL,NULL),(348,348,1,2,'2018-04-19 14:21:27',NULL,NULL),(349,349,1,2,'2018-04-19 14:21:27',NULL,NULL),(350,350,1,2,'2018-04-19 14:21:27',NULL,NULL),(351,351,1,2,'2018-04-19 14:21:27',NULL,NULL),(352,352,1,2,'2018-04-19 14:21:27',NULL,NULL),(353,353,1,2,'2018-04-19 14:21:27',NULL,NULL),(354,354,1,2,'2018-04-19 14:21:27',NULL,NULL),(355,355,1,2,'2018-04-19 14:21:27',NULL,NULL);
/*!40000 ALTER TABLE `default_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `iddepartment` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`iddepartment`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'Administrator','2017-05-17 11:30:14',NULL,'2018-04-19 14:09:06',3),(2,'Test2','2017-05-18 13:57:45','2017-05-18 14:00:38','2017-05-18 14:01:08',3),(3,'Test2','2017-05-18 14:01:51','2017-05-18 14:01:57','2017-05-18 14:02:01',3),(4,'Test2','2017-05-23 12:25:50','2017-05-23 12:26:01','2017-05-23 12:26:06',3),(5,'Test','2017-05-27 13:01:21',NULL,'2017-05-27 13:03:09',3),(6,'Test','2017-05-27 13:03:15',NULL,'2017-05-27 13:04:27',3),(7,'Test2','2017-05-27 13:04:31','2018-04-19 14:08:54',NULL,1),(8,'Departamento 2','2017-06-01 14:11:14',NULL,NULL,1),(9,'TE234234*/=','2017-06-06 12:52:43',NULL,'2017-06-06 12:52:47',3),(10,'Test','2017-06-06 16:22:06',NULL,'2018-04-19 14:09:07',3),(11,'Admin','2018-04-19 14:09:23',NULL,NULL,1);
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deposit`
--

DROP TABLE IF EXISTS `deposit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deposit` (
  `iddeposit` int(11) NOT NULL AUTO_INCREMENT,
  `user_iduser` int(11) NOT NULL,
  `amount` decimal(11,2) NOT NULL,
  `extra_info` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`iddeposit`),
  KEY `fk_deposit_user1_idx` (`user_iduser`),
  CONSTRAINT `fk_deposit_user1` FOREIGN KEY (`user_iduser`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deposit`
--

LOCK TABLES `deposit` WRITE;
/*!40000 ALTER TABLE `deposit` DISABLE KEYS */;
/*!40000 ALTER TABLE `deposit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detached_product`
--

DROP TABLE IF EXISTS `detached_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detached_product` (
  `iddetached_product` int(11) NOT NULL AUTO_INCREMENT,
  `order_idorder` int(11) NOT NULL,
  `value` decimal(11,2) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`iddetached_product`),
  KEY `fk_detached_product_order1_idx` (`order_idorder`),
  CONSTRAINT `fk_detached_product_order1` FOREIGN KEY (`order_idorder`) REFERENCES `order` (`idorder`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detached_product`
--

LOCK TABLES `detached_product` WRITE;
/*!40000 ALTER TABLE `detached_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `detached_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `discount`
--

DROP TABLE IF EXISTS `discount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `discount` (
  `iddiscount` int(11) NOT NULL AUTO_INCREMENT,
  `person_idperson` int(11) DEFAULT NULL,
  `product_idproduct` int(11) DEFAULT NULL,
  `product_gp_idproduct_gp` int(11) DEFAULT NULL,
  `customer_gp_idcustomer_gp` int(11) DEFAULT NULL,
  `percent` decimal(11,2) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`iddiscount`),
  KEY `fk_discount_person1_idx` (`person_idperson`),
  KEY `fk_discount_product1_idx` (`product_idproduct`),
  KEY `fk_discount_product_gp1_idx` (`product_gp_idproduct_gp`),
  KEY `fk_discount_customer_gp1_idx` (`customer_gp_idcustomer_gp`),
  CONSTRAINT `fk_discount_customer_gp1` FOREIGN KEY (`customer_gp_idcustomer_gp`) REFERENCES `customer_gp` (`idcustomer_gp`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_discount_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_discount_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_discount_product_gp1` FOREIGN KEY (`product_gp_idproduct_gp`) REFERENCES `product_gp` (`idproduct_gp`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `discount`
--

LOCK TABLES `discount` WRITE;
/*!40000 ALTER TABLE `discount` DISABLE KEYS */;
/*!40000 ALTER TABLE `discount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee_cards_img_tmpt`
--

DROP TABLE IF EXISTS `employee_cards_img_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_cards_img_tmpt` (
  `idemployee_cards_img_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `img` longblob,
  `active` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`idemployee_cards_img_tmpt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee_cards_img_tmpt`
--

LOCK TABLES `employee_cards_img_tmpt` WRITE;
/*!40000 ALTER TABLE `employee_cards_img_tmpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_cards_img_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exchange_platform`
--

DROP TABLE IF EXISTS `exchange_platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exchange_platform` (
  `idexchange_platform` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idexchange_platform`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exchange_platform`
--

LOCK TABLES `exchange_platform` WRITE;
/*!40000 ALTER TABLE `exchange_platform` DISABLE KEYS */;
/*!40000 ALTER TABLE `exchange_platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feature_in_license`
--

DROP TABLE IF EXISTS `feature_in_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feature_in_license` (
  `idfeaturesinlicense` int(11) NOT NULL AUTO_INCREMENT,
  `license` int(11) NOT NULL,
  `feature` int(11) NOT NULL,
  PRIMARY KEY (`idfeaturesinlicense`),
  KEY `license` (`license`),
  KEY `feature` (`feature`),
  CONSTRAINT `fk_feature` FOREIGN KEY (`feature`) REFERENCES `license_features` (`idfeatures`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_license` FOREIGN KEY (`license`) REFERENCES `license` (`idlicense`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feature_in_license`
--

LOCK TABLES `feature_in_license` WRITE;
/*!40000 ALTER TABLE `feature_in_license` DISABLE KEYS */;
INSERT INTO `feature_in_license` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,1,7),(8,1,8);
/*!40000 ALTER TABLE `feature_in_license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fidelity`
--

DROP TABLE IF EXISTS `fidelity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fidelity` (
  `idfidelity` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `type_fidelity` int(11) NOT NULL,
  `value` decimal(11,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `awards_type` int(11) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`idfidelity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fidelity`
--

LOCK TABLES `fidelity` WRITE;
/*!40000 ALTER TABLE `fidelity` DISABLE KEYS */;
/*!40000 ALTER TABLE `fidelity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `generic_table`
--

DROP TABLE IF EXISTS `generic_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `generic_table` (
  `idgeneric_table` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idgeneric_table`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generic_table`
--

LOCK TABLES `generic_table` WRITE;
/*!40000 ALTER TABLE `generic_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `generic_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_card`
--

DROP TABLE IF EXISTS `gift_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_card` (
  `idgift_card` int(11) NOT NULL AUTO_INCREMENT,
  `person_idperson` int(11) DEFAULT NULL COMMENT 'The person who owns the gift card. This can be null if company_idcompany is not null.',
  `company_idcompany` int(11) DEFAULT NULL COMMENT 'The company who owns the gift card. This can be null if person_idperson is not null.',
  `order_idorder` int(11) NOT NULL,
  `gift_card_tmpt_idgift_card_tmpt` int(11) DEFAULT NULL,
  `gift_card_identify` varchar(255) DEFAULT NULL,
  `extra_info` longtext COMMENT 'OBS in the gift card',
  `value` decimal(11,2) NOT NULL DEFAULT '0.00',
  `value_used` decimal(11,2) NOT NULL DEFAULT '0.00',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idgift_card`),
  KEY `fk_gift_card_person1_idx` (`person_idperson`),
  KEY `fk_gift_card_company1_idx` (`company_idcompany`),
  KEY `fk_gift_card_gift_card_tmpt1_idx` (`gift_card_tmpt_idgift_card_tmpt`),
  CONSTRAINT `fk_gift_card_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gift_card_gift_card_tmpt1` FOREIGN KEY (`gift_card_tmpt_idgift_card_tmpt`) REFERENCES `gift_card_tmpt` (`idgift_card_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gift_card_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_card`
--

LOCK TABLES `gift_card` WRITE;
/*!40000 ALTER TABLE `gift_card` DISABLE KEYS */;
INSERT INTO `gift_card` VALUES (1,NULL,NULL,147,NULL,'123',NULL,100.00,0.00,'2018-07-24 15:19:46',NULL,NULL,1);
/*!40000 ALTER TABLE `gift_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift_card_tmpt`
--

DROP TABLE IF EXISTS `gift_card_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift_card_tmpt` (
  `idgift_card_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `info` longtext COMMENT 'A text to be printed in gift cards.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idgift_card_tmpt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift_card_tmpt`
--

LOCK TABLES `gift_card_tmpt` WRITE;
/*!40000 ALTER TABLE `gift_card_tmpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `gift_card_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hall`
--

DROP TABLE IF EXISTS `hall`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hall` (
  `idhall` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_idwarehouse` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `position_top` int(11) NOT NULL,
  `position_left` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idhall`),
  KEY `fk_hall_warehouse1_idx` (`warehouse_idwarehouse`),
  CONSTRAINT `fk_hall_warehouse1` FOREIGN KEY (`warehouse_idwarehouse`) REFERENCES `warehouse` (`idwarehouse`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hall`
--

LOCK TABLES `hall` WRITE;
/*!40000 ALTER TABLE `hall` DISABLE KEYS */;
INSERT INTO `hall` VALUES (1,1,'',152,117,65,33,'2017-05-17 14:46:56',NULL,NULL,1),(2,1,'',249,151,65,33,'2017-05-18 17:45:19',NULL,NULL,1),(3,1,'',249,151,65,33,'2017-05-25 14:00:05',NULL,NULL,1),(4,1,'',245,147,65,33,'2017-05-25 14:00:06',NULL,NULL,1),(5,1,'',245,147,65,33,'2018-08-06 14:18:47',NULL,NULL,1),(6,1,'',245,147,65,33,'2018-08-06 14:18:47',NULL,NULL,1);
/*!40000 ALTER TABLE `hall` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `importation_settings`
--

DROP TABLE IF EXISTS `importation_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `importation_settings` (
  `id_importation_settings` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `file` longtext,
  PRIMARY KEY (`id_importation_settings`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `importation_settings`
--

LOCK TABLES `importation_settings` WRITE;
/*!40000 ALTER TABLE `importation_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `importation_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `install`
--

DROP TABLE IF EXISTS `install`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `install` (
  `idinstall` int(11) NOT NULL AUTO_INCREMENT,
  `srv` varchar(200) NOT NULL,
  `macAddress` varchar(250) NOT NULL,
  `nickname` varchar(250) NOT NULL,
  `debug` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_delete` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '0 = not installed 1 = installed 2 = remove',
  PRIMARY KEY (`idinstall`),
  UNIQUE KEY `keysrv` (`srv`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `install`
--

LOCK TABLES `install` WRITE;
/*!40000 ALTER TABLE `install` DISABLE KEYS */;
INSERT INTO `install` VALUES (1,'demo','demo','PC-ASD4','','2017-05-17 11:30:14',NULL,0),(30,'C02K36EEFFT0','14:10:9f:e3:bf:fb','PC-EFFT0','test','2017-05-04 23:43:33',NULL,1);
/*!40000 ALTER TABLE `install` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `integration`
--

DROP TABLE IF EXISTS `integration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `integration` (
  `idintegration` int(11) NOT NULL AUTO_INCREMENT,
  `exchange_platform` varchar(150) DEFAULT NULL,
  `exchange_user` varchar(200) DEFAULT NULL,
  `exchange_password` varchar(200) DEFAULT NULL,
  `address_number` int(11) DEFAULT NULL,
  `address_message_return` varchar(255) DEFAULT NULL,
  `bestseller_file` varchar(150) DEFAULT NULL,
  `bestseller_server` varchar(150) DEFAULT NULL,
  `bestseller_user` varchar(45) DEFAULT NULL,
  `bestseller_password` varchar(150) DEFAULT NULL,
  `bestseller_port` int(5) DEFAULT NULL,
  `bestseller_catalog` varchar(150) DEFAULT NULL,
  `barcode_file` varchar(150) DEFAULT NULL,
  `barcode_customerno` int(11) DEFAULT NULL,
  `barcode_password` varchar(150) DEFAULT NULL,
  `barcode_review` tinyint(1) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idintegration`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `integration`
--

LOCK TABLES `integration` WRITE;
/*!40000 ALTER TABLE `integration` DISABLE KEYS */;
INSERT INTO `integration` VALUES (1,'dsadad','user3','3fc26dc90ab29',NULL,NULL,NULL,'local','user','pass',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `integration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `idinvoice` int(11) NOT NULL AUTO_INCREMENT,
  `order_idorder` int(11) NOT NULL,
  `invoice_tmpt_idinvoice_tmpt` int(11) NOT NULL,
  `id` int(11) NOT NULL COMMENT 'This is the ID used in the company, not the ID of the table.',
  `extra_info` longtext COMMENT 'OBS in the invoice.',
  `printed` timestamp NULL DEFAULT NULL COMMENT 'When the invoice was printed for the last time.',
  `date_due` timestamp NULL DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `limit_date_discount` timestamp NULL DEFAULT NULL,
  `status_bank` int(11) NOT NULL,
  `status_payment` int(11) NOT NULL,
  `value_payment` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`idinvoice`),
  KEY `fk_invoice_order1_idx` (`order_idorder`),
  KEY `fk_invoice_invoice_tmpt1_idx` (`invoice_tmpt_idinvoice_tmpt`),
  CONSTRAINT `fk_invoice_invoice_tmpt1` FOREIGN KEY (`invoice_tmpt_idinvoice_tmpt`) REFERENCES `invoice_tmpt` (`idinvoice_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoice_order1` FOREIGN KEY (`order_idorder`) REFERENCES `order` (`idorder`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_printer`
--

DROP TABLE IF EXISTS `invoice_printer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_printer` (
  `idinvoice_printer` int(11) NOT NULL,
  `text_include_cashier` longtext,
  `size_font` int(11) DEFAULT NULL,
  `description_printed` varchar(45) DEFAULT NULL,
  `number_copies` int(11) DEFAULT NULL,
  `number_decimal_places` int(11) DEFAULT NULL,
  `print_drawing_of_color` int(11) DEFAULT NULL,
  `enter_the_customers_phone_number` int(11) DEFAULT NULL,
  `change_the_location` int(11) DEFAULT NULL,
  `print_taxes_included` int(11) DEFAULT NULL,
  `do_not_print_project_information` int(11) DEFAULT NULL,
  `use_the_default_printer_instead` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_printer`
--

LOCK TABLES `invoice_printer` WRITE;
/*!40000 ALTER TABLE `invoice_printer` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_printer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_tmpt`
--

DROP TABLE IF EXISTS `invoice_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_tmpt` (
  `idinvoice_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `tmpt_header_idtmpt_header` int(11) NOT NULL,
  `tmpt_footer_idtmpt_footer` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `info` longtext COMMENT 'A text to be printed in the invoice.',
  `font_size` int(11) NOT NULL DEFAULT '12',
  `alternate_color_line` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Print lines with alternate colors (zebra).',
  `print_customer_phone` tinyint(1) NOT NULL DEFAULT '0',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idinvoice_tmpt`),
  KEY `fk_invoice_tmpt_tmpt_footer1_idx` (`tmpt_footer_idtmpt_footer`),
  KEY `fk_invoice_tmpt_tmpt_header1_idx` (`tmpt_header_idtmpt_header`),
  CONSTRAINT `fk_invoice_tmpt_tmpt_footer1` FOREIGN KEY (`tmpt_footer_idtmpt_footer`) REFERENCES `tmpt_footer` (`idtmpt_footer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invoice_tmpt_tmpt_header1` FOREIGN KEY (`tmpt_header_idtmpt_header`) REFERENCES `tmpt_header` (`idtmpt_header`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_tmpt`
--

LOCK TABLES `invoice_tmpt` WRITE;
/*!40000 ALTER TABLE `invoice_tmpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job`
--

DROP TABLE IF EXISTS `job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job` (
  `idjob` int(11) NOT NULL AUTO_INCREMENT,
  `department_iddepartment` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idjob`),
  KEY `fk_job_department1_idx` (`department_iddepartment`),
  CONSTRAINT `fk_job_department1` FOREIGN KEY (`department_iddepartment`) REFERENCES `department` (`iddepartment`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job`
--

LOCK TABLES `job` WRITE;
/*!40000 ALTER TABLE `job` DISABLE KEYS */;
INSERT INTO `job` VALUES (1,1,'Admin','2017-05-17 11:30:14',NULL,NULL,1),(2,1,'Test2','2017-05-18 14:03:53','2017-05-18 14:03:58','2017-05-18 14:04:03',3),(3,1,'Test','2017-05-18 14:04:08',NULL,NULL,1),(4,8,'test4','2018-04-19 14:27:09','2018-04-19 14:27:26','2018-04-19 14:28:32',3);
/*!40000 ALTER TABLE `job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `idlanguage` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idlanguage`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'english','2017-08-15 23:52:11',NULL,NULL,0),(2,'portuguese','2017-10-18 16:26:52',NULL,NULL,1),(3,'spanish','2017-10-18 16:53:23',NULL,NULL,0);
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_custom`
--

DROP TABLE IF EXISTS `language_custom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_custom` (
  `idlanguagecustom` int(11) NOT NULL AUTO_INCREMENT,
  `language_derivative` int(11) NOT NULL,
  `data_type` text NOT NULL,
  `data_value` text NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idlanguagecustom`),
  KEY `language_derivative_index` (`language_derivative`),
  CONSTRAINT `fk_language_2` FOREIGN KEY (`language_derivative`) REFERENCES `language` (`idlanguage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_custom`
--

LOCK TABLES `language_custom` WRITE;
/*!40000 ALTER TABLE `language_custom` DISABLE KEYS */;
/*!40000 ALTER TABLE `language_custom` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_custom_apply`
--

DROP TABLE IF EXISTS `language_custom_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_custom_apply` (
  `idlanguagecustomapply` int(11) NOT NULL AUTO_INCREMENT,
  `languagecustomid` int(11) NOT NULL,
  `idpos` int(11) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idlanguagecustomapply`),
  KEY `languagecustomid_inx` (`languagecustomid`),
  KEY `idpos_inx` (`idpos`),
  CONSTRAINT `fk_language_3` FOREIGN KEY (`languagecustomid`) REFERENCES `language_custom` (`idlanguagecustom`),
  CONSTRAINT `fk_pos_2` FOREIGN KEY (`idpos`) REFERENCES `pos` (`idpos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_custom_apply`
--

LOCK TABLES `language_custom_apply` WRITE;
/*!40000 ALTER TABLE `language_custom_apply` DISABLE KEYS */;
/*!40000 ALTER TABLE `language_custom_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_data`
--

DROP TABLE IF EXISTS `language_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_data` (
  `idlanguagedata` int(11) NOT NULL AUTO_INCREMENT,
  `idlanguage` int(11) NOT NULL,
  `page` int(11) NOT NULL,
  `data_type` int(11) NOT NULL,
  `data_value` int(11) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idlanguagedata`),
  KEY `language_index` (`idlanguage`),
  CONSTRAINT `fk_language_1` FOREIGN KEY (`idlanguage`) REFERENCES `language` (`idlanguage`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_data`
--

LOCK TABLES `language_data` WRITE;
/*!40000 ALTER TABLE `language_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `language_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license`
--

DROP TABLE IF EXISTS `license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `license` (
  `idlicense` int(11) NOT NULL AUTO_INCREMENT,
  `key_license` varchar(200) NOT NULL,
  `validad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `limit_users` int(11) NOT NULL,
  `limit_connections` int(11) NOT NULL,
  `limit_terminals` int(11) NOT NULL,
  `limit_mobiles` int(11) NOT NULL,
  `number_of_companies` int(11) DEFAULT '1',
  `update_sync` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idlicense`),
  UNIQUE KEY `key_license` (`key_license`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license`
--

LOCK TABLES `license` WRITE;
/*!40000 ALTER TABLE `license` DISABLE KEYS */;
INSERT INTO `license` VALUES (1,'H6DF-8G3F-GJ36-557F-D88G','2017-05-30 18:22:10',10,10,1,1,1,NULL);
/*!40000 ALTER TABLE `license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `license_features`
--

DROP TABLE IF EXISTS `license_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `license_features` (
  `idfeatures` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`idfeatures`),
  KEY `indx_features` (`idfeatures`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `license_features`
--

LOCK TABLES `license_features` WRITE;
/*!40000 ALTER TABLE `license_features` DISABLE KEYS */;
INSERT INTO `license_features` VALUES (1,'Sales'),(2,'Invoicing'),(3,'Point of Sale'),(4,'Accounting'),(5,'Project'),(6,'Inventory'),(7,'Purchase'),(8,'Manufacturing'),(9,'eCommerce'),(10,'Delivery'),(11,'iBeacon'),(12,'Facial Recognition'),(13,'Tracking'),(14,'RFID'),(15,'Wifi');
/*!40000 ALTER TABLE `license_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `idlog` int(11) NOT NULL AUTO_INCREMENT,
  `webshop_idwebshop` int(11) DEFAULT NULL COMMENT 'The ID of the webshop which has launched the alert (in case a webshop has done it).',
  `pos_idpos` int(11) DEFAULT NULL COMMENT 'The ID of the POS which has launched the alert (in case a POS has done it).',
  `log_type_idlog_type` int(11) DEFAULT NULL,
  `info` mediumtext NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idlog`),
  KEY `fk_log_webshop1_idx` (`webshop_idwebshop`),
  KEY `fk_log_pos1_idx` (`pos_idpos`),
  KEY `fk_log_log_type1_idx` (`log_type_idlog_type`),
  CONSTRAINT `fk_log_log_type1` FOREIGN KEY (`log_type_idlog_type`) REFERENCES `log_type` (`idlog_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_log_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_log_webshop1` FOREIGN KEY (`webshop_idwebshop`) REFERENCES `webshop` (`idwebshop`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=503 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` VALUES (1,NULL,NULL,NULL,'Logged in with the user admin','2017-05-23 13:12:45',NULL,NULL,1),(2,NULL,NULL,NULL,'Logged in with the user admin','2017-05-23 13:30:08',NULL,NULL,1),(3,NULL,NULL,NULL,'Logged in with the user admin','2017-05-26 12:30:28',NULL,NULL,1),(4,NULL,NULL,NULL,'Logged in with the user admin','2017-05-27 12:55:55',NULL,NULL,1),(5,NULL,NULL,NULL,'Creation Test item in entry by admin','2017-05-27 13:04:31',NULL,NULL,1),(6,NULL,NULL,NULL,'Updating Test item in project by admin','2017-05-27 14:39:21',NULL,NULL,1),(7,NULL,NULL,NULL,'Logged in with the user admin','2017-05-27 22:22:41',NULL,NULL,1),(8,NULL,NULL,NULL,'Logged in with the user admin','2017-05-29 14:22:59',NULL,NULL,1),(9,NULL,NULL,NULL,'Logged in with the user admin','2017-05-29 15:03:53',NULL,NULL,1),(10,NULL,NULL,NULL,'Logged in with the user admin','2017-05-29 15:06:11',NULL,NULL,1),(11,NULL,NULL,NULL,'Logged in with the user admin','2017-05-29 15:08:37',NULL,NULL,1),(12,NULL,NULL,NULL,'Logged in with the user admin','2017-05-29 15:12:02',NULL,NULL,1),(13,NULL,NULL,NULL,'Creation test2 item in layout for terminal by admin','2017-05-30 01:30:30',NULL,NULL,1),(14,NULL,NULL,NULL,'Creation test2 item in layout for terminal by admin','2017-05-30 01:32:04',NULL,NULL,1),(15,NULL,NULL,NULL,'Creation Test3 item in layout for terminal by admin','2017-05-30 01:33:50',NULL,NULL,1),(16,NULL,NULL,NULL,'Creation Test3 item in layout for terminal by admin','2017-05-30 01:33:55',NULL,NULL,1),(17,NULL,NULL,NULL,'Creation Test3 item in layout for terminal by admin','2017-05-30 01:34:01',NULL,NULL,1),(18,NULL,NULL,NULL,'Deleting Test3 item in layout for terminal by admin','2017-05-30 01:37:56',NULL,NULL,1),(19,NULL,NULL,NULL,'Creation Test3 item in layout for terminal by admin','2017-05-30 01:39:49',NULL,NULL,1),(20,NULL,NULL,NULL,'Creation Test  item in layout for terminal by admin','2017-05-30 01:41:44',NULL,NULL,1),(21,NULL,NULL,NULL,'Logged in with the user admin','2017-05-30 12:22:04',NULL,NULL,1),(22,NULL,NULL,NULL,'Logged in with the user admin','2017-05-31 14:13:00',NULL,NULL,1),(23,NULL,NULL,NULL,'Logged in with the user admin','2017-05-31 14:19:49',NULL,NULL,1),(24,NULL,NULL,NULL,'Logged in with the user admin','2017-05-31 18:03:43',NULL,NULL,1),(25,NULL,NULL,NULL,'Logged in with the user admin','2017-05-31 18:12:18',NULL,NULL,1),(26,NULL,NULL,NULL,'Logged in with the user admin','2017-05-31 18:12:54',NULL,NULL,1),(27,NULL,NULL,NULL,'Logged in with the user admin','2017-05-31 18:14:53',NULL,NULL,1),(28,NULL,NULL,NULL,'Logged in with the user admin','2017-05-31 18:23:28',NULL,NULL,1),(29,NULL,NULL,NULL,'Logged in with the user admin','2017-06-01 13:43:40',NULL,NULL,1),(30,NULL,NULL,NULL,'Logged in with the user admin','2017-06-01 14:03:23',NULL,NULL,1),(31,NULL,NULL,NULL,'Creation Departamento 2 item in department by admin','2017-06-01 14:11:14',NULL,NULL,1),(32,NULL,NULL,NULL,'Creation G item in product size by admin','2017-06-01 14:12:31',NULL,NULL,1),(33,NULL,NULL,NULL,'Creation P item in product size by admin','2017-06-01 14:12:38',NULL,NULL,1),(34,NULL,NULL,NULL,'Creation M item in product size by admin','2017-06-01 14:12:42',NULL,NULL,1),(35,NULL,NULL,NULL,'Creation Red item in color by admin','2017-06-01 14:12:50',NULL,NULL,1),(36,NULL,NULL,NULL,'Creation Green item in color by admin','2017-06-01 14:12:54',NULL,NULL,1),(37,NULL,NULL,NULL,'Creation Blue item in color by admin','2017-06-01 14:12:59',NULL,NULL,1),(38,NULL,NULL,NULL,'Creation Test item in serial number by admin','2017-06-01 14:18:33',NULL,NULL,1),(39,NULL,NULL,NULL,'Deleting Test item in serial number by admin','2017-06-01 14:18:36',NULL,NULL,1),(40,NULL,NULL,NULL,'Logged in with the user admin','2017-06-01 14:25:03',NULL,NULL,1),(41,NULL,NULL,NULL,'Logged in with the user admin','2017-06-01 15:26:39',NULL,NULL,1),(42,NULL,NULL,NULL,'Logged in with the user admin','2017-06-01 21:21:23',NULL,NULL,1),(43,NULL,NULL,NULL,'User admin logged in.','2017-06-01 21:55:30',NULL,NULL,1),(44,NULL,NULL,NULL,'Logged in with the user admin','2017-06-01 21:56:31',NULL,NULL,1),(45,NULL,NULL,NULL,'Logged in with the user admin','2017-06-02 12:22:25',NULL,NULL,1),(46,NULL,NULL,NULL,'User admin logged in.','2017-06-02 12:22:58',NULL,NULL,1),(47,NULL,NULL,NULL,'Logged in with the user admin','2017-06-02 12:23:10',NULL,NULL,1),(48,NULL,NULL,NULL,'User admin logged in.','2017-06-02 12:23:27',NULL,NULL,1),(49,NULL,NULL,NULL,'Logged in with the user admin','2017-06-02 12:48:42',NULL,NULL,1),(50,NULL,NULL,NULL,'Logged in with the user admin','2017-06-02 13:11:35',NULL,NULL,1),(51,NULL,NULL,NULL,'Logged in with the user admin','2017-06-02 13:26:42',NULL,NULL,1),(52,NULL,NULL,NULL,'Logged in with the user admin','2017-06-02 13:32:34',NULL,NULL,1),(53,NULL,NULL,NULL,'User admin logged in.','2017-06-02 13:33:43',NULL,NULL,1),(54,NULL,NULL,NULL,'Logged in with the user admin','2017-06-02 13:33:47',NULL,NULL,1),(55,NULL,NULL,NULL,'Logged in with the user admin','2017-06-05 12:54:01',NULL,NULL,1),(56,NULL,NULL,NULL,'Logged in with the user admin','2017-06-05 18:06:15',NULL,NULL,1),(57,NULL,NULL,NULL,'Logged in with the user admin','2017-06-05 19:07:27',NULL,NULL,1),(58,NULL,NULL,NULL,'User admin logged in.','2017-06-05 19:43:05',NULL,NULL,1),(59,NULL,NULL,NULL,'Logged in with the user admin','2017-06-05 19:44:44',NULL,NULL,1),(60,NULL,NULL,NULL,'Logged in with the user admin','2017-06-06 12:52:15',NULL,NULL,1),(61,NULL,NULL,NULL,'Creation TE234234*/= item in department by admin','2017-06-06 12:52:43',NULL,NULL,1),(62,NULL,NULL,NULL,'Deleting TE234234*/= item in department by admin','2017-06-06 12:52:47',NULL,NULL,1),(63,NULL,NULL,NULL,'Logged in with the user admin','2017-06-06 13:38:07',NULL,NULL,1),(64,NULL,NULL,NULL,'User admin logged in.','2017-06-06 16:18:21',NULL,NULL,1),(65,NULL,NULL,NULL,'Logged in with the user admin','2017-06-06 16:18:28',NULL,NULL,1),(66,NULL,NULL,NULL,'Logged in with the user admin','2017-06-06 16:19:22',NULL,NULL,1),(67,NULL,NULL,NULL,'Logged in with the user admin','2017-06-06 16:21:41',NULL,NULL,1),(68,NULL,NULL,NULL,'Creation Test item in department by admin','2017-06-06 16:22:06',NULL,NULL,1),(69,NULL,NULL,NULL,'User admin logged in.','2017-06-06 16:22:12',NULL,NULL,1),(70,NULL,NULL,NULL,'Logged in with the user admin','2017-06-06 17:46:29',NULL,NULL,1),(71,NULL,NULL,NULL,'Logged in with the user admin','2017-06-16 17:18:39',NULL,NULL,1),(72,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 11:47:42',NULL,NULL,1),(73,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 13:50:56',NULL,NULL,1),(74,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 14:43:38',NULL,NULL,1),(75,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 14:49:42',NULL,NULL,1),(76,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 14:51:23',NULL,NULL,1),(77,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 15:31:39',NULL,NULL,1),(78,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 15:33:39',NULL,NULL,1),(79,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 15:39:45',NULL,NULL,1),(80,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 15:42:10',NULL,NULL,1),(81,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 15:42:11',NULL,NULL,1),(82,NULL,NULL,NULL,'Logged in with the user admin','2017-06-19 16:49:31',NULL,NULL,1),(83,NULL,NULL,NULL,'Logged in with the user admin','2017-06-20 18:32:52',NULL,NULL,1),(84,NULL,NULL,NULL,'Logged in with the user admin','2017-07-20 13:58:27',NULL,NULL,1),(85,NULL,NULL,NULL,'Logged in with the user admin','2017-07-24 18:44:46',NULL,NULL,1),(86,NULL,NULL,NULL,'Logged in with the user admin','2017-08-14 18:41:28',NULL,NULL,1),(87,NULL,NULL,NULL,'Logged in with the user admin','2017-08-23 13:08:41',NULL,NULL,1),(88,NULL,NULL,NULL,'Logged in with the user admin','2017-08-23 15:01:08',NULL,NULL,1),(89,NULL,NULL,NULL,'Logged in with the user admin','2017-08-23 18:46:34',NULL,NULL,1),(90,NULL,NULL,NULL,'Logged in with the user admin','2017-08-24 13:08:17',NULL,NULL,1),(91,NULL,NULL,NULL,'Logged in with the user admin','2017-09-08 15:29:31',NULL,NULL,1),(92,NULL,NULL,NULL,'Logged in with the user admin','2017-09-08 16:23:42',NULL,NULL,1),(93,NULL,NULL,NULL,'Logged in with the user admin','2017-09-08 16:24:25',NULL,NULL,1),(94,NULL,NULL,NULL,'User admin logged in.','2017-09-11 08:04:15',NULL,NULL,1),(95,NULL,NULL,NULL,'Logged in with the user admin','2017-09-21 18:40:17',NULL,NULL,1),(96,NULL,NULL,NULL,'Logged in with the user admin','2017-10-09 20:18:02',NULL,NULL,1),(97,NULL,NULL,NULL,'Logged in with the user admin','2017-10-09 20:27:09',NULL,NULL,1),(98,NULL,NULL,NULL,'Logged in with the user admin','2017-10-24 19:34:49',NULL,NULL,1),(99,NULL,NULL,NULL,'Logged in with the user admin','2017-10-24 19:34:51',NULL,NULL,1),(100,NULL,NULL,NULL,'Logged in with the user admin','2017-10-24 19:35:07',NULL,NULL,1),(101,NULL,NULL,NULL,'Logged in with the user admin','2017-11-07 18:29:46',NULL,NULL,1),(102,NULL,NULL,NULL,'Logged in with the user admin','2017-11-07 22:44:48',NULL,NULL,1),(103,NULL,NULL,NULL,'Logged in with the user admin','2017-11-07 23:18:25',NULL,NULL,1),(104,NULL,NULL,NULL,'Logged in with the user admin','2017-11-09 02:24:30',NULL,NULL,1),(105,NULL,NULL,NULL,'User admin logged in.','2017-11-09 02:27:34',NULL,NULL,1),(106,NULL,NULL,NULL,'Logged in with the user admin','2017-11-13 19:44:12',NULL,NULL,1),(107,NULL,NULL,NULL,'Logged in with the user admin','2017-11-13 22:09:35',NULL,NULL,1),(108,NULL,NULL,NULL,'Logged in with the user admin','2017-11-20 22:04:55',NULL,NULL,1),(109,NULL,NULL,NULL,'Logged in with the user admin','2017-11-20 22:05:52',NULL,NULL,1),(110,NULL,NULL,NULL,'Logged in with the user admin','2017-11-29 15:47:40',NULL,NULL,1),(111,NULL,NULL,NULL,'Logged in with the user admin','2017-11-29 20:43:48',NULL,NULL,1),(112,NULL,NULL,NULL,'Logged in with the user admin','2017-11-29 20:58:07',NULL,NULL,1),(113,NULL,NULL,NULL,'Logged in with the user admin','2017-11-30 14:03:46',NULL,NULL,1),(114,NULL,NULL,NULL,'Logged in with the user admin','2018-01-26 22:42:20',NULL,NULL,1),(115,NULL,NULL,NULL,'Logged in with the user admin','2018-02-08 22:38:47',NULL,NULL,1),(116,NULL,NULL,NULL,'Logged in with the user admin','2018-02-08 22:41:08',NULL,NULL,1),(117,NULL,NULL,NULL,'Logged in with the user admin','2018-02-19 22:21:00',NULL,NULL,1),(118,NULL,NULL,NULL,'Logged in with the user admin','2018-04-19 13:45:11',NULL,NULL,1),(119,NULL,NULL,NULL,'Updating Test2 item in department by admin','2018-04-19 14:08:54',NULL,NULL,1),(120,NULL,NULL,NULL,'Deleting Administrator item in department by admin','2018-04-19 14:09:06',NULL,NULL,1),(121,NULL,NULL,NULL,'Deleting Test item in department by admin','2018-04-19 14:09:07',NULL,NULL,1),(122,NULL,NULL,NULL,'Creation Admin item in department by admin','2018-04-19 14:09:23',NULL,NULL,1),(123,NULL,NULL,NULL,'Creation Test2 item in project by admin','2018-04-19 14:10:48',NULL,NULL,1),(124,NULL,NULL,NULL,'Updating Test2 item in project by admin','2018-04-19 14:11:09',NULL,NULL,1),(125,NULL,NULL,NULL,'Deleting Test2 item in project by admin','2018-04-19 14:12:02',NULL,NULL,1),(126,NULL,NULL,NULL,'Creation teste item in user by admin','2018-04-19 14:13:49',NULL,NULL,1),(127,NULL,NULL,NULL,'Deleting teste item in user by admin','2018-04-19 14:14:28',NULL,NULL,1),(128,NULL,NULL,NULL,'Updating admin item in permissions by admin','2018-04-19 14:16:44',NULL,NULL,1),(129,NULL,NULL,NULL,'Updating admin item in permissions by admin','2018-04-19 14:17:12',NULL,NULL,1),(130,NULL,NULL,NULL,'Updating admin item in permissions by admin','2018-04-19 14:17:51',NULL,NULL,1),(131,NULL,NULL,NULL,'Updating admin item in permissions by admin','2018-04-19 14:21:28',NULL,NULL,1),(132,NULL,NULL,NULL,'Updating admin item in permissions by admin','2018-04-19 14:21:30',NULL,NULL,1),(133,NULL,NULL,NULL,'Creation test2 item in type user by admin','2018-04-19 14:24:18',NULL,NULL,1),(134,NULL,NULL,NULL,'Updating test3 item in type user by admin','2018-04-19 14:24:29',NULL,NULL,1),(135,NULL,NULL,NULL,'Deleting test3 item in type user by admin','2018-04-19 14:24:37',NULL,NULL,1),(136,NULL,NULL,NULL,'Updating Test4 item in type user by admin','2018-04-19 14:25:30',NULL,NULL,1),(137,NULL,NULL,NULL,'Creation test3 item in job by admin','2018-04-19 14:27:09',NULL,NULL,1),(138,NULL,NULL,NULL,'Updating test4 item in job by admin','2018-04-19 14:27:26',NULL,NULL,1),(139,NULL,NULL,NULL,'Deleting test4 item in job by admin','2018-04-19 14:28:32',NULL,NULL,1),(140,NULL,NULL,NULL,'Updating S item in product size by admin','2018-04-19 14:36:29',NULL,NULL,1),(141,NULL,NULL,NULL,'Updating P item in product size by admin','2018-04-19 14:36:45',NULL,NULL,1),(142,NULL,NULL,NULL,'Creation S item in product size by admin','2018-04-19 14:36:56',NULL,NULL,1),(143,NULL,NULL,NULL,'Deleting S item in product size by admin','2018-04-19 14:37:46',NULL,NULL,1),(144,NULL,NULL,NULL,'Creation Gray item in color by admin','2018-04-19 14:38:26',NULL,NULL,1),(145,NULL,NULL,NULL,'Updating Pink item in project by admin','2018-04-19 14:38:38',NULL,NULL,1),(146,NULL,NULL,NULL,'Deleting Pink item in color by admin','2018-04-19 14:38:52',NULL,NULL,1),(147,NULL,NULL,NULL,'Updating Default item in template by admin','2018-04-19 14:44:09',NULL,NULL,1),(148,NULL,NULL,NULL,'Updating Default item in template by admin','2018-04-19 14:44:18',NULL,NULL,1),(149,NULL,NULL,NULL,'Creation 33 item in ProductTemp by admin','2018-04-19 14:52:02',NULL,NULL,1),(150,NULL,NULL,NULL,'Creation jghjgjghjghjghgh item in ProductTemp by admin','2018-04-19 14:53:02',NULL,NULL,1),(151,NULL,NULL,NULL,'Creation jghjgjghjghjghgh item in ProductTemp by admin','2018-04-19 14:54:04',NULL,NULL,1),(152,NULL,NULL,NULL,'Creation 34 item in ProductTemp by admin','2018-04-19 14:54:29',NULL,NULL,1),(153,NULL,NULL,NULL,'Creation test item in Productgroup by admin','2018-04-19 14:57:03',NULL,NULL,1),(154,NULL,NULL,NULL,'Updating 3 item in Productgroup by admin','2018-04-19 14:57:17',NULL,NULL,1),(155,NULL,NULL,NULL,'Deleting Array item in Productgroup by admin','2018-04-19 14:57:23',NULL,NULL,1),(156,NULL,NULL,NULL,'Deleting Array item in Productgroup by admin','2018-04-19 14:57:29',NULL,NULL,1),(157,NULL,NULL,NULL,'Creation test item in ProductUnit by admin','2018-04-19 14:58:32',NULL,NULL,1),(158,NULL,NULL,NULL,'Creation fsdfds item in ProductRule by admin','2018-04-19 15:01:07',NULL,NULL,1),(159,NULL,NULL,NULL,'Updating fsdfds item in ProductRule by admin','2018-04-19 15:01:16',NULL,NULL,1),(160,NULL,NULL,NULL,'Deleting Array item in ProductRule by admin','2018-04-19 15:01:24',NULL,NULL,1),(161,NULL,NULL,NULL,'Creation test item in ProductManufacturer by admin','2018-04-19 15:03:00',NULL,NULL,1),(162,NULL,NULL,NULL,'Updating test item in ProductManufacturer by admin','2018-04-19 15:03:26',NULL,NULL,1),(163,NULL,NULL,NULL,'Deleting Array item in ProductManufacturer by admin','2018-04-19 15:03:33',NULL,NULL,1),(164,NULL,NULL,NULL,'Creation 666 item in serial number by admin','2018-04-19 15:04:45',NULL,NULL,1),(165,NULL,NULL,NULL,'Updating 665 item in serial number by admin','2018-04-19 15:04:57',NULL,NULL,1),(166,NULL,NULL,NULL,'Deleting 665 item in serial number by admin','2018-04-19 15:05:03',NULL,NULL,1),(167,NULL,NULL,NULL,'Creation fgfdfg item in shipping method by admin','2018-04-19 15:08:42',NULL,NULL,1),(168,NULL,NULL,NULL,'Updating aaa item in shipping method by admin','2018-04-19 15:09:05',NULL,NULL,1),(169,NULL,NULL,NULL,'Updating aaa item in shipping method by admin','2018-04-19 15:09:19',NULL,NULL,1),(170,NULL,NULL,NULL,'Deleting aaa item in shipping method by admin','2018-04-19 15:09:26',NULL,NULL,1),(171,NULL,NULL,NULL,'Creation yyutuy item in tax by admin','2018-04-19 15:12:17',NULL,NULL,1),(172,NULL,NULL,NULL,'Updating aaa item in tax by admin','2018-04-19 15:12:54',NULL,NULL,1),(173,NULL,NULL,NULL,'Deleting aaa item in tax by admin','2018-04-19 15:13:01',NULL,NULL,1),(174,NULL,NULL,NULL,'Creation aaa item in tribute by admin','2018-04-19 15:14:31',NULL,NULL,1),(175,NULL,NULL,NULL,'Logged in with the user admin','2018-04-19 20:02:40',NULL,NULL,1),(176,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 13:38:31',NULL,NULL,1),(177,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 14:08:56',NULL,NULL,1),(178,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 15:09:21',NULL,NULL,1),(179,NULL,NULL,NULL,'User admin logged in.','2018-05-10 15:10:07',NULL,NULL,1),(180,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 15:10:24',NULL,NULL,1),(181,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 15:28:57',NULL,NULL,1),(182,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 15:29:30',NULL,NULL,1),(183,NULL,NULL,NULL,'Creation Einarm item in user by admin','2018-05-10 15:51:59',NULL,NULL,1),(184,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 15:58:53',NULL,NULL,1),(185,NULL,NULL,NULL,'Creation Test 50% item in tax by admin','2018-05-10 16:00:10',NULL,NULL,1),(186,NULL,NULL,NULL,'User admin logged in.','2018-05-10 16:01:33',NULL,NULL,1),(187,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 16:02:02',NULL,NULL,1),(188,NULL,NULL,NULL,'Logged in with the user admin','2018-05-10 16:02:26',NULL,NULL,1),(189,NULL,NULL,NULL,'Logged in with the user admin','2018-05-11 02:38:36',NULL,NULL,1),(190,NULL,NULL,NULL,'Logged in with the user admin','2018-05-11 12:14:53',NULL,NULL,1),(191,NULL,NULL,NULL,'Logged in with the user admin','2018-05-11 12:31:02',NULL,NULL,1),(192,NULL,NULL,NULL,'Logged in with the user admin','2018-05-11 12:31:21',NULL,NULL,1),(193,NULL,NULL,NULL,'Logged in with the user admin','2018-05-11 12:33:08',NULL,NULL,1),(194,NULL,NULL,NULL,'Logged in with the user admin','2018-06-11 11:47:00',NULL,NULL,1),(195,NULL,NULL,NULL,'Logged in with the user admin','2018-06-12 18:47:30',NULL,NULL,1),(196,NULL,NULL,NULL,'Logged in with the user admin','2018-06-13 19:30:38',NULL,NULL,1),(197,NULL,NULL,NULL,'Logged in with the user admin','2018-06-18 13:10:05',NULL,NULL,1),(198,NULL,NULL,NULL,'Logged in with the user admin','2018-06-18 13:23:29',NULL,NULL,1),(199,NULL,NULL,NULL,'Logged in with the user admin','2018-06-18 13:26:40',NULL,NULL,1),(200,NULL,NULL,NULL,'User admin logged in.','2018-06-18 13:58:36',NULL,NULL,1),(201,NULL,NULL,NULL,'Logged in with the user admin','2018-06-18 13:58:42',NULL,NULL,1),(202,NULL,NULL,NULL,'Logged in with the user admin','2018-07-03 15:41:08',NULL,NULL,1),(203,NULL,NULL,NULL,'Logged in with the user admin','2018-07-03 15:44:03',NULL,NULL,1),(204,NULL,NULL,NULL,'Logged in with the user admin','2018-07-09 11:55:09',NULL,NULL,1),(205,NULL,NULL,NULL,'Logged in with the user admin','2018-07-09 14:16:48',NULL,NULL,1),(206,NULL,NULL,NULL,'Logged in with the user admin','2018-07-13 15:19:16',NULL,NULL,1),(207,NULL,NULL,NULL,'Logged in with the user admin','2018-07-13 21:14:54',NULL,NULL,1),(208,NULL,NULL,NULL,'Logged in with the user admin','2018-07-17 19:32:35',NULL,NULL,1),(209,NULL,NULL,NULL,'Logged in with the user admin','2018-07-18 18:07:44',NULL,NULL,1),(210,NULL,NULL,NULL,'Logged in with the user admin','2018-07-19 13:32:50',NULL,NULL,1),(211,NULL,NULL,NULL,'Deleting Test2 item in project by admin','2018-07-19 13:46:00',NULL,NULL,1),(212,NULL,NULL,NULL,'Creation wandeson.jpaiva@gmail.com item in user by admin','2018-07-19 13:54:18',NULL,NULL,1),(213,NULL,NULL,NULL,'Logged in with the user admin','2018-07-19 13:55:40',NULL,NULL,1),(214,NULL,NULL,NULL,'Logged in with the user admin','2018-07-19 19:27:10',NULL,NULL,1),(215,NULL,NULL,NULL,'User admin logged in.','2018-07-19 19:30:22',NULL,NULL,1),(216,NULL,NULL,NULL,'Logged in with the user admin','2018-07-19 19:30:29',NULL,NULL,1),(217,NULL,NULL,NULL,'User admin logged in.','2018-07-19 19:31:29',NULL,NULL,1),(218,NULL,NULL,NULL,'Logged in with the user admin','2018-07-19 19:31:38',NULL,NULL,1),(219,NULL,NULL,NULL,'Logged in with the user admin','2018-07-19 19:31:49',NULL,NULL,1),(220,NULL,NULL,NULL,'User admin logged in.','2018-07-19 19:32:08',NULL,NULL,1),(221,NULL,NULL,NULL,'Logged in with the user admin','2018-07-19 19:32:13',NULL,NULL,1),(222,NULL,NULL,NULL,'User admin logged in.','2018-07-19 19:33:07',NULL,NULL,1),(223,NULL,NULL,NULL,'Creation test item in project by admin','2018-07-19 19:33:07',NULL,NULL,1),(224,NULL,NULL,NULL,'Logged in with the user admin','2018-07-19 19:33:12',NULL,NULL,1),(225,NULL,NULL,NULL,'Logged in with the user admin','2018-07-23 14:29:31',NULL,NULL,1),(226,NULL,NULL,NULL,'Logged in with the user admin','2018-07-23 15:53:31',NULL,NULL,1),(227,NULL,NULL,NULL,'User admin logged in.','2018-07-23 15:53:54',NULL,NULL,1),(228,NULL,NULL,NULL,'Logged in with the user admin','2018-07-23 17:25:35',NULL,NULL,1),(229,NULL,NULL,NULL,'Logged in with the user admin','2018-07-23 17:36:05',NULL,NULL,1),(230,NULL,NULL,NULL,'Logged in with the user admin','2018-07-23 18:28:15',NULL,NULL,1),(231,NULL,NULL,NULL,'Logged in with the user admin','2018-07-23 19:32:16',NULL,NULL,1),(232,NULL,NULL,NULL,'User admin logged in.','2018-07-24 14:33:19',NULL,NULL,1),(233,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 14:34:00',NULL,NULL,1),(234,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 14:36:41',NULL,NULL,1),(235,NULL,NULL,NULL,'User admin logged in.','2018-07-24 14:39:51',NULL,NULL,1),(236,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 15:14:39',NULL,NULL,1),(237,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 16:20:29',NULL,NULL,1),(238,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 16:20:31',NULL,NULL,1),(239,NULL,NULL,NULL,'User admin logged in.','2018-07-24 16:21:59',NULL,NULL,1),(240,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 16:23:07',NULL,NULL,1),(241,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 17:50:02',NULL,NULL,1),(242,NULL,NULL,NULL,'Creation 29 item in deleteProduct by admin','2018-07-24 17:56:59',NULL,NULL,1),(243,NULL,NULL,NULL,'Creation 30 item in deleteProduct by admin','2018-07-24 17:56:59',NULL,NULL,1),(244,NULL,NULL,NULL,'Creation 31 item in deleteProduct by admin','2018-07-24 17:56:59',NULL,NULL,1),(245,NULL,NULL,NULL,'Creation 32 item in deleteProduct by admin','2018-07-24 17:56:59',NULL,NULL,1),(246,NULL,NULL,NULL,'Updating 2 item in ProductPromotional by admin','2018-07-24 17:59:18',NULL,NULL,1),(247,NULL,NULL,NULL,'Updating 2 item in ProductPromotional by admin','2018-07-24 18:00:26',NULL,NULL,1),(248,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 18:20:34',NULL,NULL,1),(249,NULL,NULL,NULL,'Logged in with the user admin','2018-07-24 20:31:05',NULL,NULL,1),(250,NULL,NULL,NULL,'Logged in with the user admin','2018-07-25 14:03:37',NULL,NULL,1),(251,NULL,NULL,NULL,'User admin logged in.','2018-07-26 12:18:13',NULL,NULL,1),(252,NULL,NULL,NULL,'Logged in with the user admin','2018-07-26 12:18:23',NULL,NULL,1),(253,NULL,NULL,NULL,'Logged in with the user admin','2018-07-26 18:55:11',NULL,NULL,1),(254,NULL,NULL,NULL,'Logged in with the user admin','2018-07-26 20:19:42',NULL,NULL,1),(255,NULL,NULL,NULL,'Logged in with the user admin','2018-07-26 20:19:53',NULL,NULL,1),(256,NULL,NULL,NULL,'Logged in with the user admin','2018-07-26 20:21:24',NULL,NULL,1),(257,NULL,NULL,NULL,'Logged in with the user admin','2018-07-26 21:01:53',NULL,NULL,1),(258,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 13:00:38',NULL,NULL,1),(259,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 13:03:54',NULL,NULL,1),(260,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 13:37:10',NULL,NULL,1),(261,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 14:40:03',NULL,NULL,1),(262,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 14:58:09',NULL,NULL,1),(263,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 15:10:41',NULL,NULL,1),(264,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 15:28:07',NULL,NULL,1),(265,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 15:38:12',NULL,NULL,1),(266,NULL,NULL,NULL,'Logged in with the user admin','2018-07-27 17:10:41',NULL,NULL,1),(267,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 14:32:42',NULL,NULL,1),(268,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 17:43:31',NULL,NULL,1),(269,NULL,NULL,NULL,'User admin logged in.','2018-07-30 17:43:44',NULL,NULL,1),(270,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 17:44:02',NULL,NULL,1),(271,NULL,NULL,NULL,'User admin logged in.','2018-07-30 17:44:43',NULL,NULL,1),(272,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 17:45:39',NULL,NULL,1),(273,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 18:15:37',NULL,NULL,1),(274,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 18:15:48',NULL,NULL,1),(275,NULL,NULL,NULL,'User admin logged in.','2018-07-30 18:22:08',NULL,NULL,1),(276,NULL,NULL,NULL,'User admin logged in.','2018-07-30 18:22:11',NULL,NULL,1),(277,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 18:57:10',NULL,NULL,1),(278,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 19:06:05',NULL,NULL,1),(279,NULL,NULL,NULL,'Logged in with the user admin','2018-07-30 19:38:45',NULL,NULL,1),(280,NULL,NULL,NULL,'Logged in with the user admin','2018-08-01 18:06:12',NULL,NULL,1),(281,NULL,NULL,NULL,'Logged in with the user admin','2018-08-01 18:28:18',NULL,NULL,1),(282,NULL,NULL,NULL,'Logged in with the user admin','2018-08-01 18:30:40',NULL,NULL,1),(283,NULL,NULL,NULL,'Logged in with the user admin','2018-08-01 18:59:08',NULL,NULL,1),(284,NULL,NULL,NULL,'Logged in with the user admin','2018-08-01 20:38:09',NULL,NULL,1),(285,NULL,NULL,NULL,'Logged in with the user admin','2018-08-01 20:38:20',NULL,NULL,1),(286,NULL,NULL,NULL,'Logged in with the user admin','2018-08-01 20:38:20',NULL,NULL,1),(287,NULL,NULL,NULL,'Logged in with the user admin','2018-08-01 20:42:10',NULL,NULL,1),(288,NULL,NULL,NULL,'Logged in with the user admin','2018-08-03 14:34:27',NULL,NULL,1),(289,NULL,NULL,NULL,'Logged in with the user admin','2018-08-03 15:32:56',NULL,NULL,1),(290,NULL,NULL,NULL,'Logged in with the user admin','2018-08-06 12:38:55',NULL,NULL,1),(291,NULL,NULL,NULL,'Logged in with the user admin','2018-08-06 13:02:27',NULL,NULL,1),(292,NULL,NULL,NULL,'Creation pp item in color by admin','2018-08-06 13:50:58',NULL,NULL,1),(293,NULL,NULL,NULL,'Deleting pp item in color by admin','2018-08-06 13:51:06',NULL,NULL,1),(294,NULL,NULL,NULL,'Creation pp item in color by admin','2018-08-06 13:51:12',NULL,NULL,1),(295,NULL,NULL,NULL,'Updating pp2 item in project by admin','2018-08-06 13:51:21',NULL,NULL,1),(296,NULL,NULL,NULL,'Updating PP21 item in project by admin','2018-08-06 13:52:31',NULL,NULL,1),(297,NULL,NULL,NULL,'Logged in with the user admin','2018-08-06 14:41:43',NULL,NULL,1),(298,NULL,NULL,NULL,'Creation Teste item in shipping method by admin','2018-08-06 19:05:13',NULL,NULL,1),(299,NULL,NULL,NULL,'Deleting Teste item in shipping method by admin','2018-08-06 19:05:21',NULL,NULL,1),(300,NULL,NULL,NULL,'Logged in with the user admin','2018-08-07 12:39:21',NULL,NULL,1),(301,NULL,NULL,NULL,'Creation 23123123 item in serial number by admin','2018-08-07 13:00:45',NULL,NULL,1),(302,NULL,NULL,NULL,'Creation aaaaa item in serial number by admin','2018-08-07 13:00:52',NULL,NULL,1),(303,NULL,NULL,NULL,'Deleting aaaaa item in serial number by admin','2018-08-07 13:00:58',NULL,NULL,1),(304,NULL,NULL,NULL,'Updating 23123123 item in serial number by admin','2018-08-07 13:01:06',NULL,NULL,1),(305,NULL,NULL,NULL,'Updating 23123123 item in serial number by admin','2018-08-07 13:01:07',NULL,NULL,1),(306,NULL,NULL,NULL,'Deleting 23123123 item in serial number by admin','2018-08-07 13:01:15',NULL,NULL,1),(307,NULL,NULL,NULL,'Creation 23123123 item in serial number by admin','2018-08-07 13:08:30',NULL,NULL,1),(308,NULL,NULL,NULL,'Creation Teste item in ProductManufacturer by admin','2018-08-07 13:08:55',NULL,NULL,1),(309,NULL,NULL,NULL,'Updating Teste item in ProductManufacturer by admin','2018-08-07 13:09:03',NULL,NULL,1),(310,NULL,NULL,NULL,'Creation teste item in ProductManufacturer by admin','2018-08-07 13:09:09',NULL,NULL,1),(311,NULL,NULL,NULL,'Creation Teste 1 item in ProductManufacturer by admin','2018-08-07 13:17:34',NULL,NULL,1),(312,NULL,NULL,NULL,'Creation Teste item in ProductManufacturer by admin','2018-08-07 13:21:50',NULL,NULL,1),(313,NULL,NULL,NULL,'Updating Teste item in ProductManufacturer by admin','2018-08-07 13:22:03',NULL,NULL,1),(314,NULL,NULL,NULL,'Creation Teste item in ProductManufacturer by admin','2018-08-07 13:28:24',NULL,NULL,1),(315,NULL,NULL,NULL,'Updating Teste item in ProductManufacturer by admin','2018-08-07 13:29:08',NULL,NULL,1),(316,NULL,NULL,NULL,'Updating Teste item in ProductManufacturer by admin','2018-08-07 13:32:24',NULL,NULL,1),(317,NULL,NULL,NULL,'Updating Teste item in ProductManufacturer by admin','2018-08-07 13:32:29',NULL,NULL,1),(318,NULL,NULL,NULL,'Updating Teste item in ProductManufacturer by admin','2018-08-07 13:33:33',NULL,NULL,1),(319,NULL,NULL,NULL,'Updating Teste item in ProductManufacturer by admin','2018-08-07 13:33:40',NULL,NULL,1),(320,NULL,NULL,NULL,'Updating Teste 1 item in ProductManufacturer by admin','2018-08-07 13:52:48',NULL,NULL,1),(321,NULL,NULL,NULL,'Creation Teste item in ProductManufacturer by admin','2018-08-07 13:55:09',NULL,NULL,1),(322,NULL,NULL,NULL,'Creation Teste 2 item in Productgroup by admin','2018-08-07 14:35:35',NULL,NULL,1),(323,NULL,NULL,NULL,'Creation Teste 2 item in Productgroup by admin','2018-08-07 14:35:44',NULL,NULL,1),(324,NULL,NULL,NULL,'Updating 5 item in Productgroup by admin','2018-08-07 14:36:19',NULL,NULL,1),(325,NULL,NULL,NULL,'Updating 5 item in Productgroup by admin','2018-08-07 14:36:26',NULL,NULL,1),(326,NULL,NULL,NULL,'Updating 5 item in Productgroup by admin','2018-08-07 14:36:36',NULL,NULL,1),(327,NULL,NULL,NULL,'Creation Teste item in ProductUnit by admin','2018-08-07 14:38:32',NULL,NULL,1),(328,NULL,NULL,NULL,'Updating Teste item in ProductUnit by admin','2018-08-07 14:38:47',NULL,NULL,1),(329,NULL,NULL,NULL,'Updating Teste item in ProductUnit by admin','2018-08-07 14:38:57',NULL,NULL,1),(330,NULL,NULL,NULL,'Updating Teste item in ProductUnit by admin','2018-08-07 14:39:05',NULL,NULL,1),(331,NULL,NULL,NULL,'Updating Teste item in ProductUnit by admin','2018-08-07 14:39:10',NULL,NULL,1),(332,NULL,NULL,NULL,'Updating Teste item in ProductUnit by admin','2018-08-07 14:39:15',NULL,NULL,1),(333,NULL,NULL,NULL,'Creation Teste item in ProductUnit by admin','2018-08-07 14:39:30',NULL,NULL,1),(334,NULL,NULL,NULL,'Updating Teste item in ProductUnit by admin','2018-08-07 14:39:43',NULL,NULL,1),(335,NULL,NULL,NULL,'Creation Teste 2 item in Productgroup by admin','2018-08-07 14:40:46',NULL,NULL,1),(336,NULL,NULL,NULL,'Updating 6 item in Productgroup by admin','2018-08-07 14:40:52',NULL,NULL,1),(337,NULL,NULL,NULL,'Updating 6 item in Productgroup by admin','2018-08-07 14:40:58',NULL,NULL,1),(338,NULL,NULL,NULL,'Creation 11 item in Productgroup by admin','2018-08-07 14:41:11',NULL,NULL,1),(339,NULL,NULL,NULL,'Updating 7 item in Productgroup by admin','2018-08-07 14:41:21',NULL,NULL,1),(340,NULL,NULL,NULL,'Creation 11 item in Productgroup by admin','2018-08-07 14:41:32',NULL,NULL,1),(341,NULL,NULL,NULL,'Logged in with the user admin','2018-08-07 15:11:25',NULL,NULL,1),(342,NULL,NULL,NULL,'Logged in with the user admin','2018-08-07 16:56:59',NULL,NULL,1),(343,NULL,NULL,NULL,'Logged in with the user admin','2018-08-08 13:49:29',NULL,NULL,1),(344,NULL,NULL,NULL,'Creation Teste item in shipping method by admin','2018-08-08 14:14:54',NULL,NULL,1),(345,NULL,NULL,NULL,'Updating Teste 2 item in shipping method by admin','2018-08-08 14:15:00',NULL,NULL,1),(346,NULL,NULL,NULL,'Updating Teste 2 item in shipping method by admin','2018-08-08 14:15:08',NULL,NULL,1),(347,NULL,NULL,NULL,'Deleting Teste 2 item in shipping method by admin','2018-08-08 14:15:11',NULL,NULL,1),(348,NULL,NULL,NULL,'Logged in with the user admin','2018-08-10 11:57:08',NULL,NULL,1),(349,NULL,NULL,NULL,'Logged in with the user admin','2018-08-10 13:44:00',NULL,NULL,1),(350,NULL,NULL,NULL,'Logged in with the user admin','2018-08-10 15:47:42',NULL,NULL,1),(351,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-10 17:29:06',NULL,NULL,1),(352,NULL,NULL,NULL,'Logged in with the user admin','2018-08-13 12:04:24',NULL,NULL,1),(353,NULL,NULL,NULL,'Logged in with the user admin','2018-08-13 12:46:23',NULL,NULL,1),(354,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:44:06',NULL,NULL,1),(355,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:44:16',NULL,NULL,1),(356,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:44:27',NULL,NULL,1),(357,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:44:38',NULL,NULL,1),(358,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:44:41',NULL,NULL,1),(359,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:44:45',NULL,NULL,1),(360,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:44:49',NULL,NULL,1),(361,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:45:03',NULL,NULL,1),(362,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:45:08',NULL,NULL,1),(363,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:46:11',NULL,NULL,1),(364,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:18',NULL,NULL,1),(365,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:18',NULL,NULL,1),(366,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:18',NULL,NULL,1),(367,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:18',NULL,NULL,1),(368,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:18',NULL,NULL,1),(369,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:18',NULL,NULL,1),(370,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:19',NULL,NULL,1),(371,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:19',NULL,NULL,1),(372,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:19',NULL,NULL,1),(373,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:24',NULL,NULL,1),(374,NULL,NULL,NULL,'Deleting Teste item in company by admin','2018-08-13 13:47:24',NULL,NULL,1),(375,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:47:29',NULL,NULL,1),(376,NULL,NULL,NULL,'Creation  item in company by admin','2018-08-13 13:47:43',NULL,NULL,1),(377,NULL,NULL,NULL,'User admin logged in.','2018-08-13 20:24:40',NULL,NULL,1),(378,NULL,NULL,NULL,'Logged in with the user admin','2018-08-15 13:40:01',NULL,NULL,1),(379,NULL,NULL,NULL,'Logged in with the user admin','2018-08-15 17:14:17',NULL,NULL,1),(380,NULL,NULL,NULL,'Logged in with the user admin','2018-08-17 15:02:22',NULL,NULL,1),(381,NULL,NULL,NULL,'Logged in with the user admin','2018-08-27 13:11:46',NULL,NULL,1),(382,NULL,NULL,NULL,'Logged in with the user admin','2018-08-29 15:05:44',NULL,NULL,1),(383,NULL,NULL,NULL,'Logged in with the user admin','2018-08-30 14:36:03',NULL,NULL,1),(384,NULL,NULL,NULL,'Logged in with the user admin','2018-09-04 13:13:05',NULL,NULL,1),(385,NULL,NULL,NULL,'Logged in with the user admin','2018-09-04 19:17:12',NULL,NULL,1),(386,NULL,NULL,NULL,'Logged in with the user admin','2018-09-10 12:25:45',NULL,NULL,1),(387,NULL,NULL,NULL,'Logged in with the user admin','2018-09-10 13:14:36',NULL,NULL,1),(388,NULL,NULL,NULL,'Logged in with the user admin','2018-09-10 18:34:07',NULL,NULL,1),(389,NULL,NULL,NULL,'Logged in with the user admin','2018-09-11 13:17:51',NULL,NULL,1),(390,NULL,NULL,NULL,'Logged in with the user admin','2018-09-11 13:22:38',NULL,NULL,1),(391,NULL,NULL,NULL,'Logged in with the user admin','2018-09-11 18:14:03',NULL,NULL,1),(392,NULL,NULL,NULL,'Logged in with the user admin','2018-09-12 12:51:26',NULL,NULL,1),(393,NULL,NULL,NULL,'Logged in with the user admin','2018-09-12 15:32:34',NULL,NULL,1),(394,NULL,NULL,NULL,'Logged in with the user admin','2018-09-12 19:11:17',NULL,NULL,1),(395,NULL,NULL,NULL,'User admin logged in.','2018-09-12 19:11:37',NULL,NULL,1),(396,NULL,NULL,NULL,'Logged in with the user admin','2018-09-12 19:11:59',NULL,NULL,1),(397,NULL,NULL,NULL,'Logged in with the user admin','2018-09-12 19:45:10',NULL,NULL,1),(398,NULL,NULL,NULL,'Logged in with the user admin','2018-09-13 19:19:15',NULL,NULL,1),(399,NULL,NULL,NULL,'Logged in with the user admin','2018-09-13 20:21:51',NULL,NULL,1),(400,NULL,NULL,NULL,'Logged in with the user admin','2018-09-13 20:28:45',NULL,NULL,1),(401,NULL,NULL,NULL,'Logged in with the user admin','2018-09-13 20:36:31',NULL,NULL,1),(402,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 15:00:46',NULL,NULL,1),(403,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 15:27:50',NULL,NULL,1),(404,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 22:19:59',NULL,NULL,1),(405,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 22:20:04',NULL,NULL,1),(406,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 22:20:14',NULL,NULL,1),(407,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 22:21:13',NULL,NULL,1),(408,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 22:51:54',NULL,NULL,1),(409,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 22:52:01',NULL,NULL,1),(410,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 22:59:35',NULL,NULL,1),(411,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 23:01:52',NULL,NULL,1),(412,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 23:02:28',NULL,NULL,1),(413,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 23:02:39',NULL,NULL,1),(414,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 20:37:15',NULL,NULL,1),(415,NULL,NULL,NULL,'User admin logged in.','2018-09-14 20:38:03',NULL,NULL,1),(416,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 20:38:06',NULL,NULL,1),(417,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 20:51:09',NULL,NULL,1),(418,NULL,NULL,NULL,'User admin logged in.','2018-09-14 20:51:30',NULL,NULL,1),(419,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 20:53:04',NULL,NULL,1),(420,NULL,NULL,NULL,'Logged in with the user admin','2018-09-14 20:54:01',NULL,NULL,1),(421,NULL,NULL,NULL,'Logged in with the user admin','2018-09-15 00:12:15',NULL,NULL,1),(422,NULL,NULL,NULL,'Logged in with the user admin','2018-09-15 18:15:26',NULL,NULL,1),(423,NULL,NULL,NULL,'User admin logged in.','2018-09-15 22:12:42',NULL,NULL,1),(424,NULL,NULL,NULL,'Logged in with the user admin','2018-09-15 22:12:47',NULL,NULL,1),(425,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 15:22:34',NULL,NULL,1),(426,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 15:22:39',NULL,NULL,1),(427,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 15:22:58',NULL,NULL,1),(428,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 15:22:58',NULL,NULL,1),(429,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 15:23:57',NULL,NULL,1),(430,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 15:43:04',NULL,NULL,1),(431,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 17:16:32',NULL,NULL,1),(432,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 18:05:45',NULL,NULL,1),(433,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 18:05:45',NULL,NULL,1),(434,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 20:01:46',NULL,NULL,1),(435,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 21:39:05',NULL,NULL,1),(436,NULL,NULL,NULL,'User admin logged in.','2018-09-17 21:55:02',NULL,NULL,1),(437,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 21:55:06',NULL,NULL,1),(438,NULL,NULL,NULL,'Logged in with the user admin','2018-09-17 22:43:38',NULL,NULL,1),(439,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 15:39:55',NULL,NULL,1),(440,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 15:40:23',NULL,NULL,1),(441,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 18:26:03',NULL,NULL,1),(442,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 18:27:35',NULL,NULL,1),(443,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 18:27:57',NULL,NULL,1),(444,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 18:28:45',NULL,NULL,1),(445,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 18:31:41',NULL,NULL,1),(446,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 19:27:08',NULL,NULL,1),(447,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 19:54:43',NULL,NULL,1),(448,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 22:15:54',NULL,NULL,1),(449,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 22:16:52',NULL,NULL,1),(450,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 22:26:26',NULL,NULL,1),(451,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 22:29:02',NULL,NULL,1),(452,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 22:29:06',NULL,NULL,1),(453,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 22:29:09',NULL,NULL,1),(454,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 22:29:12',NULL,NULL,1),(455,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 22:29:55',NULL,NULL,1),(456,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 23:16:16',NULL,NULL,1),(457,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 23:16:54',NULL,NULL,1),(458,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 23:34:35',NULL,NULL,1),(459,NULL,NULL,NULL,'Logged in with the user admin','2018-09-18 23:43:31',NULL,NULL,1),(460,NULL,NULL,NULL,'Logged in with the user admin','2018-09-19 16:35:28',NULL,NULL,1),(461,NULL,NULL,NULL,'Logged in with the user admin','2018-09-19 23:37:52',NULL,NULL,1),(462,NULL,NULL,NULL,'Logged in with the user admin','2018-09-20 00:15:23',NULL,NULL,1),(463,NULL,NULL,NULL,'User admin logged in.','2018-09-20 00:19:37',NULL,NULL,1),(464,NULL,NULL,NULL,'Logged in with the user admin','2018-09-20 00:19:42',NULL,NULL,1),(465,NULL,NULL,NULL,'Logged in with the user admin','2018-09-20 20:08:04',NULL,NULL,1),(466,NULL,NULL,NULL,'Logged in with the user admin','2018-09-24 16:12:54',NULL,NULL,1),(467,NULL,NULL,NULL,'Logged in with the user admin','2018-09-24 16:24:23',NULL,NULL,1),(468,NULL,NULL,NULL,'User admin logged in.','2018-09-24 16:24:30',NULL,NULL,1),(469,NULL,NULL,NULL,'Logged in with the user admin','2018-09-24 16:24:36',NULL,NULL,1),(470,NULL,NULL,NULL,'User admin logged in.','2018-09-24 16:48:33',NULL,NULL,1),(471,NULL,NULL,NULL,'Logged in with the user admin','2018-09-24 16:48:38',NULL,NULL,1),(472,NULL,NULL,NULL,'Logged in with the user admin','2018-09-24 17:33:00',NULL,NULL,1),(473,NULL,NULL,NULL,'Logged in with the user admin','2018-09-24 21:34:41',NULL,NULL,1),(474,NULL,NULL,NULL,'Logged in with the user admin','2018-09-24 21:40:05',NULL,NULL,1),(475,NULL,NULL,NULL,'Logged in with the user admin','2018-09-24 21:41:37',NULL,NULL,1),(476,NULL,NULL,NULL,'Logged in with the user admin','2018-09-25 16:55:50',NULL,NULL,1),(477,NULL,NULL,NULL,'User admin logged in.','2018-09-25 21:27:44',NULL,NULL,1),(478,NULL,NULL,NULL,'Logged in with the user admin','2018-09-25 21:27:48',NULL,NULL,1),(479,NULL,NULL,NULL,'User admin logged in.','2018-09-25 21:30:27',NULL,NULL,1),(480,NULL,NULL,NULL,'Logged in with the user admin','2018-09-25 21:30:35',NULL,NULL,1),(481,NULL,NULL,NULL,'Logged in with the user admin','2018-09-26 17:45:01',NULL,NULL,1),(482,NULL,NULL,NULL,'Logged in with the user admin','2018-09-29 19:42:56',NULL,NULL,1),(483,NULL,NULL,NULL,'Logged in with the user admin','2018-09-29 19:45:56',NULL,NULL,1),(484,NULL,NULL,NULL,'Logged in with the user admin','2018-09-29 20:31:14',NULL,NULL,1),(485,NULL,NULL,NULL,'User Cacá logged in.','2018-09-29 20:44:33',NULL,NULL,1),(486,NULL,NULL,NULL,'Logged in with the user Caca','2018-09-29 20:45:48',NULL,NULL,1),(487,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-29 21:13:03',NULL,NULL,1),(488,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-29 22:42:14',NULL,NULL,1),(489,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-29 22:47:15',NULL,NULL,1),(490,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-29 23:37:58',NULL,NULL,1),(491,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-29 23:55:42',NULL,NULL,1),(492,NULL,NULL,NULL,'User Carlos Henrique logged in.','2018-09-29 23:55:51',NULL,NULL,1),(493,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-29 23:56:13',NULL,NULL,1),(494,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-29 23:56:46',NULL,NULL,1),(495,NULL,NULL,NULL,'User Carlos Henrique logged in.','2018-09-29 23:57:39',NULL,NULL,1),(496,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-30 00:00:37',NULL,NULL,1),(497,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-30 00:48:50',NULL,NULL,1),(498,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-30 00:56:22',NULL,NULL,1),(499,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-30 01:16:26',NULL,NULL,1),(500,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-30 01:23:17',NULL,NULL,1),(501,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-30 01:26:52',NULL,NULL,1),(502,NULL,NULL,NULL,'Logged in with the user Carlos Henrique','2018-09-30 03:06:33',NULL,NULL,1);
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_time`
--

DROP TABLE IF EXISTS `log_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_time` (
  `idlog_time` int(11) NOT NULL AUTO_INCREMENT,
  `users_idusers` int(11) NOT NULL,
  `pos_idpos` int(11) NOT NULL,
  `login_date` timestamp NULL DEFAULT NULL COMMENT 'The last time the user has logged in.',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'If status = 1, the user is logged. Else, the user is not logged.',
  `current_multiple_connections` int(11) NOT NULL DEFAULT '0' COMMENT 'The number of current simultaneous connections of this user.',
  PRIMARY KEY (`idlog_time`),
  KEY `fk_log_time_user1_idx` (`users_idusers`),
  KEY `fk_log_time_pos1_idx` (`pos_idpos`),
  CONSTRAINT `fk_log_time_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_log_time_user1` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_time`
--

LOCK TABLES `log_time` WRITE;
/*!40000 ALTER TABLE `log_time` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_time` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_type`
--

DROP TABLE IF EXISTS `log_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_type` (
  `idlog_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idlog_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_type`
--

LOCK TABLES `log_type` WRITE;
/*!40000 ALTER TABLE `log_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logo_tmpt`
--

DROP TABLE IF EXISTS `logo_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logo_tmpt` (
  `idlogo_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `img_tmpt` longblob,
  `active` tinyint(2) DEFAULT NULL,
  PRIMARY KEY (`idlogo_tmpt`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logo_tmpt`
--

LOCK TABLES `logo_tmpt` WRITE;
/*!40000 ALTER TABLE `logo_tmpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `logo_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loss`
--

DROP TABLE IF EXISTS `loss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loss` (
  `idloss` int(11) NOT NULL AUTO_INCREMENT,
  `user_iduser` int(11) NOT NULL,
  `extra_info` longtext COMMENT 'Any information the employee have to write.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idloss`),
  KEY `fk_loss_user1_idx` (`user_iduser`),
  CONSTRAINT `fk_loss_user1` FOREIGN KEY (`user_iduser`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loss`
--

LOCK TABLES `loss` WRITE;
/*!40000 ALTER TABLE `loss` DISABLE KEYS */;
/*!40000 ALTER TABLE `loss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loss_line`
--

DROP TABLE IF EXISTS `loss_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loss_line` (
  `idloss_line` int(11) NOT NULL AUTO_INCREMENT,
  `loss_idloss` int(11) NOT NULL,
  `product_idproduct` int(11) NOT NULL,
  `extra_info` longtext COMMENT 'Any information the employee have to write about the especific lost product.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idloss_line`),
  KEY `fk_loss_line_loss1_idx` (`loss_idloss`),
  KEY `fk_loss_line_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_loss_line_loss1` FOREIGN KEY (`loss_idloss`) REFERENCES `loss` (`idloss`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_loss_line_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loss_line`
--

LOCK TABLES `loss_line` WRITE;
/*!40000 ALTER TABLE `loss_line` DISABLE KEYS */;
/*!40000 ALTER TABLE `loss_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturer` (
  `idmanufacturer` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `negociated_discount` double NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idmanufacturer`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer`
--

LOCK TABLES `manufacturer` WRITE;
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
INSERT INTO `manufacturer` VALUES (1,'Test',1,'2017-05-24 13:36:19',NULL,'2017-05-25 13:49:47',3),(2,'Test 2',1,'2017-05-24 13:37:39',NULL,'2017-05-25 13:49:47',3),(3,'Pespi',1,'2017-05-25 13:49:42',NULL,'2017-05-25 14:15:29',3),(4,'Coca Cola',1,'2017-05-25 14:15:25',NULL,NULL,1),(5,'test',4,'2018-04-19 15:03:00','2018-04-19 15:03:26','2018-04-19 15:03:33',3),(6,'Teste',12,'2018-08-07 13:08:55','2018-08-07 13:09:02',NULL,1),(7,'teste',1,'2018-08-07 13:09:09',NULL,'2018-08-07 13:22:14',3),(8,'Teste 1',1,'2018-08-07 13:17:34',NULL,'2018-08-07 13:22:14',3),(9,'Teste',33,'2018-08-07 13:21:50','2018-08-07 13:22:03','2018-08-07 13:22:14',3),(10,'Teste 1',0,'2018-08-07 13:28:24','2018-08-07 13:52:48','2018-08-07 13:52:54',3),(11,'Teste',0,'2018-08-07 13:55:09',NULL,NULL,1);
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `idorder` int(11) NOT NULL AUTO_INCREMENT,
  `person_idperson` int(11) DEFAULT NULL COMMENT 'The ID of the customer (person) who ordered. It may be null if there is a ID in company_idcompany.',
  `users_idusers` int(11) NOT NULL COMMENT 'The ID of the user (employee) who made the order.',
  `pos_session_idpos_session` int(11) NOT NULL,
  `company_idcompany` int(11) DEFAULT NULL COMMENT 'The ID of the customer (company) who ordered. It may be null if there is a ID in person_idperson.',
  `shipping_mtd_idshipping_mtd` int(11) DEFAULT NULL,
  `webshop_idwebshop` int(11) DEFAULT NULL COMMENT 'The id of the webshop where the order come from (if it was made by a webshop)',
  `address_idaddress` int(11) DEFAULT NULL COMMENT 'This will be null if the order is not deliverable.',
  `date_shipped` timestamp NULL DEFAULT NULL COMMENT 'When the order was shipped.',
  `tracking_code` varchar(60) DEFAULT NULL COMMENT 'The code to consult where the package is, at the shipping companys site.',
  `date_cancelled` timestamp NULL DEFAULT NULL,
  `in_use` tinyint(1) NOT NULL DEFAULT '0',
  `from_mobile` tinyint(1) NOT NULL DEFAULT '0',
  `tip_value` decimal(11,2) NOT NULL DEFAULT '0.00',
  `value_shipping` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT 'The shipping cost of the shipping methods can changes in the future, so the current cost must be registered here.',
  `discount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `discount_reason` longtext,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0- Open, 1- Waiting payment confirmation, 2- Paid, waiting shippiment, 3- Shipped, waiting arrival at customers house, 4- Finished',
  `order_type` int(11) DEFAULT '0' COMMENT '0 - Parked, 1 - order, 2 - invoice',
  `RefundNote` int(11) NOT NULL DEFAULT '0' COMMENT '1 for Refund Note, where the client should have a discount later on',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `total_amount` decimal(11,2) DEFAULT NULL COMMENT 'The sum of all FINAL PRICE of the order_lines.',
  `order_identify` varchar(45) DEFAULT NULL COMMENT 'A number or string used for the company to identify each order, like a ID, but created by the company (not by the database)',
  `mobile` tinyint(4) DEFAULT '0',
  `extra_info` longtext COMMENT 'Aditional information that the employee may write.',
  PRIMARY KEY (`idorder`),
  KEY `fk_order_person1_idx` (`person_idperson`),
  KEY `fk_order_company1_idx` (`company_idcompany`),
  KEY `fk_order_shipping_mtd1_idx` (`shipping_mtd_idshipping_mtd`),
  KEY `fk_order_user1_idx` (`users_idusers`),
  KEY `fk_order_webshop1_idx` (`webshop_idwebshop`),
  KEY `fk_order_pos_session1_idx` (`pos_session_idpos_session`),
  KEY `fk_order_address1_idx` (`address_idaddress`),
  CONSTRAINT `fk_order_address1` FOREIGN KEY (`address_idaddress`) REFERENCES `address` (`idaddress`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_pos_session1` FOREIGN KEY (`pos_session_idpos_session`) REFERENCES `pos_session` (`idpos_session`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_shipping_mtd1` FOREIGN KEY (`shipping_mtd_idshipping_mtd`) REFERENCES `shipping_mtd` (`idshipping_mtd`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_user1` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_webshop1` FOREIGN KEY (`webshop_idwebshop`) REFERENCES `webshop` (`idwebshop`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=469 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (443,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 20:01:08',NULL,NULL,1,17.00,NULL,NULL,''),(444,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 20:04:32',NULL,NULL,1,12.00,NULL,NULL,''),(445,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 20:10:24',NULL,NULL,1,12.00,NULL,NULL,''),(446,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 20:49:31',NULL,NULL,1,40.00,NULL,NULL,''),(447,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0.00,0.00,0.00,NULL,0,1,0,'2018-09-29 20:51:33',NULL,NULL,1,0.00,NULL,NULL,''),(448,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0.00,0.00,0.00,NULL,0,1,0,'2018-09-29 20:51:59',NULL,NULL,1,0.00,NULL,NULL,''),(449,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,0,0,0,'2018-09-29 20:58:01',NULL,NULL,1,124.00,NULL,NULL,'cicero'),(450,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0.00,0.00,0.00,NULL,0,1,0,'2018-09-29 20:59:07',NULL,NULL,1,0.00,NULL,NULL,''),(451,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0.00,0.00,0.00,NULL,0,1,0,'2018-09-29 20:59:40',NULL,NULL,1,0.00,NULL,NULL,''),(452,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,0,0,0,'2018-09-29 21:01:51',NULL,NULL,1,5.00,NULL,NULL,'DIEGO'),(453,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 21:02:28',NULL,NULL,1,42.00,NULL,NULL,'ALICE'),(454,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 21:03:43',NULL,NULL,1,13.50,NULL,NULL,''),(455,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0.00,0.00,0.00,NULL,0,1,0,'2018-09-29 21:17:06',NULL,NULL,1,0.00,NULL,NULL,''),(456,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 21:20:27',NULL,NULL,1,9.00,NULL,NULL,''),(457,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 21:23:30',NULL,NULL,1,3.50,NULL,NULL,''),(458,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 21:26:00',NULL,NULL,1,9.00,NULL,NULL,''),(459,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 22:05:44',NULL,NULL,1,12.00,NULL,NULL,''),(460,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 22:08:34',NULL,NULL,1,15.00,NULL,NULL,''),(461,NULL,2,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 22:13:52',NULL,NULL,1,15.00,NULL,NULL,'FERNANDO'),(462,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0.00,0.00,0.00,NULL,0,1,0,'2018-09-29 22:15:21',NULL,NULL,1,0.00,NULL,NULL,''),(463,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 22:41:16',NULL,NULL,1,43.00,NULL,NULL,''),(464,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 22:43:15',NULL,NULL,1,44.00,NULL,NULL,''),(465,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 22:44:37',NULL,NULL,1,26.00,NULL,NULL,''),(466,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 22:50:18',NULL,NULL,1,57.00,NULL,NULL,''),(467,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0.00,0.00,0.00,NULL,2,1,0,'2018-09-29 22:53:11',NULL,NULL,1,51.00,NULL,NULL,''),(468,NULL,1,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0.00,0.00,0.00,NULL,0,1,0,'2018-09-29 23:06:37',NULL,NULL,1,0.00,NULL,NULL,'');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_billet`
--

DROP TABLE IF EXISTS `order_billet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_billet` (
  `idorder_billet` int(11) NOT NULL AUTO_INCREMENT,
  `order_idorder` int(11) NOT NULL,
  `value` decimal(11,2) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idorder_billet`),
  KEY `fk_order_billet_order1_idx` (`order_idorder`),
  CONSTRAINT `fk_order_billet_order1` FOREIGN KEY (`order_idorder`) REFERENCES `orders` (`idorder`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_billet`
--

LOCK TABLES `order_billet` WRITE;
/*!40000 ALTER TABLE `order_billet` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_billet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_line`
--

DROP TABLE IF EXISTS `order_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_line` (
  `idorder_line` int(11) NOT NULL AUTO_INCREMENT,
  `order_idorder` int(11) NOT NULL,
  `product_idproduct` int(11) NOT NULL,
  `size_color_idsize_color` int(11) DEFAULT NULL,
  `warranty_idwarranty` int(11) DEFAULT NULL COMMENT 'The warranty of the product.',
  `value_warranty` decimal(11,2) DEFAULT '0.00',
  `date_warranty` timestamp NULL DEFAULT NULL COMMENT 'The date until the warranty is on.',
  `extra_info` longtext COMMENT 'Aditional information that the employee may write.',
  `value` decimal(11,2) NOT NULL COMMENT 'The price of the product in the ordering moment (because the price of the product can be changed in the future, but in the past orders it must remain).',
  `actual_value` decimal(11,2) NOT NULL,
  `tax` decimal(11,2) DEFAULT NULL COMMENT 'The tax of the product in the ordering moment (because the tax of the product can be changed in the future, but in the past orders it must remain).',
  `qty` float NOT NULL DEFAULT '1',
  `discount` float DEFAULT '0',
  `discount_reason` longtext,
  `returned` decimal(11,2) DEFAULT '0.00' COMMENT 'In case of devolution, here goes the amount given back.',
  `returned_reason` longtext COMMENT 'In case of devolution, here goes the description of the reason and possible damages in the product.',
  `date_return` timestamp NULL DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idorder_line`),
  KEY `fk_order_line_order1_idx` (`order_idorder`),
  KEY `fk_order_line_product1_idx` (`product_idproduct`),
  KEY `fk_order_line_warranty1_idx` (`warranty_idwarranty`),
  CONSTRAINT `fk_order_line_order1` FOREIGN KEY (`order_idorder`) REFERENCES `order` (`idorder`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_line_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_line_warranty1` FOREIGN KEY (`warranty_idwarranty`) REFERENCES `warranty` (`idwarranty`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=424 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_line`
--

LOCK TABLES `order_line` WRITE;
/*!40000 ALTER TABLE `order_line` DISABLE KEYS */;
INSERT INTO `order_line` VALUES (372,443,40,NULL,NULL,NULL,NULL,NULL,12.00,12.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 00:01:32',NULL,NULL,1),(373,443,48,NULL,NULL,NULL,NULL,NULL,5.00,5.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 00:01:32',NULL,NULL,1),(374,444,40,NULL,NULL,NULL,NULL,NULL,12.00,12.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 00:08:08',NULL,NULL,1),(375,445,40,NULL,NULL,NULL,NULL,NULL,12.00,12.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 00:10:34',NULL,NULL,1),(381,446,37,NULL,NULL,NULL,NULL,NULL,5.00,5.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 00:53:47',NULL,NULL,1),(382,446,41,NULL,NULL,NULL,NULL,NULL,16.00,32.00,NULL,2,0,NULL,NULL,NULL,NULL,'2018-09-30 00:53:47',NULL,NULL,1),(383,446,45,NULL,NULL,NULL,NULL,NULL,3.00,3.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 00:53:47',NULL,NULL,1),(387,449,40,NULL,NULL,NULL,NULL,NULL,12.00,24.00,NULL,2,0,NULL,NULL,NULL,NULL,'2018-09-30 00:59:35',NULL,NULL,1),(388,449,41,NULL,NULL,NULL,NULL,NULL,16.00,48.00,NULL,3,0,NULL,NULL,NULL,NULL,'2018-09-30 00:59:35',NULL,NULL,1),(389,449,42,NULL,NULL,NULL,NULL,NULL,26.00,52.00,NULL,2,0,NULL,NULL,NULL,NULL,'2018-09-30 00:59:35',NULL,NULL,1),(390,452,39,NULL,NULL,NULL,NULL,NULL,5.00,5.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 01:02:18',NULL,NULL,1),(392,454,38,NULL,NULL,NULL,NULL,NULL,10.00,10.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 01:16:34',NULL,NULL,1),(393,454,46,NULL,NULL,NULL,NULL,NULL,3.50,3.50,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 01:16:34',NULL,NULL,1),(394,453,41,NULL,NULL,NULL,NULL,NULL,16.00,16.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 01:19:58',NULL,NULL,1),(395,453,42,NULL,NULL,NULL,NULL,NULL,26.00,26.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 01:19:58',NULL,NULL,1),(396,456,37,NULL,NULL,NULL,NULL,NULL,5.00,5.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 01:22:52',NULL,NULL,1),(397,456,47,NULL,NULL,NULL,NULL,NULL,4.00,4.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 01:22:52',NULL,NULL,1),(398,457,46,NULL,NULL,NULL,NULL,NULL,3.50,3.50,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 01:25:17',NULL,NULL,1),(399,458,37,NULL,NULL,NULL,NULL,NULL,5.00,5.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:05:22',NULL,NULL,1),(400,458,47,NULL,NULL,NULL,NULL,NULL,4.00,4.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:05:22',NULL,NULL,1),(401,459,40,NULL,NULL,NULL,NULL,NULL,12.00,12.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:08:05',NULL,NULL,1),(402,460,38,NULL,NULL,NULL,NULL,NULL,10.00,10.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:13:23',NULL,NULL,1),(403,460,45,NULL,NULL,NULL,NULL,NULL,3.00,3.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:13:23',NULL,NULL,1),(404,460,50,NULL,NULL,NULL,NULL,NULL,2.00,2.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:13:23',NULL,NULL,1),(407,461,36,NULL,NULL,NULL,NULL,NULL,3.00,15.00,NULL,5,0,NULL,NULL,NULL,NULL,'2018-09-30 02:41:01',NULL,NULL,1),(408,463,36,NULL,NULL,NULL,NULL,NULL,3.00,15.00,NULL,5,0,NULL,NULL,NULL,NULL,'2018-09-30 02:42:38',NULL,NULL,1),(409,463,38,NULL,NULL,NULL,NULL,NULL,10.00,20.00,NULL,2,0,NULL,NULL,NULL,NULL,'2018-09-30 02:42:38',NULL,NULL,1),(410,463,47,NULL,NULL,NULL,NULL,NULL,4.00,8.00,NULL,2,0,NULL,NULL,NULL,NULL,'2018-09-30 02:42:38',NULL,NULL,1),(411,464,42,NULL,NULL,NULL,NULL,NULL,26.00,26.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:44:24',NULL,NULL,1),(412,464,49,NULL,NULL,NULL,NULL,NULL,6.00,18.00,NULL,3,0,NULL,NULL,NULL,NULL,'2018-09-30 02:44:24',NULL,NULL,1),(413,465,37,NULL,NULL,NULL,NULL,NULL,5.00,5.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:49:54',NULL,NULL,1),(414,465,38,NULL,NULL,NULL,NULL,NULL,10.00,10.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:49:54',NULL,NULL,1),(415,465,39,NULL,NULL,NULL,NULL,NULL,5.00,5.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:49:54',NULL,NULL,1),(416,465,50,NULL,NULL,NULL,NULL,NULL,2.00,6.00,NULL,3,0,NULL,NULL,NULL,NULL,'2018-09-30 02:49:54',NULL,NULL,1),(417,466,38,NULL,NULL,NULL,NULL,NULL,10.00,20.00,NULL,2,0,NULL,NULL,NULL,NULL,'2018-09-30 02:52:21',NULL,NULL,1),(418,466,39,NULL,NULL,NULL,NULL,NULL,5.00,5.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:52:21',NULL,NULL,1),(419,466,41,NULL,NULL,NULL,NULL,NULL,16.00,16.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:52:21',NULL,NULL,1),(420,466,48,NULL,NULL,NULL,NULL,NULL,5.00,10.00,NULL,2,0,NULL,NULL,NULL,NULL,'2018-09-30 02:52:21',NULL,NULL,1),(421,466,49,NULL,NULL,NULL,NULL,NULL,6.00,6.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:52:21',NULL,NULL,1),(422,467,42,NULL,NULL,NULL,NULL,NULL,26.00,26.00,NULL,1,0,NULL,NULL,NULL,NULL,'2018-09-30 02:53:39',NULL,NULL,1),(423,467,48,NULL,NULL,NULL,NULL,NULL,5.00,25.00,NULL,5,0,NULL,NULL,NULL,NULL,'2018-09-30 02:53:39',NULL,NULL,1);
/*!40000 ALTER TABLE `order_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_line_section`
--

DROP TABLE IF EXISTS `order_line_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_line_section` (
  `idorder_line_section` int(11) NOT NULL AUTO_INCREMENT,
  `section_idsection` int(11) NOT NULL,
  `order_line_idorder_line` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `date_stock` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expiration_date` timestamp NULL DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idorder_line_section`),
  KEY `fk_order_line_section_section1_idx` (`section_idsection`),
  KEY `fk_order_line_section_order_line1_idx` (`order_line_idorder_line`),
  CONSTRAINT `fk_order_line_section_order_line1` FOREIGN KEY (`order_line_idorder_line`) REFERENCES `order_line` (`idorder_line`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_line_section_section1` FOREIGN KEY (`section_idsection`) REFERENCES `section` (`idsection`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_line_section`
--

LOCK TABLES `order_line_section` WRITE;
/*!40000 ALTER TABLE `order_line_section` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_line_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_payment`
--

DROP TABLE IF EXISTS `order_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_payment` (
  `idorder_payment` int(11) NOT NULL AUTO_INCREMENT,
  `payment_mtd_idpayment_mtd` int(11) NOT NULL,
  `order_idorder` int(11) NOT NULL,
  `CreditNoteID` int(11) DEFAULT NULL COMMENT 'OrderID for RefundNote',
  `value` decimal(11,2) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idorder_payment`),
  KEY `fk_order_payment_order1_idx` (`order_idorder`),
  KEY `fk_order_payment_payment_mtd1_idx` (`payment_mtd_idpayment_mtd`),
  CONSTRAINT `fk_order_payment_order1` FOREIGN KEY (`order_idorder`) REFERENCES `order` (`idorder`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_payment_payment_mtd1` FOREIGN KEY (`payment_mtd_idpayment_mtd`) REFERENCES `payment_mtd` (`idpayment_mtd`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_payment`
--

LOCK TABLES `order_payment` WRITE;
/*!40000 ALTER TABLE `order_payment` DISABLE KEYS */;
INSERT INTO `order_payment` VALUES (1,1,443,NULL,17.00,'2018-09-30 00:01:32',NULL,NULL,1),(2,1,444,NULL,12.00,'2018-09-30 00:08:08',NULL,NULL,1),(3,1,445,NULL,12.00,'2018-09-30 00:10:34',NULL,NULL,1),(4,1,446,NULL,20.00,'2018-09-30 00:53:47',NULL,NULL,1),(5,5,446,NULL,20.00,'2018-09-30 00:53:47',NULL,NULL,1),(6,1,454,NULL,13.50,'2018-09-30 01:16:34',NULL,NULL,1),(7,1,453,NULL,42.00,'2018-09-30 01:19:58',NULL,NULL,1),(8,1,456,NULL,10.00,'2018-09-30 01:22:52',NULL,NULL,1),(9,1,457,NULL,3.50,'2018-09-30 01:25:17',NULL,NULL,1),(10,1,458,NULL,10.00,'2018-09-30 02:05:22',NULL,NULL,1),(11,7,459,NULL,12.00,'2018-09-30 02:08:05',NULL,NULL,1),(12,1,460,NULL,50.00,'2018-09-30 02:13:23',NULL,NULL,1),(13,1,461,NULL,15.00,'2018-09-30 02:41:01',NULL,NULL,1),(14,1,463,NULL,45.00,'2018-09-30 02:42:38',NULL,NULL,1),(15,1,464,NULL,100.00,'2018-09-30 02:44:24',NULL,NULL,1),(16,1,465,NULL,30.00,'2018-09-30 02:49:54',NULL,NULL,1),(17,1,466,NULL,100.00,'2018-09-30 02:52:21',NULL,NULL,1),(18,1,467,NULL,100.00,'2018-09-30 02:53:39',NULL,NULL,1);
/*!40000 ALTER TABLE `order_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_rule`
--

DROP TABLE IF EXISTS `order_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_rule` (
  `idorderrule` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `alternative_price` tinyint(4) DEFAULT NULL,
  `show_popup` tinyint(4) DEFAULT NULL,
  `alternative_vat` tinyint(4) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idorderrule`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_rule`
--

LOCK TABLES `order_rule` WRITE;
/*!40000 ALTER TABLE `order_rule` DISABLE KEYS */;
INSERT INTO `order_rule` VALUES (1,'Rule Juice',1,1,1,'2017-05-25 13:48:58','2017-05-25 13:49:03',NULL,1),(2,'fsdfds',1,1,1,'2018-04-19 15:01:07','2018-04-19 15:01:16','2018-04-19 15:01:24',3);
/*!40000 ALTER TABLE `order_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tmpt`
--

DROP TABLE IF EXISTS `order_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tmpt` (
  `idordertmpt` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `withdrawal_from_stock` int(11) DEFAULT NULL,
  `sell_by_invoice` int(11) DEFAULT NULL,
  `include_in_stock_statistics` int(11) DEFAULT NULL,
  `change_print_send_method` int(11) DEFAULT NULL,
  `print_delivery_list` int(11) DEFAULT NULL,
  `print_invoice` int(11) DEFAULT NULL,
  `template_order` int(11) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idordertmpt`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tmpt`
--

LOCK TABLES `order_tmpt` WRITE;
/*!40000 ALTER TABLE `order_tmpt` DISABLE KEYS */;
INSERT INTO `order_tmpt` VALUES (1,'Default',1,1,1,1,1,1,1,'2017-05-25 14:16:41','2018-04-19 14:44:18',NULL,1);
/*!40000 ALTER TABLE `order_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tmpt_epost`
--

DROP TABLE IF EXISTS `order_tmpt_epost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tmpt_epost` (
  `id_order_tmpt_epost` int(11) NOT NULL AUTO_INCREMENT,
  `data_type` varchar(150) NOT NULL,
  `data_value` text NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id_order_tmpt_epost`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tmpt_epost`
--

LOCK TABLES `order_tmpt_epost` WRITE;
/*!40000 ALTER TABLE `order_tmpt_epost` DISABLE KEYS */;
INSERT INTO `order_tmpt_epost` VALUES (1,'invoice_text_sent_email_title','Test','2017-05-25 14:17:14',NULL,NULL,1),(2,'invoice_text_first_email','Test','2017-05-25 14:17:14',NULL,NULL,1),(3,'invoice_text_second_email','Test','2017-05-25 14:17:14',NULL,NULL,1),(4,'proposals_text_sent_email_title','Test','2017-05-25 14:17:14',NULL,NULL,1),(5,'proposals_text','Test','2017-05-25 14:17:14',NULL,NULL,1);
/*!40000 ALTER TABLE `order_tmpt_epost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tmpt_footer`
--

DROP TABLE IF EXISTS `order_tmpt_footer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tmpt_footer` (
  `idordertmptfooter` int(11) NOT NULL AUTO_INCREMENT,
  `idordertmpt` int(11) NOT NULL,
  `footer_1` text,
  `footer_2` text,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idordertmptfooter`),
  KEY `idordertmpt_indx_1` (`idordertmpt`),
  CONSTRAINT `fk_idorder_tmpt_1` FOREIGN KEY (`idordertmpt`) REFERENCES `order_tmpt` (`idordertmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tmpt_footer`
--

LOCK TABLES `order_tmpt_footer` WRITE;
/*!40000 ALTER TABLE `order_tmpt_footer` DISABLE KEYS */;
INSERT INTO `order_tmpt_footer` VALUES (1,1,'Test','Test',1);
/*!40000 ALTER TABLE `order_tmpt_footer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tmpt_img`
--

DROP TABLE IF EXISTS `order_tmpt_img`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tmpt_img` (
  `idordertmptimg` int(11) NOT NULL AUTO_INCREMENT,
  `idordertmpt` int(11) NOT NULL,
  `image` longblob NOT NULL,
  `image_type` varchar(10) NOT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idordertmptimg`),
  KEY `idordertmpt_indx` (`idordertmpt`),
  CONSTRAINT `fk_idorder_tmpt` FOREIGN KEY (`idordertmpt`) REFERENCES `order_tmpt` (`idordertmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tmpt_img`
--

LOCK TABLES `order_tmpt_img` WRITE;
/*!40000 ALTER TABLE `order_tmpt_img` DISABLE KEYS */;
INSERT INTO `order_tmpt_img` VALUES (1,1,_binary 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAkFBMVEWwCAi+DAz/1gD/2gC8AAz/2AD/3ACtAAj/3wC6AA2wAAjFXgb/4QCqAAizCQm3AA3ckATKSQrSeAXdhAjUgAayGgjlpAT0vAT6ywK8PAfLZwbAGgzAUAf0wATXcgn5zgLEMwvaiAbtrQXstQPJQgvOVAq4LwfCKQvimgTmrgS6RAfOcQbTYwqxJgjllgfFWAcOz0ZxAAAH3klEQVR4nO2Za3eiOhSGKySBUBBrW6Raq3irrR39///ucMkNSOLGOuucdVbeLzNqSh73PfFh9N/Uw78NYJDjGibHNUyOa5gc1zA5rmFyXMPkuIbJcQ2T4xomxzVMjmuYHNcwOa5hclzD5LiGyXEN0y1cqBRwGWSdVoO5EB3tf/Y7Sq+uW+8v+wW9kWwgV7lblq88b7WcWm2B0Mc29zwv3+xuIxvGhUZHQnAlP84/zCajP8vYr9eROPu8BWwQF1pvQ+wx+f7ZZDL6jH2+zAuXT9d8/ksutPOIpyje6sFoJumrL0BuABvAhdZLwjbym33DTMdFz2H9KS7XNetXT4NdCedCqMHC4WaS5YSB9S1Bi5DRZMWm+QK+txgKBuei57jejzwjSrnpwl7w0z1pWPIdoujDq1+QzV/jQh+NU+KPtHq1mNUvsddxEfpc1SQ4r9OQ/jRfJnweGGJgLprXIGSWNvuvG2+RbXtDdGSG/Gl46bFeh8loiMXGYygXC2a82rHnp8+E2U8Fo3vm7GPKOD/zwZ4cP5SCLUW7xj1EZCBa56rD+LvLrnvpM8vOPRCspoJy0YxZRyYWZQYjZ7kh4u8dpRFTlpNLQIRRBgXlQp/MPdtUeZcZAkvWRROEHlGygZez+KrBxpIKyEU3zA4/yrPTY9c49LnB92cKPnpiGTqzc6lQQC4eXXi5VmPp0nDhFY8w1IRcmQsqApo1f+xdzGDjDhWQi7nCP7ZihNUwj0yaDdG0eY1zFX9Ep6ykbEwR1oOCcq1wqyjxDZkj/Zy9sWGc7ZrAg9MLtd2I6qhAXOijeTD2OzV0z8aLuOZFT2x/f9reP+XZcO4bTA8F46LcX61srEjYlOVvqg/SM8PE6zaXGDCWiw6UkQrChfYrXThXJCxN6zKKFmwZzrv4P4wfz9UHWKBgXM98924FoszB1YwhGnvfXWjH8lQpt4aoGuTHJUuzZXdSR08Ys4K1GKGMR1uvgKItewJGEFPBuNCah3Ov9ZYTLBuYwzUfcJTWLkTPKrItqgZw8fmzdFYvnVDGfBcW6Q/H3y56j+CfkU0EYYJxzfje/QZHC56Ry5S7kRx7y9CIceH8FQp2DYu3t3LDflUUFax0Hl/WrV6VUv6hNw/uxcXTrJf+tSHEwS1bsf/oBi1eUDxSJPfiEv7RnH3KUs4NIf/VNJuUx6i/vBcXr5ZePNVxbVon3SbSuovG4+CRL4uhAXbNXDsWsl6oG1P4lCwlJnsBVW4SvYtvBw2wK1xiY6w9NCNeAYRaJzJe1aMDL3QkAzryCle65WHfq/Y11xPucCnVnspdIvmYCOZIOxZa8MD2t9rP13kbDPs7Xa9JeAHGq2+YI69w7Xmi+f1yWYnPrIKrmVW7rSYpeODj0124pvx5fqGdgkUn4svKLqSZFYK5SJ/JXbiOnIvMtfai5zYXybThExxiAX4PLukm0psSmEE79jKYI+FceJWAAt9urk/MwzrUH7LELMq5XgxcoiHEB5DB7FyiqmKv3x2bFZ24N9TNZMnXASurnWsqwqLfXphwu1AYykCSiUA9giqrlYvKsM8MXKmncmHvjz56Et65y5n791wpbx/asx9bojqyjGr9NsGJc+H8ARL4Vq6RmOdC3TRRapy0Jgqcm7jERAGs+DYqOat2rwA41fghOapc/tbAFV14HGLv8ddccxE7RDPc12U9eFEnHeO4EH2Lr0imv+YSRbN37yw6oOwwtVkLw57Rm2jwBNSJrFxnERT5znCEDy4tLpMtoj8yhUCHNRvXSMR0a/pqteXoW83H2Bw7fAIrSyEkIW1YsjtWB30NVM2lFrD43cSVfMmYgMz4Nq5PEav+poHqTzBK4FTr3kxbJnIgAp09bOH1KVKtupvQX8FEB4ULe2auiagnMaSA2bhE1y6n1dRw21EGtAwwvDoYuQrJBencNi552PEnpqYWvW4VrtzQHksu0YjKYgLokBYu5XBoPr9HD1+Sy18auYK5fBjkrGbjEuXLIyfjo4JMNiJ/awzp4F0a39SsgFzKIZ+YQyJRub6MpSl4E1wYcklh4Ypmsj1eQFwkM+4TvUquHHC2NVKVo4KsAMSc2snEV7iMy6JAxJcla69xVbUqilaSy3xYSAqFyzIjJ0RyAQqY1lR1rYoOCpe5RCcvKpd5Q4XLM0eFmUuU9UDpfL45ItpcFnspfRRQWLWmargu8hIQm/cLTgqXsfyWXLlcB7ij0Jqq2VBOq9izcM2Vumo41dZc8oaFWJb1ubrDQnCCcT3+TS7a/yR4EZHqm4451bKLjBvfMronW1mlTdN2h4tqfxoJ5ARgu0YGc8n6a0tbyaUxVY/LckYO3odzmcuvkPlHpEBOcrZOG7z7MK7jIC6zguNgLtNtTpfr6xdYYK43DOOayKl8C7qiMChSA+IOXIXkmv2G6+EvcoF/7tNJGQstfS94C0PSKAxtXPIiw19WXEFyU/RH8oRs5YoOEynLABNMJVf182gyn0yDG8wG5SonPkXmZQpX/bPtZPo9P97gT/UABrwTtarD9V28nLIT6Cqsy6U0Wsv8cgvX6jX4nr7MindAA9dx4UZeeA+uU+xh/sDX6K2YXooX4zWLjWuL72qvkzw3rV6j4FR8n4qbysX7o5DxOgSu6CAf91i+Tt5O77d9WyXNfo/Vzlv2+g5PdXJycnJycnJycnJycnJycnJycvrf6x+h0J2r/WsjDQAAAABJRU5ErkJggg==','image',1);
/*!40000 ALTER TABLE `order_tmpt_img` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_tmpt_printer_settings`
--

DROP TABLE IF EXISTS `order_tmpt_printer_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_tmpt_printer_settings` (
  `id_order_tmpt_printer_settings` int(11) NOT NULL AUTO_INCREMENT,
  `id_order_tmpt` int(11) NOT NULL,
  `id_printer` int(11) DEFAULT NULL,
  `numbeOfWaysToPrint` int(11) DEFAULT NULL,
  `printValues` varchar(10) DEFAULT NULL,
  `printPricesWithTaxes` varchar(10) DEFAULT NULL,
  `hideTotalPrice` varchar(10) DEFAULT NULL,
  `printObs` varchar(10) DEFAULT NULL,
  `printTheObsInASeparateLine` varchar(10) DEFAULT NULL,
  `linesWithDifferentBackgrounds` varchar(10) DEFAULT NULL,
  `sendByEmail` varchar(10) DEFAULT NULL,
  `sendBySMS` varchar(10) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id_order_tmpt_printer_settings`),
  KEY `id_order_tmpt` (`id_order_tmpt`),
  KEY `id_printer` (`id_printer`),
  CONSTRAINT `fk_idorder_tmpt_2` FOREIGN KEY (`id_order_tmpt`) REFERENCES `order_tmpt` (`idordertmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_idprinter` FOREIGN KEY (`id_printer`) REFERENCES `printer` (`idprinter`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_tmpt_printer_settings`
--

LOCK TABLES `order_tmpt_printer_settings` WRITE;
/*!40000 ALTER TABLE `order_tmpt_printer_settings` DISABLE KEYS */;
INSERT INTO `order_tmpt_printer_settings` VALUES (1,1,NULL,1,'1','1','1','1','1','1','1','1','2017-05-25 14:16:41','2017-05-25 14:16:59',NULL,1);
/*!40000 ALTER TABLE `order_tmpt_printer_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_mtd`
--

DROP TABLE IF EXISTS `payment_mtd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_mtd` (
  `idpayment_mtd` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `path_platform` varchar(500) DEFAULT NULL COMMENT 'An URL to bring the user to the payment platform of this payment method.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `invoice` tinyint(2) NOT NULL DEFAULT '1',
  `deadlinemonth` timestamp NULL DEFAULT NULL,
  `deadlineyear` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idpayment_mtd`)
) ENGINE=InnoDB AUTO_INCREMENT=10000 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_mtd`
--

LOCK TABLES `payment_mtd` WRITE;
/*!40000 ALTER TABLE `payment_mtd` DISABLE KEYS */;
INSERT INTO `payment_mtd` VALUES (1,'Dinheiro',NULL,'2018-08-07 17:59:13',NULL,NULL,1,1,NULL,NULL),(2,'Visa',NULL,'2018-08-07 18:16:51',NULL,NULL,1,1,NULL,NULL),(3,'Mastercard',NULL,'2018-08-07 18:16:55',NULL,NULL,1,1,NULL,NULL),(4,'American Express',NULL,'2018-08-07 18:17:00',NULL,NULL,1,1,NULL,NULL),(5,'Visa Electron',NULL,'2018-08-07 18:17:43',NULL,NULL,1,1,NULL,NULL),(6,'Maestro',NULL,'2018-08-07 18:17:46',NULL,NULL,1,1,NULL,NULL),(7,'ELO',NULL,'2018-08-07 18:18:00',NULL,NULL,1,1,NULL,NULL),(9999,'Refund Note',NULL,'2018-09-13 19:20:49',NULL,NULL,1,1,NULL,NULL);
/*!40000 ALTER TABLE `payment_mtd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_mtd_state`
--

DROP TABLE IF EXISTS `payment_mtd_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_mtd_state` (
  `idpayment_mtd_state` int(11) NOT NULL AUTO_INCREMENT,
  `zz_state_idzz_state` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `payment_mtd_idpayment_mtd` int(11) NOT NULL,
  PRIMARY KEY (`idpayment_mtd_state`),
  KEY `fk_payment_mtd_state_payment_mtd1_idx` (`payment_mtd_idpayment_mtd`),
  KEY `fk_payment_mtd_state_zz_state1_idx` (`zz_state_idzz_state`),
  CONSTRAINT `fk_payment_mtd_state_payment_mtd1` FOREIGN KEY (`payment_mtd_idpayment_mtd`) REFERENCES `payment_mtd` (`idpayment_mtd`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_mtd_state_zz_state1` FOREIGN KEY (`zz_state_idzz_state`) REFERENCES `zz_state` (`idzz_state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_mtd_state`
--

LOCK TABLES `payment_mtd_state` WRITE;
/*!40000 ALTER TABLE `payment_mtd_state` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_mtd_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `idpermission` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL COMMENT 'Permission name.',
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`idpermission`)
) ENGINE=InnoDB AUTO_INCREMENT=356 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (1,'Read Reports on Dashboard','Read reports on dashboard'),(2,'Read Company info','Read company info'),(3,'Update Company info','Update company info'),(4,'Read General Settings','Read general settings'),(5,'Update General Settings','Update general settings'),(6,'Read Accounting','Read accounting'),(7,'Update Accounting','Update accounting'),(8,'Create Card Accounts/Type of Operations','Create card accounts/type of operations'),(9,'Update Card Accounts/Type of Operations','Update card accounts/type of operations'),(10,'Read Card Accounts/Type of Operations','Read card accounts/type of operations'),(11,'Read Additional Settings','Read additional settings'),(12,'Update Additional Settings','Update additional settings'),(13,'Create Credit Administrator','Create credit administrator'),(14,'Update Credit Administrator','Update credit administrator'),(15,'Read Credit Administrator','Read credit administrator'),(16,'Read Others Servers Of Store','Read others servers of store'),(17,'Import data of products','Import data of products'),(18,'Read Integrations','Read integrations'),(19,'Update Integrations','Update integrations'),(20,'Import Bestseller','Import bestseller'),(21,'Import Barcode','Import barcode'),(22,'System Booking','System booking'),(23,'Create Demo','Create Demo'),(24,'Specify number initial of the order of buy','Specify number initial of the order of buy'),(25,'Reset database','Reset database'),(26,'Import clients','Import clients'),(27,'Import requests','Import requests'),(28,'Plane of numeration of the items in the system','Plane of numeration of the items in the system'),(29,'Search for clients sex in base national','Search for clients sex in base national'),(30,'Search for clients age in base national','Search for clients age in base national'),(31,'Search for weather information','Search for weather information'),(32,'Read databases','Read databases'),(33,'Reading of log','Reading of log'),(34,'Webshop','Webshop'),(35,'Import data of the .xls','Import data of the .xls'),(36,'Verify duplicates clients','Verify duplicates clients'),(37,'Import banking return','Import banking return'),(38,'Change existing shopping orders','Change existing shopping orders'),(39,'Codes of Zipcode','Codes of Zipcode'),(40,'Program of fidelity','Program of fidelity'),(41,'Create Currencies','Read currencies'),(42,'Update Currencies','Update currencies'),(43,'Read Currencies','Create currencies'),(44,'Delete Currencies','Create currencies'),(45,'Manage Devices','Manage Devices'),(46,'Update Features','Update Features'),(47,'Read Features','Read Features'),(48,'Update Delivery List','Update delivery list'),(49,'Read Delivery List','Read delivery list'),(50,'Update Closing Parameters','Update closing parameters'),(51,'Read Closing Parameters','Read closing parameters'),(52,'Update Credit Notes','Update credit notes'),(53,'Read Credit Notes','Read credit notes'),(54,'Update Receipts','Update receipts'),(55,'Read Receipts','Read receipts'),(56,'Create Layout POS','Create layout pos'),(57,'Update Layout POS','Update layout pos'),(58,'Read Layout POS','Read layout pos'),(59,'Create Layout Tables','Create layout tables'),(60,'Update Layout Tables','Update layout tables'),(61,'Read Layout Tables','Read layout tables'),(62,'Create Printers','Create printers'),(63,'Update Printers','Update printers'),(64,'Read Printers','Read printers'),(65,'Create Type of Printers','Create type of printers'),(66,'Update Type of Printers','Update type of printers'),(67,'Read Type of Printers','Read type of printers'),(68,'Display LED','Create type of printers'),(69,'Automatic Payment Machine','Automatic payment machine'),(70,'Payment Terminal','Payment terminal'),(71,'Update Signature','Update signature'),(72,'Read Signature','Read signature'),(73,'Update Wifi','Update wifi'),(74,'Read Wifi','Read wifi'),(75,'Update iBeacon','Update iBeacon'),(76,'Read iBeacon','Read iBeacon'),(77,'Update RFID','Update RFID'),(78,'Read RFID','Read RFID'),(79,'Update Facil Recognition','Update facil recognition'),(80,'Read Facil Recognition','Read facil recognition'),(81,'Update Tracking','Update tracking'),(82,'Read Tracking','Read tracking'),(83,'Read product report','Read product report'),(84,'Print Labels','Print labels'),(85,'Balance in Stock','Balance in stock'),(86,'Round Quantity','Round quantity'),(87,'Read Voucher Report','Read voucher report'),(88,'Print voucher report','Print voucher report'),(89,'Move voucher report','Move voucher report'),(90,'Change items voucher report','Change items voucher report'),(91,'Remove customer in voucher report','Remove customer in voucher report'),(92,'Update item in voucher report','Update item in voucher report'),(93,'Duplicate note in voucher report','Duplicate note in voucher report'),(94,'Create warnings in case of delay','Create warnings in case of delay'),(95,'Update warnings in case of delay','Update warnings in case of delay'),(96,'Read warnings in case of delay','Read warnings in case of delay'),(97,'Delete warnings in case of delay','Delete warnings in case of delay'),(98,'Create report credit manager','Create report credit manager'),(99,'Update report credit manager','Update report credit manager'),(100,'Read report credit manager','Read report credit manager'),(101,'Delete report credit manager','Delete report credit manager'),(102,'Read closing the carton','Read closing the carton'),(103,'Print closing the carton','Print closing the carton'),(104,'Read customers','Read customers'),(105,'Print customers','Print customers'),(106,'Read suppliers','Read suppliers'),(107,'Print suppliers','Print suppliers'),(108,'Create acquisitions','Create acquisitions'),(109,'Update acquisitions','Update acquisitions'),(110,'Read acquisitions','Read acquisitions'),(111,'Delete acquisitions','Delete acquisitions'),(112,'Print labels in acquisitions','Print labels in acquisitions'),(113,'Balance stock in acquisitions','Balance stock in acquisitions'),(114,'Roud Quantities in acquisitions','Roud Quantities in acquisitions'),(115,'Create Receipt Of Purchase Order','Create Receipt Of Purchase Order'),(116,'Update Receipt Of Purchase Order','Update Receipt Of Purchase Order'),(117,'Read Receipt Of Purchase Order','Read Receipt Of Purchase Order'),(118,'Delete Receipt Of Purchase Order','Delete Receipt Of Purchase Order'),(119,'Print labels in Receipt Of Purchase Order','Print labels in Receipt Of Purchase Order'),(120,'Balance stock in Receipt Of Purchase Order','Balance stock in Receipt Of Purchase Order'),(121,'Roud Quantities in Receipt Of Purchase Order','Roud Quantities in Receipt Of Purchase Order'),(122,'Read Suggestions for Buying','Read Suggestions for Buying'),(123,'Create make a suggestion in suggestions for buying','Create make a suggestion in suggestions for buying'),(124,'Create Turn into purchase order in suggestions for buying','Create Turn into purchase order in suggestions for buying'),(125,'Import Suggestions for Buying','Import Suggestions for Buying'),(126,'Read Inventory','Read inventory'),(127,'Print Inventory','Print inventory'),(128,'Send inventory report via email','Send inventory report via email'),(129,'Download pdf inventory report','Download pdf inventory report'),(130,'Read Inventory','Read inventory'),(131,'Update Inventory','Update inventory'),(132,'Import Inventory','Import inventory'),(133,'Move Stock','Move Stock'),(134,'Import Stock','Import Stock'),(135,'Open list from stock','Open list from stock'),(136,'Save list from stock','Save list from stock'),(137,'Read Orders','Read Orders'),(138,'Print voucher report in orders','Print voucher report in orders'),(139,'Move voucher report in orders','Move voucher report in orders'),(140,'Change items voucher report in orders','Change items voucher report in orders'),(141,'Remove customer in voucher report in orders','Remove customer in voucher report in orders'),(142,'Update item in voucher report in orders','Update item in voucher report in orders'),(143,'Duplicate note in voucher report in orders','Duplicate note in voucher report in orders'),(144,'Read Mobile in Orders','Read Mobile in Orders'),(145,'Print Mobile in Orders','Print Mobile in Orders'),(146,'Read Statistics','Read Statistics'),(147,'Create template in statistics','Create template in statistics'),(148,'Print reports in statistics','DePrint reports in statisticse'),(149,'Send for email reports in statistics','Send for email reports in statistics'),(150,'Send for sms reports in statistics','Send for sms reports in statistics'),(151,'Create graphics based in statistics','Create graphics based in statistics'),(152,'Read orders in statistics','Read orders in statistics'),(153,'Print orders in statistics','Print orders in statistics'),(154,'Read products in statistics','Read orders in statistics'),(155,'Print products in statistics','Print orders in statistics'),(156,'Create product statistics template','Create product statistics template'),(157,'Send email for product statistics','Send email for product statistics'),(158,'Read serial number in statistics','Read orders in statistics'),(159,'Print serial number in statistics','Print orders in statistics'),(160,'Read projects in statistics','Read orders in statistics'),(161,'Print projects in statistics','Read orders in statistics'),(162,'Read inventory statistics','Read inventory statistics'),(163,'Create template inventory statistics','Create template inventory statistics'),(164,'Print inventory statistics','Print inventory statistics'),(165,'Send for email inventory statistics','Send for email reports in statistics'),(166,'Send for sms inventory statistics','Send for sms reports in statistics'),(167,'Save pdf inventory statistics','Save pdf inventory statistics'),(168,'Read projects statistics','Read projects statistics'),(169,'Read invoice statistics','Read invoice statistics'),(170,'Print invoice statistics','Print invoice statistics'),(171,'Print invoice report statistics','Print invoice report statistics'),(172,'Send for email invoice statistics','Send for email invoice statistics'),(173,'Create credit for invoice','Create credit for invoice'),(174,'Duplicate invoice','Duplicate invoice'),(175,'Read Daily invoice closure','Read daily invoice closure'),(176,'Print daily invoice closure','Print invoice statistics'),(177,'Read Logs','Read Logs'),(178,'Print Logs','Print Logs'),(179,'Read Requests','Read Requests'),(180,'Print Requests','Print Requests'),(181,'Finance situation','Finance situation'),(182,'Delivery status','Delivery status'),(183,'Open order online','Open order online'),(184,'Notify output','Notify output'),(185,'Update sales resume','Update sales resume'),(186,'Read sales resume','Read sales resume'),(187,'Read payments made in the webshop','Read payments made in the webshop'),(188,'Payment Options','Payment options'),(189,'Cashier closing','Cashier closing'),(190,'Report cashier closing','Report cashier closing'),(191,'Create Department','Create department'),(192,'Update Department','Update department'),(193,'Read Department','Read department'),(194,'Delete Department','Delete department'),(195,'Create Project','Create project'),(196,'Update Project','Update project'),(197,'Read Project','Read project'),(198,'Delete Project','Delete project'),(199,'Create User','Create user'),(200,'Update User','Update user'),(201,'Read User','Read user'),(202,'Delete User','Delete user'),(203,'Update Permissions','Update permissions'),(204,'Read Permissions','Read permissions'),(205,'Remove Permissions','remove permissions'),(206,'Update permissions by type users','Update permissions by type users'),(207,'Read permissions by type users','Read permissions by type users'),(208,'Delete permissions by type users','Delete permissions by type users'),(209,'Create User Type','Create user type'),(210,'Update User Type','Update user type'),(211,'Read User Type','Read user type'),(212,'Delete User Type','Delete user type'),(213,'Create Jobs Type','Create jobs type'),(214,'Update Jobs Type','Update jobs type'),(215,'Read Jobs Type','Read jobs type'),(216,'Delete Jobs Type','Delete jobs type'),(217,'Create Stock','Create stock'),(218,'Update Stock','Update stock'),(219,'Read Stock','Read stock'),(220,'Delete Stock','Delete stock'),(221,'Create Hall','Create hall'),(222,'Update Hall','Update hall'),(223,'Read Hall','Read hall'),(224,'Delete Hall','Delete hall'),(225,'Create Wardrobes','Create wardrobes'),(226,'Update Wardrobes','Update wardrobes'),(227,'Read Wardrobes','Read wardrobes'),(228,'Delete Wardrobes','Delete wardrobes'),(229,'Create Product Size','Create product size'),(230,'Update Product Size','Update product size'),(231,'Read Product Size','Read product size'),(232,'Delete Product Size','Delete product size'),(233,'Create Color','Create color'),(234,'Update Color','Update color'),(235,'Read Color','Read color'),(236,'Delete Color','Delete color'),(237,'Create Country','Create country'),(238,'Update Country','Update country'),(239,'Read Country','Read country'),(240,'Delete Country','Delete country'),(241,'Create Supplier','Create supplier'),(242,'Update Supplier','Update supplier'),(243,'Read Supplier','Read supplier'),(244,'Delete Supplier','Delete supplier'),(245,'Create APIs','Create APIs'),(246,'Update APIs','Update APIs'),(247,'Read APIs','Read APIs'),(248,'Delete APIs','Delete APIs'),(249,'Create Template','Create template'),(250,'Update Template','Update template'),(251,'Read Template','Read template'),(252,'Delete Template','Delete template'),(253,'Cards to employeers','Defined image from cards to employeers'),(254,'Cards from access to employeers','Defined image from cards from access to employeers'),(255,'Layout from labels','Defined image for layout from labels'),(256,'Create Printer Settings','Create printer settings'),(257,'Update Printer Settings','Update printer settings'),(258,'Read Printer Settings','Read printer settings'),(259,'Delete Printer Settings','Delete printer settings'),(260,'Update E-Post','Update e-post'),(261,'Read E-Post','Read e-post'),(262,'Create Customer','Create customer'),(263,'Update Customer','Update customer'),(264,'Read Customer','Read customer'),(265,'Delete Customer','Delete customer'),(266,'Create Discount','Create discount'),(267,'Update Discount','Update discount'),(268,'Read Discount','Read discount'),(269,'Delete Discount','Delete discount'),(270,'Create Payment','Create payment'),(271,'Update Payment','Update payment'),(272,'Read Payment','Read payment'),(273,'Delete Payment','Delete payment'),(274,'Create Group CustGroupomer','Create group customer'),(275,'Update Group Customer','Update group customer'),(276,'Read Group Customer','Read group customer'),(277,'Delete Group Customer','Delete group customer'),(278,'Create Gift Card','Create gift card'),(279,'Update Gift Card','Update gift card'),(280,'Read Gift Card','Read gift card'),(281,'Delete Gift Card','Delete gift card'),(282,'Create Gift Card','Create gift card'),(283,'Update Gift Card','Update gift card'),(284,'Read Gift Card','Read gift card'),(285,'Delete Gift Card','Delete gift card'),(286,'Create Product','Create product'),(287,'Update Product','Update product'),(288,'Read Product','Read product'),(289,'Delete Product','Delete product'),(290,'Create Product Group','Create product group'),(291,'Update Product Group','Update product group'),(292,'Read Product Group','Read product group'),(293,'Delete Product Group','Delete product group'),(294,'Create Unit','Create unit'),(295,'Update Unit','Update unit'),(296,'Read Unit','Read unit'),(297,'Delete Unit','Delete unit'),(298,'Read Stock','Read stock'),(299,'Adjust Qtd','Adjust qtd'),(300,'Buy Item','Buy item'),(301,'Create Rule','Create rule'),(302,'Update Rule','Update rule'),(303,'Read Rule','Read rule'),(304,'Delete Rule','Delete rule'),(305,'Create Manufacturer','Create manufacturer'),(306,'Update Manufacturer','Update manufacturer'),(307,'Read Manufacturer','Read manufacturer'),(308,'Delete Manufacturer','Delete manufacturer'),(309,'Import Settings','Import settings'),(310,'Create Serie','Create serie'),(311,'Update Serie','Update serie'),(312,'Read Serie','Read serie'),(313,'Delete Serie','Delete serie'),(314,'Create Shipping Method','Create shipping method'),(315,'Update Shipping Method','Update shipping method'),(316,'Read Shipping Method','Read shipping method'),(317,'Delete Shipping Method','Delete shipping method'),(318,'Create Invoice','Create invoice'),(319,'Update Invoice','Update invoice'),(320,'Read Invoice','Read invoice'),(321,'Delete Invoice','Delete invoice'),(322,'Companies in bankruptcy','Companies in bankruptcy'),(323,'Print invoice','Print invoice'),(324,'Information for open tickets','Information for open tickets'),(325,'Create Default Alerts','Create default alerts'),(326,'Update Default Alerts','Update default alerts'),(327,'Read Default Alerts','Read default alerts'),(328,'Delete Default Alerts','Delete default alerts'),(329,'Create Default Alerts','Create default alerts'),(330,'Update Default Alerts','Update default alerts'),(331,'Read Default Alerts','Read default alerts'),(332,'Delete Default Alerts','Delete default alerts'),(333,'Automatic debit solution','Automatic debit solution'),(334,'Create Tax','Create tax'),(335,'Update Tax','Update tax'),(336,'Read Tax','Read tax'),(337,'Delete Tax','Delete tax'),(338,'Create Tributes','Create tributes'),(339,'Update Tributes','Update tributes'),(340,'Read Tributes','Read tributes'),(341,'Delete Tributes','Delete tributes'),(342,'Read Others Devices','Read Others Devices'),(343,'Create Company','Create company'),(344,'Update Company','Update company'),(345,'Read Company','Read company'),(346,'Delete Company','Delete company'),(347,'Read Bank Deposit','Read bank deposit'),(348,'Create Bank Deposit','Create bank deposit'),(349,'Delivery','Delivery'),(350,'POS Terminal','POS Terminal'),(351,'Accounting in reports','Accounting in reports'),(352,'Sales resume','Sales resume'),(353,'Webshop resume','Webshop resume'),(354,'External Resources','External Resources'),(355,'Manage multiple POS','Manage multiple POS');
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `idperson` int(11) NOT NULL AUTO_INCREMENT,
  `pos_idpos` int(11) DEFAULT NULL,
  `customer_gp_idcustomer_gp` int(11) DEFAULT NULL,
  `users_idusers` int(11) DEFAULT NULL COMMENT 'An employee (user) that will preferably deal with this customer',
  `zz_state_idzz_state` int(11) DEFAULT NULL,
  `person_settings_idperson_settings` int(11) DEFAULT NULL,
  `payment_mtd_idpayment_mtd` int(11) DEFAULT NULL,
  `department_iddepartment` int(11) DEFAULT NULL,
  `name` varchar(200) NOT NULL COMMENT 'The name of the person.',
  `document_type` varchar(20) DEFAULT NULL COMMENT 'The type of the document used to identify the person. Ex: CPF, National Register Number, etc.',
  `document_number` varchar(45) DEFAULT NULL COMMENT 'The number of the document used to identify the person.',
  `birth_date` varchar(45) DEFAULT NULL COMMENT 'The birth date.',
  `personal_number` varchar(45) DEFAULT NULL COMMENT 'A number of a second document (optional).',
  `contact_person` varchar(45) DEFAULT NULL COMMENT 'A person that can be contacted to pass some message to the customer (his mother, wife, etc).',
  `info` longtext,
  `genre` int(11) DEFAULT NULL COMMENT '0 = Female\n1 = Male',
  `electronic_identification` varchar(45) DEFAULT NULL,
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idperson`),
  UNIQUE KEY `document_number_UNIQUE` (`document_number`),
  UNIQUE KEY `electronic_identification_UNIQUE` (`electronic_identification`),
  KEY `fk_person_customer_gp1_idx` (`customer_gp_idcustomer_gp`),
  KEY `fk_person_zz_state1_idx` (`zz_state_idzz_state`),
  KEY `fk_person_pos1_idx` (`pos_idpos`),
  KEY `fk_person_payment_mtd1_idx` (`payment_mtd_idpayment_mtd`),
  KEY `fk_person_person_settings1_idx` (`person_settings_idperson_settings`),
  KEY `fk_person_department1_idx` (`department_iddepartment`),
  KEY `fk_person_users1_idx` (`users_idusers`),
  CONSTRAINT `fk_person_customer_gp1` FOREIGN KEY (`customer_gp_idcustomer_gp`) REFERENCES `customer_gp` (`idcustomer_gp`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_department1` FOREIGN KEY (`department_iddepartment`) REFERENCES `department` (`iddepartment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_payment_mtd1` FOREIGN KEY (`payment_mtd_idpayment_mtd`) REFERENCES `payment_mtd` (`idpayment_mtd`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_person_settings1` FOREIGN KEY (`person_settings_idperson_settings`) REFERENCES `person_settings` (`idperson_settings`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_users1` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_zz_state1` FOREIGN KEY (`zz_state_idzz_state`) REFERENCES `zz_state` (`idzz_state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,1,1,NULL,NULL,NULL,NULL,NULL,'CARLOS HENRIQUE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-17 11:30:14',NULL,NULL,1),(2,NULL,1,NULL,NULL,NULL,NULL,NULL,'Anders Salberg',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-18 11:37:54',NULL,NULL,1),(3,NULL,1,NULL,NULL,NULL,NULL,NULL,'Renato Rocha',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-25 19:13:02',NULL,NULL,1),(4,NULL,1,NULL,NULL,NULL,NULL,NULL,'ALICE TIBURCIO',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-04-19 14:13:49',NULL,NULL,1),(5,NULL,1,NULL,NULL,NULL,NULL,NULL,'Einar Morten SlapgÃ¥rd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-10 15:51:58',NULL,NULL,1),(6,NULL,1,NULL,NULL,NULL,NULL,NULL,'Wandeson Paiva',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-07-19 13:54:18',NULL,NULL,1);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_company`
--

DROP TABLE IF EXISTS `person_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_company` (
  `idperson_company` int(11) NOT NULL AUTO_INCREMENT,
  `person_idperson` int(11) NOT NULL,
  `company_idcompany` int(11) NOT NULL,
  PRIMARY KEY (`idperson_company`),
  KEY `fk_person_company_person1_idx` (`person_idperson`),
  KEY `fk_person_company_company1_idx` (`company_idcompany`),
  CONSTRAINT `fk_person_company_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_company_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_company`
--

LOCK TABLES `person_company` WRITE;
/*!40000 ALTER TABLE `person_company` DISABLE KEYS */;
INSERT INTO `person_company` VALUES (1,1,1);
/*!40000 ALTER TABLE `person_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_data`
--

DROP TABLE IF EXISTS `person_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_data` (
  `idperson_data` int(11) NOT NULL AUTO_INCREMENT,
  `person_idperson` int(11) NOT NULL,
  `data_type` varchar(250) NOT NULL COMMENT 'If this is a phone number, email, etc.',
  `data_value` varchar(250) NOT NULL COMMENT 'The value. Ex: number of the phone',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idperson_data`),
  KEY `fk_person_data_person1_idx` (`person_idperson`),
  CONSTRAINT `fk_person_data_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_data`
--

LOCK TABLES `person_data` WRITE;
/*!40000 ALTER TABLE `person_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_image`
--

DROP TABLE IF EXISTS `person_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_image` (
  `idperson_image` int(11) NOT NULL AUTO_INCREMENT,
  `person_idperson` int(11) NOT NULL,
  `path` longblob NOT NULL,
  `order` int(11) DEFAULT '0' COMMENT 'The images has a priority order: the lowest first.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `type_img` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`idperson_image`),
  KEY `fk_person_image_person1_idx` (`person_idperson`),
  CONSTRAINT `fk_person_image_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_image`
--

LOCK TABLES `person_image` WRITE;
/*!40000 ALTER TABLE `person_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_payment_mtd`
--

DROP TABLE IF EXISTS `person_payment_mtd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_payment_mtd` (
  `idperson_payment_mtd` int(11) NOT NULL AUTO_INCREMENT,
  `payment_mtd_idpayment_mtd` int(11) NOT NULL,
  `person_idperson` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idperson_payment_mtd`),
  KEY `fk_person_payment_mtd_payment_mtd1_idx` (`payment_mtd_idpayment_mtd`),
  KEY `fk_person_payment_mtd_person1_idx` (`person_idperson`),
  CONSTRAINT `fk_person_payment_mtd_payment_mtd1` FOREIGN KEY (`payment_mtd_idpayment_mtd`) REFERENCES `payment_mtd` (`idpayment_mtd`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_payment_mtd_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_payment_mtd`
--

LOCK TABLES `person_payment_mtd` WRITE;
/*!40000 ALTER TABLE `person_payment_mtd` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_payment_mtd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_settings`
--

DROP TABLE IF EXISTS `person_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_settings` (
  `idperson_settings` int(11) NOT NULL AUTO_INCREMENT,
  `tax_free` tinyint(2) DEFAULT NULL COMMENT 'Whether this customer is free from taxes',
  `inv_show_customer_name` tinyint(2) DEFAULT '1' COMMENT 'Whether the customers name should be shown in the invoice',
  `inv_group_invoices` tinyint(2) DEFAULT '1' COMMENT 'Whether the invoices should be grouped',
  `inv_tax_free` tinyint(2) DEFAULT '0' COMMENT 'Whether this customer is free from invoices fees',
  `inv_invoice_print_exclusive` tinyint(2) DEFAULT '0' COMMENT 'Whether the invoice should be printed exclusively by the invoice printer company',
  `inv_hide_products` tinyint(2) DEFAULT '0' COMMENT 'Whether the products in the invoice should be hidden',
  `inv_electronic_invoice` int(11) DEFAULT '0' COMMENT 'Whether the invoices should be electronic',
  `inv_info` mediumtext,
  `invoice_tmpt_idinvoice_tmpt` int(11) NOT NULL,
  `inv_email` varchar(100) DEFAULT NULL COMMENT 'An email to the invoice be sent to',
  `inv_track_email` tinyint(2) DEFAULT NULL COMMENT 'Whether the company would like to receive a message informing when the email was open',
  `currency_idcurrency` int(11) DEFAULT NULL COMMENT 'The default currency of this customer',
  `person_idperson` int(11) DEFAULT NULL COMMENT 'A person to redirect the invoices',
  `ord_show_info` tinyint(2) DEFAULT NULL COMMENT 'Whether it should be shown info about the customer when he is selected in the POS',
  `ord_hide_from_statistics` varchar(45) DEFAULT NULL COMMENT 'Whether this client data should be hidden from statistics',
  `ord_use_com_address` varchar(45) DEFAULT NULL COMMENT 'Wheter the commercial address should be used instead of the particular address in delivery',
  `ticket_tmpt_idticket_tmpt` int(11) DEFAULT NULL,
  `project_idproject` int(11) DEFAULT NULL,
  `ord_credit_limit` decimal(11,2) DEFAULT NULL COMMENT 'The limit of credit the customer has',
  `charge_times` int(11) DEFAULT '1' COMMENT 'How many times the customer should be charged before sending him to a charging office',
  `language_idlanguage` int(11) NOT NULL,
  `auth_receive_sms` tinyint(2) DEFAULT '0',
  `auth_receive_email` tinyint(2) DEFAULT '0',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idperson_settings`),
  KEY `fk_person_settings_invoice_tmpt1_idx` (`invoice_tmpt_idinvoice_tmpt`),
  KEY `fk_person_settings_currency1_idx` (`currency_idcurrency`),
  KEY `fk_person_settings_person1_idx` (`person_idperson`),
  KEY `fk_person_settings_ticket_tmpt1_idx` (`ticket_tmpt_idticket_tmpt`),
  KEY `fk_person_settings_project1_idx` (`project_idproject`),
  KEY `fk_person_settings_language1_idx` (`language_idlanguage`),
  CONSTRAINT `fk_person_settings_currency1` FOREIGN KEY (`currency_idcurrency`) REFERENCES `currency` (`idcurrency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_settings_invoice_tmpt1` FOREIGN KEY (`invoice_tmpt_idinvoice_tmpt`) REFERENCES `invoice_tmpt` (`idinvoice_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_settings_language1` FOREIGN KEY (`language_idlanguage`) REFERENCES `language` (`idlanguage`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_settings_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_settings_project1` FOREIGN KEY (`project_idproject`) REFERENCES `project` (`idproject`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_settings_ticket_tmpt1` FOREIGN KEY (`ticket_tmpt_idticket_tmpt`) REFERENCES `ticket_tmpt` (`idticket_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_settings`
--

LOCK TABLES `person_settings` WRITE;
/*!40000 ALTER TABLE `person_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos`
--

DROP TABLE IF EXISTS `pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos` (
  `idpos` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(300) NOT NULL,
  `macAddress` varchar(200) DEFAULT NULL,
  `srv` text NOT NULL,
  `idlicense` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `ticket_tmpt_idticket_tmpt` int(11) DEFAULT NULL,
  `temp_ticket_tmpt_idtemp_ticket_tmpt` int(11) DEFAULT NULL,
  `pos_number` varchar(45) NOT NULL,
  `valid_until` timestamp NULL DEFAULT NULL,
  `qty_users` varchar(45) NOT NULL,
  `qty_waiters` varchar(45) NOT NULL,
  `pos_company_idpos_company` int(11) NOT NULL,
  PRIMARY KEY (`idpos`),
  KEY `fk_pos_ticket_tmpt1_idx` (`ticket_tmpt_idticket_tmpt`),
  KEY `fk_pos_temp_ticket_tmpt1_idx` (`temp_ticket_tmpt_idtemp_ticket_tmpt`),
  KEY `idlicense` (`idlicense`),
  CONSTRAINT `fk_licence` FOREIGN KEY (`idlicense`) REFERENCES `license` (`idlicense`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pos_temp_ticket_tmpt1` FOREIGN KEY (`temp_ticket_tmpt_idtemp_ticket_tmpt`) REFERENCES `temp_ticket_tmpt` (`idtemp_ticket_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pos_ticket_tmpt1` FOREIGN KEY (`ticket_tmpt_idticket_tmpt`) REFERENCES `ticket_tmpt` (`idticket_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos`
--

LOCK TABLES `pos` WRITE;
/*!40000 ALTER TABLE `pos` DISABLE KEYS */;
INSERT INTO `pos` VALUES (1,'PC-EFFT0','14:10:9f:e3:bf:fb','C02K36EEFFT0',1,'2017-05-27 13:18:37',NULL,NULL,1,1,1,'',NULL,'','',0),(7,'PC-DEA55','64:1c:67:68:aa:e7','PEDEA55',1,'2017-06-19 16:49:23',NULL,NULL,1,NULL,NULL,'',NULL,'','',0),(8,'PC-OEM','undefined','OEM',1,'2017-08-14 18:41:19',NULL,NULL,1,NULL,NULL,'',NULL,'','',0),(9,'PC-80009','00:01:29:5d:76:d3','03000200-0400-0500-0006-000700080009',1,'2017-09-08 16:23:32',NULL,NULL,1,NULL,NULL,'',NULL,'','',0);
/*!40000 ALTER TABLE `pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_closure`
--

DROP TABLE IF EXISTS `pos_closure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_closure` (
  `idpos_closure` int(11) NOT NULL AUTO_INCREMENT,
  `user_iduser` int(11) NOT NULL,
  `pos_idpos` int(11) NOT NULL,
  `protocol` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount_counted` decimal(11,2) DEFAULT NULL COMMENT 'The amount counted by the user.',
  `amount_received` decimal(11,2) DEFAULT NULL COMMENT 'The amount that should be at this moment.',
  `amount_credit_card` decimal(11,2) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idpos_closure`),
  KEY `fk_pos_closure_user1_idx` (`user_iduser`),
  KEY `fk_pos_closure_pos1_idx` (`pos_idpos`),
  CONSTRAINT `fk_pos_closure_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pos_closure_user1` FOREIGN KEY (`user_iduser`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_closure`
--

LOCK TABLES `pos_closure` WRITE;
/*!40000 ALTER TABLE `pos_closure` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_closure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_company`
--

DROP TABLE IF EXISTS `pos_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_company` (
  `idpos_company` int(11) NOT NULL AUTO_INCREMENT,
  `zz_state_idzz_state` int(11) NOT NULL,
  `license` varchar(45) DEFAULT NULL,
  `name` varchar(100) NOT NULL COMMENT 'The official name of the company.',
  `fantasy_name` varchar(100) NOT NULL COMMENT 'A name that the company is known.',
  `register_number` varchar(45) NOT NULL COMMENT 'The National Register Number.',
  `address_street` varchar(100) NOT NULL,
  `address_number` varchar(14) DEFAULT NULL,
  `address_district` varchar(45) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_zipcode` varchar(45) NOT NULL,
  `address_complement` varchar(200) DEFAULT NULL,
  `address_reference` varchar(200) DEFAULT NULL,
  `vaddress_street` varchar(100) NOT NULL COMMENT 'Vaddress = visiting address.',
  `vaddress_number` varchar(14) DEFAULT NULL COMMENT 'Vaddress = visiting address.',
  `vaddress_district` varchar(45) NOT NULL COMMENT 'Vaddress = visiting address.',
  `vaddress_city` varchar(100) NOT NULL COMMENT 'Vaddress = visiting address.',
  `vaddress_zipcode` varchar(45) NOT NULL COMMENT 'Vaddress = visiting address.',
  `vaddress_complement` varchar(200) DEFAULT NULL COMMENT 'Vaddress = visiting address.',
  `vaddress_reference` varchar(200) DEFAULT NULL COMMENT 'Vaddress = visiting address.',
  `website` varchar(300) DEFAULT NULL,
  `bank_account` varchar(45) DEFAULT NULL,
  `iban` varchar(45) DEFAULT NULL,
  `bic` varchar(45) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Indicates the company status.:\n1-Active\n2-Blocked\n3-Deleted',
  `default_message_invoice` text,
  `assignor_code_br` varchar(45) NOT NULL,
  `bank_br` int(11) NOT NULL,
  `agency_br` varchar(45) NOT NULL,
  `account_br` varchar(45) NOT NULL,
  `digit_account_br` varchar(45) NOT NULL,
  `bank_wallet_br` varchar(45) NOT NULL,
  `specie_br` int(11) NOT NULL,
  `interest_day_maturity_invoice_br` int(11) NOT NULL,
  `days_after_maturity_invoice_br` int(11) NOT NULL,
  `value_of_mulct_br` decimal(11,2) NOT NULL,
  `code_agreement_br` varchar(45) NOT NULL,
  `wallet_variation_br` varchar(45) NOT NULL,
  `agency_dv_br` varchar(45) NOT NULL,
  `operation_br` varchar(45) NOT NULL,
  `sequence_number_file_br` varchar(45) NOT NULL,
  `wallet_code_br` int(11) NOT NULL,
  `registered_invoice_br` int(11) NOT NULL,
  `aceite_br` varchar(45) NOT NULL,
  PRIMARY KEY (`idpos_company`),
  KEY `fk_pos_company_zz_state1_idx` (`zz_state_idzz_state`),
  CONSTRAINT `fk_pos_company_zz_state1` FOREIGN KEY (`zz_state_idzz_state`) REFERENCES `zz_state` (`idzz_state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_company`
--

LOCK TABLES `pos_company` WRITE;
/*!40000 ALTER TABLE `pos_company` DISABLE KEYS */;
INSERT INTO `pos_company` VALUES (1,1,'demo','demo','demo','123123','demo',NULL,'demo','demo','demo',NULL,NULL,'demo',NULL,'demo','demo','',NULL,NULL,NULL,NULL,NULL,NULL,'2017-05-17 11:30:14',NULL,NULL,1,NULL,'',0,'','','','',0,0,0,0.00,'','','','','',0,0,'');
/*!40000 ALTER TABLE `pos_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_company_data`
--

DROP TABLE IF EXISTS `pos_company_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_company_data` (
  `idpos_company_data` int(11) NOT NULL AUTO_INCREMENT,
  `pos_company_idpos_company` int(11) NOT NULL,
  `data_type` varchar(250) NOT NULL COMMENT 'If this is a phone number, email, etc.',
  `data_value` varchar(250) NOT NULL COMMENT 'The Value. Ex: The number of the phone.',
  `reference_name` varchar(45) DEFAULT NULL COMMENT 'The name of a person or department what is involved with this. Ex: "Sarah Connor", "Finantial Department", etc.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idpos_company_data`),
  KEY `fk_pos_company_data_pos_company1_idx` (`pos_company_idpos_company`),
  CONSTRAINT `fk_pos_company_data_pos_company1` FOREIGN KEY (`pos_company_idpos_company`) REFERENCES `pos_company` (`idpos_company`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_company_data`
--

LOCK TABLES `pos_company_data` WRITE;
/*!40000 ALTER TABLE `pos_company_data` DISABLE KEYS */;
INSERT INTO `pos_company_data` VALUES (1,1,'phone','123123',NULL,'2017-05-17 11:30:14',NULL,NULL,1);
/*!40000 ALTER TABLE `pos_company_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_element`
--

DROP TABLE IF EXISTS `pos_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_element` (
  `idpos_element` int(11) NOT NULL AUTO_INCREMENT,
  `pos_elmt_type_idpos_elmt_type` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idpos_element`),
  KEY `fk_pos_element_pos_elmt_type1_idx` (`pos_elmt_type_idpos_elmt_type`),
  CONSTRAINT `fk_pos_element_pos_elmt_type1` FOREIGN KEY (`pos_elmt_type_idpos_elmt_type`) REFERENCES `pos_elmt_type` (`idpos_elmt_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_element`
--

LOCK TABLES `pos_element` WRITE;
/*!40000 ALTER TABLE `pos_element` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_element_in_tmpt`
--

DROP TABLE IF EXISTS `pos_element_in_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_element_in_tmpt` (
  `idpos_element_in_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `pos_tmpt_idpos_tmpt` int(11) NOT NULL,
  `pos_element_idpos_element` int(11) NOT NULL,
  `position_x` int(11) NOT NULL,
  `position_y` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`idpos_element_in_tmpt`),
  KEY `fk_pos_element_in_tmpt_pos_tmpt1_idx` (`pos_tmpt_idpos_tmpt`),
  KEY `fk_pos_element_in_tmpt_pos_element1_idx` (`pos_element_idpos_element`),
  CONSTRAINT `fk_pos_element_in_tmpt_pos_element1` FOREIGN KEY (`pos_element_idpos_element`) REFERENCES `pos_element` (`idpos_element`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pos_element_in_tmpt_pos_tmpt1` FOREIGN KEY (`pos_tmpt_idpos_tmpt`) REFERENCES `pos_tmpt` (`idpos_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_element_in_tmpt`
--

LOCK TABLES `pos_element_in_tmpt` WRITE;
/*!40000 ALTER TABLE `pos_element_in_tmpt` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_element_in_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_elmt_type`
--

DROP TABLE IF EXISTS `pos_elmt_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_elmt_type` (
  `idpos_elmt_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idpos_elmt_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_elmt_type`
--

LOCK TABLES `pos_elmt_type` WRITE;
/*!40000 ALTER TABLE `pos_elmt_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_elmt_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_preset`
--

DROP TABLE IF EXISTS `pos_preset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_preset` (
  `idpospreset` int(11) NOT NULL AUTO_INCREMENT,
  `description` int(11) NOT NULL,
  `structure` longtext,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idpospreset`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_preset`
--

LOCK TABLES `pos_preset` WRITE;
/*!40000 ALTER TABLE `pos_preset` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_preset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_printer`
--

DROP TABLE IF EXISTS `pos_printer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_printer` (
  `idpos_printer` int(11) NOT NULL AUTO_INCREMENT,
  `pos_idpos` int(11) NOT NULL,
  `printer_idprinter` int(11) NOT NULL,
  `port` varchar(20) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpos_printer`),
  KEY `fk_pos_printer_pos1_idx` (`pos_idpos`),
  KEY `fk_pos_printer_printer1_idx` (`printer_idprinter`),
  CONSTRAINT `fk_pos_printer_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pos_printer_printer1` FOREIGN KEY (`printer_idprinter`) REFERENCES `printer` (`idprinter`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_printer`
--

LOCK TABLES `pos_printer` WRITE;
/*!40000 ALTER TABLE `pos_printer` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_printer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_session`
--

DROP TABLE IF EXISTS `pos_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_session` (
  `idpos_session` int(11) NOT NULL AUTO_INCREMENT,
  `users_idusers` int(11) NOT NULL,
  `date_start` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NULL DEFAULT NULL,
  `cash_fund` decimal(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`idpos_session`),
  KEY `fk_pos_session_user1_idx` (`users_idusers`),
  CONSTRAINT `fk_pos_session_user1` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_session`
--

LOCK TABLES `pos_session` WRITE;
/*!40000 ALTER TABLE `pos_session` DISABLE KEYS */;
INSERT INTO `pos_session` VALUES (1,1,'2017-05-31 17:44:52',NULL,0.00);
/*!40000 ALTER TABLE `pos_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_settings_closing_parameters`
--

DROP TABLE IF EXISTS `pos_settings_closing_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_settings_closing_parameters` (
  `idpossettingsclosingparameters` int(11) NOT NULL AUTO_INCREMENT,
  `idpos` int(11) NOT NULL,
  `data_type` varchar(150) NOT NULL,
  `data_value` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idpossettingsclosingparameters`),
  KEY `idpos` (`idpos`),
  CONSTRAINT `fk_idpos_settings_1` FOREIGN KEY (`idpos`) REFERENCES `pos` (`idpos`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_settings_closing_parameters`
--

LOCK TABLES `pos_settings_closing_parameters` WRITE;
/*!40000 ALTER TABLE `pos_settings_closing_parameters` DISABLE KEYS */;
INSERT INTO `pos_settings_closing_parameters` VALUES (1,1,'text_additional','','2018-04-19 13:48:01',NULL,NULL,1),(2,1,'print_list_of_products_sold_on_the_day','','2018-04-19 13:48:01',NULL,NULL,1),(3,1,'print_quantity_of_products_sold_in_the_day','on','2018-04-19 13:48:01',NULL,NULL,1),(4,1,'print_currency_and_cell_ratio_in_the_box','on','2018-04-19 13:48:01',NULL,NULL,1),(5,1,'include_sales_by_tax_group','on','2018-04-19 13:48:01',NULL,NULL,1),(6,1,'use_the_default_printer_instead_of_the_fiscal_printer','','2018-04-19 13:48:01',NULL,NULL,1),(7,1,'print_discount_rate_of_th_day','','2018-04-19 13:48:01',NULL,NULL,1);
/*!40000 ALTER TABLE `pos_settings_closing_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_settings_credit_notes`
--

DROP TABLE IF EXISTS `pos_settings_credit_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_settings_credit_notes` (
  `idpossettingscreditnotes` int(11) NOT NULL AUTO_INCREMENT,
  `idpos` int(11) NOT NULL,
  `data_type` varchar(150) NOT NULL,
  `data_value` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idpossettingscreditnotes`),
  KEY `idpos` (`idpos`),
  CONSTRAINT `fk_idpos_settings_2` FOREIGN KEY (`idpos`) REFERENCES `pos` (`idpos`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_settings_credit_notes`
--

LOCK TABLES `pos_settings_credit_notes` WRITE;
/*!40000 ALTER TABLE `pos_settings_credit_notes` DISABLE KEYS */;
INSERT INTO `pos_settings_credit_notes` VALUES (1,1,'text_additional','Test3','2017-05-29 18:20:00','2018-04-19 13:51:37',NULL,1),(2,1,'number_of_ways_to_print','111122','2017-05-29 18:20:00','2018-04-19 13:51:37',NULL,1),(3,1,'print_the_receipt_status_on_the_printer','','2017-05-29 18:20:00','2018-04-19 13:51:37',NULL,1),(4,1,'gift_card_text_additional','','2017-05-29 18:20:00','2018-04-19 13:51:37',NULL,1),(5,1,'print_the_status_of_use_the_card','on','2017-05-29 18:20:00','2018-04-19 13:51:37',NULL,1);
/*!40000 ALTER TABLE `pos_settings_credit_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_settings_delivery_list`
--

DROP TABLE IF EXISTS `pos_settings_delivery_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_settings_delivery_list` (
  `idpossettingsdeliverylist` int(11) NOT NULL AUTO_INCREMENT,
  `idpos` int(11) NOT NULL,
  `data_type` varchar(150) NOT NULL,
  `data_value` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idpossettingsdeliverylist`),
  KEY `idpos` (`idpos`),
  CONSTRAINT `fk_idpos_settings_4` FOREIGN KEY (`idpos`) REFERENCES `pos` (`idpos`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_settings_delivery_list`
--

LOCK TABLES `pos_settings_delivery_list` WRITE;
/*!40000 ALTER TABLE `pos_settings_delivery_list` DISABLE KEYS */;
INSERT INTO `pos_settings_delivery_list` VALUES (1,1,'title','Test 3 ','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(2,1,'idTemplate','1','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(3,1,'group_by','1','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(4,1,'size_font','12','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(5,1,'text_additional','Test test 3','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(6,1,'group_identical_products','on','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(7,1,'confirm_print_before_print','','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(8,1,'show_price_in_print','','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(9,1,'print_same_items_on_same_line','','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(10,1,'place_the_last_two_digits_of_the_purchase','on','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(11,1,'when_you_modify_a_purchase_order','on','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(12,1,'print_param_per_carrier_title','Test2  3','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(13,1,'print_param_idTemplate','0','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(14,1,'print_two_rows_for_each_product','on','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(15,1,'omit_price_on_document','on','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(16,1,'print_group_to_which_the_product_belongs','on','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1),(17,1,'print_the_id_to_which_the_product_belongs','on','2017-05-29 18:29:55','2018-06-18 13:28:21',NULL,1);
/*!40000 ALTER TABLE `pos_settings_delivery_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_settings_features`
--

DROP TABLE IF EXISTS `pos_settings_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_settings_features` (
  `idpossettingsfeatures` int(11) NOT NULL AUTO_INCREMENT,
  `idpos` int(11) NOT NULL,
  `data_type` varchar(150) NOT NULL,
  `data_value` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idpossettingsfeatures`),
  KEY `idpos` (`idpos`),
  CONSTRAINT `fk_idpos_settings_5` FOREIGN KEY (`idpos`) REFERENCES `pos` (`idpos`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_settings_features`
--

LOCK TABLES `pos_settings_features` WRITE;
/*!40000 ALTER TABLE `pos_settings_features` DISABLE KEYS */;
INSERT INTO `pos_settings_features` VALUES (1,1,'pos_display_fist_line','Test','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(2,1,'pos_display_second_line','Test 1 ','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(3,1,'temporary_receipt_fist_line','Test 2 ','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(4,1,'temporary_receipt_second_line','Test 4 ','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(5,1,'log_purchase_order_canceled','1','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(6,1,'log_items_canceled','1','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(7,1,'log_cash_drawer_manual_openings','1','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(8,1,'log_record_purchase_orders_deleted','1','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(9,1,'show_similar_items','0','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(10,1,'add_remarks_to_prepayment','0','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(11,1,'not_emit','0','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(12,1,'replace_the_pos_logo','0','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(13,1,'show_all_orders_during','0','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(14,1,'enable_delivery_list','1','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(15,1,'do_not_issue_tax_document','0','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(16,1,'request_confirmation_of_product','0','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(17,1,'do_not_allow_the_user_terminate','1','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(18,1,'do_not_allow_payment','0','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(19,1,'in_case_customer_pays_with_giftcard','1','2017-05-29 22:13:38','2018-06-18 13:28:04',NULL,1),(20,1,'pos_display_fist_line','','2017-05-29 22:15:59',NULL,NULL,1),(21,1,'pos_display_second_line','','2017-05-29 22:15:59',NULL,NULL,1),(22,1,'temporary_receipt_fist_line','','2017-05-29 22:15:59',NULL,NULL,1),(23,1,'temporary_receipt_second_line','','2017-05-29 22:15:59',NULL,NULL,1),(24,1,'log_purchase_order_canceled','0','2017-05-29 22:15:59',NULL,NULL,1),(25,1,'log_items_canceled','0','2017-05-29 22:15:59',NULL,NULL,1),(26,1,'log_cash_drawer_manual_openings','0','2017-05-29 22:15:59',NULL,NULL,1),(27,1,'log_record_purchase_orders_deleted','0','2017-05-29 22:15:59',NULL,NULL,1),(28,1,'show_similar_items','0','2017-05-29 22:15:59',NULL,NULL,1),(29,1,'add_remarks_to_prepayment','0','2017-05-29 22:15:59',NULL,NULL,1),(30,1,'not_emit','0','2017-05-29 22:15:59',NULL,NULL,1),(31,1,'replace_the_pos_logo','0','2017-05-29 22:15:59',NULL,NULL,1),(32,1,'show_all_orders_during','0','2017-05-29 22:15:59',NULL,NULL,1),(33,1,'enable_delivery_list','0','2017-05-29 22:15:59',NULL,NULL,1),(34,1,'do_not_issue_tax_document','0','2017-05-29 22:15:59',NULL,NULL,1),(35,1,'request_confirmation_of_product','0','2017-05-29 22:15:59',NULL,NULL,1),(36,1,'do_not_allow_the_user_terminate','0','2017-05-29 22:15:59',NULL,NULL,1),(37,1,'do_not_allow_payment','0','2017-05-29 22:15:59',NULL,NULL,1),(38,1,'in_case_customer_pays_with_giftcard','0','2017-05-29 22:15:59',NULL,NULL,1),(39,1,'pos_display_fist_line','','2017-05-29 22:16:54',NULL,NULL,1),(40,1,'pos_display_second_line','','2017-05-29 22:16:54',NULL,NULL,1),(41,1,'temporary_receipt_fist_line','','2017-05-29 22:16:54',NULL,NULL,1),(42,1,'temporary_receipt_second_line','','2017-05-29 22:16:54',NULL,NULL,1),(43,1,'log_purchase_order_canceled','0','2017-05-29 22:16:54',NULL,NULL,1),(44,1,'log_items_canceled','0','2017-05-29 22:16:54',NULL,NULL,1),(45,1,'log_cash_drawer_manual_openings','0','2017-05-29 22:16:54',NULL,NULL,1),(46,1,'log_record_purchase_orders_deleted','0','2017-05-29 22:16:54',NULL,NULL,1),(47,1,'show_similar_items','0','2017-05-29 22:16:54',NULL,NULL,1),(48,1,'add_remarks_to_prepayment','0','2017-05-29 22:16:54',NULL,NULL,1),(49,1,'not_emit','0','2017-05-29 22:16:54',NULL,NULL,1),(50,1,'replace_the_pos_logo','0','2017-05-29 22:16:54',NULL,NULL,1),(51,1,'show_all_orders_during','0','2017-05-29 22:16:54',NULL,NULL,1),(52,1,'enable_delivery_list','0','2017-05-29 22:16:54',NULL,NULL,1),(53,1,'do_not_issue_tax_document','0','2017-05-29 22:16:54',NULL,NULL,1),(54,1,'request_confirmation_of_product','0','2017-05-29 22:16:54',NULL,NULL,1),(55,1,'do_not_allow_the_user_terminate','0','2017-05-29 22:16:54',NULL,NULL,1),(56,1,'do_not_allow_payment','0','2017-05-29 22:16:54',NULL,NULL,1),(57,1,'in_case_customer_pays_with_giftcard','0','2017-05-29 22:16:54',NULL,NULL,1),(58,1,'pos_display_fist_line','','2017-05-29 22:17:55',NULL,NULL,1),(59,1,'pos_display_second_line','','2017-05-29 22:17:55',NULL,NULL,1),(60,1,'temporary_receipt_fist_line','','2017-05-29 22:17:55',NULL,NULL,1),(61,1,'temporary_receipt_second_line','','2017-05-29 22:17:55',NULL,NULL,1),(62,1,'log_purchase_order_canceled','0','2017-05-29 22:17:55',NULL,NULL,1),(63,1,'log_items_canceled','0','2017-05-29 22:17:55',NULL,NULL,1),(64,1,'log_cash_drawer_manual_openings','0','2017-05-29 22:17:55',NULL,NULL,1),(65,1,'log_record_purchase_orders_deleted','0','2017-05-29 22:17:55',NULL,NULL,1),(66,1,'show_similar_items','0','2017-05-29 22:17:55',NULL,NULL,1),(67,1,'add_remarks_to_prepayment','0','2017-05-29 22:17:55',NULL,NULL,1),(68,1,'not_emit','0','2017-05-29 22:17:55',NULL,NULL,1),(69,1,'replace_the_pos_logo','0','2017-05-29 22:17:55',NULL,NULL,1),(70,1,'show_all_orders_during','0','2017-05-29 22:17:55',NULL,NULL,1),(71,1,'enable_delivery_list','0','2017-05-29 22:17:55',NULL,NULL,1),(72,1,'do_not_issue_tax_document','0','2017-05-29 22:17:55',NULL,NULL,1),(73,1,'request_confirmation_of_product','0','2017-05-29 22:17:55',NULL,NULL,1),(74,1,'do_not_allow_the_user_terminate','0','2017-05-29 22:17:55',NULL,NULL,1),(75,1,'do_not_allow_payment','0','2017-05-29 22:17:55',NULL,NULL,1),(76,1,'in_case_customer_pays_with_giftcard','0','2017-05-29 22:17:55',NULL,NULL,1),(77,1,'pos_display_fist_line','','2017-05-29 22:20:38',NULL,NULL,1),(78,1,'pos_display_second_line','','2017-05-29 22:20:38',NULL,NULL,1),(79,1,'temporary_receipt_fist_line','','2017-05-29 22:20:38',NULL,NULL,1),(80,1,'temporary_receipt_second_line','','2017-05-29 22:20:38',NULL,NULL,1),(81,1,'log_purchase_order_canceled','0','2017-05-29 22:20:38',NULL,NULL,1),(82,1,'log_items_canceled','0','2017-05-29 22:20:38',NULL,NULL,1),(83,1,'log_cash_drawer_manual_openings','0','2017-05-29 22:20:38',NULL,NULL,1),(84,1,'log_record_purchase_orders_deleted','0','2017-05-29 22:20:38',NULL,NULL,1),(85,1,'show_similar_items','0','2017-05-29 22:20:38',NULL,NULL,1),(86,1,'add_remarks_to_prepayment','0','2017-05-29 22:20:38',NULL,NULL,1),(87,1,'not_emit','0','2017-05-29 22:20:38',NULL,NULL,1),(88,1,'replace_the_pos_logo','0','2017-05-29 22:20:38',NULL,NULL,1),(89,1,'show_all_orders_during','0','2017-05-29 22:20:38',NULL,NULL,1),(90,1,'enable_delivery_list','0','2017-05-29 22:20:38',NULL,NULL,1),(91,1,'do_not_issue_tax_document','0','2017-05-29 22:20:38',NULL,NULL,1),(92,1,'request_confirmation_of_product','0','2017-05-29 22:20:38',NULL,NULL,1),(93,1,'do_not_allow_the_user_terminate','0','2017-05-29 22:20:38',NULL,NULL,1),(94,1,'do_not_allow_payment','0','2017-05-29 22:20:38',NULL,NULL,1),(95,1,'in_case_customer_pays_with_giftcard','0','2017-05-29 22:20:38',NULL,NULL,1),(96,1,'pos_display_fist_line','Test','2017-05-29 22:21:19',NULL,NULL,1),(97,1,'pos_display_second_line','','2017-05-29 22:21:19',NULL,NULL,1),(98,1,'temporary_receipt_fist_line','','2017-05-29 22:21:19',NULL,NULL,1),(99,1,'temporary_receipt_second_line','','2017-05-29 22:21:19',NULL,NULL,1),(100,1,'log_purchase_order_canceled','0','2017-05-29 22:21:19',NULL,NULL,1),(101,1,'log_items_canceled','0','2017-05-29 22:21:19',NULL,NULL,1),(102,1,'log_cash_drawer_manual_openings','0','2017-05-29 22:21:19',NULL,NULL,1),(103,1,'log_record_purchase_orders_deleted','0','2017-05-29 22:21:19',NULL,NULL,1),(104,1,'show_similar_items','0','2017-05-29 22:21:19',NULL,NULL,1),(105,1,'add_remarks_to_prepayment','0','2017-05-29 22:21:19',NULL,NULL,1),(106,1,'not_emit','0','2017-05-29 22:21:19',NULL,NULL,1),(107,1,'replace_the_pos_logo','0','2017-05-29 22:21:19',NULL,NULL,1),(108,1,'show_all_orders_during','0','2017-05-29 22:21:19',NULL,NULL,1),(109,1,'enable_delivery_list','0','2017-05-29 22:21:19',NULL,NULL,1),(110,1,'do_not_issue_tax_document','0','2017-05-29 22:21:19',NULL,NULL,1),(111,1,'request_confirmation_of_product','0','2017-05-29 22:21:19',NULL,NULL,1),(112,1,'do_not_allow_the_user_terminate','0','2017-05-29 22:21:19',NULL,NULL,1),(113,1,'do_not_allow_payment','0','2017-05-29 22:21:19',NULL,NULL,1),(114,1,'in_case_customer_pays_with_giftcard','0','2017-05-29 22:21:19',NULL,NULL,1),(115,1,'pos_display_fist_line','Test','2017-05-29 22:21:41',NULL,NULL,1),(116,1,'pos_display_second_line','Test','2017-05-29 22:21:41',NULL,NULL,1),(117,1,'temporary_receipt_fist_line','Test','2017-05-29 22:21:41',NULL,NULL,1),(118,1,'temporary_receipt_second_line','Test','2017-05-29 22:21:41',NULL,NULL,1),(119,1,'log_purchase_order_canceled','1','2017-05-29 22:21:41',NULL,NULL,1),(120,1,'log_items_canceled','1','2017-05-29 22:21:41',NULL,NULL,1),(121,1,'log_cash_drawer_manual_openings','1','2017-05-29 22:21:41',NULL,NULL,1),(122,1,'log_record_purchase_orders_deleted','1','2017-05-29 22:21:41',NULL,NULL,1),(123,1,'show_similar_items','0','2017-05-29 22:21:41',NULL,NULL,1),(124,1,'add_remarks_to_prepayment','0','2017-05-29 22:21:41',NULL,NULL,1),(125,1,'not_emit','0','2017-05-29 22:21:41',NULL,NULL,1),(126,1,'replace_the_pos_logo','0','2017-05-29 22:21:41',NULL,NULL,1),(127,1,'show_all_orders_during','0','2017-05-29 22:21:41',NULL,NULL,1),(128,1,'enable_delivery_list','0','2017-05-29 22:21:41',NULL,NULL,1),(129,1,'do_not_issue_tax_document','0','2017-05-29 22:21:41',NULL,NULL,1),(130,1,'request_confirmation_of_product','1','2017-05-29 22:21:41',NULL,NULL,1),(131,1,'do_not_allow_the_user_terminate','1','2017-05-29 22:21:41',NULL,NULL,1),(132,1,'do_not_allow_payment','1','2017-05-29 22:21:41',NULL,NULL,1),(133,1,'in_case_customer_pays_with_giftcard','1','2017-05-29 22:21:41',NULL,NULL,1),(134,1,'pos_display_fist_line','Test','2017-05-31 22:00:00',NULL,NULL,1),(135,1,'pos_display_second_line','Test','2017-05-31 22:00:00',NULL,NULL,1),(136,1,'temporary_receipt_fist_line','Test','2017-05-31 22:00:00',NULL,NULL,1),(137,1,'temporary_receipt_second_line','Test','2017-05-31 22:00:00',NULL,NULL,1),(138,1,'log_purchase_order_canceled','1','2017-05-31 22:00:00',NULL,NULL,1),(139,1,'log_items_canceled','1','2017-05-31 22:00:00',NULL,NULL,1),(140,1,'log_cash_drawer_manual_openings','1','2017-05-31 22:00:00',NULL,NULL,1),(141,1,'log_record_purchase_orders_deleted','1','2017-05-31 22:00:00',NULL,NULL,1),(142,1,'show_similar_items','0','2017-05-31 22:00:00',NULL,NULL,1),(143,1,'add_remarks_to_prepayment','0','2017-05-31 22:00:00',NULL,NULL,1),(144,1,'not_emit','0','2017-05-31 22:00:00',NULL,NULL,1),(145,1,'replace_the_pos_logo','0','2017-05-31 22:00:00',NULL,NULL,1),(146,1,'show_all_orders_during','0','2017-05-31 22:00:00',NULL,NULL,1),(147,1,'enable_delivery_list','0','2017-05-31 22:00:00',NULL,NULL,1),(148,1,'do_not_issue_tax_document','0','2017-05-31 22:00:00',NULL,NULL,1),(149,1,'request_confirmation_of_product','1','2017-05-31 22:00:00',NULL,NULL,1),(150,1,'do_not_allow_the_user_terminate','1','2017-05-31 22:00:00',NULL,NULL,1),(151,1,'do_not_allow_payment','1','2017-05-31 22:00:00',NULL,NULL,1),(152,1,'in_case_customer_pays_with_giftcard','1','2017-05-31 22:00:00',NULL,NULL,1),(153,1,'pos_display_fist_line','Test','2018-04-19 13:47:10',NULL,NULL,1),(154,1,'pos_display_second_line','Test','2018-04-19 13:47:10',NULL,NULL,1),(155,1,'temporary_receipt_fist_line','Test','2018-04-19 13:47:10',NULL,NULL,1),(156,1,'temporary_receipt_second_line','Test','2018-04-19 13:47:10',NULL,NULL,1),(157,1,'log_purchase_order_canceled','1','2018-04-19 13:47:10',NULL,NULL,1),(158,1,'log_items_canceled','1','2018-04-19 13:47:10',NULL,NULL,1),(159,1,'log_cash_drawer_manual_openings','1','2018-04-19 13:47:10',NULL,NULL,1),(160,1,'log_record_purchase_orders_deleted','1','2018-04-19 13:47:10',NULL,NULL,1),(161,1,'show_similar_items','0','2018-04-19 13:47:10',NULL,NULL,1),(162,1,'add_remarks_to_prepayment','0','2018-04-19 13:47:10',NULL,NULL,1),(163,1,'not_emit','0','2018-04-19 13:47:10',NULL,NULL,1),(164,1,'replace_the_pos_logo','0','2018-04-19 13:47:10',NULL,NULL,1),(165,1,'show_all_orders_during','0','2018-04-19 13:47:10',NULL,NULL,1),(166,1,'enable_delivery_list','1','2018-04-19 13:47:10',NULL,NULL,1),(167,1,'do_not_issue_tax_document','0','2018-04-19 13:47:10',NULL,NULL,1),(168,1,'request_confirmation_of_product','0','2018-04-19 13:47:10',NULL,NULL,1),(169,1,'do_not_allow_the_user_terminate','1','2018-04-19 13:47:10',NULL,NULL,1),(170,1,'do_not_allow_payment','0','2018-04-19 13:47:10',NULL,NULL,1),(171,1,'in_case_customer_pays_with_giftcard','1','2018-04-19 13:47:10',NULL,NULL,1);
/*!40000 ALTER TABLE `pos_settings_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_settings_receipts`
--

DROP TABLE IF EXISTS `pos_settings_receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_settings_receipts` (
  `idpossettingsreceipts` int(11) NOT NULL AUTO_INCREMENT,
  `idpos` int(11) NOT NULL,
  `data_type` varchar(150) NOT NULL,
  `data_value` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idpossettingsreceipts`),
  KEY `idpos` (`idpos`),
  CONSTRAINT `fk_idpos_settings_3` FOREIGN KEY (`idpos`) REFERENCES `pos` (`idpos`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_settings_receipts`
--

LOCK TABLES `pos_settings_receipts` WRITE;
/*!40000 ALTER TABLE `pos_settings_receipts` DISABLE KEYS */;
INSERT INTO `pos_settings_receipts` VALUES (1,1,'title_coupon','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(2,1,'set_to','1','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(3,1,'message_cupom','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(4,1,'message_byttelapp','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(5,1,'print_two_lines','on','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(6,1,'when_printing_the_tax_coupon','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(7,1,'print_shop_address_on_tax_coupon','on','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(8,1,'print_the_tax_values_on_each_product','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(9,1,'print_detailed_product_description','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(10,1,'hide_comments_entered_in_the_purchase','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(11,1,'always_the_article_number_is_printed','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(12,1,'print_discount_rate_given_on_the_day','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(13,1,'prints_the_values_without_taxes_and_after','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1),(14,1,'includes_tax_tip_amounts_paid','','2017-05-29 18:13:58','2018-04-19 13:52:29',NULL,1);
/*!40000 ALTER TABLE `pos_settings_receipts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_tmpt`
--

DROP TABLE IF EXISTS `pos_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_tmpt` (
  `idpos_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `users_idusers` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `structure_html` longtext,
  `resolution` varchar(100) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idpos_tmpt`),
  KEY `fk_pos_tmpt_users1_idx` (`users_idusers`),
  CONSTRAINT `fk_pos_tmpt_users1` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_tmpt`
--

LOCK TABLES `pos_tmpt` WRITE;
/*!40000 ALTER TABLE `pos_tmpt` DISABLE KEYS */;
INSERT INTO `pos_tmpt` VALUES (4,1,'Test ','<div id=\"image-widget\" style=\"width: 150px; height: 150px; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAkFBMVEWwCAi+DAz/1gD/2gC8AAz/2AD/3ACtAAj/3wC6AA2wAAjFXgb/4QCqAAizCQm3AA3ckATKSQrSeAXdhAjUgAayGgjlpAT0vAT6ywK8PAfLZwbAGgzAUAf0wATXcgn5zgLEMwvaiAbtrQXstQPJQgvOVAq4LwfCKQvimgTmrgS6RAfOcQbTYwqxJgjllgfFWAcOz0ZxAAAH3klEQVR4nO2Za3eiOhSGKySBUBBrW6Raq3irrR39///ucMkNSOLGOuucdVbeLzNqSh73PfFh9N/Uw78NYJDjGibHNUyOa5gc1zA5rmFyXMPkuIbJcQ2T4xomxzVMjmuYHNcwOa5hclzD5LiGyXEN0y1cqBRwGWSdVoO5EB3tf/Y7Sq+uW+8v+wW9kWwgV7lblq88b7WcWm2B0Mc29zwv3+xuIxvGhUZHQnAlP84/zCajP8vYr9eROPu8BWwQF1pvQ+wx+f7ZZDL6jH2+zAuXT9d8/ksutPOIpyje6sFoJumrL0BuABvAhdZLwjbym33DTMdFz2H9KS7XNetXT4NdCedCqMHC4WaS5YSB9S1Bi5DRZMWm+QK+txgKBuei57jejzwjSrnpwl7w0z1pWPIdoujDq1+QzV/jQh+NU+KPtHq1mNUvsddxEfpc1SQ4r9OQ/jRfJnweGGJgLprXIGSWNvuvG2+RbXtDdGSG/Gl46bFeh8loiMXGYygXC2a82rHnp8+E2U8Fo3vm7GPKOD/zwZ4cP5SCLUW7xj1EZCBa56rD+LvLrnvpM8vOPRCspoJy0YxZRyYWZQYjZ7kh4u8dpRFTlpNLQIRRBgXlQp/MPdtUeZcZAkvWRROEHlGygZez+KrBxpIKyEU3zA4/yrPTY9c49LnB92cKPnpiGTqzc6lQQC4eXXi5VmPp0nDhFY8w1IRcmQsqApo1f+xdzGDjDhWQi7nCP7ZihNUwj0yaDdG0eY1zFX9Ep6ykbEwR1oOCcq1wqyjxDZkj/Zy9sWGc7ZrAg9MLtd2I6qhAXOijeTD2OzV0z8aLuOZFT2x/f9reP+XZcO4bTA8F46LcX61srEjYlOVvqg/SM8PE6zaXGDCWiw6UkQrChfYrXThXJCxN6zKKFmwZzrv4P4wfz9UHWKBgXM98924FoszB1YwhGnvfXWjH8lQpt4aoGuTHJUuzZXdSR08Ys4K1GKGMR1uvgKItewJGEFPBuNCah3Ov9ZYTLBuYwzUfcJTWLkTPKrItqgZw8fmzdFYvnVDGfBcW6Q/H3y56j+CfkU0EYYJxzfje/QZHC56Ry5S7kRx7y9CIceH8FQp2DYu3t3LDflUUFax0Hl/WrV6VUv6hNw/uxcXTrJf+tSHEwS1bsf/oBi1eUDxSJPfiEv7RnH3KUs4NIf/VNJuUx6i/vBcXr5ZePNVxbVon3SbSuovG4+CRL4uhAXbNXDsWsl6oG1P4lCwlJnsBVW4SvYtvBw2wK1xiY6w9NCNeAYRaJzJe1aMDL3QkAzryCle65WHfq/Y11xPucCnVnspdIvmYCOZIOxZa8MD2t9rP13kbDPs7Xa9JeAHGq2+YI69w7Xmi+f1yWYnPrIKrmVW7rSYpeODj0124pvx5fqGdgkUn4svKLqSZFYK5SJ/JXbiOnIvMtfai5zYXybThExxiAX4PLukm0psSmEE79jKYI+FceJWAAt9urk/MwzrUH7LELMq5XgxcoiHEB5DB7FyiqmKv3x2bFZ24N9TNZMnXASurnWsqwqLfXphwu1AYykCSiUA9giqrlYvKsM8MXKmncmHvjz56Et65y5n791wpbx/asx9bojqyjGr9NsGJc+H8ARL4Vq6RmOdC3TRRapy0Jgqcm7jERAGs+DYqOat2rwA41fghOapc/tbAFV14HGLv8ddccxE7RDPc12U9eFEnHeO4EH2Lr0imv+YSRbN37yw6oOwwtVkLw57Rm2jwBNSJrFxnERT5znCEDy4tLpMtoj8yhUCHNRvXSMR0a/pqteXoW83H2Bw7fAIrSyEkIW1YsjtWB30NVM2lFrD43cSVfMmYgMz4Nq5PEav+poHqTzBK4FTr3kxbJnIgAp09bOH1KVKtupvQX8FEB4ULe2auiagnMaSA2bhE1y6n1dRw21EGtAwwvDoYuQrJBencNi552PEnpqYWvW4VrtzQHksu0YjKYgLokBYu5XBoPr9HD1+Sy18auYK5fBjkrGbjEuXLIyfjo4JMNiJ/awzp4F0a39SsgFzKIZ+YQyJRub6MpSl4E1wYcklh4Ypmsj1eQFwkM+4TvUquHHC2NVKVo4KsAMSc2snEV7iMy6JAxJcla69xVbUqilaSy3xYSAqFyzIjJ0RyAQqY1lR1rYoOCpe5RCcvKpd5Q4XLM0eFmUuU9UDpfL45ItpcFnspfRRQWLWmargu8hIQm/cLTgqXsfyWXLlcB7ij0Jqq2VBOq9izcM2Vumo41dZc8oaFWJb1ubrDQnCCcT3+TS7a/yR4EZHqm4451bKLjBvfMronW1mlTdN2h4tqfxoJ5ARgu0YGc8n6a0tbyaUxVY/LckYO3odzmcuvkPlHpEBOcrZOG7z7MK7jIC6zguNgLtNtTpfr6xdYYK43DOOayKl8C7qiMChSA+IOXIXkmv2G6+EvcoF/7tNJGQstfS94C0PSKAxtXPIiw19WXEFyU/RH8oRs5YoOEynLABNMJVf182gyn0yDG8wG5SonPkXmZQpX/bPtZPo9P97gT/UABrwTtarD9V28nLIT6Cqsy6U0Wsv8cgvX6jX4nr7MindAA9dx4UZeeA+uU+xh/sDX6K2YXooX4zWLjWuL72qvkzw3rV6j4FR8n4qbysX7o5DxOgSu6CAf91i+Tt5O77d9WyXNfo/Vzlv2+g5PdXJycnJycnJycnJycnJycnJycvrf6x+h0J2r/WsjDQAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-size: contain; z-index: 2; left: 8px; top: 10px;\" class=\"bordered-ui item-1 imgSize-1 ui-draggable ui-draggable-handle ui-resizable ui-resizable-autohide\" data-itam-id=\"1\"> <input id=\"inputFileToLoad\" type=\"file\" onchange=\"encodeImageFileAsURL();\" style=\"display: none\"> <div class=\"ui-resizable-handle ui-resizable-n\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-w\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-sw\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-ne\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-nw\" style=\"z-index: 90; display: none;\"></div></div> <div id=\"customer-widget\" style=\"width: 228px; height: 0px; position: relative; z-index: 4; left: 170px; top: -137px;\" class=\"bordered-ui item-2 imgSize-2 ui-draggable ui-draggable-handle ui-resizable\" data-itam-id=\"2\"> <input type=\"text\" readonly=\"\" placeholder=\"Customer\" class=\"form-control display_customer_name\"><br> <input type=\"button\" class=\"btn btn-primary change_customer\" value=\"Change\"> <div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div><div id=\"employee-widget\" style=\"width: 289px; height: 0px; position: relative; z-index: 6; left: 412px; top: -134px;\" class=\"bordered-ui item-3 imgSize-3 ui-draggable ui-draggable-handle ui-resizable\" data-itam-id=\"3\"> <input type=\"text\" readonly=\"\" placeholder=\"Employee\" class=\"form-control display_user_name\"><br> <input type=\"button\" class=\"btn btn-primary change_user\" value=\"Change\"> <div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div><div id=\"last-sale-widget\" style=\"width: 272px; height: 96px; background-color: rgb(237, 236, 236); border-radius: 5px; position: relative; z-index: 8; left: 726px; top: -144px;\" class=\"bordered-ui item-4 imgSize-4 ui-draggable ui-draggable-handle ui-resizable\" data-itam-id=\"4\"> <h4>Last sale</h4><p style=\"padding-left:15px\">Total: <span class=\"total_order\">0.00</span></p> <div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div><div id=\"table-widget\" class=\"bordered-ui item-5 imgSize-5 ui-draggable ui-draggable-handle ui-resizable\" data-itam-id=\"5\" style=\"background-color: rgb(237, 236, 236); width: 1024px; height: 364px; position: relative; z-index: 10; left: 0px; top: -80px;\"> <div style=\"width: 100%;\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control insert_barcode\" placeholder=\"Search for...\"> <span class=\"input-group-btn\"> <button class=\"btn btn-default new_product\" type=\"button\">Add</button> </span> </div> </div> <table id=\"color\" class=\"table-bordered pos_table\" cellspacing=\"0\" width=\"100%\" style=\"margin: 0px;\"> <thead> <tr> <th class=\"text-center\" style=\"width: 10px\"><input type=\"checkbox\" name=\"all\" onclick=\"markAll(\" color\')\'=\"\"></th> <th style=\"width: 10px\">ID</th> <th>Product No</th> <th>Description</th> <th>Qty</th> <th>Un Price</th> <th>Discount</th> <th>Price</th> <th>Info</th> </tr> </thead> <tbody> <tr class=\"new_product\"> <td></td> <td></td> <td class=\"product_no\"></td> <td class=\"product_description\"></td> <td></td> <td class=\"product_price\"></td> <td></td> <td></td> <td></td> </tr> </tbody> </table><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div><div id=\"tabs-widget\" style=\"width: 1024px; position: relative; z-index: 11; left: 0px; top: -42px;\" class=\"bordered-ui item-6 imgSize-6 ui-draggable ui-draggable-handle ui-resizable ui-resizable-autohide\" data-itam-id=\"6\"> <ul class=\"nav nav-tabs\"> <li class=\"active\"><a data-toggle=\"tab\" href=\"#favorite\" aria-expanded=\"true\">Favorite</a></li> <li><a data-toggle=\"tab\" href=\"#extraFunctions\" aria-expanded=\"true\">Extra Functions</a></li> <li class=\"pull-right\"><a onclick=\"openTabs(6)\">+</a></li> </ul> <div class=\"tab-content\" data-tab-id=\"6\"> <div id=\"favorite\" class=\"tab-pane fade in active\"> <div style=\"height:150px;\"> <div class=\"btn btn-primary button_get_parked_order\" style=\"margin: 5px;\">Get Parked <br>Order</div> <div class=\"btn btn-primary button_extra\" style=\"margin: 5px;\">Extra <br>functions</div> <div class=\"btn btn-primary button_discount\" style=\"margin: 5px;\">Discount</div> <div class=\"btn btn-primary button_cancel_order\" style=\"margin: 5px;\">Cancel Order</div> <div class=\"btn btn-primary button_line_info\" style=\"margin: 5px;\">Line info</div> <div class=\"btn btn-primary button_delete_line\" style=\"margin: 5px;\">Delete line</div> <div class=\"btn btn-primary button_order_info\" style=\"margin: 5px;\">Order info</div> <div class=\"btn btn-primary button_order\" style=\"margin: 5px;\">Open Credit <br>Order</div> <div class=\"btn btn-primary button_pay\" style=\"margin: 5px;\">Pay</div> <div class=\"btn btn-primary button_return\" style=\"margin: 5px;\">Return</div> <div class=\"btn btn-primary button_kvittering\" style=\"margin: 5px;\">Kvittering</div> <div class=\"btn btn-primary button_lag_nytt_gavekort\" style=\"margin: 5px;\">Lag Nytt <br>Gavekort</div> </div> </div> <div id=\"extraFunctions\" class=\"tab-pane\"> <div style=\"height:150px;\"> <div class=\"btn btn-primary button_withdrawal\" style=\"margin: 5px;\">With-drawal / Deposit</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Open Cash Drawer</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Print Receipt Copy</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Cash Statistics</div> <div class=\"btn btn-primary button_credit_last_sale\" style=\"margin: 5px;\">Credit Last Sale</div> <div class=\"btn btn-primary button_print_temp_receipt\" style=\"margin: 5px;\">Print Temp. Receipt</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Settle Invoice By Cash</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Pre Payment</div> <div class=\"btn btn-primary button_split_order\" style=\"margin: 5px;\">Split Order</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Check Giftcard Status</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Check Electr. Gift Card</div> <div class=\"btn btn-primary button_customer_orders\" style=\"margin: 5px;\">Customer Orders</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Customer Statistics</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Customer Updating</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Article Statistics</div> <div class=\"btn btn-primary\" style=\"margin: 5px;\">Article Updating</div> </div> </div> </div> <div class=\"ui-resizable-handle ui-resizable-n\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-w\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-sw\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-ne\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-nw\" style=\"z-index: 90; display: none;\"></div></div>','1024x768','2017-04-11 21:06:28','2017-05-30 01:34:01',NULL,1),(5,1,'PCK','<div class=\"dropHere\">\n    \n    <!-- Customer Widget -->\n    <div id=\"customer-widget\" style=\"width: 164px; height: 96px; position: relative; left: 137px; top: 22px;\" class=\"customer-widget bordered-ui ui-draggable ui-draggable-handle\">\n        <input readonly=\"\" placeholder=\"Customer\" class=\"form-control display_customer_name\" type=\"text\"><br>\n        <input class=\"btn btn-primary change_customer\" value=\"Change\" type=\"button\">\n    </div>\n    \n    <!-- Employee Widget -->\n    <div id=\"employee-widget\" style=\"width: 164px; height: 96px; position: relative; left: 305px; top: -74px;\" class=\"customer-widget bordered-ui ui-draggable ui-draggable-handle\">\n        <input readonly=\"\" placeholder=\"Employee\" class=\"form-control display_user_name\" type=\"text\"><br>\n        <input class=\"btn btn-primary change_user\" value=\"Change\" type=\"button\">\n    </div>\n    \n    <!-- Order Info -->\n    <div id=\"order-info-widget\" style=\"width: 164px; height: 96px; background-color: rgb(237, 236, 236); border-radius: 5px; position: relative; left: 679px; top: -169px;\" class=\"order-info-widget bordered-ui ui-draggable ui-draggable-handle ui-resizable\">\n        <h4>Order</h4><p style=\"padding-left:15px\">Total: <span class=\"total_order\">0.00</span></p>\n    <div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div>\n    \n    <!-- Last Sale Widget -->\n    <div id=\"last-sale-widget\" style=\"width: 164px; height: 96px; background-color: rgb(237, 236, 236); border-radius: 5px; position: relative; left: 847px; top: -265px;\" class=\"last-sale-widget bordered-ui ui-draggable ui-draggable-handle ui-resizable\">\n        <h4>Last sale</h4><p style=\"padding-left:15px\">Total: <span class=\"total_order\">0.00</span></p>\n    <div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div>\n    \n    <!-- Table Widget -->\n    <div id=\"table-widget\" class=\"table-widget bordered-ui ui-draggable ui-draggable-handle ui-resizable\" style=\"background-color: rgb(237, 236, 236); width: 738px; height: 268px; position: relative; left: 6px; top: -242px;\">\n        <div style=\"width: 100%;\">\n            <div class=\"input-group\">\n                <input class=\"form-control insert_barcode\" placeholder=\"Search for...\" type=\"text\">\n                <span class=\"input-group-btn\">\n                    <button class=\"btn btn-default new_product\" type=\"button\">Add</button>\n                </span>\n            </div>\n        </div>\n        <table id=\"color\" class=\"table-bordered pos_table\" style=\"margin: 0px;\" width=\"100%\" cellspacing=\"0\">\n            <thead>\n            <tr>\n                <th class=\"text-center\" style=\"width: 10px\"><input name=\"all\" onclick=\"markAll(\" color\')\'=\"\" type=\"checkbox\"></th>\n                <th style=\"width: 10px\">ID</th>\n                <th>Product No</th>\n                <th>Description</th>\n                <th>Qty</th>\n                <th>Un Price</th>\n                <th>Discount</th>\n                <th>Price</th>\n                <th>Info</th>\n            </tr>\n            </thead>\n        <tbody>\n            <tr class=\"new_product\">\n                <td></td>\n                <td></td>\n                <td class=\"product_no\"></td>\n                <td class=\"product_description\"></td>\n                <td></td>\n                <td class=\"product_price\"></td>\n                <td></td>\n                <td></td>\n                <td></td>\n            </tr>\n        </tbody>\n        </table>\n    <div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div>\n    \n    <!-- Parked Order -->\n    <div id=\"get_parked_order\" class=\"button_get_parked_order ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 750px; top: -546px;\">\n        <p>Parked<br> order</p>\n    </div>\n    \n    <!-- Pay -->\n    <div id=\"pay\" class=\"button_pay ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 134px; width: 60px; position: relative; left: 683px; top: -446px;\">\n        <p>Pay</p>\n    </div>\n    \n    <!-- Extra functions -->\n    <a data-toggle=\"tab\" href=\"#extraFunctions\" aria-expanded=\"true\" style=\"color:black\">\n        <div id=\"extra_functions\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 747px; top: -547px;\">\n            <p>Extra<br> functions</p>\n        </div>\n    </a>\n    \n    <!-- Order info -->\n    <div id=\"order_info\" class=\"button_order_info ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 614px; top: -482px;\">\n        <p>Order<br> info</p>\n    </div>\n    \n    <!-- Credit order -->\n    <div id=\"open_credit_order\" class=\"button_order ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 612px; top: -482px;\">\n        <p>Credit<br> order</p>\n    </div>\n    \n    <!-- Line info -->\n    <div id=\"line_info\" class=\"button_line_info ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 609px; top: -482px;\">\n        <p>Line info</p>\n    </div>\n    \n    <!-- Discount -->\n    <div id=\"discount\" class=\"button_discount ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 408px; top: -420px;\">\n        <p>Discount</p>\n    </div>\n    \n    <!-- Delete line -->\n    <div id=\"delete_line\" class=\"button_delete_line ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 406px; top: -419px;\">\n        <p>Delete<br> line</p>\n    </div>\n    \n    <!-- Cancel Order -->\n    <div id=\"cancel_order\" class=\"button_cancel_order ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 403px; top: -419px;\">\n        <p>Cancel<br> order</p>\n    </div>\n    \n    <!-- Keyboard -->\n    <div onclick=\"testCMD()\" id=\"keyboard\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 332px; top: -547px;\">\n        <span class=\"lnr lnr-keyboard\" style=\"font-size: 38px\"></span>\n    </div>\n    \n    <!-- Return -->\n    <div id=\"return\" class=\"button_return ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: -208px; top: -655px;\">\n        <p>Return</p>\n    </div>\n    \n    <!-- Kvittering -->\n    <div id=\"kvittering\" class=\"button_kvittering ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: -209px; top: -655px;\">\n        <p>Kvittering</p>\n    </div>\n    \n    <!-- Lag Nytt Gavekort -->\n    <div id=\"lag_nytt_gavekort\" class=\"button_lag_nytt_gavekort ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: -209px; top: -655px;\">\n        <p>Lag <br>Nytt <br> Gavekort</p>\n    </div>\n    \n    <!-- Image -->\n    <div id=\"image-widget\" style=\"width: 150px; height: 129px; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAkFBMVEWwCAi+DAz/1gD/2gC8AAz/2AD/3ACtAAj/3wC6AA2wAAjFXgb/4QCqAAizCQm3AA3ckATKSQrSeAXdhAjUgAayGgjlpAT0vAT6ywK8PAfLZwbAGgzAUAf0wATXcgn5zgLEMwvaiAbtrQXstQPJQgvOVAq4LwfCKQvimgTmrgS6RAfOcQbTYwqxJgjllgfFWAcOz0ZxAAAH3klEQVR4nO2Za3eiOhSGKySBUBBrW6Raq3irrR39///ucMkNSOLGOuucdVbeLzNqSh73PfFh9N/Uw78NYJDjGibHNUyOa5gc1zA5rmFyXMPkuIbJcQ2T4xomxzVMjmuYHNcwOa5hclzD5LiGyXEN0y1cqBRwGWSdVoO5EB3tf/Y7Sq+uW+8v+wW9kWwgV7lblq88b7WcWm2B0Mc29zwv3+xuIxvGhUZHQnAlP84/zCajP8vYr9eROPu8BWwQF1pvQ+wx+f7ZZDL6jH2+zAuXT9d8/ksutPOIpyje6sFoJumrL0BuABvAhdZLwjbym33DTMdFz2H9KS7XNetXT4NdCedCqMHC4WaS5YSB9S1Bi5DRZMWm+QK+txgKBuei57jejzwjSrnpwl7w0z1pWPIdoujDq1+QzV/jQh+NU+KPtHq1mNUvsddxEfpc1SQ4r9OQ/jRfJnweGGJgLprXIGSWNvuvG2+RbXtDdGSG/Gl46bFeh8loiMXGYygXC2a82rHnp8+E2U8Fo3vm7GPKOD/zwZ4cP5SCLUW7xj1EZCBa56rD+LvLrnvpM8vOPRCspoJy0YxZRyYWZQYjZ7kh4u8dpRFTlpNLQIRRBgXlQp/MPdtUeZcZAkvWRROEHlGygZez+KrBxpIKyEU3zA4/yrPTY9c49LnB92cKPnpiGTqzc6lQQC4eXXi5VmPp0nDhFY8w1IRcmQsqApo1f+xdzGDjDhWQi7nCP7ZihNUwj0yaDdG0eY1zFX9Ep6ykbEwR1oOCcq1wqyjxDZkj/Zy9sWGc7ZrAg9MLtd2I6qhAXOijeTD2OzV0z8aLuOZFT2x/f9reP+XZcO4bTA8F46LcX61srEjYlOVvqg/SM8PE6zaXGDCWiw6UkQrChfYrXThXJCxN6zKKFmwZzrv4P4wfz9UHWKBgXM98924FoszB1YwhGnvfXWjH8lQpt4aoGuTHJUuzZXdSR08Ys4K1GKGMR1uvgKItewJGEFPBuNCah3Ov9ZYTLBuYwzUfcJTWLkTPKrItqgZw8fmzdFYvnVDGfBcW6Q/H3y56j+CfkU0EYYJxzfje/QZHC56Ry5S7kRx7y9CIceH8FQp2DYu3t3LDflUUFax0Hl/WrV6VUv6hNw/uxcXTrJf+tSHEwS1bsf/oBi1eUDxSJPfiEv7RnH3KUs4NIf/VNJuUx6i/vBcXr5ZePNVxbVon3SbSuovG4+CRL4uhAXbNXDsWsl6oG1P4lCwlJnsBVW4SvYtvBw2wK1xiY6w9NCNeAYRaJzJe1aMDL3QkAzryCle65WHfq/Y11xPucCnVnspdIvmYCOZIOxZa8MD2t9rP13kbDPs7Xa9JeAHGq2+YI69w7Xmi+f1yWYnPrIKrmVW7rSYpeODj0124pvx5fqGdgkUn4svKLqSZFYK5SJ/JXbiOnIvMtfai5zYXybThExxiAX4PLukm0psSmEE79jKYI+FceJWAAt9urk/MwzrUH7LELMq5XgxcoiHEB5DB7FyiqmKv3x2bFZ24N9TNZMnXASurnWsqwqLfXphwu1AYykCSiUA9giqrlYvKsM8MXKmncmHvjz56Et65y5n791wpbx/asx9bojqyjGr9NsGJc+H8ARL4Vq6RmOdC3TRRapy0Jgqcm7jERAGs+DYqOat2rwA41fghOapc/tbAFV14HGLv8ddccxE7RDPc12U9eFEnHeO4EH2Lr0imv+YSRbN37yw6oOwwtVkLw57Rm2jwBNSJrFxnERT5znCEDy4tLpMtoj8yhUCHNRvXSMR0a/pqteXoW83H2Bw7fAIrSyEkIW1YsjtWB30NVM2lFrD43cSVfMmYgMz4Nq5PEav+poHqTzBK4FTr3kxbJnIgAp09bOH1KVKtupvQX8FEB4ULe2auiagnMaSA2bhE1y6n1dRw21EGtAwwvDoYuQrJBencNi552PEnpqYWvW4VrtzQHksu0YjKYgLokBYu5XBoPr9HD1+Sy18auYK5fBjkrGbjEuXLIyfjo4JMNiJ/awzp4F0a39SsgFzKIZ+YQyJRub6MpSl4E1wYcklh4Ypmsj1eQFwkM+4TvUquHHC2NVKVo4KsAMSc2snEV7iMy6JAxJcla69xVbUqilaSy3xYSAqFyzIjJ0RyAQqY1lR1rYoOCpe5RCcvKpd5Q4XLM0eFmUuU9UDpfL45ItpcFnspfRRQWLWmargu8hIQm/cLTgqXsfyWXLlcB7ij0Jqq2VBOq9izcM2Vumo41dZc8oaFWJb1ubrDQnCCcT3+TS7a/yR4EZHqm4451bKLjBvfMronW1mlTdN2h4tqfxoJ5ARgu0YGc8n6a0tbyaUxVY/LckYO3odzmcuvkPlHpEBOcrZOG7z7MK7jIC6zguNgLtNtTpfr6xdYYK43DOOayKl8C7qiMChSA+IOXIXkmv2G6+EvcoF/7tNJGQstfS94C0PSKAxtXPIiw19WXEFyU/RH8oRs5YoOEynLABNMJVf182gyn0yDG8wG5SonPkXmZQpX/bPtZPo9P97gT/UABrwTtarD9V28nLIT6Cqsy6U0Wsv8cgvX6jX4nr7MindAA9dx4UZeeA+uU+xh/sDX6K2YXooX4zWLjWuL72qvkzw3rV6j4FR8n4qbysX7o5DxOgSu6CAf91i+Tt5O77d9WyXNfo/Vzlv2+g5PdXJycnJycnJycnJycnJycnJycvrf6x+h0J2r/WsjDQAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-size: contain; z-index: 2; left: 5px; top: -783px;\" class=\"bordered-ui item-1 imgSize-1 ui-draggable ui-draggable-handle ui-resizable ui-resizable-autohide\" data-itam-id=\"1\"> <input id=\"inputFileToLoad\" onchange=\"encodeImageFileAsURL();\" style=\"display: none\" type=\"file\"> <div class=\"ui-resizable-handle ui-resizable-n\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90; display: block;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90; display: block;\"></div><div class=\"ui-resizable-handle ui-resizable-w\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90; display: block;\"></div><div class=\"ui-resizable-handle ui-resizable-sw\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-ne\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-nw\" style=\"z-index: 90; display: none;\"></div><div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div>\n    \n    \n    \n    <!-- Calc -->\n    <div id=\"cac0-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 122px; position: relative; left: 752px; top: -314px;\">\n        <p>0</p>\n    </div>\n    \n    <div id=\"cac1-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 626px; top: -378px;\">\n        <p>1</p>\n    </div>\n    \n    <div id=\"cac2-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 625px; top: -379px;\">\n        <p>2</p>\n    </div>\n    \n    <div id=\"cac3-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 623px; top: -378px;\">\n        <p>3</p>\n    </div>\n    \n    <div id=\"cac4-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 421px; top: -441px;\">\n        <p>4</p>\n    </div>\n    \n    <div id=\"cac5-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 419px; top: -441px;\">\n        <p>5</p>\n    </div>\n    \n    <div id=\"cac6-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 417px; top: -441px;\">\n        <p>6</p>\n    </div>\n    \n    <div id=\"cac7-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 215px; top: -504px;\">\n        <p>7</p>\n    </div>\n    \n    <div id=\"cac8-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 213px; top: -504px;\">\n        <p>8</p>\n    </div>\n    \n    <div id=\"cac9-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 211px; top: -505px;\">\n        <p>9</p>\n    </div>\n    \n    <div id=\"cac10-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 76px; top: -567px;\">\n        <p>%</p>\n    </div>\n    \n    <div id=\"cac11-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 74px; top: -567px;\">\n        <p>*</p>\n    </div>\n    \n    <div id=\"cac12-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 72px; top: -567px;\">\n        <p>-</p>\n    </div>\n    \n    <div id=\"cac13-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 3px; top: -504px;\">\n        <p>+</p>\n    </div>\n    \n    <div id=\"cac14-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 877px; top: -437px;\">\n        <p>,</p>\n    </div>\n    \n    <div id=\"cac15-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 183px; width: 65px; position: relative; left: 882px; top: -501px;\">\n        <p>Ok</p>\n    </div>\n    \n    <div id=\"cac16-widget\" class=\"ui-widget-content btn btn-primary btn-pos ui-draggable ui-draggable-handle\" style=\"height: 61px; width: 65px; position: relative; left: 615px; top: -688px;\">\n        <p>&lt;=</p>\n    </div>\n    \n    <div id=\"tabs-widget\" style=\"position: relative; left: 6px; top: -742px; width: 739px; height: 333px;\" class=\"ui-draggable ui-draggable-handle ui-resizable\">\n        <ul class=\"nav nav-tabs\">\n            <li class=\"\">\n                <a data-toggle=\"tab\" href=\"#products\" aria-expanded=\"false\">Products</a>\n            </li> \n            <li id=\"tab-extraFunctions\" class=\"active\">\n                <a data-toggle=\"tab\" href=\"#extraFunctions\" aria-expanded=\"true\">Extra Functions</a>\n            </li>\n        </ul> \n        <div class=\"tab-content\" data-tab-id=\"6\"> \n            <div id=\"products\" class=\"tab-pane\"> \n                <div style=\"height:150px;\"> \n                </div> \n            </div> \n            <div id=\"extraFunctions\" class=\"tab-pane fade active in\"> \n                <div style=\"height:150px;\"> \n                    <div class=\"btn btn-primary button_withdrawal\" style=\"margin: 5px;\">With-drawal / Deposit</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Open Cash Drawer</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Print Receipt Copy</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Cash Statistics</div> \n                    <div class=\"btn btn-primary button_credit_last_sale\" style=\"margin: 5px;\">Credit Last Sale</div> \n                    <div class=\"btn btn-primary button_print_temp_receipt\" style=\"margin: 5px;\">Print Temp. Receipt</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Settle Invoice By Cash</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Pre Payment</div> \n                    <div class=\"btn btn-primary button_split_order\" style=\"margin: 5px;\">Split Order</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Check Giftcard Status</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Check Electr. Gift Card</div> \n                    <div class=\"btn btn-primary button_customer_orders\" style=\"margin: 5px;\">Customer Orders</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Customer Statistics</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Customer Updating</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Article Statistics</div> \n                    <div class=\"btn btn-primary\" style=\"margin: 5px;\">Article Updating</div> \n                </div> \n            </div> \n        </div>             \n    <div class=\"ui-resizable-handle ui-resizable-e\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-s\" style=\"z-index: 90;\"></div><div class=\"ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se\" style=\"z-index: 90;\"></div></div>\n</div>','1024x768','2017-06-02 00:03:44',NULL,NULL,1),(6,1,'EinarTest','<li data-row=\"1\" data-col=\"3\" data-sizex=\"4\" data-sizey=\"2\" class=\"gs-w\" style=\"display: list-item;\"><div class=\"btn btn-primary\" id=\"buttontela\" style=\"width: 100%; height: 100%\" ondblclick=\"openModal(9.14462741740431)\"><p id=\"9.14462741740431\">betale</p></div><span class=\"gs-resize-handle gs-resize-handle-both\"></span></li><li data-row=\"1\" data-col=\"7\" data-sizex=\"3\" data-sizey=\"2\" class=\"gs-w\" style=\"display: list-item;\"><div class=\"btn btn-primary\" id=\"buttontela\" style=\"width: 100%; height: 100%\" ondblclick=\"openModal(9.898279983683434)\"><p id=\"9.898279983683434\">Ã¥pne kasse</p></div><span class=\"gs-resize-handle gs-resize-handle-both\"></span></li><li class=\"preview-holder resize-preview-holder\" data-row=\"1\" data-col=\"7\" data-sizex=\"2\" data-sizey=\"2\" style=\"\"></li><li data-row=\"1\" data-col=\"10\" data-sizex=\"2\" data-sizey=\"2\" data-min-sizex=\"2\" data-min-sizey=\"2\" class=\"gs-w\" style=\"display: list-item;\"><div id=\"order-info-widget\" class=\"text-left\"><h4>Order</h4><p style=\"padding-left:15px\">Total: <span class=\"total_order\">0.00</span></p></div><span class=\"gs-resize-handle gs-resize-handle-both\"></span></li><li data-row=\"1\" data-col=\"12\" data-sizex=\"2\" data-sizey=\"2\" data-min-sizex=\"2\" data-min-sizey=\"2\" class=\"gs-w\" style=\"display: list-item;\"><div id=\"last-sale-widget\" class=\"text-left\"><h4>Last Sale</h4><p style=\"padding-left:15px\">Total: <span class=\"last_sale\">0.00</span></p></div><span class=\"gs-resize-handle gs-resize-handle-both\"></span></li><li data-row=\"1\" data-col=\"14\" data-sizex=\"2\" data-sizey=\"2\" data-min-sizex=\"2\" data-min-sizey=\"2\" class=\"gs-w\" style=\"display: list-item;\"><div id=\"customer-widget\" class=\"customer-widget\"><input readonly=\"\" placeholder=\"Name customer\" class=\"form-control display_customer_name\" type=\"text\"><br><input class=\"btn btn-primary change_customer pull-left\" value=\"Change\" type=\"button\"></div><span class=\"gs-resize-handle gs-resize-handle-both\"></span></li><li data-row=\"1\" data-col=\"16\" data-sizex=\"2\" data-sizey=\"2\" data-min-sizex=\"2\" data-min-sizey=\"2\" class=\"gs-w\" style=\"display: list-item;\"><div id=\"employee-widget\" class=\"employee-widget\"><input readonly=\"\" placeholder=\"Name Employee\" class=\"form-control display_customer_name\" type=\"text\"><br><input class=\"btn btn-primary change_user pull-left\" value=\"Change\" type=\"button\"></div><span class=\"gs-resize-handle gs-resize-handle-both\"></span></li><li data-row=\"1\" data-col=\"20\" data-sizex=\"4\" data-sizey=\"5\" data-min-sizex=\"4\" data-min-sizey=\"5\" data-max-sizex=\"4\" data-max-sizey=\"5\" class=\"gs-w\" style=\"display: list-item;\"><div class=\"btn btn-group\" style=\"padding: 8px\"><div class=\"btn btn-calc btn-primary\"><span class=\"lnr lnr-arrow-left\"></span></div><div class=\"btn btn-calc btn-primary\">=</div><div class=\"btn btn-calc btn-primary\">/</div><div class=\"btn btn-calc btn-primary\">*</div><div class=\"btn btn-calc btn-primary\">7</div><div class=\"btn btn-calc btn-primary\">8</div><div class=\"btn btn-calc btn-primary\">9</div><div class=\"btn btn-calc btn-primary\">-</div><div class=\"btn btn-calc btn-primary\">4</div><div class=\"btn btn-calc btn-primary\">5</div><div class=\"btn btn-calc btn-primary\">6</div><div class=\"btn btn-calc btn-primary\">+</div><div class=\"btn btn-calc btn-primary\">1</div><div class=\"btn btn-calc btn-primary\">2</div><div class=\"btn btn-calc btn-primary\">3</div><div class=\"btn btn-calc btn-primary\">.</div><div class=\"btn btn-calc btn-primary\">0</div><div class=\"btn btn-calc btn-primary\" style=\"width: 75%\">Enter</div></div><span class=\"gs-resize-handle gs-resize-handle-both\"></span></li><li data-row=\"3\" data-col=\"3\" data-sizex=\"14\" data-sizey=\"3\" data-min-sizex=\"9\" data-min-sizey=\"3\" class=\"gs-w\" style=\"display: list-item;\"><div style=\"width: 100%;\"><div class=\"input-group\"><input class=\"form-control insert_barcode\" placeholder=\"Search for...\" type=\"text\"><span class=\"input-group-btn\"><button class=\"btn btn-default new_product\" type=\"button\">Add</button></span></div></div><table class=\"table-bordered pos_table\" style=\"width: 100%; margin: 0px;\"><thead><tr><td><input type=\"checkbox\" name=\"checkAll\"></td><td>ID</td><td>Product No</td><td>Description</td><td>Qty</td><td>Un Price</td><td>Discount</td><td>Price</td><td>Info</td></tr></thead><tbody><tr></tr></tbody></table><span class=\"gs-resize-handle gs-resize-handle-both\"></span></li>','1920x900','2018-05-10 15:38:29','2018-07-19 19:30:19',NULL,1),(7,1,'test1','','1920x900','2018-07-17 19:33:07',NULL,NULL,1);
/*!40000 ALTER TABLE `pos_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_tmpt_btn_functions`
--

DROP TABLE IF EXISTS `pos_tmpt_btn_functions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_tmpt_btn_functions` (
  `idposmptbtnfunctions` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `action` varchar(150) NOT NULL,
  `language` char(2) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idposmptbtnfunctions`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_tmpt_btn_functions`
--

LOCK TABLES `pos_tmpt_btn_functions` WRITE;
/*!40000 ALTER TABLE `pos_tmpt_btn_functions` DISABLE KEYS */;
INSERT INTO `pos_tmpt_btn_functions` VALUES (1,'Get parked order','get_parked_order','us','2017-05-19 13:45:34',NULL,NULL,1),(2,'Pay','pay','us','2017-05-19 13:45:34',NULL,NULL,1),(3,'Order info','order_info','us','2017-05-19 13:45:34',NULL,NULL,1),(4,'Open credit order','open_credit_order','us','2017-05-19 13:45:34',NULL,NULL,1),(5,'Line info','line_info','us','2017-05-19 13:45:34',NULL,NULL,1),(6,'Discount','discount','us','2017-05-19 13:45:34',NULL,NULL,1),(7,'Delete Line','delete_line','us','2017-05-19 13:45:34',NULL,NULL,1),(8,'Cancel order','cancel_order','us','2017-05-19 13:45:34',NULL,NULL,1),(9,'With drawal/deposit','with_drawal_deposit','us','2017-05-19 13:45:34',NULL,NULL,1),(10,'Open Cash drawer','open_cash_drawer','us','2017-05-19 13:45:34',NULL,NULL,1),(11,'Print receipt copy','print_receipt_copy','us','2017-05-19 13:45:34',NULL,NULL,1),(12,'Cash Statistics','cash_statistics','us','2017-05-19 13:45:34',NULL,NULL,1),(13,'Credit last sale','credit_last_sale','us','2017-05-19 13:45:34',NULL,NULL,1),(14,'Print temp. receipt','print_temp_receipt','us','2017-05-19 13:45:34',NULL,NULL,1),(15,'Settle invoice by Cash','settle_invoice_by_cash','us','2017-05-19 13:45:34',NULL,NULL,1),(16,'Pre payment','pre_payment','us','2017-05-19 13:45:34',NULL,NULL,1),(17,'Split order','split_order','us','2017-05-19 13:45:34',NULL,NULL,1),(18,'Check giftcard status','check_giftcard_status','us','2017-05-19 13:45:34',NULL,NULL,1),(19,'Check electr. gift card','check_electr_gift_card','us','2017-05-19 13:45:34',NULL,NULL,1),(20,'Customer orders','customer_orders','us','2017-05-19 13:45:34',NULL,NULL,1),(21,'Customer statistics','customer_statistics','us','2017-05-19 13:45:34',NULL,NULL,1),(22,'Customer updating','customer_updating','us','2017-05-19 13:45:34',NULL,NULL,1),(23,'Article statistics','article_statistics','us','2017-05-19 13:45:34',NULL,NULL,1),(24,'Article updating','artible_updating','us','2017-05-19 13:45:34',NULL,NULL,1),(25,'Price Search','price_search','us','2017-05-19 13:45:34',NULL,NULL,1),(26,'Search products','search_products','us','2017-05-19 13:45:34',NULL,NULL,1),(27,'Return','return','us','2017-05-19 13:45:34',NULL,NULL,1),(28,'Receipt','receipt','us','2017-05-19 13:45:34',NULL,NULL,1),(29,'Central Gift Vouchers','central_gift_vouchers','us','2017-05-19 13:45:34',NULL,NULL,1);
/*!40000 ALTER TABLE `pos_tmpt_btn_functions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_tmpt_commands`
--

DROP TABLE IF EXISTS `pos_tmpt_commands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_tmpt_commands` (
  `idpostmptcommands` int(11) NOT NULL AUTO_INCREMENT,
  `command` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `language` char(2) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idpostmptcommands`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_tmpt_commands`
--

LOCK TABLES `pos_tmpt_commands` WRITE;
/*!40000 ALTER TABLE `pos_tmpt_commands` DISABLE KEYS */;
INSERT INTO `pos_tmpt_commands` VALUES (1,'%A','Avslutt kontant med kvittering','no','2017-05-19 13:45:34',NULL,NULL,1),(2,'%a','Avslutt kontant uten kvittering','no','2017-05-19 13:45:34',NULL,NULL,1),(3,'%B','Avslutt bankkort med kvittering','no','2017-05-19 13:45:34',NULL,NULL,1),(4,'%b','Avslutt bankkort uten kvittering','no','2017-05-19 13:45:34',NULL,NULL,1),(5,'%C[x]','Legg til kontant betaling (x = belop)','no','2017-05-19 13:45:34',NULL,NULL,1),(6,'%c[x]','Legg til bankkort betaling (x = belop)','no','2017-05-19 13:45:34',NULL,NULL,1),(7,'%d','innbetaling av faktura','no','2017-05-19 13:45:34',NULL,NULL,1),(8,'%e<varenr/strekkode>.<serienr>','Legg til vare med serienr','no','2017-05-19 13:45:34',NULL,NULL,1),(9,'%F<x>','Sett ordrelinjeregel pa aktiv linje (x=ordrelinjeregel id)','no','2017-05-19 13:45:34',NULL,NULL,1),(10,'%G','Merk linje som gave','no','2017-05-19 13:45:34',NULL,NULL,1),(11,'%g','Ta vekk merke som gave','no','2017-05-19 13:45:34',NULL,NULL,1),(12,'%H[x[.y]]','Bytt til favorittgruppe x (0=standard) og evt fane y','no','2017-05-19 13:45:34',NULL,NULL,1),(13,'%i','Rediger ordreinfo / hent fra tidligere','no','2017-05-19 13:45:34',NULL,NULL,1),(14,'%j','Midlertidig kvittering','no','2017-05-19 13:45:34',NULL,NULL,1),(15,'%K[Kundenummer]','Velg kunde fra kundenr. Spor om nr dersom ikke kundenr er fylt ut','no','2017-05-19 13:45:34',NULL,NULL,1),(16,'%L[A4|E<x>]','Kopi av siste kvittering (%L . %LA4 eller %LEx der x er etikett ID)','no','2017-05-19 13:45:34',NULL,NULL,1),(17,'%M','Vis kart fra kundeadressen','no','2017-05-19 13:45:34',NULL,NULL,1),(18,'%N<x>','Sett ordremal og lagre ordren (x=ordremal id)','no','2017-05-19 13:45:34',NULL,NULL,1),(19,'%O','Apne kasseskuff','no','2017-05-19 13:45:34',NULL,NULL,1),(20,'%P[x]','Hent laveste pris pa vare siste x dager (365 dager er standard dersom x er utelatt)','no','2017-05-19 13:45:34',NULL,NULL,1),(21,'%p[x]','Hent laveste pris pa vare siste x dager for aktuell kundle (365 dager er standard dersom x er utelatt)','no','2017-05-19 13:45:34',NULL,NULL,1),(22,'%Q','Bytte firma','no','2017-05-19 13:45:34',NULL,NULL,1),(23,'%S','Angi totalpris pa ordren','no','2017-05-19 13:45:34',NULL,NULL,1),(24,'%T<x>','Legg til ekstratekst og vis linjeinfo. (x=tekst)','no','2017-05-19 13:45:34',NULL,NULL,1),(25,'%t<x>','Legg til ekstratekst pa linje. (x=tekst)','no','2017-05-19 13:45:34',NULL,NULL,1),(26,'%u','Sla opp saldo pa aktiv ordrelinje','no','2017-05-19 13:45:34',NULL,NULL,1),(27,'%v','Sla opp saldo i annen butikk','no','2017-05-19 13:45:34',NULL,NULL,1),(28,'%w','Uttak i kasse','no','2017-05-19 13:45:34',NULL,NULL,1),(29,'%w+','Innskudd i kasse','no','2017-05-19 13:45:34',NULL,NULL,1),(30,'%X','Debiter pa rom (Hotel integrasjon)','no','2017-05-19 13:45:34',NULL,NULL,1),(31,'%y[.x[.y[.z]]]','Velg varer fra varegrupper (x=varegr. 1 id. y=v.gr. 2id. z=v.gr. 3id). Man kan benytte %yy for a benytte varegruppenr i stedet for id','no','2017-05-19 13:45:34',NULL,NULL,1),(32,'%Z','Ressursallokeringsoversikt','no','2017-05-19 13:45:34',NULL,NULL,1),(33,'%z','Ny ressursallokering','no','2017-05-19 13:45:34',NULL,NULL,1),(34,'%%A[x]','Skrive ut ordreetiketter (x=antall. x=? vil sporre)','no','2017-05-19 13:45:34',NULL,NULL,1),(35,'%%B<x>','Apne et vindu fra admin (x er navn pa vinduet - Sla opp i manualen for oversikt over navn)','no','2017-05-19 13:45:34',NULL,NULL,1),(36,'%%C<ordrefelt>','Spor om vardien du onsker a sette i feltet.Feks. %%CYourRef','no','2017-05-19 13:45:34',NULL,NULL,1),(37,'%%C<ordrefelt>=<verdi>','Spor om verdien du onsker a sette i feltet.Feks. %%CYourRef','no','2017-05-19 13:45:34',NULL,NULL,1),(38,'%%C<ordrefelt>==<annetordrefelt>','Setter ordrefeltet lik et annet ordrefelt.Feks: %%CYourRef==Customer.Contact','no','2017-05-19 13:45:34',NULL,NULL,1),(39,'%%C<ordrefelt>=#<sokefelt>.[ledetekst][,verdi]','velger ordrefeltet fra en liste der sokefelt stemmer med verdien Dersom ikke verdi er angitt sporres det etter ved hjelp av ledeteksten. Geks: %%CCustomer=#Telephone,Kundens telefonnr %%CCustomer=#CustomerGroup.ID,,3 Dersom du angir OrderLines.<felt> i <ordrefelt> vil bety felt pa aktiv ordrelinje. Feks: %%cOrderLines.Discount=20 Dersom du angir OrderLinesAll,<felt> vil samtlige ordrelinjer endres.','no','2017-05-19 13:48:43',NULL,NULL,1),(40,'%%D[ordreId]','Hent ordre','no','2017-05-19 13:48:43',NULL,NULL,1),(41,'%%G[link]','Apner link fra vare eller angitt link','no','2017-05-19 13:48:43',NULL,NULL,1),(42,'%%J[D]','Overfore hele ordren til bestilling. Dersom D er angitt overfores leveringsdato.','no','2017-05-19 13:48:43',NULL,NULL,1),(43,'%%J1[D]','Overfore aktiv ordrelinje til bestilling/eksisterende bestilling','no','2017-05-19 13:48:43',NULL,NULL,1),(44,'%%J@[D]','Overfore hele ordren til eksisterende bestillinger','no','2017-05-19 13:48:43',NULL,NULL,1),(45,'%%K[printer]','Skriver ut byttelapp pa aktiv linje','no','2017-05-19 13:48:43',NULL,NULL,1),(46,'%%M[Melding]','Send SMS','no','2017-05-19 13:48:43',NULL,NULL,1),(47,'%%O','Splitt ordre','no','2017-05-19 13:48:43',NULL,NULL,1),(48,'%%F[x]','Forhandsbetaling (x=prosentforslag)','no','2017-05-19 13:48:43',NULL,NULL,1),(49,'%%L[x]','Velg kunde fra kundegrupper (x=kundegruppe Id)','no','2017-05-19 13:48:43',NULL,NULL,1),(50,'%%P','Oppslag pa lev. varenr (orgsa sentralt)','no','2017-05-19 13:48:43',NULL,NULL,1),(51,'%%Q','Splitt ordre (Feks del regningen pa det hver og en har spist)','no','2017-05-19 13:48:43',NULL,NULL,1),(52,'%%R[$][Merket]','Parker/hent ordreDersom $ er angitt vil eksisterende selger ta over bordet/ordren (Feks %%R$)','no','2017-05-19 13:48:43',NULL,NULL,1),(53,'%%S','Veksle mellom ink/eks mva i kassebildet','no','2017-05-19 13:48:43',NULL,NULL,1),(54,'%%Tx<Beskjed>','Skriver beskjed ut pa skriver. <br>Der x kan vare:<br> 2 - kasseskriver<br> 3 - plukklisteskriver<br> 4 - skriver<br> 5 - ekstraskriver 1<br> 6 - ekstraskriver 2<br> 7 - ekstraskriver 3<br> 8 - etikettskriver<br> Feks: %%T3BEDES<br> NB! Selger merking on ordrenr hentes fra akriv ordre.','no','2017-05-19 13:48:43',NULL,NULL,1),(55,'%%U','Hent vareantall fra handterminal','no','2017-05-19 13:48:43',NULL,NULL,1),(56,'%%V<uId>','Skrolle opp(%%vu) eller ned (%%vd) en linje','no','2017-05-19 13:48:43',NULL,NULL,1),(57,'%%W','Skrive ut kvittering eller faktura ut i fra serienr.','no','2017-05-19 13:48:43',NULL,NULL,1),(58,'%%X','Sende e-post til kundle','no','2017-05-19 13:48:43',NULL,NULL,1),(59,'%%Y','Viser dagens omsetning grafisk','no','2017-05-19 13:48:43',NULL,NULL,1),(60,'%%Z[x]','Apner betalingsbildet og klikker evt pa en betalingsknapp x=1 klikker pa gavekort, x=2 klikker pa bankkort. Feks %%Z2','no','2017-05-19 13:48:43',NULL,NULL,1),(61,'%%N<1|2>','Pck Servitor kommandoer<br> 1 - Sett aktiv medarbeiders favorittvarer<br> 2 - Send beskjed til servitorer','no','2017-05-19 13:48:43',NULL,NULL,1),(62,'%%%A[<pakkenavn[:<pakkepris>[;<antall varer>]]]','Lagpakkepris. Feks: %%$ASkipakke;1200','no','2017-05-19 13:48:43',NULL,NULL,1),(63,'%%%b[-|+]','Toggler fortegn pa aktuell linje. Dersom man angir - eller + bak sa settes antall til negativt eller positivt og gjor ingenting dersom allerede slik.','no','2017-05-19 13:48:43',NULL,NULL,1),(64,'%%%c','Spor om hvilken selger som skal logges pa','no','2017-05-19 13:48:43',NULL,NULL,1),(65,'%%%d','Spor om hvilken selger som skal logges av','no','2017-05-19 13:48:43',NULL,NULL,1),(66,'%%%e<filnavn>','Apner en wordmal og fyller inn felter fra aktiv ordre','no','2017-05-19 13:48:43',NULL,NULL,1),(67,'%%%f','Lager avtale pa en Exchange server','no','2017-05-19 13:48:43',NULL,NULL,1),(68,'%%%G[ordretype]','Lagre ordre (Far tildelt ordrenummer) ordretype: 0 er parkert, 2 er kreditt','no','2017-05-19 13:48:43',NULL,NULL,1),(69,'%%%H','Vise kontantautomat menyen','no','2017-05-19 13:48:43',NULL,NULL,1),(70,'%%%Ix','Taxfree Skjema x=i Utsted skjemax=v Annuler skjemax=r Usted nytt skjems og annuler det gamle x=p Skriv ut skjema pa nytt dersom papirstopp etc x=c Send flere ordrenummer','no','2017-05-19 13:48:43',NULL,NULL,1),(71,'%%%J[VareID]','Se ufakturerte ordre som varen pa aktuell ordrelinje ligger pa, eller pa vare angitt.','no','2017-05-19 13:48:43',NULL,NULL,1),(72,'%%%k[kode]','Vis slette linje og avbryt ordre i kassa (overstyr navarende selger)','no','2017-05-19 13:48:43',NULL,NULL,1),(73,'%%%L<x><y>','Legg til tekst i plukklisteinfo der y er teksten og x er funksjon<br> x=0 Legg til tekst<br>x=1 Legg til tekst pa ny linje<br>x=2 Toggle tekst<br>x=3 Legg til tekst og apne linjeinfo<br>Feks %%$L2Ekstra ost','no','2017-05-19 13:48:43',NULL,NULL,1),(74,'%%%M<w>.<h>.<url>','Apne webside rett i kassa.<br>w=bredde og h er hoyde, url er url. Husk a starte med<br> <<<<<<<http://>>>>>><br>%%$M1024,700,http://showbooking.no?EmpId=<employee.id>%%$M1024,700,http://webshop.no?ArtNo=<orderlines.article.articleno>','no','2017-05-19 13:48:43',NULL,NULL,1),(75,'%%%N<Beskjed>','Viser en beskjed pa skjermen','no','2017-05-19 13:48:43',NULL,NULL,1),(76,'%%%O<felt1>|<Operator>|<Felt2>|<Kommando hvis sant>[|<Kommando hvis usann>]','Vis leveringsinformasjon','no','2017-05-19 13:48:43',NULL,NULL,1),(77,'%%%Q<id>[.parameter]','Start et ordretillegg, NB! Kun etter avtale','no','2017-05-19 13:48:43',NULL,NULL,1),(78,'%%%R','Vis leveringsinformasjon','no','2017-05-19 13:48:43',NULL,NULL,1),(79,'%%%S','Splitt en ordre og ta vare pa leveringsinformasjon','no','2017-05-19 13:48:43',NULL,NULL,1),(80,'%%%T[dager]','Vis tipsrapport for aktuell medarbeider. Dager i minus gir rapport for denne dagen tilbake i tid. Positivt antall dager gir rapport fra sa mange dager og til dagens dato.','no','2017-05-19 13:48:43',NULL,NULL,1),(81,'%%%U','Viser omsetningsrapport pa skjerm','no','2017-05-19 13:48:43',NULL,NULL,1),(82,'%%%V[+]','Spor om merket hvis ikke utfylt (viser bordkart dersom definert) + bak betyr spor alltid, selv om angitt fra for. Altsa overskriv.','no','2017-05-19 13:48:43',NULL,NULL,1),(83,'%%%Y','Importer ordre fra fil<br>Pos 9, 10 lang - Kundenummer<br>Pos 40, 20 lang - Varenummer/Strkkode<br> Pos 60, 10 lang - Antall (kommaseparator som regionale innstillinger pa PC`n)','no','2017-05-19 13:48:43',NULL,NULL,1),(84,'%%%W[0|1]','Endre \"Husk ansatt i kassa\"<br>%%%W toggler<br>%%%W0 Skrur den av<br>%%%W1 skrur den pa','no','2017-05-19 13:48:43',NULL,NULL,1),(85,'%%%X','Vis webshop status','no','2017-05-19 13:48:43',NULL,NULL,1),(86,'%%%Z','Sla opp lojalitetskort status','no','2017-05-19 13:48:43',NULL,NULL,1),(87,'%%%2[Egendefinert sporsmalstekst]','Angi eksternt lojalitetskortnummer pa forrige ordre','no','2017-05-19 13:48:43',NULL,NULL,1),(88,'%%%3[Egendefinert sporsmalstekst]','Sla opp ordrelinjer som er solgt pa eksternt lojalitetskortnummer','no','2017-05-19 13:48:43',NULL,NULL,1),(89,'%%%4','Viser viduet for omregningsfaktor dersom varen har det.','no','2017-05-19 13:48:43',NULL,NULL,1),(90,'%%%5','Oppfrisk kunde','no','2017-05-19 13:48:43',NULL,NULL,1),(91,'%%%6<etikett id>[,varenr[,Spor om a repetere[,antall]]]','Skriver vareetikett','no','2017-05-19 13:48:43',NULL,NULL,1),(92,'%%%7','Velg lager pa aktiv ordrelinje','no','2017-05-19 13:48:43',NULL,NULL,1);
/*!40000 ALTER TABLE `pos_tmpt_commands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_tmpt_in_pos`
--

DROP TABLE IF EXISTS `pos_tmpt_in_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_tmpt_in_pos` (
  `idpos_tmpt_in_pos` int(11) NOT NULL AUTO_INCREMENT,
  `pos_idpos` int(11) NOT NULL,
  `pos_tmpt_idpos_tmpt` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idpos_tmpt_in_pos`),
  KEY `fk_pos_tmpt_in_pos_pos1_idx` (`pos_idpos`),
  KEY `fk_pos_tmpt_in_pos_pos_tmpt1_idx` (`pos_tmpt_idpos_tmpt`),
  CONSTRAINT `fk_pos_tmpt_in_pos_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pos_tmpt_in_pos_pos_tmpt1` FOREIGN KEY (`pos_tmpt_idpos_tmpt`) REFERENCES `pos_tmpt` (`idpos_tmpt`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_tmpt_in_pos`
--

LOCK TABLES `pos_tmpt_in_pos` WRITE;
/*!40000 ALTER TABLE `pos_tmpt_in_pos` DISABLE KEYS */;
INSERT INTO `pos_tmpt_in_pos` VALUES (1,1,5,'2017-04-11 21:03:02','2017-05-30 01:41:44','2017-05-30 01:37:56',1),(2,7,6,'2018-05-10 15:38:29',NULL,NULL,1),(3,9,7,'2018-07-17 19:33:08',NULL,NULL,1);
/*!40000 ALTER TABLE `pos_tmpt_in_pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pos_webshop`
--

DROP TABLE IF EXISTS `pos_webshop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pos_webshop` (
  `idpos_webshop` int(11) NOT NULL AUTO_INCREMENT,
  `pos_idpos` int(11) NOT NULL,
  `webshop_idwebshop` int(11) NOT NULL,
  PRIMARY KEY (`idpos_webshop`),
  KEY `fk_pos_webshop_pos1_idx` (`pos_idpos`),
  KEY `fk_pos_webshop_webshop1_idx` (`webshop_idwebshop`),
  CONSTRAINT `fk_pos_webshop_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pos_webshop_webshop1` FOREIGN KEY (`webshop_idwebshop`) REFERENCES `webshop` (`idwebshop`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pos_webshop`
--

LOCK TABLES `pos_webshop` WRITE;
/*!40000 ALTER TABLE `pos_webshop` DISABLE KEYS */;
/*!40000 ALTER TABLE `pos_webshop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printer`
--

DROP TABLE IF EXISTS `printer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printer` (
  `idprinter` int(11) NOT NULL AUTO_INCREMENT,
  `installed_in` int(11) NOT NULL,
  `id_type_printer` int(11) NOT NULL,
  `port` varchar(30) NOT NULL,
  `description` varchar(150) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idprinter`),
  KEY `id_type_printer` (`id_type_printer`),
  KEY `installed_in` (`installed_in`),
  CONSTRAINT `fk_installed_in` FOREIGN KEY (`installed_in`) REFERENCES `pos` (`idpos`),
  CONSTRAINT `fk_type_printer` FOREIGN KEY (`id_type_printer`) REFERENCES `printer_type` (`idprintertype`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `printer`
--

LOCK TABLES `printer` WRITE;
/*!40000 ALTER TABLE `printer` DISABLE KEYS */;
INSERT INTO `printer` VALUES (1,1,1,'45','yrytr','2018-04-19 15:15:38',NULL,'2018-04-19 15:15:47',3);
/*!40000 ALTER TABLE `printer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `printer_type`
--

DROP TABLE IF EXISTS `printer_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `printer_type` (
  `idprintertype` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(150) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idprintertype`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `printer_type`
--

LOCK TABLES `printer_type` WRITE;
/*!40000 ALTER TABLE `printer_type` DISABLE KEYS */;
INSERT INTO `printer_type` VALUES (1,'Test','Test','2017-05-17 15:50:54',NULL,NULL,1),(2,'Reciept printer','Reciept printer','2017-05-19 13:46:06',NULL,NULL,1),(3,'Pick List Printer','Pick List Printer','2017-05-19 13:46:06',NULL,NULL,1),(4,'Standard Printer','Standard Printer','2017-05-19 13:46:06',NULL,NULL,1),(5,'Additional Printer','Additional Printer','2017-05-19 13:46:06',NULL,NULL,1),(6,'Label Printer','Label Printer','2017-05-19 13:46:06',NULL,NULL,1),(7,'Invoice Printer','Invoice Printer','2017-05-19 13:46:06',NULL,NULL,1),(8,'ghh','fdfgfgghgh','2018-04-19 15:16:37','2018-04-19 15:16:51','2018-04-19 15:16:57',3);
/*!40000 ALTER TABLE `printer_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `idproduct` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_idmanufacturer` int(11) DEFAULT NULL,
  `currency_idcurrency` int(11) DEFAULT NULL COMMENT 'The purchase currency',
  `currency_purchase_price` decimal(11,2) DEFAULT NULL,
  `unit_idunit` int(11) DEFAULT NULL,
  `product_collection_idcollection` int(11) DEFAULT NULL,
  `supplier_idsupplier` int(11) DEFAULT NULL,
  `product_department_idproduct_department` int(11) DEFAULT NULL,
  `warranty_idwarranty` int(11) DEFAULT NULL COMMENT 'The ID of a warranty that the product has by default. Can be NULL.',
  `numberPack` int(11) DEFAULT '1',
  `tax_gp` int(11) DEFAULT NULL,
  `print_for_pick_ist` int(11) DEFAULT NULL,
  `value_for_sell_brute` decimal(11,2) DEFAULT NULL,
  `value_for_sell_liquid` decimal(11,2) DEFAULT NULL,
  `net_weight` decimal(11,2) DEFAULT NULL,
  `additional_weight` decimal(10,0) DEFAULT NULL,
  `volume` decimal(11,2) DEFAULT NULL,
  `length` decimal(11,2) DEFAULT NULL,
  `width` decimal(11,2) DEFAULT NULL,
  `height` decimal(11,2) DEFAULT NULL,
  `minimum_purchase_of_packages` int(11) DEFAULT NULL,
  `minimum_purchase` int(11) NOT NULL DEFAULT '1',
  `name` varchar(80) DEFAULT NULL,
  `product_number` varchar(45) DEFAULT NULL,
  `product_number_supplier` varchar(45) DEFAULT NULL,
  `product_number_manufacturer` varchar(45) DEFAULT NULL,
  `price` decimal(11,2) DEFAULT NULL,
  `price_purchase` decimal(11,2) DEFAULT NULL,
  `contribution` float DEFAULT NULL,
  `ncm` varchar(20) DEFAULT NULL,
  `qtrib` double(17,2) DEFAULT NULL,
  `cfop` varchar(20) DEFAULT NULL,
  `cest` varchar(20) DEFAULT NULL,
  `csosn` varchar(20) DEFAULT NULL,
  `cst` varchar(20) DEFAULT NULL,
  `promotion_weekdays` char(13) DEFAULT NULL COMMENT 'format: 0_0_0_0_0_0_0 (mon,tue,wed,thu,fri,sat,sun)',
  `price_recommended` decimal(11,2) DEFAULT NULL,
  `price_takeaway` decimal(11,2) DEFAULT NULL,
  `price_purchase_list` decimal(11,2) DEFAULT NULL,
  `price_limit_bargain` decimal(11,2) DEFAULT NULL,
  `price_large_scale` decimal(11,2) DEFAULT NULL,
  `profit_gross_profit_value` decimal(11,2) DEFAULT NULL,
  `barcode` varchar(100) DEFAULT NULL,
  `stock_enabled` tinyint(1) DEFAULT '1' COMMENT 'If 0: stock is ignored.',
  `min_stock` int(11) DEFAULT '0',
  `max_stock` int(11) DEFAULT '0',
  `description` longtext,
  `short_description` varchar(500) DEFAULT NULL,
  `has_serial_number` tinyint(1) DEFAULT '0' COMMENT 'Whether this product uses or not serial number. If so, it will be in the table product_serial_number.',
  `factor_purchase` int(11) DEFAULT '1' COMMENT 'The quantity that comes in a package when purchase this product.',
  `factor_sell` int(11) NOT NULL DEFAULT '1' COMMENT 'The quantity that goes in a package when sells this product.',
  `has_components` tinyint(1) DEFAULT '0' COMMENT 'Whether the product is composed by others or not. If so, the quantity in stock should be counted by the quantity of the lower stock quantity component divided by its quantity indicated in the table product_component.',
  `components_show_order_subitens` int(11) DEFAULT NULL,
  `has_size_and_color` tinyint(1) DEFAULT '0' COMMENT 'Whether the product has size and color options.',
  `selling_product` tinyint(1) DEFAULT '1' COMMENT 'Whether this product is sold normally or just as a part of a product.',
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1' COMMENT 'Indicates the status.: 1-Active 2-Passive 3-Expired 4-Blocked 5-Deleted 6-Except temporarily',
  `section_idsection` int(11) DEFAULT NULL,
  `create_by` int(11) NOT NULL,
  `output_control` int(11) DEFAULT NULL COMMENT '0 = no control\n1 = fifo\n2 = lifo\n3 = fefo',
  `type_product` int(11) NOT NULL DEFAULT '1' COMMENT '1 - Product, 2-Voucher,3 - Gift Card',
  `prediction_of_purchase` int(11) DEFAULT '0',
  `Importing_rate` decimal(10,0) DEFAULT NULL,
  `freight_tax_amount` int(11) DEFAULT NULL,
  `value_of_shipping` decimal(11,2) DEFAULT NULL,
  `number_of_serie_from_product` int(11) DEFAULT NULL,
  `base_of_calc` int(11) DEFAULT NULL,
  `multiple_units` int(11) DEFAULT NULL,
  `project` int(11) DEFAULT NULL,
  `trade_value_for_sell_brute` decimal(11,2) DEFAULT NULL,
  `trade_value_for_sell_liquid` decimal(11,2) DEFAULT NULL,
  `value_for_travel_liquid` decimal(11,2) DEFAULT NULL,
  `pricing_of_sell_value_without_tax` decimal(11,2) DEFAULT NULL,
  `recom_value_for_sell_brute` decimal(11,2) NOT NULL,
  `recom_value_for_sell_liquid` decimal(11,2) NOT NULL,
  `value_for_travel_brute` decimal(11,2) NOT NULL,
  PRIMARY KEY (`idproduct`),
  KEY `fk_product_currency1_idx` (`currency_idcurrency`),
  KEY `fk_product_unit1_idx` (`unit_idunit`),
  KEY `fk_product_product_collection1_idx` (`product_collection_idcollection`),
  KEY `fk_product_manufacturer1_idx` (`manufacturer_idmanufacturer`),
  KEY `fk_product_supplier1_idx` (`supplier_idsupplier`),
  KEY `fk_product_product_department1_idx` (`product_department_idproduct_department`),
  KEY `fk_product_warranty1_idx` (`warranty_idwarranty`),
  KEY `fk_product_section1_idx` (`section_idsection`),
  KEY `fk_product_create_by_idx` (`create_by`),
  KEY `fk_gp_tax` (`tax_gp`),
  KEY `print_for_pick_ist` (`print_for_pick_ist`),
  KEY `project` (`project`),
  CONSTRAINT `fk_gp_tax` FOREIGN KEY (`tax_gp`) REFERENCES `tribute` (`idtribute`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_print_pick_list` FOREIGN KEY (`print_for_pick_ist`) REFERENCES `printer` (`idprinter`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_create_by_idx` FOREIGN KEY (`create_by`) REFERENCES `users` (`idusers`),
  CONSTRAINT `fk_product_currency1` FOREIGN KEY (`currency_idcurrency`) REFERENCES `currency` (`idcurrency`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_manufacturer1` FOREIGN KEY (`manufacturer_idmanufacturer`) REFERENCES `manufacturer` (`idmanufacturer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_product_collection1` FOREIGN KEY (`product_collection_idcollection`) REFERENCES `product_collection` (`idcollection`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_product_department1` FOREIGN KEY (`product_department_idproduct_department`) REFERENCES `department` (`iddepartment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_project` FOREIGN KEY (`project`) REFERENCES `project` (`idproject`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_stock` FOREIGN KEY (`section_idsection`) REFERENCES `warehouse` (`idwarehouse`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_supplier1` FOREIGN KEY (`supplier_idsupplier`) REFERENCES `supplier` (`idsupplier`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_unit1` FOREIGN KEY (`unit_idunit`) REFERENCES `unit` (`idunit`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_warranty1` FOREIGN KEY (`warranty_idwarranty`) REFERENCES `warranty` (`idwarranty`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (35,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Coxinhas 10 Unid','1',NULL,NULL,2.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:26:48',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(36,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Coxinhas 15 Unid','2',NULL,NULL,3.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:26:45',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(37,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Coxinhas 25 Unid','3',NULL,NULL,5.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:26:41',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(38,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Coxinhas 50 Unid','4',NULL,NULL,10.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:28:04',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(39,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Coxinhas Doces 10 Unid','5',NULL,NULL,5.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:30:18',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(40,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Combo 1','6',NULL,NULL,12.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'25 Mini Coxinhas, 10 Mini Churros, 1 Refrigerante 250 ML',NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:32:48',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(41,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Combo 2','7',NULL,NULL,16.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'30 Mini Coxinhas, 15 Mini Churros, 1 Refrigerante 250 ML',NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:35:45',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(42,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Combo 3','8',NULL,NULL,26.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'60 Mini Coxinhas, 20 Mini Churros, 2 Refrigerante 250 ML',NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:37:45',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(43,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Combo 4','9',NULL,NULL,36.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,'100 Mini Coxinhas, 25 Mini Churros, 3 Refrigerante 250 ML',NULL,0,1,1,0,NULL,0,1,'2018-09-29 19:39:17',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(44,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Refrigerante 220 ML','10',NULL,NULL,2.50,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 20:08:10',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(45,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Refrigerante 250 ML','11',NULL,NULL,3.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 20:09:25',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(46,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Refrigerante 330 ML','12',NULL,NULL,3.50,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 20:10:41',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(47,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Refrigerante 350 ML','13',NULL,NULL,4.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 20:12:17',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(48,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Refrigerante 600 ML','15',NULL,NULL,5.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 20:13:01',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(49,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Refrigerante 1 LT','16',NULL,NULL,6.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 20:14:35',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(50,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Agua Mineral','17',NULL,NULL,2.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 20:15:42',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00),(51,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'Cafe Filtrado','18',NULL,NULL,3.50,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,0,0,NULL,NULL,0,1,1,0,NULL,0,1,'2018-09-29 20:16:34',NULL,NULL,1,NULL,1,NULL,1,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00,0.00,0.00);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_additional`
--

DROP TABLE IF EXISTS `product_additional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_additional` (
  `idproductadditional` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `additional_type` int(11) DEFAULT NULL,
  `info` text,
  `date_create` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`idproductadditional`),
  KEY `product_idproduct` (`product_idproduct`),
  CONSTRAINT `fk_product_additional` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_additional`
--

LOCK TABLES `product_additional` WRITE;
/*!40000 ALTER TABLE `product_additional` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_additional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_additional_item`
--

DROP TABLE IF EXISTS `product_additional_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_additional_item` (
  `idproductadditionalitem` int(11) NOT NULL AUTO_INCREMENT,
  `id_product_additional` int(11) NOT NULL,
  `product_idproduct` int(11) NOT NULL,
  `group_idgroup` int(11) NOT NULL,
  `price_type` int(11) NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `bruk_factor` int(11) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idproductadditionalitem`),
  KEY `id_product_additional` (`id_product_additional`),
  KEY `product_idproduct` (`product_idproduct`),
  KEY `group_idgroup` (`group_idgroup`),
  CONSTRAINT `fk_product_add_group` FOREIGN KEY (`group_idgroup`) REFERENCES `product_gp` (`idproduct_gp`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_add_product` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_additional_indx` FOREIGN KEY (`id_product_additional`) REFERENCES `product_additional` (`idproductadditional`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_additional_item`
--

LOCK TABLES `product_additional_item` WRITE;
/*!40000 ALTER TABLE `product_additional_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_additional_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `idproduct_category` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `category_idcategory` int(11) NOT NULL,
  PRIMARY KEY (`idproduct_category`),
  KEY `fk_product_category_product1_idx` (`product_idproduct`),
  KEY `fk_product_category_category1_idx` (`category_idcategory`),
  CONSTRAINT `fk_product_category_category1` FOREIGN KEY (`category_idcategory`) REFERENCES `category` (`idcategory`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_category_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_collection`
--

DROP TABLE IF EXISTS `product_collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_collection` (
  `idcollection` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `date_start` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idcollection`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_collection`
--

LOCK TABLES `product_collection` WRITE;
/*!40000 ALTER TABLE `product_collection` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_component`
--

DROP TABLE IF EXISTS `product_component`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_component` (
  `idproduct_component` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL COMMENT 'The ID of the main product.',
  `product_idproduct1` int(11) NOT NULL COMMENT 'The ID of the component.',
  `qty` int(11) NOT NULL DEFAULT '1',
  `cost_price` decimal(11,2) NOT NULL,
  `retail_price` decimal(11,2) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idproduct_component`),
  KEY `fk_product_component_product1_idx` (`product_idproduct`),
  KEY `fk_product_component_product2_idx` (`product_idproduct1`),
  CONSTRAINT `fk_product_component_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_component_product2` FOREIGN KEY (`product_idproduct1`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_component`
--

LOCK TABLES `product_component` WRITE;
/*!40000 ALTER TABLE `product_component` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_component` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_department`
--

DROP TABLE IF EXISTS `product_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_department` (
  `idproduct_department` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idproduct_department`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_department`
--

LOCK TABLES `product_department` WRITE;
/*!40000 ALTER TABLE `product_department` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_gp`
--

DROP TABLE IF EXISTS `product_gp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_gp` (
  `idproduct_gp` int(11) NOT NULL AUTO_INCREMENT,
  `product_gp_idproduct_gp` int(11) DEFAULT NULL COMMENT 'If it is a subgroup, it is referred to another group.',
  `name` varchar(45) NOT NULL,
  `printer` int(11) DEFAULT NULL,
  `rules` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idproduct_gp`),
  KEY `fk_product_gp_product_gp1_idx` (`product_gp_idproduct_gp`),
  KEY `fk_printer_idx` (`printer`),
  KEY `fk_department_idx` (`department`),
  KEY `fk_rules_idx` (`rules`),
  CONSTRAINT `fk_department_idx` FOREIGN KEY (`department`) REFERENCES `department` (`iddepartment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_printer_idx` FOREIGN KEY (`printer`) REFERENCES `printer` (`idprinter`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_gp_product_gp1` FOREIGN KEY (`product_gp_idproduct_gp`) REFERENCES `product_gp` (`idproduct_gp`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_rules_idx` FOREIGN KEY (`rules`) REFERENCES `order_rule` (`idorderrule`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_gp`
--

LOCK TABLES `product_gp` WRITE;
/*!40000 ALTER TABLE `product_gp` DISABLE KEYS */;
INSERT INTO `product_gp` VALUES (1,NULL,'Juice',NULL,NULL,NULL,'2017-05-25 13:43:00',NULL,NULL,1),(2,NULL,'Visa',NULL,1,8,'2017-06-01 13:17:57',NULL,'2017-06-01 14:17:57',3),(3,NULL,'test3',NULL,NULL,8,'2018-04-19 13:57:29','2018-04-19 14:57:17','2018-04-19 14:57:29',3),(4,NULL,'Teste 2',NULL,NULL,NULL,'2018-08-07 13:35:39',NULL,'2018-08-07 14:35:39',3),(5,NULL,'111',NULL,NULL,8,'2018-08-07 13:36:40','2018-08-07 14:36:36','2018-08-07 14:36:40',3),(6,NULL,'Teste 21',NULL,NULL,8,'2018-08-07 13:41:04','2018-08-07 14:40:58','2018-08-07 14:41:04',3),(7,NULL,'1',NULL,NULL,11,'2018-08-07 13:41:24','2018-08-07 14:41:21','2018-08-07 14:41:24',3),(8,NULL,'11',NULL,NULL,NULL,'2018-08-07 13:41:37',NULL,'2018-08-07 14:41:37',3);
/*!40000 ALTER TABLE `product_gp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_gp_alternatives`
--

DROP TABLE IF EXISTS `product_gp_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_gp_alternatives` (
  `idproductgpalternatives` int(11) NOT NULL AUTO_INCREMENT,
  `idproductgp` int(11) NOT NULL,
  `description` varchar(150) NOT NULL,
  `priceChange` decimal(10,0) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idproductgpalternatives`),
  KEY `idproductgp` (`idproductgp`),
  CONSTRAINT `fk_product_gp_1` FOREIGN KEY (`idproductgp`) REFERENCES `product_gp` (`idproduct_gp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_gp_alternatives`
--

LOCK TABLES `product_gp_alternatives` WRITE;
/*!40000 ALTER TABLE `product_gp_alternatives` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_gp_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image`
--

DROP TABLE IF EXISTS `product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_image` (
  `idproduct_image` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `path` longblob NOT NULL,
  `order` int(11) DEFAULT NULL,
  `main` int(11) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idproduct_image`),
  KEY `fk_product_image_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_product_image_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image`
--

LOCK TABLES `product_image` WRITE;
/*!40000 ALTER TABLE `product_image` DISABLE KEYS */;
INSERT INTO `product_image` VALUES (4,32,_binary 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJcAAACXCAMAAAAvQTlLAAAAkFBMVEWwCAi+DAz/1gD/2gC8AAz/2AD/3ACtAAj/3wC6AA2wAAjFXgb/4QCqAAizCQm3AA3ckATKSQrSeAXdhAjUgAayGgjlpAT0vAT6ywK8PAfLZwbAGgzAUAf0wATXcgn5zgLEMwvaiAbtrQXstQPJQgvOVAq4LwfCKQvimgTmrgS6RAfOcQbTYwqxJgjllgfFWAcOz0ZxAAAH3klEQVR4nO2Za3eiOhSGKySBUBBrW6Raq3irrR39///ucMkNSOLGOuucdVbeLzNqSh73PfFh9N/Uw78NYJDjGibHNUyOa5gc1zA5rmFyXMPkuIbJcQ2T4xomxzVMjmuYHNcwOa5hclzD5LiGyXEN0y1cqBRwGWSdVoO5EB3tf/Y7Sq+uW+8v+wW9kWwgV7lblq88b7WcWm2B0Mc29zwv3+xuIxvGhUZHQnAlP84/zCajP8vYr9eROPu8BWwQF1pvQ+wx+f7ZZDL6jH2+zAuXT9d8/ksutPOIpyje6sFoJumrL0BuABvAhdZLwjbym33DTMdFz2H9KS7XNetXT4NdCedCqMHC4WaS5YSB9S1Bi5DRZMWm+QK+txgKBuei57jejzwjSrnpwl7w0z1pWPIdoujDq1+QzV/jQh+NU+KPtHq1mNUvsddxEfpc1SQ4r9OQ/jRfJnweGGJgLprXIGSWNvuvG2+RbXtDdGSG/Gl46bFeh8loiMXGYygXC2a82rHnp8+E2U8Fo3vm7GPKOD/zwZ4cP5SCLUW7xj1EZCBa56rD+LvLrnvpM8vOPRCspoJy0YxZRyYWZQYjZ7kh4u8dpRFTlpNLQIRRBgXlQp/MPdtUeZcZAkvWRROEHlGygZez+KrBxpIKyEU3zA4/yrPTY9c49LnB92cKPnpiGTqzc6lQQC4eXXi5VmPp0nDhFY8w1IRcmQsqApo1f+xdzGDjDhWQi7nCP7ZihNUwj0yaDdG0eY1zFX9Ep6ykbEwR1oOCcq1wqyjxDZkj/Zy9sWGc7ZrAg9MLtd2I6qhAXOijeTD2OzV0z8aLuOZFT2x/f9reP+XZcO4bTA8F46LcX61srEjYlOVvqg/SM8PE6zaXGDCWiw6UkQrChfYrXThXJCxN6zKKFmwZzrv4P4wfz9UHWKBgXM98924FoszB1YwhGnvfXWjH8lQpt4aoGuTHJUuzZXdSR08Ys4K1GKGMR1uvgKItewJGEFPBuNCah3Ov9ZYTLBuYwzUfcJTWLkTPKrItqgZw8fmzdFYvnVDGfBcW6Q/H3y56j+CfkU0EYYJxzfje/QZHC56Ry5S7kRx7y9CIceH8FQp2DYu3t3LDflUUFax0Hl/WrV6VUv6hNw/uxcXTrJf+tSHEwS1bsf/oBi1eUDxSJPfiEv7RnH3KUs4NIf/VNJuUx6i/vBcXr5ZePNVxbVon3SbSuovG4+CRL4uhAXbNXDsWsl6oG1P4lCwlJnsBVW4SvYtvBw2wK1xiY6w9NCNeAYRaJzJe1aMDL3QkAzryCle65WHfq/Y11xPucCnVnspdIvmYCOZIOxZa8MD2t9rP13kbDPs7Xa9JeAHGq2+YI69w7Xmi+f1yWYnPrIKrmVW7rSYpeODj0124pvx5fqGdgkUn4svKLqSZFYK5SJ/JXbiOnIvMtfai5zYXybThExxiAX4PLukm0psSmEE79jKYI+FceJWAAt9urk/MwzrUH7LELMq5XgxcoiHEB5DB7FyiqmKv3x2bFZ24N9TNZMnXASurnWsqwqLfXphwu1AYykCSiUA9giqrlYvKsM8MXKmncmHvjz56Et65y5n791wpbx/asx9bojqyjGr9NsGJc+H8ARL4Vq6RmOdC3TRRapy0Jgqcm7jERAGs+DYqOat2rwA41fghOapc/tbAFV14HGLv8ddccxE7RDPc12U9eFEnHeO4EH2Lr0imv+YSRbN37yw6oOwwtVkLw57Rm2jwBNSJrFxnERT5znCEDy4tLpMtoj8yhUCHNRvXSMR0a/pqteXoW83H2Bw7fAIrSyEkIW1YsjtWB30NVM2lFrD43cSVfMmYgMz4Nq5PEav+poHqTzBK4FTr3kxbJnIgAp09bOH1KVKtupvQX8FEB4ULe2auiagnMaSA2bhE1y6n1dRw21EGtAwwvDoYuQrJBencNi552PEnpqYWvW4VrtzQHksu0YjKYgLokBYu5XBoPr9HD1+Sy18auYK5fBjkrGbjEuXLIyfjo4JMNiJ/awzp4F0a39SsgFzKIZ+YQyJRub6MpSl4E1wYcklh4Ypmsj1eQFwkM+4TvUquHHC2NVKVo4KsAMSc2snEV7iMy6JAxJcla69xVbUqilaSy3xYSAqFyzIjJ0RyAQqY1lR1rYoOCpe5RCcvKpd5Q4XLM0eFmUuU9UDpfL45ItpcFnspfRRQWLWmargu8hIQm/cLTgqXsfyWXLlcB7ij0Jqq2VBOq9izcM2Vumo41dZc8oaFWJb1ubrDQnCCcT3+TS7a/yR4EZHqm4451bKLjBvfMronW1mlTdN2h4tqfxoJ5ARgu0YGc8n6a0tbyaUxVY/LckYO3odzmcuvkPlHpEBOcrZOG7z7MK7jIC6zguNgLtNtTpfr6xdYYK43DOOayKl8C7qiMChSA+IOXIXkmv2G6+EvcoF/7tNJGQstfS94C0PSKAxtXPIiw19WXEFyU/RH8oRs5YoOEynLABNMJVf182gyn0yDG8wG5SonPkXmZQpX/bPtZPo9P97gT/UABrwTtarD9V28nLIT6Cqsy6U0Wsv8cgvX6jX4nr7MindAA9dx4UZeeA+uU+xh/sDX6K2YXooX4zWLjWuL72qvkzw3rV6j4FR8n4qbysX7o5DxOgSu6CAf91i+Tt5O77d9WyXNfo/Vzlv2+g5PdXJycnJycnJycnJycnJycnJycvrf6x+h0J2r/WsjDQAAAABJRU5ErkJggg==',1,1,'2017-06-01 14:17:02',NULL,'2017-06-01 14:17:06',3);
/*!40000 ALTER TABLE `product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_in_section`
--

DROP TABLE IF EXISTS `product_in_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_in_section` (
  `idproduct_in_section` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '1' COMMENT 'Quantity in stock',
  `section_idsection` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration_date` timestamp NULL DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idproduct_in_section`),
  KEY `fk_product_in_warehouse_product1_idx` (`product_idproduct`),
  KEY `fk_product_in_section_section1_idx` (`section_idsection`),
  CONSTRAINT `fk_product_in_section_section1` FOREIGN KEY (`section_idsection`) REFERENCES `section` (`idsection`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_in_warehouse_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_in_section`
--

LOCK TABLES `product_in_section` WRITE;
/*!40000 ALTER TABLE `product_in_section` DISABLE KEYS */;
INSERT INTO `product_in_section` VALUES (4,29,500,1,'2018-08-01 04:00:00','2018-08-31 04:00:00','2018-08-07 17:11:56',NULL,NULL,1),(5,1,120,1,'2018-08-15 04:00:00','2019-02-13 05:00:00','2018-08-15 17:24:33',NULL,NULL,1);
/*!40000 ALTER TABLE `product_in_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_included_and_substitute`
--

DROP TABLE IF EXISTS `product_included_and_substitute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_included_and_substitute` (
  `id_product_ included_and_substitute` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `product_incl_subs` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id_product_ included_and_substitute`),
  KEY `product_idproduct` (`product_idproduct`),
  KEY `product_incl_subs` (`product_incl_subs`),
  CONSTRAINT `fk_product_subs_inclu` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`),
  CONSTRAINT `fk_product_subs_inclu_2` FOREIGN KEY (`product_incl_subs`) REFERENCES `product` (`idproduct`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_included_and_substitute`
--

LOCK TABLES `product_included_and_substitute` WRITE;
/*!40000 ALTER TABLE `product_included_and_substitute` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_included_and_substitute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_order`
--

DROP TABLE IF EXISTS `product_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_order` (
  `idproduct` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `percent` decimal(11,0) DEFAULT NULL,
  `the_sale_price_will` int(11) DEFAULT NULL COMMENT 'The sale price will only be set at the time of the first purchase',
  `do_not_put_a_selling_price` int(11) DEFAULT NULL COMMENT 'Do not put a selling price, ask for every time the price',
  `do_not_allow_discount` int(11) DEFAULT NULL COMMENT 'Do not allow discount for this product',
  `product_only_to_buy` int(11) DEFAULT NULL COMMENT 'Product only to buy another product, can not be sold alone',
  `print_product_information_in_tax_coupon` int(11) DEFAULT NULL COMMENT 'Print product information in the tax coupon',
  `fill_quantity_based_on_the_information` int(11) DEFAULT NULL COMMENT 'Fill quantity based on the information of a scale integrated to the system',
  `hide_from_statistics` int(11) DEFAULT NULL COMMENT 'Hide from statistics',
  `display_alternate` int(11) DEFAULT NULL COMMENT 'Display Alternate Products at Checkout',
  `the_purchase_price` int(11) DEFAULT NULL COMMENT 'The purchase price will only be set at the time of the first purchase',
  `view_product_description` int(11) DEFAULT NULL COMMENT 'View product description in POS',
  `do_not_show_product` int(11) DEFAULT NULL COMMENT 'Do not show product on tax coupon',
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idproduct`),
  KEY `product_idproduct` (`product_idproduct`),
  CONSTRAINT `fk_product` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_order`
--

LOCK TABLES `product_order` WRITE;
/*!40000 ALTER TABLE `product_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_product_gp`
--

DROP TABLE IF EXISTS `product_product_gp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_product_gp` (
  `idproduct_product_gp` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `product_gp_idproduct_gp` int(11) NOT NULL,
  PRIMARY KEY (`idproduct_product_gp`),
  KEY `fk_product_product_gp_product_gp1_idx` (`product_gp_idproduct_gp`),
  KEY `fk_product_product_gp_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_product_product_gp_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_product_gp_product_gp1` FOREIGN KEY (`product_gp_idproduct_gp`) REFERENCES `product_gp` (`idproduct_gp`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_product_gp`
--

LOCK TABLES `product_product_gp` WRITE;
/*!40000 ALTER TABLE `product_product_gp` DISABLE KEYS */;
INSERT INTO `product_product_gp` VALUES (2,32,1);
/*!40000 ALTER TABLE `product_product_gp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_promotional`
--

DROP TABLE IF EXISTS `product_promotional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_promotional` (
  `idproductpromotional` int(11) NOT NULL AUTO_INCREMENT,
  `idproduct` int(11) NOT NULL,
  `gross_price` decimal(10,0) NOT NULL,
  `initial_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `price_without_tax` decimal(10,0) NOT NULL,
  `final_date` timestamp NULL DEFAULT NULL,
  `week` varchar(30) NOT NULL,
  `date_create` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idproductpromotional`),
  KEY `product` (`idproduct`),
  CONSTRAINT `fk_product_promotional` FOREIGN KEY (`idproduct`) REFERENCES `product` (`idproduct`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_promotional`
--

LOCK TABLES `product_promotional` WRITE;
/*!40000 ALTER TABLE `product_promotional` DISABLE KEYS */;
INSERT INTO `product_promotional` VALUES (2,28,23,'2018-07-24 04:00:00',12,'2018-07-31 04:00:00','0-0-1-1-1-1-1','2017-05-25 13:21:08','2018-07-24 18:00:26',NULL,1);
/*!40000 ALTER TABLE `product_promotional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_serial_number`
--

DROP TABLE IF EXISTS `product_serial_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_serial_number` (
  `idproduct_serial_number` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `order_line_idorder_line` int(11) DEFAULT NULL,
  `serial` varchar(200) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `section_idsection` int(11) DEFAULT NULL,
  PRIMARY KEY (`idproduct_serial_number`),
  KEY `fk_product_serial_number_product1_idx` (`product_idproduct`),
  KEY `fk_product_serial_number_order_line1_idx` (`order_line_idorder_line`),
  KEY `fk_product_serial_number_section1_idx` (`section_idsection`),
  CONSTRAINT `fk_product_serial_number_order_line1` FOREIGN KEY (`order_line_idorder_line`) REFERENCES `order_line` (`idorder_line`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_serial_number_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_serial_number_section1` FOREIGN KEY (`section_idsection`) REFERENCES `section` (`idsection`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_serial_number`
--

LOCK TABLES `product_serial_number` WRITE;
/*!40000 ALTER TABLE `product_serial_number` DISABLE KEYS */;
INSERT INTO `product_serial_number` VALUES (3,33,NULL,'23123123','2018-08-07 12:00:45','2018-08-07 13:01:06','2018-08-07 13:01:15',3,NULL),(4,1,NULL,'aaaaa','2018-08-07 12:00:52',NULL,'2018-08-07 13:00:57',3,NULL),(5,1,NULL,'23123123','2018-08-07 12:08:30',NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `product_serial_number` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_wholesale_price`
--

DROP TABLE IF EXISTS `product_wholesale_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_wholesale_price` (
  `id_product_wholesale_price` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `unit_price` decimal(11,2) NOT NULL,
  `qtd` int(11) NOT NULL DEFAULT '1',
  `price_for_quantity` decimal(11,2) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id_product_wholesale_price`),
  KEY `product_idproduct` (`product_idproduct`),
  CONSTRAINT `fk_wholesale_product` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_wholesale_price`
--

LOCK TABLES `product_wholesale_price` WRITE;
/*!40000 ALTER TABLE `product_wholesale_price` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_wholesale_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_wty`
--

DROP TABLE IF EXISTS `product_wty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_wty` (
  `idproduct_wty` int(11) NOT NULL AUTO_INCREMENT,
  `warranty_idwarranty` int(11) NOT NULL,
  `product_idproduct` int(11) NOT NULL,
  `value` decimal(11,2) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idproduct_wty`),
  KEY `fk_product_wty_warranty1_idx` (`warranty_idwarranty`),
  KEY `fk_product_wty_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_product_wty_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_wty_warranty1` FOREIGN KEY (`warranty_idwarranty`) REFERENCES `warranty` (`idwarranty`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_wty`
--

LOCK TABLES `product_wty` WRITE;
/*!40000 ALTER TABLE `product_wty` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_wty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `idproject` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` longtext,
  `date_start` timestamp NULL DEFAULT NULL,
  `date_stop` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `price` decimal(11,2) NOT NULL,
  `priceAdditional` decimal(11,2) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted\n4-Removed',
  PRIMARY KEY (`idproject`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'Test','Test','2017-05-02 04:00:00','2017-05-31 04:00:00','2017-05-31 04:00:00',1.23,1.23,'2017-05-17 15:46:05','2017-05-27 14:39:21','2017-05-18 14:02:23',1),(2,'Test2','gdfgdgfd','2018-04-09 04:00:00','2018-04-12 04:00:00','2018-05-03 04:00:00',446455.00,40.00,'2018-04-19 14:10:47','2018-04-19 14:11:09','2018-07-19 13:46:00',3),(3,'test','sss','2018-07-11 04:00:00','2018-07-03 04:00:00','2018-07-03 04:00:00',1.00,1.00,'2018-07-19 19:33:07',NULL,NULL,1);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase`
--

DROP TABLE IF EXISTS `purchase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase` (
  `idpurchase` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_idsupplier` int(11) NOT NULL,
  `user_iduser` int(11) NOT NULL,
  `extra_info` longtext,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `date_delivery` timestamp NULL DEFAULT NULL,
  `date_delivered` timestamp NULL DEFAULT NULL,
  `discount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `id` int(11) NOT NULL COMMENT 'An id used in the company, not the one of DB.',
  PRIMARY KEY (`idpurchase`),
  KEY `fk_purchase_supplier1_idx` (`supplier_idsupplier`),
  KEY `fk_purchase_user1_idx` (`user_iduser`),
  CONSTRAINT `fk_purchase_supplier1` FOREIGN KEY (`supplier_idsupplier`) REFERENCES `supplier` (`idsupplier`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_user1` FOREIGN KEY (`user_iduser`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase`
--

LOCK TABLES `purchase` WRITE;
/*!40000 ALTER TABLE `purchase` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_line`
--

DROP TABLE IF EXISTS `purchase_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_line` (
  `idpurchase_line` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `purchase_idpurchase` int(11) NOT NULL,
  `value` decimal(11,2) NOT NULL,
  `discount` decimal(11,2) NOT NULL DEFAULT '0.00',
  `extra_info` longtext,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `not_delivered` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'When it was expected to receive a product, and the purchase arrive with it missing, this value is 1.',
  `section_idsection` int(11) NOT NULL,
  PRIMARY KEY (`idpurchase_line`),
  KEY `fk_purchase_line_product1_idx` (`product_idproduct`),
  KEY `fk_purchase_line_purchase1_idx` (`purchase_idpurchase`),
  KEY `fk_purchase_line_section1_idx` (`section_idsection`),
  CONSTRAINT `fk_purchase_line_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_line_purchase1` FOREIGN KEY (`purchase_idpurchase`) REFERENCES `purchase` (`idpurchase`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_line_section1` FOREIGN KEY (`section_idsection`) REFERENCES `section` (`idsection`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_line`
--

LOCK TABLES `purchase_line` WRITE;
/*!40000 ALTER TABLE `purchase_line` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_serial_number`
--

DROP TABLE IF EXISTS `purchase_serial_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_serial_number` (
  `idpurchase_serial_number` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_line_idpurchase_line` int(11) NOT NULL,
  `product_serial_number_idproduct_serial_number` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idpurchase_serial_number`),
  KEY `fk_purchase_serial_number_purchase_line1_idx` (`purchase_line_idpurchase_line`),
  KEY `fk_purchase_serial_number_product_serial_number1_idx` (`product_serial_number_idproduct_serial_number`),
  CONSTRAINT `fk_purchase_serial_number_product_serial_number1` FOREIGN KEY (`product_serial_number_idproduct_serial_number`) REFERENCES `product_serial_number` (`idproduct_serial_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_purchase_serial_number_purchase_line1` FOREIGN KEY (`purchase_line_idpurchase_line`) REFERENCES `purchase_line` (`idpurchase_line`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_serial_number`
--

LOCK TABLES `purchase_serial_number` WRITE;
/*!40000 ALTER TABLE `purchase_serial_number` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchase_serial_number` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `register_customer_type`
--

DROP TABLE IF EXISTS `register_customer_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `register_customer_type` (
  `idregister_customer_type` int(11) NOT NULL AUTO_INCREMENT,
  `register_idregister` int(11) NOT NULL,
  `customer_type_idcustumer_type` int(11) NOT NULL,
  PRIMARY KEY (`idregister_customer_type`),
  KEY `fk_register_customer_type_register1_idx` (`register_idregister`),
  KEY `fk_register_customer_type_customer_type1_idx` (`customer_type_idcustumer_type`),
  CONSTRAINT `fk_register_customer_type_customer_type1` FOREIGN KEY (`customer_type_idcustumer_type`) REFERENCES `customer_type` (`idcustumer_type`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_register_customer_type_register1` FOREIGN KEY (`register_idregister`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `register_customer_type`
--

LOCK TABLES `register_customer_type` WRITE;
/*!40000 ALTER TABLE `register_customer_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `register_customer_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_credit_manager`
--

DROP TABLE IF EXISTS `report_credit_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_credit_manager` (
  `id_report_credit_manager` int(11) NOT NULL AUTO_INCREMENT,
  `send_after` int(11) NOT NULL,
  `send_copy` int(11) DEFAULT NULL,
  `message_title` varchar(300) NOT NULL,
  `message` varchar(300) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id_report_credit_manager`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_credit_manager`
--

LOCK TABLES `report_credit_manager` WRITE;
/*!40000 ALTER TABLE `report_credit_manager` DISABLE KEYS */;
INSERT INTO `report_credit_manager` VALUES (1,2,NULL,'Test','Test','2017-05-17 15:53:39','2017-05-17 15:53:47','2017-05-17 15:53:53',3),(2,123,NULL,'Test','Test','2017-05-24 13:29:11','2017-05-24 13:29:16',NULL,1);
/*!40000 ALTER TABLE `report_credit_manager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `idrequests` int(11) NOT NULL AUTO_INCREMENT,
  `query` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL COMMENT 'status 0 = pendente 1 = processado 2 = descartado',
  `direction` int(11) NOT NULL COMMENT 'direction 0 = saindo 1 = chegando',
  PRIMARY KEY (`idrequests`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `screen`
--

DROP TABLE IF EXISTS `screen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `screen` (
  `idscreen` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idscreen`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `screen`
--

LOCK TABLES `screen` WRITE;
/*!40000 ALTER TABLE `screen` DISABLE KEYS */;
/*!40000 ALTER TABLE `screen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `idsection` int(11) NOT NULL AUTO_INCREMENT,
  `shelf_idshelf` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `position_left` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `fixed` int(11) NOT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idsection`),
  KEY `fk_section_shelf1_idx` (`shelf_idshelf`),
  CONSTRAINT `fk_section_shelf1` FOREIGN KEY (`shelf_idshelf`) REFERENCES `shelf` (`idshelf`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (1,1,'Standard',0,'2017-05-31 18:28:37',NULL,NULL,1,1);
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section_transfer`
--

DROP TABLE IF EXISTS `section_transfer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section_transfer` (
  `idsection_transfer` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `product_idproduct` int(11) NOT NULL,
  `product_serial_number_idproduct_serial_number` int(11) DEFAULT NULL,
  `size_color_section_idsize_color_section` int(11) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `extra_info` longtext,
  `section_idsection` int(11) NOT NULL,
  `section_idsection1` int(11) NOT NULL,
  PRIMARY KEY (`idsection_transfer`),
  KEY `fk_warehouse_transfer_product1_idx` (`product_idproduct`),
  KEY `fk_warehouse_transfer_product_serial_number1_idx` (`product_serial_number_idproduct_serial_number`),
  KEY `fk_section_transfer_size_color_section1_idx` (`size_color_section_idsize_color_section`),
  KEY `fk_section_transfer_section1_idx` (`section_idsection`),
  KEY `fk_section_transfer_section2_idx` (`section_idsection1`),
  CONSTRAINT `fk_section_transfer_section1` FOREIGN KEY (`section_idsection`) REFERENCES `section` (`idsection`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_section_transfer_section2` FOREIGN KEY (`section_idsection1`) REFERENCES `section` (`idsection`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_section_transfer_size_color_section1` FOREIGN KEY (`size_color_section_idsize_color_section`) REFERENCES `size_color_section` (`idsize_color_section`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_warehouse_transfer_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_warehouse_transfer_product_serial_number1` FOREIGN KEY (`product_serial_number_idproduct_serial_number`) REFERENCES `product_serial_number` (`idproduct_serial_number`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section_transfer`
--

LOCK TABLES `section_transfer` WRITE;
/*!40000 ALTER TABLE `section_transfer` DISABLE KEYS */;
/*!40000 ALTER TABLE `section_transfer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `idsettings` int(11) NOT NULL AUTO_INCREMENT,
  `language_idlanguage` int(11) NOT NULL COMMENT 'The language used in the system.',
  `screen_idscreen` int(11) NOT NULL COMMENT 'The initial screen.',
  `sms_platform_idsms_platform` int(11) NOT NULL,
  `import_only_prod_name` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'If it is 1, the quantities of products are not imported from the supplier.',
  `print_sold_prod_at_closure` tinyint(1) DEFAULT '0' COMMENT 'If it is 1, at the closure, the system will print a list with all sold products at this day.',
  `product_numeration_min` int(11) DEFAULT NULL COMMENT 'Some companies want to establish an interval for their products numeration. This is what number the interval will start.',
  `product_numeration_max` int(11) DEFAULT NULL COMMENT 'Some companies want to establish an interval for their products numeration. This is the last number allowed.',
  `apply_discount_invoice` tinyint(1) DEFAULT '0' COMMENT 'Apply automatically product default discount for clients who pay by invoice',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `invoice_sn` int(11) NOT NULL DEFAULT '1' COMMENT 'Invoice Start Number',
  `pos_closure_log_sn` int(11) NOT NULL DEFAULT '1' COMMENT 'POS closure start number',
  `accounting_report_sn` int(11) NOT NULL DEFAULT '1' COMMENT 'Accounting report start number',
  `invoice_report_sn` int(11) NOT NULL DEFAULT '1' COMMENT 'Invoice report start number',
  `webshop_sale_report_sn` int(11) NOT NULL DEFAULT '1' COMMENT 'Webshop sale report start number',
  `show_pos_session_ir` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Show the POS session in the internal report.',
  `show_pos_session_ar` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Show the POS session in the accounting report.',
  `show_project_of_sale` tinyint(1) NOT NULL DEFAULT '0',
  `password_admin` varchar(45) NOT NULL,
  `password_setup` varchar(45) NOT NULL,
  `password_closure` varchar(45) NOT NULL COMMENT 'Password to close a POS.',
  `password_products` varchar(45) NOT NULL COMMENT 'Password to edit products.',
  `password_cancel_o` varchar(45) NOT NULL COMMENT 'Password to cancel orders.',
  `password_cancel_p` varchar(45) NOT NULL COMMENT 'Password to cancel products from an order.',
  `sender_mail` varchar(90) DEFAULT NULL,
  `alerts_mail` varchar(45) DEFAULT NULL COMMENT 'Email to receive alerts of the system.',
  `use_default_email_soft` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Use default email software from the operational system.',
  `sms_user` varchar(45) DEFAULT NULL,
  `sms_password` varchar(45) DEFAULT NULL,
  `send_sms_at_closure` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'If the system must send a sms message with the total amount in POS at the closure.',
  `mobile_sms_receiver` varchar(30) DEFAULT NULL COMMENT 'A mobile number to receive the sms message with the total amount in POS at the closure.',
  `exchange_platform_idexchange_platform` int(11) NOT NULL,
  `exchange_password` varchar(45) DEFAULT NULL,
  `exchange_user` varchar(45) DEFAULT NULL,
  `closure_info` longtext COMMENT 'A text to be printed in the closure document.',
  `print_qty_prod_sold_at_closure` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print the quantities of produtcs sold in this day.',
  `print_money_coin_in_draw` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print the relation of coins and money bills in the cashdraw.',
  `include_sale_by_tax` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Include the sales by tax groups',
  `use_common_printer` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Use common printer instead of fiscal printer',
  `print_discount_at_closure` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print a list of discounts gave at this day.',
  `qty_credit_note_to_print` int(11) NOT NULL DEFAULT '2',
  `credit_note_extra_info` longtext,
  `print_credit_note_use_status` tinyint(1) NOT NULL DEFAULT '0',
  `print_gift_card_use_status` tinyint(1) NOT NULL DEFAULT '0',
  `qty_invoice_to_print` int(11) NOT NULL DEFAULT '1',
  `inv_qty_decimals` int(11) NOT NULL DEFAULT '2' COMMENT 'Decimal quantities used in invoices.',
  `inv_use_common_printer` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Use common printer instead of fiscal printer, to print invoices.',
  `inv_print_value_inc_tax` tinyint(1) NOT NULL DEFAULT '0',
  `inv_print_project_info` tinyint(1) NOT NULL DEFAULT '0',
  `inv_folder` varchar(300) NOT NULL COMMENT 'The folder where are saved the invoices.',
  `closure_canceled_order` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print canceled orders at closure.',
  `closure_canceled_prod` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print canceled products at closure.',
  `closure_draw_opening` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print manueal cashdraw openings at closure.',
  `rep_closure_gen_pdf` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Generate the report closure only in PDF.',
  `rep_closure_send_report` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Send the report closure.',
  `rep_closure_show_screen` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Show in the screen the closure report.',
  `rep_closure_print` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print the closure report.',
  `rep_inv_closure_gen_pdf` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Generate the report invoice closure only in PDF.',
  `rep_inv_closure_send_report` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Send the invoice report closure.',
  `rep_inv_closure_show_screen` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Show in the screen the invoice closure report.',
  `rep_inv_closure_print` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print the invoice closure report.',
  `rep_inv_detailed` tinyint(1) NOT NULL DEFAULT '0',
  `rep_inv_send_cred_adm` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Send the report invoice to the credit administrator.',
  `rep_web_pay_gen_pdf` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Generate the report of webshop payments only in PDF.',
  `rep_web_pay_send_report` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Send the webshop payments report.',
  `rep_web_pay_show_screen` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Show in the screen the webshop payments report.',
  `rep_web_pay_print` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print the webshop payments report.',
  `rep_web_pay_detailed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idsettings`),
  KEY `fk_settings_language1_idx` (`language_idlanguage`),
  KEY `fk_settings_screen1_idx` (`screen_idscreen`),
  KEY `fk_settings_sms_platform1_idx` (`sms_platform_idsms_platform`),
  KEY `fk_settings_exchange_platform1_idx` (`exchange_platform_idexchange_platform`),
  CONSTRAINT `fk_settings_exchange_platform1` FOREIGN KEY (`exchange_platform_idexchange_platform`) REFERENCES `exchange_platform` (`idexchange_platform`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_settings_language1` FOREIGN KEY (`language_idlanguage`) REFERENCES `language` (`idlanguage`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_settings_screen1` FOREIGN KEY (`screen_idscreen`) REFERENCES `screen` (`idscreen`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_settings_sms_platform1` FOREIGN KEY (`sms_platform_idsms_platform`) REFERENCES `sms_platform` (`idsms_platform`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings_accounting`
--

DROP TABLE IF EXISTS `settings_accounting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings_accounting` (
  `idsettingsaccounting` int(11) NOT NULL AUTO_INCREMENT,
  `data_type` varchar(250) NOT NULL,
  `data_value` varchar(250) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idsettingsaccounting`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings_accounting`
--

LOCK TABLES `settings_accounting` WRITE;
/*!40000 ALTER TABLE `settings_accounting` DISABLE KEYS */;
INSERT INTO `settings_accounting` VALUES (1,'info_cash_acc','0000','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(2,'info_cash_withdrawl_acc','1231','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(3,'info_rounding_acc','1231','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(4,'info_credit_note_acc','1232','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(5,'info_gift_voucher_acc','1233','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(6,'info_overages','123','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(7,'info_credit_card_acc','123123','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(8,'info_payment_automatic_account','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(9,'info_hotel_booking_account','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(10,'info_free_acc','1231','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(11,'checkout_default_customer','3','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(12,'checkout_starting_petty_cash','1234','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(13,'checkout_current_cash_on_hand','1234','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(14,'checkout_divergence','1234','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(15,'checkout_gift_voucher','1234','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(16,'credit_manag_default_report_software_id','1','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(17,'credit_manag_email_of_contact','1232','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(18,'credit_manag_id_customer','1232','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(19,'credit_manag_id_invoice','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(20,'credit_manag_id_credit_note','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(21,'init_param_number_of_first_inv','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(22,'init_param_number_of_first_log_of_cash','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(23,'init_param_number_of_first_report_acc','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(24,'init_param_number_of_first_report_inv','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(25,'init_param_number_of_first_report_sales','1230','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(26,'credit_management_id','1','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(27,'credit_management_email_or_idaccount','123','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(28,'credit_management_user','123','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1),(29,'credit_management_password','123','2017-05-17 15:33:11','2018-05-11 08:02:39',NULL,1);
/*!40000 ALTER TABLE `settings_accounting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings_additional`
--

DROP TABLE IF EXISTS `settings_additional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings_additional` (
  `idsettingsadditional` int(11) NOT NULL AUTO_INCREMENT,
  `data_type` varchar(250) NOT NULL,
  `data_value` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idsettingsadditional`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings_additional`
--

LOCK TABLES `settings_additional` WRITE;
/*!40000 ALTER TABLE `settings_additional` DISABLE KEYS */;
INSERT INTO `settings_additional` VALUES (1,'showPosSessionIr','on','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(2,'showPlaceSale','on','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(3,'showSessionPOSacc','','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(4,'UseStandardEmailSoftware','','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(5,'language','0','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(6,'initialPage','0','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(7,'sendersEmail','','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(8,'alertEmail','','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(9,'password_to_access_admin','test1','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(10,'password_to_access_employee','dfdsa','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(11,'password_to_access_cashRegisterClos','28','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(12,'password_to_access_product','ddd','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(13,'password_to_access_cancelItem','fff','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(14,'password_to_access_cancelPurchase','','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(15,'password_to_access_showDataSale','','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(16,'ftp_of_provides_host','localhost','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(17,'ftp_of_provides_user','test','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(18,'ftp_of_provides_password','ewew','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(19,'ftp_of_provides_layout','0','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1),(20,'ftp_of_provides_includeImg','','2017-05-31 22:08:45','2018-05-11 08:03:28',NULL,1);
/*!40000 ALTER TABLE `settings_additional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings_factoring`
--

DROP TABLE IF EXISTS `settings_factoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings_factoring` (
  `idsettingsfactoring` int(11) NOT NULL AUTO_INCREMENT,
  `company_factoring` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `cod_postal` varchar(100) NOT NULL,
  `invoice_text` text NOT NULL,
  `bank_account` varchar(50) NOT NULL,
  `kid_layout` varchar(30) NOT NULL,
  `client_id` int(11) NOT NULL,
  `email` varchar(80) DEFAULT NULL,
  `add_invoice_text` text,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idsettingsfactoring`),
  KEY `company_factoring` (`company_factoring`),
  CONSTRAINT `fk_company_refactoring` FOREIGN KEY (`company_factoring`) REFERENCES `company_factoring` (`idcompanyfactoring`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings_factoring`
--

LOCK TABLES `settings_factoring` WRITE;
/*!40000 ALTER TABLE `settings_factoring` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings_factoring` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings_general`
--

DROP TABLE IF EXISTS `settings_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings_general` (
  `idsettingsgeneral` int(11) NOT NULL AUTO_INCREMENT,
  `data_type` varchar(250) NOT NULL,
  `data_value` varchar(250) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idsettingsgeneral`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings_general`
--

LOCK TABLES `settings_general` WRITE;
/*!40000 ALTER TABLE `settings_general` DISABLE KEYS */;
INSERT INTO `settings_general` VALUES (1,'inv_default_value','1231244','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(2,'inv_type_tax','3','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(3,'inv_minimum_value_for_tax_free','1234','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(4,'inv_always_collect_tax_on_inv','0','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(5,'oth_impt_only_product_name_acquired','0','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(6,'oth_print_list_of_all_sold','1','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(7,'pur_when_customer_is_change','1','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(8,'pur_send_message_to_customer','1','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(9,'pur_apply_default_discount','1','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1),(10,'pur_automatically_put_in_all_docs','1','2017-05-17 15:33:09','2018-05-11 08:01:55',NULL,1);
/*!40000 ALTER TABLE `settings_general` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shelf`
--

DROP TABLE IF EXISTS `shelf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shelf` (
  `idshelf` int(11) NOT NULL AUTO_INCREMENT,
  `wardrobe_idwardrobe` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `position_top` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `fixed` int(11) NOT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idshelf`),
  KEY `fk_shelf_wardrobe1_idx` (`wardrobe_idwardrobe`),
  CONSTRAINT `fk_shelf_wardrobe1` FOREIGN KEY (`wardrobe_idwardrobe`) REFERENCES `wardrobe` (`idwardrobe`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shelf`
--

LOCK TABLES `shelf` WRITE;
/*!40000 ALTER TABLE `shelf` DISABLE KEYS */;
INSERT INTO `shelf` VALUES (1,1,NULL,10,'2017-05-31 18:27:26',NULL,NULL,1,1),(2,1,NULL,0,'2017-05-31 18:27:48',NULL,NULL,1,1);
/*!40000 ALTER TABLE `shelf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_mtd`
--

DROP TABLE IF EXISTS `shipping_mtd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_mtd` (
  `idshipping_mtd` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_cmp_idshipping_cmp` int(11) NOT NULL,
  `name` varchar(45) NOT NULL COMMENT 'The methods name.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idshipping_mtd`),
  KEY `fk_shipping_mtd_shipping_cmp1_idx` (`shipping_cmp_idshipping_cmp`),
  CONSTRAINT `fk_shipping_mtd_shipping_cmp1` FOREIGN KEY (`shipping_cmp_idshipping_cmp`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_mtd`
--

LOCK TABLES `shipping_mtd` WRITE;
/*!40000 ALTER TABLE `shipping_mtd` DISABLE KEYS */;
INSERT INTO `shipping_mtd` VALUES (1,1,'aaa','2018-04-19 14:09:26','2018-04-19 15:09:19','2018-04-19 15:09:26',3),(2,1,'Teste','2018-08-06 18:05:21',NULL,'2018-08-06 19:05:21',3),(3,1,'Teste 2','2018-08-08 13:15:11','2018-08-08 14:15:08','2018-08-08 14:15:11',3);
/*!40000 ALTER TABLE `shipping_mtd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_place`
--

DROP TABLE IF EXISTS `shipping_place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_place` (
  `idshipping_place` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_mtd_idshipping_mtd` int(11) NOT NULL,
  `zz_state_idzz_state` int(11) DEFAULT NULL COMMENT 'If this field comes NULL, it considers all states in the country. If there is another shipping_place to the same country, the one NOT NULL will be predominate.',
  `zz_country_idzz_country` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idshipping_place`),
  KEY `fk_shipping_place_shipping_mtd1_idx` (`shipping_mtd_idshipping_mtd`),
  KEY `fk_shipping_place_zz_state1_idx` (`zz_state_idzz_state`),
  KEY `fk_shipping_place_zz_country1_idx` (`zz_country_idzz_country`),
  CONSTRAINT `fk_shipping_place_shipping_mtd1` FOREIGN KEY (`shipping_mtd_idshipping_mtd`) REFERENCES `shipping_mtd` (`idshipping_mtd`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_place_zz_country1` FOREIGN KEY (`zz_country_idzz_country`) REFERENCES `zz_country` (`idzz_country`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_shipping_place_zz_state1` FOREIGN KEY (`zz_state_idzz_state`) REFERENCES `zz_state` (`idzz_state`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_place`
--

LOCK TABLES `shipping_place` WRITE;
/*!40000 ALTER TABLE `shipping_place` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping_place` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipping_settings`
--

DROP TABLE IF EXISTS `shipping_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipping_settings` (
  `idshipping_settings` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_place_idshipping_place` int(11) NOT NULL,
  `weight_limit` float DEFAULT NULL,
  `value` decimal(11,2) NOT NULL DEFAULT '0.00',
  `time_taken` int(11) DEFAULT NULL COMMENT 'Time taken in days.',
  `working_days` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether time_taken considers only working days (1) or it considers also weekend and holidays (0).',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idshipping_settings`),
  KEY `fk_shipping_settings_shipping_place1_idx` (`shipping_place_idshipping_place`),
  CONSTRAINT `fk_shipping_settings_shipping_place1` FOREIGN KEY (`shipping_place_idshipping_place`) REFERENCES `shipping_place` (`idshipping_place`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipping_settings`
--

LOCK TABLES `shipping_settings` WRITE;
/*!40000 ALTER TABLE `shipping_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipping_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `size` (
  `idsize` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idsize`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size`
--

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;
INSERT INTO `size` VALUES (1,'Test2','2017-05-18 18:45:44','2017-05-18 18:45:49','2017-05-18 18:45:52',3),(2,'G','2017-06-01 14:12:31',NULL,NULL,1),(3,'P','2017-06-01 14:12:38','2018-04-19 14:36:45',NULL,1),(4,'M','2017-06-01 14:12:42',NULL,NULL,1),(5,'S','2018-04-19 14:36:56',NULL,'2018-04-19 14:37:46',3);
/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size_color`
--

DROP TABLE IF EXISTS `size_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `size_color` (
  `idsize_color` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL,
  `color_idcolor` int(11) NOT NULL,
  `size_idsize` int(11) NOT NULL,
  `codEan` varchar(300) DEFAULT NULL,
  `codSupplier` int(11) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idsize_color`),
  KEY `fk_size_color_color1_idx` (`color_idcolor`),
  KEY `fk_size_color_size1_idx` (`size_idsize`),
  KEY `fk_size_color_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_size_color_color1` FOREIGN KEY (`color_idcolor`) REFERENCES `color` (`idcolor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_size_color_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_size_color_size1` FOREIGN KEY (`size_idsize`) REFERENCES `size` (`idsize`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size_color`
--

LOCK TABLES `size_color` WRITE;
/*!40000 ALTER TABLE `size_color` DISABLE KEYS */;
INSERT INTO `size_color` VALUES (1,28,3,2,'9988',9,'2018-07-24 18:01:52',NULL,NULL,1);
/*!40000 ALTER TABLE `size_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size_color_section`
--

DROP TABLE IF EXISTS `size_color_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `size_color_section` (
  `idsize_color_section` int(11) NOT NULL AUTO_INCREMENT,
  `size_color_idsize_color` int(11) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `section_idsection` int(11) NOT NULL,
  PRIMARY KEY (`idsize_color_section`),
  KEY `fk_size_color_warehouse_size_color1_idx` (`size_color_idsize_color`),
  KEY `fk_size_color_section_section1_idx` (`section_idsection`),
  CONSTRAINT `fk_size_color_section_section1` FOREIGN KEY (`section_idsection`) REFERENCES `section` (`idsection`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_size_color_warehouse_size_color1` FOREIGN KEY (`size_color_idsize_color`) REFERENCES `size_color` (`idsize_color`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size_color_section`
--

LOCK TABLES `size_color_section` WRITE;
/*!40000 ALTER TABLE `size_color_section` DISABLE KEYS */;
/*!40000 ALTER TABLE `size_color_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_platform`
--

DROP TABLE IF EXISTS `sms_platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_platform` (
  `idsms_platform` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idsms_platform`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_platform`
--

LOCK TABLES `sms_platform` WRITE;
/*!40000 ALTER TABLE `sms_platform` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_accounting`
--

DROP TABLE IF EXISTS `software_accounting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `software_accounting` (
  `idsoftwareaccounting` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idsoftwareaccounting`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_accounting`
--

LOCK TABLES `software_accounting` WRITE;
/*!40000 ALTER TABLE `software_accounting` DISABLE KEYS */;
INSERT INTO `software_accounting` VALUES (1,'None','2017-05-19 13:43:55',NULL,NULL,1),(2,'24SevenOffice','2017-05-19 13:43:55',NULL,NULL,1),(3,'Agresso','2017-05-19 13:43:55',NULL,NULL,1),(4,'Agro Okonomi','2017-05-19 13:43:55',NULL,NULL,1),(5,'Ajour','2017-05-19 13:43:55',NULL,NULL,1),(6,'Akelius','2017-05-19 13:43:55',NULL,NULL,1),(7,'Axapta','2017-05-19 13:43:55',NULL,NULL,1),(8,'BAS Okonomisystem','2017-05-19 13:43:55',NULL,NULL,1),(9,'DI Systemer','2017-05-19 13:43:55',NULL,NULL,1),(10,'Duett Regnskap','2017-05-19 13:43:55',NULL,NULL,1),(11,'economic','2017-05-19 13:43:55',NULL,NULL,1),(12,'Formula Regnskap','2017-05-19 13:43:55',NULL,NULL,1),(13,'Info Easy Regnskap','2017-05-19 13:43:55',NULL,NULL,1),(14,'Mamut','2017-05-19 13:43:55',NULL,NULL,1),(15,'Maritech Regnskap','2017-05-19 13:43:55',NULL,NULL,1),(16,'Matrix Okonomi','2017-05-19 13:43:55',NULL,NULL,1),(17,'Microsoft Dyn. 4.0 (n4b)','2017-05-19 13:43:55',NULL,NULL,1),(18,'PCK','2017-05-19 13:43:55',NULL,NULL,1),(19,'PowerOfficeGo','2017-05-19 13:43:55',NULL,NULL,1),(20,'Procountor','2017-05-19 13:43:55',NULL,NULL,1),(21,'Sage50','2017-05-19 13:43:55',NULL,NULL,1),(22,'Scenario','2017-05-19 13:43:55',NULL,NULL,1),(23,'Tripletex (Mamut)','2017-05-19 13:43:55',NULL,NULL,1),(24,'Uni Micro','2017-05-19 13:43:55',NULL,NULL,1),(25,'VB Special (R2,R3,R1)','2017-05-19 13:43:55',NULL,NULL,1),(26,'Visma Avendo','2017-05-19 13:43:55',NULL,NULL,1),(27,'Visma Business','2017-05-19 13:43:55',NULL,NULL,1),(28,'Visma Contracting','2017-05-19 13:43:55',NULL,NULL,1),(29,'Visma Enterprise Okonomi','2017-05-19 13:43:55',NULL,NULL,1),(30,'Visma Global','2017-05-19 13:43:55',NULL,NULL,1),(31,'Visma Global(yyyy.MM.dd)','2017-05-19 13:43:55',NULL,NULL,1),(32,'Visma MultiSoft','2017-05-19 13:43:55',NULL,NULL,1),(33,'Visma Rubicon','2017-05-19 13:43:55',NULL,NULL,1),(34,'XLEDGER (AR10)','2017-05-19 13:43:55',NULL,NULL,1),(35,'XLEDGER (GL02)','2017-05-19 13:43:55',NULL,NULL,1),(36,'Zirius Regnskap','2017-05-19 13:43:55',NULL,NULL,1);
/*!40000 ALTER TABLE `software_accounting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subproduct`
--

DROP TABLE IF EXISTS `subproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subproduct` (
  `idsubproduct` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL COMMENT 'The ID of the main product.',
  `qty_allowed` int(11) NOT NULL DEFAULT '1',
  `qty_min` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0' COMMENT 'A product can have more than one list of subproducts, so this column determines which one will appear first.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idsubproduct`),
  KEY `fk_subproduct_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_subproduct_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subproduct`
--

LOCK TABLES `subproduct` WRITE;
/*!40000 ALTER TABLE `subproduct` DISABLE KEYS */;
/*!40000 ALTER TABLE `subproduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subproduct_line`
--

DROP TABLE IF EXISTS `subproduct_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subproduct_line` (
  `idsubproduct_line` int(11) NOT NULL AUTO_INCREMENT,
  `product_idproduct` int(11) NOT NULL COMMENT 'The ID of the SUBPRODUCT.',
  `load` int(11) NOT NULL DEFAULT '1' COMMENT 'A subproduct may be more expensive than other, then it can count as more than one.',
  `subproduct_linecol` varchar(45) DEFAULT NULL,
  `subproduct_idsubproduct` int(11) NOT NULL,
  PRIMARY KEY (`idsubproduct_line`),
  KEY `fk_subproduct_line_subproduct1_idx` (`subproduct_idsubproduct`),
  KEY `fk_subproduct_line_product1_idx` (`product_idproduct`),
  CONSTRAINT `fk_subproduct_line_product1` FOREIGN KEY (`product_idproduct`) REFERENCES `product` (`idproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_subproduct_line_subproduct1` FOREIGN KEY (`subproduct_idsubproduct`) REFERENCES `subproduct` (`idsubproduct`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subproduct_line`
--

LOCK TABLES `subproduct_line` WRITE;
/*!40000 ALTER TABLE `subproduct_line` DISABLE KEYS */;
/*!40000 ALTER TABLE `subproduct_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier` (
  `idsupplier` int(11) NOT NULL AUTO_INCREMENT,
  `company_idcompany` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idsupplier`),
  KEY `fk_supplier_company1_idx` (`company_idcompany`),
  CONSTRAINT `fk_supplier_company1` FOREIGN KEY (`company_idcompany`) REFERENCES `company` (`idcompany`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier`
--

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_api`
--

DROP TABLE IF EXISTS `supplier_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_api` (
  `idsupplierapi` int(11) NOT NULL AUTO_INCREMENT,
  `idsupplier` int(11) NOT NULL,
  `idtypeapi` int(11) NOT NULL,
  `idformatapi` int(11) NOT NULL,
  `address` text NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` text NOT NULL,
  `date_create` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_update` datetime DEFAULT NULL,
  `date_delete` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idsupplierapi`),
  KEY `idsupplier_indx` (`idsupplier`),
  KEY `idsuppliertypeapi_indx` (`idtypeapi`),
  KEY `idformatapi_indx` (`idformatapi`),
  CONSTRAINT `fk_supplier_format_api` FOREIGN KEY (`idformatapi`) REFERENCES `supplier_format_api` (`idsupplierformatapi`),
  CONSTRAINT `fk_supplier_id` FOREIGN KEY (`idsupplier`) REFERENCES `supplier` (`idsupplier`),
  CONSTRAINT `fk_supplier_type_api` FOREIGN KEY (`idtypeapi`) REFERENCES `supplier_type_api` (`idsuppliertypeapi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_api`
--

LOCK TABLES `supplier_api` WRITE;
/*!40000 ALTER TABLE `supplier_api` DISABLE KEYS */;
/*!40000 ALTER TABLE `supplier_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_format_api`
--

DROP TABLE IF EXISTS `supplier_format_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_format_api` (
  `idsupplierformatapi` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idsupplierformatapi`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_format_api`
--

LOCK TABLES `supplier_format_api` WRITE;
/*!40000 ALTER TABLE `supplier_format_api` DISABLE KEYS */;
INSERT INTO `supplier_format_api` VALUES (1,'Station',1),(2,'EFO/NELFO Purchase Order',1),(3,'Malorama',1),(4,'RNB',1),(5,'Forlagssentralen',1),(6,'SentralDistribusjon',1),(7,'Yes vi leker',1);
/*!40000 ALTER TABLE `supplier_format_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_type_api`
--

DROP TABLE IF EXISTS `supplier_type_api`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supplier_type_api` (
  `idsuppliertypeapi` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idsuppliertypeapi`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_type_api`
--

LOCK TABLES `supplier_type_api` WRITE;
/*!40000 ALTER TABLE `supplier_type_api` DISABLE KEYS */;
INSERT INTO `supplier_type_api` VALUES (1,'Send Purchase Order',1),(2,'Get Purchase Order',1),(3,'Get Packing list',1);
/*!40000 ALTER TABLE `supplier_type_api` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `table_column`
--

DROP TABLE IF EXISTS `table_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `table_column` (
  `idtable_column` int(11) NOT NULL AUTO_INCREMENT,
  `pos_element_idpos_element` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `label` varchar(45) NOT NULL COMMENT 'The name that appear in the table.',
  `order` int(11) DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n',
  PRIMARY KEY (`idtable_column`),
  KEY `fk_table_column_pos_element1_idx` (`pos_element_idpos_element`),
  CONSTRAINT `fk_table_column_pos_element1` FOREIGN KEY (`pos_element_idpos_element`) REFERENCES `pos_element` (`idpos_element`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `table_column`
--

LOCK TABLES `table_column` WRITE;
/*!40000 ALTER TABLE `table_column` DISABLE KEYS */;
/*!40000 ALTER TABLE `table_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tables`
--

DROP TABLE IF EXISTS `tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tables` (
  `idtables` int(11) NOT NULL AUTO_INCREMENT,
  `structure` longtext NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`idtables`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tables`
--

LOCK TABLES `tables` WRITE;
/*!40000 ALTER TABLE `tables` DISABLE KEYS */;
/*!40000 ALTER TABLE `tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tax`
--

DROP TABLE IF EXISTS `tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tax` (
  `idtax` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `percent` float NOT NULL,
  `mva_account` int(11) NOT NULL,
  `account_relapse` int(11) DEFAULT NULL,
  `external_tax_code` int(11) DEFAULT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idtax`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tax`
--

LOCK TABLES `tax` WRITE;
/*!40000 ALTER TABLE `tax` DISABLE KEYS */;
INSERT INTO `tax` VALUES (1,'Test ',123,1,1,1,NULL,'2017-05-19 14:21:45','2017-05-19 14:23:51',3),(2,'Test2',12.32,1,1,1,NULL,'2017-05-19 14:26:25','2017-05-19 14:26:29',3),(3,'Test',1.23,123,123,123,NULL,NULL,NULL,1),(4,'Høy sats',25,2700,0,3,'2017-05-19 13:44:59',NULL,NULL,1),(5,'Middels sats',15,2701,0,13,'2017-05-19 13:44:59',NULL,NULL,1),(6,'Lav sats',10,2702,0,NULL,'2017-05-19 13:44:59',NULL,NULL,1),(7,'aaa',11,1,1,1,NULL,'2018-04-19 15:12:54','2018-04-19 15:13:01',3),(8,'Test 50%',50,2750,22,8,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_ticket_tmpt`
--

DROP TABLE IF EXISTS `temp_ticket_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_ticket_tmpt` (
  `idtemp_ticket_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `info` longtext,
  PRIMARY KEY (`idtemp_ticket_tmpt`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_ticket_tmpt`
--

LOCK TABLES `temp_ticket_tmpt` WRITE;
/*!40000 ALTER TABLE `temp_ticket_tmpt` DISABLE KEYS */;
INSERT INTO `temp_ticket_tmpt` VALUES (1,'Demo','2017-05-17 11:30:14',NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `temp_ticket_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_print`
--

DROP TABLE IF EXISTS `ticket_print`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_print` (
  `idticket_print` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `text_print` longtext,
  PRIMARY KEY (`idticket_print`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_print`
--

LOCK TABLES `ticket_print` WRITE;
/*!40000 ALTER TABLE `ticket_print` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_print` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_tmpt`
--

DROP TABLE IF EXISTS `ticket_tmpt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_tmpt` (
  `idticket_tmpt` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `info` longtext,
  `print_two_lines` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print two lines by item, so more information can be shown.',
  `print_promo_and_normal_price` tinyint(1) NOT NULL DEFAULT '0',
  `print_store_address` tinyint(1) NOT NULL DEFAULT '0',
  `print_product_tax` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Print the tax in each product',
  `print_product_details` tinyint(1) NOT NULL DEFAULT '0',
  `print_product_number` tinyint(1) NOT NULL DEFAULT '0',
  `print_tip` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idticket_tmpt`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_tmpt`
--

LOCK TABLES `ticket_tmpt` WRITE;
/*!40000 ALTER TABLE `ticket_tmpt` DISABLE KEYS */;
INSERT INTO `ticket_tmpt` VALUES (1,'Demo','2017-05-17 11:30:14',NULL,NULL,1,NULL,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `ticket_tmpt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmpt_footer`
--

DROP TABLE IF EXISTS `tmpt_footer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmpt_footer` (
  `idtmpt_footer` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idtmpt_footer`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmpt_footer`
--

LOCK TABLES `tmpt_footer` WRITE;
/*!40000 ALTER TABLE `tmpt_footer` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmpt_footer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmpt_header`
--

DROP TABLE IF EXISTS `tmpt_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmpt_header` (
  `idtmpt_header` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idtmpt_header`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmpt_header`
--

LOCK TABLES `tmpt_header` WRITE;
/*!40000 ALTER TABLE `tmpt_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `tmpt_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tribute`
--

DROP TABLE IF EXISTS `tribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tribute` (
  `idtribute` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `pay_vat` tinyint(4) DEFAULT NULL,
  `tax` int(11) NOT NULL,
  `treasury` int(11) NOT NULL,
  `treasury_without_vat` int(11) NOT NULL,
  `cod_additional` int(11) NOT NULL,
  `vat_alternative` int(11) DEFAULT NULL,
  `treasury_alternative` int(11) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`idtribute`),
  KEY `vat_alternative` (`vat_alternative`),
  KEY `tax` (`tax`),
  CONSTRAINT `fk_tax_1` FOREIGN KEY (`tax`) REFERENCES `tax` (`idtax`),
  CONSTRAINT `fk_tax_2` FOREIGN KEY (`vat_alternative`) REFERENCES `tax` (`idtax`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tribute`
--

LOCK TABLES `tribute` WRITE;
/*!40000 ALTER TABLE `tribute` DISABLE KEYS */;
INSERT INTO `tribute` VALUES (1,'Test',1,3,123,123,123,3,123,NULL,NULL,NULL,1),(2,'aaa',0,3,11,55,51,3,67,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `tribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit` (
  `idunit` int(11) NOT NULL AUTO_INCREMENT,
  `unit_idunit` int(11) DEFAULT NULL COMMENT 'A unit can be a fraction or multiple of another one.',
  `name` varchar(45) NOT NULL,
  `divisible` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Whether the unit can be fractioned or not.',
  `ratio` float DEFAULT NULL COMMENT 'The ratio between this unit and the other one indicated by unit_idunit',
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idunit`),
  KEY `fk_unit_unit1_idx` (`unit_idunit`),
  CONSTRAINT `fk_unit_unit1` FOREIGN KEY (`unit_idunit`) REFERENCES `unit` (`idunit`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit`
--

LOCK TABLES `unit` WRITE;
/*!40000 ALTER TABLE `unit` DISABLE KEYS */;
INSERT INTO `unit` VALUES (5,NULL,'Und',0,1,NULL,NULL,NULL,1),(6,5,'test',0,33,NULL,NULL,NULL,1),(7,5,'Teste',1,1,NULL,'2018-08-07 14:39:15','2018-08-07 14:39:18',3),(8,5,'Teste',1,3,NULL,'2018-08-07 14:39:43','2018-08-07 14:40:06',3);
/*!40000 ALTER TABLE `unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `idusers` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Authentication data from users',
  `users_type_idusers_type` int(11) NOT NULL,
  `job_idjob` int(11) NOT NULL,
  `person_idperson` int(11) NOT NULL,
  `login` varchar(200) NOT NULL COMMENT 'A name the user uses to access.',
  `password` varchar(200) NOT NULL COMMENT 'A password the user uses to access.',
  `system_version` int(11) DEFAULT NULL COMMENT 'The version of the system used by this user. To the developers, the version is count sequentially.',
  `multiple_connections_limit` int(11) DEFAULT '1' COMMENT 'The maximum allowed number of simultaneous connections of this user.',
  `date_creation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idusers`),
  UNIQUE KEY `login_UNIQUE` (`login`),
  KEY `fk_user_user_type1_idx` (`users_type_idusers_type`),
  KEY `fk_user_job1_idx` (`job_idjob`),
  KEY `fk_user_person1_idx` (`person_idperson`),
  CONSTRAINT `fk_user_job1` FOREIGN KEY (`job_idjob`) REFERENCES `job` (`idjob`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_person1` FOREIGN KEY (`person_idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_user_type1` FOREIGN KEY (`users_type_idusers_type`) REFERENCES `users_type` (`idusers_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,1,1,'Carlos Henrique','21232f297a57a5a743894a0e4a801fc3',NULL,1,'2018-09-29 16:47:43',NULL,NULL,1),(2,1,1,4,'Alice tiburcio','21232f297a57a5a743894a0e4a801fc3',NULL,NULL,'2018-09-29 16:47:48',NULL,NULL,1),(3,1,1,5,'Einarm','123kasse',NULL,NULL,'2018-09-29 16:46:39',NULL,'2018-09-29 16:44:23',0),(4,1,1,6,'wandeson.jpaiva@gmail.com','1q2w3e4r',NULL,NULL,'2018-09-29 16:46:42',NULL,'2018-09-29 16:44:26',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_permission`
--

DROP TABLE IF EXISTS `users_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_permission` (
  `idusers_permission` int(11) NOT NULL AUTO_INCREMENT,
  `users_idusers` int(11) NOT NULL,
  `permission_idpermission` int(11) NOT NULL,
  `active` int(11) DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idusers_permission`),
  KEY `fk_user_permission_user1_idx` (`users_idusers`),
  KEY `fk_user_permission_permission2_idx` (`permission_idpermission`),
  CONSTRAINT `fk_user_permission_permission2` FOREIGN KEY (`permission_idpermission`) REFERENCES `permission` (`idpermission`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_permission_user1` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=358 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_permission`
--

LOCK TABLES `users_permission` WRITE;
/*!40000 ALTER TABLE `users_permission` DISABLE KEYS */;
INSERT INTO `users_permission` VALUES (1,1,1,1),(2,1,2,1),(3,1,3,1),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,7,1),(8,1,8,1),(9,1,9,1),(10,1,10,1),(11,1,11,1),(12,1,12,1),(13,1,13,1),(14,1,14,1),(15,1,15,1),(16,1,16,1),(17,1,17,1),(18,1,18,1),(19,1,19,1),(20,1,20,1),(21,1,21,1),(22,1,22,1),(23,1,23,1),(24,1,24,1),(25,1,25,1),(26,1,26,1),(27,1,27,1),(28,1,28,1),(29,1,29,1),(30,1,30,1),(31,1,31,1),(32,1,32,1),(33,1,33,1),(34,1,34,1),(35,1,35,1),(36,1,36,1),(37,1,37,1),(38,1,38,1),(39,1,39,1),(40,1,40,1),(41,1,41,1),(42,1,42,1),(43,1,43,1),(44,1,44,1),(45,1,45,1),(46,1,46,1),(47,1,47,1),(48,1,48,1),(49,1,49,1),(50,1,50,1),(51,1,51,1),(52,1,52,1),(53,1,53,1),(54,1,54,1),(55,1,55,1),(56,1,56,1),(57,1,57,1),(58,1,58,1),(59,1,59,1),(60,1,60,1),(61,1,61,1),(62,1,62,1),(63,1,63,1),(64,1,64,1),(65,1,65,1),(66,1,66,1),(67,1,67,1),(68,1,68,1),(69,1,69,1),(70,1,70,1),(71,1,71,1),(72,1,72,1),(73,1,73,1),(74,1,74,1),(75,1,75,1),(76,1,76,1),(77,1,77,1),(78,1,78,1),(79,1,79,1),(80,1,80,1),(81,1,81,1),(82,1,82,1),(83,1,83,1),(84,1,84,1),(85,1,85,1),(86,1,86,1),(87,1,87,1),(88,1,88,1),(89,1,89,1),(90,1,90,1),(91,1,91,1),(92,1,92,1),(93,1,93,1),(94,1,94,1),(95,1,95,1),(96,1,96,1),(97,1,97,1),(98,1,98,1),(99,1,99,1),(100,1,100,1),(101,1,101,1),(102,1,102,1),(103,1,103,1),(104,1,104,1),(105,1,105,1),(106,1,106,1),(107,1,107,1),(108,1,108,1),(109,1,109,1),(110,1,110,1),(111,1,111,1),(112,1,112,1),(113,1,113,1),(114,1,114,1),(115,1,115,1),(116,1,116,1),(117,1,117,1),(118,1,118,1),(119,1,119,1),(120,1,120,1),(121,1,121,1),(122,1,122,1),(123,1,123,1),(124,1,124,1),(125,1,125,1),(126,1,126,1),(127,1,127,1),(128,1,128,1),(129,1,129,1),(130,1,130,1),(131,1,131,1),(132,1,132,1),(133,1,133,1),(134,1,134,1),(135,1,135,1),(136,1,136,1),(137,1,137,1),(138,1,138,1),(139,1,139,1),(140,1,140,1),(141,1,141,1),(142,1,142,1),(143,1,143,1),(144,1,144,1),(145,1,145,1),(146,1,146,1),(147,1,147,1),(148,1,148,1),(149,1,149,1),(150,1,150,1),(151,1,151,1),(152,1,152,1),(153,1,153,1),(154,1,154,1),(155,1,155,1),(156,1,156,1),(157,1,157,1),(158,1,158,1),(159,1,159,1),(160,1,160,1),(161,1,161,1),(162,1,162,1),(163,1,163,1),(164,1,164,1),(165,1,165,1),(166,1,166,1),(167,1,167,1),(168,1,168,1),(169,1,169,1),(170,1,170,1),(171,1,171,1),(172,1,172,1),(173,1,173,1),(174,1,174,1),(175,1,175,1),(176,1,176,1),(177,1,177,1),(178,1,178,1),(179,1,179,1),(180,1,180,1),(181,1,181,1),(182,1,182,1),(183,1,183,1),(184,1,184,1),(185,1,185,1),(186,1,186,1),(187,1,187,1),(188,1,188,1),(189,1,189,1),(190,1,190,1),(191,1,191,1),(192,1,192,1),(193,1,193,1),(194,1,194,1),(195,1,195,1),(196,1,196,1),(197,1,197,1),(198,1,198,1),(199,1,199,1),(200,1,200,1),(201,1,201,1),(202,1,202,1),(203,1,203,1),(204,1,204,1),(205,1,205,1),(206,1,206,1),(207,1,207,1),(208,1,208,1),(209,1,209,1),(210,1,210,1),(211,1,211,1),(212,1,212,1),(213,1,213,1),(214,1,214,1),(215,1,215,1),(216,1,216,1),(217,1,217,1),(218,1,218,1),(219,1,219,1),(220,1,220,1),(221,1,221,1),(222,1,222,1),(223,1,223,1),(224,1,224,1),(225,1,225,1),(226,1,226,1),(227,1,227,1),(228,1,228,1),(229,1,229,1),(230,1,230,1),(231,1,231,1),(232,1,232,1),(233,1,233,1),(234,1,234,1),(235,1,235,1),(236,1,236,1),(237,1,237,1),(238,1,238,1),(239,1,239,1),(240,1,240,1),(241,1,241,1),(242,1,242,1),(243,1,243,1),(244,1,244,1),(245,1,245,1),(246,1,246,1),(247,1,247,1),(248,1,248,1),(249,1,249,1),(250,1,250,1),(251,1,251,1),(252,1,252,1),(253,1,253,1),(254,1,254,1),(255,1,255,1),(256,1,256,1),(257,1,257,1),(258,1,258,1),(259,1,259,1),(260,1,260,1),(261,1,261,1),(262,1,262,1),(263,1,263,1),(264,1,264,1),(265,1,265,1),(266,1,266,1),(267,1,267,1),(268,1,268,1),(269,1,269,1),(270,1,270,1),(271,1,271,1),(272,1,272,1),(273,1,273,1),(274,1,274,1),(275,1,275,1),(276,1,276,1),(277,1,277,1),(278,1,278,1),(279,1,279,1),(280,1,280,1),(281,1,281,1),(282,1,282,1),(283,1,283,1),(284,1,284,1),(285,1,285,1),(286,1,286,1),(287,1,287,1),(288,1,288,1),(289,1,289,1),(290,1,290,1),(291,1,291,1),(292,1,292,1),(293,1,293,1),(294,1,294,1),(295,1,295,1),(296,1,296,1),(297,1,297,1),(298,1,298,1),(299,1,299,1),(300,1,300,1),(301,1,301,1),(302,1,302,1),(303,1,303,1),(304,1,304,1),(305,1,305,1),(306,1,306,1),(307,1,307,1),(308,1,308,1),(309,1,309,1),(310,1,310,1),(311,1,311,1),(312,1,312,1),(313,1,313,1),(314,1,314,1),(315,1,315,1),(316,1,316,1),(317,1,317,1),(318,1,318,1),(319,1,319,1),(320,1,320,1),(321,1,321,1),(322,1,322,1),(323,1,323,1),(324,1,324,1),(325,1,325,1),(326,1,326,1),(327,1,327,1),(328,1,328,1),(329,1,329,1),(330,1,330,1),(331,1,331,1),(332,1,332,1),(333,1,333,1),(334,1,334,1),(335,1,335,1),(336,1,336,1),(337,1,337,1),(338,1,338,1),(339,1,339,1),(340,1,340,1),(341,1,341,1),(342,1,342,1),(343,1,343,1),(344,1,344,1),(345,1,345,1),(346,1,346,1),(347,1,347,1),(348,1,348,1),(349,1,349,1),(350,1,350,1),(351,1,351,1),(352,1,352,1),(353,1,353,1),(356,1,354,1),(357,1,355,1);
/*!40000 ALTER TABLE `users_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_pos`
--

DROP TABLE IF EXISTS `users_pos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_pos` (
  `idusers_pos` int(11) NOT NULL AUTO_INCREMENT,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  `users_idusers` int(11) NOT NULL,
  `pos_idpos` int(11) NOT NULL,
  PRIMARY KEY (`idusers_pos`),
  KEY `fk_user_pos_user1_idx` (`users_idusers`),
  KEY `fk_user_pos_pos1_idx` (`pos_idpos`),
  CONSTRAINT `fk_user_pos_pos1` FOREIGN KEY (`pos_idpos`) REFERENCES `pos` (`idpos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_pos_user1` FOREIGN KEY (`users_idusers`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_pos`
--

LOCK TABLES `users_pos` WRITE;
/*!40000 ALTER TABLE `users_pos` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_pos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_type`
--

DROP TABLE IF EXISTS `users_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_type` (
  `idusers_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL COMMENT 'Name of the user types. Ex: Admin, Customer, Employee, Manager',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idusers_type`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_type`
--

LOCK TABLES `users_type` WRITE;
/*!40000 ALTER TABLE `users_type` DISABLE KEYS */;
INSERT INTO `users_type` VALUES (1,'admin','2017-05-17 11:30:14',NULL,NULL,1),(2,'Test2','2017-05-18 14:03:21','2017-05-18 14:03:25','2017-05-18 14:03:32',3),(3,'Test4','2017-05-18 14:03:36','2018-04-19 14:25:30',NULL,1),(4,'test3','2018-04-19 14:24:17','2018-04-19 14:24:29','2018-04-19 14:24:37',3);
/*!40000 ALTER TABLE `users_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle`
--

DROP TABLE IF EXISTS `vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle` (
  `idvehicle` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL COMMENT 'What identifies the vehicle.',
  `plate` varchar(45) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idvehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle`
--

LOCK TABLES `vehicle` WRITE;
/*!40000 ALTER TABLE `vehicle` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_imei`
--

DROP TABLE IF EXISTS `vehicle_imei`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle_imei` (
  `idvehicle_imei` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idvehicle_imei`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_imei`
--

LOCK TABLES `vehicle_imei` WRITE;
/*!40000 ALTER TABLE `vehicle_imei` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_imei` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicle_imei_order`
--

DROP TABLE IF EXISTS `vehicle_imei_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehicle_imei_order` (
  `idvehicle_imei_order` int(11) NOT NULL AUTO_INCREMENT,
  `order_idorder` int(11) NOT NULL,
  `vehicle_imei_idvehicle_imei` int(11) NOT NULL,
  `vehicle_idvehicle` int(11) NOT NULL,
  PRIMARY KEY (`idvehicle_imei_order`),
  KEY `fk_vehicle_imei_order_order1_idx` (`order_idorder`),
  KEY `fk_vehicle_imei_order_vehicle_imei1_idx` (`vehicle_imei_idvehicle_imei`),
  KEY `fk_vehicle_imei_order_vehicle1_idx` (`vehicle_idvehicle`),
  CONSTRAINT `fk_vehicle_imei_order_order1` FOREIGN KEY (`order_idorder`) REFERENCES `order` (`idorder`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vehicle_imei_order_vehicle1` FOREIGN KEY (`vehicle_idvehicle`) REFERENCES `vehicle` (`idvehicle`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vehicle_imei_order_vehicle_imei1` FOREIGN KEY (`vehicle_imei_idvehicle_imei`) REFERENCES `vehicle_imei` (`idvehicle_imei`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicle_imei_order`
--

LOCK TABLES `vehicle_imei_order` WRITE;
/*!40000 ALTER TABLE `vehicle_imei_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehicle_imei_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wardrobe`
--

DROP TABLE IF EXISTS `wardrobe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wardrobe` (
  `idwardrobe` int(11) NOT NULL AUTO_INCREMENT,
  `hall_idhall` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `position_top` int(11) NOT NULL,
  `position_left` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idwardrobe`),
  KEY `fk_wardrobe_hall1_idx` (`hall_idhall`),
  CONSTRAINT `fk_wardrobe_hall1` FOREIGN KEY (`hall_idhall`) REFERENCES `hall` (`idhall`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wardrobe`
--

LOCK TABLES `wardrobe` WRITE;
/*!40000 ALTER TABLE `wardrobe` DISABLE KEYS */;
INSERT INTO `wardrobe` VALUES (1,1,'',104,80,95,127,'2017-05-31 18:27:09',NULL,NULL,1);
/*!40000 ALTER TABLE `wardrobe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warehouse`
--

DROP TABLE IF EXISTS `warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warehouse` (
  `idwarehouse` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idwarehouse`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warehouse`
--

LOCK TABLES `warehouse` WRITE;
/*!40000 ALTER TABLE `warehouse` DISABLE KEYS */;
INSERT INTO `warehouse` VALUES (1,'Test','2017-05-17 15:46:47',NULL,NULL,1),(2,'Test','2017-05-18 18:42:09','2017-05-18 18:42:14','2017-05-18 18:42:17',3),(3,'Test','2017-06-07 12:49:40',NULL,'2017-06-07 12:49:46',3),(4,'test2','2018-04-19 14:34:58','2018-04-19 14:35:07','2018-04-19 14:35:53',3),(5,'Home','2018-05-10 15:52:18',NULL,NULL,1);
/*!40000 ALTER TABLE `warehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warnings_in_case_of_delay`
--

DROP TABLE IF EXISTS `warnings_in_case_of_delay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warnings_in_case_of_delay` (
  `id_warnings_in_case_of_delay` int(11) NOT NULL AUTO_INCREMENT,
  `send_after` int(11) NOT NULL,
  `send_copy` int(11) DEFAULT NULL,
  `message_title` varchar(300) NOT NULL,
  `message` varchar(300) NOT NULL,
  `date_create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  PRIMARY KEY (`id_warnings_in_case_of_delay`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warnings_in_case_of_delay`
--

LOCK TABLES `warnings_in_case_of_delay` WRITE;
/*!40000 ALTER TABLE `warnings_in_case_of_delay` DISABLE KEYS */;
INSERT INTO `warnings_in_case_of_delay` VALUES (1,2,NULL,'Test','Test','2017-05-17 15:53:06','2017-05-17 15:53:23','2017-05-17 15:53:27',3),(2,123,NULL,'Test','Test','2017-05-24 13:28:42','2017-05-24 13:29:02',NULL,1);
/*!40000 ALTER TABLE `warnings_in_case_of_delay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `warranty`
--

DROP TABLE IF EXISTS `warranty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warranty` (
  `idwarranty` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '1',
  `duration_unit` int(11) NOT NULL DEFAULT '0' COMMENT 'Which unit is duration:\n\n0- Days\n1- Weeks\n3- Months\n4- Years',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idwarranty`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warranty`
--

LOCK TABLES `warranty` WRITE;
/*!40000 ALTER TABLE `warranty` DISABLE KEYS */;
/*!40000 ALTER TABLE `warranty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `webshop`
--

DROP TABLE IF EXISTS `webshop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `webshop` (
  `idwebshop` int(11) NOT NULL AUTO_INCREMENT,
  `path_webshop` varchar(500) NOT NULL,
  `api_url` varchar(500) NOT NULL,
  `api_user` varchar(45) NOT NULL,
  `api_password` varchar(60) NOT NULL,
  `api_parent_cat` int(11) DEFAULT NULL COMMENT 'The ID of the first category (the category which is parent of all others). This is necessary in Magento.',
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idwebshop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `webshop`
--

LOCK TABLES `webshop` WRITE;
/*!40000 ALTER TABLE `webshop` DISABLE KEYS */;
/*!40000 ALTER TABLE `webshop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `word_original`
--

DROP TABLE IF EXISTS `word_original`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `word_original` (
  `idword_original` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(45) NOT NULL DEFAULT 'en',
  `word` varchar(45) NOT NULL,
  PRIMARY KEY (`idword_original`),
  UNIQUE KEY `word` (`word`)
) ENGINE=InnoDB AUTO_INCREMENT=495 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `word_original`
--

LOCK TABLES `word_original` WRITE;
/*!40000 ALTER TABLE `word_original` DISABLE KEYS */;
INSERT INTO `word_original` VALUES (1,'en','coupon'),(2,'en','to'),(3,'en','set'),(4,'en','orders'),(5,'en','sales'),(6,'en','&'),(7,'en','average'),(8,'en','order'),(9,'en','value'),(10,'en','new'),(11,'en','accounts'),(12,'en','existing'),(13,'en','guests'),(14,'en','items'),(15,'en','total'),(16,'en','per'),(17,'en','discounts'),(18,'en','coupons'),(19,'en','shipping'),(20,'en','free'),(21,'en','taxes'),(22,'en','taxed'),(23,'en','tax'),(24,'en','avg'),(25,'en','breakdown'),(26,'en','by'),(27,'en','account'),(28,'en','guest'),(29,'en','/'),(30,'en','warning'),(31,'en','system'),(32,'en','in'),(33,'en','development'),(34,'en',','),(35,'en','admin'),(36,'en','delivery'),(37,'en','company'),(38,'en','info'),(39,'en','general'),(40,'en','settings'),(41,'en','accounting'),(42,'en','additional'),(43,'en','integrations'),(44,'en','database'),(45,'en','external'),(46,'en','resources'),(47,'en','configurations'),(48,'en','currencies'),(49,'en','name'),(50,'en','fantasy'),(51,'en','informations'),(52,'en','address'),(53,'en','bank'),(54,'en','and'),(55,'en','license'),(56,'en','cancel'),(57,'en','save'),(58,'en','register'),(59,'en','number'),(60,'en','phone'),(61,'en','fax'),(62,'en','default'),(63,'en','message'),(64,'en','street'),(65,'en','district'),(66,'en','city'),(67,'en','code'),(68,'en','zip'),(69,'en','complement'),(70,'en','reference'),(71,'en','visiting'),(72,'en','email'),(73,'en','validad'),(74,'en','of'),(75,'en','companies'),(76,'en','users'),(77,'en','terminals'),(78,'en','mobiles'),(79,'en',':'),(80,'en','check'),(81,'en','modules'),(82,'en','for'),(83,'en','your'),(84,'en','description'),(85,'en','status'),(86,'en','list'),(88,'en','edit'),(89,'en','remove'),(90,'en','close'),(91,'en','coin'),(92,'en','symbol'),(93,'en','exchange'),(94,'en','rate'),(95,'en','unit'),(96,'en','minimum'),(97,'en','treasury'),(98,'en','currency'),(101,'en','dashboard'),(102,'en','setup'),(103,'en','entry'),(104,'en','devices'),(105,'en','reports'),(106,'en','insert'),(107,'en','smallest'),(108,'en','denomination'),(109,'en','update'),(110,'en','device'),(111,'en','select'),(112,'en','layout'),(113,'en','tables'),(114,'en','build'),(115,'en','template'),(116,'en','back'),(117,'en','features'),(118,'en','closing'),(119,'en','parameters'),(120,'en','credit'),(121,'en','notes'),(122,'en','receipts'),(123,'en','pos'),(124,'en','display'),(125,'en','client'),(126,'en','first'),(127,'en','line'),(128,'en','second'),(129,'en','temporary'),(130,'en','receipt'),(131,'en','header'),(132,'en','records'),(133,'en','be'),(134,'en','printed'),(135,'en','at'),(136,'en','the'),(137,'en','end'),(138,'en','box'),(139,'en','log'),(140,'en','purchase'),(141,'en','canceled'),(142,'en','cash'),(143,'en','drawer'),(144,'en','manual'),(145,'en','openings'),(146,'en','('),(147,'en',')'),(148,'en','record'),(149,'en','deleted'),(151,'en','clicking'),(152,'en','button'),(153,'en','created'),(154,'en','a'),(155,'en','already'),(156,'en','you'),(157,'en','can'),(158,'en','add'),(159,'en','columns'),(160,'en','this'),(161,'en','listing'),(162,'en','through'),(163,'en','element'),(164,'en','.'),(165,'en','show'),(166,'en','similar'),(167,'en','shopping'),(168,'en','after'),(169,'en','typing'),(170,'en','on'),(171,'en','remarks'),(172,'en','pre'),(173,'en','payment'),(174,'en','restaurants'),(175,'en','-'),(176,'en','issue'),(177,'en','not'),(178,'en','document'),(179,'en','replace'),(180,'en','logo'),(181,'en','with'),(182,'en','bars'),(183,'en','indicating'),(184,'en','volume'),(185,'en','stores'),(186,'en','all'),(187,'en','during'),(188,'en','search'),(189,'en','exact'),(190,'en','names'),(191,'en','only'),(192,'en','enable'),(193,'en','printing'),(194,'en','request'),(195,'en','confirmation'),(196,'en','product'),(197,'en','values'),(198,'en','when'),(199,'en','changing'),(200,'en','customer'),(201,'en','allow'),(202,'en','user'),(203,'en','terminate'),(204,'en','before'),(205,'en','entering'),(206,'en','amount'),(207,'en','paid'),(208,'en','card'),(209,'en','debit'),(210,'en','case'),(211,'en','pays'),(212,'en','giftcard'),(213,'en','details'),(214,'en','title'),(215,'en','size'),(216,'en','font'),(217,'en','group'),(218,'en','text'),(219,'en','print'),(220,'en','carrier'),(221,'en','identical'),(222,'en','products'),(223,'en','confirm'),(224,'en','price'),(225,'en','same'),(226,'en','place'),(227,'en','last'),(228,'en','two'),(229,'en','digits'),(230,'en','bold'),(231,'en','modify'),(232,'en','prints'),(233,'en','changes'),(234,'en','printer'),(235,'en','rows'),(236,'en','each'),(237,'en','omit'),(238,'en','which'),(239,'en','belongs'),(240,'en','id'),(241,'en','inserted'),(242,'en','cashier'),(243,'en','sold'),(244,'en','day'),(245,'en','quantity'),(246,'en','cell'),(247,'en','ratio'),(248,'en','include'),(249,'en','instead'),(250,'en','use'),(251,'en','discount'),(252,'en','help'),(253,'en','ways'),(254,'en','present'),(255,'en','lines'),(256,'en','item'),(257,'en','allowing'),(258,'en','more'),(259,'en','information'),(260,'en','actual'),(261,'en','promotional'),(262,'en','are'),(263,'en','shop'),(264,'en','detailed'),(265,'en','hide'),(266,'en','comments'),(267,'en','enfered'),(268,'en','always'),(269,'en','article'),(270,'en','is'),(271,'en','given'),(272,'en','without'),(273,'en','shows'),(274,'en','sums'),(275,'en','includes'),(276,'en','tip'),(277,'en','amounts'),(278,'en','templates'),(279,'en','resolution'),(280,'en','preset'),(282,'en','table'),(283,'en','section'),(284,'en','seats'),(285,'en','qty'),(286,'en','delete'),(287,'en','reset'),(288,'en','create'),(289,'en','image'),(290,'en','employee'),(291,'en','sale'),(292,'en','tab'),(293,'en','calculator'),(301,'en','department'),(302,'en','projects'),(303,'en','start'),(304,'en','stop'),(305,'en','project'),(306,'en','date'),(307,'en','permissions'),(308,'en','type'),(309,'en','jobs'),(310,'en','full'),(311,'en','login'),(312,'en','job'),(313,'en','password'),(314,'en','types'),(315,'en','warehouse'),(316,'en','stocks'),(317,'en','physical'),(318,'en','stock'),(319,'en','colors'),(320,'en','color'),(321,'en','country'),(322,'en','suppliers'),(323,'en','apis'),(324,'en','supplier'),(325,'en','from'),(326,'en','web'),(327,'en','site'),(328,'en','language'),(329,'en','org'),(330,'en','nr'),(331,'en','establishment'),(332,'en','mailing'),(333,'en','gln'),(334,'en','contact'),(335,'en','person'),(336,'en','send'),(337,'en','xml'),(338,'en','api'),(339,'en','format'),(340,'en','username'),(341,'en','define'),(342,'en','withdrawal'),(343,'en','sell'),(344,'en','invoice'),(345,'en','statistics'),(346,'en','change'),(347,'en','method'),(348,'en','options'),(349,'en','images'),(350,'en','cards'),(351,'en','access'),(352,'en','label'),(353,'en','footer'),(354,'en','a5'),(355,'en','obs'),(356,'en','separate'),(357,'en','different'),(358,'en','backgrounds'),(359,'en','sms'),(360,'en','sent'),(361,'en','proposals'),(362,'en','proposal'),(363,'en','customers'),(364,'en','payment\'s'),(365,'en','gift'),(367,'en','model'),(368,'en','state'),(369,'en','option'),(370,'en','business'),(371,'en','view'),(372,'en','invoices'),(373,'en','requests'),(374,'en','collection'),(375,'en','letter'),(376,'en','extra'),(377,'en','data'),(378,'en','item\'s'),(379,'en','invoiced'),(380,'en','electronic'),(381,'en','allowed'),(382,'en','redirect'),(383,'en','track'),(384,'en','open'),(385,'en','automatically'),(386,'en','opens'),(387,'en','describe'),(388,'en','d\'not'),(389,'en','statical'),(390,'en','selects'),(395,'en','ticket'),(396,'en','limit'),(398,'en','store'),(399,'en','times'),(400,'en','they'),(401,'en','should'),(402,'en','letters'),(403,'en','sending'),(404,'en','exchequer'),(407,'en','authorization'),(412,'en','genre'),(413,'en','identification'),(414,'en','birthday'),(416,'en','specified'),(417,'en','male'),(418,'en','female'),(419,'en','single'),(420,'en','percentage'),(421,'en','percent'),(425,'en','month'),(426,'en','year'),(427,'en','path'),(428,'en','platform'),(429,'en','initial'),(430,'en','final'),(431,'en','card\'s'),(432,'en','balance'),(433,'en','groups'),(434,'en','rules'),(435,'en','manufacturers'),(436,'en','importation'),(437,'en','series'),(438,'en','package'),(439,'en','postering'),(440,'en','labels'),(441,'en','round'),(442,'en','qrcode'),(443,'en','barcode'),(444,'en','generate'),(445,'en','instaled'),(446,'en','port'),(447,'en','active'),(448,'en','voucher'),(449,'en','output'),(450,'en','control'),(451,'en','out'),(452,'en','expiry'),(453,'en','pricing'),(454,'en','buy'),(455,'en','profit'),(456,'en','margin'),(457,'en','gross'),(458,'en','tributes'),(459,'en','pick'),(460,'en','components'),(461,'en','alternatives'),(462,'en','sizes'),(463,'en','acquisition'),(464,'en','webshop'),(466,'en','cod'),(467,'en','ean'),(468,'en','temporarily'),(469,'en','week'),(470,'en','days'),(471,'en','weekend'),(472,'en','monday'),(473,'en','tuesday'),(474,'en','wednesday'),(475,'en','thursday'),(476,'en','friday'),(477,'en','saturday'),(478,'en','sunday'),(479,'en','main'),(480,'en','one'),(481,'en','?'),(482,'en','recommended'),(483,'en','brute'),(484,'en','liquid'),(485,'en','traded'),(486,'en','takeaway'),(487,'en','travel'),(488,'en','wholesale'),(489,'en','serie'),(491,'en','bought'),(492,'en','match'),(493,'en','returned'),(494,'en','manufacturer');
/*!40000 ALTER TABLE `word_original` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `word_translated`
--

DROP TABLE IF EXISTS `word_translated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `word_translated` (
  `idword_translated` int(11) NOT NULL AUTO_INCREMENT,
  `word_original_idword_original` int(11) NOT NULL,
  `language_idlanguage` int(11) NOT NULL,
  `new_word` varchar(45) NOT NULL,
  PRIMARY KEY (`idword_translated`),
  KEY `fk_word_translated_word_original` (`word_original_idword_original`) USING BTREE,
  KEY `fk_word_translation_language` (`language_idlanguage`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=931 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `word_translated`
--

LOCK TABLES `word_translated` WRITE;
/*!40000 ALTER TABLE `word_translated` DISABLE KEYS */;
INSERT INTO `word_translated` VALUES (1,1,2,'cupom'),(2,1,3,'tarjetas'),(3,2,2,'para'),(4,2,3,'para'),(5,3,2,'setar'),(6,3,3,'cambiar'),(7,4,2,'pedidos'),(8,4,3,'pedidos'),(9,5,2,'vendas'),(10,5,3,'ventas'),(11,6,2,'&'),(12,6,3,'&'),(13,7,2,'média'),(14,7,3,'promedio'),(15,8,2,'pedido'),(16,8,3,' solicitud'),(17,9,2,'valor'),(18,9,3,'valor'),(19,10,2,'novo'),(20,10,3,'nuevo'),(21,11,2,'contas'),(22,11,3,'cuentas'),(23,12,2,'existente'),(24,12,3,'existente'),(25,13,2,'convidados'),(26,13,3,'invitados'),(27,14,2,'itens'),(28,14,3,'items'),(29,15,2,'total'),(30,15,3,'total'),(31,16,2,'por'),(32,16,3,'por'),(33,17,2,'descontos'),(34,17,3,'descuentos'),(35,18,2,'cupons'),(36,18,3,'cupones'),(37,19,2,'envio'),(38,19,3,'envio'),(39,20,2,'gratis'),(40,20,3,'gratis'),(41,21,2,'impostos'),(42,21,3,'impuestos'),(43,22,2,'com imposto'),(44,22,3,'con impuesto'),(45,23,2,'imposto'),(46,23,3,'impuesto'),(47,24,2,'media'),(48,24,3,'promedio'),(49,25,2,'repartidos'),(50,25,3,'distribuídos'),(51,26,2,'por'),(52,26,3,'por'),(53,27,2,'conta'),(54,27,3,'cuenta'),(55,28,2,'convidado'),(56,28,3,'invitado'),(57,29,2,'/'),(58,29,3,'/'),(59,30,2,'aviso'),(60,30,3,'advertencia'),(61,31,2,'sistema'),(62,31,3,'sistema'),(63,32,2,'em'),(64,32,3,'en'),(65,33,2,'desenvolvimento'),(66,33,3,'desarrollo'),(67,34,2,','),(68,34,3,','),(69,35,2,'administrador'),(70,35,3,'administrador'),(71,36,2,'entrega'),(72,36,3,'entrega'),(73,37,2,'empresa'),(74,37,3,'empresa'),(75,38,2,'informação'),(76,38,3,'informacion'),(77,39,2,'geral'),(78,39,3,'general'),(79,40,2,'configurações'),(80,40,3,'configuraciones'),(81,41,2,'contabilidade'),(82,41,3,'contabilidad'),(83,42,2,'adicional'),(84,42,3,'adicional'),(85,43,2,'integrações'),(86,43,3,'integraciones'),(87,44,2,'banco de dados'),(88,44,3,'base de datos'),(89,45,2,'externo'),(90,45,3,'externo'),(91,46,2,'recursos'),(92,46,3,'recursos'),(93,47,2,'configurações'),(94,47,3,'configuraciones'),(95,48,2,'moedas'),(96,48,3,'monedas'),(97,49,2,'nome'),(98,49,3,'nombre'),(99,51,2,'informações'),(100,51,3,'informaciones'),(101,52,2,'endereço'),(102,52,3,'dirección'),(103,53,2,'banco'),(104,53,3,'banco'),(105,54,2,'e'),(106,54,3,'y'),(107,55,2,'licença'),(108,55,3,'licencia'),(109,56,2,'cancelar'),(110,56,3,'cancelar'),(111,57,2,'salvar'),(112,57,3,'guardar'),(113,50,2,'fantasia'),(114,50,3,'fantasía'),(115,58,2,'registro'),(116,58,3,'registro'),(117,59,2,'número'),(118,59,3,'número'),(119,60,2,'telefone'),(120,60,3,'teléfono'),(121,61,2,'fax'),(122,61,3,'fax'),(123,62,2,'padrão'),(124,62,3,'estándar'),(125,63,2,'mensagem'),(126,63,3,'mensaje'),(127,64,2,'rua'),(128,64,3,'calle'),(129,65,2,'bairro'),(130,65,3,'barrio'),(131,66,2,'cidade'),(132,66,3,'ciudad'),(133,67,2,'código'),(134,67,3,'código'),(135,68,2,'postal'),(136,68,3,'postal'),(137,69,2,'complemento'),(138,69,3,'complemento'),(139,70,2,'referência'),(140,70,3,'referencia'),(141,71,2,'visitante'),(142,71,3,'visitante'),(143,72,2,'email'),(144,72,3,'correo electrónico'),(145,73,2,'validade'),(146,73,3,'validez'),(147,74,2,'de'),(148,74,3,'de'),(149,75,2,'empresas'),(150,75,3,'compañías'),(151,76,2,'usuários'),(152,76,3,'usuarios'),(153,77,2,'terminais'),(154,77,3,'terminales'),(155,78,2,'móveis'),(156,78,3,'muebles'),(157,79,2,':'),(158,79,3,':'),(159,80,2,'verificar'),(160,80,3,'comprobar'),(161,81,2,'módulos'),(162,81,3,'módulos'),(163,82,2,'para'),(164,82,3,'para'),(165,83,2,'sua'),(166,83,3,'su'),(167,84,2,'descrição'),(168,84,3,'descripción'),(169,85,2,'status'),(170,85,3,'estado'),(171,86,2,'lista'),(172,86,3,'catálogo'),(173,87,2,'novo'),(174,87,3,'nuevo'),(175,88,2,'editar'),(176,88,3,'cambiar'),(177,89,2,'remover'),(178,89,3,'pallar'),(179,90,2,'fechar'),(180,90,3,'cerrar'),(181,91,2,'moeda'),(182,91,3,'acuñar'),(183,92,2,'símbolo'),(184,92,3,'símbolo'),(185,93,2,'intercâmbio'),(186,93,3,'intercambio'),(187,94,2,'taxa'),(188,94,3,'tarifa'),(189,95,2,'unidade'),(190,95,3,'unidad'),(191,96,2,'mínimo'),(192,96,3,'mínimo'),(193,97,2,'casa da moeda'),(194,97,3,'pagaduría'),(195,98,2,'moeda'),(196,98,3,'moneda'),(197,101,2,'painel'),(198,101,3,'panel'),(199,102,2,'configuração'),(200,102,3,'configuracion'),(201,103,2,'entrada'),(202,103,3,'entrada'),(203,104,2,'dispositivos'),(204,104,3,'dispositivos'),(205,105,2,'relatórios'),(206,105,3,'informes'),(207,106,2,'inserir'),(208,106,3,'insertar'),(209,107,2,'menor'),(210,107,3,'menor'),(211,108,2,'denominação'),(212,108,3,'denominación'),(213,109,2,'atualizar'),(214,109,3,'actualizar'),(215,110,2,'dispositivo'),(216,110,3,'dispositivo'),(217,111,2,'selecionar'),(218,111,3,'seleccionar'),(219,112,2,'layout'),(220,112,3,'diseño'),(221,113,2,'tabelas'),(222,113,3,'tablas'),(223,114,2,'construir'),(224,114,3,'construir'),(225,115,2,'modelo'),(226,115,3,'modelo'),(227,116,2,'voltar'),(228,116,3,'volver'),(229,117,2,'características'),(230,117,3,'características'),(231,118,2,'fechamento'),(232,118,3,'cerradura'),(233,119,2,'parâmetros'),(234,119,3,'parámetros'),(235,120,2,'crédito'),(236,120,3,'crédito'),(237,121,2,'notas'),(238,121,3,'notas'),(239,122,2,'recibos'),(240,122,3,'albarán'),(241,123,2,'pos'),(242,123,3,'pos'),(243,124,2,'exibir'),(244,124,3,'exhibir'),(245,125,2,'cliente'),(246,125,3,'cliente'),(247,126,2,'primeira'),(248,126,3,'primera'),(249,127,2,'linha'),(250,127,3,'línea'),(251,128,2,'segunda'),(252,128,3,'segunda'),(253,129,2,'temporário'),(254,129,3,'provisional'),(255,130,2,'recibo'),(256,130,3,'albarán'),(257,131,2,'cabeçalho'),(258,131,3,'encabezamiento'),(259,132,2,'registros'),(260,132,3,'archivos'),(261,133,2,'ser'),(262,133,3,'ser'),(263,134,2,'impressos'),(264,134,3,'impresos'),(265,135,2,'em'),(266,135,3,'en'),(267,136,2,'o(a)'),(268,136,3,'el(la)'),(269,137,2,'fim'),(270,137,3,'fin'),(271,138,2,'caixa'),(272,138,3,'cajero'),(273,139,2,'histórico'),(274,139,3,'historial'),(275,140,2,'compra'),(276,140,3,'compra'),(277,141,2,'cancelado(s)(as)'),(278,141,3,'cancelado(s)(as)'),(279,142,2,'dinheiro'),(280,142,3,'dinero'),(281,143,2,'gaveta'),(282,143,3,'cajón'),(283,144,2,'manual'),(284,144,3,'manual'),(285,145,2,'aberturas'),(286,145,3,'aberturas'),(287,146,2,'('),(288,146,3,'('),(289,147,2,')'),(290,147,3,')'),(291,148,2,'registro'),(292,148,3,'registro'),(293,149,2,'apagado(s)(as)'),(294,149,3,'apagado(s)(as)'),(295,151,2,'clicando'),(296,151,3,'hacer clic'),(297,152,2,'botão'),(298,152,3,'botón'),(299,153,2,'criado(a)'),(300,153,3,'criado(a)'),(301,154,2,'um(a)'),(302,154,3,'un(a)'),(303,155,2,'já'),(304,155,3,'ya'),(305,156,2,'você(s)'),(306,156,3,'usted(es)'),(307,157,2,'pode'),(308,157,3,'puede'),(309,158,2,'adicionar'),(310,158,3,'sumar'),(311,159,2,'colunas'),(312,159,3,'columnas'),(313,160,2,'esse(a)'),(314,160,3,'ese(a)'),(315,161,2,'listagem'),(316,161,3,'listado'),(317,162,2,'através de'),(318,162,3,'través de'),(319,163,2,'elemento'),(320,163,3,'elemento'),(321,164,2,'.'),(322,164,3,'.'),(323,165,2,'mostrar'),(324,165,3,'exhibir'),(325,166,2,'semelhante'),(326,166,3,'semejante'),(327,167,2,'compras'),(328,167,3,'compras'),(329,168,2,'depois de'),(330,168,3,'después de'),(331,169,2,'digitação'),(332,169,3,'digitación'),(333,170,2,'em'),(334,170,3,'en'),(335,171,2,'observações'),(336,171,3,'observaciónes'),(337,172,2,'pré'),(338,172,3,'pre'),(339,173,2,'pagamento'),(340,173,3,'pagamiento'),(341,174,2,'restaurantes'),(342,174,3,'retaurantes'),(343,175,2,'-'),(344,175,3,'-'),(345,176,2,'emitir'),(346,176,3,'emitir'),(347,177,2,'não'),(348,177,3,'no'),(349,178,2,'documento'),(350,178,3,'documento'),(351,179,2,'substituir'),(352,179,3,'sustituir'),(353,180,2,'logotipo'),(354,180,3,'logotipo'),(355,181,2,'com'),(356,181,3,'con'),(357,182,2,'barras'),(358,182,3,'barras'),(359,183,2,'indicando'),(360,183,3,'indicando'),(361,184,2,'volume'),(362,184,3,'volumen'),(363,185,2,'lojas'),(364,185,3,'tiendas'),(365,186,2,'todos(as)'),(366,186,3,'todos(as)'),(367,187,2,'durante'),(368,187,3,'durante'),(369,188,2,'pesquisa'),(370,188,3,'encuesta'),(371,189,2,'exato'),(372,189,3,'exacto'),(373,190,2,'nomes'),(374,190,3,'nombres'),(375,191,2,'somente'),(376,191,3,'solamente'),(377,192,2,'habilitar'),(378,192,3,'habilitar'),(379,193,2,'impressão'),(380,193,3,'impresión'),(381,194,2,'pedido'),(382,194,3,'solicitud'),(383,195,2,'confirmação'),(384,195,3,'confirmación'),(385,196,2,'produto'),(386,196,3,'producto'),(387,197,2,'valores'),(388,197,3,'valores'),(389,198,2,'quando'),(390,198,3,'cuando'),(391,199,2,'mudado'),(392,199,3,'cambiado'),(393,200,2,'cliente'),(394,200,3,'cliente'),(395,201,2,'permitir'),(396,201,3,'permitir'),(397,202,2,'usuário'),(398,202,3,'usuario'),(399,203,2,'terminar'),(400,203,3,'terminar'),(401,204,2,'antes de'),(402,204,3,'antes de'),(403,205,2,'entrando'),(404,205,3,'entrando'),(405,206,2,'quantidade'),(406,206,3,'cantidad'),(407,207,2,'pago'),(408,207,3,'pagado'),(409,208,2,'cartão'),(410,208,3,'cartón'),(411,209,2,'débito'),(412,209,3,'débito'),(413,210,2,'caso'),(414,210,3,'caso'),(415,211,2,'pagamentos'),(416,211,3,'pagamientos'),(417,212,2,'cartão presente'),(418,212,3,'tarjeta de regalo'),(419,213,2,'detalhes'),(420,213,3,'detalles'),(421,214,2,'título'),(422,214,3,'título'),(423,215,2,'tamanho'),(424,215,3,'tamaño'),(425,216,2,'fonte'),(426,216,3,'fuente'),(427,217,2,'grupo'),(428,217,3,'grupo'),(429,218,2,'texto'),(430,218,3,'texto'),(431,219,2,'imprimir'),(432,219,3,'imprimir'),(433,220,2,'transportador'),(434,220,3,'portador'),(435,221,2,'idêntico'),(436,221,3,'idéntico'),(437,222,2,'produtos'),(438,222,3,'productos'),(439,223,2,'confirmar'),(440,223,3,'confirmar'),(441,224,2,'preço'),(442,224,3,'precio'),(443,225,2,'mesmo'),(444,225,3,'mismo'),(445,226,2,'colocar'),(446,226,3,'poner'),(447,227,2,'último(s)'),(448,227,3,'último(s)'),(449,228,2,'dois'),(450,228,3,'dos'),(451,229,2,'digitos'),(452,229,3,'dígitos'),(453,230,2,'negrito'),(454,230,3,'audaz'),(455,231,2,'modificar'),(456,231,3,'cambiar'),(457,232,2,'impressões'),(458,232,3,'impresiones'),(459,233,2,'mudanças'),(460,233,3,'mudanzas'),(461,234,2,'impressora'),(462,234,3,'impresora'),(463,235,2,'linhas'),(464,235,3,'líneas'),(465,236,2,'cada'),(466,236,3,'cada'),(467,237,2,'omitir'),(468,237,3,'omitir'),(469,238,2,'qual'),(470,238,3,'cual'),(471,239,2,'pertence'),(472,239,3,'pertenece'),(473,240,2,'id'),(474,240,3,'id'),(475,241,2,'inserido'),(476,241,3,'insertado'),(477,242,2,'caixa'),(478,242,3,'cajero'),(479,243,2,'vendido'),(480,243,3,'vendido'),(481,244,2,'dia'),(482,244,3,'día'),(483,245,2,'quantidade'),(484,245,3,'cantidad'),(485,246,2,'célula'),(486,246,3,'celda'),(487,247,2,'relação'),(488,247,3,'proporción'),(489,248,2,'incluir'),(490,248,3,'incluir'),(491,249,2,'em vez de'),(492,249,3,'en vez de'),(493,250,2,'usar'),(494,250,3,'utilizar'),(495,251,2,'desconto'),(496,251,3,'descuento'),(497,252,2,'ajuda'),(498,252,3,'ayuda'),(499,253,2,'maneiras'),(500,253,3,'maneras'),(501,254,2,'presente'),(502,254,3,'regalo'),(503,255,2,'linhas'),(504,255,3,'líneas'),(505,256,2,'item'),(506,256,3,'ítem'),(507,257,2,'permitindo'),(508,257,3,'permitiendo'),(509,258,2,'mais'),(510,258,3,'más'),(511,259,2,'informação'),(512,259,3,'información'),(513,260,2,'atual'),(514,260,3,'actual'),(515,261,2,'promocional'),(516,261,3,'promocional'),(517,262,2,'é(são)/está(ão)'),(518,262,3,'es(son)/está(están)'),(519,263,2,'loja'),(520,263,3,'tienda'),(521,264,2,'detalhado'),(522,264,3,'detallado'),(523,265,2,'ocultar'),(524,265,3,'ocultar'),(525,266,2,'comentários'),(526,266,3,'comentarios'),(527,267,2,'inseridos'),(528,267,3,'introducidos'),(529,268,2,'sempre'),(530,268,3,'siempre'),(531,269,2,'artigo'),(532,269,3,'artículo'),(533,270,2,'é/está'),(534,270,3,'es/está'),(535,271,2,'dado'),(536,271,3,'dado'),(537,272,2,'sem'),(538,272,3,'sin'),(539,273,2,'mostra'),(540,273,3,'muestra'),(541,274,2,'somas'),(542,274,3,'sumas'),(543,275,2,'incluir'),(544,275,3,'incluir'),(545,276,2,'dica'),(546,276,3,'pista'),(547,277,2,'quantidades'),(548,277,3,'cantidades'),(549,278,2,'modelos'),(550,278,3,'plantillas'),(551,279,2,'resolução'),(552,279,3,'resolución'),(553,280,2,'predefinido'),(554,280,3,'predeterminado'),(555,282,2,'tabela'),(556,282,3,'tabla'),(557,283,2,'seção'),(558,283,3,'sección'),(559,284,2,'assentos'),(560,284,3,'asientos'),(561,285,2,'qty'),(562,285,3,'qty'),(563,286,2,'excluir'),(564,286,3,'eliminar'),(565,287,2,'reiniciar'),(566,287,3,'reiniciar'),(567,288,2,'criar'),(568,288,3,'crear'),(569,289,2,'imagem'),(570,289,3,'imagen'),(571,290,2,'funcionário'),(572,290,3,'empleado'),(573,291,2,'venda'),(574,291,3,'venta'),(575,292,2,'aba'),(576,292,3,'lengüeta'),(577,293,2,'calculadora'),(578,293,3,'calculadora'),(579,301,2,'departamento'),(580,301,3,'departamento'),(581,302,2,'projetos'),(582,302,3,'proyectos'),(583,303,2,'inicio'),(584,303,3,'inicio'),(585,304,2,'parada'),(586,304,3,'parada'),(587,305,2,'projeto'),(588,305,3,'proyecto'),(589,306,2,'data'),(590,306,3,'fecha'),(591,307,2,'permissões'),(592,307,3,'permisos'),(593,308,2,'tipo'),(594,308,3,'tipo'),(595,309,2,'empregos'),(596,309,3,'trabajos'),(597,310,2,'completo'),(598,310,3,'completo'),(599,311,2,'login'),(600,311,3,'login'),(601,312,2,'emprego'),(602,312,3,'trabajo'),(603,313,2,'senha'),(604,313,3,'contraseña'),(605,314,2,'tipos'),(606,314,3,'tipos'),(607,315,2,'depósito'),(608,315,3,'depósito'),(609,316,2,'estoques'),(610,316,3,'cepo'),(611,317,2,'físico'),(612,317,3,'físico'),(613,318,2,'estoque'),(614,318,3,'existencias'),(615,319,2,'cores'),(616,319,3,'colores'),(617,320,2,'cor'),(618,320,3,'color'),(619,321,2,'país'),(620,321,3,'país'),(621,322,2,'fornecedores'),(622,322,3,'proveedores'),(623,323,2,'apis'),(624,323,3,'apis'),(625,324,2,'fornecedor'),(626,324,3,'proveedor'),(627,325,2,'do(a)'),(628,325,3,'del(de la)'),(629,326,2,'web'),(630,326,3,'web'),(631,327,2,'site'),(632,327,3,'sitio'),(633,328,2,'idioma'),(634,328,3,'idioma'),(635,329,2,'org'),(636,329,3,'org'),(637,330,2,'nr'),(638,330,3,'nr'),(639,331,2,'estabelecimento'),(640,331,3,'establecimiento'),(641,332,2,'envio'),(642,332,3,'envío'),(643,333,2,'gln'),(644,333,3,'gln'),(645,334,2,'contato'),(646,334,3,'contacto'),(647,335,2,'pessoa'),(648,335,3,'persona'),(649,336,2,'enviar'),(650,336,3,'enviar'),(651,337,2,'xml'),(652,337,3,'xml'),(653,339,2,'formato'),(654,339,3,'formato'),(655,338,2,'api'),(656,338,3,'api'),(657,340,2,'nome de usuário'),(658,340,3,'nombre de usuário'),(659,341,2,'definir'),(660,341,3,'definir'),(661,342,2,'retirada'),(662,342,3,'retirada'),(663,343,2,'vender'),(664,343,3,'vender'),(665,344,2,'fatura'),(666,344,3,'factura'),(667,345,2,'estatisticas'),(668,345,3,'estadísticas'),(669,346,2,'mudança'),(670,346,3,'cambio'),(671,347,2,'método'),(672,347,3,'método'),(673,348,2,'opções'),(674,348,3,'opciones'),(675,349,2,'imagens'),(676,349,3,'imágenes'),(677,350,2,'cartões'),(678,350,3,'tarjetas'),(679,351,2,'acesso'),(680,351,3,'acceso'),(681,352,2,'rótulo'),(682,352,3,'etiqueta'),(683,353,2,'rodapé'),(684,353,3,'pie de página'),(685,354,2,'a5'),(686,354,3,'a5'),(687,355,2,'obs'),(688,355,3,'obs'),(689,356,2,'separado(a)'),(690,356,3,'separado(a)'),(691,357,2,'diferente'),(692,357,3,'diferente'),(693,358,2,'planos de fundo'),(694,358,3,'planes de fondo'),(695,359,2,'sms'),(696,359,3,'sms'),(697,360,2,'enviado(a)'),(698,360,3,'expedido(a)'),(699,361,2,'propostas'),(700,361,3,'propuestas'),(701,362,2,'proposta'),(702,362,3,'propuesta'),(703,363,2,'clientes'),(704,363,3,'clientes'),(705,364,2,'pagamentos'),(706,364,3,'pagos'),(707,365,2,'presente'),(708,365,3,'regalo'),(709,367,2,'modelos'),(710,367,3,'modelos'),(711,368,2,'estado'),(712,368,3,'estado'),(713,369,2,'opção'),(714,369,3,'opción'),(715,370,2,'negócio'),(716,370,3,'negocio'),(717,371,2,'ver'),(718,371,3,'ver'),(719,372,2,'faturas'),(720,372,3,'facturas'),(721,373,2,'solicitações'),(722,373,3,'peticiones'),(723,374,2,'cobrança'),(724,374,3,'cobro'),(725,375,2,'carta'),(726,375,3,'carta'),(727,376,2,'extra'),(728,376,3,'extra'),(729,377,2,'dados'),(730,377,3,'datos'),(731,378,2,'itens'),(732,378,3,'elementos'),(733,379,2,'faturado(a)'),(734,379,3,'facturado(a)'),(735,380,2,'eletrônico(a)'),(736,380,3,'electrónico(a)'),(737,381,2,'permitido(a)'),(738,381,3,'permitido(a)'),(739,382,2,'redirecionar'),(740,382,3,'redirigir'),(741,383,2,'acompanhar'),(742,383,3,'acompañar'),(743,384,2,'aberto(s)(as)'),(744,384,3,'abierto(s)(as)'),(745,385,2,'automaticamente'),(746,385,3,'automáticamente'),(747,386,2,'abertos(as)'),(748,386,3,'abiertos(as)'),(749,388,2,'não'),(750,388,3,'no'),(751,389,2,'estático'),(752,389,3,'estatico'),(753,390,2,'seleciona'),(754,390,3,'selecciona'),(755,387,2,'descrever'),(756,387,3,'describir'),(757,395,2,'bilhete'),(758,395,3,'billete'),(759,396,2,'limite'),(760,396,3,'limite'),(761,398,2,'loja'),(762,398,3,'tienda'),(763,399,2,'vezes'),(764,399,3,'veces'),(765,400,2,'eles(as)'),(766,400,3,'ellos(as)'),(767,401,2,'deveria'),(768,401,3,'deberia'),(769,402,2,'cartas'),(770,402,3,'cartas'),(771,403,2,'envio'),(772,403,3,'envío'),(773,404,2,'erário'),(774,404,3,'erario'),(775,407,2,'autorização'),(776,407,3,'autorización'),(777,412,2,'gênero'),(778,412,3,'género'),(779,413,2,'identificação'),(780,413,3,'identificación'),(781,414,2,'aniversário'),(782,414,3,'cumpleaños'),(783,416,2,'especificado'),(784,416,3,'especificado'),(785,417,2,'masculino'),(786,417,3,'masculino'),(787,418,2,'feminino'),(788,418,3,'feminino'),(789,419,2,'único'),(790,419,3,'único'),(791,420,2,'porcentagem'),(792,420,3,'porcentaje'),(793,421,2,'por cento'),(794,421,3,'por ciento'),(795,425,2,'mês'),(796,425,3,'mes'),(797,426,2,'ano'),(798,426,3,'año'),(799,427,2,'caminho'),(800,427,3,'camino'),(801,428,2,'plataforma'),(802,428,3,'plataforma'),(803,429,2,'inicial'),(804,429,3,'inicial'),(805,430,2,'final'),(806,430,3,'final'),(807,431,2,'cartões'),(808,431,3,'cartones'),(809,432,2,'saldo'),(810,432,3,'saldo'),(811,433,2,'grupos'),(812,433,3,'grupos'),(813,434,2,'regras'),(814,434,3,'reglas'),(815,435,2,'fabricantes'),(816,435,3,'fabricantes'),(817,436,2,'importação'),(818,436,3,'importación'),(819,437,2,'séries'),(820,437,3,'series'),(821,438,2,'pacote(s)'),(822,438,3,'paquete(s)'),(823,439,2,'postagem'),(824,439,3,'correo postal'),(825,440,2,'rótulos'),(826,440,3,'etiquetas'),(827,441,2,'redondo(a)'),(828,441,3,'redondo(a)'),(829,442,2,'qrcode'),(830,442,3,'qrcode'),(831,443,2,'código de barras'),(832,443,3,'código de barras'),(833,444,2,'gerar'),(834,444,3,'generar'),(835,445,2,'instalado(a)'),(836,445,3,'instalado(a)'),(837,446,2,'porta'),(838,446,3,'puerta'),(839,447,2,'ativo'),(840,447,3,'activo'),(841,448,2,'comprovante'),(842,448,3,'prueba'),(843,449,2,'saída'),(844,449,3,'salída'),(845,450,2,'controle'),(846,450,3,'control'),(847,451,2,'fora'),(848,451,3,'fuera'),(849,452,2,'expiração'),(850,452,3,'expiración'),(851,453,2,'preço'),(852,453,3,'precios'),(853,454,2,'compra'),(854,454,3,'compra'),(855,455,2,'lucro'),(856,455,3,'lucro'),(857,456,2,'margem'),(858,456,3,'margen'),(859,457,2,'bruto'),(860,457,3,'bruto'),(861,458,2,'tributos'),(862,458,3,'tributos'),(863,459,2,'seleção'),(864,459,3,'selección'),(865,460,2,'componentes'),(866,460,3,'componentes'),(867,461,2,'alternativas'),(868,461,3,'alternativas'),(869,462,2,'tamanhos'),(870,462,3,'tamaños'),(871,463,2,'aquisição'),(872,463,3,'adquisición'),(873,464,2,'webshop'),(874,464,3,'tienda web'),(875,466,2,'cod'),(876,466,3,'cod'),(877,467,2,'ean'),(878,467,3,'ean'),(879,468,2,'temporariamente'),(880,468,3,'temporalmente'),(881,469,2,'semana'),(882,469,3,'semana'),(883,470,2,'dias'),(884,470,3,'días'),(885,471,2,'fim de semana'),(886,471,3,'fin de semana'),(887,472,2,'segunda'),(888,472,3,'lunes'),(889,473,2,'terça'),(890,473,3,'martes'),(891,474,2,'quarta'),(892,474,3,'miércoles'),(893,475,2,'quinta'),(894,475,3,'jueves'),(895,476,2,'sexta'),(896,476,3,'viernes'),(897,477,2,'sábado'),(898,477,3,'sábado'),(899,478,2,'domingo'),(900,478,3,'domingo'),(901,479,2,'principal'),(902,479,3,'principal'),(903,480,2,'um(a)'),(904,480,3,'un(a)'),(905,481,2,'?'),(906,481,3,'?'),(907,482,2,'recomendado(a)'),(908,482,3,'recomendado(a)'),(909,483,2,'bruto'),(910,483,3,'bruto'),(911,484,2,'líquido'),(912,484,3,'líquido'),(913,485,2,'negociado(a)'),(914,485,3,'negociado(a)'),(915,486,2,'retirar'),(916,486,3,'llevarse'),(917,487,2,'viagem'),(918,487,3,'viaje'),(919,488,2,'atacado'),(920,488,3,'al por mayor'),(921,489,2,'série'),(922,489,3,'serie'),(923,491,2,'comprado'),(924,491,3,'comprado'),(925,492,2,'partida(o)'),(926,492,3,'partida(o)'),(927,493,2,'devolvido(a)'),(928,493,3,'devuelto(a)'),(929,494,2,'fabricante'),(930,494,3,'fabricante');
/*!40000 ALTER TABLE `word_translated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_country`
--

DROP TABLE IF EXISTS `zz_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_country` (
  `idzz_country` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) NOT NULL,
  `name` varchar(60) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` varchar(45) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idzz_country`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_country`
--

LOCK TABLES `zz_country` WRITE;
/*!40000 ALTER TABLE `zz_country` DISABLE KEYS */;
INSERT INTO `zz_country` VALUES (1,'BR','Brazil','2017-05-17 11:30:14',NULL,NULL,'1'),(2,'TR','Test2','2017-05-18 18:46:21','2017-05-18 18:46:26','2017-05-18 18:46:29','3'),(3,'AF','Afghanistan','2017-05-19 13:44:59',NULL,NULL,'1'),(4,'AL','Albania','2017-05-19 13:44:59',NULL,NULL,'1'),(5,'DZ','Algeria','2017-05-19 13:44:59',NULL,NULL,'1'),(6,'DS','American Samoa','2017-05-19 13:44:59',NULL,NULL,'1'),(7,'AD','Andorra','2017-05-19 13:44:59',NULL,NULL,'1'),(8,'AO','Angola','2017-05-19 13:44:59',NULL,NULL,'1'),(9,'AI','Anguilla','2017-05-19 13:44:59',NULL,NULL,'1'),(10,'AQ','Antarctica','2017-05-19 13:44:59',NULL,NULL,'1'),(11,'AG','Antigua and Barbuda','2017-05-19 13:44:59',NULL,NULL,'1'),(12,'AR','Argentina','2017-05-19 13:44:59',NULL,NULL,'1'),(13,'AM','Armenia','2017-05-19 13:44:59',NULL,NULL,'1'),(14,'AW','Aruba','2017-05-19 13:44:59',NULL,NULL,'1'),(15,'AU','Australia','2017-05-19 13:44:59',NULL,NULL,'1'),(16,'AT','Austria','2017-05-19 13:44:59',NULL,NULL,'1'),(17,'AZ','Azerbaijan','2017-05-19 13:44:59',NULL,NULL,'1'),(18,'BS','Bahamas','2017-05-19 13:44:59',NULL,NULL,'1'),(19,'BH','Bahrain','2017-05-19 13:44:59',NULL,NULL,'1'),(20,'BD','Bangladesh','2017-05-19 13:44:59',NULL,NULL,'1'),(21,'BB','Barbados','2017-05-19 13:44:59',NULL,NULL,'1'),(22,'BY','Belarus','2017-05-19 13:44:59',NULL,NULL,'1'),(23,'BE','Belgium','2017-05-19 13:44:59',NULL,NULL,'1'),(24,'BZ','Belize','2017-05-19 13:44:59',NULL,NULL,'1'),(25,'BJ','Benin','2017-05-19 13:44:59',NULL,NULL,'1'),(26,'BM','Bermuda','2017-05-19 13:44:59',NULL,NULL,'1'),(27,'BT','Bhutan','2017-05-19 13:44:59',NULL,NULL,'1'),(28,'BO','Bolivia','2017-05-19 13:44:59',NULL,NULL,'1'),(29,'BA','Bosnia and Herzegovina','2017-05-19 13:44:59',NULL,NULL,'1'),(30,'BW','Botswana','2017-05-19 13:44:59',NULL,NULL,'1'),(31,'BV','Bouvet Island','2017-05-19 13:44:59',NULL,NULL,'1'),(32,'BR','Brazil','2017-05-19 13:44:59',NULL,NULL,'1'),(33,'IO','British Indian Ocean Territory','2017-05-19 13:44:59',NULL,NULL,'1'),(34,'BN','Brunei Darussalam','2017-05-19 13:44:59',NULL,NULL,'1'),(35,'BG','Bulgaria','2017-05-19 13:44:59',NULL,NULL,'1'),(36,'BF','Burkina Faso','2017-05-19 13:44:59',NULL,NULL,'1'),(37,'BI','Burundi','2017-05-19 13:44:59',NULL,NULL,'1'),(38,'KH','Cambodia','2017-05-19 13:44:59',NULL,NULL,'1'),(39,'CM','Cameroon','2017-05-19 13:44:59',NULL,NULL,'1'),(40,'CA','Canada','2017-05-19 13:44:59',NULL,NULL,'1'),(41,'CV','Cape Verde','2017-05-19 13:44:59',NULL,NULL,'1'),(42,'KY','Cayman Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(43,'CF','Central African Republic','2017-05-19 13:44:59',NULL,NULL,'1'),(44,'TD','Chad','2017-05-19 13:44:59',NULL,NULL,'1'),(45,'CL','Chile','2017-05-19 13:44:59',NULL,NULL,'1'),(46,'CN','China','2017-05-19 13:44:59',NULL,NULL,'1'),(47,'CX','Christmas Island','2017-05-19 13:44:59',NULL,NULL,'1'),(48,'CC','Cocos (Keeling) Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(49,'CO','Colombia','2017-05-19 13:44:59',NULL,NULL,'1'),(50,'KM','Comoros','2017-05-19 13:44:59',NULL,NULL,'1'),(51,'CG','Congo','2017-05-19 13:44:59',NULL,NULL,'1'),(52,'CK','Cook Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(53,'CR','Costa Rica','2017-05-19 13:44:59',NULL,NULL,'1'),(54,'HR','Croatia (Hrvatska)','2017-05-19 13:44:59',NULL,NULL,'1'),(55,'CU','Cuba','2017-05-19 13:44:59',NULL,NULL,'1'),(56,'CY','Cyprus','2017-05-19 13:44:59',NULL,NULL,'1'),(57,'CZ','Czech Republic','2017-05-19 13:44:59',NULL,NULL,'1'),(58,'DK','Denmark','2017-05-19 13:44:59',NULL,NULL,'1'),(59,'DJ','Djibouti','2017-05-19 13:44:59',NULL,NULL,'1'),(60,'DM','Dominica','2017-05-19 13:44:59',NULL,NULL,'1'),(61,'DO','Dominican Republic','2017-05-19 13:44:59',NULL,NULL,'1'),(62,'TP','East Timor','2017-05-19 13:44:59',NULL,NULL,'1'),(63,'EC','Ecuador','2017-05-19 13:44:59',NULL,NULL,'1'),(64,'EG','Egypt','2017-05-19 13:44:59',NULL,NULL,'1'),(65,'SV','El Salvador','2017-05-19 13:44:59',NULL,NULL,'1'),(66,'GQ','Equatorial Guinea','2017-05-19 13:44:59',NULL,NULL,'1'),(67,'ER','Eritrea','2017-05-19 13:44:59',NULL,NULL,'1'),(68,'EE','Estonia','2017-05-19 13:44:59',NULL,NULL,'1'),(69,'ET','Ethiopia','2017-05-19 13:44:59',NULL,NULL,'1'),(70,'FK','Falkland Islands (Malvinas)','2017-05-19 13:44:59',NULL,NULL,'1'),(71,'FO','Faroe Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(72,'FJ','Fiji','2017-05-19 13:44:59',NULL,NULL,'1'),(73,'FI','Finland','2017-05-19 13:44:59',NULL,NULL,'1'),(74,'FR','France','2017-05-19 13:44:59',NULL,NULL,'1'),(75,'FX','France, Metropolitan','2017-05-19 13:44:59',NULL,NULL,'1'),(76,'GF','French Guiana','2017-05-19 13:44:59',NULL,NULL,'1'),(77,'PF','French Polynesia','2017-05-19 13:44:59',NULL,NULL,'1'),(78,'TF','French Southern Territories','2017-05-19 13:44:59',NULL,NULL,'1'),(79,'GA','Gabon','2017-05-19 13:44:59',NULL,NULL,'1'),(80,'GM','Gambia','2017-05-19 13:44:59',NULL,NULL,'1'),(81,'GE','Georgia','2017-05-19 13:44:59',NULL,NULL,'1'),(82,'DE','Germany','2017-05-19 13:44:59',NULL,NULL,'1'),(83,'GH','Ghana','2017-05-19 13:44:59',NULL,NULL,'1'),(84,'GI','Gibraltar','2017-05-19 13:44:59',NULL,NULL,'1'),(85,'GK','Guernsey','2017-05-19 13:44:59',NULL,NULL,'1'),(86,'GR','Greece','2017-05-19 13:44:59',NULL,NULL,'1'),(87,'GL','Greenland','2017-05-19 13:44:59',NULL,NULL,'1'),(88,'GD','Grenada','2017-05-19 13:44:59',NULL,NULL,'1'),(89,'GP','Guadeloupe','2017-05-19 13:44:59',NULL,NULL,'1'),(90,'GU','Guam','2017-05-19 13:44:59',NULL,NULL,'1'),(91,'GT','Guatemala','2017-05-19 13:44:59',NULL,NULL,'1'),(92,'GN','Guinea','2017-05-19 13:44:59',NULL,NULL,'1'),(93,'GW','Guinea-Bissau','2017-05-19 13:44:59',NULL,NULL,'1'),(94,'GY','Guyana','2017-05-19 13:44:59',NULL,NULL,'1'),(95,'HT','Haiti','2017-05-19 13:44:59',NULL,NULL,'1'),(96,'HM','Heard and Mc Donald Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(97,'HN','Honduras','2017-05-19 13:44:59',NULL,NULL,'1'),(98,'HK','Hong Kong','2017-05-19 13:44:59',NULL,NULL,'1'),(99,'HU','Hungary','2017-05-19 13:44:59',NULL,NULL,'1'),(100,'IS','Iceland','2017-05-19 13:44:59',NULL,NULL,'1'),(101,'IN','India','2017-05-19 13:44:59',NULL,NULL,'1'),(102,'IM','Isle of Man','2017-05-19 13:44:59',NULL,NULL,'1'),(103,'ID','Indonesia','2017-05-19 13:44:59',NULL,NULL,'1'),(104,'IR','Iran (Islamic Republic of)','2017-05-19 13:44:59',NULL,NULL,'1'),(105,'IQ','Iraq','2017-05-19 13:44:59',NULL,NULL,'1'),(106,'IE','Ireland','2017-05-19 13:44:59',NULL,NULL,'1'),(107,'IL','Israel','2017-05-19 13:44:59',NULL,NULL,'1'),(108,'IT','Italy','2017-05-19 13:44:59',NULL,NULL,'1'),(109,'CI','Ivory Coast','2017-05-19 13:44:59',NULL,NULL,'1'),(110,'JE','Jersey','2017-05-19 13:44:59',NULL,NULL,'1'),(111,'JM','Jamaica','2017-05-19 13:44:59',NULL,NULL,'1'),(112,'JP','Japan','2017-05-19 13:44:59',NULL,NULL,'1'),(113,'JO','Jordan','2017-05-19 13:44:59',NULL,NULL,'1'),(114,'KZ','Kazakhstan','2017-05-19 13:44:59',NULL,NULL,'1'),(115,'KE','Kenya','2017-05-19 13:44:59',NULL,NULL,'1'),(116,'KI','Kiribati','2017-05-19 13:44:59',NULL,NULL,'1'),(117,'KP','Korea, Democratic People\'s Republic of','2017-05-19 13:44:59',NULL,NULL,'1'),(118,'KR','Korea, Republic of','2017-05-19 13:44:59',NULL,NULL,'1'),(119,'XK','Kosovo','2017-05-19 13:44:59',NULL,NULL,'1'),(120,'KW','Kuwait','2017-05-19 13:44:59',NULL,NULL,'1'),(121,'KG','Kyrgyzstan','2017-05-19 13:44:59',NULL,NULL,'1'),(122,'LA','Lao People\'s Democratic Republic','2017-05-19 13:44:59',NULL,NULL,'1'),(123,'LV','Latvia','2017-05-19 13:44:59',NULL,NULL,'1'),(124,'LB','Lebanon','2017-05-19 13:44:59',NULL,NULL,'1'),(125,'LS','Lesotho','2017-05-19 13:44:59',NULL,NULL,'1'),(126,'LR','Liberia','2017-05-19 13:44:59',NULL,NULL,'1'),(127,'LY','Libyan Arab Jamahiriya','2017-05-19 13:44:59',NULL,NULL,'1'),(128,'LI','Liechtenstein','2017-05-19 13:44:59',NULL,NULL,'1'),(129,'LT','Lithuania','2017-05-19 13:44:59',NULL,NULL,'1'),(130,'LU','Luxembourg','2017-05-19 13:44:59',NULL,NULL,'1'),(131,'MO','Macau','2017-05-19 13:44:59',NULL,NULL,'1'),(132,'MK','Macedonia','2017-05-19 13:44:59',NULL,NULL,'1'),(133,'MG','Madagascar','2017-05-19 13:44:59',NULL,NULL,'1'),(134,'MW','Malawi','2017-05-19 13:44:59',NULL,NULL,'1'),(135,'MY','Malaysia','2017-05-19 13:44:59',NULL,NULL,'1'),(136,'MV','Maldives','2017-05-19 13:44:59',NULL,NULL,'1'),(137,'ML','Mali','2017-05-19 13:44:59',NULL,NULL,'1'),(138,'MT','Malta','2017-05-19 13:44:59',NULL,NULL,'1'),(139,'MH','Marshall Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(140,'MQ','Martinique','2017-05-19 13:44:59',NULL,NULL,'1'),(141,'MR','Mauritania','2017-05-19 13:44:59',NULL,NULL,'1'),(142,'MU','Mauritius','2017-05-19 13:44:59',NULL,NULL,'1'),(143,'TY','Mayotte','2017-05-19 13:44:59',NULL,NULL,'1'),(144,'MX','Mexico','2017-05-19 13:44:59',NULL,NULL,'1'),(145,'FM','Micronesia, Federated States of','2017-05-19 13:44:59',NULL,NULL,'1'),(146,'MD','Moldova, Republic of','2017-05-19 13:44:59',NULL,NULL,'1'),(147,'MC','Monaco','2017-05-19 13:44:59',NULL,NULL,'1'),(148,'MN','Mongolia','2017-05-19 13:44:59',NULL,NULL,'1'),(149,'ME','Montenegro','2017-05-19 13:44:59',NULL,NULL,'1'),(150,'MS','Montserrat','2017-05-19 13:44:59',NULL,NULL,'1'),(151,'MA','Morocco','2017-05-19 13:44:59',NULL,NULL,'1'),(152,'MZ','Mozambique','2017-05-19 13:44:59',NULL,NULL,'1'),(153,'MM','Myanmar','2017-05-19 13:44:59',NULL,NULL,'1'),(154,'NA','Namibia','2017-05-19 13:44:59',NULL,NULL,'1'),(155,'NR','Nauru','2017-05-19 13:44:59',NULL,NULL,'1'),(156,'NP','Nepal','2017-05-19 13:44:59',NULL,NULL,'1'),(157,'NL','Netherlands','2017-05-19 13:44:59',NULL,NULL,'1'),(158,'AN','Netherlands Antilles','2017-05-19 13:44:59',NULL,NULL,'1'),(159,'NC','New Caledonia','2017-05-19 13:44:59',NULL,NULL,'1'),(160,'NZ','New Zealand','2017-05-19 13:44:59',NULL,NULL,'1'),(161,'NI','Nicaragua','2017-05-19 13:44:59',NULL,NULL,'1'),(162,'NE','Niger','2017-05-19 13:44:59',NULL,NULL,'1'),(163,'NG','Nigeria','2017-05-19 13:44:59',NULL,NULL,'1'),(164,'NU','Niue','2017-05-19 13:44:59',NULL,NULL,'1'),(165,'NF','Norfolk Island','2017-05-19 13:44:59',NULL,NULL,'1'),(166,'MP','Northern Mariana Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(167,'NO','Norway','2017-05-19 13:44:59',NULL,NULL,'1'),(168,'OM','Oman','2017-05-19 13:44:59',NULL,NULL,'1'),(169,'PK','Pakistan','2017-05-19 13:44:59',NULL,NULL,'1'),(170,'PW','Palau','2017-05-19 13:44:59',NULL,NULL,'1'),(171,'PS','Palestine','2017-05-19 13:44:59',NULL,NULL,'1'),(172,'PA','Panama','2017-05-19 13:44:59',NULL,NULL,'1'),(173,'PG','Papua New Guinea','2017-05-19 13:44:59',NULL,NULL,'1'),(174,'PY','Paraguay','2017-05-19 13:44:59',NULL,NULL,'1'),(175,'PE','Peru','2017-05-19 13:44:59',NULL,NULL,'1'),(176,'PH','Philippines','2017-05-19 13:44:59',NULL,NULL,'1'),(177,'PN','Pitcairn','2017-05-19 13:44:59',NULL,NULL,'1'),(178,'PL','Poland','2017-05-19 13:44:59',NULL,NULL,'1'),(179,'PT','Portugal','2017-05-19 13:44:59',NULL,NULL,'1'),(180,'PR','Puerto Rico','2017-05-19 13:44:59',NULL,NULL,'1'),(181,'QA','Qatar','2017-05-19 13:44:59',NULL,NULL,'1'),(182,'RE','Reunion','2017-05-19 13:44:59',NULL,NULL,'1'),(183,'RO','Romania','2017-05-19 13:44:59',NULL,NULL,'1'),(184,'RU','Russian Federation','2017-05-19 13:44:59',NULL,NULL,'1'),(185,'RW','Rwanda','2017-05-19 13:44:59',NULL,NULL,'1'),(186,'KN','Saint Kitts and Nevis','2017-05-19 13:44:59',NULL,NULL,'1'),(187,'LC','Saint Lucia','2017-05-19 13:44:59',NULL,NULL,'1'),(188,'VC','Saint Vincent and the Grenadines','2017-05-19 13:44:59',NULL,NULL,'1'),(189,'WS','Samoa','2017-05-19 13:44:59',NULL,NULL,'1'),(190,'SM','San Marino','2017-05-19 13:44:59',NULL,NULL,'1'),(191,'ST','Sao Tome and Principe','2017-05-19 13:44:59',NULL,NULL,'1'),(192,'SA','Saudi Arabia','2017-05-19 13:44:59',NULL,NULL,'1'),(193,'SN','Senegal','2017-05-19 13:44:59',NULL,NULL,'1'),(194,'RS','Serbia','2017-05-19 13:44:59',NULL,NULL,'1'),(195,'SC','Seychelles','2017-05-19 13:44:59',NULL,NULL,'1'),(196,'SL','Sierra Leone','2017-05-19 13:44:59',NULL,NULL,'1'),(197,'SG','Singapore','2017-05-19 13:44:59',NULL,NULL,'1'),(198,'SK','Slovakia','2017-05-19 13:44:59',NULL,NULL,'1'),(199,'SI','Slovenia','2017-05-19 13:44:59',NULL,NULL,'1'),(200,'SB','Solomon Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(201,'SO','Somalia','2017-05-19 13:44:59',NULL,NULL,'1'),(202,'ZA','South Africa','2017-05-19 13:44:59',NULL,NULL,'1'),(203,'GS','South Georgia South Sandwich Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(204,'ES','Spain','2017-05-19 13:44:59',NULL,NULL,'1'),(205,'LK','Sri Lanka','2017-05-19 13:44:59',NULL,NULL,'1'),(206,'SH','St. Helena','2017-05-19 13:44:59',NULL,NULL,'1'),(207,'PM','St. Pierre and Miquelon','2017-05-19 13:44:59',NULL,NULL,'1'),(208,'SD','Sudan','2017-05-19 13:44:59',NULL,NULL,'1'),(209,'SR','Suriname','2017-05-19 13:44:59',NULL,NULL,'1'),(210,'SJ','Svalbard and Jan Mayen Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(211,'SZ','Swaziland','2017-05-19 13:44:59',NULL,NULL,'1'),(212,'SE','Sweden','2017-05-19 13:44:59',NULL,NULL,'1'),(213,'CH','Switzerland','2017-05-19 13:44:59',NULL,NULL,'1'),(214,'SY','Syrian Arab Republic','2017-05-19 13:44:59',NULL,NULL,'1'),(215,'TW','Taiwan','2017-05-19 13:44:59',NULL,NULL,'1'),(216,'TJ','Tajikistan','2017-05-19 13:44:59',NULL,NULL,'1'),(217,'TZ','Tanzania, United Republic of','2017-05-19 13:44:59',NULL,NULL,'1'),(218,'TH','Thailand','2017-05-19 13:44:59',NULL,NULL,'1'),(219,'TG','Togo','2017-05-19 13:44:59',NULL,NULL,'1'),(220,'TK','Tokelau','2017-05-19 13:44:59',NULL,NULL,'1'),(221,'TO','Tonga','2017-05-19 13:44:59',NULL,NULL,'1'),(222,'TT','Trinidad and Tobago','2017-05-19 13:44:59',NULL,NULL,'1'),(223,'TN','Tunisia','2017-05-19 13:44:59',NULL,NULL,'1'),(224,'TR','Turkey','2017-05-19 13:44:59',NULL,NULL,'1'),(225,'TM','Turkmenistan','2017-05-19 13:44:59',NULL,NULL,'1'),(226,'TC','Turks and Caicos Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(227,'TV','Tuvalu','2017-05-19 13:44:59',NULL,NULL,'1'),(228,'UG','Uganda','2017-05-19 13:44:59',NULL,NULL,'1'),(229,'UA','Ukraine','2017-05-19 13:44:59',NULL,NULL,'1'),(230,'AE','United Arab Emirates','2017-05-19 13:44:59',NULL,NULL,'1'),(231,'GB','United Kingdom','2017-05-19 13:44:59',NULL,NULL,'1'),(232,'US','United States','2017-05-19 13:44:59',NULL,NULL,'1'),(233,'UM','United States minor outlying islands','2017-05-19 13:44:59',NULL,NULL,'1'),(234,'UY','Uruguay','2017-05-19 13:44:59',NULL,NULL,'1'),(235,'UZ','Uzbekistan','2017-05-19 13:44:59',NULL,NULL,'1'),(236,'VU','Vanuatu','2017-05-19 13:44:59',NULL,NULL,'1'),(237,'VA','Vatican City State','2017-05-19 13:44:59',NULL,NULL,'1'),(238,'VE','Venezuela','2017-05-19 13:44:59',NULL,NULL,'1'),(239,'VN','Vietnam','2017-05-19 13:44:59',NULL,NULL,'1'),(240,'VG','Virgin Islands (British)','2017-05-19 13:44:59',NULL,NULL,'1'),(241,'VI','Virgin Islands (U.S.)','2017-05-19 13:44:59',NULL,NULL,'1'),(242,'WF','Wallis and Futuna Islands','2017-05-19 13:44:59',NULL,NULL,'1'),(243,'EH','Western Sahara','2017-05-19 13:44:59',NULL,NULL,'1'),(244,'YE','Yemen','2017-05-19 13:44:59',NULL,NULL,'1'),(245,'ZR','Zaire','2017-05-19 13:44:59',NULL,NULL,'1'),(246,'ZM','Zambia','2017-05-19 13:44:59',NULL,NULL,'1'),(247,'ZW','Zimbabwe','2017-05-19 13:44:59',NULL,NULL,'1');
/*!40000 ALTER TABLE `zz_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_default_user_type`
--

DROP TABLE IF EXISTS `zz_default_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_default_user_type` (
  `idzz_default_user_type` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT 'The name of the User Type. This column must be EXACTLY equals to the column name of the table user_type.',
  PRIMARY KEY (`idzz_default_user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_default_user_type`
--

LOCK TABLES `zz_default_user_type` WRITE;
/*!40000 ALTER TABLE `zz_default_user_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `zz_default_user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zz_state`
--

DROP TABLE IF EXISTS `zz_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zz_state` (
  `idzz_state` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `zz_country_idzz_country` int(11) NOT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_update` timestamp NULL DEFAULT NULL,
  `date_delete` timestamp NULL DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Indicates the status.:\n1-Active\n2-Blocked\n3-Deleted',
  PRIMARY KEY (`idzz_state`),
  KEY `fk_zz_state_zz_country1_idx` (`zz_country_idzz_country`),
  CONSTRAINT `fk_zz_state_zz_country1` FOREIGN KEY (`zz_country_idzz_country`) REFERENCES `zz_country` (`idzz_country`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zz_state`
--

LOCK TABLES `zz_state` WRITE;
/*!40000 ALTER TABLE `zz_state` DISABLE KEYS */;
INSERT INTO `zz_state` VALUES (1,'RN',1,'2017-05-17 11:30:14',NULL,NULL,1);
/*!40000 ALTER TABLE `zz_state` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-01  8:10:59
