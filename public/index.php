<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Oct 14, 2016
 */
require_once '../init_autoload.php';
// require_once 'default_helper.php';
require_once '../libraries/default_helper.php';
require_once '../libraries/date_time_helper.php';
$url = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
if(stripos($_SERVER['SERVER_SIGNATURE'], "443")) {
    $protocol = "https://";
} else {
    $protocol = "http://";
    
}
$url_base = $protocol.substr($url, 0, -16);

define("URL_BASE", $url_base . "public/");


require_once '../module/Application/controller/GenericController.php';
        const DS = DIRECTORY_SEPARATOR;
        const PS = PATH_SEPARATOR;

define("ORIGEN", __DIR__);

new GenericController();
?>

