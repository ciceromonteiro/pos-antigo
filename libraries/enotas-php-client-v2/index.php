<?php
ini_set('display_errors', 1);
header('Content-Type: application/json');

require('src/eNotasGW.php');

use eNotasGW\Api\Exceptions as Exceptions;

eNotasGW::configure(array(
	'apiKey' => 'NjIwMmE0OTItOTdkMS00NGE0LWI1NTItNmU4YThmMzcwNDAw'
));

$idEmpresa = 'B2E5F6DD-AACC-4887-8D25-BFD3D2390400';

try
{
	$result = eNotasGW::$NFeConsumidorApi->emitir($idEmpresa, array(
// identificador único da requisição de emissão de nota fiscal 
// (normalmente será preenchido com o id único do registro no sistema de origem)
		'id' => '108333',
		'ambienteEmissao' => 'Producao', //'Producao' ou 'Homologacao'
		'pedido' => array(
			'presencaConsumidor' => 'OperacaoPresencial',
			'pagamento' => array(
				'tipo' => 'PagamentoAVista',
				'formas' => array(
					array(
						'tipo' => 'Dinheiro',
						'valor' => 3.98
					)
				)
			)
		),
		'itens' => array(
			array(
				'cfop' => '5102',
				'codigo' => '128',
				'descricao' => 'ESCONDIDINHO DE CAMARAO',
				'ncm' => '16023230',
				'quantidade' => 2,
				'unidadeMedida' => 'UN',
				'valorUnitario' => 1.99,
				'impostos' => array(
					'icms' => array (
						'situacaoTributaria' => '102',
						'origem' => 0, //0 - Nacional
						'aliquota' => 18
					)
				)
			)
		),
		'informacoesAdicionais' => 'Documento emitido por ME ou EPP optante pelo Simples Nacional. Não gera direito a crédito fiscal de IPI.'
	));

	echo 'Sucesso!';
	echo json_encode($result, JSON_PRETTY_PRINT);
}
catch(Exceptions\invalidApiKeyException $ex) {
	echo 'Erro de autenticação: </br></br>';
	echo $ex->getMessage();
}
catch(Exceptions\unauthorizedException $ex) {
	echo 'Acesso negado: </br></br>';
	echo $ex->getMessage();
}
catch(Exceptions\apiException $ex) {
	echo 'Erro de validação: </br></br>';
	echo $ex->getMessage();
}
catch(Exceptions\requestException $ex) {
	echo 'Erro na requisição web: </br></br>';

	echo 'Requested url: ' . $ex->requestedUrl;
	echo '</br>';
	echo 'Response Code: ' . $ex->getCode();
	echo '</br>';
	echo 'Message: ' . $ex->getMessage();
	echo '</br>';
	echo 'Response Body: ' . $ex->responseBody;
}