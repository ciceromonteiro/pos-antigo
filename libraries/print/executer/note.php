<?php

/**
 * @author Servulo Fonseca <servulofonseca@gmail.com>
 * @version 1.0.0
 * @abstract file created in date Jun 16, 2017
 */
/* Example print-outs using the older bit image print command */



require __DIR__ . '../../vendor/autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;

for ($i = 1; $i < 10; $i++):
    $teste = 0;
    try {
        $connector = new FilePrintConnector("com$i");
    } catch (Exception $exc) {
        $teste = 1;
    }

    if ($teste == 0) {
        $printer = new Printer($connector);

        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?> <root>";
        $xml .= $argv[1];
        $xml .= "</root>";
     
        $doc = new DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->loadXML($xml);
        $k = 0;


        try {
            $tux = EscposImage::load(__DIR__ ."/img/logo1.png", false);
            $printer->bitImage($tux, Printer::JUSTIFY_CENTER);
            $justification = array(
                "JUSTIFY_LEFT" => Printer::JUSTIFY_LEFT,
                "JUSTIFY_CENTER" => Printer::JUSTIFY_CENTER,
                "JUSTIFY_RIGHT" => Printer::JUSTIFY_RIGHT);
            while (is_object($finance = $doc->getElementsByTagName("body")->item($k))) {

                foreach ($finance->childNodes as $nodename) {
                    if ($nodename->nodeName == 'dado1') {
                        foreach ($nodename->childNodes as $subNodes) {
                            $printer->setJustification($justification[$subNodes->nodeName]);
                            $printer->text($subNodes->nodeValue . "\n");
                        }
                    } else if ($nodename->nodeName == 'code1') {
                        $printer->setBarcodeHeight(80);
                        $printer->setBarcodeTextPosition(Printer::BARCODE_TEXT_BELOW);
                        $printer->barcode($nodename->nodeValue);
                    } else {
                        $printer->text($nodename->nodeValue . "\n");
                    }
                }

                $k++;
            }
            // $printer->text($argv[1] . "\n\n");
        } catch (Exception $e) {
            /* Images not supported on your PHP, or image file not found */
            $printer->text($e->getMessage() . "\n");
        }

        //$printer->cut();
         $printer->pulse();

        $printer->close();

        /*
         * <body>
          <dado1>
          <JUSTIFY_LEFT>teste</JUSTIFY_LEFT>
          </dado1>
          <dado1>
          <JUSTIFY_LEFT>teste</JUSTIFY_LEFT>
          </dado1>
          <line>____________________________</line>
          </body>
          <body>
          <dado1>
          <JUSTIFY_LEFT>teste</JUSTIFY_LEFT>
          </dado1>
          <dado1>
          <JUSTIFY_CENTER>teste</JUSTIFY_CENTER>
          </dado1>
          <code1>12345</code1>
          </body>
         */
    }

endfor;
