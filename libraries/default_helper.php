<?php
// useful functions

function printFloat($value) {
    return number_format($value, 2, '.', '');
}

// generates strong random id. returns int.
function crypto_rand_secure($min, $max) {
    $range = $max - $min;
    if ($range < 0)
        return $min; // not so random...
    $log = log($range, 2);
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd >= $range);
    return $min + $rnd;
}

function utf8ize($d) {
    if (is_array($d)) {
        foreach ($d as $k => $v) {
            $d[$k] = utf8ize($v);
        }
    } else if (is_string ($d)) {
        return utf8_encode($d);
    }
    return $d;
}

// generates unique token. returns string.
function get_token($length = 22, $include_alpha = false, $upper = true) {
    $token = "";
    $codeAlphabet = "123456789";
    if ($include_alpha == true) {
        $codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
    }
    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, strlen($codeAlphabet))];
    }
    if ($upper)
        $token = strtoupper($token);
    return $token;
}

function _dump($var, $die=false) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
    if ($die) {
        die();
    }
}

function get_http_codes() {
    return array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
        601 => 'Custom Status 1',
        602 => 'Custom Status 2',
        603 => 'Custom Status 3',
        604 => 'Custom Status 4',
        605 => 'Custom Status 5',
    );
}

function returnJson($httpStatus, $items=array(), $extra=array()) {
    $http_codes = get_http_codes();
    $return = array(
        'status' => array(
            'code' => $httpStatus,
            'status' => $http_codes[$httpStatus]
        ),
        'items' => $items,
        'extra' => $extra
    );

    die(json_encode($return, JSON_PRETTY_PRINT));
}

function getUniqueMachineID($salt = "") {
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $temp = sys_get_temp_dir().DIRECTORY_SEPARATOR."diskpartscript.txt";
        if(!file_exists($temp) && !is_file($temp)) file_put_contents($temp, "select disk 0\ndetail disk");
        $output = shell_exec("diskpart /s ".$temp);
        $lines = explode("\n",$output);
        $result = array_filter($lines,function($line) {
            return stripos($line,"ID:")!==false;
        });
        if(count($result)>0) {
            $result = array_shift(array_values($result));
            $result = explode(":",$result);
            $result = trim(end($result));       
        } else $result = $output;       
    } else {
        $result = shell_exec("blkid -o value -s UUID");  
        if(stripos($result,"blkid")!==false) {
            $result = $_SERVER['HTTP_HOST'];
        }
    }   
    return md5($salt.md5($result));
}

function format_money($value, $symbol = 'R$ ') {
    return $symbol . number_format($value, 2, ',', '.');
}

function printOrderStatus($status) {
    // 0- Open, 
    // 1- Waiting payment confirmation, 
    // 2- Paid, waiting shippiment, 
    // 3- Shipped, waiting arrival at customers house, 
    // 4- Finished

    switch ($status) {
        case 0:
            return "Em Aberto";
            break;
        case 1:
            return "Esperando confirmação de pagamento";
            break;
        case 2:
            return "Pago";
            break;
        case 3:
            return "Enviado, aguardando ser entregue";
            break;
        case 4:
            return "Concluido";
            break;
    }
}

function loadFormData() {
    
}













