// Mask
$('.money').mask('000.000.000.000.000,00', {reverse: true});
$('.percent').mask('##0,00%', {reverse: true});
$('.number').mask('000000000#');
$('.date-input').mask('00/00/0000');
  
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});

if($("input[name='all']").val()){
    $("input[name='all']")[0].checked = false;
}

function reload_table(nameTable){
    nameTable.ajax.reload(null,false);
}

function resetForm(idForm){
    $(idForm)[0].reset();
}
function notification(action){
    if(action == true){
        var title = "Feito";
        var text = "Operação concluida com sucesso.";
        var icon = "glyphicon glyphicon-plus";
        var type = "success";
    } else {
        var title = "Ops...";
        var text = "Encontramos um erro, tente novamente";
        var icon = "glyphicon glyphicon-minus";
        var type = "error";
    }
    var notice = new PNotify({
        title: title,
        text: text,
        icon: icon,
        type: type,
        confirm: {
            confirm: false
        },
        buttons: {
            closer: false,
            sticker: false
        }
    });
    PNotify.prototype.options.styling = "bootstrap3";
    notice.get().click(function() {
        notice.remove();
    }); 
}

function deleteItens(ajaxUrl, array_deletes, table){
    PNotify.prototype.options.styling = "bootstrap3";
    (new PNotify({
        title: "Tem certeza?",
        text: "Tem certeza que deseja deletar este item?",
        icon: "glyphicon glyphicon-minus",
        hide: false,
        confirm: {
            confirm: true
        },
        buttons: {
            closer: false,
            sticker: false
        },
        history: {
            history: false
        },
        addclass: 'stack-modal',
        stack: {'dir1': 'down', 'dir2': 'right', 'modal': true}
    }))
    .get().on('pnotify.confirm', function(){
        $.ajax({
            url : my_url + ajaxUrl,
            type: "POST",
            dataType: 'JSON',
            data : array_deletes,
            success: function(data, textStatus, jqXHR){
                if (data.result == 'success'){
                    notification(true);
                } else {
                    notification(false);
                    console.log(data.message);
                }
                reload_table(table);
            },
            error: function (jqXHR, textStatus, errorThrown){
                notification(false);
                console.log(jqXHR.responseText);
            }
        });
        $('.ui-pnotify-modal-overlay').remove();
    })
    .on('pnotify.cancel', function(){
        $('.ui-pnotify-modal-overlay').remove();
    });
}

function markAll(idTable){
    var checkboxArray = $("#"+idTable+" tbody tr").find('input:checkbox');
    var isChecked = $("#"+idTable+" thead tr").find('input:checkbox');
    if(isChecked[0].checked == false){
        for(i = 0; i < checkboxArray.length; i++){
            $("#"+idTable+" tbody").find('tr').removeClass('hover-select');
            checkboxArray[i].checked = false;
        }
        $("#"+idTable+"_update").attr('disabled', true);
        $("#"+idTable+"_delete").attr('disabled', true);            
    } else {
        for(i = 0; i < checkboxArray.length; i++){
            $("#"+idTable+" tbody").find('tr').addClass('hover-select');
            checkboxArray[i].checked = true;
        }
        $("#"+idTable+"_delete").attr('disabled', false);
    }
}

function checkMark(idTable){
    var count = 0;
    var checkboxArray = $(idTable+" tbody tr").find('input:checkbox');
    for(i = 0; i < checkboxArray.length; i++){
        if(checkboxArray[i].checked == true){
            count += 1;
        }
    }
    if(count == 0){
        $("input[name='all']")[0].checked = false;
    }
    return count;
}