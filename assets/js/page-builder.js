/**
 * @author Deyvison Rodrigo B. Estevam <deyvisonestevam@gmail.com>
 * @version 1.0.0
 * 
 * @type type
 * 
 */

$(document).ready(function() {
	$("*[class*='edit-']").click(function(e) {
		showModalEdit(this, e);
	});

	$("#editionMode").click(function() {
		getInEditionMode(this);
	});

	$("#saveEdition").click(function() {
		saveChanges();
	});

	// loadPage();
});

/**
 * Show modal form to edit element.
 * 
 * @param elem
 * @param e
 * @returns
 */
function showModalEdit(elem, e) {
	checkContentModal();
	e.preventDefault();

	var msg = "";
	var classeId = getClassId(elem);
	var baseUrl = getUrlModule() + "/pagebuilder/getModal/formEditElement";
	var element;
	var user = 34;
	var pos = 57;

	// check 'edit mode'
	if (!$("#editionMode").hasClass('active')) {
		return false;
	}

	element = {
		name : "dataJson",
		value : '{"user":"' + user + '", "pos":"' + pos + '", "tagName":"'
				+ elem.tagName + '", "classId":"' + classeId
				+ '", "pageName":"' + pageName + '" }',
	};

	var data = [ element ];

	msg = requestAjax(baseUrl, data);

	openModalPageBuilder(msg);
}

/**
 * 
 * @param elem
 * @returns
 */
function getInEditionMode(elem) {
	checkContentModal();

	var msg = "";
	var urlModule = getUrlModule() + "/pagebuilder/activateEditionMode";
	var user = "34";
	var pos = "57";

	// check 'edit mode'
	if ($(elem).hasClass('active')) {
		return false;
	} else {
		$(elem).addClass('active');
		$("#saveEdition").show('slow');
	}

	var element = {
		name : "dataJson",
		value : '{"user":"' + user + '", "pos":"' + pos + '", "pageName":"'
				+ pageName + '"}',
	};

	var data = [ element ];

	msg = requestAjax(urlModule, data);

	var jsonInfo = JSON.parse(msg);

	if (jsonInfo.exist == 1) {

		if (builderPage(jsonInfo.json)) {

			var urlModule = getUrlModule()
					+ "/pagebuilder/getModal/comfirmRestore";
			var htmlModal = requestAjax(urlModule, "");
			openModalPageBuilder(htmlModal);

		}
	}
}

/**
 * 
 * @param {Element} form
 * @param {Event} e
 * @returns {Boolean}
 */
function changeElement(form, e, user, pos) {
	e.preventDefault();

	var msg = "";
	var baseUrl = getUrlModule() + "/pagebuilder/editElement";
	var data = $(form).serializeArray();
	var element;
	element = {
		name : "dataJson",
		value : '{"user":"' + user + '", "pos":"' + pos + '", "pageName":"'
				+ pageName + '"}',
	};

	data.push(element);

	msg = requestAjax(baseUrl, data);

	builderPage(msg);

	$(".closeModalEdit").click();

	return true;
}

/**
 * 
 * @param STRING
 *            html -> Full HTML page
 * @returns STRING
 * 
 */
function getBodyAjax(html) {

	var init = html.indexOf("<body>") + 6;

	/**
	 * HTML without tag BODY, this is ivalid!
	 */
	if (init < 0) {
		return '';
	}

	var length = html.indexOf("</body>") - init;

	html = html.substr(init, length).trim();

	return html;
}

/**
 * Return URL module
 * 
 * @returns STRING
 */
function getUrlModule() {
	var strHref = window.location.href;
	var finalPoint = strHref.split("/", 5).join("/").length;

	return strHref.substr(0, finalPoint);
}

function addAttr(className, attr, value) {
	switch (attr) {
	case "innerHTML":
		$("." + className).html(value);
		break;
	case "display":
		$("." + className).css(attr, value);
		break;
	case "value":
		if (className.indexOf("button") >= 0) {
			$("." + className).html(value);
		} else {
			$("." + className).val(value);
		}
		break;
	default:
		$("." + className).attr(attr, value);
		break;
	}
}

function getClassId(element) {
	var classe = element.className;
	var initClasse = classe.indexOf("edit-");
	var lenClasse = classe.lenght;
	var classeId = classe.substring(initClasse, lenClasse);

	return classeId;
}

function loadPage() {

	var user = 34;
	var pos = 57;
	var baseUrl = getUrlModule() + "/pagebuilder/loadPage";	
	var element = {
			name : "dataJson",
			value : '{"user":"' + user + '", "pos":"' + pos + '","pageName":"'
					+ pageName + '"}',
		};

	var data = [ element ];
	
	var msg = requestAjax(baseUrl, data);

	builderPage(msg);

	return true;
}

function builderPage(msg) {

	var jsonContent = JSON.parse(msg);

	if (jsonContent.length != 0 && jsonContent.length != 'undefined') {
		var k, j;

		for (k in jsonContent) {
			var elements2 = jsonContent[k];
			for (j in elements2) {
				if (k.toString().indexOf("edit-") >= 0) {
					addAttr(k, j, elements2[j]);
				}
			}
		}
	} else {
		return false;
	}

	return true;
}

function saveChanges() {
	checkContentModal();

	var baseUrl = getUrlModule() + "/pagebuilder/saveChanges";
	var element;
	var user = 34;
	var pos = 57;

	element = {
		name : "dataJson",
		value : '{"user":"' + user + '", "pos":"' + pos + '","pageName":"'
				+ pageName + '"}',
	};

	var data = [ element ];

	var msg = requestAjax(baseUrl, data);
	var returnJson = JSON.parse(msg);

	if (returnJson.error < 0) {

		showError(returnJson.jsonError);

	} else {

		builderPage(returnJson.dataJson);

		showError(returnJson.jsonError);

		$("#editionMode").removeClass('active');
		$("#saveEdition").hide('slow');

	}
}

function cancelRecover() {

	checkContentModal();

	$("#modalPageBuilder").html("");

	var user = 34;
	var pos = 57;
	var baseUrl = getUrlModule() + "/pagebuilder/eraserPage";
	var element = {
		name : "dataJson",
		value : '{"user":"' + user + '", "pos":"' + pos + '","pageName":"'
				+ pageName + '"}',
	};
	var data = [ element ];

	var msg = requestAjax(baseUrl, data);
	var returnJson = JSON.parse(msg);

	if (returnJson.error < 0) {
		$("#modalPageBuilder").load();
	}
}

function requestAjax(url, data) {
	var this_return = "";

	$.ajax({

		url : url,
		method : "POST",
		async : false,
		data : data,

	}).done(function(msg) {

		this_return = msg;

	}).fail(function(jqXHR, textStatus) {

		alert("Request failed: " + textStatus);
		this_return = "Request failed: " + textStatus;

	});

	return this_return;
}

function showError(returnJson) {
	checkContentModal();

	var urlModule = getUrlModule() + "/pagebuilder/getModal/error";

	var data = new Array();
	data.push({
		name : "titleError",
		value : returnJson.titleError
	});
	data.push({
		name : "msgError",
		value : returnJson.msgError
	});

	var msg = requestAjax(urlModule, data);

	openModalPageBuilder(msg);
}

/**
 * Add container to put modals
 * 
 * @returns
 */
function checkContentModal() {
	var elementExists = document.getElementById("modalPageBuilder");

	if (elementExists == null) {
		$("body")
				.append(
						'<div class="modal" id="modalPageBuilder" role="dialog"></div>');
	}
}

/**
 * Open modal pagebuilder
 * 
 * @param msg
 * @returns
 */
function openModalPageBuilder(msg) {
	// Puts data in modal container
	$("#modalPageBuilder").html(msg);

	// Show modal button
	$("#openmodal").click();
}

/**
 * Clean content of modal
 * 
 * @returns
 */
function cleanModalPageBuilder() {
	$("#modalPageBuilder").html("");
}