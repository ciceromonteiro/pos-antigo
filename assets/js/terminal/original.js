$("#choose_user").modal({show : true});

    var count_products = 0;
    var payment_billets = 0;
    var count_payment_billets = 0;
    var current_user = $("#user").val();

    $("#find_product_filter input").val("teste");

    var table_find_produt = $('#find_product').DataTable( {
        "ajax": {"url": my_url+"Terminal/Terminal/getAllProducts"},
        /*"dom": 'frtip',*/
        "columns": [
            { "data": "id" },
            { "data": "description" },
            { "data": "stock" }
        ],
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [-1] }
        ],
        "ordering": false,
        "info":     false,
        "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
    });

    $(".button_lag_nytt_gavekort").click(function(){
        $("#modal_lag_nytt_gavekort").modal({show:true});
    });

    $(".button_print_receipt_copy").click(function(){
        $("#modal_print_receipt_copy").modal({show:true});
    });

    
    $("#lag_nytt_gavekort_insert").click(function(){
        serial = $("#lag_nytt_gavekort_serial").val();
        amount = $("#lag_nytt_gavekort_amount").val();
        order_id = $("#order").val();
//alert(my_url+"Terminal/terminal/generateGiftcard/"+serial+"/"+amount);
        var baseUrl = my_url+"Terminal/terminal/generateGiftcard/"+order_id+"/"+serial+"/"+amount;
        $.ajax({
            url: baseUrl,
            async: false,

        }).done(function (json) {
            alert("Success!");

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });

        $("#modal_lag_nytt_gavekort").modal('hide');
        $("#lag_nytt_gavekort_serial").val("");
        $("#lag_nytt_gavekort_amount").val("");
    });

    $("#withdrawal_insert").click(function(){
        user_id = $("#user").val();
        amount = $("#withdrawal_amount").val();
        description = $("#withdrawal_description").val();

        var baseUrl = my_url+"Terminal/terminal/generateWithdraw/0/"+user_id+"/"+amount+"/"+description;
        $.ajax({
            url: baseUrl,
            async: false,

        }).done(function (json) {
            alert("Success!");

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });

        $("#modal_withdrawal").modal('hide');
    });
    
    $("#deposit_insert").click(function(){
        user_id = $("#user").val();
        amount = $("#deposit_amount").val();
        description = $("#deposit_description").val();

        var baseUrl = my_url+"Terminal/terminal/generateWithdraw/1/"+user_id+"/"+amount+"/"+description;
        $.ajax({
            url: baseUrl,
            async: false,

        }).done(function (json) {
            alert("Success!");

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });

        $("#modal_deposit").modal('hide');
    });

    $("#button_get_customer_orders").click(function(){
        customer_id = $("#customer_orders_id").val();

        var table_customer_orders = $('#table_customer_orders').DataTable( {
            "ajax": {"url": my_url+"Terminal/Terminal/getOrder/0/1/"+customer_id+"/0"},
            "columns": [
                { "data": "id" },
                { "data": "date_create" },
                { "data": "order_identify" },
                { "data": "user_name" },
                { "data": "merket" },
                { "data": "extra_info" },
                { "data": "button_show_order_lines" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "retrieve": true,
            "paging": true,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        reload_table(table_customer_orders);
    });

    $(".button_article_statistics").click(function(){
        var conf = $("#article_statistics_id").val();
        if(conf){
            product = $("#article_statistics_id").val(product);
        }else{
            product = $("#article").val();
            $("#article_statistics_id").val(product);
        }       
//alert(my_url+"Terminal/Terminal/getSoldProducts/"+product);        
        var table_article_statistics = $('#table_article_statistics').DataTable( {
            "ajax": {"url": my_url+"Terminal/Terminal/getSoldProducts/"+product},
            "columns": [
                { "data": "id" },
                { "data": "date_create" },
                { "data": "product_number" },
                { "data": "customer_name" },
                { "data": "variety" },
                { "data": "qty" },
                { "data": "price" },
                { "data": "discount" },
                { "data": "amount_inc_tax" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "retrieve": true,
            "paging": true,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        reload_table(table_article_statistics);

        $("#modal_article_statistics").modal({show:true});
    });

    $(".button_customer_statistics").click(function(){
        var conf = $("#customer_statistics_id").val();
        if(conf){
            customer = $("#customer_statistics_id").val(customer);
        }else{
            customer = $("#customer").val();
            $("#customer_statistics_id").val(customer);
        }       
//alert(my_url+"Terminal/Terminal/getCustomerProducts/"+customer);        
        var table_customer_statistics = $('#table_customer_statistics').DataTable( {
            "ajax": {"url": my_url+"Terminal/Terminal/getCustomerProducts/"+customer},
            "columns": [
                { "data": "id" },
                { "data": "date_create" },
                { "data": "product_number" },
                { "data": "description" },
                { "data": "qty" },
                { "data": "price" },
                { "data": "discount" },
                { "data": "amount_inc_tax" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "retrieve": true,
            "paging": true,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        reload_table(table_customer_statistics);

        $("#modal_customer_statistics").modal({show:true});
    });

    $(".button_customer_orders").click(function(){
        var conf = $("#customer_orders_id").val();
        if(conf){
            customer = $("#customer_orders_id").val(customer);
        }else{
            customer = $("#customer").val();
            $("#customer_orders_id").val(customer);
        }       
        var table_customer_orders = $('#table_customer_orders').DataTable( {
            "ajax": {"url": my_url+"Terminal/Terminal/getOrder/0/1/"+customer+"/0"},
            "columns": [
                { "data": "id" },
                { "data": "date_create" },
                { "data": "order_identify" },
                { "data": "user_name" },
                { "data": "merket" },
                { "data": "extra_info" },
                { "data": "button_show_order_lines" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "retrieve": true,
            "paging": true,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        reload_table(table_customer_orders);

        $("#modal_customer_orders").modal({show:true});
    });

    $("#select_date_chosen_to_print_receipt").click(function(e){
        e.preventDefault();
        var chosen_date = $("#date_chosen_to_print_receipt").val();
        chosen_date = chosen_date.split("/");
        chosen_date = chosen_date[1]+"."+chosen_date[0]+"."+chosen_date[2];
        var table_choose_order_to_print_receipt = $('#choose_order_to_print_receipt').DataTable( {
            "ajax": {"url": my_url+"Terminal/Terminal/getOrder/0/1/0/0/0/"+chosen_date},
            "columns": [
                { "data": "merket" },
                { "data": "order_identify" },
                { "data": "date_create" },
                { "data": "customer_name" },
                { "data": "user_name" },
                { "data": "total_order" },
                { "data": "button_print" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "retrieve": true,
            "paging": true,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        reload_table(table_choose_order_to_print_receipt);
        $("#modal_choose_by_date").modal('hide');
        $("#modal_another_receipt").modal({show:true});
    });                    


    $("#choose_another_receipt").click(function(e){
        e.preventDefault();
        var current_order = $("#order").val();
        var current_user = $("#user").val();     
        var table_choose_order_to_print_receipt = $('#choose_order_to_print_receipt').DataTable( {
            "ajax": {"url": my_url+"Terminal/Terminal/getOrder/0/1/0/0/0"},
            "columns": [
                { "data": "merket" },
                { "data": "order_identify" },
                { "data": "date_create" },
                { "data": "customer_name" },
                { "data": "user_name" },
                { "data": "total_order" },
                { "data": "button_print" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "retrieve": true,
            "paging": true,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        reload_table(table_choose_order_to_print_receipt);
        $("#modal_another_receipt").modal({show:true});
    });  

    $("#choose_by_date").click(function(e){
        e.preventDefault();
        
        $("#modal_choose_by_date").modal({show:true});
    });

    $(".button_get_parked_order").click(function(e){
        e.preventDefault();
        var current_order = $("#order").val();
        var current_user = $("#user").val();     
        var table_find_parked_order = $('#find_parked_order').DataTable( {
            "ajax": {"url": my_url+"Terminal/Terminal/getAllParkedOrders/"+current_order+"/"+current_user},
            "columns": [
                { "data": "order_identify" },
                { "data": "customer" },
                { "data": "employee" },
                { "data": "date_create" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "retrieve": true,
            "paging": true,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        reload_table(table_find_parked_order);
        $("#choose_parked_order").modal({show:true});
    });

    $("#split_back").click(function(){
        conf = $(".split_new_order_table tbody").html();
        if(conf==""){
            $("#modal_split_order").modal('hide');
        }else{
            alert("Set the new order as Parked or Paid.");
        }
    });
    
    $("#split_park").click(function(){
        conf = $(".split_new_order_table tbody").html();

        order_id = $("#split_new_order").val();
        var baseUrl = my_url+"Terminal/terminal/parkOrder/"+order_id;

        $.ajax({

            url: baseUrl,
            method: "POST",
            async: false,

        }).done(function (json) {
            $("#split_new_order").val("");
            $(".split_new_order_table").html("");

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        }); 
    });
    
    $("#split_pay").click(function(){
        var order_id = $("#split_new_order").val();
        var amount_paid = "all";
        var status = 4; //teste
        pay_order(order_id,amount_paid,status,0);

        $(".split_new_order_table tbody").html("");
        $("#split_new_order").val("");
    });

    $(document).on('click', '#find_parked_order tr', function(){
         conf = $(this).find(".order_id").val();
         if(conf){
            load_parked_order(conf);
         }
    });

    $(".button_withdrawal").click(function(){
        user_name = $("#display_user_name").val();                
        $("#withdrawal_user").html(user_name);
        $("#deposit_user").html(user_name);

        $("#modal_withdrawal").modal('hide');
        $("#modal_deposit").modal('hide');
        $("#modal_withdrawal_or_deposit").modal({show:true});
    });
    
    $("#choose_withdraw").click(function(){
        $("#modal_withdrawal_or_deposit").modal('hide');
        $("#modal_withdrawal").modal({show:true});
    });
    
    $("#choose_deposit").click(function(){
        $("#modal_withdrawal_or_deposit").modal('hide');
        $("#modal_deposit").modal({show:true});
    });

    $(".change_split_state").click(function(){
        var state = $(this).attr("state");
        $("#split_state").val(state); 
    });

    $(".button_split_order").click(function(){
        order_id = $("#order").val();
        mount_table_split_order("current",order_id);

        $("#modal_split_order").modal({show:true});
    });

    $(".button_discount").click(function(){
        checked_products = 0;

        $(".check_product").each(function(){
            if($(this).is(":checked")){
                checked_products++;
            }
        });
        
        if(checked_products>0){
            $("#modal_discount").modal({show:true});
        }else{
            alert('Select at least one product');
        }
    });

    $(".button_line_info").click(function(){
        checked_products = 0;

        $(".check_product").each(function(){
            if($(this).is(":checked")){
                checked_products++;
            }
        });
        
        if(checked_products>0){
            $("#modal_line_info").modal({show:true});
        }else{
            alert('Select at least one product');
        }
    });

    $(".button_order_info").click(function(){
        $("#modal_order_info").modal({show:true});
    });

    $(".button_kvittering").click(function(){
        var user_id = $("#user").val();
        var order_id = $("#order").val();
        var baseUrl = my_url+"Terminal/terminal/getLastOrder/"+user_id;

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (idorder) {
            if(idorder){
                print_receipt(idorder,"standard");
            }else{
                alert("No previous orders");
            }
        });
    });

    $("#choose_previous_receipt").click(function(){
        var baseUrl = my_url+"Terminal/terminal/getLastOrder";

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (order_id) {
            print_receipt(order_id,"standard");
        }).fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });

    $("#choose_previous_receipt_a4").click(function(){
        var baseUrl = my_url+"Terminal/terminal/getLastOrder";

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (order_id) {
            print_receipt(order_id,"standard");
        }).fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
    });

    $("#insert_info_order").click(function(){
        var order_id = $("#order").val();
        var info_ = $("#info_order").val();
        var update_type = "order_info";

        var baseUrl = my_url+"Terminal/terminal/updateOrder/"+update_type+"/"+info_+"/"+order_id;

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) {
            
        });
    });

    $("#insert_discount_for_many").click(function(){
        discount_ = $("#discount_for_many").val();
        $(".check_product").each(function(){
            if($(this).is(":checked")){
                order_line_id = $(this).attr("which");
                product_id = $(this).attr("which_product_id");
                $("#edit_discount_"+order_line_id).val(discount_);
                save_edition_attribute("discount",order_line_id,product_id);
            }
        });
        $("#modal_discount").modal('hide');
    });

    $("#insert_info_for_many").click(function(){
        info_ = $("#info_for_many").val();
        $(".check_product").each(function(){
            if($(this).is(":checked")){
                order_line_id = $(this).attr("which");
                product_id = $(this).attr("which_product_id");
                $("#edit_info_"+order_line_id).val(info_);
                save_edition_attribute("info",order_line_id,product_id);
            }
        });
        $("#modal_line_info").modal('hide');
    });

    $(".button_cancel_order").click(function(){
        if(confirm('Would you like to remove all the products?')){
            var order_id = $("#order").val();
            var update_type = "cancel_order";
            var baseUrl = my_url+"Terminal/terminal/updateOrder/"+update_type+"/0/"+order_id;

            $.ajax({

                url: baseUrl,
                async: false,

            }).done(function (json) {
                var elements = JSON.parse(json);
                if(elements.result=="success"){
                    $('.line_product').remove();
                    $('.product').remove();
                    $('#order').val(0);
                    $('#empty_order').val(1);
                    $('#user').val(0);
                    $('#customer').val(0);
                    $("#choose_user").modal({show : true});
                    $('.display_customer_name').val('');
                    $('.display_user_name').val('');
                }
            });

            calculate_order_total();
        }
    });

    $(".button_delete_line").click(function(){
        qty_removed = 0;

        $('.check_product').each(function() {
            order_line_id = $(this).attr("which");           
            if($(this).is(":checked")){
                var baseUrl = my_url+"Terminal/terminal/deleteProduct/"+order_line_id;

                $.ajax({

                    url: baseUrl,
                    async: false,

                }).done(function (html) {
                    $("#line_product_"+order_line_id).remove();
                    $("#product_"+order_line_id).remove();

                    qty_removed++;
                });
            }
        });

        count = count_products - qty_removed;
        $('.line_product').each(function() {
            this_id = $(this).attr("id");
            $('#'+this_id+' count_product').html(count);
            count--;
        });

        calculate_order_total();
        
    });

    $("#button_pay_all").click(function(){
        delete_all_billets();
        billet = parseFloat($(".total_order").html());
        add_billet(billet);
    });

    $("#button_clear").click(function(){
        delete_all_billets();
    });

    $(".button_print_temp_receipt").click(function(){
        order_id = $("#order").val();
        print_receipt(order_id,"standard");
    });
    
    $(".new_product").click(function(){
        reload_table(table_find_produt);
        if($(".display_user_name").val()==""){
            $("#choose_user").modal({show : true});
        }else if($(".display_customer_name").val()==""){
            $("#choose_customer").modal({show : true});
        }else{
            $("#include_new_product").modal({show:true});
            $("#find_product").focus();
        }
    });

    $(".change_customer").click(function(){
        if($(".display_user_name").val()==""){
            $("#choose_user").modal({show : true});
        }else{
            $("#choose_customer").modal({show : true});
        }
    });
    
    $(".change_user").click(function(){
        $("#choose_user").modal({show : true});
    });
   
    $(".close-dialog").click(function(){
        $(".modal-dialog").hide();
    });
    
    //$('select').select2();

    $("#qty_plus").click(function(){
        qty = parseInt($("#include_new_product_qty").val());
        qty++;
        $("#include_new_product_qty").val(qty);
    });

    $("#qty_minus").click(function(){        
        qty = parseInt($("#include_new_product_qty").val());
        if(qty>1){
            qty--;
            $("#include_new_product_qty").val(qty);
        }
    });

    $(".button_credit_last_sale").click(function(){
        var baseUrl = my_url+"Terminal/terminal/getLastOrder";

        $.ajax({

            url: baseUrl,
            method: "POST",
            async: false,

        }).done(function (order_id) {
            print_receipt(order_id,"credit");
        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });
    });

    $(".button_pay").click(function(){
        if($(".display_customer_name").val()!=""){
            $("#pay_order").modal({show:true});
        }
        else{
            alert("Please, select a customer before payment.");
        }
    });

    function clear_order(){
        $("#customer").val("");
        $("#user").val("");
        $("#order").val("");
        $("#article").val("");
        $("#empty_order").val(1);
        $(".display_customer_name").val("");
        $(".display_user_name").val("");
        $('.line_product').remove();
        $('.product').remove();
        calculate_order_total();
        $("#pay_order").modal('hide');
        $("#pay_order_received_amount").html("");
        $("#pay_order_received_total_amount").html("0");
        $("#info_order").val("");
        
        payment_billets = 0;
        count_payment_billets = 0;
    }

    function verifyEnter(event,column,order_line_id){
        if (event.keyCode == 13) {
            $("#label_"+column+"_"+order_line_id).show();
            $("#edit_"+column+"_"+order_line_id).hide();
        }
    }

    function generate_product_line_attribute(order_line_id,column,value,permission,product_id){
        phrase = "";

        if(permission){
            phrase += "<td onclick='open_edition_attribute(\""+column+"\","+order_line_id+")'><span class='label_"+column+" label_product_line_attribute' id='label_"+column+"_"+order_line_id+"' which='"+order_line_id+"'>"+value+"</span><input type='text' class='edit_"+column+" edit_product_line_attribute' id='edit_"+column+"_"+order_line_id+"' which='"+order_line_id+"' value='"+value+"' onfocusout='save_edition_attribute(\""+column+"\","+order_line_id+","+product_id+")' onkeypress='return verifyEnter(event,\""+column+"\","+order_line_id+")'></td>";
        }else{
            phrase += "<td>"+$("#include_new_product_"+column).val()+"</td>";
        }

        return phrase;
    }

    function pay_order(order_id,amount_paid,status,clear){
        var dados = [];

        // customer_id = $("#customer").val();
        // user_id = $("#user").val();
        //order_id = $("#order").val();

        //var products = "";


        // $(".product").each(function(){
        //     products += $(this).val(); //id
        //     products += "|";
        //     products += $(this).attr("qty"); //qty
        //     products += "|";
        //     products += $(this).attr("price"); //value
        //     products += "|";
        //     products += $(this).attr("discount"); //discount
        //     products += "|";
        //     products += $(this).attr("info"); //extra_info

        //     products += "^^^";
        // });

        var baseUrl = my_url+"Terminal/terminal/payOrder/"+order_id+"/"+amount_paid+"/"+status; // +"/"+products

        $.ajax({

            url: baseUrl,
            method: "POST",
            data: dados,
            async: false,

        }).done(function (json) {
            alert("Success!");
            if(clear){
                clear_order();
                $("#choose_user").modal({show : true});
            }
            var xml = "<body> <dado1> <JUSTIFY_CENTER>Helene - Kristine Scavenius Fossum</JUSTIFY_CENTER> </dado1> <dado1> <JUSTIFY_CENTER>Fokus Butikksenter, 1920 Sorumsand</JUSTIFY_CENTER> </dado1> <dado1> <JUSTIFY_CENTER>Org. nr 997 854 632 MVA</JUSTIFY_CENTER> </dado1> <line>________________________________________________</line> </body> <body> <dado1> <JUSTIFY_CENTER>Dato: 06.02.2017 18:13 Ordre nr: 24631</JUSTIFY_CENTER> </dado1> <dado1> <JUSTIFY_CENTER>Kunde: Kontant (1000)</JUSTIFY_CENTER> </dado1> <dado1> <JUSTIFY_CENTER>Var ref: Helene</JUSTIFY_CENTER> </dado1> </body> <body> <line>________________________________________________</line> <dado1> <JUSTIFY_RIGHT>MVA</JUSTIFY_RIGHT> </dado1> <dado1> <JUSTIFY_CENTER>Kunde: Kontant (1000)</JUSTIFY_CENTER> </dado1> <dado1> <JUSTIFY_CENTER>Var ref: Helene</JUSTIFY_CENTER> </dado1> <line>________________________________________________</line> </body> <body> <dado1> <JUSTIFY_LEFT>Beamont 25% 99.00</JUSTIFY_LEFT> </dado1> <dado1> <JUSTIFY_LEFT>(1 x 99,00)</JUSTIFY_LEFT> </dado1> <dado1> <JUSTIFY_LEFT>2002</JUSTIFY_LEFT> </dado1> <line>________________________________________________</line> </body> <body> <dado1> <JUSTIFY_LEFT>Beamont 25% 99.00</JUSTIFY_LEFT> </dado1> <dado1> <JUSTIFY_LEFT>(1 x 99,00)</JUSTIFY_LEFT> </dado1> <dado1> <JUSTIFY_LEFT>2002</JUSTIFY_LEFT> </dado1> <line>________________________________________________</line> </body> <body> <dado1> <JUSTIFY_RIGHT>Sum 99,00</JUSTIFY_RIGHT> </dado1> <dado1> <JUSTIFY_RIGHT>25% MVA 24.75-</JUSTIFY_RIGHT> </dado1> <dado1> <JUSTIFY_RIGHT>Total 123.75</JUSTIFY_RIGHT> </dado1> <dado1> <JUSTIFY_CENTER>Kontant 1 000,00</JUSTIFY_CENTER> </dado1> <dado1> <JUSTIFY_CENTER>Takk for besoket</JUSTIFY_CENTER> </dado1> <dado1> <JUSTIFY_CENTER>Velkommen igjen</JUSTIFY_CENTER> </dado1> <code1>25633</code1> </body>";
            loadPrint(xml);
        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });
    }

    function conf_billets(){
        total_order = parseFloat($(".total_order").html());

        if(payment_billets>=total_order){
            var order_id = $("#order").val();
            var status = 4; //test - por enquanto todas as orders ficam com status 4 (finished)
            pay_order(order_id,total_order,status,1);
        }
    }
    
    function check(checkbox_id){
        $("#"+checkbox_id) //finalizar isso - teste
    }

    function select_user(user_id,user_name){ 
        $(".display_user_name").val(user_name);
        $("#user").val(user_id);
        $("#choose_user").modal('hide');

        if($("#empty_order").val()){
            var baseUrl = my_url+"Terminal/terminal/startOrder/"+user_id;
        
            $.ajax({

                url: baseUrl,
                async: false,

            }).done(function (json) {

                obj = JSON.parse(json);
                $("#order").val(obj.data);

            }).fail(function (jqXHR, textStatus) {

                alert("Request failed: " + textStatus);

            });
        }
    }

    function select_customer(customer_id,customer_name){
        ok = 0;
        article = $("#article").val();
        if(article!=0){
            if(confirm('Changing the customer may change the price of some products. Are you sure you want to do it?')){
                ok = 1;
            }
        }else{
            ok = 1;
        }

        if(ok){
            $(".display_customer_name").val(customer_name);
            $("#customer").val(customer_id);
            $("#choose_customer").modal('hide');

            var order_id = $("#order").val();
            var update_type = "customer";
            var baseUrl = my_url+"Terminal/terminal/updateOrder/"+update_type+"/"+customer_id+"/"+order_id;
        
            $.ajax({

                url: baseUrl,
                async: false,

            }).done(function (json) {
                $("#empty_order").val(0);

                var baseUrl = my_url+"Terminal/terminal/getOrderLinesPrices/"+order_id+"/"+customer_id;
                //trará uma lista com os precos dos produtos que existem nesta order, para em seguida sobrescrever os preços na tabela do POS
                $.ajax({

                    url: baseUrl,
                    async: false,

                }).done(function (json) {
                    json = JSON.parse(json);
                    var elements = json.data;
                    var order_line_id = 0;
                    if (elements != "") {
                        for(i=0;i<elements.length;i++){
                            order_line_id = elements[i].id;
                            $("#label_price_"+order_line_id).html(elements[i].price); //teste - por algum motivo isto nao está sobrescrevendo o html
                            save_edition_attribute("price",order_line_id,elements[i].product_id);
                        }
                    }
                }).fail(function (jqXHR, textStatus) {

                    alert("Request failed: " + textStatus);

                });
            }).fail(function (jqXHR, textStatus) {

                alert("Request failed: " + textStatus);

            });
        }
    }

    function insert_new_product(product_id){
        var dados = [];
        dados.push(product_id);
        var element = { name: "product_id", value: product_id };
       
        dados.push(element);
      
        var qty = $("#include_new_product_qty").val();
        var order = $("#order").val();
        var customer = $("#customer").val();
        var baseUrl = my_url+"Terminal/terminal/insertProduct/"+product_id+"/"+qty+"/"+order+"/"+customer;

        $.ajax({

            url: baseUrl,
            method: "POST",
            data: dados,
            async: false,

        }).done(function (json) {
            json = JSON.parse(json);

            var elements = json.data; 
            var order_line_id = elements[0]['order_line_id'];
       
            count_products++;

            if (elements != "") {
                var phrase = "";
                phrase += "<tr id='line_product_"+order_line_id+"' class='line_product'><td onclick='check(\"check_product_"+order_line_id+"\")'><input type='checkbox' which='"+order_line_id+"' id='check_product_"+order_line_id+"' class='check_product'></td><td class='count_product'>"+count_products+"</td><td>"+elements[0]['product_number']+"</td><td>"+elements[0]['name']+"</td>";

                phrase += generate_product_line_attribute(order_line_id,"qty",$("#include_new_product_qty").val(),_permissions['edit_qty'],product_id);

                phrase += generate_product_line_attribute(order_line_id,"price",elements[0]['price'],_permissions['edit_price'],product_id);

                phrase += generate_product_line_attribute(order_line_id,"discount","",_permissions['edit_discount'],product_id);

                phrase += generate_product_line_attribute(order_line_id,"final_price",(elements[0]['price'] * parseInt($("#include_new_product_qty").val())).toFixed(2),_permissions['edit_final_price'],product_id);

                phrase += generate_product_line_attribute(order_line_id,"info","",_permissions['edit_info'],product_id);

                phrase += "</tr>";

                $(".pos_table tbody").prepend(phrase);

                $("#product_list").append("<input type='hidden' name='products[]' id='product_"+order_line_id+"' class='product' qty='"+$("#include_new_product_qty").val()+"' price='"+elements[0]['price']+"' discount='0' final_price='"+(elements[0]['price'] * parseInt($("#include_new_product_qty").val())).toFixed(2)+"' info='' value='"+product_id+"'>");

                $("#empty_order").val(0);

                if(elements[0]['has_size_and_color']){
                    varieties_buttons = elements[0]['varieties'];

                    $("#product_varieties").html(varieties_buttons);
                    $("#choose_product_variety").modal({show : true});
                }

               $("#article").val(product_id);
            }

            calculate_order_total();

        }).fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });

        $("#include_new_product").modal('hide');
        $("#include_new_product_qty").val(1);
    }
    
    function select_variety(order_line_id,size_and_color_id){
        var baseUrl = my_url+"Terminal/terminal/selectVariety/"+order_line_id+"/"+size_and_color_id;

        $.ajax({

            url: baseUrl,
            method: "POST",
            async: false,

        }).done(function (json) {
            json = JSON.parse(json);
            var elements = json.data;
            var complement_name = elements[0]['complement_name'];

            $("#line_product_"+order_line_id+" td:eq(3)").append(" | "+complement_name);

            $("#choose_product_variety").modal('hide');

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });
    }
    
    function open_edition_attribute(column,order_line_id){ //edit products put in POS
        $(".label_product_line_attribute").show();
        $(".edit_product_line_attribute").hide();

        $("#label_"+column+"_"+order_line_id).hide();
        $("#edit_"+column+"_"+order_line_id).show();
        $("#edit_"+column+"_"+order_line_id).focus();
    }
    
    function save_edition_attribute(column,order_line_id,product_id){ //edit products put in POS
        $("#label_"+column+"_"+order_line_id).show();
        $("#edit_"+column+"_"+order_line_id).hide();

        var value = $("#edit_"+column+"_"+order_line_id).val();
        $("#label_"+column+"_"+order_line_id).html(value);

        var qty_ = parseFloat($("#label_qty_"+order_line_id).html());
        var discount_ = $("#label_discount_"+order_line_id).html();
        if(discount_==""){
            discount_ = 0;
        }
        console.log(order_line_id);
        var price_ = parseFloat($("#label_price_"+order_line_id).html());
        var info_ = $("#label_info_"+order_line_id).html();
        $("#label_"+column+"_"+order_line_id).html(value);
        $("#product_"+order_line_id).attr(column,value);

        if(column=="qty"||column=="price"||column=="discount"){
            if(discount_==""){
                discount_ = 0;
            }
            discount_ = parseFloat(discount_);

            var final_price_ = ((qty_ * price_)*((100 - discount_)/100)).toFixed(2);
            $("#label_final_price_"+order_line_id).html(final_price_);
            $("#edit_final_price_"+order_line_id).val(final_price_);

            calculate_order_total();
           
            if(discount_!=0){
                console.log(order_line_id);
               
                $("#line_product_"+order_line_id).css("background","#FFDDDD");
            }else{
                console.log(order_line_id);
                $("#line_product_"+order_line_id).css("background","#EDECEC");
            }

            if(column=="qty"){       
                var baseUrl = my_url+"Terminal/terminal/deleteProduct/"+order_line_id; 
                $.ajax({
                    url: baseUrl,
                    async: false,
                }).done(function () {
                    order_id = $("#order").val();
               
                    person_id = $("#customer").val();
                    var baseUrl = my_url+"Terminal/terminal/insertProduct/"+product_id+"/"+qty_+"/"+order_id+"/"+person_id;
                    $.ajax({
                        url: baseUrl,
                        async: false,
                    }).done(function (json) {
                        //
                    });
                });
            }
        }else if(column=="final_price"){
            var final_price_ = parseFloat($("#label_final_price_"+order_line_id).html());

            if(discount_==""){
                discount_ = 0;
            }
            discount_ = parseFloat(discount_);

            price_ = (final_price_/(qty_ * ((100 - discount_)/100))).toFixed(2);

            if(((price_*qty_)*(100 - discount_)/100).toFixed(2)>final_price_.toFixed(2)){
                price_ -= 0.01;
            }
            final_price_ = ((qty_ * price_)*((100 - discount_)/100)).toFixed(2);

            $("#label_final_price_"+order_line_id).html(final_price_);
            $("#edit_final_price_"+order_line_id).val(final_price_);

            $("#label_price_"+order_line_id).html(price_);
            $("#edit_price_"+order_line_id).val(price_);
            $("#product_"+order_line_id).attr("price",price_);

            calculate_order_total();
        }                                                     
        var baseUrl = my_url+"Terminal/terminal/updateProduct/"+order_line_id+"/"+qty_+"/"+discount_+"/"+price_+"/"+info_;

        $.ajax({

            url: baseUrl,
            method: "POST",
            async: false,

        }).done(function (json) {
            json = JSON.parse(json);
            var elements = json.data;



        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });        
    }

    function getOrderLines(order_id){ //customer_id
//alert(my_url+"Terminal/Terminal/getOrderLines/"+order_id);        
        var table_customer_order_lines = $('#table_customer_order_lines').DataTable( {
            "ajax": {"url": my_url+"Terminal/Terminal/getOrderLines/"+order_id},
            "columns": [
                { "data": "id" },
                { "data": "product_number" },
                { "data": "description_with_variety" },
                { "data": "qty" },
                { "data": "price" },
                { "data": "discount" },
                { "data": "amount" },
                { "data": "amount_inc_tax" }
            ],
            "aoColumnDefs": [
                { "bSortable": true, "aTargets": [-1] }
            ],
            "retrieve": true,
            "paging": true,
            "info":     false,
            "language": {
                "lengthMenu": "Display _MENU_ records per page",
                "zeroRecords": "Nothing found - sorry",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "(filtered from _MAX_ total records)"
            }
        });
        reload_table(table_customer_order_lines);

        $("#modal_customer_order_lines").modal({show:true});
        $("#modal_customer_orders").modal('hide');
    }
    
    function load_parked_order(order_id){
        var baseUrl = my_url+"Terminal/terminal/getOrder/"+order_id+"/1";
        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) {
            var elements = JSON.parse(json);
            elements = elements.data;

            count_products = 0;

            clear_order();
            $("#order").val(elements[0].id);
            $("#info_order").val(elements[0].extra_info);
            if(elements[0].customer_name){
                $(".display_customer_name").val(elements[0].customer_name);
            }else{
                $(".display_customer_name").val("");
            }
            $(".display_user_name").val(elements[0].user_name);
            $(".total_order").html(elements[0].total_order);
            if(elements[0].products){
                max = elements[0].products.length;
                for(i=0;i<max;i++){
                    count_products++;
                    var variety = "";
                    if(elements[0].products[i].variety!=""){
                        variety = " | "+elements[0].products[i].variety;
                    }
                    var phrase = "";
                    phrase += "<tr id='line_product_"+elements[0].products[i].id+"' class='line_product'><td onclick='check(\"check_product_"+elements[0].products[i].id+"\")'><input type='checkbox' which='"+elements[0].products[i].id+"' id='check_product_"+elements[0].products[i].id+"' class='check_product'></td><td class='count_product'>"+count_products+"</td><td>"+elements[0].products[i].product_number+"</td><td>"+elements[0].products[i].description+" "+variety+"</td>";

                    phrase += generate_product_line_attribute(elements[0].products[i].id,"qty",elements[0].products[i].qty,_permissions['edit_qty'],elements[0].products[i].product_id);

                    phrase += generate_product_line_attribute(elements[0].products[i].id,"price",elements[0].products[i].price,_permissions['edit_price'],elements[0].products[i].product_id);

                    phrase += generate_product_line_attribute(elements[0].products[i].id,"discount",elements[0].products[i].discount,_permissions['edit_discount'],elements[0].products[i].product_id);

                    phrase += generate_product_line_attribute(elements[0].products[i].id,"final_price",(((elements[0].products[i].qty * elements[0].products[i].price)*((100 - elements[0].products[i].discount)/100)).toFixed(2)),_permissions['edit_final_price'],elements[0].products[i].product_id);

                    info_ = "";
                    if(elements[0].products[i].info){
                        info_ = elements[0].products[i].info;
                    }
                    phrase += generate_product_line_attribute(elements[0].products[i].id,"info",info_,_permissions['edit_info'],elements[0].products[i].product_id);

                    phrase += "</tr>";

                    $(".pos_table tbody").prepend(phrase);
                }
            }

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });

        $("#choose_parked_order").modal("hide");
    }

    function calculate_order_total(){
        order_total_price = 0;
        $(".label_final_price").each(function(){
            order_total_price += parseFloat($(this).html());
        });
        $(".total_order").html(order_total_price.toFixed(2));
        $("#pay_order_total_amount").html(order_total_price.toFixed(2));
        $("#button_pay_all").html(order_total_price.toFixed(2));

        var order_id = $("#order").val();
        var update_type = "total_amount";
        var baseUrl = my_url+"Terminal/terminal/updateOrder/"+update_type+"/"+(order_total_price.toFixed(2))+"/"+order_id;

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (html) {

        });
    }

    function print_receipt(order_id,type){
        var baseUrl = my_url+"Terminal/terminal/getOrder/"+order_id+"/1/0/1/"+type;

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) {

            total = 0;
            order_lines = "";

            var retorno = JSON.parse(json);
            retorno = retorno.data;          
            lines = retorno[0].products;

            if(lines){
                order_lines += "<table><tr><th>Product Number</th><th>Descripfsdfsdtion</th><th>Qty</th><th>Price</th><th>Product Discount</th><th>Total</th><th>Info</th></tr>";

                max = lines.length;
                for(i=0;i<max;i++){
                    signal = "";
                    if(type=="credit"){ //i.e. if this receipt is from a credit note
                        signal = "-"
                    }

                    order_lines += "<tr>";
                    if(receipt_show_article_number=="on"){
                        order_lines += "<td>"+lines[i].product_number+"</td>";
                    }
                    price = lines[i].price;
                    if(receipt_print_actual_prices=="on"){
                        if(price!=lines[i].actual_price){
                            price = "<span style='text-decoration: line-through'>"+lines[i].actual_price+"</span> "+lines[i].price;
                        }
                    }
                    order_lines += "<td>"+lines[i].description+" | "+lines[i].variety+"</td>";
                    order_lines += "<td>"+lines[i].qty+"</td>";
                    order_lines += "<td>"+price+"</td>";
                    if(receipt_show_discount_rate=="on"){
                        order_lines += "<td>"+lines[i].discount+"</td>";
                    }
                    if(receipt_hide_comments=="on"){
                        order_lines += "<td>"+lines[i].info+"</td>";
                    }    

                    total_ = 0;
                    total_ = parseFloat(lines[i].price) * parseFloat(lines[i].qty);
                    total_ = total_ - (total_ * (parseFloat(lines[i].discount)/100));

                    total = parseFloat(total) + parseFloat(total_);

                    order_lines += "<td>"+signal+total_+"</td>";
                    order_lines += "</tr>";

                    if(receipt_two_lines=="on"){ //test - i dont know exactly what extra information put here
                        order_lines += "<tr>";
                        if(receipt_show_article_number=="on"){
                            order_lines += "<td>"+"</td>";
                        }    
                        order_lines += "<td>"+"</td>";
                        order_lines += "<td>"+"</td>";
                        order_lines += "<td>"+"</td>";
                        order_lines += "<td>"+"</td>";
                        if(receipt_show_discount_rate=="on"){
                            order_lines += "<td>"+"</td>";
                        }
                        if(receipt_hide_comments=="on"){    
                            order_lines += "<td>"+"</td>";
                        }    
                        order_lines += "</tr>";
                    }
                }

                order_lines += "</table>";
            }

            signal = "";
            if(type=="credit"){ //i.e. if this receipt is from a credit note
                signal = "-"
            }
            extra_text = "";
            if(type=="credit"){
                extra_text = retorno[0].extra_text;
//alert(my_url+"Terminal/terminal/generateCreditNote/"+order_id);
                var baseUrl = my_url+"Terminal/terminal/generateCreditNote/"+order_id;
                $.ajax({

                    url: baseUrl,
                    async: false,

                }).done(function (json) {

                });    
            }
            order_date = retorno[0].date_create;

            company_address = "";
            if(receipt_show_shop_address=="on"){
                company_address = _company_address;
            }

            $("#print").html("<div id='printable_receipt'><div>"+receipt_title+"</div><div><img src='"+company_image+"'></div><div>"+company_name+"</div>"+company_address+"<div>"+order_date+"</div><div>Receipt</div><div>"+order_lines+"</div><div>Total: "+signal+total+"</div><div>"+extra_text+"</div><div>"+receipt_text+"</div></div>");
            printJS('printable_receipt', 'html');
        });
    }

    function send_to_other_order(from,line_id){
        var conf = $("#split_new_order").val();
        if(!conf){            
            user_id = $("#user").val();
            var baseUrl = my_url+"Terminal/terminal/startOrder/"+user_id;
        
            $.ajax({

                url: baseUrl,
                async: false,

            }).done(function (json) {
                obj = JSON.parse(json);
                $("#split_new_order").val(obj.data);

                new_order = $("#split_new_order").val();
                customer_id = $("#customer").val();
                var update_type = "customer";

                var update_type = "customer";
                var baseUrl = my_url+"Terminal/terminal/updateOrder/"+update_type+"/"+customer_id+"/"+new_order;

                $.ajax({

                    url: baseUrl,
                    async: false,

                }).done(function (json) {
                    


                }).fail(function (jqXHR, textStatus) {

                    alert("Request failed: " + textStatus);

                });

            }).fail(function (jqXHR, textStatus) {

                alert("Request failed: " + textStatus);

            });
        }

        var target = "";
        var from_order = 0;
        if(from=="current"){
            target = "new";
            from_order = $("#order").val();
            target_order = $("#split_"+target+"_order").val();
        }else{
            target = "current";
            from_order = $("#split_new_order").val();
            target_order = $("#order").val();
        }

               
        qty = $("#split_"+from+"_qty_"+line_id).html();       
        fraction = $("#split_state").val();
        if(fraction!=0){
            qty = 1/fraction;
        }
        qty = parseFloat(qty);

        var baseUrl = my_url+"Terminal/terminal/exchangeOrderLine/"+line_id+"/"+target_order+"/"+qty;
        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) {
            mount_table_split_order(target,target_order);
            mount_table_split_order(from,from_order);
            order_id = $("#order").val();
            mount_table_pos(order_id);

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });
    }

    function mount_table_pos(order_id){
        var baseUrl = my_url+"Terminal/terminal/getOrder/"+order_id+"/1";

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) {
            var elements = JSON.parse(json);
            elements = elements.data;

            count_products = 0;

            $(".total_order").html(elements[0].total_order);
            $(".pos_table tbody").html("");
            if(elements[0].products){
                max = elements[0].products.length;
                for(i=0;i<max;i++){
                    count_products++;
                    var variety = "";
                    if(elements[0].products[i].variety!=""){
                        variety = " | "+elements[0].products[i].variety;
                    }
                    var phrase = "";
                    phrase += "<tr id='line_product_"+elements[0].products[i].id+"' class='line_product'><td onclick='check(\"check_product_"+elements[0].products[i].id+"\")'><input type='checkbox' which='"+elements[0].products[i].id+"' id='check_product_"+elements[0].products[i].id+"' class='check_product'></td><td class='count_product'>"+count_products+"</td><td>"+elements[0].products[i].product_number+"</td><td>"+elements[0].products[i].description+" "+variety+"</td>";

                    phrase += generate_product_line_attribute(elements[0].products[i].id,"qty",elements[0].products[i].qty,_permissions['edit_qty'],elements[0].products[i].product_id);

                    phrase += generate_product_line_attribute(elements[0].products[i].id,"price",elements[0].products[i].price,_permissions['edit_price'],elements[0].products[i].product_id);

                    phrase += generate_product_line_attribute(elements[0].products[i].id,"discount",elements[0].products[i].discount,_permissions['edit_discount'],elements[0].products[i].product_id);

                    phrase += generate_product_line_attribute(elements[0].products[i].id,"final_price",(((elements[0].products[i].qty * elements[0].products[i].price)*((100 - elements[0].products[i].discount)/100)).toFixed(2)),_permissions['edit_final_price'],elements[0].products[i].product_id);

                    info_ = "";
                    if(elements[0].products[i].info){
                        info_ = elements[0].products[i].info;
                    }
                    phrase += generate_product_line_attribute(elements[0].products[i].id,"info",info_,_permissions['edit_info'],elements[0].products[i].product_id);

                    phrase += "</tr>";

                    $(".pos_table tbody").prepend(phrase);
                }
            }

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });
    }

    function mount_table_split_order(which_table,order_id){       
        var baseUrl = my_url+"Terminal/terminal/getOrder/"+order_id+"/1";

        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) {
            var elements = JSON.parse(json);
            elements = elements.data;

            //$("#split_"+which_table+"_total").html(elements[0].total_order);
            $(".split_"+which_table+"_order_table tbody").html("");
            if(elements[0].products){              
                max = elements[0].products.length;
                this_total = 0;
                for(i=0;i<max;i++){                    
                    var variety = "";
                    if(elements[0].products[i].variety){
                        variety = " | "+elements[0].products[i].variety;
                    }
                    var phrase = "";
                    phrase += "<tr id='split_line_product_"+elements[0].products[i].id+"' class='split_line_product' onclick='send_to_other_order(\""+which_table+"\",\""+elements[0].products[i].id+"\")'><td id='split_"+which_table+"_qty_"+elements[0].products[i].id+"'>"+elements[0].products[i].qty+"</td><td>"+elements[0].products[i].description+" "+variety+"</td>";

                    phrase += "<td>"+elements[0].products[i].price+"</td>";
                    phrase += "<td>"+elements[0].products[i].discount+"</td>";

                    phrase += "</tr>";
                    //this_total += parseFloat((elements[0].products[i].price * elements[0].products[i].qty));
                    $(".split_"+which_table+"_order_table tbody").prepend(phrase);
                }
                $(".split_"+which_table+"_order_table tbody").append("<tr><td colspan='4'>Total: "+elements[0].total_order+"</td></tr>");
                
               
            }

        }).fail(function (jqXHR, textStatus) {

            alert("Request failed: " + textStatus);

        });
    }

    function add_billet(billet){
        var order_id = $("#order").val();
        var baseUrl = my_url+"Terminal/terminal/addBillet/"+order_id+"/"+billet;
        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) { 
            var elements = JSON.parse(json);
            payment_billets += billet;
            var order_billet_id = elements.data;
            $("#pay_order_received_total_amount").html(payment_billets);
            $("#pay_order_received_amount").append("<div class='billet_line' id='billet_line_"+count_payment_billets+"' >"+billet+" <input type='button' onclick='delete_billet_line(\""+count_payment_billets+"\",\""+billet+"\",\""+order_billet_id+"\")' value='X'></div>");
            count_payment_billets++;
            conf_billets();            
        });
    }

    function delete_billet_line(bl,amount,order_billet_id){
        var order_id = $("#order").val();
        var baseUrl = my_url+"Terminal/terminal/deleteBillet/"+order_billet_id;
        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) { 
            $("#billet_line_"+bl).remove();
            payment_billets -= amount;
            $("#pay_order_received_total_amount").html(payment_billets);
            conf_billets();            
        });
    }

    function delete_all_billets(){
        var order_id = $("#order").val();
        var baseUrl = my_url+"Terminal/terminal/deleteAllBillets/"+order_id;
        $.ajax({

            url: baseUrl,
            async: false,

        }).done(function (json) { 
            $("#pay_order_received_amount").html("");
            $(".total_order").html("0.00");          
        });
    }
    
    $(".keyboard").click(function(){
       alert("as"); //testCMD();
    });
    
    function loadPrint(xml) {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML =
                this.responseText;
              }
            };            
            xhttp.open("GET", "http://localhost:27432/executer/exec.php?xml=" + xml, true);
            xhttp.send();
    }