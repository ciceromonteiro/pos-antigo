var returnAJAX = "";
var nameTable = "";
var idTable = "";

/* Begin Structure tables*/
function createTable(tableName, urlAJAX){
    var idTableL = "#"+tableName;
    var table = $(idTableL).DataTable({
        "ajax": {"url": my_url+urlAJAX},
        "aoColumnDefs": [
            { "bSortable": true, "aTargets": [-1] }
        ],
        "ordering": true,
        "info":     false,
        "language": {
            "lengthMenu": "Display _MENU_ records per page",
            "zeroRecords": "Nothing found - sorry",
            "info": "Showing page _PAGE_ of _PAGES_",
            "infoEmpty": "No records available",
            "infoFiltered": "(filtered from _MAX_ total records)"
        }
    });
    idTable = idTableL;
    nameTable = tableName;
}

// Select tr
$(".table-with-checkbox tbody").on( 'click', 'tr', function () {
    var checkboxInput = $(this).find('input:checkbox');
    if ( $(this).hasClass('hover-select') ) {
        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);
        $(this).removeClass('hover-select');
        checkboxInput[0].checked = false;
    }
    else {
        $('#update').attr('disabled', false);
        $('#delete').attr('disabled', false);
        $(this).addClass('hover-select');
        checkboxInput[0].checked = true;
    }
});

// Multiple deletes rows
$('#'+nameTable+' thead').on('click', '#all-checkbox', function () {   
    var i;
    var checkboxArray = $("#"+nameTable+" tbody tr").find('input:checkbox');
    var isChecked = $("#"+nameTable+" thead tr").find('input:checkbox');

    if(isChecked[0].checked == false){
        for(i = 0; i < checkboxArray.length; i++){
            $("#"+nameTable+" tbody").find('tr').removeClass('hover-select');
            checkboxArray[i].checked = false;
        }

        $('#update').attr('disabled', true);
        $('#delete').attr('disabled', true);            
    } else {
        for(i = 0; i < checkboxArray.length; i++){
            $("#"+nameTable+" tbody").find('tr').addClass('hover-select');
            checkboxArray[i].checked = true;
        }
        $('#delete').attr('disabled', false);
    }
});

function reload_table(nameTable){
    nameTable.ajax.reload(null,false);
}
/* End Structure tables */

function openModal(idModal, action, idForm){
    $(idModal).modal({show : true});
    $(idModal).addClass(action);
    var btn_send = $(idModal).find('#btn_action_send');
    btn_send.html(action);
}

function execAction(idForm, idTable, urlAJAX, dataAJAX, idModal){  
    var typeCreate = $(idModal).hasClass("create");
    var typeUpdate = $(idModal).hasClass("update");
    var typeDelete = $(idModal).hasClass("delete");
    
    if(typeCreate != false){
        alert('ok');
    }
    
    if(action[0] == "create"){
        var request   = $.ajax({
            url:          urlAJAX,
            cache:        false,
            data:         dataAJAX,
            dataType:     'json',
            contentType:  'application/json; charset=utf-8',
            type:         'get'
        });
        request.done(function(output){
            if (output.result == 'success'){
                returnAJAX = output.result;
            } else {
                var title = "Failed";
                var text = "Try again";
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
            }
        });
        request.fail(function(jqXHR, textStatus){
            var title = "Failed";
            var text = textStatus;
            var icon = "glyphicon glyphicon-minus";
            var type = "error";
            notification(title, text, icon, type);
            console.log(jqXHR.responseText);
        });
    }

    if(action[0] == "update"){
        var id      = action[1].$('tr.hover-select').find('input:checkbox').data('id');
        var request = $.ajax({
            url:          urlAJAX,
            cache:        false,
            data:         'id=' + id,
            dataType:     'json',
            contentType:  'application/json; charset=utf-8',
            type:         'get'
        });
        request.done(function(output){
            if (output.result == 'success'){
                returnAJAX = output;
            } else {
                var title = "failed";
                var text = "Information request failed";
                var icon = "glyphicon glyphicon-minus";
                var type = "error";
                notification(title, text, icon, type);
            }
        });
        request.fail(function(jqXHR, textStatus){
            var title = "failed";
            var text = "Information request failed: " + textStatus;
            var icon = "glyphicon glyphicon-minus";
            var type = "error";
            notification(title, text, icon, type);
            console.log(jqXHR.responseText);
        });
    }

    if(action[0] == "delete"){
        var trs = idTable.$('tr.hover-select').each(function () {
            var sThisVal = (this.checked ? $(this).val() : "");
        });        

        var i=0, ids = [], names = "";

        for(i=0; i<trs.length; i++){
            names += $(trs[i]).find('input:checkbox').data('name');
            ids.push({
                name: "idColor[]",
                value: $(trs[i]).find('input:checkbox').data('id'),
            });
        }                    
        
        var text  = "Are you sure you want to delete ('"+names+"')?";
        var icon = "glyphicon glyphicon-minus";

        actionDeleteWithConfirm(text, ids, names, urlAJAX);
    }

    
}

function actionDeleteWithConfirm(text, ids, names, urlAJAX){
    var request = $.ajax({
        url:          urlAJAX,
        cache:        false,
        data:         ids,
        dataType:     'json',
        contentType:  'application/json; charset=utf-8',
        type:         'get'
    });
    request.done(function(output){
        if (output.result == 'success'){
            var title = "Deleted successfully";
            var text = names + "' deleted successfully";
            var icon = "glyphicon glyphicon-plus";
            var type = "success";
            reload_table();
            notification(title, text, icon, type);
        } else {
            var title = "Delete request failed";
            var text = "Try again";
            var icon = "glyphicon glyphicon-minus";
            var type = "error";
            reload_table();
            notification(title, text, icon, type);
        }
        $('.ui-pnotify-modal-overlay').remove();
    });
    request.fail(function(jqXHR, textStatus){
        var title = "Delete request failed";
        var text = textStatus;
        var icon = "glyphicon glyphicon-minus";
        var type = "error";
        notification(title, text, icon, type);
        console.log(jqXHR.responseText);
    });
}