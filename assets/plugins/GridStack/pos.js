
var element;
var $gs = [];
var new_element = null;
var new_button = null;
var idModal = "#myModal";
var button_abas = [];
var id_now = 0;

var count = [];
count['container'] = 1;
count['logo'] = 1;
count['customer'] = 1;
count['employee'] = 1;
count['lastsale'] = 1;
count['order'] = 1;
count['button'] = 1;
count['abas'] = 1;
count['table'] = 1;

function main() {

    $(".grid-stack").on("change", function (event, ui) {
        
        if (new_element != null) {
            
            var type = $(new_element).data('type'),
                    id = $(element).data('id');
            $('.sidebar-' + type + ' .ui-draggable[data-type="' + type + '"]').remove();
            $('.sidebar-' + type).append(new_element);
            
            if (type == "container") {
                $('.grid-stack-item[data-type="'+type+'"] .grid-stack-item-content[data-id="' + id + '"]').append('')
                        .append('<div class="grid-stack grid-stack-6"></div>');
                $('.grid-stack-item[data-type="'+type+'"] .grid-stack-item-content[data-id="' + id + '"]').append(
                    '<ul class="buttons">'+actionWidget(id,'add',type)+actionWidget(id,'remove',type)+'</ul>')
                .append('<div class="grid-stack grid-stack-6"></div>');
                createGridstack(id, '.grid-stack-item-content[data-id="' + id + '"] .grid-stack', '.sidebar .grid-stack-item', 6);
            } else if ((type == "customer") || (type == "employee") || (type == "lastsale") || (type == "order")) {
                var r = getLayout($(new_element), type,id);
                $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"] span.title').remove();
                $('.grid-stack-item[data-type="'+type+'"] .grid-stack-item-content[data-id="' + id + '"]').append(
                    '<ul class="buttons">'+actionWidget(id,'remove',type)+'</ul>').append(r['content']);
            } else if (type == "abas") {
                modal(idModal,$(new_element), type, id);
                $('.grid-stack-item[data-type="'+type+'"] .grid-stack-item-content[data-id="' + id + '"]').append('<div class="grid-stack grid-stack-6"></div>')
                .append('<ul class="buttons">'+actionWidget(id,'add',type)+actionWidget(id,'edit',type)+actionWidget(id,'remove',type)+'</ul>');
                createGridstack(id, '.grid-stack-item-content[data-id="' + id + '"] .grid-stack', '.sidebar .grid-stack-item', 6);
            } else {
                modal(idModal,$(new_element), type, id);
                $('.grid-stack-item[data-type="'+type+'"] .grid-stack-item-content[data-id="' + id + '"]').append(
                    '<ul class="buttons">'+actionWidget(id,'edit',type)+actionWidget(id,'remove',type)+'</ul>');
            }
            
            new_element = null;
            main();
        }
        
    });

    $('.sidebar .grid-stack-item').draggable({
        revert: 'invalid',
        scroll: true,
        handle: '.grid-stack-item-content',
        start: function (event, ui) {
            element = ui.helper[0].outerHTML;
            new_element = widget($(element).data("type"));
            count[$(element).data("type")] += 1;
        }
    });

}

function createGridstack(id, grid, acceptWidgets, width) {
    var options = {
        width: width,
        float: false,
        acceptWidgets: acceptWidgets,
        alwaysShowResizeHandle: false,
        resizable: {
            handles: 'e, se, s, sw, w'
        }
    };
    $gs[id] = $(grid).gridstack(options);
}

function modal(el,obj, type, id, idelement, idgridstack) {
    var r = null;
    $(el).on('show.bs.modal', function (event) {
        
        var element = getLayout(obj, type,id, idelement);
        
        var modal = $(this);
        modal.find('.modal-title').text(element['title']);
        modal.find('.modal-body').html(element['content']);
        modal.find('.modal-footer .btn:nth-child(1)').text("Close");
        modal.find('.modal-footer .btn:nth-child(2)').text("Save");
        
        if (type == "container" || type == "button_abas") {
            modal.find('.modal-body input.cor-botao').colorpicker();
            modal.find('.modal-body input.cor-fonte').colorpicker();
        }
        
        modal.find('.modal-footer .btn:nth-child(2)').on('click', function(){
            
            if (type == "logo") {
                var width = $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"]').width(),
                    height = $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"]').height();
                $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"] span.title').remove();
                $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"] img').remove();
                $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"]').append('<img src="http://placehold.it/'+width+'x'+height+'" />');
            } else if (type == "table") {
                
                if ($('.row[data-type="'+type+'"][data-id="'+id+'"] input.column').size() > 0) {
                
                    $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"]').css('overflow-y','hidden');
                    $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"] span.title').remove();
                    $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"] .row').remove();

                    var table = '\
                        <div class="row" data-type="'+type+'" data-id="'+id+'">\
                            <div class="col-md-12">\
                                <table id="example" class="table-bordered" cellspacing="0" width="100%">\
                                    <thead>\
                                        <tr>\
                    ';

                                            $('.row[data-type="'+type+'"][data-id="'+id+'"] input.column').each(function(e) {
                                                table += '<th>'+$(this).val()+'</th>';
                                            });

                    table += '\
                                        </tr>\
                                    </thead>\
                                    <tfoot>\
                                        <tr>\
                    ';

                                            $('.row[data-type="'+type+'"][data-id="'+id+'"] input.column').each(function(e) {
                                                table += '<th>'+$(this).val()+'</th>';
                                            });

                    table += '\
                                        </tr>\
                                    </tfoot>\
                                    <tbody>\
                                        <tr>\
                    ';

                                            $('.row[data-type="'+type+'"][data-id="'+id+'"] input.column').each(function(e) {
                                                table += '<td>...</td>';
                                            });

                    table += '\
                                        </tr>\
                                    </tbody>\
                            </table>\
                        </div>\
                    </div>';

                    $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"]').append(table);

                    if ( ! $.fn.DataTable.isDataTable( '.row[data-type="'+type+'"][data-id="'+id+'"] #example' ) ) {
                        $('.row[data-type="'+type+'"][data-id="'+id+'"] #example').DataTable({
                            responsive: true
                        });
                    } else {
                        $(".row[data-type='"+type+"'][data-id='"+id+"'] #example").dataTable().fnDestroy();
                        $('.row[data-type="'+type+'"][data-id="'+id+'"] #example').DataTable({
                            responsive: true
                        });
                    }
                    
                }
        
            } else if (type == "abas") {
                
                if ($('.row[data-type="'+type+'"][data-id="'+id+'"] input.column').size() > 0) {
                    
                    $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"] span.title').remove();
                    $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"] .row').remove();

                    var table = '\
                        <div class="row" style="margin-left: 0px; margin-right: 0px; height: calc(100% - 10px);" data-type="'+type+'" data-id="'+id+'">\
                            <ul class="nav nav-tabs" role="tablist">\
                    ';

                        $('.row[data-type="'+type+'"][data-id="'+id+'"] input.column').each(function(e) {
                            var c = (e == 0) ? 'class="active"' : '' ;
                            table += '\
                                <li role="presentation" '+c+'><a href="#'+slugify($(this).val())+'" aria-controls="'+slugify($(this).val())+'" role="tab" data-toggle="tab">'+$(this).val()+'</a></li>\
                            ';
                        });

                        table += '\
                            </ul>\
                            <div class="tab-content" style="height: calc(100% - 51px);">\
                        ';

                        $('.row[data-type="'+type+'"][data-id="'+id+'"] input.column').each(function(e) {
                            var c = (e == 0) ? 'active' : '' ;
                            table += '\
                                <div role="tabpanel" class="tab-pane '+c+'" id="'+slugify($(this).val())+'" style="height: 100%;" data-id="'+parseInt("" + id + "0000"+ e)+'" data-idgridstack="'+id+'"><div class="grid-stack grid-stack-6"></div></div>\
                            ';
                        });

                        table += '\
                            </div>\
                        ';

                    table += '\
                        </div>';

                    $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"]').append(table);
                    
                    $('.row[data-type="'+type+'"][data-id="'+id+'"] .tab-content .tab-pane .grid-stack').each(function(i) {
                        createGridstack(parseInt("" + id + "0000"+ i), $(this), '.sidebar .grid-stack-item', 6);
                    });
                    
                }

            } else if (type == "container") {
                
//                $('.grid-stack-item[data-type="' + type + '"] .grid-stack-item-content[data-id="' + id + '"] span.title').remove();
                
                    var idelement_tmp = modal.find('.modal-body input.idelement').val(),
                        id_tmp = modal.find('.modal-body input.id').val();
                    
                    if (id_now == " "+idelement_tmp+id_tmp) {
                        
                        var nome = modal.find('.modal-body input.nome-botao').val(),
                            comando = modal.find('.modal-body input.comando').val(),
                            bkg = modal.find('.modal-body input.cor-botao').val(),
                            color = modal.find('.modal-body input.cor-fonte').val(),
                            fontesize = modal.find('.modal-body input.tamanho-fonte').val();

                        var button = $('.grid-stack-item[data-id="'+id_tmp+'"] .grid-stack-item-content[data-id="'+id_tmp+'"] .grid-stack .grid-stack-item .grid-stack-item-content[data-id="'+id_tmp+'"][data-type="button"][data-idelement="'+idelement_tmp+'"] button');

                        button.text(nome);
                        button.attr("data-command",comando);
                        button.attr("data-bkg",bkg);
                        button.attr("data-color",color);
                        button.attr("data-fontsize",fontesize);

                        button.css("background-color",bkg);
                        button.css("color",color);
                        button.css("font-size",fontesize+"px");
                        button.css("border","0px");
                        
                    }

            } else if (type == "button_abas") {
                
                var idelement_tmp = modal.find('.modal-body input.idelement').val(), 
                    id_tmp = modal.find('.modal-body input.id').val();
                    
                if (id_now == " "+idelement_tmp+id_tmp) {
                    
                    var nome = modal.find('.modal-body input.nome-botao').val(),
                        comando = modal.find('.modal-body input.comando').val(),
                        bkg = modal.find('.modal-body input.cor-botao').val(),
                        color = modal.find('.modal-body input.cor-fonte').val(),
                        fontesize = modal.find('.modal-body input.tamanho-fonte').val();

                        var button = $('.grid-stack-item[data-id="'+idgridstack+'"] .grid-stack-item-content[data-id="'+idgridstack+'"] .row[data-id="'+idgridstack+'"] .tab-content .tab-pane.active[data-id="'+id+'"] .grid-stack .grid-stack-item[data-type="abas"] .grid-stack-item-content[data-idelement="'+idelement_tmp+'"][data-id="'+id_tmp+'"] button');
                        
                        button.text(nome);
                        button.attr("data-command",comando);
                        button.attr("data-bkg",bkg);
                        button.attr("data-color",color);
                        button.attr("data-fontsize",fontesize);

                        button.css("background-color",bkg);
                        button.css("color",color);
                        button.css("font-size",fontesize+"px");
                        button.css("border","0px");
                    
                }

            }
            
            
            $(el).modal('hide');
        });
        
    });
    $(el).modal('show');
}

function getLayout(obj, type,id, idelement) {
    var r = [];
    
    switch (type) {
        case "button_abas":
//            var idelement = obj.parents().eq(2).data("idelement");
//            id_now = " "+idelement+id;
        case "container": 
            
            r['title'] = 'Botão';
            
            if (obj.hasClass("edit-widget")) {
                
                var btn = obj.parents().eq(2).find('button'), comando, bkg, color, fontsize, nome, idelement, id;
                
                comando = ((typeof btn.attr("data-command") == "undefined") ?  "" : btn.attr("data-command") ),
                bkg = ((typeof btn.attr("data-bkg") == "undefined") ?  "" : btn.attr("data-bkg") ),
                color = ((typeof btn.attr("data-color") == "undefined") ? "" : btn.attr("data-color") ),
                fontsize = ((typeof btn.attr("data-fontsize") == "undefined") ? 12 : btn.attr("data-fontsize") ),
                nome = btn.text(),
                idelement = obj.parents().eq(2).data("idelement");
        
                id_now = " "+idelement+id;
            
                r['content'] = '<form>\
                    <div class="form" data-type="'+type+'" data-id="'+obj.parents().eq(2).data("id")+'">\
                        <div class="row">\
                            <div class="col-md-6">\
                                <div class="form-group">\
                                    <label for="nome-botao" class="control-label">Nome do bot&atilde;o:</label>\
                                    <input type="text" name="nome-botao" class="nome-botao form-control" value="'+nome+'">\
                                </div>\
                            </div>\
                            <div class="col-md-6">\
                                <div class="form-group">\
                                    <label for="comando" class="control-label">Comando:</label>\
                                    <input type="text" name="comando" class="comando form-control" value="'+comando+'">\
                                </div>\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-4">\
                                <div class="form-group">\
                                    <label for="cor-botao" class="control-label">Cor de fundo:</label>\
                                    <input type="text" data-format="hex" name="cor-botao" class="cor-botao form-control" value="'+bkg+'">\
                                </div>\
                            </div>\
                            <div class="col-md-4">\
                                <div class="form-group">\
                                    <label for="cor-botao" class="control-label">Cor da fonte:</label>\
                                    <input type="text" name="cor-botao" class="cor-fonte form-control" value="'+color+'">\
                                </div>\
                            </div>\
                            <div class="col-md-4">\
                                <div class="form-group">\
                                    <label for="tamanho-fonte" class="control-label">Tamanho da fonte:</label>\
                                    <input type="number" min="1" name="tamanho-fonte" class="tamanho-fonte form-control" value="'+fontsize+'">\
                                </div>\
                            </div>\
                        </div>\
                        <input type="hidden" class="idelement" name="idelement" value="'+idelement+'">\
                        <input type="hidden" class="id" name="id" value="'+id+'">\
                    </div>\
                </form>';
            
            } else {
                id_now = " "+idelement+id;
                
                r['content'] = '<form>\
                    <div class="form" data-type="'+type+'" data-id="'+id+'">\
                        <div class="row">\
                            <div class="col-md-6">\
                                <div class="form-group">\
                                    <label for="nome-botao" class="control-label">Nome do bot&atilde;o:</label>\
                                    <input type="text" name="nome-botao" class="nome-botao form-control">\
                                </div>\
                            </div>\
                            <div class="col-md-6">\
                                <div class="form-group">\
                                    <label for="comando" class="control-label">Comando:</label>\
                                    <input type="text" name="comando" class="comando form-control">\
                                </div>\
                            </div>\
                        </div>\
                        <div class="row">\
                            <div class="col-md-4">\
                                <div class="form-group">\
                                    <label for="cor-botao" class="control-label">Cor de fundo:</label>\
                                    <input type="text" data-format="hex" name="cor-botao" class="cor-botao form-control">\
                                </div>\
                            </div>\
                            <div class="col-md-4">\
                                <div class="form-group">\
                                    <label for="cor-botao" class="control-label">Cor da fonte:</label>\
                                    <input type="text" name="cor-botao" class="cor-fonte form-control">\
                                </div>\
                            </div>\
                            <div class="col-md-4">\
                                <div class="form-group">\
                                    <label for="tamanho-fonte" class="control-label">Tamanho da fonte:</label>\
                                    <input type="number" min="1" name="tamanho-fonte" class="tamanho-fonte form-control" value="12">\
                                </div>\
                            </div>\
                        </div>\
                        <input type="hidden" class="idelement" name="idelement" value="'+idelement+'">\
                        <input type="hidden" class="id" name="id" value="'+id+'">\
                    </div>\
                </form>';
            }

            break;
        case "logo": 
            r['title'] = "Logo";
            r['content'] = "<form>\
                <div class='form-group'>\
                    <label for='recipient-name' class='control-label'>File Input:</label>\
                    <input type='file' id='exampleInputFile'>\
                </div>\
                </form>";
            break;
        case "customer":
            r['title'] = null;
            r['content'] = "<form>\
                    <div class='row'><h4>Customer</h4></div>\
                    <div class='row'>\
                        <div class='col-md-4'><img src='http://placehold.it/80x80' /></div>\
                        <div class='col-md-8'>\
                            <div class='form-group'>\
                                <div class='row'><input type='text' class='form-control' placeholder='Customer'></div>\
                                <div class='row'><button class='btn' style='width: 100%; margin-top: 10px;'>Change</button></div>\
                            </div>\
                        </div>\
                    </div>\
                </form>";
            break;
        case "employee":
            r['title'] = null;
            r['content'] = "<form>\
                    <div class='row'><h4>Employee</h4></div>\
                    <div class='row'>\
                        <div class='col-md-4'><img src='http://placehold.it/80x80' /></div>\
                        <div class='col-md-8'>\
                            <div class='form-group'>\
                                <div class='row'><input type='text' class='form-control' placeholder='Employee'></div>\
                                <div class='row'><button class='btn' style='width: 100%; margin-top: 10px;'>Change</button></div>\
                            </div>\
                        </div>\
                    </div>\
                </form>";
            break;
        case "lastsale":
            r['title'] = null;
            r['content'] = "<div class='row'><h4>Last Sale</h4></div>\
                <div class='row' style='background-color: #CECECE; height: 50%;'></div>";
            break;
        case "order":
            r['title'] = null;
            r['content'] = "<div class='row'><h4>Order</h4></div>\
                <div class='row' style='background-color: #CECECE; height: 50%;'></div>";
            break;
        case "table":
            
            r['title'] = 'Tabela';
            
            if ($('.row[data-type="'+type+'"][data-id="'+id+'"] .dataTables_wrapper thead tr th').size() == 0) {
                r['content'] = "<form>\
                    <div class='row main' data-type='"+type+"' data-id='"+id+"'>\
                        <div class='col-md-10'>\
                            <input type='text' class='form-control column' placeholder='Coluna'>\
                        </div>\
                        <div class='col-md-2'>\
                            <div class='pull-right'>\
                                <div class='btn-group' role='group' aria-label=''>\
                                    <button type='button' class='btn add-column'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></button>\
                                    <button type='button' class='btn remove-column' disabled='disabled'><span class='glyphicon glyphicon-minus' aria-hidden='true'></span></button>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class='newelements'></div>\
                </form>";
            } else {
                    var columns = [], css = "display: block;";
                    $('.row[data-type="'+type+'"][data-id="'+id+'"] .dataTables_wrapper thead tr th').each(function(i) {
                        columns[i] = $(this).text();
                    });
                    
                    r['content'] = "<form>\
                        <div class='row main' data-type='"+type+"' data-id='"+id+"'>\
                            <div class='col-md-10'>\
                                <input type='text' class='form-control column' placeholder='Coluna' value='"+columns[0]+"'>\
                            </div>\
                            <div class='col-md-2'>\
                    ";

                    if (columns.length > 1) {
                        css = "display: none;";
                    }
                    
                    r['content'] += "\
                        <div class='pull-right' style='"+css+"'>\
                            <div class='btn-group' role='group' aria-label=''>\
                                <button type='button' class='btn add-column'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></button>\
                                <button type='button' class='btn remove-column' disabled='disabled'><span class='glyphicon glyphicon-minus' aria-hidden='true'></span></button>\
                            </div>\
                        </div>\
                    ";
                    
                    r['content'] += "\
                            </div>\
                        </div>\
                        <div class='newelements'>\
                    ";

                    $('.row[data-type="'+type+'"][data-id="'+id+'"] .dataTables_wrapper thead tr th').each(function(i) {
                        if (i != 0) {

                            if (columns.length == i+1) {
                                css = "display: block;";
                            } else {
                                css = "display: none;";
                            }
                            
                            r['content'] += "\
                                <div class='row' data-type='"+type+"' data-id='"+id+"'>\
                                    <div class='col-md-10'>\
                                        <input type='text' class='form-control column' placeholder='Coluna' value='"+columns[i]+"'>\
                                    </div>\
                                    <div class='col-md-2'>\
                                        <div class='pull-right' style='"+css+"'>\
                                            <div class='btn-group' role='group' aria-label=''>\
                                                <button type='button' class='btn add-column'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></button>\
                                                <button type='button' class='btn remove-column'><span class='glyphicon glyphicon-minus' aria-hidden='true'></span></button>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            ";
                        }
                    });
                                            
                    r['content'] += "\
                        </div>\
                    </form>";
                    
            }
            
            break;
        case "abas":
            
            r['title'] = 'Abas';
            
            
            if ($(".row[data-type='"+type+"'][data-id='"+id+"'] .nav-tabs li").size() == 0) {
            
                r['content'] = "<form>\
                    <div class='row main' data-type='"+type+"' data-id='"+id+"'>\
                        <div class='col-md-10'>\
                            <input type='text' class='form-control column' placeholder='Aba'>\
                        </div>\
                        <div class='col-md-2'>\
                            <div class='pull-right'>\
                                <div class='btn-group' role='group' aria-label=''>\
                                    <button type='button' class='btn add-column'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></button>\
                                    <button type='button' class='btn remove-column' disabled='disabled'><span class='glyphicon glyphicon-minus' aria-hidden='true'></span></button>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                    <div class='newelements'></div>\
                </form>";
                
            } else {

                var columns = [];

                $('.row[data-type="'+type+'"][data-id="'+id+'"] .nav-tabs li').each(function(i) {
                    columns[i] = $(this).text();
                });
                
                    r['content'] = "<form>\
                        <div class='row main' data-type='"+type+"' data-id='"+id+"'>\
                            <div class='col-md-10'>\
                                <input type='text' class='form-control column' placeholder='Aba' value='"+columns[0]+"'>\
                            </div>\
                            <div class='col-md-2'>\
                    ";

                    if (columns.length > 1) {
                        css = "display: none;";
                    }
                    
                    r['content'] += "\
                        <div class='pull-right' style='"+css+"'>\
                            <div class='btn-group' role='group' aria-label=''>\
                                <button type='button' class='btn add-column'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></button>\
                                <button type='button' class='btn remove-column' disabled='disabled'><span class='glyphicon glyphicon-minus' aria-hidden='true'></span></button>\
                            </div>\
                        </div>\
                    ";
                    
                    r['content'] += "\
                            </div>\
                        </div>\
                        <div class='newelements'>\
                    ";

                    $('.row[data-type="'+type+'"][data-id="'+id+'"] .nav-tabs li').each(function(i) {
                        if (i != 0) {

                            if (columns.length == i+1) {
                                css = "display: block;";
                            } else {
                                css = "display: none;";
                            }
                            
                            r['content'] += "\
                                <div class='row' data-type='"+type+"' data-id='"+id+"'>\
                                    <div class='col-md-10'>\
                                        <input type='text' class='form-control column' placeholder='Aba' value='"+columns[i]+"'>\
                                    </div>\
                                    <div class='col-md-2'>\
                                        <div class='pull-right' style='"+css+"'>\
                                            <div class='btn-group' role='group' aria-label=''>\
                                                <button type='button' class='btn add-column'><span class='glyphicon glyphicon-plus' aria-hidden='true'></span></button>\
                                                <button type='button' class='btn remove-column'><span class='glyphicon glyphicon-minus' aria-hidden='true'></span></button>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            ";
                        }
                    });
                                            
                    r['content'] += "\
                        </div>\
                    </form>";
                
            }
            
            break;
    }
    return r;
}

function getData(id) {
    return $gs[id].data('gridstack');
}

function actionWidget(id,action,type) {
    txt = null;
    switch(action) {
        case 'add':
            txt = '<li>\
                        <a href="javascript:;" data-id="'+id+'" data-type="'+type+'" class="add-widget">\
                            <span class="lnr lnr-layers"></span>\
                        </a>\
                    </li>\
            ';
            break;
        case 'remove':
            txt = '<li>\
                        <a href="javascript:;" data-type="'+type+'" data-id="'+id+'" class="remove-widget">\
                            <span class="lnr lnr-trash"></span>\
                        </a>\
                    </li>\
            ';
            break;
        case 'edit':
            txt = '<li>\
                        <a href="javascript:;" data-type="'+type+'" data-id="'+id+'" class="edit-widget">\
                            <span class="lnr lnr-pencil"></span>\
                        </a>\
                    </li>\
            ';
            break;
        case 'move':
            txt = '<li>\
                        <a href="javascript:;" data-type="'+type+'" data-id="'+id+'" class="move-widget">\
                            <span class="lnr lnr-move"></span>\
                        </a>\
                    </li>\
            ';
            break;
    }
    return txt;
} 

function widget(type) {
    var element = null;
    var id = getRandomInt(11111,99999);
    switch (type) {
        case "container":
            element = "<div class='grid-stack-item container' id='grid-stack-" + id + "' data-type='container' data-id='" + id + "'>" +
                    "<div class='grid-stack-item-content' data-id='" + id + "'>" +
                    "<span class='title'>Botões</span>" +
                    "</div>" +
                    "</div>";
            break;
        case "logo":
            element = "<div class='grid-stack-item logo' data-type='logo' data-gs-width='1' data-gs-height='1' data-id='" + id + "'>" +
                    "<div class='grid-stack-item-content' data-id='" + id + "'><span class='title'>Logo</span></div>" +
                    "</div>";
            break;
        case "customer":
            element = "<div class='grid-stack-item customer' data-type='customer' data-gs-width='1' data-gs-height='1' data-id='" + id + "'>" +
                    "<div class='grid-stack-item-content' data-id='" + id + "'><span class='title'>Customers</span></div>" +
                    "</div>";
            break;
        case "employee":
            element = "<div class='grid-stack-item employee' data-type='employee' data-gs-width='1' data-gs-height='1' data-id='" + id + "'>" +
                    "<div class='grid-stack-item-content' data-id='" + id + "'><span class='title'>Employee</span></div>" +
                    "</div>";
            break;
        case "lastsale":
            element = "<div class='grid-stack-item lastsale' data-type='lastsale' data-gs-width='1' data-gs-height='1' data-id='" + id + "'>" +
                    "<div class='grid-stack-item-content' data-id='" + id + "'><span class='title'>Last Sale</span></div>" +
                    "</div>";
            break;
        case "order":
            element = "<div class='grid-stack-item order' data-type='order' data-gs-width='1' data-gs-height='1' data-id='" + id + "'>" +
                    "<div class='grid-stack-item-content' data-id='" + id + "'><span class='title'>Order</span></div>" +
                    "</div>";
            break;
        case "table":
            element = "<div class='grid-stack-item table' data-type='table' data-gs-width='1' data-gs-height='1' data-id='" + id + "'>" +
                    "<div class='grid-stack-item-content' data-id='" + id + "'><span class='title'>Tabela</span></div>" +
                    "</div>";
            break;
        case "abas":
            element = "<div class='grid-stack-item abas' data-type='abas' data-gs-width='1' data-gs-height='1' data-id='" + id + "'>" +
                    "<div class='grid-stack-item-content' data-id='" + id + "'><span class='title'>Abas</span></div>" +
                    "</div>";
            break;
    }
    return element;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function slugify(text){
  return text.toString().toLowerCase()
    .replace(/\s+/g, '-')           // Replace spaces with -
    .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
    .replace(/\-\-+/g, '-')         // Replace multiple - with single -
    .replace(/^-+/, '')             // Trim - from start of text
    .replace(/-+$/, '');            // Trim - from end of text
}

$(function () {
    $(".sidebar .grid-stack-item").each(function(){
        var id = getRandomInt(11111,99999);
        $(this).attr("data-id",id).find(".grid-stack-item-content").attr("data-id",id);
    });
    createGridstack(0, '.grid-stack', '.sidebar .grid-stack-item', 6);
    main();
});

$('body').on('click', '.remove-widget', function(e){
    var grid = getData(0),
        el = $(this).parents().eq(3);
        
        if ($(this).data("type") == "button") {
            grid = getData($(this).data("id"));
            grid.removeWidget(el);
        } else {
            grid.removeWidget(el);
        }
});

$('body').on('click', '.add-widget', function(e){
    var type = $(this).data("type"), grid, id, count, idgridstack;

    if (typeof type != "undefined") {

        if(type == "abas") {
            id = $(this).parents().eq(2).find('.row .tab-content .active').data('id');
            idgridstack = $(this).parents().eq(2).find('.row .tab-content .active').data('idgridstack');
            idelement = parseInt($(this).parents().eq(2).find('.row .tab-content .active .grid-stack-item').size()) + 1;
            id_now = " "+idelement+id;
        } else {
            id = $(this).data("id");
            idelement = parseInt($(this).parents().eq(2).children().eq(1).find('.grid-stack-item').size()) + 1;
            if (type == "container") {
                id_now = " "+idelement+id;
                modal(idModal, $(this), type, id, idelement);
            }
        }
        
        grid = getData(id);
        
        if (type == "container") {
            
            grid.addWidget(
                $('<div data-type="'+type+'"><div class="grid-stack-item-content" data-type="button" data-id="'+id+'" data-idelement="'+idelement+'">\
                    <ul class="buttons">'+actionWidget(id,'edit',type)+actionWidget(id,'remove',type)+'</ul>\
                    <div style="width: 100%; height: 100%; position: absolute;"></div>\
                    <button class="btn" style="width: 100%; height: 100%;">Primary</button>\
                </div></div>'),
                Math.floor(1 + 6 * Math.random()),
                Math.floor(1 + 6 * Math.random()),
                1,
                1
            );
    
        } else if (type == "abas") {
            grid.addWidget(
                $('<div data-type="'+type+'"><div class="grid-stack-item-content" data-type="button_abas" data-id="'+id+'" data-idelement="'+idelement+'">\
                    <ul class="buttons">'+actionWidget(id,'edit','button_abas')+actionWidget(id,'remove',type)+'</ul>\
                    <div style="width: 100%; height: 100%; position: absolute;"></div>\
                    <button class="btn" style="width: 100%; height: 100%;">Primary</button>\
                </div></div>'),
                Math.floor(1 + 6 * Math.random()),
                Math.floor(1 + 6 * Math.random()),
                1,
                1
            ); 
            modal(idModal,$('.grid-stack-item[data-id="'+idgridstack+'"] .grid-stack-item-content[data-id="'+idgridstack+'"] .row[data-id="'+idgridstack+'"] .tab-content .tab-pane.active[data-id="'+id+'"] .grid-stack .grid-stack-item[data-type="abas"] .grid-stack-item-content[data-idelement="'+idelement+'"][data-id="'+id+'"] button'), 'button_abas', id, idelement,idgridstack);
        } else {
            grid.addWidget(
                $('<div data-type="'+type+'"><div class="grid-stack-item-content" data-type="'+type+'" data-id="'+id+'" data-idelement="'+idelement+'"><ul class="buttons">'+actionWidget(id,'edit',type)+actionWidget(id,'remove',type)+'</ul></div></div>'),
                Math.floor(1 + 6 * Math.random()),
                Math.floor(1 + 6 * Math.random()),
                1,
                1
            );
        }
        
    }
    
});

$('body').on('click', '.edit-widget', function(){
    var id = $(this).data("id"),
        type = $(this).data("type");
    modal(idModal,$(this), type, id);
});

$('body').on('click', '.add-column', function(){
    var element = $(this).parents().eq(3),
        newElement = element.clone(),
        count = $('.newelements .row').size() + 1;
        
    newElement.find('input').val("");
        
    $(element).find('.pull-right').hide();
    
    $('.newelements').append(newElement);
    
    
    if (count > 0) {
        $('.newelements .row:last-child .pull-right .btn-group .btn:nth-child(2)').removeAttr("disabled");
    }
    
});

$('body').on('click', '.remove-column', function(){
    var count = $('.newelements .row').size();
    
    $(this).parents().eq(3).remove();
    
    if (count == 1) {
        $('.row.main .pull-right').show();
    } else {
        $('.newelements .row:last-child .pull-right').show();
    }
    
});