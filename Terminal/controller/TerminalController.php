<?php

require_once '../init_autoload.php';

require_once '../libraries/default_helper.php';
require_once '../libraries/enotas-php-client-v2/src/eNotasGW.php';

use eNotasGW\Api\Exceptions as Exceptions;

use Doctrine\ORM\Query\ResultSetMapping;
use NFePHP\NFe\Make;

ini_set('display_errors', 1);

class TerminalController {

    public function __construct() {
        
    }

    public function getPaymentMethods() {
        $em = getEm();
        echo '2';
        $repository = $em->getRepository('Person');
        $query = $repository->createQueryBuilder('p')
            ->select(['p.idperson', 'p.name'])
            ->where('p.name LIKE :sSearch')
            ->setParameter('sSearch', '%'.$_GET['person'].'%')
            ->getQuery();
        _dump($query->getResult(2));
    }

    public function showLog() {
        header('Content-type: application/json');
        if (!isset($_GET['file'])) {
            $_GET['file'] = 'output.txt';
        }
        $data = file_get_contents(__DIR__.DIRECTORY_SEPARATOR.$_GET['file']);
        echo $data;
    }

    // search for customers by name (returns to datatable plugin)
    public function searchCustomers() {
        // ini_set('display_errors', 0);
        header('Content-type: application/json');
        $em = getEm();
        $output_file = __DIR__."/output.txt";
        file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $rsm = new ResultSetMapping;

        $sSearch = $_GET['search']['value'];

        try {
            $repository = $em->getRepository('Person');
            $query = $repository->createQueryBuilder('p')
                ->select(['p.idperson', 'p.name', 'p.documentNumber'])
                ->where('p.name LIKE :sSearch')
                ->orWhere('p.documentNumber LIKE :sSearch')
                ->setParameter('sSearch', '%'.$sSearch.'%')
                ->getQuery();
        } catch(Exception $e) {
            var_dump($e);
        }

        $result = [
            'draw' => $_GET['draw']
        ];

        if ($customers = $query->getResult(2)) {
            $customers_values = array_map('array_values', $customers);
            $result['data'] = array_values($customers_values);
        }
        else {
            $result['data'] = [];
        }

        $result['recordsTotal'] = count($customers);
        $result['recordsFiltered'] = count($customers);

        echo json_encode($result, JSON_PRETTY_PRINT);
        return;
    }

    // search for products by name or product number (returns to datatable plugin)
    public function searchProducts() {
        // ini_set('display_errors', 1);
        header('Content-type: application/json');
        $em = getEm();
        // $output_file = __DIR__."/output.txt";
        // file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $rsm = new ResultSetMapping;

        $sSearch = $_GET['search']['value'];

        $result = [
            'draw' => $_GET['draw']
        ];

        $sql = "SELECT 
            SQL_CALC_FOUND_ROWS
            IF (SC.idsize_color, CONCAT(P.idproduct, '-', SC.idsize_color), P.idproduct) AS productNumber,
            P.name, 
            CONCAT(IFNULL(S.name, ''), ' ', IFNULL(C.name, ''), '', IFNULL(P.description, '')) AS info, 
            CONCAT('R$ ', FORMAT(P.price, 2)) AS price,
            P.idproduct,
            SC.idsize_color
        FROM product P
        LEFT JOIN size_color SC ON (SC.product_idproduct = P.idproduct)
        LEFT JOIN size S ON (S.idsize = SC.size_idsize)
        LEFT JOIN color C ON (C.idcolor = SC.color_idcolor)
        WHERE
            (P.name LIKE '%{$sSearch}%' OR
            P.product_number = '%{$sSearch}%' OR
            P.barcode = '%{$sSearch}%' OR
            SC.codEan = '%{$sSearch}%')
        LIMIT {$_GET['length']} OFFSET {$_GET['start']};";
        // die($sql);
        $stmt = getEm()->getConnection()->prepare($sql);
        $stmt->execute();

        if ($query_result = $stmt->fetchAll()){
            foreach ($query_result as $value) {
                $result['data'][] = array_values($value);
            }
        }

        $_sql = "SELECT FOUND_ROWS() AS 'total'";
        $total_rs = getEm()->getConnection()->prepare($_sql);
        $total_rs->execute();
        $total_result = $total_rs->fetch();

        $result['recordsTotal'] = $total_result['total'];
        $result['recordsFiltered'] = $total_result['total'];

        echo json_encode(utf8ize($result), JSON_PRETTY_PRINT);
        return;
    }

    // search for products by product_id and returns info for main table in the POS terminal screen
    public function searchProductsMainTable() {
        // ini_set('display_errors', 0);
        header('Content-type: application/json');
        $em = getEm();
        $output_file = __DIR__."/output.txt";
        file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $rsm = new ResultSetMapping;

        $product_id = $_GET['product_id'];
        if (strstr($product_id, '-')) {
            $arr = explode('-', $product_id);
            $product_id = $arr[0];
            $size_color_id = $arr[1];
        }

        $sql = "SELECT 
            IF (SC.idsize_color, CONCAT(P.idproduct, '-', SC.idsize_color), P.idproduct) AS productNumber,
            P.idproduct, SC.idsize_color, P.name, P.price, P.description, S.name AS sizeName, C.name AS colorName
        FROM product P
        LEFT JOIN size_color SC ON (SC.product_idproduct = P.idproduct)
        LEFT JOIN size S ON (S.idsize = SC.size_idsize)
        LEFT JOIN color C ON (C.idcolor = SC.color_idcolor)
        WHERE
            (P.idproduct = '{$product_id}' OR
            SC.codEan = '{$product_id}' OR
            P.barcode = '{$product_id}')";
        if (isset($size_color_id) && $size_color_id != '') {
            $sql .= " AND SC.idsize_color = '{$size_color_id}'";
        }
        // die($sql);
        $stmt = getEm()->getConnection()->prepare($sql);
        $stmt->execute();

        // $query_result = $stmt->fetchAll();
        
        // try {
        //     $repository = $em->getRepository('Product');
        //     $query = $repository->createQueryBuilder('p')
        //         ->select(['p.idproduct, p.productNumber', 'p.name', 'p.price', 'p.description'])
        //         ->where('p.idproduct = :product_id')
        //         ->orWhere('p.barcode = :product_id')
        //         ->setParameter('product_id', $product_id)
        //         ->getQuery();
        // } catch(Exception $e) {
        //     var_dump($e);
        // }

        // $result = [
        //     'status' => 200,
        //     'data' => []
        // ];

        // if ($products = $query->getResult(2)) {
        if ($products = $stmt->fetchAll()) {
            // $result['data'] = array_values($products);
            returnJson(200, array_values($products));
        }
        else {
            // $result['status'] = 404;
            returnJson(404);
        }

    }

    // search for parked orders (returns to datatable plugin)
    public function searchParkedOrders() {
        ini_set('display_errors', 1);
        header('Content-type: application/json');
        $em = getEm();
        $output_file = __DIR__."/output.txt";
        file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $result = [
            'draw' => $_GET['draw']
        ];

        if ($orders = $em->getRepository("Order")->findBy(array('orderType' => 0), array('idorder'=>'DESC'), $_GET['length'], $_GET['start'])) {
            $result['data'] = [];
            foreach ($orders as $key => $order) {
                $result['data'][] = [
                    $order->getIdorder(),
                    $order->getExtraInfo(),
                    $order->getUsersusers()->getPersonperson()->getName(),
                    $order->getDateCreate(),
                    $order->getTotalAmount()
                ];
            }
        }
        else {
            $result['data'] = [];
        }

        // if ($orders = $query->getResult(2)) {
        //     $orders_values = array_map('array_values', $orders);
        //     $result['data'] = array_values($orders_values);
        // }
        // else {
        //     $result['data'] = [];
        // }
        $orders_total = $em->getRepository("Order")->findBy(array('orderType' => 0));
        $result['recordsTotal'] = count($orders_total);
        $result['recordsFiltered'] = count($orders_total);

        echo json_encode($result, JSON_PRETTY_PRINT);
        return;
    }

    // returns array with all company info
    private function getCompanyInfo() {
        $em = getEm();

        $company = [];
        $company_info = $em->getRepository("CompanyInfo")->findAll();//$query->getResult();
        foreach ($company_info as $key => $value) {
            $company[$value->getDataType()] = $value->getDataValue();
        }
        return $company;
    }

    /**
    * Generate template to print refund note.
    * This method receives order_id via POST and don't take any arguments
    *
    * @return void
    */
    public function printRefundNote() {
        if (!$order_details = $this->getOrder($_REQUEST['order_id'])) {
            die('Order not found');
        }
        $order = $order_details[0];
        $total = $order_details[1];

        // _dump($order);

        // company info
        $company = $this->getCompanyInfo();

        $array_answer = array (
            'url_base' => URL_BASE,
            'order' => $order,
            'company' => $company,
            'total' => $total
        );

        GenericController::template2("Terminal", "terminal", "print-refund-note", null, $array_answer, 350);
    }

    /**
    * Generate template to print orde receipt.
    * This method receives order_id via POST and don't take any arguments
    *
    * @return void
    */
    public function printReceipt() {
        if (!$order_details = $this->getOrder($_REQUEST['order_id'])) {
            die('Order not found');
        }
        $order = $order_details[0];
        $total = $order_details[1];

        $order_lines = $this->getOrderLines($_REQUEST['order_id'], false);

        $order_nota = getEm()->getRepository('OrderNota')->findBy(['orderIdorder'=>$_REQUEST['order_id']]);

        // company info
        $company = $this->getCompanyInfo();

        if(isset($_REQUEST['ignore_products']) && $_REQUEST['ignore_products'] != ""){
            if(strpos($_REQUEST['ignore_products'], ',')){
                $ignore_order_lines = explode(',', $_REQUEST['ignore_products']);
            } else {
                $ignore_order_lines = [$_REQUEST['ignore_products']];
            }
        } else {
            $ignore_order_lines = [];
        }

        $array_answer = array (
            'url_base' => URL_BASE,
            'order' => $order,
            'company' => $company,
            'total' => $total,
            'order_lines' => $order_lines,
            'ignore_order_lines' => $ignore_order_lines,
            'order_nota' => $order_nota
        );

        GenericController::template2("Terminal", "terminal", "print-receipt", null, $array_answer, 350);
    }

    public function getProduct($product_id = false,$do_echo = true) {
        $em = getEm();
        
        if($product_id){
            $products = getEm()->getRepository("Product")->findBy(array('active' => 1, 'idproduct' => $product_id));
        }
        else{
            $products = getEm()->getRepository("Product")->findBy(array('active' => 1));
        }

        $data = array ();

        foreach ($products as $value){
            $dat = array (
                "id" => $value->getIdproduct(),
                "name" => $value->getName(),
                "product_number" => $value->getProductNumber(),
                "price" => $value->getPrice()
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        
        return $data;

    }

    // newest method to pay for order
    public function pay() {
        header('Content-type: application/json');
        // save into log
        // $output_file = __DIR__."/output.txt";
        // file_put_contents($output_file, json_encode($_POST, JSON_PRETTY_PRINT));

        // retrieve current order
        $em = getEm();

        if (!$order = $em->find("Order", $_REQUEST['order_id'])) {
            echo json_encode(['status'=>200]);
            return false;
        }

        $_REQUEST['customer_id'] = isset($_REQUEST['customer_id']) ? $_REQUEST['customer_id'] : 1;
        if ($customer = $em->find('Person', $_REQUEST['customer_id'])) {
            $order->setCustomer($customer);
        }

        // default definitions
        $order->setInUse(0);
        if (isset($_REQUEST['refund_note']) && $_REQUEST['refund_note'] != 1) {
            $order->setCard($_REQUEST['cartao']);
            $order->setViacliente($_REQUEST['viacliente']);
            $order->setVialoja($_REQUEST['vialoja']);
        }
        
        $proceed_payment = false;
        $total_amount = 0.0;

        // set as refund note
        if (@$_REQUEST['refund_note'] == 1) {
            $order->setRefundNote(1);
            $order->setStatus(4);
        }
        // park order
        else if (@$_REQUEST['park_order'] == 1) {
            $order->setOrderType(0);
            $order->setExtraInfo($_REQUEST['extra_info']);
        }
        // regular order payment
        else {
            $order->setOrderType(1);
            $order->setStatus(2);
            $proceed_payment = true;
        }

        // remove existing orderlines, probably not useful for production
        $orderlines = $em->getRepository('OrderLine')->findBy(['orderorder'=>$order]);
        foreach ($orderlines as $key => $value) {
            $em->remove($value);
            $em->flush();
        }

        // insert orderlines
        $date = new DateTime();
        foreach ($_POST['products'] as $key => $product) {
            $product_obj = $em->find("Product", $product['idproduct']);//->findBy(['active' => 1, 'idproduct' => $product['idproduct']]);
            $product['price'] = $product['price'] == '' ? 0 : $product['price'];
            $product['totalPrice'] = (float)$product['price'] * (int)$product['qty'];

            $order_line = new OrderLine();

            $order_line->setOrder($order);
            $order_line->setProduct($product_obj);
            $order_line->setQty($product['qty']);
            $order_line->setValue($product['price']);
            $order_line->setActualValue($product['totalPrice']);
            $order_line->setDiscount($product['discount']);
            // $order_line->setTax($tax);
            $order_line->setActive(1);
            $order_line->setDateCreate($date);
            // register size_color_id
            if ($product['idsize_color']) {
                $size_color_obj = $em->find("SizeColor", $product['idsize_color']);
                $order_line->setSizecolorsizecolor($size_color_obj);
                // update stock
                $total_amount_in_stock = (int)$size_color_obj->getTotalAmount() - (int)$product['qty'];
                $size_color_obj->setTotalAmount($total_amount_in_stock);
                getEm()->persist($size_color_obj);
                getEm()->flush();
            }
            else {
                // update stock
                $total_amount_in_stock = (int)$product_obj->getTotalAmount() - (int)$product['qty'];
                $product_obj->setTotalAmount($total_amount_in_stock);
                getEm()->persist($product_obj);
                getEm()->flush();
            }
            
            getEm()->persist($order_line);

            getEm()->flush();

            $total_amount += (float)$product['totalPrice'];
        }

        $order->setTotalAmount($total_amount);

        // update order
        getEm()->persist($order);
        getEm()->flush();

        if ($proceed_payment == true) {
            // insert payment
            foreach ($_POST['payment_methods'] as $key => $value) {
                $payment_method_obj = $em->find("PaymentMtd", $key);

                $order_payment = new OrderPayment();

                $order_payment->setOrderorder($order);
                $order_payment->setPaymentMtdpaymentMtd($payment_method_obj);
                $order_payment->setValue((float)$value);
                $order_payment->setActive(1);
                $order_payment->setDateCreate($date);
                getEm()->persist($order_payment);

                getEm()->flush();
            }
            // check if any refund notes were sent
            if (isset($_POST['refund_note_list'])) {
                foreach ($_POST['refund_note_list'] as $key => $value) {
                    $payment_method_obj = $em->find("PaymentMtd", 9999);

                    $order_payment = new OrderPayment();

                    $order_payment->setOrderorder($order);
                    $order_payment->setPaymentMtdpaymentMtd($payment_method_obj);
                    $order_payment->setValue((float)$value);
                    $order_payment->setActive(1);
                    $order_payment->setDateCreate($date);
                    // $order_payment->setCreditNoteID($key);
                    getEm()->persist($order_payment);

                    getEm()->flush();
                }
            }
        }

        returnJson(200);
        // echo json_encode(['status'=>200]);
    }

    public function getLastOrder($user_id = false,$do_echo = true) {
        $em = getEm();
        
        if($user_id){
            $user = getEm()->getRepository("Users")->findBy(array('active' => 1, 'idusers' => $user_id));
            if($user[0]){
                $orders = getEm()->getRepository("Order")->findBy(array('active' => 1, 'usersusers' => $user[0]),array('idorder'=>'desc'));
            }
        }
        else{
            $orders = getEm()->getRepository("Order")->findBy(array('active' => 1),array('idorder'=>'desc'));
        }

        $last_order = null;

        if($orders){
            foreach($orders as $order){
                if($order->getStatus()!=0){
                    $last_order = $order->getIdorder();
                    break;
                }
            }
        }
        
        if($do_echo){
            echo $last_order;
        }
        
        return $last_order;
    }

    public function getOrderLines($order_id, $json=true){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();

            $orders = getEm()->getRepository("Order")->findBy(array('active' => 1, 'idorder' => $order_id));

            $data = array ();
            $order = [];

            foreach ($orders as $value){
                $order = [
                    'idorder' => $value->getIdOrder(),
                    'extra_info' => $value->getExtraInfo()
                ];
                $order_lines = getEm()->getRepository("OrderLine")->findBy(array('active' => 1, 'orderorder' => $value));
                foreach ($order_lines as $value2){
                    $size = $color = '';
                    $discount = $value2->getDiscount() ? $value2->getDiscount() : 0;
                    $size = $color = $size_id_label = "";
                    if ($value2->getSizecolorsizecolor()){
                        $size = ' - '.$value2->getSizecolorsizecolor()->getSizesize()->getName();
                        $size_id_label = '-'.$value2->getSizecolorsizecolor()->getIdsizeColor();
                        // TODO: check if color exists and include it, if needed
                        //$color = $value2->getSizecolorsizecolor()->getColorcolor()->getName();
                    }
                    
                    $dat = array (
                        "id" => $value2->getIdorderLine(),
                        "description" => $value2->getProductproduct()->getName(),
                        "description_with_variety" => $value2->getProductproduct()->getName().$size,//." ".$color,
                        "product_number" => $value2->getProductproduct()->getProductnumber(),
                        "product_id" => $value2->getProductproduct()->getIdproduct(),
                        "size_id_label" => $size_id_label,
                        "cfop" => $value2->getProductproduct()->getCfop(),
                        "ncm" => $value2->getProductproduct()->getNcm(),
                        "qtrib" => $value2->getProductproduct()->getQtrib(),
                        "variety" => $size,//." ".$color,
                        "discount" => $discount, 
                        "qty" => $value2->getQty(),
                        "price" => $value2->getValue(),
                        "info" => $value2->getExtraInfo(),
                        "amount" => $value2->getAmount(),
                        "amount_inc_tax" => $value2->getAmountIncTax(),
                        "barcode" => $value2->getProductproduct()->getBarcode()
                    );
                    array_push($data, $dat);
                }
            }

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = '';
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        if ($json == true) {
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data,
                "order"   => $order
            );

            $json_data = json_encode($data);
            print $json_data;
        }
        else {
            return $data;
        }
         
    }

    /**
    * Get order details and total amount.
    *
    * @param int $order_id ID for a particular order
    * @param int $json Specifies if the return value should be json or array
    *
    * @return mixed[] Array | boolean
    */
    public function getOrder($order_id, $json=0) {
        // get order
        $em = getEm();
        $rsm = new ResultSetMapping;
        if (!$order = $em->find("Order", $order_id)) {
            return false;
        }

        // get orderlines and total amount
        $total = 0.0;
        $orderlines = $em->getRepository('OrderLine')->findBy(['orderorder'=>$order]);
        foreach ($orderlines as $key => $value) {
            $total += abs($value->getValue() * $value->getQty());
        }

        if ($json == 1) {
            header('Content-type: application/json');
            echo json_encode([[
                'idorder' => $order->getIdOrder(),
                'users_idusers' => $order->getUsersusers()->getIdusers(),
                'status' => $order->getStatus(),
                'extra_info' => $order->getExtraInfo()
            ], $total], JSON_PRETTY_PRINT);
            return true;
        }
        else {
            return [$order, $total, $orderlines];
        }
    }
    public function getCustomer($person_id = false,$do_echo = true) {
        $em = getEm();
        
        if($person_id){
            $customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'idperson' => $person_id));
        }
        else{
            $customers = getEm()->getRepository("Person")->findBy(array('active' => 1));
            //$customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'customer_gp_idcustomer_gp' => 'IS NOT NULL'));

            // $qb = $this->createQueryBuilder('SELECT * FROM Person');
            // $qb->where('active = 1');
            // $qb->where('customer_gp_idcustomer_gp IS NOT NULL');

            // $customers = $qb->getQuery();

            // $rsm = "";
            // $query = $em->createNativeQuery('SELECT * FROM Person WHERE active = 1, customer_gp_idcustomer_gp IS NOT NULL');
            // $query->setParameter(1, 'romanb');

            // $customers = $query->getResult();

        }

        $data = array ();

        foreach ($customers as $value){
            if($phone = getEm()->getRepository("PersonData")->findBy(array('active' => 1, 'personperson' => $value, 'dataType' => 'Phone'))){
                $phone = $phone[0]->getDataValue();
            }else{
                $phone = "";
            }

            $dat = array (
                "id" => $value->getIdperson(),
                "name" => $value->getName(),
                "phone" => $phone
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        return $data;

    }
    public function getUser($user_id = false,$do_echo = true) {
        $em = getEm();
        
        if($user_id){
            $users = getEm()->getRepository("Users")->findBy(array('active' => 1, 'iduser' => $user_id));
        }
        else{
            $users = getEm()->getRepository("Users")->findBy(array('active' => 1));
            //$customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'customer_gp_idcustomer_gp' => 'IS NOT NULL'));

            // $qb = $this->createQueryBuilder('SELECT * FROM Person');
            // $qb->where('active = 1');
            // $qb->where('customer_gp_idcustomer_gp IS NOT NULL');

            // $customers = $qb->getQuery();

            // $rsm = "";
            // $query = $em->createNativeQuery('SELECT * FROM Person WHERE active = 1, customer_gp_idcustomer_gp IS NOT NULL');
            // $query->setParameter(1, 'romanb');

            // $customers = $query->getResult();

        }

        $data = array ();

        foreach ($users as $value){
            //$phone = getEm()->getRepository("PersonData")->findBy(array('active' => 1, 'personperson' => $value));

            $dat = array (
                "id" => $value->getIdusers(),
                "name" => $value->getLogin()
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        return $data;

    }
    
    public function index(){
        $em = getEm();

        $products = $this->getProduct(false,false);
        $customers = $this->getCustomer(false,false);
        $users = $this->getUser(false,false);

        // get pos session
        $pos = $em->getRepository("Pos")->findBy(array('active' => 1, 'idpos' => $_SESSION['pos']));

        // payment methods
        try {
            $repository = $em->getRepository('PaymentMtd');
            $query = $repository->createQueryBuilder('p')
                ->select(['p.idpaymentMtd', 'p.name'])
                ->where('p.idpaymentMtd != :return_note')
                ->setParameter('return_note', '9999')
                ->getQuery();
        } catch(Exception $e) {
            var_dump($e);
        }

        $payment_methods = $query->getResult(2);

        $array_answer = array (
            '0'=>'',
            'payment_methods' => $payment_methods,
            'products'=>$products,
            'customers'=>$customers,
            'users'=>$users,
            'url_base' => URL_BASE
        ); 

        $navbar = null;
    
        GenericController::template("Terminal", "terminal","index", $navbar, $array_answer, 350);
    }

    public function startOrder($user_id){
        header('Content-type: application/json');
        try {
            $begin = getEm()->getConnection()->beginTransaction();

            $em = getEm();
            $user = $em->getRepository("Users")->findBy(array('active' => 1, 'idusers' =>$user_id));
            $pos_session = $em->getRepository("PosSession")->findBy(array('idposSession' => 1));//este parametro esta assim por motivos de teste

            $order = new Order();

            $order->setUsersusers($user[0]);
            $order->setActive(1);
            $order->setPosSessionposSession($pos_session[0]);
            $order->setTotalAmount(0);
            $order->setInUse(1);
            $order->setRefundNote(0);
            $order->setOrderType(1);// order by default
            $order->setStatus(0);// open order

            getEm()->persist($order);

            getEm()->flush();

            $commit = getEm()->getConnection()->commit();
            // $result  = 'success';
            // $message = 'query success';
            $data = $order->getIdorder();
            $status = 200;
            $extra = false;

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            // $result  = 'error';
            $extra = $e->getMessage();
            $data = "";
            $status = 500;
        }    

        // $data = array(
        //     "result"  => $result,
        //     "message" => $message,
        //     "data"    => $data
        // );


        // $json_data = json_encode($data);
        // print $json_data;

        returnJson($status, $data, $extra);
        
    }

    private function printFloat($value) {
        return number_format($value, 2, '.', '');
    }

    private function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 0)
            return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    // generates unique token. returns string.
    private function get_token($length = 22, $include_alpha = false, $upper = true) {
        $token = "";
        $codeAlphabet = "123456789";
        if ($include_alpha == true) {
            $codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
        }
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        if ($upper)
            $token = strtoupper($token);
        return $token;
    }

    // generates and print nota fiscal for a specific order
    public function generateNotaFiscal($order_id) {
        // header('Content-type: application/json');
        // return;
        $em = getEm();
        $itens = $cliente = [];
        if (!$order = $em->find("Order", $order_id)) {
            die('Erro ao gerar nota: ordem não encontrada');
        }
        $sales_person = $order->getUsersusers()->getPersonperson()->getName();
        $total = $order->getTotalAmount();
        $order_lines = $this->getOrderLines($order_id, false);
        // _dump($order_lines);
        // return;
        foreach ($order_lines as $key => $value) {
            $itens[] = [
                'cfop' => ''.$value['cfop'],
                'codigo' => ''.$value['product_id'],
                'descricao' => ''.$value['description_with_variety'],
                'ncm' => ''.$value['ncm'],
                'quantidade' => ''.$value['qty'],
                'unidadeMedida' => 'PECA',
                'valorUnitario' => ''.$value['price'],
                'impostos' => array(
                    'icms' => array (
                        'situacaoTributaria' => '400',
                        'origem' => 0, //0 - Nacional
                        'aliquota' => ''.$value['qtrib']
                    )
                )
            ];
        }

        // monta array do eNotas
        $eNotasParams = [
            // identificador único da requisição de emissão de nota fiscal 
            // (normalmente será preenchido com o id único do registro no sistema de origem)
            'id' => ''.$order_id,
            'ambienteEmissao' => 'Producao', //'Producao' ou 'Homologacao'
            'pedido' => array(
                'presencaConsumidor' => 'OperacaoPresencial',
                'pagamento' => array(
                    'tipo' => 'PagamentoAVista',
                    'formas' => array(
                        array(
                            'tipo' => 'Dinheiro',
                            'valor' => ''.$total
                        )
                    )
                )
            ),
            'itens' => $itens,
            'cliente' => null,
            'informacoesAdicionais' => 'PROCON || Endereço: R. Ulisses Caldas, no 181 - Cidade Alta, Natal - RN, 59025090 || Telefone: (84) 3232-9050/ 3232-9051 || Vendedor: '.$sales_person.' || Tipo Pag: 01; Valor: '.number_format($total, 2, ',', '.').'; Troco: 0; '.date('H:i:s')
        ];

        if ($order->getPersonperson() && ($cpf = $order->getPersonperson()->getDocumentNumber())) {
            $eNotasParams['cliente'] = [
                'nome' => ''.$order->getPersonperson()->getName(),
                'cpfCnpj' => ''.$cpf
            ];
        }
        // die(_dump($cliente));
        $settings = $this->getCompanyInfo();
        // return false;
        eNotasGW::configure(array(
            'apiKey' => 'NjIwMmE0OTItOTdkMS00NGE0LWI1NTItNmU4YThmMzcwNDAw'
        ));

        $idEmpresa = '10E298AB-599D-4D31-B1F4-4398663B0400';

        
        // _dump($eNotasParams);
        // die();
        // return;
        try
        {
            $result = eNotasGW::$NFeConsumidorApi->emitir($idEmpresa, $eNotasParams);

            // save nota into the database
            $order_nota = new OrderNota();
            $order_nota->setorderIdorder($order_id);
            $order_nota->setnotaNumber($result->numero);
            $order_nota->setnotaSerie($result->serie);
            $order_nota->setnotaKey($result->chaveAcesso);
            $order_nota->setnotaXml(file_get_contents($result->linkDownloadXml));
            $order_nota->setnotaPdf($result->linkDanfe);
            $order_nota->setreturnXml($result->status);
            $order_nota->setDateCreate(new DateTime());

            getEm()->persist($order_nota);
            getEm()->flush();

            // echo 'Sucesso!';
            // header('Content-Type: application/json');
            // echo json_encode($result, JSON_PRETTY_PRINT);
            // saves nota info
            file_put_contents(__DIR__.'/nota.json', json_encode($result, JSON_PRETTY_PRINT));
            // return true;
            //Load file content
            $pdf_content = file_get_contents($result->linkDanfe);
            //Specify that the content has PDF Mime Type
            header("Content-Type: application/pdf");
            //Display it
            echo $pdf_content;
            return true;
        }
        catch(Exceptions\invalidApiKeyException $ex) {
            echo 'Erro de autenticação: </br></br>';
            echo $ex->getMessage();
        }
        catch(Exceptions\unauthorizedException $ex) {
            echo 'Acesso negado: </br></br>';
            echo $ex->getMessage();
        }
        catch(Exceptions\apiException $ex) {
            echo 'Erro de validação: </br></br>';
            echo $ex->getMessage();
        }
        catch(Exceptions\requestException $ex) {
            echo 'Erro na requisição web: </br></br>';

            echo 'Requested url: ' . $ex->requestedUrl;
            echo '</br>';
            echo 'Response Code: ' . $ex->getCode();
            echo '</br>';
            echo 'Message: ' . $ex->getMessage();
            echo '</br>';
            echo 'Response Body: ' . $ex->responseBody;
        }
        // GenericController::template2("Terminal", "terminal", "nota-fiscal", null, [], 350);
    }

    public function utilInsertProducts() {
        $em = getEm();
        $size_array = $em->getRepository("Size")->findBy(array('active' => 1));
        $sizes = [];
        foreach ($size_array as $key => $size) {
            $sizes[$size->getName()] = $size->getIdsize();
        }
        // _dump($sizes);

        $sql = "";
        $id = 200;
        $sku = [];
        if (($handle = fopen(__DIR__."/itens.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $price = str_replace(',', '.', $data[8]);
                $ncm = str_replace('.', '', $data[9]);
                // if product is already registered, registers only size/color
                if (!in_array(trim($data[1]), $sku)) {
                    $id = str_replace('?', '', $data[0]);
                    
                    $sql .= "INSERT INTO product(idproduct, name, sku, barcode, price, ncm, qtrib, cfop, create_by)";
                    $sql .= " VALUES('{$id}', '{$data[3]}', '{$data[1]}', '', '{$price}', '{$ncm}', '{$data[13]}', '{$data[12]}', '1');<br>";
                    $sku[] = trim($data[1]);
                }
                // $size_name = (string)$data[5];
                // if (preg_match('/([0-9]{1,})/', $data[5])) {
                //     $size_name = (int)$data[5];
                // }
                if (!isset($sizes[$data[5]])) {
                    echo $data[5].' - ';
                    continue;
                }
                $size_id = $sizes[$data[5]];
                $sql .= "INSERT INTO size_color(product_idproduct, size_idsize, codEan, active)";
                $sql .= " VALUES('{$id}', '{$size_id}', '{$data[2]}', '1');<br>";
            }
            fclose($handle);
        }
        else {
            echo 'Could not open file';
        }
        echo $sql;
    }

    public function printAnything() {
        // $content = $_POST['content'];
        GenericController::template2("Terminal", "terminal", "print-anything", null, $_GET, 350);
    }

    public function updatePrintFiles() {
        $viacliente = $_POST['viacliente'];
        $vialoja = $_POST['vialoja'];

        file_put_contents(__DIR__.'/viacliente.txt', $viacliente);
        file_put_contents(__DIR__.'/vialoja.txt', $vialoja);

        echo 'arquivos de impressão atualizados com sucesso';
        
    }

}






























