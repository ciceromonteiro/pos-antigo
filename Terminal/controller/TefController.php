<?php
ini_set('display_errors', 1);
require_once '../init_autoload.php';

require_once '../libraries/default_helper.php';
require_once '../libraries/sped-nfe/vendor/autoload.php';

use Doctrine\ORM\Query\ResultSetMapping;
use NFePHP\NFe\Make;

class TerminalController {

    public function __construct() {
        
    }

    public function getPaymentMethods() {
        $em = getEm();
        echo '2';
        $repository = $em->getRepository('Person');
        $query = $repository->createQueryBuilder('p')
            ->select(['p.idperson', 'p.name'])
            ->where('p.name LIKE :sSearch')
            ->setParameter('sSearch', '%'.$_GET['person'].'%')
            ->getQuery();
        _dump($query->getResult(2));
    }

    public function showLog() {
        header('Content-type: application/json');
        $data = file_get_contents(__DIR__.'/output.txt');
        echo $data;
    }

    // search for customers by name (returns to datatable plugin)
    public function searchCustomers() {
        // ini_set('display_errors', 0);
        header('Content-type: application/json');
        $em = getEm();
        $output_file = __DIR__."/output.txt";
        file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $rsm = new ResultSetMapping;

        $sSearch = $_GET['search']['value'];

        try {
            $repository = $em->getRepository('Person');
            $query = $repository->createQueryBuilder('p')
                ->select(['p.idperson', 'p.name'])
                ->where('p.name LIKE :sSearch')
                ->setParameter('sSearch', '%'.$sSearch.'%')
                ->getQuery();
        } catch(Exception $e) {
            var_dump($e);
        }

        $result = [
            'draw' => $_GET['draw']
        ];

        if ($customers = $query->getResult(2)) {
            $customers_values = array_map('array_values', $customers);
            $result['data'] = array_values($customers_values);
        }
        else {
            $result['data'] = [];
        }

        $result['recordsTotal'] = count($customers);
        $result['recordsFiltered'] = count($customers);

        echo json_encode($result, JSON_PRETTY_PRINT);
        return;
    }

    // search for products by name or product number (returns to datatable plugin)
    public function searchProducts() {
        // ini_set('display_errors', 1);
        header('Content-type: application/json');
        $em = getEm();
        // $output_file = __DIR__."/output.txt";
        // file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $rsm = new ResultSetMapping;

        $sSearch = $_GET['search']['value'];
        
        try {
            $repository = $em->getRepository('Product');
            $query = $repository->createQueryBuilder('p')
                ->select(['p.productNumber', 'p.name', 'p.description', 'p.price'])
                ->where('p.name LIKE :name')
                ->orWhere('p.productNumber = :product_number')
                ->setParameter('name', '%'.$sSearch.'%')
                ->setParameter('product_number', $sSearch)
                ->setFirstResult( $_GET['start'] )
                ->setMaxResults( $_GET['length'] )
                ->getQuery();
        } catch(Exception $e) {
            var_dump($e);
            die();
        }

        try {
            $repository = $em->getRepository('Product');
            $query_total = $repository->createQueryBuilder('p')
                ->select(['p.productNumber', 'p.name', 'p.description', 'p.price'])
                ->where('p.name LIKE :name')
                ->orWhere('p.productNumber = :product_number')
                ->setParameter('name', '%'.$sSearch.'%')
                ->setParameter('product_number', $sSearch)
                ->getQuery();
        } catch( Exception $e) {
            var_dump($e);
            die();
        }


        $result = [
            'draw' => $_GET['draw']
        ];

        if ($products = $query->getResult(2)) {
            $products_values = array_map('array_values', $products);
            $result['data'] = array_values($products_values);
            $products_total = $query_total->getResult(2);
        }
        else {
            $result['data'] = [];
        }

        $result['recordsTotal'] = count($products_total);
        $result['recordsFiltered'] = count($products_total);

        echo json_encode(utf8ize($result), JSON_PRETTY_PRINT);
        return;
    }

    // search for products by product_no and returns info for main table in the POS terminal screen
    public function searchProductsMainTable() {
        // ini_set('display_errors', 0);
        header('Content-type: application/json');
        $em = getEm();
        $output_file = __DIR__."/output.txt";
        file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $rsm = new ResultSetMapping;

        $product_no = $_GET['product_no'];
        
        try {
            $repository = $em->getRepository('Product');
            $query = $repository->createQueryBuilder('p')
                ->select(['p.idproduct, p.productNumber', 'p.name', 'p.price', 'p.description'])
                ->where('p.productNumber = :product_number')
                ->orWhere('p.barcode = :product_number')
                ->setParameter('product_number', $product_no)
                ->getQuery();
        } catch(Exception $e) {
            var_dump($e);
        }

        // $result = [
        //     'status' => 200,
        //     'data' => []
        // ];

        if ($products = $query->getResult(2)) {
            // $result['data'] = array_values($products);
            returnJson(200, array_values($products));
        }
        else {
            // $result['status'] = 404;
            returnJson(404);
        }

    }

    // search for parked orders (returns to datatable plugin)
    public function searchParkedOrders() {
        ini_set('display_errors', 1);
        header('Content-type: application/json');
        $em = getEm();
        $output_file = __DIR__."/output.txt";
        file_put_contents($output_file, json_encode($_GET, JSON_PRETTY_PRINT));

        $result = [
            'draw' => $_GET['draw']
        ];

        if ($orders = $em->getRepository("Order")->findBy(array('orderType' => 0), array('idorder'=>'DESC'), $_GET['length'], $_GET['start'])) {
            $result['data'] = [];
            foreach ($orders as $key => $order) {
                $result['data'][] = [
                    $order->getIdorder(),
                    $order->getExtraInfo(),
                    $order->getUsersusers()->getPersonperson()->getName(),
                    $order->getDateCreate(),
                    $order->getTotalAmount()
                ];
            }
        }
        else {
            $result['data'] = [];
        }

        // if ($orders = $query->getResult(2)) {
        //     $orders_values = array_map('array_values', $orders);
        //     $result['data'] = array_values($orders_values);
        // }
        // else {
        //     $result['data'] = [];
        // }
        $orders_total = $em->getRepository("Order")->findBy(array('orderType' => 0));
        $result['recordsTotal'] = count($orders_total);
        $result['recordsFiltered'] = count($orders_total);

        echo json_encode($result, JSON_PRETTY_PRINT);
        return;
    }

    // returns array with all company info
    private function getCompanyInfo() {
        $em = getEm();

        $company = [];
        $company_info = $em->getRepository("CompanyInfo")->findAll();//$query->getResult();
        foreach ($company_info as $key => $value) {
            $company[$value->getDataType()] = $value->getDataValue();
        }
        return $company;
    }

    /**
    * Generate template to print refund note.
    * This method receives order_id via POST and don't take any arguments
    *
    * @return void
    */
    public function printRefundNote() {
        if (!$order_details = $this->getOrder($_REQUEST['order_id'])) {
            die('Order not found');
        }
        $order = $order_details[0];
        $total = $order_details[1];

        // company info
        $company = $this->getCompanyInfo();

        $array_answer = array (
            'url_base' => URL_BASE,
            'order' => $order,
            'company' => $company,
            'total' => $total
        );

        GenericController::template2("Terminal", "terminal", "print-refund-note", null, $array_answer, 350);
    }

    /**
    * Generate template to print orde receipt.
    * This method receives order_id via POST and don't take any arguments
    *
    * @return void
    */
    public function printReceipt() {
        if (!$order_details = $this->getOrder($_REQUEST['order_id'])) {
            die('Order not found');
        }
        $order = $order_details[0];
        $total = $order_details[1];

        $order_lines = $this->getOrderLines($_REQUEST['order_id'], false);

        // company info
        $company = $this->getCompanyInfo();

        $array_answer = array (
            'url_base' => URL_BASE,
            'order' => $order,
            'company' => $company,
            'total' => $total,
            'order_lines' => $order_lines
        );

        GenericController::template2("Terminal", "terminal", "print-receipt", null, $array_answer, 350);
    }

    public function getProduct($product_id = false,$do_echo = true) {
        $em = getEm();
        
        if($product_id){
            $products = getEm()->getRepository("Product")->findBy(array('active' => 1, 'idproduct' => $product_id));
        }
        else{
            $products = getEm()->getRepository("Product")->findBy(array('active' => 1));
        }

        $data = array ();

        foreach ($products as $value){
            $dat = array (
                "id" => $value->getIdproduct(),
                "name" => $value->getName(),
                "product_number" => $value->getProductNumber(),
                "price" => $value->getPrice()
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        
        return $data;

    }

    // newest method to pay for order
    public function pay() {
        header('Content-type: application/json');
        // save into log
        // $output_file = __DIR__."/output.txt";
        // file_put_contents($output_file, json_encode($_POST, JSON_PRETTY_PRINT));

        // retrieve current order
        $em = getEm();

        if (!$order = $em->find("Order", $_REQUEST['order_id'])) {
            echo json_encode(['status'=>200]);
            return false;
        }

        // default definitions
        $order->setInUse(0);
        $proceed_payment = false;
        $total_amount = 0.0;

        // set as refund note
        if (@$_REQUEST['refund_note'] == 1) {
            $order->setRefundNote(1);
            $order->setStatus(4);
        }
        // park order
        else if (@$_REQUEST['park_order'] == 1) {
            $order->setOrderType(0);
            $order->setExtraInfo($_REQUEST['extra_info']);
        }
        // regular order payment
        else {
            $order->setOrderType(1);
            $order->setStatus(2);
            $proceed_payment = true;
        }

        // remove existing orderlines, probably not useful for production
        $orderlines = $em->getRepository('OrderLine')->findBy(['orderorder'=>$order]);
        foreach ($orderlines as $key => $value) {
            $em->remove($value);
            $em->flush();
        }

        // insert orderlines
        $date = new DateTime();
        foreach ($_POST['products'] as $key => $product) {
            $product_obj = $em->find("Product", $product['idproduct']);//->findBy(['active' => 1, 'idproduct' => $product['idproduct']]);
            $product['price'] = $product['price'] == '' ? 0 : $product['price'];
            $product['totalPrice'] = $product['totalPrice'] == '' ? 0 : $product['totalPrice'];

            $order_line = new OrderLine();

            $order_line->setOrder($order);
            $order_line->setProduct($product_obj);
            $order_line->setQty($product['qty']);
            $order_line->setValue($product['price']);
            $order_line->setActualValue($product['totalPrice']);
            $order_line->setDiscount($product['discount']);
            // $order_line->setTax($tax);
            $order_line->setActive(1);
            $order_line->setDateCreate($date);
            getEm()->persist($order_line);

            getEm()->flush();

            $total_amount += (float)$product['totalPrice'];
        }

        $order->setTotalAmount($total_amount);

        if ($proceed_payment == true) {
            // insert payment
            foreach ($_POST['payment_methods'] as $key => $value) {
                $payment_method_obj = $em->find("PaymentMtd", $key);

                $order_payment = new OrderPayment();

                $order_payment->setOrderorder($order);
                $order_payment->setPaymentMtdpaymentMtd($payment_method_obj);
                $order_payment->setValue((float)$value);
                $order_payment->setActive(1);
                $order_payment->setDateCreate($date);
                getEm()->persist($order_payment);

                getEm()->flush();
            }
            // check if any refund notes were sent
            if (isset($_POST['refund_note_list'])) {
                foreach ($_POST['refund_note_list'] as $key => $value) {
                    $payment_method_obj = $em->find("PaymentMtd", 9999);

                    $order_payment = new OrderPayment();

                    $order_payment->setOrderorder($order);
                    $order_payment->setPaymentMtdpaymentMtd($payment_method_obj);
                    $order_payment->setValue((float)$value);
                    $order_payment->setActive(1);
                    $order_payment->setDateCreate($date);
                    // $order_payment->setCreditNoteID($key);
                    getEm()->persist($order_payment);

                    getEm()->flush();
                }
            }
        }

        // update order
        getEm()->persist($order);
        getEm()->flush();

        returnJson(200);
        // echo json_encode(['status'=>200]);
    }

    public function getLastOrder($user_id = false,$do_echo = true) {
        $em = getEm();
        
        if($user_id){
            $user = getEm()->getRepository("Users")->findBy(array('active' => 1, 'idusers' => $user_id));
            if($user[0]){
                $orders = getEm()->getRepository("Order")->findBy(array('active' => 1, 'usersusers' => $user[0]),array('idorder'=>'desc'));
            }
        }
        else{
            $orders = getEm()->getRepository("Order")->findBy(array('active' => 1),array('idorder'=>'desc'));
        }

        $last_order = null;

        if($orders){
            foreach($orders as $order){
                if($order->getStatus()!=0){
                    $last_order = $order->getIdorder();
                    break;
                }
            }
        }
        
        if($do_echo){
            echo $last_order;
        }
        
        return $last_order;
    }

    public function getOrderLines($order_id, $json=true){
        try {
            $begin = getEm()->getConnection()->beginTransaction();
            $em = getEm();
            

            $orders = getEm()->getRepository("Order")->findBy(array('active' => 1, 'idorder' => $order_id));

            $data = array ();
            $order = [];

            foreach ($orders as $value){
                $order = [
                    'idorder' => $value->getIdOrder(),
                    'extra_info' => $value->getExtraInfo()
                ];
                $order_lines = getEm()->getRepository("OrderLine")->findBy(array('active' => 1, 'orderorder' => $value));
                foreach ($order_lines as $value2){
                    $discount = $value2->getDiscount() ? $value2->getDiscount() : 0;
                    $size = $color = "";
                    if($value2->getSizecolorsizecolor()){
                        $size = $value2->getSizecolorsizecolor()->getSizesize()->getName();
                        $color = $value2->getSizecolorsizecolor()->getColorcolor()->getName();
                    }
                    
                    $dat = array (
                        "id" => $value2->getIdorderLine(),
                        "description" => $value2->getProductproduct()->getName(),
                        "description_with_variety" => $value2->getProductproduct()->getName()." | ".$size." ".$color,
                        "product_number" => $value2->getProductproduct()->getProductnumber(),
                        "product_id" => $value2->getProductproduct()->getIdproduct(),
                        "variety" => $size." ".$color,
                        "discount" => $discount, 
                        "qty" => $value2->getQty(),
                        "price" => $value2->getValue(),
                        "info" => $value2->getExtraInfo(),
                        "amount" => $value2->getAmount(),
                        "amount_inc_tax" => $value2->getAmountIncTax(),
                        "barcode" => $value2->getProductproduct()->getBarcode()
                    );
                    array_push($data, $dat);
                }
            }

            $commit = getEm()->getConnection()->commit();
            $result  = 'success';
            $message = '';
        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            $result  = 'error';
            $message = $e->getMessage();
            $data = "";
        }

        if ($json == true) {
            $data = array(
                "result"  => $result,
                "message" => $message,
                "data"    => $data,
                "order"   => $order
            );

            $json_data = json_encode($data);
            print $json_data;
        }
        else {
            return $data;
        }
         
    }

    /**
    * Get order details and total amount.
    *
    * @param int $order_id ID for a particular order
    * @param int $json Specifies if the return value should be json or array
    *
    * @return mixed[] Array | boolean
    */
    public function getOrder($order_id, $json=0) {
        // get order
        $em = getEm();
        $rsm = new ResultSetMapping;
        if (!$order = $em->find("Order", $order_id)) {
            return false;
        }

        // get orderlines and total amount
        $total = 0.0;
        $orderlines = $em->getRepository('OrderLine')->findBy(['orderorder'=>$order]);
        foreach ($orderlines as $key => $value) {
            $total += abs($value->getValue() * $value->getQty());
        }

        if ($json == 1) {
            header('Content-type: application/json');
            echo json_encode([[
                'idorder' => $order->getIdOrder(),
                'users_idusers' => $order->getUsersusers()->getIdusers(),
                'status' => $order->getStatus(),
                'extra_info' => $order->getExtraInfo()
            ], $total], JSON_PRETTY_PRINT);
            return true;
        }
        else {
            return [$order, $total, $orderlines];
        }
    }
    public function getCustomer($person_id = false,$do_echo = true) {
        $em = getEm();
        
        if($person_id){
            $customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'idperson' => $person_id));
        }
        else{
            $customers = getEm()->getRepository("Person")->findBy(array('active' => 1));
            //$customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'customer_gp_idcustomer_gp' => 'IS NOT NULL'));

            // $qb = $this->createQueryBuilder('SELECT * FROM Person');
            // $qb->where('active = 1');
            // $qb->where('customer_gp_idcustomer_gp IS NOT NULL');

            // $customers = $qb->getQuery();

            // $rsm = "";
            // $query = $em->createNativeQuery('SELECT * FROM Person WHERE active = 1, customer_gp_idcustomer_gp IS NOT NULL');
            // $query->setParameter(1, 'romanb');

            // $customers = $query->getResult();

        }

        $data = array ();

        foreach ($customers as $value){
            if($phone = getEm()->getRepository("PersonData")->findBy(array('active' => 1, 'personperson' => $value, 'dataType' => 'Phone'))){
                $phone = $phone[0]->getDataValue();
            }else{
                $phone = "";
            }

            $dat = array (
                "id" => $value->getIdperson(),
                "name" => $value->getName(),
                "phone" => $phone
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        return $data;

    }
    public function getUser($user_id = false,$do_echo = true) {
        $em = getEm();
        
        if($user_id){
            $users = getEm()->getRepository("Users")->findBy(array('active' => 1, 'iduser' => $user_id));
        }
        else{
            $users = getEm()->getRepository("Users")->findBy(array('active' => 1));
            //$customers = getEm()->getRepository("Person")->findBy(array('active' => 1, 'customer_gp_idcustomer_gp' => 'IS NOT NULL'));

            // $qb = $this->createQueryBuilder('SELECT * FROM Person');
            // $qb->where('active = 1');
            // $qb->where('customer_gp_idcustomer_gp IS NOT NULL');

            // $customers = $qb->getQuery();

            // $rsm = "";
            // $query = $em->createNativeQuery('SELECT * FROM Person WHERE active = 1, customer_gp_idcustomer_gp IS NOT NULL');
            // $query->setParameter(1, 'romanb');

            // $customers = $query->getResult();

        }

        $data = array ();

        foreach ($users as $value){
            //$phone = getEm()->getRepository("PersonData")->findBy(array('active' => 1, 'personperson' => $value));

            $dat = array (
                "id" => $value->getIdusers(),
                "name" => $value->getLogin()
            );
            array_push($data, $dat);
        }

        $data = json_encode($data);

        if($do_echo){
            echo $data;
        }
        return $data;

    }
    
    public function index(){
        $products = $this->getProduct(false,false);
        $customers = $this->getCustomer(false,false);
        $users = $this->getUser(false,false);

        $em = getEm();
        $pos = $em->getRepository("Pos")->findBy(array('active' => 1, 'idpos' => $_SESSION['pos']));
        // $pos_tmpt_in_pos = $em->getRepository("PosTmptInPos")->findBy(array('active' => 1, 'pospos' => $_SESSION['pos']));
        // if($pos_tmpt_in_pos){
        //     $pos_tmpt = $em->getRepository("PosTmpt")->findBy(array('active' => 1, 'idposTmpt' =>$pos_tmpt_in_pos[0]->getPosTmptposTmpt()));
        //    // var_dump($pos_tmpt[0]->getName());
        //   //  die();
        //     $pos_html = $pos_tmpt[0]->getStructureHtml();
        // } else {
        //     $pos_html = '<div class="col-md-12 text-center">You need to define the template for your terminal <a href="'.URL_BASE.'Pos/layout/index">Click here</a></div>';
        // }

        // $permissions = array('edit_price'=>1, 'edit_qty'=>1, 'edit_discount'=>1, 'edit_final_price'=>1, 'edit_info'=>1); //por enquanto esta tudo 1 - teste

        // $receipt_byttelapp = '';
        // $receipt_title = '';
        // $receipt_text = '';
        // $receipt_two_lines = '';
        // $receipt_print_actual_prices = '';
        // $receipt_show_shop_address = '';
        // $receipt_show_each_product_tax = '';
        // $receipt_show_detailed_description = '';
        // $receipt_hide_comments = '';
        // $receipt_show_article_number = '';
        // $receipt_show_discount_rate = '';
        // $receipt_show_taxes_only_after = '';
        // $receipt_show_tax_tip = '';

        // if($pos_settings_receipts = $em->getRepository("PosSettingsReceipts")->findBy(array('active' => 1, 'idpos' => $pos))){
        //     foreach($pos_settings_receipts as $psr){
        //         switch($psr->getDataType()){
        //             case "title_coupon": $receipt_title = $psr->getDataValue(); break;
        //             case "message_cupom": $receipt_text = $psr->getDataValue(); break;
        //             case "message_byttelapp": $receipt_byttelapp = $psr->getDataValue(); break;
        //             case "print_two_lines": $receipt_two_lines = $psr->getDataValue(); break;
        //             case "when_printing_the_tax_coupon": $receipt_print_actual_prices = $psr->getDataValue(); break;
        //             case "print_shop_address_on_tax_coupon": $receipt_show_shop_address = $psr->getDataValue(); break;
        //             case "print_the_tax_values_on_each_product": $receipt_show_each_product_tax = $psr->getDataValue(); break;
        //             case "print_detailed_product_description": $receipt_show_detailed_description = $psr->getDataValue(); break;
        //             case "hide_comments_entered_in_the_purchase": $receipt_hide_comments = $psr->getDataValue(); break;
        //             case "always_the_article_number_is_printed": $receipt_show_article_number = $psr->getDataValue(); break;
        //             case "print_discount_rate_given_on_the_day": $receipt_show_discount_rate = $psr->getDataValue(); break;
        //             case "prints_the_values_without_taxes_and_after": $receipt_show_taxes_only_after = $psr->getDataValue(); break;
        //             case "includes_tax_tip_amounts_paid": $receipt_show_tax_tip = $psr->getDataValue(); break;
        //         }
        //     }
        // }

        // $do_not_allow_payment_by_credit_card = '';

        // if($pos_settings_features = $em->getRepository("PosSettingsFeatures")->findBy(array('active' => 1, 'idpos' => $pos))){
        //     foreach($pos_settings_features as $psf){
        //         switch($psf->getDataType()){
        //             case "do_not_allow_payment": $do_not_allow_payment_by_credit_card = $psf->getDataValue(); break;
        //         }
        //     }
        // }

        // payment methods
        try {
            $repository = $em->getRepository('PaymentMtd');
            $query = $repository->createQueryBuilder('p')
                ->select(['p.idpaymentMtd', 'p.name'])
                ->where('p.idpaymentMtd != :return_note')
                ->setParameter('return_note', '9999')
                ->getQuery();
        } catch(Exception $e) {
            var_dump($e);
        }

        $payment_methods = $query->getResult(2);

        $array_answer = array (
            '0'=>'',
            'payment_methods' => $payment_methods,
            'products'=>$products,
            'customers'=>$customers,
            'users'=>$users,
            // 'permissions'=>$permissions,
            // // 'pos_html'=>$pos_html,
            // 'receipt_title' => $receipt_title,
            // 'receipt_text' => $receipt_text,
            // 'receipt_two_lines' => $receipt_two_lines,
            // 'receipt_print_actual_prices' => $receipt_print_actual_prices,
            // 'receipt_show_shop_address' => $receipt_show_shop_address,
            // 'receipt_show_each_product_tax' => $receipt_show_each_product_tax,
            // 'receipt_show_detailed_description' => $receipt_show_detailed_description,
            // 'receipt_hide_comments' => $receipt_hide_comments,
            // 'receipt_show_article_number' => $receipt_show_article_number,
            // 'receipt_show_discount_rate' => $receipt_show_discount_rate,
            // 'receipt_show_taxes_only_after' => $receipt_show_taxes_only_after,
            // 'receipt_show_tax_tip' => $receipt_show_tax_tip,
            // 'do_not_allow_payment_by_credit_card' => $do_not_allow_payment_by_credit_card,
            'url_base' => URL_BASE
        ); 

        $navbar = null;
    
        GenericController::template("Terminal", "terminal","index", $navbar, $array_answer, 350);
    }

    public function startOrder($user_id){
        header('Content-type: application/json');
        try {
            $begin = getEm()->getConnection()->beginTransaction();

            $em = getEm();
            $user = $em->getRepository("Users")->findBy(array('active' => 1, 'idusers' =>$user_id));
            $pos_session = $em->getRepository("PosSession")->findBy(array('idposSession' => 1));//este parametro esta assim por motivos de teste

            $order = new Order();

            $order->setUsersusers($user[0]);
            $order->setActive(1);
            $order->setPosSessionposSession($pos_session[0]);
            $order->setTotalAmount(0);
            $order->setInUse(1);
            $order->setRefundNote(0);
            $order->setOrderType(1);// order by default
            $order->setStatus(0);// open order

            getEm()->persist($order);

            getEm()->flush();

            $commit = getEm()->getConnection()->commit();
            // $result  = 'success';
            // $message = 'query success';
            $data = $order->getIdorder();
            $status = 200;
            $extra = false;

        } catch (Exception $e) {
            $rollback = getEm()->getConnection()->rollback();
            // $result  = 'error';
            $extra = $e->getMessage();
            $data = "";
            $status = 500;
        }    

        // $data = array(
        //     "result"  => $result,
        //     "message" => $message,
        //     "data"    => $data
        // );


        // $json_data = json_encode($data);
        // print $json_data;

        returnJson($status, $data, $extra);
        
    }

    private function printFloat($value) {
        return number_format($value, 2, '.', '');
    }

    private function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 0)
            return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    // generates unique token. returns string.
    private function get_token($length = 22, $include_alpha = false, $upper = true) {
        $token = "";
        $codeAlphabet = "123456789";
        if ($include_alpha == true) {
            $codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
        }
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, strlen($codeAlphabet))];
        }
        if ($upper)
            $token = strtoupper($token);
        return $token;
    }

    // print nota fiscal for a specific order
    public function generateNotaFiscal() {
        var_dump($_GET);

        // array de finalidades
        $finNfeArray = [1=>'NF-e normal', 2=>'NF-e complementar', 3=>'NF-e de ajuste', 4=>'Devolução/Retorno'];

        // get order
        $em = getEm();
        if (!$order = $em->find("Order", $_GET['order_id'])) {
            return false;
        }

        // get settings
        $settings = $this->getCompanyInfo();

        // get orderlines and total amount
        $orderlines = $em->getRepository('OrderLine')->findBy(['orderorder'=>$order]);

        // make nota
        $nfe = new Make();
        
        // CABEÇALHO
        $std = new stdClass();
        $std->versao = $settings['nota_versao']; //versão do layout
        $std->Id = null;//se o Id de 44 digitos não for passado será gerado automaticamente
        $std->pk_nItem = null; //deixe essa variavel sempre como NULL
        $nfe->taginfNFe($std);

        // IDENTIFICAÇÃO
        $std = new stdClass();
        $std->tpAmb = $settings['nota_tpAmb'];// 1 - PRODUÇAO, 2 - HOMOLOGAÇAO
        $std->cUF = $settings['nota_cUF'];
        $std->cMunFG = $settings['nota_cMunFG'];
        $std->cNF = get_token(8);// número da nota fiscal
        $std->serie = 8;// série da nota fiscal
        $std->natOp = $settings['nota_natOp'];
        $std->mod = $settings['nota_mod'];// NFE
        $std->nNF = get_token(9);
        $std->dhEmi = date('Y-m-d\TH:i:sP');// formato '2015-02-19T13:48:00-02:00';
        $std->dhSaiEnt = null;
        $std->tpNF = 1;// SAIDA
        $std->idDest = 1;
        $std->tpImp = 1;
        $std->tpEmis = 1;
        $std->cDV = 2;
        $std->finNFe = 1;
        $std->indFinal = 1; 
        $std->indPres = 0;
        $std->procEmi = '3';
        $std->verProc = '1.0.1';
        $std->dhCont = null;
        $std->xJust = null;
        if ($settings['nota_versao'] != '4.0') {
            $std->indPag = 0; //NÃO EXISTE MAIS NA VERSÃO 4.00 // 0=Pagamento à vista; 1=Pagamento a prazo; 2=Outros
        }
        $nfe->tagide($std);

        // IDENTIFICAÇAO DO EMISSOR
        $std = new stdClass();
        $std->xNome = $settings['name'];
        $std->xFant = $settings['fantasy_name'];
        $std->IE = $settings['inscricao_estadual'];
        $std->IEST = null;
        $std->IM = null;
        // $std->CNAE = $settings['cnae'];
        $std->CRT = 1;// 1 = SIMPLES NACIONAL
        $std->CNPJ = $settings['register_number']; //indicar apenas um CNPJ ou CPF
        $std->CPF = null;
        $nfe->tagemit($std);

        // ENDEREÇO DO EMISSOR
        $std = new stdClass();
        $std->xLgr = $settings['nota_xLgr'];
        $std->nro = $settings['nota_nro'];
        $std->xCpl = $settings['nota_xCpl'];
        $std->xBairro = $settings['nota_xBairro'];
        $std->cMun = $settings['nota_cMunFG'];
        $std->xMun = $settings['nota_xMun'];
        $std->UF = $settings['nota_UF'];
        $std->CEP = $settings['address_zip'];
        $std->cPais = $settings['nota_cPais'];
        $std->xPais = $settings['nota_xPais'];
        $std->fone = $settings['nota_fone'];
        $nfe->tagenderEmit($std);


        // DESTINATARIO
        $std = new \stdClass();
        $std->xNome = 'Empresa destinatário teste';
        $std->indIEDest = 9;
        $std->CPF = '05865770411';
        $nfe->tagdest($std);

        $std = new \stdClass();
        $std->xLgr = "Rua Teste";
        $std->nro = '203';
        $std->xBairro = 'Centro';
        $std->cMun = $settings['nota_cMunFG'];
        $std->xMun = $settings['nota_xMun'];
        $std->UF = $settings['nota_UF'];
        $std->CEP = $settings['address_zip'];
        $std->cPais = $settings['nota_cPais'];
        $std->xPais = $settings['nota_xPais'];
        $nfe->tagenderDest($std);

        // insert products into nota


    }

}






























