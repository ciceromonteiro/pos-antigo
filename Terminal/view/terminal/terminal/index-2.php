<style type="text/css">
	.select_customer_employee .btn-primary {
		margin-top: 10px;
	}
	.btn-group-terminal .btn-primary {
		margin-top: -60px;
		padding-top: 15px;
		padding-bottom: 15px;
		padding-left: 25px;
		padding-right: 25px;
	}
	.btn-group-terminal .btn-payment {
		margin-top: 0px;
		padding-top: 50px;
		padding-bottom: 50px;
	}
</style>

<div class="row row-fluid">
	<div class="col-md-8">
		<div class="pull-left">
			<img>
		</div>
		<div class="select_customer_employee">
			<div class="col-md-6">
				<h4>Customers</h4>
				<input type="text" name="customer_input" class="form-control span-4">
				<button type="button" class="btn btn-primary">Select customer</button>
			</div>
			<div class="col-md-6">
				<h4>Employee</h4>
				<input type="text" name="employee_input" class="form-control span-4">
				<button type="button" class="btn btn-primary">Select employee</button>
			</div>
		</div>
		<div class="col-md-12">
			<table class="table-bordered" style="width: 100%">
				<thead>
					<tr>
						<td style="width: 50px"><input type="checkbox" name="checkbox_all"></td>
						<td>ID</td>
						<td>Description</td>
						<td>Qtd</td>
						<td>Price</td>
						<td>Discount</td>
						<td>Tax</td>
						<td>Info</td>
					</tr>
				</thead>
			</table>
		</div>

		<div class="col-md-12">
			<div class="btn-group-terminal">
				<button class="btn btn-primary">Get Parked Order</button>
				<button class="btn btn-primary">Extra Functions</button>
				<button class="btn btn-primary">Discount</button>
				<button class="btn btn-primary">Cancel Order</button>
				<button class="btn btn-payment btn-primary">Pay</button>
				<button class="btn btn-primary">Retur</button>
				<button class="btn btn-primary">Lag nytt gavekort</button>
				<button class="btn btn-primary">Line info</button>
				<button class="btn btn-primary">Delete line</button>
				<button class="btn btn-primary">Order info</button>
				<button class="btn btn-primary">Open credit order</button>
				<button class="btn btn-primary">Kvittering</button>
			</div>
		</div>

		<div class="informations-terminal">
			<div class="pull-left">
				<h4>Last Sale</h4>
			</div>
			<div class="pull-right">
				<h4>Order</h4>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="tabs-terminal">
			<ul class="nav nav-tabs">
			  <li class="active"><a href="#">Favoritter 1</a></li>
			  <li><a href="#">Favoritter 2</a></li>
			  <li><a href="#">Favoritter 3</a></li>
			</ul>
		</div>
	</div>
</div>