<?php
extract($view);
?>
<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<title>Recibo de compra</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,700" rel="stylesheet">
	<style type="text/css">
	/* http://meyerweb.com/eric/tools/css/reset/ 
	   v2.0 | 20110126
	   License: none (public domain)
	*/

	html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, img, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	b, u, i, center,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, canvas, details, embed, 
	figure, figcaption, footer, header, hgroup, 
	menu, nav, output, ruby, section, summary,
	time, mark, audio, video {
		margin: 0;
		padding: 0;
		border: 0;
		font-size: 100%;
		font: inherit;
		vertical-align: baseline;
	}
	/* HTML5 display-role reset for older browsers */
	article, aside, details, figcaption, figure, 
	footer, header, hgroup, menu, nav, section {
		display: block;
	}
	body {
		line-height: 1;
	}
	ol, ul {
		list-style: none;
	}
	blockquote, q {
		quotes: none;
	}
	blockquote:before, blockquote:after,
	q:before, q:after {
		content: '';
		content: none;
	}
	table {
		border-collapse: collapse;
		border-spacing: 0;
	}
	*, html, body {
		font-family: /*'Roboto Mono', */'IBM Plex Mono', monospace;
	}
	body {
		margin: 8px 6px;
		text-align: center;
	}
	table.table {
		width: 100%;
		margin: 6px auto;
	}
	thead > tr > th {
		text-align: center;
	}
	th, td {
		font-size: 9px;
	}
	.nopadding {
		padding: 0 !important;
		margin: 0 !important;
	}
	.bold {
		font-weight: bold;
	}
	.large {
		font-size: 13px;
		text-transform: uppercase;
	}
	.line {
		height: 4px;
		border-bottom: 1px dashed #000;
		margin-bottom: 4px;
	}
	</style>
</head>
<body>
<?php 
$num = 1;
foreach ($order_lines as $key => $item):
	$qty = (int)$item['qty'];
	foreach (range(1, $qty) as $v):?>
<table border="0" class="table container" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th class="bold"><?=$company['name']?></th>
		</tr>
		<tr>
			<th><?=$company['address_district'].', '.$company['address_city'].'/'.$company['address_state']?></th>
		</tr>
		<tr>
			<th><?=$company['address_street'].' '.$company['address_number']?></th>
		</tr>
		<tr>
			<th>CNPJ: <?=$company['register_number']?></th>
		</tr>
		<!-- <tr>
			<th>I.E: <?=$company['inscricao_estadual']?></th>
		</tr> -->
		<tr>
			<th class="line"></th>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" class="table">
					<tr>
						<td style="text-align: left;"><?=date('d/m/Y H:i:s')?></td>
						<td class="bold" style="text-align: right;">NOTA FISCAL No: <?=str_pad($order->getIdorder(), 8, '0', STR_PAD_LEFT)?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th class="bold large">COMPROVANTE DE COMPRA</th>
		</tr>
		<tr>
			<th class="bold">Não possui valor fiscal</th>
		</tr>
		<tr>
			<th class="bold">Pode ser usado para troca</th>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" class="table">
					<tr>
						<td>ITEM&nbsp;</td>
						<td>COD.&nbsp;</td>
						<td>DESCRICAO&nbsp;</td>
						<!-- <td>QTD UN&nbsp;</td> -->
						<!-- <td>VL UNIT (R$)&nbsp;</td> -->
						<!-- <td>DESC (R$)&nbsp;</td> -->
						<!-- <td style="text-align: right;">VL ITEM</td> -->
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th class="line"></th>
		</tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" class="table">
					<?php 
					#$total = 0.0;
					#foreach ($order_lines as $key => $item):
						#$total += (float)$item['amount'];
					
						?>
					<tr>
						<td style="text-align: left;">
							<?=str_pad(($num), 3, '0', STR_PAD_LEFT)?>&#9;
							<?=$item['barcode']?>&#9;
							<?=$item['description_with_variety']?>&#9;
						</td>
					</tr>
					<?php $num++;?>
					<tr>
						<td colspan="2" class="line">&nbsp;</td>
					</tr>
					<tr>
						<td class="2">&nbsp;</td>
					</tr>
					<!-- <tr>
						<td style="text-align: left;">TOTAL&#9;R$</td>
						<td style="text-align: right; width: 20%;"><?=number_format($total, 2, ',', '.')?></td>
					</tr> -->
					<tr>
						
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th class="line"></th>
		</tr>
		<?php if ($order->getPersonperson()):?>
		<tr style="padding-top: 30px;display: block;">
			<th>Cliente: <?=$order->getPersonperson()->getName()?></th>
		</tr>
		<tr style="padding-bottom: 60px;display: block;">
			<th>CPF: <?=$order->getPersonperson()->getDocumentNumber()?></th>
		</tr>
		<tr>
			<th class="line"></th>
		</tr>
		<?php endif?>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="2">&nbsp;</td>
		</tr>
	</thead>
</table>
<?php endforeach;endforeach;?>
</body>
</html>