<?php
extract($view);
?>
<html en="pt-br">
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="Expires" content="-1" />
	<title>Refund Note</title>
	<link rel="stylesheet" type="text/css" href="https://nfe-extranet.sefazvirtual.rs.gov.br/ws/consNFCeXslt/css/sefaz_nfce.css" xmlns:n="http://www.portalfiscal.inf.br/nfe" xmlns:chave="http://exslt.org/chaveacesso" xmlns:r="http://www.serpro.gov.br/nfe/remessanfe.xsd">

	<style type="text/css">
	.NFCCabecalho_SubTitulo, .NFCCabecalho, .NFCCabecalho_Titulo, .NFCCabecalho_SubTitulo1 {
		width: 100% !important;
	}
	body {
		font-size: 10px !important;
	}
	</style>
</head>
<body style="margin: 0px;">

	<div id="nfce">
		<table style="width: 100%;" id="respostaWS" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td valign="top">

					<table width="100%" border="0" cellspacing="0" cellpadding="0" xmlns:n="http://www.portalfiscal.inf.br/nfe" xmlns:chave="http://exslt.org/chaveacesso" xmlns:r="http://www.serpro.gov.br/nfe/remessanfe.xsd">
						<tr>
							<td>
								<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="borda-pontilhada-botton">
														<table class="NFCCabecalho" border="0">
															<tr>
																<td rowspan="4" align="left">&nbsp;</td>
																<td class="NFCCabecalho_SubTitulo" align="left"><?=$company['fantasy_name'].' - '.$company['name']?></td>
															</tr>
															<tr>
																<td class="NFCCabecalho_SubTitulo1" align="left">
																	CNPJ: <?=$company['register_number']?>
																	Inscrição Estadual: <?=$company['inscricao_estadual']?>
																</td>
															</tr>
														</table>
														<table class="NFCCabecalho" border="0">
															<tr>
																<td class="NFCCabecalho_SubTitulo1" align="left">
																	<?=$company['address_street'].' '.$company['address_number'].', '.$company['address_district'].', '.$company['address_city'].'/'.$company['address_state']?>
																</td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="borda-pontilhada-botton">
														<table class="NFCCabecalho" border="0">
															<tr>
																<td class="NFCCabecalho_Titulo NFCBold" align="center">Cupom de Devolução</td>
															</tr>
															<tr>
																<td class="NFCCabecalho_SubTitulo" align="center">Para devolver qualquer mercadoria desta compra, favor apresentar o presente comprovante</td>
															</tr>
															<!-- <tr>
																<td class="NFCCabecalho_Titulo NFCBold" align="center">Contingência off-line da NFC-E</td>
															</tr> -->
														</table>
													</td>
												</tr>
												<tr>
													<td>
														<table border="0" style="width: 100%;">
															<tr>
																<td class="NFCCabecalho_SubTitulo" align="center">Data de Emissão: <?=date('H:i:s d/m/Y')?></td>
															</tr>
															<tr>
																<td class="NFCCabecalho_SubTitulo NFCBold" align="center">Valor: R$ <?=number_format($total, 2, ',', '.')?></td>
															</tr>
														</table>
													</td>
												</tr>
												<tr>
													<td class="borda-pontilhada-top">
														<table border="0" style="width: 100%;">
															<tr>
																<td class="NFCCabecalho_SubTitulo NFCBold" align="center" id="barcode"></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<script type="text/javascript" src="<?=$url_base?>../assets/plugins/jQuery/jquery-2.2.0.min.js"></script>
	<script type="text/javascript" src="<?=$url_base?>../assets/plugins/barcode/jquery-barcode.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#barcode').barcode('<?=$order->getIdorder()?>', 'code128', {barWidth:3, barHeight:30});
	});
	</script>
</body>
</html>
