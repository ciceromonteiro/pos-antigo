
<script src="https://s3.amazonaws.com/cappta.api/js/cappta-checkout.js"></script>
<script src="<?= URL_BASE ?>../assets/app/js/sample.js"></script>
<?php
$products = json_decode($view['products']);
$customers = json_decode($view['customers']);
$users = json_decode($view['users']);

$payment_methods = $view['payment_methods'];

$currency_billet[0] = 1000;
$currency_billet[1] = 500;
$currency_billet[2] = 200;
$currency_billet[3] = 100;
$currency_billet[4] = 50;

$company2  = getEm()->getRepository('PosCompany')->findBy(array('idposCompany' => $_SESSION['poscompany']));

$company = $company2[0];

$company_name = $company->getName();
$company_address = $company->getAddress();
//$company_phone = $company->getPhone();

// function get_payment_buttons($cb){
//     $buttons = "";

//     foreach($cb as $button){
//         $buttons .="<input type='button' class='btn btn-store btn-primary' style='' value='".$button."' onclick='add_billet(".$button.")'>";
//     }

//     return $buttons;
// }

?>

<?php if($_SESSION['mode'] != "Fake"): ?>
    <button style="margin-top:100px;" onclick="thermalPrinter()">Test Thermal Printer</button>
<?php endif; ?>

<!-- <input type="hidden" name="order" id="order" value="0">
<input type="hidden" name="article" id="article" value="0">
<input type="hidden" name="empty_order" id="empty_order" value="1">
<input type="hidden" name="user" id="user" value="0">
<input type="hidden" name="customer" id="customer" value="0"> -->

<div id="product_list"></div>

<div class="container" style="margin-top: 70px; height: 690px; width: 970px;">
    <div class="row">
        <div class="col-md-9">
            <div class="row system-pane" id="products-pane">
                <div class="col-md-6">
                    <div class="input-group">
                        <input readonly="" placeholder="Select Employee" class="form-control display_user_name" type="text">
                        <span class="input-group-btn">
                            <input class="btn btn-primary change_user" value="Change" type="button">
                        </span>
                    </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                    <div class="input-group">
                        <input readonly="" placeholder="Select Customer" class="form-control display_customer_name" type="text">
                        <span class="input-group-btn">
                            <input class="btn btn-primary change_customer" value="Change" type="button">
                        </span>
                    </div>
                </div><!-- /.col -->
                <div class="clearfix"></div>
                <div class="col-md-12" style="margin-top: 8px;">
                    <!-- product search -->
                    <div class="input-group">
                        <input class="form-control" id="input_product_no" placeholder="Search for..." type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" id="add_product" type="button">BUSCAR</button>
                        </span>
                    </div><br>
                </div>
                <div class="col-md-12" id="products_container" style="margin-top: 8px; height: 320px; border-bottom: 1px solid #ccc; overflow: scroll;">
                    <!-- product table -->
                    <table id="product_table" class="table table-bordered table-striped pos_table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>Qtd</th>
                                <th>Preço Und</th>
                                <th>Desconto</th>
                                <th>Preço</th>
                                <th>Info</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div><!-- /.col -->
                <div class="col-md-4">
                    <h3 style="margin: 0;">Total: <span id="total">0</span></h3>
                </div>
                <div class="col-md-8" style="padding-top: 20px;">
                    <button class="btn btn-test pull-right" onclick="scrollPanel('up')"><i class="fas fa-arrow-up"></i></button>
                    <button class="btn btn-test pull-right" onclick="scrollPanel('down')"><i class="fas fa-arrow-down"></i></button>
                </div>
                <!-- favorite buttons -->
                <div class="col-md-12">
                </div>
            </div><!-- /.row -->
            <div class="row system-pane" id="payment-pane">
                <form action="" method="post" id="payment-form">
                    <div class="col-md-6">
                        <h3 style="margin: 0;">Total: <span id="payment_total"></span></h3>
                        <div id="payment_output">
                            <table id="table-payment-list" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Payment Method</th>
                                        <th>Value</th>
                                        <th style="max-width: 40px;">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <h3 style="margin: 0;">Remaining: <span id="payment_remaining"></span></h3>
                    </div>
                    <div class="col-md-6">
                         <h3 style="margin: 0;">Payment Options</h3>
                        
                         <button type="button" class="btn btn-default" aria-label="Left Align" onclick="addPaymentMethodRow('<?= $payment_methods[0]['idpaymentMtd'] ?>', '<?= $payment_methods[0]['name'] ?>')" >
                            <span class="fa fa-dollar-sign" aria-hidden="true"> <?= $payment_methods[0]['name'] ?></span>
                        </button>
                        <button type="button" class="btn btn-default" aria-label="Left Align" onclick="addPaymentMethodRow('<?= $payment_methods[7]['idpaymentMtd'] ?>', '<?= $payment_methods[7]['name'] ?>')" >
                            <?= $payment_methods[7]['name'] ?></span>
                        </button>
                         <div id="resposta"></div>
                        
                        <button type="button" class="btn btn-default" aria-label="Left Align" onclick="useRefundNote()" >
                            <span class="fa fa-hand-holding-usd" aria-hidden="true"> Usar cupom de desconto</span>
                        </button>
                        
                        <hr>
                        <button type="button" class="btn btn-default" aria-label="Left Align" onclick="stopPayment();" >
                            <span class="fa fa-undo-alt" aria-hidden="true"> Voltar</span>
                        </button>
                        
                        <button type="button" class="btn btn-primary" id="refundNote" onclick="saveRefundNote();">Gerar cupom de desconto</button>
                        <div class="clearfix"></div>
                        <button type="button" class="btn btn-danger" aria-label="Left Align" onclick="pay();" >
                            <span class="fa fa-check" aria-hidden="true"> FINALIZAR COMPRA</span>
                        </button>
                    </div>
                </form>
            </div>
            <div class="row system-pane" id="pos-pane">
                <form action="" method="post" id="payment-form">
                    <h3 style="margin: 0;">Fechamento de Caixa</h3>
                    <?php foreach ($payment_methods as $key => $value):?>
                    <div class="col-md-3">
                        <?=$value['name']?>:
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control money" name="closing_parameters[<?=$value['idpaymentMtd']?>]" value="">
                    </div>
                    <div class="clearfix"></div>
                    <?php endforeach?>
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary" onclick="openPane('products-pane')">Voltar</button>
                        <button type="submit" class="btn btn-primary" onclick="openPane('products-pane')">Fechar Caixa</button>
                    </div>
                </form>
            </div>
        </div><!-- /.col -->
        <div class="col-md-3">
            <!-- keypad -->
            <div class="row">
                <div class="col-md-12" style="text-align: center; max-height: 300px;">
                    <img src="<?=URL_BASE?>../assets/images/coxinharia-logo.png" style="max-width: 100%; height: auto; max-height: 150px;">
                </div><!-- /.col -->
            </div>
            <div class="row" style="margin-top: 8px;">
                <!-- <div class="col-sm-12 col-md-12" style="margin-top: 4px;">
                    <button class="btn btn-primary btn-test btn-placeholder">&nbsp;</button>
                    <button class="btn btn-primary btn-test btn-text">Return</button>
                    <button class="btn btn-primary btn-test btn-text">Receipt</button>
                    <button class="btn btn-primary btn-test btn-text">Gift Card</button>
                </div> -->
                <div class="col-sm-12 col-md-12" style="margin-top: 4px;">
                    <button class="btn btn-primary btn-test btn-text" onclick="keypadFunction('qty')">Qtd</button>
                    <button class="btn btn-primary btn-test btn-text" onclick="keypadFunction('unit_price')">Preço</button>
                    <button class="btn btn-primary btn-test btn-text" onclick="keypadFunction('details')" style="padding-left: 0; padding-right: 0;">Detalhes</button>
                    <button class="btn btn-primary btn-test btn-text danger" onclick="keypadFunction('delete')" style="padding: 2%;">Remove Linha</button>
                </div>
                <div class="col-sm-12 col-md-12" style="margin-top: 20px;">
                    <button class="btn btn-primary btn-test" onclick="keypadType('1');">1</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('2');">2</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('3');">3</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('-');"><i class="fas fa-minus"></i></button>
                </div>
                <div class="col-sm-12 col-md-12" style="margin-top: 4px;">
                    <button class="btn btn-primary btn-test" onclick="keypadType('4');">4</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('5');">5</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('6');">6</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('*');"><i class="fas fa-asterisk"></i></button>
                </div>
                <div class="col-sm-12 col-md-12" style="margin-top: 4px;">
                    <button class="btn btn-primary btn-test" onclick="keypadType('7');">7</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('8');">8</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('9');">9</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('<');"><i class="fas fa-chevron-left"></i></button>
                </div>
                <div class="col-sm-12 col-md-12" style="margin-top: 4px;">
                    <button class="btn btn-primary btn-test" onclick="keypadType('0');" style="width: 104px;">0</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType(',');" style="font-size: 20px;">,</button>
                    <button class="btn btn-primary btn-test" onclick="keypadType('=');"><i class="fas fa-check"></i></button>
                </div>
                <div class="col-sm-12 col-md-12" style="margin-top: 20px;">
                    <button class="btn btn-primary btn-test btn-text" onclick="parkOrder();">Salvar</button>
                    <button class="btn btn-primary btn-test btn-text" onclick="applyGeneralDiscount()" style="padding-left: 0; padding-right: 0;">Desconto</button>
                    <button class="btn btn-primary btn-test btn-text" id="open-keyboard"><span class="lnr lnr-keyboard" style="font-size: 33px;"></span></button>
                    <button class="btn btn-primary btn-test btn-text danger" onclick="cancelOrder(true);">Cancel Order</button>
                </div>
                <div class="col-sm-12 col-md-12" style="margin-top: 4px;">
                    <button class="btn btn-primary btn-test btn-text search_parked_orders" style="width: 103px;">Resgatar Compra</button>
                    <button class="btn btn-primary btn-test btn-text" onclick="closePosSession();" style="width: 104px;">Fechar Caixa</button>
                </div>
                <div class="col-sm-12 col-md-12" style="margin-top: 4px;">                    
                    <button class="btn btn-primary btn-test btn-text" onclick="startPayment();" id="payment-button" style="width: 210px; font-size: 14px;">Pagamento</button>
                </div>
            </div><!-- /.row -->
        </div>
    </div><!-- /.row -->
        
</div><!-- /.container -->

<div class="modal" id="modal_choose_user" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select User</h4>
            </div>
            <form id="form_color" name="form_color" class="form_color">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if($users){ ?>
                                <?php foreach($users as $user){ ?>
                                    <button type="button" class="btn btn-primary" onclick="select_user('<?= $user->id ?>','<?= $user->name ?>')"><?= $user->name ?></button>
                                <?php } ?>
                            <?php } ?>  
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal bs-example-modal-lg" id="modal_choose_customer" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select customer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="find_customer" class="table table-bordered" style="width: 100%;">
                            <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Name</td>
                                    <td>CPF</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_choose_product" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Select Product</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="find_product" class="table table-bordered" style="width: 100%;">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Nome</td>
                                    <td>Info</td>
                                    <td>Preço</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_choose_order" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Selecionar Compra Salva</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="find_order" class="table table-bordered" style="width: 100%;">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Identificação</td>
                                    <td>Vendedor</td>
                                    <td>Data</td>
                                    <td>Valor</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modal_order_line" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Detalhes do Produto</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" id="order_line_product_no">
                    <div class="col-md-3">
                        Produto:
                    </div>
                    <div class="col-md-9" id="order_line_product_name"></div>
                    <div class="clearfix"></div>
                    <div class="col-md-3">
                        Vendedor:
                    </div>
                    <div class="col-md-9">
                        <select class="select2" id="order_line_user_id">
                        <?php foreach ($users as $user):?>
                            <option value="<?=$user->id?>"><?=$user->name?></option>
                        <?php endforeach?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" onclick="saveProductDetails();">Salvar</button>
            </div>
        </div>
    </div>
</div>

<iframe style="position: absolute; top: -1000px; left: -800px;" src="" name="print-frame" id="print-frame"></iframe>
<iframe style="position: absolute; top: -1000px; left: -800px;" src="" name="print-nota" id="print-nota"></iframe>
<iframe style="position: absolute; top: -1000px; left: -800px;" src="" name="print-return-note" id="print-return-note"></iframe>
<iframe style="position: absolute; top: -1000px; left: -800px;" src="" name="print-vialoja" id="print-vialoja"></iframe>
<iframe style="position: absolute; top: -1000px; left: -800px;" src="" name="print-viacliente" id="print-viacliente"></iframe>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                        
                <button type="button" class="btn btn-default" aria-label="Left Align" onclick="getValeuPayment();" data-toggle="modal" data-target="#exampleModal4" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true"> Débito</span>
                        </button>
                        <button type="button" class="btn btn-default" aria-label="Left Align" onclick="getValeuPayment();" data-toggle="modal" data-target="#exampleModal2" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true"> Crédito</span>
                        </button>
                        <button type="button" class="btn btn-default" aria-label="Left Align"  data-toggle="modal" data-target="#exampleModal3" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true"> Cancelar Débito/Crédito</span>
                        </button>
            
             
          
                <?php
              /*  foreach ($payment_methods as $item):
                    if ($item['idpaymentMtd'] > 1):
                        ?>

                        <button type="button" class="btn btn-primary payment-method-button" onclick="addPaymentMethodRow('<?= $item['idpaymentMtd'] ?>', '<?= $item['name'] ?>')" style="margin-bottom: 6px;"><?= $item['name'] ?></button>
                        <?php
                    endif;
                endforeach*/
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Fechar</button>
               
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                
                   
                 
          <input type="text" class="form-control" id="txtCreditAmount" placeholder="Valor">
                        <br/>
                        <label>Transação Parcelada?</label>
                        <br/>
                        <label class="radio-inline">
                            <input type="radio" value="false" checked="checked" name="opInstallmenteType"
                                   onclick="selectInstallmenteType(false);">Não</label>
                        <label class="radio-inline">
                            <input type="radio" value="true" name="opInstallmenteType"
                                   onclick="selectInstallmenteType(true);">Sim</label>
                        <br/>
                        <div class="hide" id="installmentDetails">
                            <br/>
                            <label>Tipo de Parcelamento:</label>
                            <br/>
                            <select class="form-control" id="installmentType">
                                <option value="1">Administradora</option>
                                <option value="2">Loja</option>
                            </select>
                            <br/>
                            <br/>
                            <input type="text" class="form-control" id="txtCreditInstallments"
                                   placeholder="Quantidade parcelas">
                        </div>
                   
                        
           
            
      </div>
      <div class="modal-footer">
          <button class="btn btn-default" onclick="creditPayment();" data-dismiss="modal" aria-label="Close">Executar Operação</button>
        
       
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                
                
                        <input type="text" class="form-control" id="administrativePassword"
                               placeholder="Senha administrativa"/>
                        <br/>
                        <input type="text" class="form-control" id="administrativeCode"
                               placeholder="Número de controle"/>
                        <br/>
                                        
               
                        
           
            
      </div>
      <div class="modal-footer">
          <button class="btn btn-default" onclick="paymentReversal();" data-dismiss="modal" aria-label="Close">Executar Operação</button>
 
       
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                 <input type="text" class="form-control" id="txtDebitAmount" placeholder="Valor">
                             
           
            
      </div>
      <div class="modal-footer">
         
                        <button class="btn btn-default" onclick="debitPayment();" data-dismiss="modal" aria-label="Close">Executar Operação</button>
                       
       
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
// default php variables ported to javascript

</script>
<script src="<?= URL_BASE ?>../assets/js/select2.js" type="text/javascript"></script>
<script src="<?= URL_BASE ?>../assets/js/terminal/index.js" type="text/javascript"></script>
