{
    "id": "10E298AB-599D-4D31-B1F4-4398663B0400",

    "emissaoNFeConsumidor": {
        "ambienteProducao": {
            "sequencialNFe": 1,
            "serieNFe": "2",
          
            "csc": {
                "id": "000001", // id do csc na sefaz
                "codigo": "313736363039343000000000000000000000" // inserir aqui o csc
            }
        }
    }
}